app.directive('imageFill', function(){
  return {
    restrict: 'AE',
    link: function(scope, elem, attrs) {
    	elem.imagefill({throttle:1000/60});
    }
  }
});
