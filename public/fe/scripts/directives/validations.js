/**
 * Created by ebautistajr on 3/18/15.
 */

app.directive('userExistsValidator', function($q, User) {
    return {
        require : 'ngModel',
        link: function($scope, element, attrs, ngModel) {
            ngModel.$asyncValidators.exists = function(value) {
                var defer = $q.defer();
                var params = {};
                params[attrs.name] = value;

                User.usernameExists(params, function(data) {
                    if(data.exists == 'true') {
                        defer.reject();
                    } else {
                        defer.resolve();
                    }
                });
                return defer.promise;
            };
        }
    };
}).directive('emailExistsValidator', function($q, User) {
    return {
        require : 'ngModel',
        link: function($scope, element, attrs, ngModel) {
            ngModel.$asyncValidators.exists = function(value) {
                var defer = $q.defer();
                var params = {};
                params[attrs.name] = value;

                User.emailExists(params, function(data) {
                    if(data.exists == 'true') {
                        defer.reject();
                    } else {
                        defer.resolve();
                    }
                });
                return defer.promise;
            };
        }
    };
}).directive('passStrength', function() {
    return {
        // Restrict it to be an attribute in this case
        restrict: 'A',
        // responsible for registering DOM listeners as well as updating the DOM
        link: function(scope, element, attrs) {
            $(element).strength({
                strengthClass: 'strength',
                strengthMeterClass: 'strength_meter',
                strengthButtonClass: 'button_strength',
                strengthButtonText: 'Show Password',
                strengthButtonTextToggle: 'Hide Password'
            });
        }
    }
}).directive('onlyDigits', function () {
  return {
    require: 'ngModel',
    restrict: 'A',
    link: function (scope, element, attr, ctrl) {
      function inputValue(val) {
        if (val) {
          var digits = val.replace(/[^0-9.]/g, '');

          if (digits !== val) {
            ctrl.$setViewValue(digits);
            ctrl.$render();
          }
          return parseFloat(digits);
        }
        return undefined;
      }
      ctrl.$parsers.push(inputValue);
    }
  };
})

/*jeevon*/
// filter dont remove
app.filter('slice', function() {
  return function(arr, start, end) {
    return (arr || []).slice(start, end);
  };
})
.directive('allowPattern', function () {
   return {
       restrict: "A",
       compile: function(tElement, tAttrs) {
           return function(scope, element, attrs) {
       // I handle key events
               element.bind("keypress", function(event) {
                   var keyCode = event.which || event.keyCode; // I safely get the keyCode pressed from the event.
                   var keyCodeChar = String.fromCharCode(keyCode); // I determine the char from the keyCode.

         // If the keyCode char does not match the allowed Regex Pattern, then don't allow the input into the field.
                   if (!keyCodeChar.match(new RegExp(attrs.allowPattern, "i"))) {
           event.preventDefault();
                       return false;
                   }

               });
           };
       }
   };
})
.directive('ngSpace', function() {
  return function(scope, element, attrs) {
      element.bind("keydown keypress", function(event) {
          if(event.which === 32) {
                  scope.$apply(function(){
                          scope.$eval(attrs.ngSpace);
                  });

                  event.preventDefault();
          }
      });
  };
});
