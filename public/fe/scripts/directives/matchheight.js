app.directive('matchHeight', function(){
  return {
    restrict: 'AE',
    link: function(scope, elem, attrs) {
		if (scope.$last === true) {
			elem.matchHeight(
    		{
    			byRow: true,
    			property: 'height',
    			target: null,
    			remove: false
    		});
			console.log('page Is Ready!');
		}
		
    }
  }
});
