app.controller("changepasswordCtrl", function($scope, Factory, AccountFactory, $window) { "use strict";
	var requestCode = Factory.urlLastIndex();
	$scope.saveChanges = function(password) {
		$scope.isBusy = true;
		if($scope.changepasswordForm.$invalid === true) {
  		Factory.toaster("warning","Warning!","Please check your password.");
  	} else {
  		AccountFactory.changepassword(requestCode, password.new, function(data) {
  				if(data.status == "success") {
  						$scope.isSuccess = true;

  						setTimeout(function(){
  							$window.location.href = "/";
  						}, 5000);

  				} else {
  						Factory.toaster("error","ERROR!","Something went wrong, please try again.");
  				}
  				$scope.isBusy = false;
  		});
  	}
	};
});
