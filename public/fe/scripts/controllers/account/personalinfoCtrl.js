app.controller("personalinfoCtrl", function($scope, Config, AccountFactory, Factory, store) { 'use strict';

  $scope.saveChanges = function(member) {
    $scope.isBusy = true;

    if(member.firstname === undefined || member.lastname === undefined || member.username === undefined || member.email === undefined) {
      Factory.toaster("warning","Warning!","Please check the required fields");
      $scope.isBusy = false;
    } else {
      AccountFactory.updatepersonalinfo(member, function(data){
          if(data.status == "ok") {
              Factory.toaster("success","Success!","Alright!");
              $scope.member = data.member;
          } else {
              Factory.toaster("error","Error!","wee.");
          }
          $scope.isBusy = false;
      });
    }
  };

});
