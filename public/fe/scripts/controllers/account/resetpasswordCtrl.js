app.controller("resetpasswordCtrl", function($scope, Factory, AccountFactory) { "use strict";
  $scope.saveChanges = function(password) {
  	if($scope.resetpasswordForm.$invalid === true) {
  		Factory.toaster("warning","Warning!","There are invalid fields in your form.");
  	} else {
  		AccountFactory.resetpassword($scope.member.id, password, function(data){
  			if(data.hasOwnProperty("success")) {
  				Factory.toaster("success","Success","Successfully changed password");
  				$scope.password = angular.copy(undefined,$scope.password);
  			} else if (data.hasOwnProperty("warning")) {
  				Factory.toaster("warning","Warning!","Current password is incorrect!");
  			} else {
  				Factory.toaster("error","Error!","Something went wrong! Please try again.");
  			}
  		});
  	}
  };

  $scope.forgotPassword = function(member) {
  	Factory.modaL("forgotpassword","md",
  		function($scope, $modalInstance) {
  			$scope.close = function() {
  				$modalInstance.dismiss();
  			};
  			$scope.sendEmail = function() {
  				$scope.isBusy = true;
  				AccountFactory.forgotpassword(member, function(data){
  					if(data.hasOwnProperty("success")) {
  						Factory.toaster("success","Success","Change Password Request has been sent to your email.");
  					} else {
  						Factory.toaster("error","Error!","Something went wrong, please try again.");
  					}
  					$scope.isBusy = false;
  					$modalInstance.dismiss();
  				});
  			};
  		}
  	);
  };

});
