'use strict';

app.controller("contactUsCtrl", function($scope, $http, Config, Factory, $modal, $window) {

  $scope.openContactUs = function() {
    Factory.modaL("contactus","md", openContactUsCtrl);
  };

  var openContactUsCtrl = function($scope, $modalInstance) {
    $scope.x = function() {
      $modalInstance.dismiss();
    };

    $scope.contact = {};

    Factory.stateswithcenter(function(data) {
      $scope.contactUsStates = data;
    });

    var state_code = "";

    function centerList(state_code) {
      $http({
        url: Config.ApiURL + "/contactus/centerlist",
        method: "post",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param({statecode:state_code})
      }).success(function(data, status, headers, config) {
        $scope.contactUsCenters = data;
        $scope.contact.center = "";
        $scope.contact.email = "customerservice@bodynbrain.com";
        $scope.contact.phone = "877-477-9642";
      });
    }
    centerList(state_code);

    $scope.changeContactState = function(state_code) {
      centerList(state_code);
      $scope.compose = false;
    };

    $scope.changeContactCenter = function(centerslugs) {
      $scope.compose = false;
      if(centerslugs == "") {
          $scope.contact.email = "customerservice@bodynbrain.com";
          $scope.contact.phone = "877-477-9642";
      } else {
        $http({
          url: Config.ApiURL + "/contactus/centercontact",
          method: "post",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param({centerslugs:centerslugs})
        }).success(function(data, status, headers, config) {
            $scope.contact.email = data.email;
            $scope.contact.phone = data.phonenumber;
        });
      }

    };

    $scope.visitCenter = function(centerslugs) {
      $window.location.href = "/"+centerslugs;
    };

    $scope.openEmailbox = function() {
      $scope.compose = true;
    };

    $scope.email = {};
    $scope.sendEmail = function(email) {
      $scope.isBusy = true;
      email['centerslugs'] = $scope.contact.center;
      Factory.sendEmail(email, function(data) {
        $scope.isBusy = false;
        $scope.email = angular.copy(undefined, $scope.email);

        if(data.status == "success") {
            Factory.toaster("success","Contact Us","Message sent.");
        } else {
            Factory.toaster("error","Contact Us","Error occured, please try again.");
        }

      });
    };
  };

});
