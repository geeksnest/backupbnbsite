app.controller("bnbbuzzCtrl", function ($scope, Config, $sce, bnbbuzzFactory) { 'use strict';
    $scope.BaseURL = Config.BaseURL;

    var category = 'all';
    $scope.pagemaxsize = 5;
    var page = 1;
    $scope.amazonlink = Config.amazonlink;

    var getnewscategories = function(){
        bnbbuzzFactory.getnewscategories(function(data){
            $scope.newscategory = data.newscategory;
        });
    };

    getnewscategories();

    var getnewsdata = function(){
        angular.element('.page-loading').removeClass('hidden');
        bnbbuzzFactory.getnews(category, page, function(data){
            $scope.newslist = data.newslist;
            $scope.newstotalitems = data.totalnews;
            $scope.newsCurrentPage = data.index;
            angular.element('.page-loading').addClass('hidden');
        });
    };

    getnewsdata();

    $scope.getnewsbycat = function(catdata){
        page = 1;
        category = catdata;
        getnewsdata();
    };

    $scope.setPage = function (pageNo) {
        page = pageNo;
        getnewsdata();
    };

});
