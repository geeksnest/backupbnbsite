app.controller("MemberCtrl", function ($scope, Config, Member, Login, store, $window, jwtHelper, Facebook, GooglePlus, twitterService, $timeout) { 'use strict';
    $scope.login = false;
    $scope.signup = false;
    $scope.forgotpassword = false;
    $scope.member = Login.getCurrentUser();
    $scope.wizard = 1;
    $scope.error = {};
    $scope.btn = "next";
    $scope.base = Config.BaseURL;
    $scope.state = [];
    $scope.center = [];
    $scope.perc = 33.333;

    twitterService.initialize();

    var hidecontent = function() {
      if($(window).width() <= 1000){
        $(".wrapper").addClass("hide");
      }
    }

    var showcontent = function() {
      if($("#mainContent").css("display") == "none"){
        $("#mainContent").fadeToggle( "slow", "swing" ); //this is the body of main.volt
      }
      if($(".home-swiper").css("display") == "none"){
        $(".home-swiper").fadeToggle( "slow", "linear" ); // this is the body of home.volt
      }
    }

    var fpassres = function() {
      if($(window).width() <= 1000) {
        $(".wrapper").removeClass("hide");
        $scope.login = false;
        $scope.signup = false;
        $scope.forgotpassword = false;
        $timeout(function() {
          if($("#forgotpass-res").css('display') == "none"){
            $("#forgotpass-res").fadeToggle( "slow", "linear" );
            $("#forgotpass-res").css("visibility","visible");
            $("#topMenu-res").fadeOut( "slow", "linear" );
            if($("#mainContent").length == 0){
              $("#mainContent").fadeToggle( "slow", "swing" ); //this is the body of main.volt
            }
            if($(".home-swiper").length == 0){
              $(".home-swiper").fadeToggle( "slow", "linear" ); // this is the body of home.volt
            }
          }
        }, 500);
      }
    }

    var hideforgotpass = function() {
      if($(window).width() <= 1000) {
        if($("#forgotpass-res").css('display') == 'block'){
          $("#forgotpass-res").fadeOut( "slow", "linear" );

          $timeout(function() {
            showcontent();
          }, 500);
        }
      }
    }

    $scope.closebtn = function() {
        $scope.login = false;
        $scope.signup = false;
        $scope.forgotpassword = false;
        $scope.alert = null;
        $scope.wizard = 1;
        $scope.perc = 33.333;
        $scope.error = {};
        $(".wrapper").removeClass("hide");
        $timeout(function() {
          showcontent();
        }, 500);
    };

    $scope.loginbtn = function() {
        if($scope.login === false){
            hideforgotpass();
            $scope.login = true;
            $scope.signup = false;
            $scope.forgotpassword = false;
            $scope.member = {};
            $scope.alert = null;
            $("#loginreg").removeClass("hide");
            // $timeout(function() {
              hidecontent();
            // },1000);
        }
    };

    $scope.loginaction = function(member) {
        if(!member.email && !member.password){
            $scope.alert="Please input your email and password";
        } else {
            Member.login(member, function(data) {
                if(data.hasOwnProperty('error')){
                    $scope.alert = data.error;
                } else {
                    store.set('jwt', data.success);
                    getuserbag();
                    $scope.member = Login.getCurrentUser();
                    $window.location = '/' + $scope.member.username;
                }
            });
        }
    };

    $scope.signupbtn = function() {
        if($scope.signup === false){
            hideforgotpass();
            $scope.signup = true;
            $scope.login = false;
            $scope.forgotpassword = false;
            $scope.member = {};
            $scope.alert = null;
            $scope.wizard = 1;
            $("#loginreg").removeClass("hide");
            // $timeout(function() {
              hidecontent();
            // },1000);
        }
    };


    var validatepass = function(member) {
        if(!member.password){
            $scope.error.password = "Password is required";
            return false;
        } else {
          if(member.password.length >= 8){
            $scope.error.password = undefined;
            if(!member.confpass){
                $scope.error.confpass = "Please confirm your password";
            }else {
                if(member.password != member.confpass){
                    $scope.error.confpass = "Password did not match!";
                    return false;
                }else if(member.password === member.confpass){
                    $scope.error.confpass = undefined;
                    return true;
                }
            }
          } else {
            $scope.error.password = "Minimum of 8 characters.";
          }
        }
    };

    var validatename = function(name, type) {
        if(type == 'fname'){
            if(name){
                $scope.error.firstname = undefined;
                return true;
            }else {
                $scope.error.firstname = "First name is required";
                return false;
            }
        }else {
            if(name){
                $scope.error.lastname = undefined;
                return true;
            }else {
                $scope.error.lastname = "Last name is required";
                return false;
            }
        }
    };

    $scope.next = function(mem) {
        switch($scope.wizard){
            case 1:
                var res = validatepass(mem);
                if(mem.email){
                  var pattern = new RegExp(/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i);
                  if(pattern.test(mem.email)){
                    Member.validateEmail(mem.email, function(data) {
                        if(res && data.exist===false){
                            $scope.error.email = undefined;
                            $scope.wizard = 2;
                            $scope.btn = "register";
                            $scope.perc = 66.666;
                        }else {
                            $scope.error.email = "Email already exist";
                        }
                    });
                  } else {
                    $scope.error.email = "Invalid email address";
                  }
                } else {
                    $scope.error.email = "Email is required";
                    return false;
                }
                break;
            case 2:
                var res1 = validatename(mem.firstname, 'fname');
                var res2 = validatename(mem.lastname, 'lname');
                if(res1 && res2 && mem.phoneno){
                    $scope.wizard = 3;
                    $scope.perc = 100;
                    Member.getstates(function(data) {
                        $scope.states = data;
                        $scope.states.unshift({ state: "Select State", state_code: undefined });
                    });
                }
                break;
            case 3:
                Member.register(mem, function(data) {
                    if(data.hasOwnProperty('success')){
                        // $scope.msg = data.success;
                        $scope.closebtn();
                        // $scope.member = {};
                        store.set('jwt', data.success);
                        $scope.member = Login.getCurrentUser();
                        $window.location = '/' + $scope.member.username;
                    }else {
                        $scope.msg = data;
                    }
                });
                break;
        }
    };

    $scope.changestate = function(state) {
      if(state){
        Member.getcenter(state.state_code, function(data) {
            $scope.centers = data;
            $scope.centers.unshift({ centertitle: "Select State" });
        });
      } else {
        $scope.centers = [{ centertitle: "Select State" }];
      }
    };

    $scope.logout = function() {
        store.remove('jwt');
        store.remove('bag');
        $window.location = '/';
    };

    // social media login
    // facebook
    var getfbdata = function() {
        Facebook.getLoginStatus(function(response) {
            if(response.status == 'connected'){
                Facebook.api('/me', {fields: 'last_name, email, first_name, picture, hometown'}, function(data) {
                    Member.loginFB(data, function(res) {
                        store.set('jwt', res.success);
                        $scope.member = Login.getCurrentUser();
                        $window.location = '/';
                    });
                });
            }
        });
    };

    $scope.loginFb = function() {
        Facebook.login(function(res) {
            getfbdata();
        });
    };

    // google+
    $scope.logingplus = function () {
        GooglePlus.login().then(function (authResult) {
            GooglePlus.getUser().then(function (user) {
                Member.logingplus(user, function(res) {
                    store.set('jwt', res.success);
                    $scope.member = Login.getCurrentUser();
                    $window.location = '/';
                });
            });
        }, function (err) {
            console.log(err);
        });
    };

    // twitter
    $scope.logintwitter = function() {
        twitterService.connectTwitter().then(function() {
            if (twitterService.isReady()) {

            } else {

            }
        });
    };

    //forgot password
    $scope.forgotpass = function() {
      if($(window).width() > 1000){
        $scope.login = false;
        $scope.signup = false;
        $scope.forgotpassword = true;
      } else {
        fpassres();
      }
    }

    $scope.$watch(function() {
        if(store.get('bag') === null || store.get('bag') === undefined || store.get('bag').length === 0){
            return 0;
        }else {
            return store.get('bag').length;
        }
    }, function() {
        if(store.get('bag') == null) {
            $scope.bagcount = 0;
        }else {
            $scope.bagcount = store.get('bag').length;
        }
    });

    var getuserbag = function() {
        Member.getbag(Login.getCurrentUser().id, function(data) {
            store.set('bag', data.bag)
        });
    };

    $scope.$watch(function()  {
        return store.get('jwt');
    }, function() {
        if(store.get('jwt') !== null){
            // $scope.member = Login.getCurrentUser(); -commented by null
            var token = store.get('jwt');
            Member.getCurrentUser(jwtHelper.decodeToken(token).id, function(data){
                $scope.member = data;
            });
        }
    });

    $scope.$watch(function() {
        if(store.get('new') !== null || store.get('new') !== undefined) {
            return store.get('new');
        }
    }, function() {
        if(store.get('new') !== null || store.get('new') !== undefined) {
            $scope.new = store.get('new');
        }
    });

    if(store.get('new') !== null || store.get('new') !== undefined) {
        $timeout(function() {
            $scope.new = undefined;
            store.remove('new');
        },5000);
    }
});
