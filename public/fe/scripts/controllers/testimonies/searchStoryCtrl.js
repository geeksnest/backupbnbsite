app.controller("searchStoryCtrl", function($scope, Config, $window, toaster) {
   $scope.searchStory = function(keyword) {
     if(keyword != undefined && keyword != "") {
         $window.location.href = "/success-stories/"+keyword+"/1";
     }
   };
});
