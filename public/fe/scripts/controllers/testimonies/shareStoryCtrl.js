app.controller("shareStoryCtrl", function($scope, Config, Upload, toaster, UploadFactory, ShareStoryFactory, Factory, LocIndexFactory, $sce) {

  $scope.shareStory = function() {
    Factory.modal("shareStoryTpl",shareStoryCTRL,"md","sharestory");
  };

  var shareStoryCTRL = function($scope, $modalInstance) {
    $scope.fileName = 'Browse Picture';
    $scope.disabled = true;
    $scope.onChangePic = function(photo) {
      if(photo.length  > 0) {
        $scope.fileName = photo[0].name;
      }
    };
    $scope.shareStoryMaster = {};
    $scope.states = [];
    $scope.centers = [];
    $scope.shareStory = {};

    LocIndexFactory.states(function(data) {
      $scope.states = data;
      $scope.states.unshift({ state: "Select State", state_code: undefined });
      $scope.centers = [{ centertitle: "Select Center", centerid: undefined }];
    });
    var validemail = false;
    $scope.onemail = function(email) {
      var pattern = new RegExp(/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i);
      if(pattern.test(email)){
        $scope.erroremail = undefined;
        validemail = true;
      } else {
        $scope.erroremail = $sce.trustAsHtml("<i class='fa fa-close'></i> Invalid email address");
        validemail = false;
      }
    }

    $scope.onstate = function(state) {
      if(state) {
        LocIndexFactory.selectstate(state.state_code, function(data) {
          $scope.centers = data;
          $scope.centers.unshift({ centertitle: "Select Center", centerid: undefined });
        });
        $scope.disabled = false;
      } else {
        $scope.centers = [{ centertitle: "Select Center", centerid: undefined }];
        $scope.shareStory.center = undefined;
        $scope.disabled = true;
      }
    }

    $scope.submitStory = function(story) {
      if(story && validemail == true) {
        $scope.isSaving = true;
        if(story.hasOwnProperty('photo') == true) {
          if(story.photo != null) {

            if(story.photo[0].size <= 2000000) {
              UploadFactory.singleShareStory(story.photo[0], function(data) {
                data.then(function(data) {
                  story['photo'] = data.config.file.name;
                  ShareStoryFactory.submitStory(story, function(data) {
                    if(data.error==false) {
                      successSharing();
                      $modalInstance.dismiss();
                    } else {
                      errorFailed();
                    }
                  });
                });
              });
            } else {
              picSizeInvalid();
            }
          } else {
            errorNoPicture();
          }
        } else {
          errorNoPicture();
        }
      }
      function errorNoPicture() {
        $scope.isSaving = false;
        toaster.pop({
          type: 'error',
          title: 'Error!',
          body: 'You forgot to browse a picture.',
          showCloseButton: true
        });
      }
      function errorFailed() {
        $scope.isSaving = false;
        toaster.pop({
          type: 'error',
          title: 'Error!',
          body: 'Failed! Please try again',
          showCloseButton: true
        });
      }
      function picSizeInvalid() {
        $scope.isSaving = false;
        toaster.pop({
          type: 'warning',
          title: 'Warning!',
          body: 'Please choose a picture not larger than 2mb.',
          showCloseButton: true
        });
      }
      function successSharing() {
        $scope.isSaving = false;
        toaster.pop({
          type: 'success',
          title: 'Success!',
          body: 'Thanks for sharing your story with us.  -BodyNBrain.com',
          showCloseButton: true
        });
        $scope.shareStory = angular.copy($scope.shareStoryMaster, $scope.shareStory);
        $scope.fileName = "Browse Picture";
      }
    };

    $scope.exit = function() {
      $modalInstance.dismiss();
    };
  };



});
