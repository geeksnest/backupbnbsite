app.controller("ReadCtrl", function ($scope, Config, pressFactory, $window, $sce) { 'use strict';
  $scope.amazonlink = Config.amazonlink+"/uploads/newsimage";
  $scope.BaseURL = Config.BaseURL;
  $scope.show = false;

  pressFactory.readpage(slug, function(_rdata){
    if(_rdata.error !== true) {
      $scope._ddata = _rdata;
     	$scope.htmlbody = $sce.trustAsHtml(_rdata.body);
     	if(_rdata.banner.substring(1, 7) == 'iframe'){
     		$scope._ddata.bannertype = 'video';
     		$scope._ddata.banner = _rdata.banner.replace('style="width:500px; height:300px;"','style="width: 868px; height: 500px;margin: auto;display: block;"');
     		$scope._ddata.banner = $sce.trustAsHtml($scope._ddata.banner);
     	}else {
     		$scope._ddata.bannertype = 'image';
     	}
      if(_rdata.banner === null || _rdata.banner === '' || _rdata.banner === undefined){
        $scope.show = true;
        angular.element('.press-read-img').removeClass('hidden');
      }
      console.log(_rdata);
    } else {
      $window.location.href = "/route404";
    }

  });

});
