app.controller("PressCtrl", function ($scope, Config, pressFactory, $sce, store) { 'use strict';
  $scope.amazonlink = Config.amazonlink+"/uploads/newsimage";
  $scope.data = {};
  $scope.press = true;
  var num = 9;
  var off = 1;
  var keyword = null;
  $scope.currentstatusshow = '';
  var loadlist  = function(num, off, keyword){
    pressFactory.loadlist(num,off, keyword, function(data){

      console.log(data.data);

      var returnYoutubeThumb = function(item){
            var x = '';
            var thumb = {};
            if(item){
                var newdata = item;
                // var x;
                x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
                thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
                thumb['yid'] = x[1];
                return thumb;
            }else{
                return x;
            }
        };

      angular.forEach(data.data, function(value, key) {
        var myDate = new Date(data.data[key].date);
        var newdate = myDate.toString().split(' ');
        data.data[key].date = newdate[1]+" "+ newdate[2]+", "+ newdate[3];
        if(data.data[key].banner.substring(0, 7) == '<iframe'){
          var yid = returnYoutubeThumb(data.data[key].banner);
          data.data[key].banner = $sce.trustAsHtml('<iframe src="https://www.youtube.com/embed/' + yid['yid'] + '" frameborder="0" style="width:100%; height:320px; margin: auto; display: block;" allowfullscreen></iframe>');
          data.data[key].type = "video";
        }else {
          data.data[key].type = "image";
        }
      });
      $scope.data = data.data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    });
  };

  // loadlist(num, off, keyword);

  $scope.search = function (keyword) {
    var off = 1;
    pressFactory.loadlist('6','1', keyword, function(data){
      angular.forEach(data.data, function(value, key) {
        var myDate = new Date(data.data[key].date);
        var newdate = myDate.toString().split(' ');
        data.data[key].date = newdate[1]+" "+ newdate[2]+", "+ newdate[3];
      });
      $scope.data = data.data;
      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    });
  };

  $scope.resetsearch  = function(){
    $scope.pressFactory = undefined;
    loadlist();
  };

  $scope.numpages = function (off, keyword) {
    pressFactory.loadlist('6', off, $scope.searchtext, function(data){
      angular.forEach(data.data, function(value, key) {
        var myDate = new Date(data.data[key].date);
        var newdate = myDate.toString().split(' ');
        data.data[key].date = newdate[1]+" "+ newdate[2]+", "+ newdate[3];
      });
      $scope.data = data.data;

      $scope.maxSize = 5;
      $scope.bigTotalItems = data.total_items;
      $scope.bigCurrentPage = data.index;
    });
  };

  $scope.setPage = function (pageNo) {
    loadlist(num, pageNo, keyword);
  };

});
