app.controller("locindexCtrl", function($scope, Config, LocIndexFactory, uiGmapGoogleMapApi, MapFactory, uiGmapIsReady) {
  var lat = 1; var lon = 1; var events; var maplocations = [];
  $scope.location = { state : "Select State"};
  $scope.isOpen = false;
  $scope.offset = 0;
  var defaultlimit = 8;
  $scope.limit = defaultlimit;
  var counter = 1;

  $scope.scroll = function(scroll) {
    if(scroll == 'down'){
      counter++;
    } else {
      counter--;
    }
    $scope.offset = (counter * defaultlimit) - defaultlimit;
    $scope.limit = (counter * defaultlimit);
  };

  $scope.selectstate = function(states) {
    maplocations = [];
    $scope.centertoggle = true;

    angular.element("#mainfooter").addClass("locationfooter-res");
    $scope.location.state = states.state;
    LocIndexFactory.selectstate(states.state_code, function(data){
      $scope.centers = data;
      MapFactory.multipin(data, function(data) {
        uiGmapGoogleMapApi.then(function(maps) {
          if( typeof _.contains === 'undefined' ) {
            _.contains = _.includes;
          }
          if( typeof _.object === 'undefined' ) {
            _.object = _.zipObject;
          }

          // uiGmapIsReady.promise().then(function() {
            $scope.randomMarkers = data.markers;
          // });

          $scope.map = data.map;
          $scope.options = data.options;
          $scope.isOpen = false;
        });
      });
    });
  };

  LocIndexFactory.states(function(data){
    var arraykey = 0;
    var loc = [];
    var lockey = 0;
    $scope.statesmobile = data;
    angular.forEach(data, function(value, key) {
      if(lockey === 0) {
        loc.push([value]);
        lockey++;
      } else {
        if(lockey <= 4) {
          loc[arraykey].push(value);
          lockey++;
        } else {
          loc.push([value]);
          lockey = 1;
          arraykey++;
        }
      }
    });
    $scope.states = loc;
  });

  $scope.select = function() {
    $scope.offset = 0;
    $scope.limit = defaultlimit;
    counter = 1;
    if($scope.isOpen){
      $scope.isOpen = false;
    } else {
      $scope.isOpen = true;
    }
  };

  // ====================== vvv jquery & javascripts vvv ============================
  $('#locationbanner').imagefill({throttle:1000/60});

});
