app.controller("centerCtrl", function($scope, Config, CenterFactory, $modal, $sce, $filter, uiGmapGoogleMapApi, MapFactory, store, Factory, $timeout) {
  $scope.amazonlink = Config.amazonlink;

  var configulr = Config.BaseURL;
  var re = new RegExp(configulr+'\/(.*)');
  var url = window.location.href;
  var auth = url.match(re);
  // var centerslugs = auth[0].substring(auth[0].lastIndexOf('/')+1);
  var centerslugs = window.location.pathname.split( '/' )[1];
  $scope.slug = centerslugs;
  console.log(centerslugs);

  var lat = ""; var lon = ""; var centerevents;

  CenterFactory.properties(centerslugs, function(data){

    $scope.centerlat = lat = data.center.lat;
    $scope.centerlon = lon = data.center.lon;
    // centerevents = data.centerevents;

    if(window.location.pathname.split( '/' )[2]) {
      $scope.viewmonthsched();
    }

    MapFactory.singlemap(data.center, function(data) {
      $scope.randomMarkers = data.markers;
      $scope.map = data.map;
      $scope.options = data.options;
    });

  });

  // $scope.viewfullevents = function() {
  //   var modalInstance = $modal.open({
  //       templateUrl: 'viewfullevents',
  //       controller: viewfulleventsCTRL,
  //       size: "lg"
  //   });
  // };
  // var viewfulleventsCTRL = function($modalInstance, $scope) {
  //   centerevents.map(function(val) {
  //     val.desc = $sce.trustAsHtml(val.edesc);
  //     val.datestart = $filter('date')(val.edatestart, "MM/dd");
  //     val.day = $filter('date')(val.edatestart, "EEEE");
  //   });
  //   $scope.monthsched = false;
  //   $scope.centerevents = centerevents;
  //   $scope.exit = function() {
  //     $modalInstance.dismiss();
  //   };
  // };

  $scope.viewmonthsched = function() {
    var modalInstance = $modal.open({
        templateUrl: 'viewfullevents',
        controller: viewmonthschedCTRL,
        size: "lg"
    });
  };
  var viewmonthschedCTRL = function($scope, $modalInstance) {
    var newdate = moment().format('YYYY-MM-DD');
    var params = {
      'date': newdate,
      'centerslugs':centerslugs
    };

    $scope.togglecalendar = function(toggl){
      if(toggl === true) {
        CenterFactory.getcentermonthlysched(params, function(data) {
          $scope.centerevents = data;
          $scope.monthsched = true;
          console.log(data);
        });
      } else {
        CenterFactory.getcenterevents(params, function(data) {
          data.map(function(val) {
            val.desc = $sce.trustAsHtml(val.edesc);
            val.datestart = $filter('date')(val.edatestart, "MM/dd");
            val.day = $filter('date')(val.edatestart, "EEEE");
          });
          $scope.centerevents = data;
          $scope.monthsched = false;
        });
      }
    };

    $scope.togglecalendar(true);

    $scope.viewmore = function(id, date, start, index) {
      openviewmore(id, date, start, index);
      $('.modal:last-child').css('background', 'rgba(0,0,0,0.5)');
      $('.fullevent').css('background', 'rgba(0,0,0,0.5)');
      $('.fullevent').css('padding', 0);
      $('.fullevent').css('margin', 0);
    };

    $scope.exit = function() {
      $modalInstance.dismiss();
    };
  };

  $scope.classselecteddate = function(datedata){

    var newdate = moment(datedata).format('YYYY-MM-DD');

    var params = {
      'date': newdate,
      'centerslugs':centerslugs
    };

    CenterFactory.getScheduleAction(params, function(data) {
      $scope.schedulelist = data.schedule;
      schedulelist = data.schedule;
      $scope.days = data.days;
      $scope.schedulelist.unshift({});
    });

  };

  $scope.viewfullnews = function() {

    Factory.modaL(
      tplUrl = "viewfullnews",
      size = "lg",
      ctrl = function($scope, $modalInstance) {
              $scope.amazonlink = Config.amazonlink;
              CenterFactory.fullnews(centerslugs, function(data){
                $scope.centerslugs = data.centerslugs;
                $scope.fullnews = data.fullnews;
              });
              $scope.exit = function() { $modalInstance.dismiss(); };
      }
    );
  };

  $scope.viewfulltestimonials = function() {

    Factory.modaL(
      tplUrl = "viewfulltestimonials",
      size = "lg",
      ctrl = function($scope, $modalInstance) {

              $scope.amazonlink = Config.amazonlink;
              $scope.fulltestimonials = [];
              var offset = 1;

              function fulltestimonials(offset) {
                console.log(offset);
                CenterFactory.fulltestimonials(offset, centerslugs, function(data){
                  data.fulltestimonials.map(function(val) {
                    $scope.fulltestimonials.push(val);
                  });
                  $scope.totalItems = data.totalItems;
                  if($scope.fulltestimonials.length == data.totalItems) { $scope.showmore = false; }
                  else { $scope.showmore = true; }
                });
              }
              fulltestimonials(offset++);

              $scope.showMore = function() {
                fulltestimonials(offset++);
              };

              $scope.exit = function() { $modalInstance.dismiss(); };
      }
    );
  };

  $scope.viewmoresched = function(id, date, start, index) {
    var param = { id:id, centerslugs:centerslugs, date:date, start: start, index };
    var modalInstance = $modal.open({
        templateUrl: "viewsched",
        controller: function($scope, $modalInstance, data) {
            $scope.index = index;
            CenterFactory.viewsched(data, function(data){
              $scope.sched = data[0];
            });

            $scope.exit = function() { $modalInstance.dismiss(); };
        },
        size: "md",
        backdrop: true,
        resolve: {
            data: function() {
                return param;
            }
        }
    }).result.then(function() {

    }, function() {
      $('.modal').css('background', 'none');
      $('.fullevent').css('background', 'none');
      $('.fullevent').css('padding', '30px');
      $('.fullevent').css('margin', '0px -15px');
    });
  };

  var openviewmore = function(id, date, start, index) {
    $scope.viewmoresched(id, date, start, index);
  };
});

angular.element(document).ready(function(){
    angular.element('.center-sec').removeClass('hidden');
  });
