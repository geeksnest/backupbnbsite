app.controller("wsTitleCtrl", function($scope, Config, starterspackageFactory, WsTitleFactory, MapFactory, toaster, DateFactory, deviceDetector) {
  $scope.pagetitle = "Find A Center";

  if(deviceDetector.isDesktop()){ var device = 'desktop'; }
  else { var device = deviceDetector.device; }
  var deviceos = deviceDetector.os;
  var devicebrowser = deviceDetector.browser;
  var Device = { device:device, deviceos:deviceos, devicebrowser:devicebrowser };

  var url = new RegExp(Config.BaseURL+'\/(.*)');
 	var auth = window.location.href.match(url);
  var workshoptitleSlugs = auth[0].substring(auth[0].lastIndexOf('/')+1);

  var sessionKey = WsTitleFactory.randomalphanum();

  var loadstatewithcenters = function() {
    starterspackageFactory.loadstatewithcenters(function(data){
      $scope.statelist = data;
    });
  };
  loadstatewithcenters();

  $scope.findWorkshopSubmit = function(find) {
    $scope.isBusy = true;
    if(find != undefined) {
      find['workshoptitleslugs'] = workshoptitleSlugs;
      WsTitleFactory.findworkshop(find, function(data){
        if(data.workshopResult.length > 0) {
          $scope.hasResult = true;
          $scope.workshopState = data.workshopState;
          $scope.workshopResult = data.workshopResult;
          $scope.noResult = false;

          MapFactory.multipin(data.workshopResult, function(data){
            $scope.randomMarkers = data.markers;
            $scope.map = data.map;
            $scope.options = data.options;
          });

        } else {
          $scope.noResult = true;
          $scope.hasResult = false;
        }
      })
      $scope.isBusy = false;
    } else {
      $scope.isBusy = false;
    }
  }

  $scope.gotoPhase = function(phase) {
    gotoPhase(phase);
  }

  $scope.gotostep2 = function(workshop) {
    $scope.hasResult = false;
    gotoPhase(2); //function

    WsTitleFactory.thisworkshop(workshoptitleSlugs, workshop.workshopid, workshop.workshopstatecode, function(data) {
      if(data.error == true) {
          gotoPhase(1); //function
          toaster.pop({
            type: 'error',
            title: 'Error!',
            body: data.errorMsg,
            showCloseButton: true
          });
      } else {
        $scope.step2workshop = data.workshop[0];
        $scope.thisworkshop = workshop;
        MapFactory.multipin(data.workshop, function(data) {
          $scope.randomMarkers = data.markers;
          $scope.map = data.map;
          $scope.options = data.options;
        });
      }
    });
  };

  $scope.gotostep3 = function(info) {
    if(info.hasOwnProperty('name') && info.hasOwnProperty('contact')) {
      if(info.name == undefined || info.name == "" || info.contact == undefined || info.contact == "") {
        warning(); //function
      } else {
        gotoPhase(3); //function
        $scope.information = info;
        $scope.expimonth = DateFactory.month();
        $scope.expiyear = DateFactory.expiyear_up();
      }
    } else {
      Warning('Please complete the form with your valid information'); //function
    }
  }

  $scope.submitPayment = function(paymentinfo, billinginfo) {
    $scope.isBusy = true;
    if(paymentinfo.hasOwnProperty('ccNumber') && paymentinfo.hasOwnProperty('fname') && paymentinfo.hasOwnProperty('lname')) {
      if(paymentinfo.ccNumber != undefined && paymentinfo.ccNumber != "" && paymentinfo.fname != undefined && paymentinfo.fname != "" && paymentinfo.lname != undefined && paymentinfo.lname != "") {
          if(paymentinfo.hasOwnProperty('ccExpiMonth') && paymentinfo.hasOwnProperty('ccExpiYear')) {
            if(paymentinfo.hasOwnProperty('terms')) {
              console.log($scope.step2workshop);
              WsTitleFactory.submitpayment(sessionKey, Device, $scope.step2workshop, $scope.information, paymentinfo, billinginfo, function(data) {
                  if(data.error == true) {
                      Error(data.errorMsg);
                  } else {
                      if(data.warning == true) {
                        Warning(data.warningMsg); //function
                      } else {
                        Success('Your registration is successfully submitted. Thank you.'); //function
                      }
                      gotoPhase(4); //function
                      $scope.voucher = data.voucher;
                      sessionKey = WsTitleFactory.randomalphanum();
                  }
              });

            } else {
              Warning('Please accept the terms & conditions before you proceed'); //function
            }
          } else {
            Warning('Undefined Card Expiration Date'); //function
          }
      } else {
        Warning('Please complete the form with your valid information'); //function
      }
    } else {
      Warning('Please complete the form with your valid information'); //function
    }
  }

  function Warning(msg) {
    toaster.pop({
      type: 'warning',
      title: 'Warning!',
      body: msg,
      showCloseButton: true
    });
    $scope.isBusy = false;
  }

  function Error(msg) {
    toaster.pop({
      type: 'error',
      title: 'Error!',
      body: msg,
      showCloseButton: true
    });
    $scope.isBusy = false;
  }

  function Success(msg) {
    toaster.pop({
      type: 'success',
      title: 'Success!',
      body: msg,
      showCloseButton: true
    });
    $scope.isBusy = false;
  }

  function gotoPhase(phase) {
    if(phase == 1) {
      $scope.pagetitle = "Find A Center";
      $scope.disPhase2 = true;
      $scope.step1 = true;
      $scope.step2 = false;
      $scope.step3 = false;
      angular.element(".phase1").addClass('active');
      angular.element(".phase2").removeClass('active');
      angular.element(".phase3").removeClass('active');
    } else if(phase == 2) {
      $scope.pagetitle = "Enter Information";
      $scope.disPhase2 = false;
      $scope.step1 = false;
      $scope.step2 = true;
      $scope.step3 = false;
      angular.element(".phase1").removeClass('active');
      angular.element(".phase2").addClass('active');
      angular.element(".phase3").removeClass('active');
    } else if (phase == 3) {
      $scope.pagetitle = "Confirm Your Appointment";
      $scope.disPhase2 = false;
      $scope.step1 = false;
      $scope.step2 = false;
      $scope.step3 = true;
      angular.element(".phase1").removeClass('active');
      angular.element(".phase2").removeClass('active');
      angular.element(".phase3").addClass('active');
    } else if (phase == 4) {
      $scope.pagetitle = "Success";
      $scope.disPhase1 = true;
      $scope.disPhase2 = true;
      $scope.step1 = false;
      $scope.step2 = false;
      $scope.step3 = false;
      $scope.step4 = true;
      angular.element(".phase1").removeClass('active');
      angular.element(".phase2").removeClass('active');
      angular.element(".phase3").addClass('active');
    }
  }


  $scope.dlPdf = function() {
    var pdf = new jsPDF('p', 'pt', 'letter');
    source = $('#voucher')[0];
    specialElementHandlers = {
      '#bypassme': function(element, renderer){
        return true
      }
    }
    margins = {
        top: 50,
        left: 60,
        width: 545
      };
    pdf.fromHTML(
        source // HTML string or DOM elem ref.
        , margins.left // x coord
        , margins.top // y coord
        , {
          'width': margins.width // max width of content on PDF
          , 'elementHandlers': specialElementHandlers
        },
        function (dispose) {
          // dispose: object with X, Y of the last line add to the PDF
          //          this allow the insertion of new lines after html
            pdf.save('voucher.pdf');
        }
    )
  }

});
