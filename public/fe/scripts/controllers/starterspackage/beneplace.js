'use strict';

app.controller("beneplaceCtrl", function ($scope, Config, beneplaceFactory, uiGmapGoogleMapApi, MapFactory, $sce, deviceDetector, $window) {

  beneplaceFactory.viewreferrallink(function(data){
    if (document.referrer != data.link) {
      console.log(data.link);
      $window.location.href = "/";
    }
  })

  $scope.BaseURL = Config.BaseURL;
  $scope.firststep = true;
  var classvalid = false;
  var classtype = '';
  var classitemprice = '';
  var classitemqty = 1;
  var classtotalprice = '';
  var classdate = '';
  var classtime = '';
  var classsession = '';

  var classfullname = '';
  var classemail = '';
  var classphonenumber = '';
  var classcomment = '';

  var centerid = '';
  var centertitle = '';
  var centeraddress = '';
  var centerzip = '';
  var centerstatename = '';
  var centerstatecode = '';
  var centerphone = '';
  var centerlatitude = '';
  var longitude = '';

  var introonlinerate = '';
  var grouponlinerate = '';
  var paypalid = '';
  var authorizeid = '';
  var authorizekey = '';

  var device = '';
  var deviceos = '';
  var devicebrowser = '';

  // uiGmapGoogleMapApi.then(function(maps) {
  //   if( typeof _.contains === 'undefined' ) {
  //     _.contains = _.includes;
  //   }
  //   if( typeof _.object === 'undefined' ) {
  //     _.object = _.zipObject;
  //   }
  // })

  var url = window.location.href;

  if(url.match(/\/getstarted\/starterspackage\/(.*)+/) != null){
    // var slug = url.match(/\/getstarted\/starterspackage\/(.*)+/)[1];
    var slug = url.replace(/%20/g, " ") .split('/');
    var datefromcenter = url.replace(/%20/g, " ") .split('/');
    var timefromcenter = url.replace(/%20/g, " ") .split('/');
    var indexfromcenter = url.replace(/%20/g, " ") .split('/');
    var classfromcenter = url.replace(/%20/g, " ") .split('/');

    if(datefromcenter[6] != '' && datefromcenter[6] != null && datefromcenter[6] != undefined){
      beneplaceFactory.e(function(data){

        introonlinerate = data.introonlinerate;
        grouponlinerate = data.grouponlinerate;

        $scope.introonlinerate = introonlinerate;
        $scope.grouponlinerate = grouponlinerate;

        $scope.pickclasstype('group');



        $scope.summaryselectedschedule = false;
        $scope.classdetailsschedulelist = true
        var newdate = moment(datefromcenter[6]).format('YYYY-MM-DD');
        var newdateordinal = moment(datefromcenter[6]).format('MMMM, Do');

        classdate = newdate;
        classtime = '';

        classdetailsinlinecalendarvalidation(classdate);

        beneplaceFactory.getcenter(slug[5], function(data) {

          var params = {
            'date': newdate,
            'centerid':data.centerid
          };

          beneplaceFactory.getstarterloadscheduleAction(params, function(data) {
            $scope.schedulelistofcenter = data.schedule;
            $scope.returnday = data.day;
            $scope.returndate = newdate;
            $scope.returndayordinal = newdateordinal;

            classtime = timefromcenter[7];
            classdetailsschedulelistvalidation(timefromcenter[7]);
            $scope.selectedschedule = indexfromcenter[8];
            $scope.scopeclasstime = timefromcenter[7];
            $scope.scopeclassandday = classfromcenter[9];
            $scope.summaryselectedschedule = true;

          });

        });




      });

    }


  } else {
    var slug = [];
  }


  var loadprices = function(){
    beneplaceFactory.loadprice(function(data){
      console.log(data);
      introonlinerate = data.introonlinerate;
      grouponlinerate = data.grouponlinerate;

      $scope.introofflinerate = data.introofflinerate;
      $scope.groupofflinerate = data.groupofflinerate;

      $scope.introonlinerate = introonlinerate;
      $scope.grouponlinerate = grouponlinerate;

    });
  };

  loadprices();

  if(deviceDetector.isDesktop() == true){
    device = 'desktop';
  }
  else{
    device = deviceDetector.device;
  }

  deviceos = deviceDetector.os;
  devicebrowser = deviceDetector.browser;


  var loadstatewithcenters = function(){
    beneplaceFactory.loadstatewithcenters(function(data){
      $scope.statelist = data;
    });
  };
  loadstatewithcenters();


  $scope.pickclasstype = function(data){
    classtype = data;
    $scope.paypalclasstype = classtype;
    if(classtype == 'intro'){
      $scope.classtype          = 'one';
      $scope.scopeclasstype     = '1-on-1 Intro Session';
      classsession              = '1-on-1 Intro Session';
      $scope.itemprice          = introonlinerate;
      $scope.paypalpricerate    = introonlinerate;
      $scope.paypalitemquantity = classitemqty;
      classitemprice            = introonlinerate;
      classtotalprice           = (parseInt(classitemprice) * parseInt(classitemqty));
      $scope.classtotalprice    = classtotalprice;
    }
    else if(classtype == 'group'){
      $scope.classtype          = 'group';
      $scope.scopeclasstype     = '1 Group Class + 1-on-1 Intro Session';
      classsession              = '1 Group Class + 1-on-1 Intro Session';
      $scope.itemprice          = grouponlinerate;
      $scope.paypalpricerate    = grouponlinerate;
      $scope.paypalitemquantity = classitemqty;
      classitemprice            = grouponlinerate;
      classtotalprice           = (parseInt(classitemprice) * parseInt(classitemqty));
      $scope.classtotalprice    = classtotalprice;
    }

    if(slug[5] == null) {
      if(classtype != '' || classtype != undefined || classtype != null) {
        $scope.firststep = false;
        $scope.secondstep = true;
      }
    } else {
      beneplaceFactory.getcenter(slug[5], function(data) {
        if(!data.hasOwnProperty('error')){
          $scope.firststep = false;
          $scope.selectedcenter(data.centerid, data.centertitle, data.centeraddress, data.centerzip, data.state, data.centerstate, data.phonenumber, data.lat, data.lon, data.paypalid, data.authorizeid, data.authorizekey);
        } else {
          $scope.firststep = false;
          $scope.secondstep = true;
        }
      });
    }
  };

  $scope.gotofirststep = function(){
    classtype = '';
    $scope.firststep = true;
    $scope.secondstep = false;
    $scope.thirdstep = false;
  };

  $scope.gotosecondstep = function(){
    $scope.firststep = false;
    $scope.secondstep = true;
    $scope.thirdstep = false;
  };

  $scope.gotothirdstep = function(){
    $scope.firststep = false;
    $scope.secondstep = false;
    $scope.thirdstep = true;
    $scope.fourthstep = false;
  };

  $scope.searchlocation = function(locationdata){

    if(locationdata){
      $scope.searchlocationloader = true;

      beneplaceFactory.searchlocation(locationdata, function(data){
        if(data.hasOwnProperty('error')){
          $scope.locationnoresult = true;
          $scope.locationresult = false;
        }
        else{

          $scope.centerlist = data;
          $scope.locationresult = true;
          $scope.locationnoresult = false;

          MapFactory.multipin(data, function(data) {
            console.log(data);
            $scope.randomMarkers = data.markers;
            $scope.map = data.map;
            $scope.options = data.options;
          });

        }

        $scope.searchlocationloader = false;
      });
    }
  };

  $scope.selectedcenter = function(centerlistcenterid, centerlistcentertitle, centerlistcenteraddress, centerlistcenterzip, centerliststate, centerlistcenterstate, centerlistphonenumber, centerlistlat, centerlistlon, centerlistpaypalid, centerlistauthorizeid, centerlistauthorizekey){
    centerid = centerlistcenterid;
    centertitle = centerlistcentertitle;
    centeraddress = centerlistcenteraddress;
    centerzip = centerlistcenterzip;
    centerstatename = centerliststate;
    centerstatecode = centerlistcenterstate;
    centerphone = centerlistphonenumber;
    centerlatitude = centerlistlat;
    longitude = centerlistlon;
    paypalid = centerlistpaypalid;
    authorizeid = centerlistauthorizeid;
    authorizekey = centerlistauthorizekey;

    $scope.scopepaypalid = paypalid;
    $scope.scopecentertitle = centertitle;
    $scope.scopecenteraddress = centeraddress;
    $scope.scopecenterstatename = centerstatename;
    $scope.scopecenterphone = centerphone;
    $scope.scopecenterzip = centerzip;

    var centerselected = [];
    centerselected.push({
      'centerid' : centerid,
      'centertitle': centertitle,
      'centeraddress': centeraddress,
      'centerzip': centerzip,
      'lat': centerlatitude,
      'lon': longitude
    });

    MapFactory.singlepin(centerselected, function(data) {
      $scope.singlecenterrandomMarkers = data.markers;
      $scope.singlecentermap = data.map;
      $scope.options = data.options;
    });


    $scope.secondstep = false;
    $scope.thirdstep = true;

    $scope.classtype = classtype;
  };

  var classemailvalid = false;
  var classfullnamevalid = false;
  var classphonevalid = false;
  var classdatevalid = false;
  var classtimevalid = false
  var classinlinecalendaralid = false
  var classschedulelistvalid = false


  $scope.submitclassdetails = function(classdetailsdata){
    console.log(classdetailsdata);
    classfullname = classdetailsdata.fullname;
    classemail = classdetailsdata.email;
    classphonenumber = classdetailsdata.phonenumber;
    classcomment = classdetailsdata.classcomment;

    if(classtype == 'intro'){
      classdetailsfullnamevalidation(classdetailsdata.fullname);
      classdetailsemailvalidation(classdetailsdata.email);
      classdetailsphonevalidation(classdetailsdata.phonenumber);
      classdetailsdatevalidation(classdetailsdata.classdate);
      classdetailstimevalidation(classdetailsdata.classtime);
      if(classemailvalid && classfullnamevalid && classphonevalid && classdatevalid && classtimevalid){
        $scope.thirdstep = false;
        $scope.fourthstep = true;
        classdate = moment(classdetailsdata.classdate).format('YYYY-MM-DD');;
        classtime = classdetailsdata.classtime;
      }
    }
    else if(classtype == 'group'){
      classdetailsfullnamevalidation(classdetailsdata.fullname);
      classdetailsemailvalidation(classdetailsdata.email);
      classdetailsphonevalidation(classdetailsdata.phonenumber);
      classdetailsinlinecalendarvalidation(classdate);
      classdetailsschedulelistvalidation(classtime);
      if(classemailvalid && classfullnamevalid && classdetailsphonevalidation && classinlinecalendaralid && classschedulelistvalid){
        $scope.thirdstep = false;
        $scope.fourthstep = true;
      }
    }

  };

  $scope.classselecteddate = function(datedata){
    console.log(datedata);
    $scope.summaryselectedschedule = false;
    $scope.classdetailsschedulelist = true
    var newdate = moment(datedata).format('YYYY-MM-DD');
    var newdateordinal = moment(datedata).format('MMMM, Do');

    classdate = newdate;
    classtime = '';

    classdetailsinlinecalendarvalidation(classdate);

    var params = {
      'date': newdate,
      'centerid':centerid
    };

    beneplaceFactory.getstarterloadscheduleAction(params, function(data) {
      $scope.schedulelistofcenter = data.schedule;
      $scope.returnday = data.day;
      $scope.returndate = newdate;
      $scope.returndayordinal = newdateordinal;
    });

  };

  $scope.selectschedule = function(listindex, classtimedata, classandday){
    classtime = classtimedata;
    classdetailsschedulelistvalidation(classtime);
    $scope.selectedschedule = listindex;
    $scope.scopeclasstime = classtimedata;
    $scope.scopeclassandday = classandday;
    $scope.summaryselectedschedule = true;

  };

  // classchange validation
  $scope.classdetailsfullnamechange = function(data){
    classdetailsfullnamevalidation(data);
  };

  $scope.classdetailsemailchange = function(data){
    classdetailsemailvalidation(data);
  };

  $scope.classdetailsphonenumberchange = function(data){
    classdetailsphonevalidation(data);
  };

  $scope.classdetailsclassdatechange = function(data){
    classdetailsdatevalidation(data);
  };

  $scope.classdetailsclasstimechange = function(data){
    classdetailstimevalidation(data);
  };

  $scope.classdetailsclasstimechange = function(data){
    classdetailstimevalidation(data);
  };
  $scope.classdetailsitemqtychange = function(data){
    classdetailsitemqtycalculate(data);
  };

  // end of classchange validation

  // class details validation
  var classdetailsemailvalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classemailvalid = false;
      $scope.classdetailsemailvalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Email is required!');
    }
    else{
      classemailvalid = true;
      $scope.classdetailsemailvalidation = '';
    }
  };

  var classdetailsfullnamevalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classfullnamevalid = false;
      $scope.classdetailsfullnamevalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Name is required!');
    }
    else{
      classfullnamevalid = true;
      $scope.classdetailsfullnamevalidation = '';
    }
  };

  var classdetailsphonevalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classphonevalid = false;
      $scope.classdetailsphonevalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Phonenumber is required!');
    }
    else{
      if(data.length < 12){
        classphonevalid = false;
        $scope.classdetailsphonevalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> please complete phone number 10 digits!');
      }
      else{
      classphonevalid = true;
      $scope.classdetailsphonevalidation = '';
      }
    }
  };

  var classdetailsdatevalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classdatevalid = false;
      $scope.classdetailsdatevalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Date is required!');
    }
    else{
      classdatevalid = true;
      $scope.classdetailsdatevalidation = '';
    }
  };

  var classdetailstimevalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classtimevalid = false;
      $scope.classdetailstimevalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Time is required!');
    }
    else{
      classtimevalid = true;
      $scope.classdetailstimevalidation = '';
    }
  };

  var classdetailsinlinecalendarvalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classinlinecalendaralid = false;
      $scope.classdetailsinlinecalendarvalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Date is required!');
    }
    else{
      classinlinecalendaralid = true;
      $scope.classdetailsinlinecalendarvalidation = '';
    }
  };

  var classdetailsschedulelistvalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classschedulelistvalid = false;
      $scope.classdetailsschedulelistvalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Class schedule is required!');
    }
    else{
      classschedulelistvalid = true;
      $scope.classdetailsschedulelistvalidation = '';
    }
  };

  var classdetailsitemqtycalculate = function(data){
    classitemqty = data;
    $scope.paypalitemquantity = classitemqty;
    classtotalprice = (parseInt(classitemprice) * parseInt(data));
    $scope.classtotalprice = classtotalprice;
  };
  // end of class details validation

  var classcardnumbervalid = false;
  var classfirstnamevalid = false;
  var classlastnamevalid = false;
  var classexpirationmonthvalid = false;
  var classexpirationyearvalid = false;
  var classfullnamevalid = false;
  var classaddress1valid = false;
  var classcityvalid = false;
  var classstatevalid = false;
  var classphonenumbervalid = false;
  var classzipvalid = false;
  var classtermsvalid = false;


  $scope.submitbillinginfo = function(billingdata){
    $scope.billbuttonload = true;
    $scope.billbtndisabled = true;
    tryaclasspaymentformcardnumbervalidation(billingdata.cardnumber);
    tryaclasspaymentformfirstnamevalidation(billingdata.firstname);
    tryaclasspaymentformlastnamevalidation(billingdata.lastname);
    tryaclasspaymentformexpirationmonthvalidation(billingdata.expirationmonth);
    tryaclasspaymentformexpirationyearvalidation(billingdata.expirationyear);
    tryaclasspaymentformfullnamevalidation(billingdata.fullname);
    tryaclasspaymentformaddress1validation(billingdata.address1);
    tryaclasspaymentformcityvalidation(billingdata.city);
    tryaclasspaymentformstatevalidation(billingdata.state);
    tryaclasspaymentformphonenumbervalidation(billingdata.phonenumber);
    tryaclasspaymentformzipvalidation(billingdata.zip);
    tryaclasspaymentformtermsvalidation(billingdata.terms);

    if(classcardnumbervalid && classfirstnamevalid && classlastnamevalid && classexpirationmonthvalid && classexpirationyearvalid && classfullnamevalid && classaddress1valid && classcityvalid && classstatevalid && classphonenumbervalid && classzipvalid && classtermsvalid){
        var randomfilename = randomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

        var ccpaymentparams = {
          'authorizeid':authorizeid,
          'authorizekey': authorizekey,
          'cardnumber':billingdata.cardnumber,
          'expimonth':billingdata.expirationmonth,
          'expiyear':billingdata.expirationyear,
          'billfirstname':billingdata.firstname,
          'billlastname':billingdata.lastname,
          'billadd1':billingdata.address1,
          'billstate':billingdata.state,
          'billzip':billingdata.zip,
          'centerid':centerid,
          'temptransactionid': "cc-"+randomfilename,
          'name':classfullname,
          'email':classemail,
          'phone':classphonenumber,
          'day':classdate,
          'hour':classtime,
          'comment':classcomment,
          'qty':classitemqty,
          'device':device,
          'deviceos':deviceos,
          'devicebrowser':devicebrowser,
          'payment_amount':classtotalprice,
          'classtype':'beneplace',
          'beneplaceclasstype':classtype,
          'classsession': classsession
        };

        beneplaceFactory.authorize(ccpaymentparams,function(data){
          console.log(data);

          if(data.hasOwnProperty('success')){
            window.location = '/payment/transactions/success?tx='+data.msg+'&cm=beneplace';
          }
          else if(data.hasOwnProperty('error')){
            if(data.error == 'savingerror'){
              $scope.billbuttonload = false;
              $scope.billbtndisabled = true;
              $scope.tryaclasspaymentformsummaryerror = $sce.trustAsHtml('<i class="fa fa-close"></i> Transaction is completed but Class session is not verified please contact our customer Service.. Server error');
            }
            else if(data.error == 'authorizeerror'){
              console.log(data.mgs[0]);
              if(data.mgs[0] == 3 && data.mgs[1] == 1 && data.mgs[2] == 6){
                $scope.tryaclasspaymentformcardnumbervalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> '+data.mgs[3]);
                $scope.tryaclasspaymentformsummaryerror = $sce.trustAsHtml('<i class="fa fa-close"></i> Something went wrong please check your fields and try again');
                $scope.billbuttonload = false;
                $scope.billbtndisabled = false;
              }
              else if(data.mgs[0] == 3 && data.mgs[1] == 1 && data.mgs[2] == 8){
                $scope.tryaclasspaymentformexpirationyearvalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> '+data.mgs[3]);
                $scope.tryaclasspaymentformsummaryerror = $sce.trustAsHtml('<i class="fa fa-close"></i> Something went wrong please check your fields and try again');
                $scope.billbuttonload = false;
                $scope.billbtndisabled = false;
              }
              else{
                $scope.tryaclasspaymentformsummaryerror = $sce.trustAsHtml('<i class="fa fa-close"></i> Something went wrong please check your fields and try again or press submit button');
                $scope.billbuttonload = false;
                $scope.billbtndisabled = false;
              }
            }
          }
          else{
            $scope.tryaclasspaymentformsummaryerror = $sce.trustAsHtml('<i class="fa fa-close"></i> '+data.mgs);
            $scope.billbuttonload = false;
            $scope.billbtndisabled = false;
          }

        });
    }
    else{
      $scope.billbuttonload = false;
      $scope.billbtndisabled = false;
    }

  };

  // billing validation
  $scope.tryaclasspaymentformcardnumberchange = function(data){
    tryaclasspaymentformcardnumbervalidation(data);
  };

  $scope.tryaclasspaymentformfirstnamechange = function(data){
    tryaclasspaymentformfirstnamevalidation(data);
  };

  $scope.tryaclasspaymentformlastnamechange = function(data){
    tryaclasspaymentformlastnamevalidation(data);
  };

  $scope.tryaclasspaymentformexpirationmonthchange = function(data){
    tryaclasspaymentformexpirationmonthvalidation(data);
  };

  $scope.tryaclasspaymentformexpirationyearchange = function(data){
    tryaclasspaymentformexpirationyearvalidation(data);
  };

  $scope.tryaclasspaymentformfullnamechange = function(data){
    tryaclasspaymentformfullnamevalidation(data);
  };

  $scope.tryaclasspaymentformaddress1change = function(data){
    tryaclasspaymentformaddress1validation(data);
  };

  $scope.tryaclasspaymentformcitychange = function(data){
    tryaclasspaymentformcityvalidation(data);
  };

  $scope.tryaclasspaymentformstatechange = function(data){
    tryaclasspaymentformstatevalidation(data);
  };

  $scope.tryaclasspaymentformphonenumberchange = function(data){
    tryaclasspaymentformphonenumbervalidation(data);
  };

  $scope.tryaclasspaymentformzipchange = function(data){
    tryaclasspaymentformzipvalidation(data);
  };

  $scope.tryaclasspaymentformtermschange = function(data){
    tryaclasspaymentformtermsvalidation(data);
  };
  // end of billing validation

  // billing validations
  var tryaclasspaymentformcardnumbervalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classcardnumbervalid = false;
      $scope.tryaclasspaymentformcardnumbervalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Card number is required!');
    }
    else{
      classcardnumbervalid = true;
      $scope.tryaclasspaymentformcardnumbervalidation = '';
    }
  };

  var tryaclasspaymentformfirstnamevalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classfirstnamevalid = false;
      $scope.tryaclasspaymentformfirstnamevalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> First Name is required!');
    }
    else{
      classfirstnamevalid = true;
      $scope.tryaclasspaymentformfirstnamevalidation = '';
    }
  };

  var tryaclasspaymentformlastnamevalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classlastnamevalid = false;
      $scope.tryaclasspaymentformlastnamevalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Last Name is required!');
    }
    else{
      classlastnamevalid = true;
      $scope.tryaclasspaymentformlastnamevalidation = '';
    }
  };

  var tryaclasspaymentformexpirationmonthvalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classexpirationmonthvalid = false;
      $scope.tryaclasspaymentformexpirationmonthvalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Card expiration month is required!');
    }
    else{
      classexpirationmonthvalid = true;
      $scope.tryaclasspaymentformexpirationmonthvalidation = '';
    }
  };

  var tryaclasspaymentformexpirationyearvalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classexpirationyearvalid = false;
      $scope.tryaclasspaymentformexpirationyearvalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Card expiration year is required!');
    }
    else{
      classexpirationyearvalid = true;
      $scope.tryaclasspaymentformexpirationyearvalidation = '';
    }
  };

  var tryaclasspaymentformfullnamevalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classfullnamevalid = false;
      $scope.tryaclasspaymentformfullnamevalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Full name is required!');
    }
    else{
      classfullnamevalid = true;
      $scope.tryaclasspaymentformfullnamevalidation = '';
    }
  };

  var tryaclasspaymentformaddress1validation = function(data){
    if(data == '' || data == null || data == undefined){
      classaddress1valid = false;
      $scope.tryaclasspaymentformaddress1validation = $sce.trustAsHtml('<i class="fa fa-close"></i> Address Line 1 is required!');
    }
    else{
      classaddress1valid = true;
      $scope.tryaclasspaymentformaddress1validation = '';
    }
  };

  var tryaclasspaymentformcityvalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classcityvalid = false;
      $scope.tryaclasspaymentformcityvalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> City is required!');
    }
    else{
      classcityvalid = true;
      $scope.tryaclasspaymentformcityvalidation = '';
    }
  };

  var tryaclasspaymentformstatevalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classstatevalid = false;
      $scope.tryaclasspaymentformstatevalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> City is required!');
    }
    else{
      classstatevalid = true;
      $scope.tryaclasspaymentformstatevalidation = '';
    }
  };

  var tryaclasspaymentformphonenumbervalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classphonenumbervalid = false;
      $scope.tryaclasspaymentformphonenumbervalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Phone number is required!');
    }
    else{

      if(data.length < 12){
        classphonenumbervalid = false;
        $scope.tryaclasspaymentformphonenumbervalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> please complete phone number 10 digits!');
      }
      else{
        classphonenumbervalid = true;
        $scope.tryaclasspaymentformphonenumbervalidation = '';
      }

    }
  };

  var tryaclasspaymentformzipvalidation = function(data){
    if(data == '' || data == null || data == undefined){
      classzipvalid = false;
      $scope.tryaclasspaymentformzipvalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> Zip is required!');
    }
    else{
      classzipvalid = true;
      $scope.tryaclasspaymentformzipvalidation = '';
    }
  };

  var tryaclasspaymentformtermsvalidation = function(data){
    if(data == false){
      classtermsvalid = false;
      $scope.tryaclasspaymentformtermsvalidation = $sce.trustAsHtml('<i class="fa fa-close"></i> You need to agree the terms and conditions!');
    }
    else{
      classtermsvalid = true;
      $scope.tryaclasspaymentformtermsvalidation = '';
    }
  };
  // end of billing validation

  function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
      return result;
  }


  $scope.processpaypal = function(){
    $scope.paypalprocess = true;
    var randomfilename = randomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    $scope.transactionid = "paypal-"+randomfilename;

    var classtempparams = {
      'centerid':centerid,
      'transactionid': "paypal-"+randomfilename,
      'name':classfullname,
      'email':classemail,
      'phone':classphonenumber,
      'day':classdate,
      'hour':classtime,
      'comment':classcomment,
      'qty':classitemqty,
      'device':device,
      'deviceos':deviceos,
      'devicebrowser':devicebrowser,
      'payment_amount':classtotalprice,
      'classtype':'beneplace',
      'classtypebeneplace':classtype
    };

    beneplaceFactory.savetemppaypaltransactionAction(classtempparams, function(data) {
      if(data.hasOwnProperty('success')){
        $('.paypalform').submit();
      }
      else if(data.hasOwnProperty('error')){
        $scope.paypalprocess = false;
        $scope.paypalprocesserror = 'Something went wrong please try again later!';
      }
    });
  };


  $scope.today = function () {
    $scope.dt = new Date();
  };

  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;

  };

  $scope.toggleMin = function () {
    $scope.minDate = $scope.minDate ? null : new Date();
  };

  $scope.toggleMin();

  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    class: 'datepicker',
    showWeeks:false
  };

  $scope.showWeeks = false;

  $scope.initDate = new Date('2016-15-20');
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];

  angular.element(document).ready(function(){
    angular.element('.tryaclassbtn').removeClass('hidden');
    angular.element('.secondstep').removeClass('hidden');
    angular.element('.secondstep').removeClass('hidden');
    angular.element('.thirdstep').removeClass('hidden');
    angular.element('.fourthstep').removeClass('hidden');
  });

  $scope.onzip = function(zip) {
    if(zip){
      beneplaceFactory.searchzip(zip, function(data) {
        if(data.length > 0){
          $scope.centerlist = data;
          $scope.locationresult = true;
          $scope.locationnoresult = false;

          MapFactory.multipin(data, function(data) {
            $scope.randomMarkers = data.markers;
            $scope.map = data.map;
            $scope.options = data.options;
          });
        } else {
          $scope.centerlist = [];
          $scope.locationresult = false;
          $scope.locationnoresult = true;
        }
      });
    } else {
      $scope.centerlist = [];
      $scope.locationresult = false;
      $scope.locationnoresult = false;
    }
  }

})
