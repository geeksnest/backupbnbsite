app.controller("pgPrivacyCtrl", function($scope, $location, $anchorScroll) {
  $scope.scrollTo = function(number) {
    $location.hash(number);
    $anchorScroll();
  };
});
