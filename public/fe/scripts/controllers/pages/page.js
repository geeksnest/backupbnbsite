app.controller("pageCtrl", function ($scope, Config, pageFactory, $sce) { 'use strict';
    var pageslugs = '';

    $(function() {
      $('.item').matchHeight(
      {
        byRow: true,
        property: 'height',
        target: null,
        remove: false
      });
    });

    $('.pagebanner').imagefill({throttle:1000/60});
});
