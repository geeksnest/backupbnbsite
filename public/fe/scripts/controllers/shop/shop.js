'use strict';

app.controller("ShopCtrl", function ($scope, Config, Shop, store, $window, jwtHelper, $timeout) {
    $scope.selected = { filter : null };
    $scope.amazonlink = Config.amazonlink;
    $scope.searchinp = false;
    $scope.active = {};
    var getdata = function(selected) {
        Shop.get(selected, function(data) {
            $scope.featured = data.featured;
            $scope.products = data.products;
            console.log(data);
        });
    }

    getdata($scope.selected);

    $scope.filter = function(data, event) {
        if(data.filter == 'category'){
            $scope.active = { category : data.category };
        } else if(data.filter == 'subcategory'){
            $scope.active = { category : data.category, subcategory : data.subcategory };
        } else if(data.filter == 'type'){
            $scope.active = { category : data.category, subcategory : data.subcategory, type : data.type };
            $timeout(function() {
                $('.focus').removeClass('focus');
            }, 500);
        } else {
            $scope.active = {};
        }
    	getdata(data);
    }

    $scope.search = function() {
        if($scope.searchinp){
            $scope.searchinp = false;
        } else {
            $scope.searchinp = true;
        }
    }

    $(document).click(function() {
        $('.focus').removeClass('focus');
    });
})
