'use strict';

app.controller("BagCtrl", function($scope, Config, Shop, Login, $window, jwtHelper, store, toaster, $q) {
    var taxpercentage = 0.05;
    $scope.amazonlink = Config.amazonlink;
    $scope.shippingfee = 2;
    $scope.total = 0;
    $scope.tax = 0;

    var generatetotal = function() {
        angular.forEach($scope.bag, function(value, key) {
            $scope.bag[key].avquantity = [];
            var x = 0;
            while(x < $scope.bag[key].maxquantity){
                if(x >= $scope.bag[key].minquantity){
                    x++;
                }else {
                    x = $scope.bag[key].minquantity;
                }
                $scope.bag[key].avquantity.push(x);
            }

            var discount_from = new Date(value['discount_from']).getTime();
            var discount_to = new Date(value['discount_to']).getTime();
            var now = new Date(new Date().toJSON().slice(0,10)).getTime();
            if(discount_from <= now && now <= discount_to){
                $scope.bag[key].disc = true;
                var total =  value['price'] - ((value['discount'] / 100) * value['price']);
                $scope.bag[key].total = total * value['bagquantity'];
            }else {
                $scope.bag[key].disc = false;
                $scope.bag[key].total = value['price'] * value['bagquantity'];
            }
            $scope.total = $scope.total + $scope.bag[key].total;
        });
        $scope.tax = $scope.total * taxpercentage;
        $scope.total = $scope.total + $scope.tax + $scope.shippingfee;
    };
    
    if(Login.getCurrentUser().id != undefined){
        Shop.getmemberbag(Login.getCurrentUser().id, function(data) {
            $scope.bag = data.bag;
            generatetotal();
        });
    }else {
        Shop.getbag({bag : store.get('bag')}, function(data) {
            console.log(data);
            $scope.bag = data;
            generatetotal();
        });
    }

    $scope.remove = function(id) {
        if(Login.getCurrentUser().id != undefined){
            Shop.removefrombag(id, function(data) {
                if(data.hasOwnProperty('success')){
                    angular.forEach($scope.bag, function(value, key) {
                        if(value['id'] == id){
                            $scope.bag.splice(key, 1);
                        }
                    });
                    store.set('bag', $scope.bag);
                }
            });
        }else {
            angular.forEach($scope.bag, function(value, key) {
                if(value['id'] == id){
                    $scope.bag.splice(key, 1);
                }
            });
        }
    }

    $scope.onqty = function(product) {
        if(Login.getCurrentUser().id != undefined){
            Shop.updateQuantity(product.id, product.bagquantity, function(data) {
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', '', data.success);
                    generatetotal();
                }else {
                    toaster.pop('error', '', data.error);
                }
            });
        } else {
            store.set('bag', $scope.bag);
            generatetotal();
        }
    }

    $scope.$watch(function() {
        if($scope.bag == null || $scope.bag == undefined || $scope.bag.length == 0){
            return 0;
        }else {
            return $scope.bag.length;
        }
    }, function() {
        if($scope.bag == null){
            $scope.bagcount = 0;
            store.set('bag', []);
        }else {
            $scope.bagcount = $scope.bag.length;
            store.set('bag', $scope.bag);
        }
    });

    $(window).load(function() {
        $("#main-container").css('min-height', (screen.height - 80)+'px');
        // $("#main-container").css('min-height', (screen.height - 155)+'px');
    });
})

