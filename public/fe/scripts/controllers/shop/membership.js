'use strict';

app.controller("shopMembershipCtrl", function($scope, Config, Login, $window, store, toaster, $q, Shop) {

  var keyword = undefined;

  $scope.openSearch = function(keyword) {
    if(keyword == ""){
      keyword = undefined;
    }
    Shop.getevents(keyword, function(data) {
      $scope.events = data;
    });
  }

})
