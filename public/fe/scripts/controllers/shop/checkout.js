'use strict';

app.controller("CheckoutCtrl", function($scope, Config, Shop, Member, Login, $window, jwtHelper, store, Facebook, GooglePlus, twitterService, rfc4122, toaster, $timeout, $modal) {
    $scope.register = false;
    $scope.member = {};
    $scope.error = {};
    $scope.alerts = [];
    $scope.ccinfo = true;
    $scope.verification = false;
    var uniqid = rfc4122.v4();
    var taxpercentage = 0.05;
    $scope.invoiceno = parseInt(uniqid.substr(0, 8), 16) * 10;
    $scope.months =  Shop.month();
    $scope.years =  Shop.year();
    $scope.shippinginfo = {};
    $scope.creditcard = {};
    $scope.total = 0;
    $scope.paymenttype='paypal';
    $scope.shippingfee = 2;
    $scope.total = 0;
    $scope.tax = 0;
    $scope.amazonlink = Config.amazonlink;
    $scope.BaseURL = Config.BaseURL;
    $scope.billinginfo = {};
    $scope.checkout = {};

    var getmemberbag = function() {
        Shop.getmemberbag(Login.getCurrentUser().id, function(data) {
            $scope.bag = data.bag;
            $scope.billinginfo = data.billinginfo;
            generatetotal();
        });
    }

    if(Login.getCurrentUser().id != undefined){
        $scope.member = Login.getCurrentUser();
        getmemberbag();    
    }

    $scope.closeAlert = function() {    
        $scope.alerts.splice(0,1);
    }

    var generatetotal = function() {
        $scope.bag = store.get('bag');
        angular.forEach($scope.bag, function(value, key) {
            $scope.bag[key].avquantity = [];
            var x = 0;
            while(x < $scope.bag[key].maxquantity){
                if(x >= $scope.bag[key].minquantity){
                    x++;
                }else {
                    x = $scope.bag[key].minquantity;
                }
                $scope.bag[key].avquantity.push(x);
            }

            var discount_from = new Date(value['discount_from']).getTime();
            var discount_to = new Date(value['discount_to']).getTime();
            var now = new Date(new Date().toJSON().slice(0,10)).getTime();
            if(discount_from <= now && now <= discount_to){
                $scope.bag[key].disc = true;
                var total =  value['price'] - ((value['discount'] / 100) * value['price']);
                $scope.bag[key].total = total * value['bagquantity'];
            }else {
                $scope.bag[key].disc = false;
                $scope.bag[key].total = value['price'] * value['bagquantity'];
            }
            $scope.total = $scope.total + $scope.bag[key].total;
        });
        $scope.tax = $scope.total * taxpercentage;
        $scope.total = $scope.total + $scope.tax + $scope.shippingfee;
    };

    //register
    $scope.reg = function(member) {
        if($scope.register == true) {
            $scope.onemail(member.email);
            $scope.onpass(member);
            $scope.onname(member.firstname, 'fname');
            $scope.onname(member.lastname, 'lname');
            $scope.onname(member.phoneno, 'phoneno');
            if($scope.error.email == undefined &&
               $scope.error.firstname == undefined &&
               $scope.error.lastname == undefined &&
               $scope.error.password == undefined &&
               $scope.error.confpass == undefined &&
               $scope.error.phoneno == undefined ){
                Member.register(member, function(data) {
                    store.set('jwt', data.success);
                    $scope.member = Login.getCurrentUser();
                    addmultipletocart(store.get('bag'), Login.getCurrentUser().id);
                });
            }
        }else {
            $scope.closeAlert();
            $scope.register = true;
            $scope.member = {};
        }
    }

    $scope.registerfalse = function() {
        $scope.register = false;
        $scope.member = {};
        $scope.error = {};
        $scope.alerts = [];
    }

    $scope.onemail = function(email) {
        if($scope.register){
            if(email != undefined){
                Member.validateEmail(email, function(data) {
                    if(data.exist==false){
                        $scope.error.email = undefined;
                    }else {
                        $scope.error.email = "Email address already exist";
                    }
                });
            }else {
                $scope.error.email = "Email address is required";
            }
        }
    }

    $scope.onpass = function(member){
        if(!member.password){
            $scope.error.password = "Password is required";
            return false;
        }else {
            $scope.error.password = undefined;
            if(!member.confpass){
                $scope.error.confpass = "Please confirm your password"
            }else {
                if(member.password != member.confpass){
                    $scope.error.confpass = "Password did not match!";
                    return false;
                }else if(member.password === member.confpass){
                    $scope.error.confpass = undefined;
                    return true;
                }
            }
        }
    }

    $scope.onname = function(name, type) {
        if(type == 'fname'){
            if(name){
                $scope.error.firstname = undefined;
            }else {
                $scope.error.firstname = "First name is required";
            }
        } else if(type == 'phoneno') {
            if(name){
                $scope.error.phoneno = undefined;
            }else {
                $scope.error.phoneno = "Phone number is required";
            }
        } else {
            if(name){
                $scope.error.lastname = undefined;
            }else {
                $scope.error.lastname = "Last name is required";
            }
        }
    }

    //login
    $scope.signin = function(member) {
        Member.login(member, function(data) {
            if(data.hasOwnProperty('success')){
                store.set('jwt', data.success);
                $scope.member = Login.getCurrentUser();
                addmultipletocart(store.get('bag'), Login.getCurrentUser().id);
                $scope.closeAlert();
            }else {
                $scope.closeAlert();
                $scope.alerts.push({ type : "danger", msg : data.error});
            }
        });
    }

    var addmultipletocart = function(bag, id) {
        Shop.addmultipletocart({bag : bag }, id, function(data) {
            getmemberbag();
        });
    }

    // social media login
    // facebook
    var getfbdata = function() {
        Facebook.getLoginStatus(function(response) {
            console.log(response);
            if(response.status == 'connected'){
                Facebook.api('/me', {fields: 'last_name, email, first_name, picture, hometown'}, function(data) {
                    Member.loginFB(data, function(res) {
                        store.set('jwt', res.success);
                        $scope.member = Login.getCurrentUser();
                        addmultipletocart(store.get('bag'), Login.getCurrentUser().id);
                    });
                });
            }
        });
    }

    $scope.loginFb = function() {
        Facebook.login(function(res) {
            getfbdata();
        });
    }

    // google+
    $scope.logingplus = function () {
        GooglePlus.login().then(function (authResult) {
            GooglePlus.getUser().then(function (user) {
                Member.logingplus(user, function(res) {
                    store.set('jwt', res.success);
                    $scope.member = Login.getCurrentUser();
                    addmultipletocart(store.get('bag'), Login.getCurrentUser().id);
                });
            });
        }, function (err) {
            console.log(err);
        });
    };

    // twitter
    $scope.logintwitter = function() {
        twitterService.connectTwitter().then(function() {
            if (twitterService.isReady()) {

            } else {

            }
        });
    }

    var billinginfoCtrl = function($scope, $modalInstance, id) {
        $scope.billingaddr = {};
        $scope.error = {};

        $scope.change = function(data, field) {
            switch(field) {
                case 'fname' :
                    if(data == undefined){
                        $scope.error.firstname = true;
                    } else {
                        $scope.error.firstname = false;
                    }
                    break;
                case 'lname' :
                    if(data == undefined){
                        $scope.error.lastname = true;
                    } else {
                        $scope.error.lastname = false;
                    }
                    break;
                case 'address' :
                    if(data == undefined){
                        $scope.error.address = true;
                    } else {
                        $scope.error.address = false;
                    }
                    break;
                case 'city' :
                    if(data == undefined){
                        $scope.error.city = true;
                    } else {
                        $scope.error.city = false;
                    }
                    break;
                case 'state' :
                    if(data == undefined){
                        $scope.error.state = true;
                    } else {
                        $scope.error.state = false;
                    }
                    break;
                case 'country' :
                    if(data == undefined){
                        $scope.error.country = true;
                    } else {
                        $scope.error.country = false;
                    }
                    break;
            }
        }

        $scope.submit = function(billing){
            $scope.change(billing.firstname, 'fname');
            $scope.change(billing.lastname, 'lname');
            $scope.change(billing.address, 'address');
            $scope.change(billing.city, 'city');
            $scope.change(billing.country, 'country');
            $scope.change(billing.state, 'state');

            if($scope.error.firstname == false &&
                $scope.error.lastname == false &&
                $scope.error.address == false &&
                $scope.error.city == false &&
                $scope.error.state == false &&
                $scope.error.country == false){
                Shop.savebillinginfo(billing, id, function(data) {
                    if(data.hasOwnProperty('success')){
                        $modalInstance.close(billing);
                    }
                });
            }
        }
    }

    $scope.change = function(data, field) {

        switch(field){
            case 'name' :
                if(data == undefined){
                    $scope.checkout.name = true;
                }else {
                    $scope.checkout.name = false;
                }
                break;
            case 'ccn' :
                if(data == undefined){
                    $scope.checkout.ccn = true;
                }else {
                    $scope.checkout.ccn = false;
                }
                break;
            case 'ccvn' :
                if(data == undefined){
                    $scope.checkout.ccvn = true;
                }else {
                    $scope.checkout.ccvn = false;
                }
                break;
            case 'expiremonth' :
                if(data == undefined){
                    $scope.checkout.expiremonth = true;
                }else {
                    $scope.checkout.expiremonth = false;
                }
                break;
            case 'expireyear' :
                if(data == undefined){
                    $scope.checkout.expireyear = true;
                }else {
                    $scope.checkout.expireyear = false;
                }
                break;
            case 'firstname' :
                if(data == undefined){
                    $scope.checkout.firstname = true;
                }else {
                    $scope.checkout.firstname = false;
                }                break;
            case 'lastname' :
                if(data == undefined){
                    $scope.checkout.lastname = true;
                }else {
                    $scope.checkout.lastname = false;
                }
                break;
            case 'address' :
                if(data == undefined){
                    $scope.checkout.address = true;
                }else {
                    $scope.checkout.address = false;
                }
                break;
            case 'city' :
                if(data == undefined){
                    $scope.checkout.city = true;
                }else {
                    $scope.checkout.city = false;
                }
                break;
            case 'country' :
                if(data == undefined){
                    $scope.checkout.country = true;
                }else {
                    $scope.checkout.country = false;
                }
                break;
            case 'state' :
                if(data == undefined){
                    $scope.checkout.state = true;
                }else {
                    $scope.checkout.state = false;
                }
                break;
            case 'zipcode' :
                if(data == undefined){
                    $scope.checkout.zipcode = true;
                }else {
                    $scope.checkout.zipcode = false;
                }
                break;
        }
    }

    //checkout
    $scope.next = function() {
        $scope.change($scope.creditcard.name, 'name');
        $scope.change($scope.creditcard.ccn, 'ccn');
        $scope.change($scope.creditcard.ccvn, 'ccvn');
        $scope.change($scope.creditcard.expiremonth, 'expiremonth');
        $scope.change($scope.creditcard.expireyear, 'expireyear');
        $scope.change($scope.shippinginfo.firstname, 'firstname');
        $scope.change($scope.shippinginfo.lastname, 'lastname');
        $scope.change($scope.shippinginfo.address, 'address');
        $scope.change($scope.shippinginfo.city, 'city');
        $scope.change($scope.shippinginfo.country, 'country');
        $scope.change($scope.shippinginfo.state, 'state');
        $scope.change($scope.shippinginfo.zipcode, 'zipcode');

        if($scope.checkout.name == false &&
            $scope.checkout.ccn == false &&
            $scope.checkout.ccvn == false &&
            $scope.checkout.expiremonth == false &&
            $scope.checkout.expireyear == false &&
            $scope.checkout.firstname == false &&
            $scope.checkout.lastname == false &&
            $scope.checkout.address == false &&
            $scope.checkout.city == false &&
            $scope.checkout.country == false &&
            $scope.checkout.state == false &&
            $scope.checkout.zipcode == false){

            if($scope.billinginfo){
                $scope.ccinfo = false;
                $scope.verification = true;
            }else {
                var modalInstance = $modal.open({
                    templateUrl: 'billinginfo.html',
                    controller: billinginfoCtrl,
                    resolve : {
                        id : function() {
                            return $scope.member.id;
                        }
                    }
                }).result.then(function(data) {
                    $scope.billinginfo = data;
                    console.log($scope.billinginfo);
                });
            }
        }
    }

    $scope.processpaypal = function(){
        $scope.change($scope.shippinginfo.firstname, 'firstname');
        $scope.change($scope.shippinginfo.lastname, 'lastname');
        $scope.change($scope.shippinginfo.address, 'address');
        $scope.change($scope.shippinginfo.city, 'city');
        $scope.change($scope.shippinginfo.country, 'country');
        $scope.change($scope.shippinginfo.state, 'state');
        $scope.change($scope.shippinginfo.zipcode, 'zipcode');

        if($scope.checkout.firstname == false &&
            $scope.checkout.lastname == false &&
            $scope.checkout.address == false &&
            $scope.checkout.city == false &&
            $scope.checkout.country == false &&
            $scope.checkout.state == false &&
            $scope.checkout.zipcode == false){
            if($scope.billinginfo){
                var params = {
                  'invoice': $scope.invoiceno,
                  'shippinginfo': $scope.shippinginfo,
                  'orders' : $scope.bag,
                  'member' : $scope.member,
                  'totalamount' : $scope.total
                };

                Shop.savepaypaltemp(params, function(data) {
                  if(data.hasOwnProperty('success')){
                    store.set('bag', {});
                    $('.paypalform').submit();
                  } else if(data.hasOwnProperty('error')){
                    $scope.paypalprocess = false;
                    $scope.paypalprocesserror = 'Something went wrong please try again later!';
                  }
                });
            }else {
                var modalInstance = $modal.open({
                    templateUrl: 'billinginfo.html',
                    controller: billinginfoCtrl,
                    resolve : {
                        id : function() {
                            return $scope.member.id;
                        }
                    }
                }).result.then(function(data) {
                    $scope.billinginfo = data;
                });
            }
        }
    }

    $scope.onpaymenttype = function(data) {
        $scope.paymenttype=data;
    }
    
    $scope.placeorder = function(){
        if($scope.billinginfo){
            var order = {
                paymenttype : $scope.paymenttype,
                order: $scope.bag,
                shippinginfo: $scope.shippinginfo,
                payment : $scope.creditcard,
                totalamount: $scope.total,
                tax : $scope.tax,
                shippingfee : $scope.shippingfee,
                invoice : $scope.invoiceno
            }

            Shop.placeorder(order, $scope.member.id, function(data) {
                console.log(data);
                if(data.hasOwnProperty('success')){
                    toaster.pop('success', '', 'Your order has been successfully submitted.');
                    store.remove('bag');
                    $timeout(function() {
                        $window.location = '/shop';
                    }, 4000);
                }else {
                    toaster.pop('error', '', '* make sure you have already entered your billing information on the account settings.');
                }
            });
        }
    }

    
})

