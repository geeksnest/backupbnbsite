'use strict';

app.controller("addCartCtrl", function($scope, Config, Shop, Login, $window, jwtHelper, $timeout, store, $modal) {
    var slug = window.location.pathname.replace('/shop/products/', '');
    $scope.amazonlink = Config.amazonlink;
    $scope.BaseURL = Config.BaseURL;
    $scope.featuredimg = '';
    $scope.featured = [];
    $scope.slugs = slug;
    $scope.qty = 1;

    var getproduct = function(slug) {
        Shop.getproduct(slug, function(data) {
            $scope.suggestions = data.suggestions;
            $scope.category = data.category;
            $scope.product = data.product;
            $scope.product.qty = 1;
            $scope.featured = data.featured;
            if($scope.product.maxquantity > $scope.product.quantity){
                $scope.product.maxquantity = $scope.product.quantity;
            }
            $scope.product.avquantity = [];
            var x = 0;
            while(x < $scope.product.maxquantity){
                if(x >= $scope.product.minquantity){
                    x++;

                }else {
                    x = $scope.product.minquantity;
                }
                $scope.product.avquantity.push(x);
            }
            angular.forEach($scope.product.images, function(value, key) {
                if(value['status'] == 1){
                    // $scope.featuredimg = Config.amazonlink + "/uploads/productimage/" + $scope.product.productid + "/" +value['filename'];
                    $scope.featuredimg = value['filename'];
                }
            });
        });
    }

    getproduct(slug);

    $scope.setimg = function(img) {
        $scope.featuredimg = img;
    }

    var redirect = function() {
        $timeout(function() {
            $window.location = '/bag';
        }, 500);
    }
    $scope.addtocart = function(qty) {
        // $cookieStore.remove('cart');
        if(qty != undefined || qty != null){
            if(Login.getCurrentUser().firstname == undefined){
                if(store.get('bag') != undefined){
                    var temp = store.get('bag');
                    temp.push({ id : $scope.product.productid, quantity : qty });
                    store.set('bag', temp);
                    store.set('new', $scope.product.name);
                    redirect();
                }else {
                    store.set('bag', [{ id : $scope.product.productid, quantity : qty}]);
                    store.set('new', $scope.product.name);
                    redirect();
                }
            } else {
                Shop.addtocart({ id : $scope.product.productid, quantity : qty, memberid : Login.getCurrentUser().id}, function(data) {
                    store.set('new', $scope.product.name);
                    redirect();
                });
            }

        }
    }

    $scope.imgZoom = function(img) {
        var modalInstance = $modal.open({
            templateUrl: 'imgzoom.html',
            controller: function($scope, $modalInstance) {
                $scope.img = img;
            },
            resolve: {
                img : function() {
                    return img;
                }
            }
        });
    }
})
