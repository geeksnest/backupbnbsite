app.factory("bnbbuzzFactory", function($http, Config) {
  return {
    getnews: function(category, page, callback) {
      $http({
        url: Config.ApiURL + "/fe/bnbbuzz/index/"+ category +"/"+ page,
        method: "GET"
      }).success(function(data){
        callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    },
    getnewscategories: function(callback) {
      $http({
        url: Config.ApiURL + "/fe/bnbbuzz/index/getnewscategories",
        method: "GET"
      }).success(function(data){
        callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    }
  };
});
