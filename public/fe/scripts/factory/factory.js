app.factory("Factory", function($http, Config, toaster, $modal) {
  return {
    stateswithcenter: function(callback) {
      $http({
        url: Config.ApiURL + "/fe/starterspackage/loadstatewithcenters",
        method: "get",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function(data) {
          callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    },
    modal: function(tplUrl, ctrl, size, data) {
      var modalInstance = $modal.open({
          templateUrl: tplUrl,
          controller: ctrl,
          size: size,
          resolve: {
              data: function() {
                  return data;
              }
          }
      });
      return modalInstance;
    },
    Modal: function(tplUrl, size, data, ctrl) {
      var modalInstance = $modal.open({
          templateUrl: tplUrl,
          controller: ctrl,
          size: size,
          resolve: {
              data: function() {
                  return data;
              }
          }
      });
      return modalInstance;
    },
    modaL: function(tplUrl, size, ctrl) {
      var modalInstance = $modal.open({
          templateUrl: tplUrl,
          size: size,
          controller: ctrl
      });
      return modalInstance;
    },
    toaster: function(type, title, body) {
      return toaster.pop({
        type: type,
        title: title,
        body: body,
        showCloseButton: true
      });
    },
    sendEmail: function(email, callback) {
      $http({
        url: Config.ApiURL + "/fe/contactus/sendmail",
        method: "post",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(email)
      }).success(function(data) {
          callback(data);
      });
    },
    url: function() {
        var url = new RegExp(Config.BaseURL+'\/(.*)');
        var auth = window.location.href.match(url);
        return auth[0];
    },
    urlLastIndex: function() {
        var url = new RegExp(Config.BaseURL+'\/(.*)');
        var auth = window.location.href.match(url);
        return auth[0].substring(auth[0].lastIndexOf('/')+1);
    }

  };
});
