/*
construct by:
[. .]nulL
11/18/15
*/
app.factory("DateFactory", function() {
  return {
    month: function () {
        return [
            {name: 'January', val: 01},
            {name: 'February', val: 02},
            {name: 'March', val: 03},
            {name: 'April', val: 04},
            {name: 'May', val: 05},
            {name: 'June', val: 06},
            {name: 'July', val: 07},
            {name: 'August', val: 08},
            {name: 'September', val: 09},
            {name: 'October', val: 10},
            {name: 'November', val: 11},
            {name: 'December', val: 12}
        ];
    },
    expiyear_up: function() {
      var d = new Date();
      var year = d.getFullYear();

      var expiyear_up = [];
      for(var o=year; o<=year+50; o++) {
        expiyear_up.push(o);
      }

      return expiyear_up;
    }
  };
});
