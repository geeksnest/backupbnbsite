app.factory("ShareStoryFactory", function($http, Config) {
  return {
    submitStory: function(story, callback) {
      $http({
        url: Config.ApiURL + "/success-stories/submitstory",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(story)
      }).success(function (data) {
        callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    }
  };
});
