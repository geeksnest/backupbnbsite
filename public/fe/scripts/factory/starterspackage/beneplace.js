app.factory('beneplaceFactory', function($http, Config, $window) {
    return {
        loadstatewithcenters: function(callback) {
            $http({
                url: Config.ApiURL + "/fe/starterspackage/loadstatewithcenters",
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
      					callback({err:"Can't communicate to API properly"});
      			});
        },
        searchlocation: function(params, callback) {
            $http({
                url: Config.ApiURL + "/fe/starterspackage/searchlocation",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(params)
            }).success(function (data) {
                callback(data);
            }).error(function () {
      					callback({err:"Can't communicate to API properly"});
      			});
        },
        getstarterloadscheduleAction: function(params, callback) {
            $http({
                url: Config.ApiURL + "/fe/starterspackage/getstarterloadscheduleAction",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(params)
            }).success(function (data) {
                callback(data);
            }).error(function () {
      					callback({err:"Can't communicate to API properly"});
      			});
        },
        loadprice: function(callback) {
            $http({
                url: Config.ApiURL + "/price/loadbeneplaceprice",
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
      					callback({err:"Can't communicate to API properly"});
      			});
        },
        savetemppaypaltransactionAction: function(params, callback) {
            $http({
                url: Config.ApiURL + "/fe/starterspackage/savetemppaypaltransactionAction",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(params)
            }).success(function (data) {
                callback(data);
            }).error(function () {
      					callback({err:"Can't communicate to API properly"});
      			});
        },
        authorize: function(params,callback){
            $http({
                url: Config.ApiURL + "/fe/starterspackage/authorize",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(params)
            }).success(function (data) {
                callback(data);
            }).error(function () {
      					callback({err:"Can't communicate to API properly"});
      			});
        },
        searchzip: function(zip, callback) {
            $http({
                url: Config.ApiURL + "/fe/starterspackage/searchzip/" + zip,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
      					callback({err:"Can't communicate to API properly"});
      			});
        },
        getcenter: function(slug, callback) {
            $http({
                url: Config.ApiURL + "/fe/starterspackage/getcenter/" + slug,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
      					callback({err:"Can't communicate to API properly"});
      			});
        },
        viewreferrallink: function(callback){
           $http({
               url: Config.ApiURL +"/be/beneplace/viewreferrallink",
               method: "GET",
               headers: {'Content-Type': 'application/x-www-form-urlencoded'}
           }).success(function (data) {
               callback(data);
           }).error(function () {
     					   callback({err:"Can't communicate to API properly"});
     			});
       }
    };
});
