app.factory("WsTitleFactory", function($http, Config) {
  return {
    findworkshop: function(find, callback) {
      $http({
        url: Config.ApiURL + "/_workshops/findworkshop",
        method: "POST",
        headers: {"Content-Type" : "application/x-www-form-urlencoded"},
        data: $.param(find)
      }).success(function(data){
        callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    },
    thisworkshop: function(workshoptitleslugs, workshopid, statecode, callback) {
      $http({
        url: Config.ApiURL + "/_workshops/thisworkshop",
        method: "POST",
        headers: {"Content-Type" : "application/x-www-form-urlencoded"},
        data: $.param({'workshoptitleslugs':workshoptitleslugs, 'workshopid':workshopid, 'statecode':statecode})
      }).success(function(data){
        callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    },
    submitpayment: function(sessionkey, device, workshop, registrant, paymentinfo, billinginfo, callback) {
      $http({
        url: Config.ApiURL + "/_workshops/submitpayment",
        method: "POST",
        headers: {"Content-Type" : "application/x-www-form-urlencoded"},
        data: $.param({'sessionkey':sessionkey, 'device':device, 'workshop':workshop, 'registrant':registrant, 'paymentinfo':paymentinfo, 'billinginfo':billinginfo})
      }).success(function(data){
        callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    },
    randomalphanum: function() {
        var s = "";
        while(s.length<36&&36>0) {
            var r = Math.random();
            s+= (r<0.1?Math.floor(r*100):String.fromCharCode(Math.floor(r*26) + (r>0.5?97:65)));
        }
        return s;
    }
  };
});
