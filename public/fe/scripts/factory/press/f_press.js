/**
 * Created by ebautistajr on 3/30/15.
 */

 app.factory('pressFactory', function($http, Config, $window) {
    return {
        getpage: function(data, callback) {
            $http({
                url: Config.ApiURL + "/page/getpage/" + data,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        readpage: function(_slug, callback) {
            $http({
                url: Config.ApiURL + "/read/presspage/" + _slug,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        loadlist: function(num, off, keyword , callback){
            $http({
                url: Config.ApiURL +"/presspage/managenews/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
             callback(data);
             pagetotalitem = data.total_items;
             currentPage = data.index;
         }).error(function () {
   					callback({err:"Can't communicate to API properly"});
   			});
     },
 };
});
