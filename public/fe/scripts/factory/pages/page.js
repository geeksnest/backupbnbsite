/**
 * Created by ebautistajr on 3/30/15.
 */

app.factory('pageFactory', function($http, Config, $window) {
    return {
        getpage: function(data, callback) {
            $http({
                url: Config.ApiURL + "/page/getpage/" + data,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
    };
});
