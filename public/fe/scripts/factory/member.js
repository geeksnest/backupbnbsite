/**
 * Created by ebautistajr on 3/30/15.
 */

app.factory('Member', function($http, Config, $window) {
    return {
        login: function(data, callback) {
            $http({
                url: Config.ApiURL + "/member/login",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        validateEmail: function(data, callback) {
            $http({
                url: Config.ApiURL + "/member/validateemail/" + data,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        register: function(data, callback) {
            $http({
                url: Config.ApiURL + "/member/register",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        loginFB: function(data, callback) {
            $http({
                url: Config.ApiURL + "/member/loginfb",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        logingplus: function(data, callback) {
            $http({
                url: Config.ApiURL + "/member/logingplus",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        getbag: function(id, callback) {
            $http({
                url: Config.ApiURL + "/shop/getmemberbag/" + id,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        getstates: function(callback) {
          $http({
            url: Config.ApiURL + "/fe/location/regions",
            method: "GET",
            headers: {"Content-Type":"application/x-www-form-urlencoded"},
          }).success(function(data, status, headers, config){
            callback(data);
          });
        },
        getcenter: function(state_code, callback) {
          $http({
            url: Config.ApiURL + "/fe/location/selectstate",
            method: "POST",
            headers: {"Content-Type":"application/x-www-form-urlencoded"},
            data: $.param({state_code:state_code})
          }).success(function(data, status, headers, config){
            callback(data);

          });
        },
        getCurrentUser: function(memberid, callback) {
          $http({
            url: Config.ApiURL + "/fe/getcurrentuser/"+memberid,
            method: "GET",
            headers: {"Content-Type":"application/x-www-form-urlencoded"}
          }).success(function(data){
              callback(data);
          }).error(function () {
              callback("error");
          });
        }
    };
});
