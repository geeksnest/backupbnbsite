app.factory("AccountFactory", function($http, Config) { 'use strict';
  return {
    updatepersonalinfo: function(member, callback) {
      $http({
          url: Config.ApiURL + "/account/personalinfo/update",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(member)
      }).success(function (data) {
          callback(data);
      }).error(function () {
          callback("error");
      });
    },
    resetpassword: function(memberid, password, callback) {
      $http({
          url: Config.ApiURL + "/account/resetpassword",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param({memberid:memberid, password:password})
      }).success(function (data) {
          callback(data);
      }).error(function () {
          callback("error");
      });
    },
    forgotpassword: function(member, callback) {
      $http({
          url: Config.ApiURL + "/account/forgotpassword",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(member)
      }).success(function (data) {
          callback(data);
      }).error(function () {
          callback("error");
      });
    },
    changepassword: function(requestcode, newpwd, callback) {
      $http({
          url: Config.ApiURL + "/changepassword",
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param({requestcode:requestcode, newpwd:newpwd})
      }).success(function (data) {
          callback(data);
      }).error(function () {
          callback("error");
      });
    }
  };
});
