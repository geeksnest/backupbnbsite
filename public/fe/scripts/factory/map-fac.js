app.factory('MapFactory', function($http, uiGmapGoogleMapApi) {
  return {
    singlemap: function(data, callback) {
      var markers = [];
      var map = {center: {latitude: data.lat, longitude: data.lon }, zoom: 16 };
      var styleArray = [
      {     "featureType": "road",
             "elementType":
             "labels.icon",
             "stylers": [
              { "saturation": 1 },
              { "gamma": 1 },
              { "visibility": "on" },
              { "hue": "#e6ff00" }
             ]
          },
          { "elementType": "geometry", "stylers": [
            { "saturation": -100 }
            ]
          }
      ];

      var options = {
        styles: styleArray,
        backgroundColor:"#eeeeee",
        panControl:true,
        scrollwheel: false,
        zoomControl:true,
        mapTypeControl:false,
        scaleControl:true,
        streetViewControl:true,
        overviewMapControl:true
        // zoomControlOptions: { style:google.maps.ZoomControlStyle.SMALL }
      };

      markers.push({
        id: data.centerid,
        latitude: data.lat,
        longitude: data.lon,
        title: 'Address > ' + data.centertitle + ': ' +  data.centeraddress + ', ' + data.centerzip,
        icon: '/img/pin.png'
      });

      callback({'map':map, 'options':options, 'markers':markers});

    },
    singlepin: function(data, callback) {
      var markers = [];
      var map = {center: {latitude: data[0].lat, longitude: data[0].lon }, zoom: 17 };
      var styleArray = [
      {     "featureType": "road",
             "elementType": "labels.icon",
             "stylers": [
              { "saturation": 1 },
              { "gamma": 1 },
              { "visibility": "on" },
              { "hue": "#e6ff00" }
             ]
          },
          { "elementType": "geometry", "stylers": [
            { "saturation": -100 }
            ]
          }
      ];

      var options = {
        styles: styleArray,
        backgroundColor:"#eeeeee",
        panControl:true,
        scrollwheel: false,
        zoomControl:true,
        mapTypeControl:false,
        scaleControl:true,
        streetViewControl:true,
        overviewMapControl:true,
        // zoomControlOptions: { style:google.maps.ZoomControlStyle.SMALL }
      };

      data.map(function(entry) {
        markers.push({
          id: entry.centertitle,
          latitude: entry.lat,
          longitude: entry.lon,
          title: 'Address > ' + entry.centertitle + ': ' +  entry.centeraddress + ', ' + entry.centerzip,
          icon: '/img/pin.png'});
      });

      callback({'map':map, 'options':options, 'markers':markers});

    },
    multipin: function(data, callback) {

      var zoom = 5;
      var markers = [];

      data.map(function(entry) {
          markers.push({
            id: entry.centertitle,
            latitude: entry.lat,
            longitude: entry.lon,
            title: 'Address > ' + entry.centertitle + ': ' +  entry.centeraddress + ', ' + entry.centerzip,
            icon: '/img/pin.png'
          });
      });


      var styleArray = [
      {     "featureType": "road",
             "elementType": "labels.icon",
             "stylers": [
              { "saturation": 1 },
              { "gamma": 1 },
              { "visibility": "on" },
              { "hue": "#e6ff00" }
             ]
          },
          { "elementType": "geometry", "stylers": [
            { "saturation": -100 }
            ]
          }
      ];

      var options = {
        styles: styleArray,
        backgroundColor:"#eeeeee",
        panControl:true,
        scrollwheel: false,
        zoomControl:true,
        mapTypeControl:false,
        scaleControl:true,
        streetViewControl:true,
        overviewMapControl:true,
      };

      var map = {center: {latitude: data[0].lat, longitude: data[0].lon }, zoom: zoom };

      callback({'map':map, 'options':options, 'markers':markers});
    }
  };
});
