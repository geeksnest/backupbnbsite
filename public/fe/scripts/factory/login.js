/**
 * Created by ebautistajr on 3/30/15.
 */

app.factory('Login', function($http, store, jwtHelper, $window) {
    return {
        getUserFromToken: function (callback) {
            if (store.get('jwt')) {
                var token = store.get('jwt');
                callback(jwtHelper.decodeToken(token));
            } else {
                callback({'error': 'No login yet.'});
            }
        },
        redirectToMainifLogin: function(){
            if (store.get('jwt')) {
                if(!jwtHelper.isTokenExpired(store.get('jwt'))){
                    $window.location = '/';
                }else{
                    store.remove('jwt');
                }
            }
        },
        getCurrentUser: function (){
            if (store.get('jwt')) {
                var token = store.get('jwt');
                return jwtHelper.decodeToken(token);
            } else {
                return {};
            }
        }
    };
});
