app.factory("CenterFactory", function($http, Config) {
  return {
    properties: function(centerslugs, callback) {
      $http({
        url: Config.ApiURL + "/fe/center/main/properties",
        method: "POST",
        headers: {"Content-Type":"application/x-www-form-urlencoded"},
        data: $.param({centerslugs:centerslugs})
      }).success(function(data){
        callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    },
    getScheduleAction: function(params, callback) {
      $http({
        url: Config.ApiURL + "/fe/center/main/getScheduleAction",
        method: "post",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(params)
      }).success(function (data) {
        callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    },
    getcentermonthlysched: function(params, callback) {
      $http({
        url: Config.ApiURL + "/fe/center/monthly/sched",
        method: "POST",
        headers: {"Content-Type":"application/x-www-form-urlencoded"},
        data: $.param(params)
      }).success(function(data){
        callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    },
    fullnews: function(centerslugs, callback) {
      $http({
        url: Config.ApiURL + "/fe/"+centerslugs+"/fullnews",
        method: "GET",
        headers: {"Content-Type":"application/x-www-form-urlencoded"}
      }).success(function(data){
          callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    },
    fulltestimonials: function(offset, centerslugs, callback) {
      $http({
        url: Config.ApiURL + "/fe/"+centerslugs+"/fulltestimonials/"+offset,
        method: "GET",
        headers: {"Content-Type":"application/x-www-form-urlencoded"}
      }).success(function(data){
          callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    },
    viewsched: function(data, callback) {
      $http({
        url: Config.ApiURL + "/fe/viewsched",
        method: "POST",
        headers: {"Content-Type":"application/x-www-form-urlencoded"},
        data: $.param(data)
      }).success(function(data){
          callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    },
    getcenterevents: function(data, callback) {
      $http({
        url: Config.ApiURL + "/fe/center/main/geteventAction",
        method: "POST",
        headers: {"Content-Type":"application/x-www-form-urlencoded"},
        data: $.param(data)
      }).success(function(data){
          callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    }
  };
});
