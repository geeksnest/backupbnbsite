app.factory("LocIndexFactory", function($http, Config) {
  return {
    states: function(callback) {
      $http({
        url: Config.ApiURL + "/fe/location/regions",
        method: "GET",
        headers: {"Content-Type":"application/x-www-form-urlencoded"},
      }).success(function(data){
        callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    },
    selectstate: function(state_code, callback) {
      $http({
        url: Config.ApiURL + "/fe/location/selectstate",
        method: "POST",
        headers: {"Content-Type":"application/x-www-form-urlencoded"},
        data: $.param({state_code:state_code})
      }).success(function(data){
        callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    }
  };
});
