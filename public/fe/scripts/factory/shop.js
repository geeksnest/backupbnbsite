app.factory('Shop', function($http, Config, $q, $window) {
    return {
        get: function(data, callback){
            $http({
                url: Config.ApiURL + "/shop/get",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        getproduct: function(slug, callback) {
            $http({
                url: Config.ApiURL + "/shop/getproduct/" + slug,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        addtocart: function(data, callback) {
            $http({
                url: Config.ApiURL + "/shop/addtocart",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        getbag: function(data, callback) {
            $http({
                url: Config.ApiURL + "/shop/getbag",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        getmemberbag: function(id, callback) {
            $http({
                url: Config.ApiURL + "/shop/getmemberbag/" + id,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        removefrombag: function(id, callback){
            $http({
                url: Config.ApiURL + "/shop/removefrombag/" + id,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        addmultipletocart: function(data, id, callback) {
            $http({
                url: Config.ApiURL + "/shop/addmultipletocart/" + id,
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        updateQuantity: function(id, quantity, callback) {
            $http({
                url: Config.ApiURL + "/shop/updateQuantity/" + id + "/" + quantity,
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        month: function () {
            return [
                {name: 'January', val: 1},
                {name: 'February', val: 2},
                {name: 'March', val: 3},
                {name: 'April', val: 4},
                {name: 'May', val: 5},
                {name: 'June', val: 6},
                {name: 'July', val: 7},
                {name: 'August', val: 8},
                {name: 'September', val: 9},
                {name: 'October', val: 10},
                {name: 'November', val: 11},
                {name: 'December', val: 12}
            ]
        },
        day: function () {
            var day = [];
            for (var x = 1; x <= 31; x++) {
                day.push({'val': x})
            }
            return day;
        },
        year: function () {
            var year = [];
            for (var y = 2015; y <= 2050; y++) {
                year.push({'val': y})
            }
            return year;
        },
        getbillingaddr: function(id, callback) {
            $http({
                url: Config.ApiURL + "/shop/getbillingaddr/" + id,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        placeorder: function(data, id, callback) {
            $http({
                url: Config.ApiURL + "/shop/placeorder/" + id,
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        savepaypaltemp: function(data, callback) {
            $http({
                url: Config.ApiURL + "/shop/savepaypaltemp",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        savebillinginfo: function(data, id, callback) {
            $http({
                url: Config.ApiURL + "/shop/savebillinginfo/" + id,
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        },
        getevents: function(keyword, callback) {
            $http({
                url: Config.ApiURL + "/fe/getevents/" + keyword,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({success: "Something went wrong please check your feilds!", type: 'danger'});
            });
        }
    }
});
