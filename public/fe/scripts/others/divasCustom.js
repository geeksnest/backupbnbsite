$(document).ready(function() {

  $("#slider").divas({
    slideTransitionClass: "divas-slide-transition-left",
    titleTransitionClass: "divas-title-transition-left",
    titleTransitionParameter: "left",
    titleTransitionStartValue: "-999px",
    titleTransitionStopValue: "0px",
    wingsOverlayColor: "rgba(255,255,255,0.6)",
    mainImageWidth: "50%",
    // bullets: "yes" ,
    onImageClick: function(data) {console.log(data.currentTarget.currentSrc)}
  });

  $("#slider1").divas({
    slideTransitionClass: "divas-slide-transition-left",
    titleTransitionClass: "divas-title-transition-left",
    titleTransitionParameter: "left",
    titleTransitionStartValue: "-999px",
    titleTransitionStopValue: "0px",
    wingsOverlayColor: "rgba(255,255,255,0.6)",
    mainImageWidth: "100%",
    // bullets: "yes" ,
    onImageClick: function(data) {console.log(data.currentTarget.currentSrc)}
  });

});
