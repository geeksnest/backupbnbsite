if($(window).width() < 1312) { mobiletestiswiper(); }
else { normaltestiswiper(); }
if ($(window).width() < 940) { mobileSwiper(); }
else { normalSwiper(); }


$(window).resize(function() {
  if($(window).width() < 1312) { mobiletestiswiper(); }
  else { normaltestiswiper(); }
  if ($(window).width() < 940) { mobileSwiper(); }
  else { normalSwiper(); }
});

function normaltestiswiper() {
  var swiper2 = new Swiper('.swiper-centertestimonial', {
      pagination: '.swiper-pagination',
      slidesPerView: 2,
      paginationClickable: true,
      spaceBetween: 0,
      preloadImages: false,
      autoplay: 5000,
      loop: true,
      nextButton: '.next-btn',
      prevButton: '.prev-btn'
  });
}

function normalSwiper() {
  setTimeout(function() {
    var swiper = new Swiper('.swiper-center', {
      slidesPerView: 3,
      spaceBetween: 0,
      loop: true,
      preloadImages: false,
      lazyLoading: true,
      autoplay: 5000,
      autoplayDisableOnInteraction: false,
      pagination: '.swiper-pagination',
      paginationClickable: true,
    });

    var swiper3 = new Swiper('.swiper-centernews', {
        pagination: '.swiper-pagination',
        slidesPerView: 2,
        paginationClickable: true,
        spaceBetween: 30,
        preloadImages: false,
        autoplay: 5000,
        loop: true,
        nextButton: '.next-btn-news',
        prevButton: '.prev-btn-news'
    });

  }, 100);
}

function mobileSwiper() {
  setTimeout(function() {
    var swiper = new Swiper('.swiper-center', {
      slidesPerView: 1,
      spaceBetween: 0,
      loop: true,
      preloadImages: false,
      lazyLoading: true,
      autoplay: 5000,
      autoplayDisableOnInteraction: false,
      pagination: '.swiper-pagination',
      paginationClickable: true,
    });

    var swiper3 = new Swiper('.swiper-centernews', {
        pagination: '.swiper-pagination',
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 30,
        preloadImages: false,
        autoplay: 5000,
        loop: true,
        nextButton: '.next-btn-news',
        prevButton: '.prev-btn-news'
    });

  }, 100);
}

function mobiletestiswiper() {
  var swiper2 = new Swiper('.swiper-centertestimonial', {
      pagination: '.swiper-pagination',
      slidesPerView: 1,
      paginationClickable: true,
      spaceBetween: 30,
      preloadImages: false,
      // autoplay: 5000,
      loop: true,
      nextButton: '.next-btn',
      prevButton: '.prev-btn'
  });
}
