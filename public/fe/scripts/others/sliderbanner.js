var swiper = new Swiper('.swiper-banner', {
    slidesPerView: 1,
    pagination: '.swiper-pagination',
    paginationClickable: true,
    spaceBetween: 0,
    loop: true,
    preloadImages: false,
    lazyLoading: true,
    autoplay: 5000,
    autoplayDisableOnInteraction: false
});
