$(function() {
  $('.item').matchHeight({
    byRow: true,
    property: 'height',
    target: null,
    remove: false
  });
});

// <<< nu77
$(".a-contactus").click(function(){
  $("#dv-contactus").css("display","inline-block");
  $("html, body").animate({ scrollTop: 0 }, "slow");
});

$(".x-contactus").click(function(){
  $("#dv-contactus").css("display","none");
});

$("#topMenu-res").hide();
$("#forgotpass-res").hide();

$("#navMenuSwitch").click(function(){
  $("#topMenu-res").fadeToggle( "slow", "linear" );
  $("#topMenu-res").css("visibility","visible");

  $(".home-swiper").fadeToggle( "slow", "linear" ); // this is the body of home.volt
  $("#mainContent").fadeToggle( "slow", "swing" ); // this is the body of home.volt
});

$('#x').click(function(){
  $("#topMenu-res").fadeOut( "slow", "linear" );
  $(".home-swiper").fadeToggle( "slow", "linear" ); // this is the body of home.volt
  $("#mainContent").fadeToggle( "slow", "swing" ); //this is the body of main.volt
});

$('#x2').click(function(){
  $("#forgotpass-res").fadeOut( "slow", "linear" );
  $(".home-swiper").fadeToggle( "slow", "linear" ); // this is the body of home.volt
  $("#mainContent").fadeToggle( "slow", "swing" ); //this is the body of main.volt
});

var swiper = new Swiper('.swiper-banner', {
    slidesPerView: 1,
    pagination: '.swiper-pagination',
    paginationClickable: true,
    spaceBetween: 0,
    loop: true,
    preloadImages: false,
    lazyLoading: true,
    autoplay: 5000,
    autoplayDisableOnInteraction: false
});

var swiper = new Swiper('.swiper-slider', {
    slidesPerView: 1,
    pagination: '.swiper-pagination',
    paginationClickable: true,
    spaceBetween: 0,
    loop: true,
    preloadImages: false,
    lazyLoading: true,
    autoplay: 5000,
    autoplayDisableOnInteraction: false
});
// nu77 >>>
