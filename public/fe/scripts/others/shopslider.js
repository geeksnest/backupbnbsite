$(document).ready(function() {
      $("#slider").divas({
        slideTransitionClass: "divas-slide-transition-left",
        titleTransitionClass: "divas-title-transition-left",
        titleTransitionParameter: "left",
        titleTransitionStartValue: "-999px",
        titleTransitionStopValue: "0px",
        wingsOverlayColor: "rgba(255,255,255,0.6)",
        mainImageWidth: "50%",
        // bullets: "yes" ,
        onImageClick: function(data) {console.log(data.currentTarget.currentSrc)}
      });
      $("#slider1").divas({
        slideTransitionClass: "divas-slide-transition-left",
        titleTransitionClass: "divas-title-transition-left",
        titleTransitionParameter: "left",
        titleTransitionStartValue: "-999px",
        titleTransitionStopValue: "0px",
        wingsOverlayColor: "rgba(255,255,255,0.6)",
        mainImageWidth: "100%",
        // bullets: "yes" ,
        onImageClick: function(data) {console.log(data.currentTarget.currentSrc)}
      });
      
      $(".shop-menu ul li").click(function() {
        var sublayer1 = $(this).find('div.sub-layer1');
        var sublayer2 = $(this).find('div.sub-layer1 ul li div.sub-layer2');

        $('div.sub-layer1').not(sublayer1).css("display","none");

        if(sublayer1.css("display") != 'none' ) {
          sublayer1.css("display","none");
          sublayer2.css("display","none");
        }
        else { sublayer1.css("display","inline-block"); }
      })

      $(".shop-menu ul li .sub-layer1 ul li").click(function() {
        var sublayer2 = $(this).find('div.sub-layer2');
        console.log(sublayer2.css("display"));
        $('div.sub-layer2').not(sublayer2).css("display","none");

        if(sublayer2.css("display") != 'none' ) { sublayer2.css("display","none");  }
        else { sublayer2.css("display","inline-block"); }

      })

      $(".shop-menu-active").click(function() {
        if($(".shop-menu").is(':visible') === false) {
          $(".shop-menu").show("slow");
        } else {
          $(".shop-menu").hide("slow");
        }
        $("div.sub-layer1").css("display","none");
        $("div.sub-layer2").css("display","none");
      })
      responsiveShopMenu();
    });

    var doit;
    window.onresize = function() {
      clearTimeout(doit);
      doit = setTimeout(function() {
          responsiveShopMenu();
      }, 100);
    };

    function responsiveShopMenu() {
      if($(window).width() > 1066) {
        function runme() {
          if ($(".shop-menu").prop('scrollWidth') > $(".shop-menu").width()) {
            $(".shop-menu").hide();
            $(".shop-menu").addClass("rs");
            $(".shop-menu-active").show();
            $(".xdtri").show();
            return "mv";
          } else {
            $(".shop-menu").show();
            $(".shop-menu").removeClass("rs");
            $(".xdtri").hide();
            $(".shop-menu-active").hide();
            return "fv";
          }
        }
        runme();
        if(runme() == "fv") {
          runme(); //runagain to check if it is overflowed
        }
      } else {
        $(".shop-menu").hide();
        $(".shop-menu").addClass("rs");
        $(".shop-menu-active").show();
        $(".xdtri").show();
      }
    }