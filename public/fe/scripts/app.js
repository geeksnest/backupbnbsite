'use strict';

var app = angular.module('app', [
    'angular-storage',
    'ngAnimate',
    'toaster',
    'angular-jwt',
    'facebook',
    'googleplus',
    'twitterApp.services',
    'angular.vertilize',
    'ui.bootstrap',
    'uiGmapgoogle-maps',
    'angularUtils.directives.dirPagination',
    'ngFileUpload',
    'angularMoment',
    'ngCountryStateSelect',
    'uuid',
    'ng.deviceDetector',
    'ngSanitize',
    'iso.directives',
    'validation.match' //password match
    ])

.run(['$rootScope','store', 'Config', '$http', 'jwtHelper', function ($rootScope, store, Config, $http, jwtHelper) {
    if(store.get('jwt') != null){

        if(jwtHelper.isTokenExpired(store.get('jwt'))){
            // window.location = '/';
            // store.remove('jwt');
            console.log('expired');
        }
    }

}])

.config(function ($interpolateProvider, $httpProvider, FacebookProvider, GooglePlusProvider, uiGmapGoogleMapApiProvider){

    $httpProvider.interceptors.push(['$q', '$location', 'store', function($q, $location, store) {
        $httpProvider.defaults.headers.common['Authorization'] = "Bearer " + store.get('jwt');
        return {
            'request': function (config) {
                config.headers = config.headers || {};
                return config;
            },
            'responseError': function(response) {
                if(response.status === 401 || response.status === 403) {
                    // store.remove('jwt')
                    // window.location = '/';
                    console.log(response);
                }
                return $q.reject(response);
             }
        };
    }]);

    uiGmapGoogleMapApiProvider.configure({
        v: '3.20',
        libraries: 'weather,geometry,visualization'
    });

    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');

    FacebookProvider.init('424095004448491');

    GooglePlusProvider.init({
        clientId: '606284122887-a4nc22v8ukfrl3dfa2nduskl1a62d53u.apps.googleusercontent.com',
        apiKey: 'AIzaSyCOxzGM_rvpwlafWwalmvTRyYWAP97EGdg'
    });


})

;
