<?php

error_reporting(E_ALL);

try {
	/**
	 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
	 */
	$di = new \Phalcon\DI\FactoryDefault();
	/**
	 * Registering a router
	 */
	$di['router'] = function() {

		$router = new \Phalcon\Mvc\Router(false);

		$router->add('/:controller/:action', array(
			'module'=> 'frontend',
			'controller' => 'index',
			'action' => 'route404',
		));

		// $router->add('/{dataslugs}', array(
		// 	'module'=> 'frontend',
		// 	'controller' => 'pages',
		// 	'action' => 'view'
		// ));

		$router->add('/:params', array(
			'module'=> 'frontend',
			'controller' => 'pages',
			'action' => 'view',
			'params' => 1
		));


		//location custom URL
		// $router->add('/brooklynheights', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'pages',
		// 	'action' => 'view',
		// 	'params' => 'brooklyn-heights'
		// ));
		// $router->add('/downtownalbuquerque', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'pages',
		// 	'action' => 'view',
		// 	'params' => 'downtown-albuquerque'
		// ));
		// $router->add('/glenellyn', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'pages',
		// 	'action' => 'view',
		// 	'params' => 'glen-ellyn'
		// ));
		// $router->add('/oakpark', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'pages',
		// 	'action' => 'view',
		// 	'params' => 'oak-park'
		// ));
		// $router->add('/peachtreebuckhead', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'pages',
		// 	'action' => 'view',
		// 	'params' => 'peachtree-buckhead'
		// ));
		// $router->add('/suncity', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'pages',
		// 	'action' => 'view',
		// 	'params' => 'sun-city'
		// ));
		// $router->add('/washingtondc', array(
		// 	'module' => 'frontend',
		// 	'controller' => 'pages',
		// 	'action' => 'view',
		// 	'params' => 'washington-dc'
		// ));
		//end

		$router->add('/{dataslugs}/class-schedule', array(
			'module'=> 'frontend',
			'controller' => 'pages',
			'action' => 'view'
		));

		$router->add('/', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'home'
		));

		$router->add('/home', array(
			'module' => 'frontend',
			'controller' => 'index',
			'action' => 'home'
		));

		$router->add('/ilchi-lee', array(
			'module' => 'frontend',
			'controller' => 'pages',
			'action' => 'founder'
		));

		$router->add('/what-is/ilchi-lee', array(
			'module' => 'frontend',
			'controller' => 'pages',
			'action' => 'founder'
		));

		$router->add('/affiliates', array(
			'module' => 'frontend',
			'controller' => 'pages',
			'action' => 'affiliates'
		));

		$router->add('/faqs', array(
			'module' => 'frontend',
			'controller' => 'pages',
			'action' => 'faqs'
		));

		$router->add('/franchising', array(
			'module' => 'frontend',
			'controller' => 'pages',
			'action' => 'franchising'
		));

		$router->add('/terms-of-use', array(
			'module' => 'frontend',
			'controller' => 'pages',
			'action' => 'terms'
		));

		$router->add('/privacy-policies', array(
			'module' => 'frontend',
			'controller' => 'pages',
			'action' => 'privacy'
		));

		$router->add('/locations', array(
			'module' => 'frontend',
			'controller' => 'locations',
			'action' => 'index'
		));

		$router->add('/success-stories/{offset}', array(
			'module' => 'frontend',
			'controller' => 'testimonies',
			'action' => 'index'
		));

		$router->add('/success-stories/{keyword}/{offset}', array(
			'module' => 'frontend',
			'controller' => 'testimonies',
			'action' => 'searchresult'
		));

		$router->add('/success-stories/details/{ssid}/{subject}', array(
			'module' => 'frontend',
			'controller' => 'testimonies',
			'action' => 'details'
		));

		$router->add('/workshops', array(
			'module' => 'frontend',
			'controller' => 'workshops',
			'action' => 'index'
		));

		$router->add('/workshops/{title}', array(
			'module' => 'frontend',
			'controller' => 'workshops',
			'action' => 'title'
		));

		$router->add('/bnb-buzz/{page}', array(
			'module'=> 'frontend',
			'controller' => 'bnbbuzz',
			'action' => 'index'
		));

		$router->add('/bnb-buzz/{category}/{page}', array(
			'module'=> 'frontend',
			'controller' => 'bnbbuzz',
			'action' => 'category'
		));

		$router->add('/bnb-buzz/view/{pageslugs}', array(
			'module'=> 'frontend',
			'controller' => 'bnbbuzz',
			'action' => 'view'
		));

		$router->add('/{centerslugs}/newspost/{centernewslugs}', array(
			'module' => 'frontend',
			'controller' => 'locations',
			'action' => 'centernews'
		));

		$router->add('/getstarted/starterspackage/{slug}/{date}/{time}/{index}/{classandday}', array(
			'module'=> 'frontend',
			'controller' => 'starterspackage',
			'action' => 'index',
			'params' => 1
		));

		$router->add('/getstarted/starterspackage/{slug}', array(
			'module'=> 'frontend',
			'controller' => 'starterspackage',
			'action' => 'index',
			'params' => 1
		));

		$router->add('/getstarted/starterspackage', array(
			'module'=> 'frontend',
			'controller' => 'starterspackage',
			'action' => 'index',
		));

		$router->add('/beneplace/trial', array(
			'module'=> 'frontend',
			'controller' => 'starterspackage',
			'action' => 'beneplace',
		));

		$router->add('/payment/transactions/success', array(
			'module'=> 'frontend',
			'controller' => 'starterspackage',
			'action' => 'success'
		));

		$router->add('/press/:params', array(
			'module'=> 'frontend',
			'controller' => 'press',
			'action' => 'index',
			'params' => 1
		));

		$router->add('/read-press/:params', array(
			'module'=> 'frontend',
			'controller' => 'press',
			'action' => 'read',
			'params' => 1
		));

		// shop routes
		$router->add('/bag', array(
			'module'=> 'frontend',
			'controller' => 'shop',
			'action' => 'bag'
		));

		$router->add('/checkout', array(
			'module'=> 'frontend',
			'controller' => 'shop',
			'action' => 'checkout'
		));

		$router->add('/shop/products/:params', array(
			'module'=> 'frontend',
			'controller' => 'shop',
			'action' => 'view'
		));

		$router->add('/shop/payment/success', array(
			'module'=> 'frontend',
			'controller' => 'shop',
			'action' => 'success'
		));

		$router->add('/shop/products', array(
			'module'=> 'frontend',
			'controller' => 'shop',
			'action' => 'index',
		));

		$router->add('/shop', array(
			'module'=> 'frontend',
			'controller' => 'shop',
			'action' => 'index',
		));

		$router->add('/shop/digitalmedia', array(
			'module'=> 'frontend',
			'controller' => 'shop',
			'action' => 'digitalmedia',
		));

		$router->add('/shop/membership', array(
			'module'=> 'frontend',
			'controller' => 'shop',
			'action' => 'membership',
		));
		// end shop routes

		$router->add('/home/:controller/:action/:params', array(
			'module'=> 'frontend',
			'controller' => 'bnbbuzz',
			'action' => 'bnbbuzz'
		));

		$router->add('/account', array(
			'module'=> 'frontend',
			'controller' => 'account',
			'action' => 'index'
		));

		$router->add('/account/profile/:action', array(
			'module'    => 'frontend',
			'controller'=> 'account',
			'action'    => 1
		));
		$router->add('/changepassword/{requestcode}', array(
			'module'    => 'frontend',
			'controller'=> 'account',
			'action'    => 'changepassword'
		));

		$router->add('/account/media/:action', array(
			'module'=> 'frontend',
			'controller' => 'account',
			'action' => 1
		));

		$router->add('/bnbadmin', array(
			'module' => 'backend',
			'controller' => 'index',
			'action' => 'index'
		));

		$router->add('/bnbadmin/:controller', array(
			'module'=> 'backend',
			'controller' => 1
		));

		$router->add('/bnbadmin/:controller/:action/:params', array(
			'module'=> 'backend',
			'controller' => 1,
			'action' => 2,
			'params' => 3
		));

		$router->removeExtraSlashes(true);

		return $router;
	};

	/**
	 * The URL component is used to generate all kind of urls in the application
	 */
	$di->set('url', function() {
		$url = new \Phalcon\Mvc\Url();
		$url->setBaseUri('/');
		return $url;
	});

	/**
	 * Start the session the first time some component request the session service
	 */
	$di->set('session', function() {
		$session = new \Phalcon\Session\Adapter\Files();
		$session->start();
		return $session;
	});

	/**
	 * If the configuration specify the use of metadata adapter use it or use memory otherwise
	 */
	$di->set('modelsMetadata', function () {
	    return new MetaDataAdapter();
	});

    /*
    ModelsManager
    */
	$di->set('modelsManager', function() {
	      return new Phalcon\Mvc\Model\Manager();
	});
    $config = include "../app/config/config.php";
    // Store it in the Di container
    $di->set('config', function () use ($config) {
        return $config;
    });
	/**
	 * Handle the request
	 */
	$application = new \Phalcon\Mvc\Application();

	$application->setDI($di);

	/**
	 * Register application modules
	 */
	$application->registerModules(array(
		'frontend' => array(
			'className' => 'Modules\Frontend\Module',
			'path' => '../app/frontend/Module.php'
		),
		'backend' => array(
			'className' => 'Modules\Backend\Module',
			'path' => '../app/backend/Module.php'
		)
	));

	$config = include __DIR__ . "/disqusapi/disqusapi.php";

	echo $application->handle()->getContent();

} catch (Phalcon\Exception $e) {
	echo $e->getMessage();
} catch (PDOException $e){
	echo $e->getMessage();
}
