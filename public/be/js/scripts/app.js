'use strict';
// Declare app level module which depends on filters, and services
var app = angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'pascalprecht.translate',
    'app.factory',
    'app.directives',
    'app.controllers',
    'ngFileUpload',
    'xeditable',
    'ngImgCrop',
    'uiGmapgoogle-maps',
    'angularMoment',
    'ngAudio',
    'toaster',
    'angular.chosen',
    'highcharts-ng',
    'ui.autocomplete',
    'uuid',
    'ui.select',
    'ngSanitize',
    'colorpicker.module',
    'angular-storage',
    'ngCountryStateSelect',
    'angular.vertilize',
    'ngTagsInput',
    'angular-jwt'
  ])

.run(['$rootScope', '$state', '$stateParams', 'editableOptions','$templateCache', 'store', 'Config', '$http', function ($rootScope, $state, $stateParams,editableOptions, $templateCache, store, Config, $http) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    editableOptions.theme = 'bs3';
    $rootScope.ryan = 'ryan';

    if (store.get('AccessToken') == null) {
        window.location = '/bnbadmin';
    }else{
        $http({
            url: Config.ApiURL + '/authenticate/get',
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': "Bearer " + store.get('AccessToken')
            }
        }).success(function(data, status, headers, config) {

        });
    }

}])

.config(
  [          '$stateProvider', '$httpProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$interpolateProvider','$parseProvider', 'uiGmapGoogleMapApiProvider' ,
  function ($stateProvider, $httpProvider,  $urlRouterProvider,   $controllerProvider,   $compileProvider,   $filterProvider,   $provide, $interpolateProvider, $parseProvider,uiGmapGoogleMapApiProvider ) {

        uiGmapGoogleMapApiProvider.configure({
              //    key: 'your api key',
              v: '3.17',
              libraries: 'places' // Required for SearchBox.
        });

        $httpProvider.interceptors.push(['$q', '$location', 'store', function($q, $location, store) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    $httpProvider.defaults.headers.common['Authorization'] = "Bearer " + store.get('AccessToken');
                    return config;
                },
                'responseError': function(response) {
                    if(response.status === 401 || response.status === 403) {
                         store.remove('AccessToken')
                         window.location = '/bnbadmin/admin/logout';
                    }
                    return $q.reject(response);
                }
            };
        }]);

        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.factory    = $provide.factory;
        app.service    = $provide.service;
        app.constant   = $provide.constant;
        app.value      = $provide.value;

        $interpolateProvider.startSymbol('{[{');
        $interpolateProvider.endSymbol('}]}');

        $urlRouterProvider
        .otherwise('/dashboard');

        $stateProvider

        .state('dashboard', {
            url: '/dashboard',
            controller: 'dashboardCtrl',
            templateUrl: '/bnbadmin/admin/dashboard',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/dashboard/dashboard.js',
                        '/be/js/scripts/factory/dashboard/dashboard.js'
                        ]);
                }]
            }
        })
        /*USER STATE*/
        .state('userscreate', {
            url: '/userscreate',
            controller: 'AddUserCtrl',
            templateUrl: '/bnbadmin/users/create',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/user/adduser.js',
                        '/be/js/scripts/directives/validate.js',
                        '/be/js/scripts/factory/user.js'
                        ]);
                }]
            }
        })

        /*USER STATE*/
        .state('changepassword', {
            url: '/changepassword/:userid',
            controller: 'changepasswordCtrl',
            templateUrl: '/bnbadmin/users/changepassword',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/user/changepassword.js',
                        '/be/js/scripts/factory/user.js'
                        ]);
                }]
            }
        })

        .state('userlist', {
            url: '/userlist',
            templateUrl: '/bnbadmin/users/userlist',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/users.js',
                        '/be/js/scripts/directives/validate.js',
                        '/be/js/scripts/factory/user.js'
                        ]);
                }]
            }
        })
        .state('updateuser', {
            url: '/updateuser/:userid',
            controller: 'UserUpdateCtrl',
            templateUrl: '/bnbadmin/users/edituser',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/user/updateuser.js',
                        '/be/js/scripts/directives/validate.js',
                        '/be/js/scripts/factory/user.js'
                        ]);
                }]
            }
        })
        .state('editprofile', {
            url: '/editprofile/',
            templateUrl: '/bnbadmin/users/editprofile',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/user/updateuser.js',
                        '/be/js/scripts/directives/validate.js',
                        '/be/js/scripts/factory/user.js'
                        ]);
                }]
            }
        })
        /*END USER STATE*/


            //rainier state


            //start createpage
            .state('createpage', {
                url: '/createpage',
                controller: 'Createpage',
                templateUrl: '/bnbadmin/pages/createpage',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/page/createpage.js',
                        '/fe/scripts/others/jquery-imagefill.js'
                        ]);
                }]
                },

            })
            //end createpage

            //start managepage
            .state('managepage', {
                url: '/managepage',
                controller: 'Managepage',
                templateUrl: '/bnbadmin/pages/managepage',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/page/managepage.js',
                        '/be/js/scripts/factory/page/managepage.js'
                        ]);
                }]
                }

            })
            //end managepage

            //start editpage
            .state('editpage', {
                url: '/editpage/:pageid',
                controller: 'Editpage',
                templateUrl: '/bnbadmin/pages/editpage',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/page/editpage.js'
                        ]);
                }]
                }

            })
            //end editpage

            //page banner
            .state('pagebanner', {
                url: '/managepage/banner/:slugs',
                controller: 'PageBanner',
                templateUrl: '/bnbadmin/pages/pagebanner',
                resolve: {
                deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                                '/be/js/scripts/controllers/page/pagebanner.js',
                                '/be/js/scripts/factory/page/pagebanner.js'
                            ]);
                    }]
                }

            })
            //end page banner

            //start createnews
            .state('createnews', {
                url: '/createnews',
                controller: 'Createnews',
                templateUrl: '/bnbadmin/news/createnews',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/news/createnews.js',
                        '/be/js/scripts/factory/news/createnews.js'
                        ]);
                }]
                }

            })
            //end createnews

            //start managenews
            .state('managenews', {
                url: '/managenews/:page',
                controller: 'Managenews',
                templateUrl: '/bnbadmin/news/managenews',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/news/managenews.js',
                        '/be/js/scripts/factory/news/managenews.js',
                        '/be/js/scripts/filter/htmlfilter.js'
                        ]);
                }]
                }

            })
            //end managenews

            //start editnews
            .state('editnews', {
                url: '/editnews/:newsid/:page',
                controller: 'Editnews',
                templateUrl: '/bnbadmin/news/editnews',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/news/editnews.js',
                        '/be/js/scripts/factory/news/createnews.js',
                        '/be/js/scripts/factory/news/managenews.js'
                        ]);
                }]
                }
            })
            //end editnews

            //start editnews
            .state('editnewscenter', {
                url: '/editnewscenter/:newsid',
                controller: 'Editnewscenter',
                templateUrl: '/bnbadmin/news/editnewscenter',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/news/editnewscenter.js',
                        '/be/js/scripts/factory/news/createnews.js',
                        '/be/js/scripts/factory/news/managenews.js',
                        '/be/js/scripts/services/anchorsmooth.js'
                        ]);
                }]
                }

            })
            //end editnews

            //start editnews
            .state('createcategory', {
                url: '/createcategory',
                controller: 'Createcategory',
                templateUrl: '/bnbadmin/news/createcategory',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/news/createcategory.js'
                        ]);
                }]
                }

            })
            //end editnews

            //start createcenter
            .state('createcenter', {
                url: '/createcenter',
                controller: 'Createcenter',
                templateUrl: '/bnbadmin/center/createcenter',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/createcenter.js',
                        '/be/js/scripts/factory/center/createcenter.js',
                        '/be/js/scripts/services/anchorsmooth.js'
                        ]);
                }]
                }

            })
            //end createcenter

            //start managecenter
            .state('managecenter', {
                url: '/managecenter/:userid',
                controller: 'Managecenter',
                templateUrl: '/bnbadmin/center/managecenter',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/managecenter.js',
                        '/be/js/scripts/factory/center/managecenter.js',
                        '/be/js/scripts/services/anchorsmooth.js'
                        ]);
                }]
                }
            })
            //end managecenter

            //  managemycenter FOR CENTER MANAGER
            .state('managemycenter', {
                url: '/centerview/:centerid/',
                controller: 'Centerview',
                templateUrl: '/bnbadmin/center/centerview',
                resolve: {
                deps: ['uiLoad',
                 function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/center/centerview.js',
                        '/be/js/scripts/factory/center/centerview.js',
                        '/be/js/scripts/services/anchorsmooth.js'
                        ]);
                }]
                }
            })

            //start editcenter
            .state('editcenter', {
                url: '/editcenter/:centerid',
                controller: 'Editcenter',
                templateUrl: '/bnbadmin/center/editcenter',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/editcenter.js',
                        '/be/js/scripts/factory/center/editcenter.js',
                        // '/be/js/scripts/factory/center/createcenter.js',
                        '/be/js/scripts/services/anchorsmooth.js'
                        ]);
                }]
                }
            })
            //end editnews

            //start regiondistrict
            .state('regiondistrict', {
                url: '/regiondistrict',
                controller: 'regionanddistrictCtrl',
                templateUrl: '/bnbadmin/center/regionanddistrictmanagement',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                         '/be/js/scripts/controllers/center/regiondistrict.js',
                         '/be/js/scripts/factory/center/regiondistrict.js',
                        ]);
                }]
                }

            })
            //end regiondistrict

            //start globalpricing
            .state('globalpricing', {
                url: '/globalpricing',
                controller: 'globalpricingCtrl',
                templateUrl: '/bnbadmin/center/globalpricing',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/globalpricing.js',
                        '/be/js/scripts/factory/center/globalpricing.js'
                        ]);
                }]
                }

            })
            //end globalpricing

            //start beneplacepricing
            .state('beneplacepricing', {
                url: '/beneplacepricing',
                controller: 'beneplacepricingCtrl',
                templateUrl: '/bnbadmin/center/beneplacepricing',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/beneplacepricing.js',
                        '/be/js/scripts/factory/center/beneplacepricing.js'
                        ]);
                }]
                }

            })
            //end globalpricing

            //start allbeneplace
            .state('allbeneplace', {
                url: '/beneplace/:userid',
                controller: 'allbeneplaceCtrl',
                templateUrl: '/bnbadmin/center/allbeneplace',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/allbeneplace.js',
                        '/be/js/scripts/factory/center/allbeneplace.js'
                        ]);
                }]
                }

            })
            //end allbeneplace

            //start allbeneplaceunverified
            .state('allbeneplaceunverified', {
                url: '/beneplaceunverified/:userid',
                controller: 'allbeneplaceunverifiedCtrl',
                templateUrl: '/bnbadmin/center/allbeneplaceunverified',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/allbeneplaceunverified.js',
                        '/be/js/scripts/factory/center/allbeneplace.js'
                        ]);
                }]
                }

            })
            //end allbeneplaceunverified

            //start allbeneplacecancelled
            .state('allbeneplacecancelled', {
                url: '/allbeneplacecancelled/:userid',
                controller: 'allbeneplacecancelledCtrl',
                templateUrl: '/bnbadmin/center/allbeneplacecancelled',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/allbeneplacecancelled.js',
                        '/be/js/scripts/factory/center/allbeneplace.js'
                        ]);
                }]
                }

            })
            //end allbeneplaceunverified

            //start allintrosession
            .state('allintrosession', {
                url: '/introsession/:userid',
                controller: 'allintrosessionCtrl',
                templateUrl: '/bnbadmin/center/allintrosession',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/allintrosession.js',
                        '/be/js/scripts/factory/center/allintrosession.js'
                        ]);
                }]
                }

            })
            //end allintrosession

            //start allintrosession
            .state('allintrosession2', {
                url: '/introsessionunverified/:userid',
                controller: 'allintrosession2Ctrl',
                templateUrl: '/bnbadmin/center/allintrosession2',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/allintrosession2.js',
                        '/be/js/scripts/factory/center/allintrosession.js'
                        ]);
                }]
                }
            })
            //end allintrosession

            //start allintrosessioncanceled
            .state('allintrosessioncanceled', {
                url: '/introsessioncancelled/:userid',
                controller: 'allintrosessioncanceledCtrl',
                templateUrl: '/bnbadmin/center/allintrosessioncanceled',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/allintrosessioncanceled.js',
                        '/be/js/scripts/factory/center/allintrosession.js'
                        ]);
                }]
                }

            })
            //end allintrosessioncanceled

            //start allgroupclasssession
            .state('allgroupclasssession', {
                url: '/groupclasssession/:userid',
                controller: 'allgroupclasssessionCtrl',
                templateUrl: '/bnbadmin/center/allgroupclasssession',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/allgroupclasssession.js',
                        '/be/js/scripts/factory/center/allgroupclasssession.js'
                        ]);
                }]
                }

            })
            //end allgroupclasssession

            //start allgroupclasssession
            .state('allgroupclasssession2', {
                url: '/groupclasssessionunverified/:userid',
                controller: 'allgroupclasssession2Ctrl',
                templateUrl: '/bnbadmin/center/allgroupclasssession2',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/allgroupclasssession2.js',
                        '/be/js/scripts/factory/center/allgroupclasssession.js'
                        ]);
                }]
                }

            })
            //end allgroupclasssession

            //start allgroupclasssessioncanceled
            .state('allgroupclasssessioncanceled', {
                url: '/groupclasssessionuncancelled/:userid',
                controller: 'allgroupclasssessioncanceledCtrl',
                templateUrl: '/bnbadmin/center/allgroupclasssessioncanceled',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/allgroupclasssessioncanceled.js',
                        '/be/js/scripts/factory/center/allgroupclasssession.js'
                        ]);
                }]
                }

            })
            //end allgroupclasssessioncanceled

            //start centerview
            .state('centerview', {
                url: '/centerview/:centerid/:uifrom',
                controller: 'Centerview',
                templateUrl: '/bnbadmin/center/centerview',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/center/centerview.js',
                        '/be/js/scripts/factory/center/centerview.js',
                        '/be/js/scripts/factory/center/managenews.js'
                        ]);
                }]
                }

            })
            //end centerview

            .state('mycenter', {
                url: '/mycenter/:centerid',
                controller: 'mycenterCtrl',
                templateUrl: '/bnbadmin/center/mycenter',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/center/mycenterCtrl.js',
                        '/be/js/scripts/factory/center/MycenterFactory.js'
                        ]);
                }]
                }

            })

            //start notification
            .state('managenotification', {
                url: '/managenotification',
                controller: 'managenotificationCtrl',
                templateUrl: '/bnbadmin/notification/managenotification',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/notification/managenotification.js',
                        '/be/js/scripts/factory/notification/managenotification.js',
                        ]);
                }]
                }

            })
            //end notification

            .state('manageauditlog', {
                url: '/manageauditlog',
                controller: 'manageauditlogCtrl',
                templateUrl: '/bnbadmin/auditlog/manageauditlog',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/auditlog/manageauditlog.js',
                        '/be/js/scripts/factory/auditlog/manageauditlog.js',
                        ]);
                }]
                }

            })
            //end manageauditlog

            .state('manageauditlogcsv', {
                url: '/manageauditlogcsv',
                controller: 'manageauditlogcsvCtrl',
                templateUrl: '/bnbadmin/auditlog/manageauditlogcsv',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/auditlog/manageauditlogcsv.js',
                        '/be/js/scripts/factory/auditlog/manageauditlog.js',
                        ]);
                }]
                }

            })
            //end manageauditlogcsv

            //start slider
            .state('centerview.slider', {
                url: '/slider/{fold}',
                controller: 'sliderCtrl',
                templateUrl: '/bnbadmin/center/slider',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/slider.js',
                        ]);
                }]
                }
            })
            //end list

            //start contact
            .state('centerview.contact', {
                url: '/contact/{fold}',
                controller: 'detailCtrl',
                templateUrl: '/bnbadmin/center/contact',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/center/details.js',
                        '/be/js/scripts/factory/center/details.js'
                        ]);
                }]
                }
            })
            //end contact

            //start hours
            .state('centerview.hours', {
                url: '/hours/{fold}',
                controller: 'detailCtrl',
                templateUrl: '/bnbadmin/center/hours',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/center/details.js',
                        '/be/js/scripts/factory/center/details.js'
                        ]);
                }]
                }
            })
            //end hours

            //start list
            .state('centerview.list', {
                url: '/list/:userid/{fold}',
                controller: 'introsessionCtrl',
                templateUrl: '/bnbadmin/center/introsession',
                 resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/center/introsession.js',
                            '/be/js/scripts/factory/center/introsession.js'
                            ]);
                    }]
                }
            })
            //end list

            //start priv
            .state('centerview.priv', {
                url: '/priv/:userid/{fold}',
                controller: 'groupclasssessionCtrl',
                templateUrl: '/bnbadmin/center/groupclasssession',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/center/groupclasssession.js',
                            '/be/js/scripts/factory/center/groupclasssession.js'
                            ]);
                    }]
                }
            })
            //end priv

            //start priv
            .state('centerview.bene', {
                url: '/bene/:userid/{fold}',
                controller: 'beneplaceCtrl',
                templateUrl: '/bnbadmin/center/beneplace',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/center/beneplace.js',
                            '/be/js/scripts/factory/center/beneplace.js'
                            ]);
                    }]
                }
            })
            //end priv

            //start schedule
            .state('centerview.schedule', {
                url: '/schedule/{fold}',
                controller: 'scheduleCtrl',
                templateUrl: '/bnbadmin/center/schedule',
                 resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/center/schedule.js',
                            '/be/js/scripts/factory/center/schedule.js',
                            '/be/js/scripts/factory/center/calendar.js'
                            ]);
                    }]
                }
            })
            //end schedule

            //start pricing
            .state('centerview.pricing', {
                url: '/pricing/{fold}',
                controller: 'pricingCtrl',
                templateUrl: '/bnbadmin/center/pricing',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/center/pricing.js',
                            '/be/js/scripts/factory/center/pricing.js'
                            ]);
                    }]
                }
            })
            //end pricing

            //start addnews
            .state('centerview.addnews', {
                url: '/addnews/{fold}',
                controller: 'CreatenewsCtrl',
                templateUrl: '/bnbadmin/center/addnews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/center/createnews.js',
                            '/be/js/scripts/factory/center/createnews.js',
                            '/be/js/scripts/services/anchorsmooth.js'
                            ]);
                    }]
                }
            })
            //end addnews

            //start editnews
            .state('centerview.editnews', {
                url: '/centereditnews/:newsid{fold}',
                controller: 'Editnews',
                templateUrl: '/bnbadmin/center/editnews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/center/editnews.js',
                            '/be/js/scripts/factory/center/createnews.js',
                            '/be/js/scripts/services/anchorsmooth.js'
                            ]);
                    }]
                }
            })
            //end editnews

            //start managenews
            .state('centerview.managenews', {
                url: '/managenews/{fold}',
                controller: 'managenewsCtrl',
                templateUrl: '/bnbadmin/center/managenews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/center/managenews.js',

                            ]);
                    }]
                }
            })
            //end managenews



            //start calendar
            // .state('centerview.calendar', {
            //     url: '/calendar/{fold}',
            //     controller: 'CalendarCtrl',
            //     templateUrl: '/bnbadmin/center/calendar',
            //     resolve: {
            //     deps: ['uiLoad',
            //     function( uiLoad ){
            //         return uiLoad.load( [
            //             '/be/js/scripts/controllers/center/calendar.js',
            //             '/be/js/scripts/factory/center/calendar.js'
            //             ]);
            //     }]
            //     }
            // })
            //end calendar

            //start calendar
            .state('centerview.editcalendar', {
                url: '/calendar/{fold}/:activityid',
                controller: 'CalendarCtrl',
                templateUrl: '/bnbadmin/center/calendar',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/calendar.js',
                        '/be/js/scripts/factory/center/calendar.js'
                        ]);
                }]
                }
            })
            //end calendar

            //start socialmedia
            .state('centerview.socialmedia', {
                url: '/socialmedia/{fold}',
                controller: 'detailCtrl',
                templateUrl: '/bnbadmin/center/socialmedia',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/center/details.js',
                        '/be/js/scripts/factory/center/details.js'
                        ]);
                }]
                }
            })
            //end socialmedia

            //start spnl
            .state('centerview.spnl', {
                url: '/spnl/{fold}',
                controller: 'detailCtrl',
                templateUrl: '/bnbadmin/center/spnl',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/center/details.js',
                        '/be/js/scripts/factory/center/details.js'
                        ]);
                }]
                }
            })
            //end spnl

            //start success
            .state('centerview.success', {
                url: '/success/{fold}',
                templateUrl: '/bnbadmin/center/success',
                controller: 'successCtrl',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/center/success.js',
                        '/be/js/scripts/factory/testimony/managestory.js'
                        ]);
                }]
                }
            })
            //end success

            //start details
            .state('centerview.details', {
                url: '/details/{fold}',
                controller: 'detailCtrl',
                templateUrl: '/bnbadmin/center/details',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/center/details.js',
                        '/be/js/scripts/factory/center/details.js'
                        ]);
                }]
                }
            })
            //end details

            //start specialoffer
            .state('centerview.specialoffer', {
                url: '/specialoffer/{fold}',
                controller: 'specialofferCtrl',
                templateUrl: '/bnbadmin/center/specialoffer',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/center/specialoffer.js',
                        '/be/js/scripts/factory/center/specialoffer.js'
                        ]);
                }]
                }
            })
            //end specialoffer

            //start cell
            .state('centerview.cell', {
                url: '/cell/{fold}',
                controller: 'cellCtrl',
                templateUrl: '/bnbadmin/center/cell',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/center/cell.js',
                        '/be/js/scripts/factory/center/cell.js'
                        ]);
                }]
                }
            })
            //end cell

            //success-stories
            .state('managestories', {
                url: '/successstories/managestories',
                controller: 'managestoriesCtrl',
                templateUrl: '/bnbadmin/successstories/managestories',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/stories/manage.js',
                        '/be/js/scripts/factory/testimony/managestory.js',
                        ]);
                }]
                }
            })

            .state('viewstory', {
                url: '/successstories/viewstory/:storyid',
                controller: 'viewstoryCtrl',
                templateUrl: '/bnbadmin/successstories/viewstory',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/stories/viewstory.js',
                        '/be/js/scripts/factory/testimony/viewstory.js',
                        '/be/js/scripts/factory/testimony/managestory.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js'
                        ]);
                }]
                }
            })
            //end of success-stories

            .state('manageworkshop', {
                url: '/mgmt/workshop/:tab',
                controller: 'workshopCtrl',
                templateUrl: '/bnbadmin/workshop/index',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/workshop/workshop.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js'
                        ]);
                }]
                }
            })
            .state('workshopvenueeditpage', {
                url: '/mgmt/workshop/editvenue/:workshopvenueid',
                controller: 'workshopEditVenueCtrl',
                templateUrl: '/bnbadmin/workshop/editvenue',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/workshop/workshopEditVenueCtrl.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js'
                        ]);
                }]
                }
            })
            .state('workshoptitles', {
                url: '/workshop/titles',
                controller: 'workshopTitlesCtrl',
                templateUrl: '/bnbadmin/workshop/workshoptitles',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/workshop/workshopTitlesCtrl.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js'
                        ]);
                }]
                }
            })
            .state('new_wstitle', {
                url: '/workshop/new/title',
                controller: 'newwstitleCtrl',
                templateUrl: '/bnbadmin/workshop/new_wstitle',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load([
                        '/be/js/scripts/controllers/workshop/newwstitleCtrl.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js'
                    ]);
                }]
                }
            })
            .state('createworkshop', {
                url: '/mgmt/workshop/:tab',
                controller: 'workshopCtrl',
                templateUrl: '/bnbadmin/workshop/index',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/workshop/workshop.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js'
                        ]);
                }]
                }
            })
            .state('create_workshop', {
                url: '/create/workshop',
                controller: 'workshopCreateCtrl',
                templateUrl: '/bnbadmin/workshop/createworkshop',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/workshop/workshopCreateCtrl.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js'
                        ]);
                      }]
                }
            })
            .state('manage_workshops', {
                url: '/manage/workshops',
                controller: 'workshopManageCtrl',
                templateUrl: '/bnbadmin/workshop/manageworkshops',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/workshop/workshopManageCtrl.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js'
                        ]);
                      }]
                }
            })
            .state('workshoprelated', {
                url: '/workshop/related',
                controller: 'workshopRelatedCtrl',
                templateUrl: '/bnbadmin/workshop/workshoprelated',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/workshop/workshopRelatedCtrl.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js'
                        ]);
                      }]
                }
            })
            .state('workshopregistrants', {
                url: '/workshop/registrants',
                controller: 'workshopRegistrantsCtrl',
                templateUrl: '/bnbadmin/workshop/registrants',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/workshop/workshopRegistrantsCtrl.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js',
                        '/be/js/scripts/factory/universal.js'
                        ]);
                }]
                }
            })
            .state('workshopeditpage', {
                url: '/mgmt/workshop/edit/:workshopid',
                controller: 'workshopEditCtrl',
                templateUrl: '/bnbadmin/workshop/editworkshop',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/workshop/workshopEditCtrl.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js'
                        ]);
                }]
                }
            })
            .state('workshop_create_venue', {
                url: '/mgmt/workshop/new/venue',
                controller: 'workshopCreateVenueCtrl',
                templateUrl: '/bnbadmin/workshop/createvenue',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/workshop/workshopCreateVenueCtrl.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js',
                        '/be/js/scripts/factory/universal.js'
                        ]);
                      }]
                }
            })
            .state('workshop_manage_venue', {
                url: '/mgmt/workshop/manage/venue',
                controller: 'workshopManageVenueCtrl',
                templateUrl: '/bnbadmin/workshop/managevenue',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/workshop/workshopManageVenueCtrl.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js',
                        '/be/js/scripts/factory/universal.js'
                        ]);
                      }]
                }
            })
            .state('workshopsettings', {
                url: '/workshop/settings',
                controller: 'workshopSettingsCtrl',
                templateUrl: '/bnbadmin/workshop/workshopsettings',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/workshop/workshopSettingsCtrl.js',
                        '/be/js/scripts/factory/workshop/manageworkshop.js',
                        '/be/js/scripts/factory/universal.js'
                        ]);
                      }]
                }
            })
            .state('managecontacts', {
                url: '/managecontacts/managecontacts',
                controller: 'Managecontacts',
                templateUrl: '/bnbadmin/managecontacts/managecontacts',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/managecontacts/managecontacts.js',
                        '/be/js/scripts/factory/managecontacts/managecontacts.js'
                        ]);
                }]
                }
            })
            .state('managesubscribers', {
                url: '/managenewsletter/managesubscribers',
                controller: 'Managesubscribers',
                templateUrl: '/bnbadmin/managenewsletter/managesubscribers',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/managenewsletter/managesubscribers.js',
                        '/be/js/scripts/factory/managenewsletter/managesubscribers.js'
                        ]);
                }]
                }
            })
            .state('createnewsletter', {
                url: '/managenewsletter/createnewsletter',
                controller: 'Createnewsletter',
                templateUrl: '/bnbadmin/managenewsletter/createnewsletter',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/managenewsletter/createnewsletter.js',
                        '/be/js/scripts/factory/managenewsletter/managenewsletter.js'
                        ]);
                }]
                }
            })
            .state('managenewsletter', {
                url: '/managenewsletter/managenewsletter',
                controller: 'Managenewsletter',
                templateUrl: '/bnbadmin/managenewsletter/managenewsletter',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/managenewsletter/managenewsletter.js',
                        '/be/js/scripts/factory/managenewsletter/managenewsletter.js'
                        ]);
                }]
                }
            })
            .state('editnewspdf', {
                url: '/editnewspdf/:id',
                controller: 'Editnewspdf',
                templateUrl: '/bnbadmin/managenewsletter/editnewspdf',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/managenewsletter/editnewspdf.js',
                            '/be/js/scripts/factory/managenewsletter/managenewsletter.js'
                            ]);
                    }]
                }

            })
            .state('editnewsletter', {
                url: '/editnewsletter/:id',
                controller: 'Editnewsletter',
                templateUrl: '/bnbadmin/managenewsletter/editnewsletter',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/managenewsletter/editnewsletter.js',
                            '/be/js/scripts/factory/managenewsletter/managenewsletter.js'
                            ]);
                    }]
                }
            })

            .state('centerincome', {
                url: '/mgmt/centerincome',
                controller: 'centerincomeCtrl',
                templateUrl: '/bnbadmin/centerincome/view',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/centerincome/view.js',
                            '/be/js/scripts/factory/centerincome.js'
                            ]);
                    }]
                }
            })
            .state('centerincome_franchise', {
                url: '/mgmt/centerincome/franchise',
                controller: 'centerincomefranchiseCtrl',
                templateUrl: '/bnbadmin/centerincome/franchise',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/centerincome/franchise.js',
                            '/be/js/scripts/factory/centerincome.js'
                            ]);
                    }]
                }
            })

            /*product*/
            .state('addproduct', {
                url: '/addproduct',
                controller: 'AddProduct',
                templateUrl: '/bnbadmin/product/addproduct',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/addproduct.js',
                                '/be/js/scripts/factory/product.js'
                            ]);
                        }
                    ]
                }
            })

            .state('manageproduct', {
                url: '/manageproduct',
                controller: 'ManageProduct',
                templateUrl: '/bnbadmin/product/manageproduct',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/manage.js',
                                '/be/js/scripts/factory/product.js'
                            ]);
                        }
                    ]
                }
            })

            .state('editproduct', {
                url: '/editproduct/:productid',
                controller: 'EditProduct',
                templateUrl: '/bnbadmin/product/editproduct',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/edit.js',
                                '/be/js/scripts/factory/product.js'
                            ]);
                        }
                    ]
                }
            })

            .state('viewproduct', {
                url: '/viewproduct/:productid',
                controller: 'ViewProduct',
                templateUrl: '/bnbadmin/product/viewproduct',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/view.js',
                                '/be/js/scripts/factory/product.js'
                            ]);
                        }
                    ]
                }
            })

            .state('manageproductcategory', {
                url: '/manageproductcategory',
                controller: 'ManageCategory',
                templateUrl: '/bnbadmin/product/managecategory',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/managecategory.js',
                                '/be/js/scripts/factory/product.js'
                            ])
                        }
                    ]
                }
            })

            .state('manageproductsubcategory', {
                url: '/managesubcategory',
                controller: 'ManageSubCategory',
                templateUrl: '/bnbadmin/product/managesubcategory',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/managesubcategory.js',
                                '/be/js/scripts/factory/product.js'
                            ]);
                        }
                    ]
                }
            })

            .state('manageproducttype', {
                url: '/manageproducttype',
                controller: 'ManageType',
                templateUrl: '/bnbadmin/product/managetype',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad){
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/managetype.js',
                                '/be/js/scripts/factory/product.js'
                            ]);
                        }
                    ]
                }
            })

            .state('manageproducttags', {
                url : '/manageproducttags',
                controller: 'ManageTags',
                templateUrl: '/bnbadmin/product/managetags',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/product/managetags.js',
                                '/be/js/scripts/factory/product.js'
                            ])
                        }
                    ]
                }
            })

            /*order*/
            .state('addorder', {
                url: '/offlineorder',
                controller: 'AddOrder',
                templateUrl: '/bnbadmin/order/addorder',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/order/addorder.js',
                                '/be/js/scripts/factory/order.js'
                            ]);
                        }
                    ]
                }
            })

            .state('manageorder', {
                url: '/manageorders',
                controller: 'ManageOrder',
                templateUrl: '/bnbadmin/order/manage',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/order/manage.js',
                                '/be/js/scripts/factory/order.js'
                            ]);
                        }
                    ]
                }
            })

            .state('vieworder', {
                url: '/vieworder/:id',
                controller: 'ViewOrder',
                templateUrl: '/bnbadmin/order/view',
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/be/js/scripts/controllers/order/view.js',
                                '/be/js/scripts/factory/order.js'
                            ]);
                        }
                    ]
                }
            })

            .state('settings', {
                url: '/settings',
                controller: 'Settings',
                templateUrl: '/bnbadmin/settings/index',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/settings/settings.js',
                            '/be/js/scripts/factory/settings/settings.js'
                            ]);
                    }]
                }

            })
            //PRESS PAGE START HERE
            .state('create_presspage', {
                url: '/presspage',
                controller: 'PresspageCtrl',
                templateUrl: '/bnbadmin/presspage/createpage',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/presspage/co_create_presspage.js',
                        '/be/js/scripts/factory/presspage/fe_create_presspage.js',
                        '/be/js/scripts/factory/uploadimg/_imgupload.js'
                        ]);
                }]
                }

            })
             .state('manage_presspage', {
                url: '/managepresspage',
                controller: 'ManagPresspageCtrl',
                templateUrl: '/bnbadmin/presspage/managepage',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/presspage/co_manage_presspage.js',
                        '/be/js/scripts/factory/presspage/fa_manage_presspage.js',
                        '/be/js/scripts/filter/htmlfilter.js'
                        ]);
                }]
                }
            })
            .state('edit_presspage', {
                url: '/editpresspage/:newsid',
                controller: 'Editpresspage',
                templateUrl: '/bnbadmin/presspage/editpresspage',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/presspage/co_edit_presspage.js',
                        '/be/js/scripts/factory/presspage/fe_create_presspage.js',
                        '/be/js/scripts/factory/presspage/fa_manage_presspage.js',
                        '/be/js/scripts/services/anchorsmooth.js',
                        '/be/js/scripts/factory/uploadimg/_imgupload.js'
                        ]);
                }]
                }

            })
            //PRESS PAGE ENDS HERE

            //BNB SLIDER START
             .state('bnbslider', {
                url: '/slider',
                controller: 'GalleryCTRL',
                templateUrl: '/bnbadmin/slider/gallery',
                resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/be/js/scripts/controllers/slider/_galleryCTRL.js',
                        '/be/js/scripts/factory/gallery/fegallery.js'
                        ]);
                }]
                }
            })
            //BNB SLIDER START

            .state('esignature', {
               url: '/esignature',
               controller: 'testCtrl',
               templateUrl: '/bnbadmin/test/esignature',
               resolve: {
               deps: ['uiLoad',
               function( uiLoad ){
                   return uiLoad.load( [
                       '/be/js/scripts/controllers/test/testCtrl.js',
                       '/be/js/scripts/factory/test/TestFactory.js'
                       ]);
               }]
               }
           })

          //  manage class
          .state('manageclass', {
            url: '/manageclass',
            controller: 'ManageClassCtrl',
            templateUrl: '/bnbadmin/class/index',
            resolve: {
              deps: ['uiLoad', function(uiLoad) {
                return uiLoad.load([
                  '/be/js/scripts/controllers/class/manage.js',
                  '/be/js/scripts/factory/class/manage.js'
                ]);
              }]
            }
          })
    }
    ]
    )

.config(['$translateProvider', function($translateProvider){

  // Register a loader for the static files
  // So, the module will search missing translation tables under the specified urls.
  // Those urls are [prefix][langKey][suffix].
  $translateProvider.useStaticFilesLoader({
    prefix: '/be/js/jsons/',
    suffix: '.json'
});

  // Tell the module what language to use by default
  $translateProvider.preferredLanguage('en');

  // Tell the module to store the language in the local storage
  $translateProvider.useLocalStorage();

}])


/**
 * jQuery plugin config use ui-jq directive , config the js and css files that required
 * key: function name of the jQuery plugin
 * value: array of the css js file located
 */
 .constant('JQ_CONFIG', {
    easyPieChart:   ['/be/js/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
    sparkline:      ['/be/js/jquery/charts/sparkline/jquery.sparkline.min.js'],
    plot:           ['/be/js/jquery/charts/flot/jquery.flot.min.js',
                     '/be/js/jquery/charts/flot/jquery.flot.resize.js',
                     '/be/js/jquery/charts/flot/jquery.flot.tooltip.min.js',
                     '/be/js/jquery/charts/flot/jquery.flot.spline.js',
                     '/be/js/jquery/charts/flot/jquery.flot.orderBars.js',
                     '/be/js/jquery/charts/flot/jquery.flot.pie.min.js'],
    slimScroll:     ['/be/js/jquery/slimscroll/jquery.slimscroll.min.js'],
    sortable:       ['/be/js/jquery/sortable/jquery.sortable.js'],
    nestable:       ['/be/js/jquery/nestable/jquery.nestable.js',
                     '/be/js/jquery/nestable/nestable.css'],
    filestyle:      ['/be/js/jquery/file/bootstrap-filestyle.min.js'],
    slider:         ['/be/js/jquery/slider/bootstrap-slider.js',
                     '/be/js/jquery/slider/slider.css'],
    chosen:         ['/be/js/jquery/chosen/chosen.jquery.min.js',
                     '/be/js/jquery/chosen/chosen.css'],
    TouchSpin:      ['/be/js/jquery/spinner/jquery.bootstrap-touchspin.min.js',
                     '/be/js/jquery/spinner/jquery.bootstrap-touchspin.css'],
    wysiwyg:        ['/be/js/jquery/wysiwyg/bootstrap-wysiwyg.js',
                     '/be/js/jquery/wysiwyg/jquery.hotkeys.js'],
    dataTable:      ['/be/js/jquery/datatables/jquery.dataTables.min.js',
                     '/be/js/jquery/datatables/dataTables.bootstrap.js',
                     '/be/js/jquery/datatables/dataTables.bootstrap.css'],
    vectorMap:      ['/be/js/jquery/jvectormap/jquery-jvectormap.min.js',
                     '/be/js/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
                     '/be/js/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
                     '/be/js/jquery/jvectormap/jquery-jvectormap.css'],
    footable:       ['/be/js/jquery/footable/footable.all.min.js',
                     '/be/js/jquery/footable/footable.core.css']
}
)


.constant('MODULE_CONFIG', {
    select2:        ['/be/js/jquery/select2/select2.css',
    '/be/js/jquery/select2/select2-bootstrap.css',
    '/be/js/jquery/select2/select2.min.js',
    '/be/js/modules/ui-select2.js']
}
)
;
