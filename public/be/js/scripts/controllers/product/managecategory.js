app.controller('ManageCategory', function($scope, $state, Config, $modal, Product, $anchorScroll, $timeout){
    var num = 10;
    var off = 1;
    $scope.keyword = null;
    $scope.alerts = [];

    var alerts = function(data) {
        $scope.closeAlert();
        $scope.alerts.push({ type: data.type, msg: data.msg });
    }

    $scope.closeAlert = function() {
        $scope.alerts.splice(0,1);
    }

    var loadcategory = function(num, off, keyword) {
        Product.loadcategory(num, off, keyword, function(data) {
            $scope.category = data.category;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
            angular.forEach($scope.category, function(value, key) {
                $scope.category[key].subcat = '';
                angular.forEach(value['subcategory'], function(value2, key2) {
                    if(value['subcategory'].length != (key2 +1)){
                        data = ", ";
                    }else {
                        data = "";
                    }
                    $scope.category[key].subcat = $scope.category[key].subcat + value2['subcategory'] + data;
                });
            });
        });
    }

    loadcategory(num, off, $scope.keyword);

    $scope.addCategory = function() {
        var modalInstance = $modal.open({
            templateUrl: 'addProductCategory.html',
            controller: function($scope, $modalInstance) {
                $scope.exist = false;
                $scope.subcategories = [];
                $scope.category = {};

                Product.loadsubcateogry(function(data) {
                    $scope.subcategories = data;
                });
                

                $scope.validateCategory = function(category) {
                    Product.validateCategory(null, category, function(data) {
                        $scope.exist = data.exist;
                    });
                }

                $scope.validation = function(data) {
                    if(data.length > 0){
                        $scope.required = false;
                    }else {
                        $scope.required = true;
                    }
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }

                $scope.ok = function(category) {
                    if($scope.exist==false && category.subcategory.length > 0){
                        Product.saveCategory(category, function(data) {
                            alerts(data);
                            $modalInstance.dismiss();
                            loadcategory(num, off, null);
                        });
                    }
                }
            }            
        });
    }

    $scope.clear = function(){
        $scope.keyword=null;
        loadcategory(num, off, $scope.keyword);
    }

    $scope.search = function (keyword) {
        off = 1;
        $scope.keyword = keyword;
        loadcategory(num, off, $scope.keyword);
        $scope.searchtext='';
    }

    $scope.delete = function(id){
        var modalInstance = $modal.open({
            templateUrl: 'deleteProductCategory.html',
            controller: function($scope, $modalInstance) {
                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }

                $scope.ok = function() {
                    Product.deleteCategory(id, function(data) {
                        alerts(data);
                        $modalInstance.dismiss();
                        loadcategory(num, off, null);
                    });
                }
            },
            resolve: {
                id: function() {
                    return id;
                }
            }
        });
    }

    var openedit = function(id) {
        var modalInstance = $modal.open({
            templateUrl: 'editProductCategory2.html',
            controller: function($scope, $modalInstance) {
                $scope.subcategories = [];

                Product.loadeditcategory(id, function(data) {
                    $scope.category = data.category;
                    $scope.subcategories = data.subcategory
                    $scope.category.subcategory = [];

                    angular.forEach(data.subcat, function(value, key) {
                        $scope.category.subcategory.push(value['subcategory']);
                    });
                });

                $scope.validateCategory = function(category, id) {
                    Product.validateCategory(id, category, function(data) {
                        $scope.exist = data.exist;
                    });
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }

                $scope.ok = function(category) {
                    Product.editCategory(category, function(data) {
                        alerts(data);
                        $modalInstance.dismiss();
                        loadcategory(num, off, null);
                    });
                }
            },
            resolve: {
                id: function() {
                    return id;
                }
            }
        });
    }

    $scope.edit = function(id) {
        var modalInstance = $modal.open({
            templateUrl: 'editProductCategory.html',
            controller: function($scope, $modalInstance){
                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }

                $scope.ok = function() {
                    $modalInstance.dismiss();
                    openedit(id);
                }
            },
            resolve : {
                id : function() {
                    return id;
                }
            }
        });
    }

    $scope.setPage = function(page) {
        loadcategory(num, page, $scope.keyword);
    }
})