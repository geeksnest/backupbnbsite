app.controller('ViewProduct', function($scope, $state, $stateParams, Upload ,$q, $http, Config, $modal, $filter, Product, $anchorScroll, $timeout){
    $scope.amazonlink = Config.amazonlink;
    var invoicenum = 10;
    var invoiceoff = 1;
    $scope.maxSize = 10;

    var loadproduct = function(productid) {
        Product.viewproduct(productid, function(data) {
            $scope.product = data.product;
            $scope.invoice = data.invoice;
            $scope.bigTotalItems_invoice = data.total_items_invoice;
            $scope.bigTotalItems_hist = data.total_items_hist;
            $scope.bigCurrentPage_invoice = data.index;
            $scope.bigCurrentPage_hist = data.index;
            $scope.history = data.history;
        });
    } 

    loadproduct($stateParams.productid);

    $scope.setPage = function(page, type){
        Product.viewproductdetails($scope.product.productid, page, type, function(data){
            if(type=='invoice'){
                $scope.invoice = data.data;
                $scope.bigTotalItems_invoice = data.total_items;
                $scope.bigCurrentPage_invoice = data.index;
            }else if(type=='history'){
                $scope.history = data.data;
                $scope.bigTotalItems_hist = data.total_items;
                $scope.bigCurrentPage_hist = data.index;
            }
        });
    }

    $scope.print = function() {
        Product.getReport($stateParams.productid, function(data) {
            $scope.invoice2 = data.invoice;
            $scope.history2 = data.history;

            $timeout(function() {
                var pdf = new jsPDF('p', 'pt', 'letter');
                source = $('#print')[0];
                specialElementHandlers = {
                  '#bypassme': function(element, renderer){
                    return true
                  }
                }
                 
                margins = {
                    top: 50,
                    left: 60,
                    width: 545
                  };

                pdf.fromHTML(
                    source // HTML string or DOM elem ref.
                    , margins.left // x coord
                    , margins.top // y coord
                    , {
                      'width': margins.width // max width of content on PDF
                      , 'elementHandlers': specialElementHandlers
                    },
                    function (dispose) {
                    
                      // dispose: object with X, Y of the last line add to the PDF
                      //          this allow the insertion of new lines after html
                        pdf.save('BodynBrain.pdf');
                    }
                )
            }, 500);
        });
    }
})