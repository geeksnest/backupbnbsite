app.controller('ManageType', function($scope, $state, Config, $modal, Product, $anchorScroll, $timeout){
    var num = 10;
    var off = 1;
    $scope.keyword = null;
    $scope.alerts = [];

    var alerts = function(data) {
        $scope.closeAlert();
        $scope.alerts.push({ type: data.type, msg: data.msg });
    }

    $scope.closeAlert = function() {
        $scope.alerts.splice(0, 1);
    }

    var loadtypes = function(num, off, keyword) {
        Product.loadtypes(num, off, keyword, function(data) {
            $scope.types = data.type;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        });
    }

    loadtypes(num, off, $scope.keyword);

    $scope.clear = function(){
        $scope.keyword=null;
        loadtypes(num, off, $scope.keyword);
    }

    $scope.search = function (keyword) {
        off = 1;
        $scope.keyword = keyword;
        loadtypes(num, off, $scope.keyword);
        $scope.searchtext='';
    }

    $scope.setPage = function(page) {
        loadtypes(num, page, $scope.keyword);
    }
    
    $scope.delete = function(id) {
        
        var modalInstance = $modal.open({
            templateUrl: 'deleteProducttype.html',
            controller: function($scope, $modalInstance) {
                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }
                $scope.ok = function(){
                    Product.deleteType(id, function(data) {
                        alerts(data);
                        loadtypes(num, off, null);
                        $modalInstance.dismiss();
                    });
                }
            },
            resolve: {
                id : function() {
                    return id;
                }
            }
        });
    }

    $scope.addTags = function(){
        var modalInstance = $modal.open({
            templateUrl: 'addProducttype.html',
            controller: function($scope, $modalInstance){
                $scope.type = {}
                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }

                $scope.ok = function(type) {
                    if($scope.exist==false && type.type != undefined){
                        Product.addType(type, function(data){
                            alerts(data);
                            loadtypes(num, off, null);
                            $modalInstance.dismiss();
                        });
                    }
                }

                $scope.validateType = function(type) {
                    Product.validateType(type, function(data) {
                        $scope.exist = data.exist;
                    });
                }
            }
        });
    }

    $scope.updatetype = function(data, id) {
        Product.updatetype({ id: id, type : data }, function(data){
            alerts(data);
            loadtypes(num, off, null);
        });
    }
})