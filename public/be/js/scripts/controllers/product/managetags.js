app.controller('ManageTags', function($scope, $state, Config, $modal, Product, $anchorScroll, $timeout){
    var num = 10;
    var off = 1;
    $scope.keyword = null;
    $scope.alerts = [];
    var num2 = 10;
    var off2 = 1;
    $scope.keyword2 = null;
    $scope.alerts2 = [];
    $scope.maxSize = 10;

    var alerts = function(data) {
        $scope.closeAlert();
        $scope.alerts.push({ type: data.type, msg: data.msg });
    }

    $scope.closeAlert = function() {
        $scope.alerts.splice(0, 1);
    }

    var alerts2 = function(data) {
        $scope.closeAlert();
        $scope.alerts2.push({ type: data.type, msg: data.msg });
    }

    $scope.closeAlert2 = function() {
        $scope.alerts2.splice(0, 1);
    }

    var loadtagssize = function() {
        Product.loadtagssize(function(data) {
            $scope.tags = data.tags;
            $scope.CurrentPage = 1;
            $scope.size = data.size;
            $scope.TotalItems = data.total_items2;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = 1;
        });
    }

    loadtagssize();

    var loadtags = function(num, off, keyword) {
        Product.loadtags(num, off, keyword, function(data) {
            $scope.tags = data.tags;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        });
    }

    var loadsize = function(num2, off2, keyword2) {
        Product.loadsize(num2, off2, keyword2, function(data) {
            $scope.size = data.size;
            $scope.TotalItems = data.total_items;
            $scope.CurrentPage = data.index;
        });
    }

    $scope.clear = function(){
        $scope.keyword=null;
        loadtags(num, off, $scope.keyword);
    }

    $scope.clear2 = function(){
        $scope.keyword2=null;
        loadsize(num2, off2, $scope.keyword2);
    }

    $scope.search = function (keyword) {
        off = 1;
        $scope.keyword = keyword;
        loadtags(num, off, $scope.keyword);
        $scope.searchtext='';
    }

    $scope.search2 = function (keyword) {
        off = 1;
        $scope.keyword2 = keyword;
        loadsize(num2, off2, $scope.keyword2);
        $scope.searchtext2='';
    }

    $scope.setPage = function(page) {
        loadtags(num, page, $scope.keyword);
    }
    
    $scope.setPage2 = function(page2) {
        loadsize(num2, page2, $scope.keyword2);
    }

    $scope.delete = function(id) {
        
        var modalInstance = $modal.open({
            templateUrl: 'deleteProducttags.html',
            controller: function($scope, $modalInstance) {
                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }
                $scope.ok = function(){
                    Product.deleteTags(id, function(data) {
                        alerts(data);
                        loadtags(num, off, null);
                        $modalInstance.dismiss();
                    });
                }
            },
            resolve: {
                id : function() {
                    return id;
                }
            }
        });
    }

    $scope.addTags = function(){
        var modalInstance = $modal.open({
            templateUrl: 'addProducttags.html',
            controller: function($scope, $modalInstance){
                $scope.tags = {}
                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }

                $scope.ok = function(tags) {
                    if($scope.exist==false && tags.tag != undefined){
                        Product.addTags(tags, function(data){
                            alerts(data);
                            loadtags(num, off, null);
                            $modalInstance.dismiss();
                        });
                    }
                }

                $scope.validateTags = function(tags) {
                    Product.validateTags(null, tags, function(data) {
                        $scope.exist = data.exist;
                    });
                }
            }
        });
    }

    $scope.updatetags = function(data, id) {
        Product.updatetags({ id: id, tags : data }, function(data){
            alerts(data);
            loadtags(num, off, null);
        });
    }

    $scope.updatesize = function(data, id) {
        Product.updatesize({ id: id, size : data }, function(data){
            alerts2(data);
            loadsize(num2, off2, null);
        });
    }

    $scope.delete2 = function(id) {
        
        var modalInstance = $modal.open({
            templateUrl: 'deleteProductsize.html',
            controller: function($scope, $modalInstance) {
                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }
                $scope.ok = function(){
                    Product.deleteSize(id, function(data) {
                        alerts2(data);
                        loadsize(num2, off2, null);
                        $modalInstance.dismiss();
                    });
                }
            },
            resolve: {
                id : function() {
                    return id;
                }
            }
        });
    }
})