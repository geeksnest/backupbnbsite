app.controller('ManageProduct', function($scope, $state, Upload ,$q, $http, Config, $modal, $sce, $filter, Product, $anchorScroll, $timeout){
    $scope.products = [];
    $scope.alerts = [];
    $scope.keyword=null;
    var find = null;
    var num = 10;
    var off = 1;
    $scope.currentstatusshow = '';

    var alert = function(data){
        $scope.closeAlert();
        if(data.type == 'success'){
            $scope.alerts.push({ type: 'success', msg: data.msg });
        }else {
            $scope.alerts.push({ type: 'danger', msg: 'An error occurred please try again later.' });
        }
    }

    $scope.closeAlert = function(){
        $scope.alerts.splice(0,1);
    }

    var getproducts = function(number, page, keyword, find) {
        Product.getlist(number, page, keyword, find, function(data) {
            $scope.products = data.products;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.totalp = data.total;
            $scope.bigCurrentPage = data.index;
            $scope.loading = false;
            $scope.out_of_stock = data.out_of_stock;
            $scope.added_today = data.added_today;
            $scope.nearly_out_of_stock = data.nearly_out_of_stock;
            $scope.products.type = '';
        });
    }

    getproducts(num, off, $scope.keyword, find);

    $scope.delete = function(id) {
        var modalInstance = $modal.open({
            templateUrl: 'deleteProduct.html',
            controller : function($scope, $modalInstance) {
                var keyword = null;
                $scope.ok = function() {
                    Product.delete(id, function(data){
                        alert(data);
                        getproducts(num, off, keyword, find);
                        $modalInstance.dismiss();
                    });
                }
                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }
            },
            resolve: {
                id : function() {
                    return id;
                }
            }
        });
    }

    $scope.edit = function(id) {
        var modalUnstance = $modal.open({
            templateUrl: 'editProduct.html',
            controller : function($scope, $modalInstance) {
                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }
                $scope.ok = function() {
                    $modalInstance.dismiss();
                    $state.go('editproduct', {productid: id });
                }
            },
            resolve: {
                id: function() {
                    return id;
                }
            }
        });
    }

    $scope.setstatus = function (status,id) {
        $scope.currentstatusshow = id;
        var newstat;
        if(status==1){
            newstat = 0;
        }else{
            newstat = 1;
        }
        Product.updatestatus(newstat,id, function(data){
            var i = 2;
            setInterval(function(){
                i--;
                if(i == 0)
                {
                    getproducts(num, off, $scope.keyword, find);
                    $scope.currentstatusshow = '';
                }
            },1000)

        });
    }

    $scope.addquantity = function(id, quantity) {
        var modalInstance = $modal.open({
            templateUrl: 'addProductQuantity.html',
            controller: function($scope, $modalInstance) {
                $scope.product = { date : moment().format('YYYY-MM-DD')};
                $scope.quantity = parseInt(quantity);
                $scope.open = function($event,opened) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope[opened] = true;
                };
                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1,
                    class: 'datepicker'
                };

                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }

                $scope.ok = function() {
                    Product.addQuantity(id, $scope.product, function(data) {
                        alert(data);
                        getproducts(num, off, null, null);
                        $modalInstance.dismiss();
                    });
                }
            },
            resolve: {
                id: function() {
                    return id;
                },
                quantity: function() {
                    return quantity;
                }
            }
        });
    }

    $scope.adddiscount = function(product) {
        var modalInstance = $modal.open({
            templateUrl: 'addDiscount.html',
            controller: function($scope, $modalInstance, product) {
                $scope.product = product;

                $scope.open = function($event,opened) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope[opened] = true;
                };
                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1,
                    class: 'datepicker'
                };

                $scope.discount = function(data, type) {
                    if($scope.product.price!=undefined && $scope.product.price!=0){
                        if(type=="percentage" && data != undefined){
                            var res = ($scope.product.price * (data / 100)).toFixed(2);
                            $scope.product.discprice = res;
                            $scope.product.discounted = $scope.product.price - res;
                        }else if(type=="price" && data != undefined){
                            var res = (data * 100) / $scope.product.price;
                            $scope.product.discount = (res % 2) != 0 ? res.toFixed(2) : res;
                            $scope.product.discounted = $scope.product.price - data;
                        }
                    }
                }


                $scope.discount($scope.product.discount, "percentage");

                $scope.submit = function(prod) {
                    prod.discount_from = moment(prod.discount_from).format('YYYY-MM-DD');
                    prod.discount_to = moment(prod.discount_to).format('YYYY-MM-DD');

                    Product.addDiscount(prod.productid, prod, function(data) {
                        alert(data);
                        $modalInstance.dismiss();
                    });
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }
            },
            resolve : {
                product: function() {
                    return product;
                }
            }
        });
    }

    $scope.view = function(id) {
        $state.go('viewproduct', {productid: id });
    }

    $scope.setPage = function(page) {
        getproducts(num, page, $scope.keyword, find);
    }

    $scope.clear = function(){
        $scope.keyword=null;
        find = null;
        getproducts(num, off, null, null);
    }

    $scope.search = function (keyword) {
        off = 1;
        $scope.keyword = keyword;
        getproducts(num, off, keyword, find);
        $scope.searchtext='';
        $scope.formmanagenews.$setPristine();
    }

    $scope.notif = function(f) {
        $scope.keyword = null;
        find = f;
        getproducts(num, off, $scope.keyword, find);
    }

    $scope.print = function() {
        Product.getproductreport(function(data) {
            $scope.product2 = data;
            $timeout(function() {
                var pdf = new jsPDF('p', 'pt', 'letter');
                source = $('#print')[0];
                specialElementHandlers = {
                  '#bypassme': function(element, renderer){
                    return true
                  }
                }
                margins = {
                    top: 50,
                    left: 60,
                    width: 545
                  };
                pdf.fromHTML(
                    source // HTML string or DOM elem ref.
                    , margins.left // x coord
                    , margins.top // y coord
                    , {
                      'width': margins.width // max width of content on PDF
                      , 'elementHandlers': specialElementHandlers
                    },
                    function (dispose) {
                      // dispose: object with X, Y of the last line add to the PDF
                      //          this allow the insertion of new lines after html
                        pdf.save('BodynBrain.pdf');
                    }
                )
            }, 500);
        });
    }
})
