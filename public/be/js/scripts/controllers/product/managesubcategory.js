app.controller('ManageSubCategory', function($scope, $state, $q, $http, Config, $modal, Product, $anchorScroll, $timeout){
    var num = 10;
    var off = 1;
    $scope.keyword = null;
    $scope.alerts = [];

    var alerts = function(data) {
        $scope.closeAlert();
        $scope.alerts.push({ type: data.type, msg: data.msg });
    }

    $scope.closeAlert = function() {
        $scope.alerts.splice(0, 1);
    }

    var loadsubcategory = function(num, off, keyword) {
        Product.loadsubcategory(num, off, keyword, function(data) {
            $scope.subcategories = data.subcategory;
            $scope.maxSize = 10;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

            angular.forEach($scope.subcategories, function(value, key) {
                $scope.subcategories[key].type = '';
                angular.forEach(value['types'], function(value2, key2) {
                    if(value['types'].length != (key2 +1)){
                        data = ", ";
                    }else {
                        data = "";
                    }
                    $scope.subcategories[key].type = $scope.subcategories[key].type + value2['type'] + data;
                });
            });
        });
    }

    loadsubcategory(num, off, $scope.keyword);

    $scope.clear = function(){
        $scope.keyword=null;
        loadsubcategory(num, off, $scope.keyword);
    }

    $scope.search = function (keyword) {
        off = 1;
        $scope.keyword = keyword;
        loadsubcategory(num, off, $scope.keyword);
        $scope.searchtext='';
    }

    $scope.setPage = function(page) {
        loadsubcategory(num, page, $scope.keyword);
    }
    
    $scope.delete = function(id) {
        
        var modalInstance = $modal.open({
            templateUrl: 'deleteProductsubcategory.html',
            controller: function($scope, $modalInstance) {
                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }
                $scope.ok = function(){
                    Product.deleteSubcategory(id, function(data) {
                        alerts(data);
                        loadsubcategory(num, off, null);
                        $modalInstance.dismiss();
                    });
                }
            },
            resolve: {
                id : function() {
                    return id;
                }
            }
        });
    }

    $scope.addSubCategory = function(){
        var modalInstance = $modal.open({
            templateUrl: 'addProductSubCategory.html',
            controller: function($scope, $modalInstance){
                $scope.types = {}
                $scope.exist = false;

                Product.loadtype(function(data) {
                    $scope.types = data;
                });

                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }

                $scope.validation = function(data) {
                    if(data.length > 0){
                        $scope.required = false;
                    }else {
                        $scope.required = true;
                    }
                }

                $scope.ok = function(subcategory) {
                    if($scope.exist==false && subcategory.type != undefined){
                        Product.addSubCategory(subcategory, function(data){
                            alerts(data);
                            loadsubcategory(num, off, null);
                            $modalInstance.dismiss();
                        });
                    }
                }

                $scope.validateSubCategory = function(subcategory) {
                    Product.validateSubCategory(null, subcategory, function(data) {
                        $scope.exist = data.exist;
                    });
                }
            }
        });
    }

    var openedit = function(id) {
        var modalInstance = $modal.open({
            templateUrl: 'editProductsubcat2.html',
            controller: function($scope, $modalInstance) {
                $scope.types = {}
                Product.loadsubcatedit(id, function(data){
                    $scope.subcategory = data.subcategory;
                    $scope.types = data.type;
                });

                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }

                $scope.validation = function(data) {
                    if(data.length > 0){
                        $scope.required = false;
                    }else {
                        $scope.required = true;
                    }
                }

                $scope.validateSubCategory = function(id, subcategory) {
                    Product.validateSubCategory(id, subcategory, function(data) {
                        $scope.exist = data.exist;
                    });
                }

                $scope.ok = function(subcategory) {
                    Product.updateSubcategory(subcategory, function(data) {
                        alerts(data);
                        loadsubcategory(num, off, null);
                        $modalInstance.dismiss();
                    });
                }
            },
            resolve : {
                id: function() {
                    return id;
                }
            }
        });
    }

    $scope.edit = function(id){
        var modalInstance = $modal.open({
            templateUrl: 'editProductsubcat.html',
            controller: function($scope, $modalInstance){
                $scope.cancel = $modalInstance.dismiss();

                $scope.ok = function(){
                    $modalInstance.dismiss();
                    openedit(id);
                }

                $scope.validation = function(data) {
                    if(data.length > 0){
                        $scope.required = false;
                    }else {
                        $scope.required = true;
                    }
                }

                $scope.validateSubCategory = function(subcategory) {
                    Product.validateSubCategory(null, subcategory, function(data) {
                        $scope.exist = data.exist;
                    });
                }
            },
            resolve: {
                id : function(){
                    return id;
                }
            }
        });
    }
})