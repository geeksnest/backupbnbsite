app.controller('AddProduct', function($scope, $state, Upload,rfc4122,$q, $http, Config, $modal, $sce, $anchorScroll, $filter, Product, $anchorScroll, $timeout, $compile){
	$scope.invoice = {};
	$scope.inventory = [];
	$scope.selectedIndex = undefined;
	$scope.product = { id : rfc4122.v4(), tags: [], date : moment().format('YYYY-MM-DD'), images: [], type: [], category: [], subcategory: [], size:[]};
	$scope.alerts = [];
	$scope.amazonlink = Config.amazonlink;
    $scope.disable = false;
    $scope.exist = false;
    $scope.editslug = false;
    $scope.editedslug = false;
    $scope.exist2 = false;
    var slugstorage = "";
    $scope.stat = false;
    $scope.required = { type : false, description: false, category: false, subcategory: false, tags: false, images: false }

    $scope.dataPickerStates = {
      open1:false,
      open2:false,
      open3:false
    }

    $scope.today = moment().format('YYYY-MM-DD');
	$scope.closeAlert = function() {
		$scope.alerts = [];
	}

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.open = function($event, opened) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.dataPickerStates[opened] = true;
    };

    var loadaddproduct = function() {
    	Product.load(function(data) {
    		$scope.pcode = data.pcode;
    		$scope.tags = data.tags;
    		$scope.itemname = data.itemname;
    		$scope.types = data.type;
            $scope.categories = data.category;
            $scope.sizes = data.size;
    	});
    }

    loadaddproduct();

    $scope.$watch(function(){
        return $scope.product.price;
    }, function() {
        $scope.discount($scope.product.discount, 'percentage');
    });
    
    $scope.changeClass = function (options) {
        var widget = options.methods.widget();
        // remove default class, use bootstrap style
        widget.removeClass('ui-menu ui-corner-all ui-widget-content').addClass('dropdown-menu');
    };

    $scope.myOption = {
        options: {
            html: true,
            focusOpen: true,
            onlySelectValid: false,
            source: function (request, response) {
                var data = $scope.itemname;
                data = $scope.myOption.methods.filter(data, request.term);

                if (!data.length) {
                    data.push({
                        label: 'not found',
                        value: ''
                    });
                }
                response(data);
            }
        },
        methods: {}
    };

	$scope.complete = function(name) {
		Product.complete(name, function(data) {
			if(data.product.length > 0) {
				$scope.product = {
                    id: data.product[0].productid,
					code: data.product[0].productcode, 
					name: data.product[0].name,
					type: data.type,
					date : moment().format('YYYY-MM-DD'),
					tags : data.tags,
					shortdesc: data.product[0].shortdesc,
					description: data.product[0].description,
					category: data.category,
					color: data.product[0].color,
					size: data.size,
					quantity: undefined,
					price : data.product[0].price,
					discount: data.product[0].discount,
                    discount_from: data.product[0].discount_from,
                    discount_to : data.product[0].discount_to,
					discprice: data.product[0].price * (data.product[0].discount / 100),
					belowquantity : data.product[0].belowquantity,
					status: data.product[0].status==1 ? true : false,
					maxquantity: data.product[0].maxquantity,
					minquantity: data.product[0].minquantity,
					images: data.images,
                    slugs: data.product[0].slugs,
                    subcategory: data.subcategory
				};
                if($scope.product.discount_from <= $scope.today && $scope.today <= $scope.product.discount_to){
                    $scope.stat = true;
                }else {
                    $scope.stat = false;
                }
                $scope.disable = true;
                $scope.productname = data.product[0].name;
                $scope.productname2 = data.product[0].name;
                $scope.showexisting = false;
                CKEDITOR.instances['description'].setReadOnly(true);
                $scope.show = true;
                $scope.closeAlert();
                $scope.alerts.push({ type : 'info', msg: 'This product already exist please add quantity to add from quantity of existing product'})
                $scope.required = { type : false, description: false, category: false, tags: false, images: false }
			}else if($scope.disable==false){
                $scope.productname = $scope.productname2;
                if(name != null  && $scope.editedslug == false && $scope.editslug == false)
                {
                    var text1 = name.replace(/[^\w ]+/g,'');
                    $scope.product.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
                }
            }else {
                $scope.closeAlert();
                if($scope.product.name == undefined){
                    $scope.productname = undefined;
                    $scope.productname2 = undefined;
                    $scope.product = { id : rfc4122.v4(), tags: [], date : moment().format('YYYY-MM-DD'), images: [], type: [], category: [], subcategory: [], size:[]};
                    $scope.disable = false;
                    CKEDITOR.instances['description'].setReadOnly(false);
                }else {
                    $scope.productname2 = $scope.product.name;
                    $scope.showexisting = true;
                }
            }
		});

	}

	$scope.discount = function(data, type) {
		if($scope.product.price!=undefined && $scope.product.price!=0){
			if(type=="percentage" && data != undefined){
				var res = ($scope.product.price * (data / 100)).toFixed(2);
				$scope.product.discprice = res;
                $scope.product.discounted = $scope.product.price - res;
			}else if(type=="price" && data != undefined){
				var res = (data * 100) / $scope.product.price;
				$scope.product.discount = (res % 2) != 0 ? res.toFixed(2) : res;
                $scope.product.discounted = $scope.product.price - data;
			}
		}
	}

	$scope.addinvoice = function() {
		var modalInstance = $modal.open({
			templateUrl: 'addInvoice.html',
			controller: function($scope, $modalInstance, invoice) {
				$scope.inv = invoice;

				if(invoice.hasOwnProperty('number')){
					$scope.msg = "Remove";
				}else {
					$scope.msg = "Cancel";
				}
				$scope.ok = function(res) {
					$modalInstance.close(res);
				}
				$scope.cancel = function(){
					$scope.inv = [];
					$modalInstance.close($scope.inv);
				}
			},
			resolve: {
				invoice: function() {
					return $scope.invoice;
				}
			}
		}).result.then(function(res) {
			if(res){
				$scope.invoice = res;
			}
		});
	}

	$scope.setactive = function(index) {
		$scope.selectedIndex = index;
		$scope.product = $scope.inventory[$scope.selectedIndex];
	}

	$scope.addproduct = function() {
		$scope.selectedIndex = undefined;
        setpristine();
	}

	$scope.removeProduct = function() {
		var modalInstance = $modal.open({
			templateUrl: 'removeProduct.html',
			controller: function($scope, $modalInstance, index) {
				$scope.ok = function() {
					$modalInstance.close(index);
				}
				$scope.cancel = function() {
					$modalInstance.dismiss();
				}
			},
			resolve: {
				index: function() {
					return $scope.selectedIndex;
				}
			}
		}).result.then(function(res) {
			if(res!=undefined){
				$scope.inventory.splice($scope.selectedIndex, 1);
                $scope.product = { id : rfc4122.v4(), tags: [], date : moment().format('YYYY-MM-DD'), images: [], type: [], category: [], subcategory: [], size:[]};
				$scope.selectedIndex = undefined;
			}
		});
	}
	$scope.saveProduct = function(product){
		if($scope.selectedIndex==undefined){
            validate(function() {
                $scope.inventory.push(product);
                setpristine();
            });
            $scope.closeAlert();
            $scope.disable = false;
		}else {
			 $scope.inventory[$scope.selectedIndex] = product;
             $scope.addproduct();
		}
        $scope.productname=$scope.productname2;
        console.log($scope.inventory);
	}

    var validate = function(callback) {  
        if($scope.product.tags.length == 0){
            $scope.required.tags = true;
        }
        if($scope.product.category.length == 0) {
            $scope.required.category = true;
        }
        if($scope.product.subcategory.length == 0) {
            $scope.required.subcategory = true;
        }
        if($scope.product.type.length == 0){
            $scope.required.type = true;
        }
        if($scope.product.description == undefined) {
            $scope.required.description = true;
        }
        if($scope.product.images.length == 0){
            $scope.required.images = true;
        }
        $('input').trigger("blur");
        $('textarea').trigger("blur");
        if($scope.product.tags.length > 0 && $scope.product.category.length > 0 && $scope.product.type.length > 0 && $scope.product.description != undefined && $scope.product.images.length > 0 && $scope.product.name != undefined && $scope.product.code!=undefined && $scope.product.shortdesc != undefined && $scope.product.quantity!=undefined && $scope.product.price != undefined && $scope.product.belowquantity!=undefined && $scope.product.maxquantity != undefined && $scope.product.minquantity!=undefined){
            callback();
        }else {
            console.log('error');
            console.log($scope.product);
        }
    }

    var setpristine = function() {
        $timeout(function() {
            $scope.required = { type : false, description: false, category: false, tags: false, images: false }
            $scope.product = { id : rfc4122.v4(), tags: [], date : moment().format('YYYY-MM-DD'), images: [], type: [], category: [], subcategory: [], size:[] };
            $scope.formproduct.$setPristine();
            $scope.subcategories = [];
            $scope.types = [];
            $timeout(function() {
                $scope.formproduct.itemname.$error.required = false;
                $scope.formproduct.pcode.$error.required = false;
                $('.form-group.has-error').removeClass('has-error');
            }, 500);
        }, 500);
    }
	$scope.insertProducts = function() {
		var data = {};
		if($scope.invoice.hasOwnProperty('number')){
            if($scope.selectedIndex == undefined){
                $scope.inventory.push($scope.product);
            }
			data = {"product" : $scope.inventory, "invoice" : $scope.invoice };
            angular.forEach($scope.invetory, function(value, key) {
                $scope.invetory[key].discount_from = moment($scope.invetory[key].discount_from).format('YYYY-MM-DD');
                $scope.invetory[key].discount_to = moment($scope.invetory[key].discount_to).format('YYYY-MM-DD');
            });
		}else {
			data = {"product" : $scope.product, "invoice" : $scope.invoice };
            $scope.product.discount_from = moment($scope.product.discount_from).format('YYYY-MM-DD');
            $scope.product.discount_to = moment($scope.product.discount_to).format('YYYY-MM-DD');
		}
        validate(function() {
            Product.add(data, function(data) {
                $scope.invoice = {};
                $scope.inventory = [];
                $scope.closeAlert();
                $scope.alerts.push({type: data.type, msg : data.msg});
                $anchorScroll();
                $scope.productname=undefined;
                $scope.disable = false;
                $scope.showexisting = false;
                loadaddproduct();
                $scope.images = [];
                setpristine();
            });
        });
	}

	$scope.media = function(type){
        var modalInstance = $modal.open({
            templateUrl: 'mediagallery.html',
            controller: function($scope, $modalInstance, Config, type, productid) {
                $scope.imageloader=false;
                $scope.imagecontent=true;
                $scope.directory = 'productimage/' + productid;
                $scope.videoenabled=false;
                $scope.mediaGallery = 'images';
                $scope.invalidvideo = false;
                $scope.currentSelected = '';
                $scope.currentDeleting = '';
                $scope.type = type;
                $scope.s3link = Config.amazonlink;
                $scope.imagelength = 0;
                $scope.alertss = [];
                var selected = {};

                $scope.closeAlerts = function() {
                    $scope.alertss = [];
                }

                $scope.set = function(data){
                    selected = data;
                    $scope.currentSelected = data;
                }

                var returnYoutubeThumb = function(item){
                    var x= '';
                    var thumb = {};
                    if(item){
                        var newdata = item;
                        var x;
                        x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
                        thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
                        thumb['yid'] = x[1];
                        return thumb;
                    }else{
                        return x;
                    }
                }

                $scope.returnImageThumb = function(){
                    return function (item) {
                        if(item){
                            return Config.amazonlink + "/uploads/productimage/" + productid + "/" + item;
                        }else{
                            return item;
                        }
                    };
                }

                // $scope.$watch(function() {
                // 	return $scope.imagelength;
                // }, function() {
                // 	if($scope.imagelength >= 6){
                // 		$(".dragdropcenter").addClass('hide');
                // 	}else {
                // 		$(".dragdropcenter.hide").removeClass('hide');
                // 	}
                // });

                var loadimages = function() {
                    Product.loadimages(productid, function(data){
                        $scope.imagelist = data;
                        $scope.imagelength = data.length;
                        Product.images = data;
                    });
                }

                loadimages();

                $scope.$watch('files', function () {
                    $scope.upload($scope.files);

                });

                $scope.upload = function (files)
                {

                    var filename
                    var filecount = 0;
                    if (files && files.length)
                    {
                        $scope.imageloader=true;
                        $scope.imagecontent=false;
                        var i = 0;
                        for (i; i < files.length; i++)
                        {
                            var file = files[i];

                            if (file.size >= 2000000)
                            {
                                console.log('File ' + file.name + ' is too big');
                                $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                                filecount = filecount + 1;

                                if(filecount == files.length)
                                {
                                    $scope.imageloader=false;
                                    $scope.imagecontent=true;
                                }


                            }
                            else

                            {
                            	var count = $scope.imagelength + i + 1;
                            	if(count <= 6){
	                                var promises;

	                                promises = Upload.upload({

	                                    url: Config.amazonlink, //S3 upload url including bucket name
	                                    method: 'POST',
	                                    transformRequest: function (data, headersGetter) {
	                                        //Headers change here
	                                        var headers = headersGetter();
	                                        delete headers['Authorization'];
	                                        return data;
	                                    },
	                                    fields : {
	                                        key: 'uploads/productimage/' + productid+ '/' + file.name, // the key to store the file on S3, could be file name or customized
	                                        AWSAccessKeyId: Config.AWSAccessKeyId,
	                                        acl: 'private', // sets the access to the uploaded file in the bucket: private or public
	                                        policy: Config.policy, // base64-encoded json policy (see article below)
	                                        signature: Config.signature, // base64-encoded signature based on policy string (see article below)
	                                        "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
	                                    },
	                                    file: file
	                                })
	                                promises.then(function(data){

	                                    filecount = filecount + 1;
	                                    filename = data.config.file.name;
	                                    var fileout = {
	                                        'filename' : filename
	                                    };
	                                    Product.saveImage(productid, fileout, function(data){
	                                        loadimages();
	                                        if(filecount == files.length || count >= 6)
	                                        {
	                                            $scope.imageloader=false;
	                                            $scope.imagecontent=true;
	                                        }
	                                    });
	                                });
	                            }
                            }

                        }
                    }
                };

                $scope.deletenewsimg = function (dataimg, $event)
                {
                    var fileout = {
                        'filename' : dataimg
                    };
                    Product.deleteImage(productid, fileout, function(data) {
                        loadimages();
                    });
                    $event.stopPropagation();
                }

                $scope.ok = function() {
                    if(selected.hasOwnProperty('filename')){
                        Product.selectImage(selected, function(data) {
                            Product.images = data;
                            $timeout(function() {
                                $modalInstance.dismiss();
                            },500);
                        });
                    }else {
                        $modalInstance.close($scope.imagelist);
                    }
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                }

            },
            resolve: {
                type: function(){
                    return type;
                },
                productid: function() {
                	return $scope.product.id;
                }
            },
            windowClass: 'xxx-modal-window'
        }).result.finally(function() {
            $scope.product.images = Product.images;
            if(Product.images.length > 0){
                $scope.required.images = false;
            }
        });
    }


    $scope.reset = function() {
        setpristine();
        $scope.disable = false;
        $scope.closeAlert();
        CKEDITOR.instances['description'].setReadOnly(false);
    }

    $scope.yes = function() {
        $scope.disable = false;
        $scope.product.code = undefined;
        $scope.productname = $scope.productname2;
        $scope.product.id = rfc4122.v4();
        $scope.product.images = [];
        $scope.product.discount = undefined;
        $scope.product.discprice = undefined;
        $scope.product.discount_from = undefined;
        $scope.product.discount_to = undefined;
        $scope.product.discounted = undefined
        CKEDITOR.instances['description'].setReadOnly(false);
        convertSlug($scope.product.name);
        $timeout(function() {
            $scope.validation($scope.product.category, 'category');
            $timeout(function() {
                $scope.validation($scope.product.subcategory, 'subcategory');
            },500);
        }, 500);
    }

    $scope.no = function() {
        $scope.productname = $scope.productname2;
        $scope.disable = false;
        CKEDITOR.instances['description'].setReadOnly(false);
        $scope.product = { id : rfc4122.v4(), 
            name: $scope.product.name,  
            tags: [], 
            date : moment().format('YYYY-MM-DD'), 
            images: [], 
            type: [], 
            category: [],
            subcategory: [], 
            size:[]};
        convertSlug($scope.product.name);
    }    

    $scope.validatepcode = function(code, name) {
        Product.validatecode(code, name, function(data) {
            $scope.exist = data.exist;
        });
    }

    //slug
    var convertSlug = function(name) {
        if(name != null)
        {
            var text1 = name.replace(/[^\w ]+/g,'');
            $scope.product.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
        }
    }
    $scope.editprodslug = function(){
        $scope.editslug = true;
        slugstorage = $scope.product.slugs;
    }

    $scope.clearslug = function(name){
        if(name != null)
        {
            $scope.editedslug = false;
            $scope.product.slugs = name.replace(/\s+/g, '-').toLowerCase();
        }else {
            $scope.product.slugs = '';
            $scope.editedslug = false;
        }
    }

    $scope.setslug = function(slug){
        $scope.editedslug = true;
        $scope.editslug = false;
        if(slug != null)
        {
            $scope.product.slugs = slug.replace(/\s+/g, '-').toLowerCase();

        }
    }

    $scope.cancelpageslug = function(name) {
        if(name != null)
        {
            $scope.editedslug = false;
            $scope.product.slugs = slugstorage;
        }else {
            $scope.product.slugs = '';
            $scope.editedslug = false;
        }
        $scope.editslug = false;
    }

    $scope.onslugs = function(Text){
        if(Text != null)
        {
            $scope.product.slugs = Text.replace(/\s+/g, '-').toLowerCase();
            Product.validateSlugs($scope.product.id, $scope.product.slugs, function(data){
                $scope.exist2 = data.exist;
            });
        }
    }

    $scope.validation = function (data, type) {
        if(data == undefined || data.length==0){
            if(type=='type'){
                $scope.required.type = true;
            }else if(type=='description') {
                $scope.required.description = true;
            }else if(type=='category') {
                $scope.required.category = true;
                $scope.required.subcategory = true;
                $scope.required.type = true;
                $scope.subcategories = [];
                $scope.product.subcategory = [];
                $scope.product.type = [];
            }else if(type=='subcategory') {
                $scope.required.subcategory = true;
                $scope.required.type = true;
                $scope.types = [];
                $scope.product.type = [];
            }else if(type=='tags') {
                $scope.required.tags = true;
            }
        }else {
            var diff = [];
            if(type=='type'){
                $scope.required.type = false;
            }else if(type=='description') {
                $scope.required.description = false;
            }else if(type=='category') {
                $scope.required.category = false;
                Product.getsubcat({ category : data }, function(data) {
                    $scope.subcategories = data;
                    diff = $($scope.product.subcategory).not(data).get();
                });
            }else if(type=='subcategory') {
                $scope.required.subcategory = false;
                Product.gettypes({ subcategory : data }, function(data) {
                    $scope.types = data;
                    diff = $($scope.product.type).not(data).get();
                });
            }else if(type=='tags') {
                $scope.required.tags = false;
            }

            $timeout(function(){
                if(diff.length > 0){
                    diff.map(function(dif) {
                        if(type=='subcategory'){
                            $scope.product.type.splice( $.inArray(dif, $scope.product.type), 1 );
                        }else if(type=='category'){
                            $scope.product.subcategory.splice( $.inArray(dif, $scope.product.subcategory), 1 );

                            Product.gettypes({ subcategory : $scope.product.subcategory }, function(data) {
                                $scope.types = data;
                                diff2 = $($scope.product.type).not(data).get();
                                 $timeout(function(){
                                    if(diff2.length > 0){
                                        diff2.map(function(dif2) {
                                            $scope.product.type.splice( $.inArray(dif2, $scope.product.type), 1 );
                                        })
                                    }
                                }, 500);
                            });
                        }
                    });
                }
            }, 500);
        }
    }

    $scope.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
        }
    };  

    $scope.setimage = function(img) {
        Product.selectImage(img, function(data) {
            $scope.product.images = data;
        });
    }
})
