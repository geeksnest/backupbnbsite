
app.controller('UserListCtrl', function($http, $scope, $stateParams, $modal,$sce ,Config) {

	//LIST ALL USERS
	$scope.userid = userid;

	var num = 10;
	var off = 1;
	var keyword = null;

	var paginate = function (off, keyword) {
		$http({
			url: Config.ApiURL + "/user/list/" + num + '/' + off + '/' + keyword,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.data = data;
			$scope.maxSize = 5;
			$scope.bigTotalItems = data.total_items;
			console.log(data.total_items)
			$scope.bigCurrentPage = data.index;
		}).error(function (data, status, headers, config) {
			$scope.status = status;
		});
	}
	$scope.search = function (searchtext) {
		if(searchtext=='' || searchtext===null) {
			paginate(off, keyword);
		} else {
			paginate(off, searchtext);
		}
	}

	$scope.numpages = function (off, keyword) {
		paginate(off, keyword);
	}
	paginate(off, keyword);

	$scope.setPage = function (off) {
		paginate(off, keyword);
		// ManageCenterFactory.sample(num,pageNo, keyword, userid, function(data){
		// 	$scope.data = data;
		// 	$scope.maxSize = 5;
		// 	$scope.bigTotalItems = data.total_items;
		// 	$scope.bigCurrentPage = data.index;
		// });
	};
	//END USER LISTING


	//DELETE USER
	$scope.delete = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userDelete.html',
			controller: dltCTRL,
			resolve: {
				dltuser: function () {
					return user;
				}
			}
		});
	};
	$scope.alerts = [];
	$scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
	var alertme = function(){
		$scope.alerts.splice(0, 1);
		$scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});      
	}
	var dltCTRL = function ($scope, $modalInstance, dltuser) {
		$scope.dltuser = dltuser;

		$scope.ok = function (dltuser) {

			$http({
				url:Config.ApiURL + "/user/delete/"+dltuser,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				paginate(off, keyword);
				$modalInstance.dismiss('cancel');
				alertme();
				
			})
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
	//UPDATE USER
	$scope.update = function(user){
		var modalInstance = $modal.open({
			templateUrl: 'userUpdate.html',
			controller: updateCTRL,
			resolve: {
				update: function () {
					return user;
				}
			}
		});
	};
	var updateCTRL = function ($scope, $modalInstance, update, $state) {
		$scope.update = update;
		$scope.ok = function (update) {
			$state.go('updateuser', {userid:update});
			$modalInstance.dismiss('cancel');
			
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}

	$scope.setstatus = function (userid,userstatus) {
	   // $('#'+userid).show();
       $http({
			url:Config.ApiURL + "/user/setstatus/"+userid+"/"+userstatus,
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			console.log(data.result);
			// setTimeout(function() {
			//     $('#'+userid).fadeOut('fast');
			// }, 1000);
		})
    }

});
// app.controller('UserUpdateCtrl', function($http, $scope, $stateParams, $modal,$sce,Config) {
// 	$http({
// 		url: Config.ApiURL + "/user/info/"+$stateParams.userid,
// 		method: "GET",
// 		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
// 	}).success(function (data, status, headers, config) {
// 		$scope.user = data;
		
// 	})

// 	$scope.updateData = function(user){
// 		$scope.alerts = [];
// 		$scope.closeAlert = function(index) {
// 			$scope.alerts.splice(index, 1);
// 		};


// 		$http({
// 			url: Config.ApiURL +"/user/update",
// 			method: "POST",
// 			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
// 			data: $.param(user)
// 		}).success(function (data, status, headers, config) {
// 			msgmodal("User has been successfuly Updated");
			
// 		})
		
		
// 	};
// });




