'use strict';

app.controller('Editnewsletter', function($scope,$http, $modal, $state, $stateParams, Config ,Managenewsletter){

	console.log("JS SUCCESSFULY LOADED : Editnewsletter")

	 //Editnewsletter
	 Managenewsletter.editnewsletter($stateParams.id,function(data){
	 	$scope.news = data;
    $scope.amazonpath = data.image;
    $scope.defaulttit = data.title;
  });

     

	$scope.alerts = [];

	$scope.closeAlert = function (index) {
		$scope.alerts.splice(index, 1);
	};



	$scope.clearfields = function(){
		$scope.news = "";
		$scope.formpage.$setPristine();
	}

	var alertme = function(){
		$scope.alerts.splice(0, 1);
		$scope.alerts.push({type: 'success', msg: 'NEWS LETTER SUCCESSFULLY UPDATED!'});      
	}


	$scope.saveNews = function(newsletter){
		Managenewsletter.updatenewsletter(newsletter,function(data){
			if(data.error == "!SAVE")
			{
				$scope.savecheck = 'Oops! Something went wrong!Check your fields';
			}
			else{
				alertme();
			}
		});
	}

	$scope.onnewstitle = function(title){
		if($scope.defaulttit != title){
      Managenewsletter.checktitle(title,function(data){
        if(data>=1){
          $scope.invalidtitle = true;
        }else{
          $scope.invalidtitle = false;
        }
      });
    }else{
      $scope.invalidtitle = false;
    }
	}

  	// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////
  	$scope.showimageList = function(size,path){
  		var amazon = $scope.amazon;
  		var modalInstance = $modal.open({
  			templateUrl: 'newsletterimagelist.html',
  			controller: imagelistCTRL,
  			size: size,
  			resolve: {
  				path: function() {
  					return amazon
  				}
  			}

  		});
  	}
  	var pathimage = "";

  	var pathimages = function(){
  		$scope.amazonpath=pathimage;
  	}

  	var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
  		console.log(path);
  		$scope.amazonpath= path;
  		$scope.imageloader=false;
  		$scope.imagecontent=true;
  		$scope.noimage = false;
  		$scope.imggallery = false;

  		var loadimages = function() {
  			$http({
  				url: Config.ApiURL + "/newsletter/listimages",
  				method: "GET",
  				headers: {
  					'Content-Type': 'application/x-www-form-urlencoded'
  				}
  			}).success(function(data) {
  				if(data.error == "NOIMAGE" ){
  					$scope.imggallery=false;
  					$scope.noimage = true;
  				}else{
  					$scope.noimage = false;
  					$scope.imggallery=true;
  					$scope.imagelist = data;
  				}
  			}).error(function(data) {
  			});
  		}
  		loadimages();

  		$scope.path=function(path){
       var texttocut = Config.amazonlink + '/uploads/newsletter/';
       var newpath = path.substring(texttocut.length); 
       pathimage = newpath;
       pathimages();
       $modalInstance.dismiss('cancel');
     }



  		$scope.upload = function(files) {
  			$scope.upload(files);  
  		};

  		$scope.delete = function(id){
  			var modalInstance = $modal.open({
  				templateUrl: 'delete.html',
  				controller: deleteCTRL,
  				resolve: {
  					imgid: function() {
  						return id
  					}
  				}

  			});

  		}

  		var deleteCTRL = function($scope, $modalInstance, imgid) {

  			$scope.alerts = [];

  			$scope.closeAlert = function (index) {
  				$scope.alerts.splice(index, 1);
  			};

  			$scope.message="Are you sure do you want to delete this Photo?";
  			$scope.ok = function() {
  				$http({
  					url: Config.ApiURL+"/newsletter/delete/"+ imgid,
  					method: "get",
  					headers: {
  						'Content-Type': 'application/x-www-form-urlencoded'
  					},
  				}).success(function(data, status, headers, config) {
  					console.log(data);
  					$scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
  					loadimages();
  					$modalInstance.close();
  				}).error(function(data, status, headers, config) {
  					loadimages();
  					$modalInstance.close();
  					$scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
  				});
  			};

  			$scope.cancel = function() {
  				$modalInstance.dismiss('cancel');
  			};
  		}



  		$scope.upload = function (files) 
  		{
  			console.log(files);
  			var filename;
  			var filecount = 0;
  			if (files && files.length) 
  			{
  				$scope.imageloader=true;
  				$scope.imagecontent=false;

  				for (var i = 0; i < files.length; i++) 
  				{
  					var file = files[i];

  					if (file.size >= 2000000)
  					{
  						$scope.alerts.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
  						filecount = filecount + 1;

  						if(filecount == files.length)
  						{
  							$scope.imageloader=false;
  							$scope.imagecontent=true;
  						}
  					}
  					else
  					{
  						var promises;

  						var fileExtension = '.' + file.name.split('.').pop();

                  // rename the file with a sufficiently random value and add the file extension back
                  var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + i + fileExtension;

                  promises = Upload.upload({

                               url:Config.amazonlink, //S3 upload url including bucket name
                               method: 'POST',
                               transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                  key: 'uploads/newsletter/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                              },
                              file: file
                          })
				promises.then(function(data){

					filecount = filecount + 1;

					filename = data.config.file.name;
					var fileout = {
						'imgfilename' : renamedFile
					};
					$http({
						url: Config.ApiURL + "/newsletter/saveimage",
						method: "POST",
						headers: {'Content-Type': 'application/x-www-form-urlencoded'},
						data: $.param(fileout)
					}).success(function (data, status, headers, config) {
						loadimages();
						if(filecount == files.length)
						{
							$scope.imageloader=false;
							$scope.imagecontent=true;
						}

					}).error(function (data, status, headers, config) {
						$scope.imageloader=false;
						$scope.imagecontent=true;
					});

				});
				}



				}
				}
				};
				$scope.cancel = function() {
					$modalInstance.dismiss('cancel');

				};
				};
				// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////



  	//DATE
  	$scope.today = function() {
  		$scope.dt = new Date();
  	};
  	$scope.today();

  	$scope.clear = function () {
  		$scope.dt = null;
  	};

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
    	return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
    	$scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
    	$event.preventDefault();
    	$event.stopPropagation();

    	$scope.opened = true;
    };

    $scope.dateOptions = {
    	formatYear: 'yy',
    	startingDay: 1,
    	class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

});