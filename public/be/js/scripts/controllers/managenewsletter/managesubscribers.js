'use strict';

app.controller('Managesubscribers', function($scope,$http, $modal, $state, $stateParams, Config ,Managesubscribers){

	console.log("JS LOADED Managesubscribers")


	$scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    //{MODAL**
		var msgCTRL = function ($scope, $modalInstance, $state, message) {
			$scope.message = message;
			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
		}
		var msgmodal = function(msg){
			var message ="wew";
			var modalInstance = $modal.open({
				templateUrl: 'success.html',
				controller: msgCTRL,
				resolve: {
					message: function () {
						return msg;
					}
				}
			});
		}
    //**MODAL}//

    $scope.clearfields = function(){
    	$scope.subscriber = "";
    	$scope.invalidemail = false;
    }

   $scope.addsubscriber = function(subscriber){
   		Managesubscribers.addsubscriber(subscriber,function(data){
   			$scope.subscriber = "";
   			msgmodal("New Subscriber has been successfully Added");
   		});
   }
   $scope.checkemail = function(email){
   	 	Managesubscribers.checkemail(email,function(data){
   	 		if(data>=1){
   	 			$scope.invalidemail = true;
   	 		}else{
   	 			$scope.invalidemail = false;
   	 		}
   	 	});
   }

   // FACTORIZED PAGINATION
   var num = 10;
   var off = 1;
   var keyword = null;
   var loadlist = function(off,keyword){
   		Managesubscribers.loadlist(num,off,keyword,function(data){
   			$scope.data = data;
   			$scope.maxSize = 5;
			$scope.TotalItems = data.total_items;
			$scope.CurrentPage = data.index;
   		});
   }
   loadlist(off,keyword);


   $scope.search = function (searchkeyword) {
   	loadlist('1', searchkeyword);
   	keyword = searchkeyword;
   }

   $scope.clearsearch = function() {
		var off = 1;
		loadlist(off,null);
		angular.element('.searchtext').val("");
		keyword = null;
	}

	$scope.numpages = function (off, keyword) {
		loadlist(off, keyword);
	}

	$scope.setPage = function (pageNo) {
		loadlist(pageNo, keyword);
		off = pageNo;
	};

 // ;FACTORIZED PAGINATION

  var successloadalert = function(){
        $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'Subscriber successfully Deleted!' });
            
    }
 	// DELETE
	$scope.delete = function(id) {
		var modalInstance = $modal.open({
			templateUrl: 'deletesubscriber.html',
			controller: deletesubscriberCTRL,
			resolve: {
				id: function() {
					return id
				}
			}
		});
	}
	var deletesubscriberCTRL = function($scope, $modalInstance, id) {
		$scope.ok = function() {
			Managesubscribers.deletesubscriber(id, function(data){
				$modalInstance.dismiss('cancel');
				successloadalert();
				loadlist(off, keyword);
			});
		};
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};
	};
	// ;DELETE


});