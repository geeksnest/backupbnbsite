'use strict';

/* Controllers */

app.controller('allgroupclasssession2Ctrl', function($scope, $state, Upload ,$q, $http, Config, $stateParams,allgroupclasssessionFactory, $modal){

     $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    var pushAlert = function(data) {
        $scope.alerts = [];
        if(data.hasOwnProperty('success')){
            $scope.alerts.push({ type: 'success', msg: data.success });
        }else {
            $scope.alerts.push({ type: 'danger', msg: 'An error occurred please try again later.'});
        }
    };

    var num = 10;
    var off = 1;
    var keyword = null;

    $scope.search = function (searchkeyword) {
        keyword = searchkeyword;
        $scope.searchtext = undefined;
        allgroupclasssessionFactory.loadclassunverifiedlist(num,off, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }

    // $scope.numpages = function (off, keyword) {
    //     allgroupclasssessionFactory.loadclasslist(num,off, keyword, function(data){
    //         $scope.sessiondata = data.data;
    //         $scope.maxSize = 5;
    //         $scope.bigTotalItems = data.total_items;
    //         $scope.bigCurrentPage = data.index;

    //     })
    // }

    $scope.setPage = function (pageNo) {
        off = pageNo;
        allgroupclasssessionFactory.loadclassunverifiedlist(num,pageNo, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })
    };

    var loadnews = function(){
        allgroupclasssessionFactory.loadclassunverifiedlist(num,off, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }

    loadnews();

    $scope.viewsession = function(sessionid){
        var modalInstance = $modal.open({
            templateUrl: 'groupsessionView2.html',
            controller: groupsessionViewCTRL,
            resolve: {
                sessionid: function() {
                    return sessionid
                }
            }
        });
    }

    var groupsessionVerifyCtrl = function($scope, $modalInstance, sessionid) {
        $scope.cancel = function() {
            $modalInstance.dismiss();
        }

        $scope.change = function(confirmation) {
            if(confirmation != undefined){
                $scope.error = false;
            }else {
                $scope.error = true;
            }
        }

        $scope.ok = function(confirmation) {
            if(confirmation != undefined){
                var session = {
                  'sessionid':sessionid,
                  'confirmation':confirmation
                }
                allgroupclasssessionFactory.verifysession(session, function(data) {
                  console.log(data);
                  if(data.hasOwnProperty('success')){
                    $modalInstance.dismiss();
                    pushAlert(data)
                  }
                  else{
                    $scope.error = true;
                  }
                });
            }else {
                $scope.error = true;
            }
        }
    }

    var groupsessionViewCTRL = function($scope, $modalInstance, sessionid, $state) {
        allgroupclasssessionFactory.loadunverifiedgroupsession(sessionid, function(data){
            $scope.session = data;
            loadnews();
        })

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.verify = function() {
            var modalInstance = $modal.open({
                templateUrl: 'sessionVerify.html',
                controller: groupsessionVerifyCtrl,
                resolve: {
                    sessionid: function() {
                        return sessionid
                    }
                }
            }).result.then(function(data){

            });
            $modalInstance.dismiss();
        }
    }

})
