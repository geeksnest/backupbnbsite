'use strict';
'use strict';

/* Controllers */

app.controller('allintrosessioncanceledCtrl', function($scope, $state, Upload ,$q, $http, Config, $stateParams,allintrosessionFactory, $modal){

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    var pushAlert = function(data) {
        $scope.alerts = [];
        if(data.hasOwnProperty('success')){
            $scope.alerts.push({ type: 'success', msg: data.success });
        }else {
            $scope.alerts.push({ type: 'danger', msg: 'An error occurred please try again later.'});
            console.log(data);
        }
    }

    var num = 10;
    var off = 1;
    var keyword = null;

    $scope.search = function (searchkeyword) {
        keyword = searchkeyword;
        $scope.searchtext = undefined;
        allintrosessionFactory.loadcanceledclasslist(num,off, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }
    
    // $scope.numpages = function (off, keyword) {
    //     allintrosessionFactory.loadclasslist(num,off, keyword, function(data){
    //         $scope.sessiondata = data.data;
    //         $scope.maxSize = 5;
    //         $scope.bigTotalItems = data.total_items;
    //         $scope.bigCurrentPage = data.index;

    //     })
    // }

    $scope.setPage = function (pageNo) {
        off = pageNo;
        allintrosessionFactory.loadcanceledclasslist(num,pageNo, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })
    };
    
    var loadsession = function(){
        allintrosessionFactory.loadcanceledclasslist(num,off, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }

    loadsession();

    $scope.viewsession = function(sessionid){
        var modalInstance = $modal.open({
            templateUrl: 'groupsessioncanceledView.html',
            controller: sessionViewCTRL,
            resolve: {
                sessionid: function() {
                    return sessionid
                }
            }
        });
    }

    var sessionVerifyCtrl = function($scope, $modalInstance, sessionid) {
        $scope.cancel = function() {
            $modalInstance.dismiss();
        }

        $scope.change = function(confirmation) {
            if(confirmation != undefined){
                $scope.error = false;
            }else {
                $scope.error = true;
            }
        }
        
        $scope.ok = function(confirmation) {
            if(confirmation != undefined){
                allintrosessionFactory.verifysession(sessionid, { conf: confirmation }, function(data) {
                    $modalInstance.close(data);
                });
            }else {
                $scope.error = true;
            }

        }
    }
    
    var sessionViewCTRL = function($scope, $modalInstance, sessionid) {
        allintrosessionFactory.loadcanceledsession(sessionid, function(data){
            $scope.session = data;
            loadsession();
        })

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.verify = function() {
            var modalInstance = $modal.open({
                templateUrl: 'sessionVerify.html',
                controller: sessionVerifyCtrl,
                resolve: {
                    sessionid: function() {
                        return sessionid
                    }
                }
            }).result.then(function(data){
                if(data){
                    pushAlert(data);
                    if(data.hasOwnProperty('success')){
                        loadsession();
                    }
                }
            });
            $modalInstance.dismiss();
        };
    }

})