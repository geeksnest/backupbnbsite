app.controller('mycenterCtrl', function($scope, Config, MycenterFactory, Factory, jwtHelper, store, $stateParams){
  'use strict';

  var ctr_id = $stateParams.centerid;
  var ctr_mgr_id = jwtHelper.decodeToken(store.get('AccessToken')).id;

  function FnLoadMyCenter() {
    MycenterFactory.mycenter(ctr_id, ctr_mgr_id, function(data){
      if(!data.hasOwnProperty("err")) {

        $scope.center = data.mycenter;

        if(data.regions.length > 0) {
          $scope.regions = data.regions;
          $scope.hideregion = false;
        } else { $scope.hideregion = true; }

        $scope.centerstate = data.states;
        $scope.centercity = data.cities;
        $scope.getcenterzip = data.zips;

        if($scope.center.openingdate == "0000-00-00") {
          $scope.center.openingdate = undefined;
        }

        $scope.center.centerslugs = [{ "slug" : data.mycenter.centerslugs }];

        if(data.mycenter.centerslugs2 !== "" && data.mycenter.centerslugs2 !== null) {
          $scope.center.centerslugs.push({ "slug" : data.mycenter.centerslugs2});
        }
        if(data.mycenter.centerslugs3 !== "" && data.mycenter.centerslugs3 !== null) {
          $scope.center.centerslugs.push({ "slug" : data.mycenter.centerslugs3});
        }

      } else {
        Factory.toaster("error","Error","Something went wrong" + data.err);
      }
    });
  }
  FnLoadMyCenter();

  $scope.saveMyCenter = function(center) {
    $scope.isSaving = true;
    if(center.centerslugs.length === 0 ) {
        Factory.toaster("warning","Warning","Please check your Center SEO");
        $scope.isSaving = false;
    } else {
      if (center.openingdate == undefined) { center.openingdate = new Date(center.openingdate); }
      else {  center.openingdate = new Date(center.openingdate); }

      MycenterFactory.savecenter(center,function(data){
          if(data.hasOwnProperty("scs")) {
              Factory.toaster("success","Success", data.scs);
          } else if (data.hasOwnProperty("err")) {
              Factory.toaster("error","Error", data.err);
          }
          FnLoadMyCenter();
          $scope.isSaving = false;
      });
    }
  };

  $scope.statechange = function(statecode) {
      MycenterFactory.loadcity(statecode,function(data){
        $scope.centercity = data;
        $scope.center.centercity=data[0].city;

        MycenterFactory.loadzip(data[0].city,function(data){
            $scope.getcenterzip = data;
            $scope.center.centerzip =data[0].zip;
        });
      });
  };

  $scope.citychange = function(cityname) {
      MycenterFactory.loadzip(cityname,function(data){
          $scope.getcenterzip = data;
          $scope.center.centerzip =data[0].zip;
      });
  };

  $scope.authorizehelp = function() {
    Factory.modaL("authorizeHelp.html","md",
      function($scope, $modalInstance) {
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
      }
    );
  };

  $scope.paypalhelp = function() {
    Factory.modaL("paypalHelp.html","md",
      function($scope, $modalInstance) {
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
      }
    );
  };

  //DATE PICKER
  $scope.today = function () {
  $scope.dt = new Date();
  };

  $scope.today();
  $scope.clear = function () {
  $scope.dt = null;

  };

  $scope.toggleMin = function () {
      $scope.minDate = $scope.minDate ? null : new Date();
  };

  $scope.toggleMin();

  $scope.open = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = true;
  };

  $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      class: 'datepicker'
  };

  $scope.initDate = new Date('2016-15-20');
  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];

  $scope.onslug = function() {
    if($scope.center.centerslugs.length > 3) {
      $scope.center.centerslugs.splice(($scope.center.centerslugs.length-1), 1);
    }
  };

});
