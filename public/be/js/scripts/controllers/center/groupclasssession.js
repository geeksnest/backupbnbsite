'use strict';

/* Controllers */

app.controller('groupclasssessionCtrl', function($scope, $state, Upload ,$q, $http, Config, $stateParams,groupclasssessionFactory, $modal){

     $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    var num = 10;
    var off = 1;
    var keyword = null;

    $scope.search = function (searchkeyword) {
         keyword = searchkeyword;
        groupclasssessionFactory.loadclasslist(num,off, keyword,$stateParams.centerid, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }
    
    $scope.numpages = function (off, keyword) {
        groupclasssessionFactory.loadclasslist(num,off, keyword,$stateParams.centerid, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })
    }

    $scope.setPage = function (pageNo) {
         off = pageNo;
        groupclasssessionFactory.loadclasslist(num,pageNo, keyword,$stateParams.centerid, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })
    };
    
    var loadnews = function(){
        groupclasssessionFactory.loadclasslist(num,off, keyword,$stateParams.centerid, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }

    loadnews();

    $scope.viewsession = function(sessionid){
        var modalInstance = $modal.open({
            templateUrl: 'groupsessionView.html',
            controller: groupsessionViewCTRL,
            resolve: {
                sessionid: function() {
                    return sessionid
                }
            }
        });
    }

    var groupsessionViewCTRL = function($scope, $modalInstance, sessionid, $state) {
        groupclasssessionFactory.loadgroupsession(sessionid, $stateParams.userid, function(data){
            $scope.session = data;
            loadnews();
        })

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

})