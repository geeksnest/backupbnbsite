'use strict';

/* Controllers */

app.controller('successCtrl', function ($scope, $state, $modal, Config, $stateParams, ManageStoryFactory){
	
	var viewStoryCTRL = function($scope, $modalInstance, storyid, $state) {

        $scope.storyid = storyid;
        $scope.ok = function(storyid) {
            $scope.storyid = storyid;
            $state.go('viewstory', {storyid: storyid });
                $modalInstance.dismiss('cancel');
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }

	$scope.viewStory = function(storyid) {

        var modalInstance = $modal.open({
            templateUrl: 'viewStory.html',
            controller: viewStoryCTRL,
            resolve: {
                storyid: function() {
                    return storyid
                }
            }
        });
    }

    $scope.deleteStory = function(storyid) {
        var modalInstance = $modal.open({
            templateUrl: 'deleteStory.html',
            controller: deleteStoryCTRL,
            resolve: {
                storyid: function() {
                    return storyid
                }
            }
        });
    }

    var deleteStoryCTRL = function($scope, $modalInstance, storyid, $state) {
      
        $scope.storyid = storyid;
        $scope.ok = function(storyid) {
            ManageStoryFactory.deleteStory(storyid, function (data){
                paginate(off, keyword);
                $modalInstance.dismiss('cancel');
            })
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

	$scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
	
	var pagination = function(off, keyword) {
		ManageStoryFactory.storyPerCenter($stateParams.centerid, num, off, keyword, function (data) {
		$scope.stories = data;
        $scope.maxSize = 5;
        $scope.TotalItems = data.total_items;
        $scope.CurrentPage = data.index;
		})
	}

	pagination(off, keyword);

    $scope.search = function (searchkeyword) {
        var off = 1;
        pagination(off, searchkeyword);
        keyword = searchkeyword;
    }

    $scope.clearsearch = function() {
    	var off = 1;
    	pagination(off,null);
    	$scope.searchtext = '';
    	keyword = null;
    }
    
    $scope.numpages = function (off, keyword) {
        pagination(off, keyword);
    }

    $scope.setPage = function (pageNo) {
        pagination(pageNo, keyword);
        off = pageNo;
    };

	

	

})