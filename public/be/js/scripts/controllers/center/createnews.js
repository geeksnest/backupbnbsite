app.controller('CreatenewsCtrl', function(moment, $scope, $state, $modal, Upload ,$q, $http, Config, Createnews, $stateParams,anchorSmoothScroll, $localStorage, $window, Factory){
  'use strict';
    $scope.imageloader=false;
    $scope.imagecontent=true;

    // $scope.news = {
    //   title: "",
    //   slugs: "",
    //   author: "",
    //   date: "",
    //   newslocation: "",
    //   category: "",
    //   body: "",
    //   status: 1,
    //   featured: 0,
    //   banner: ""
    // };
    //Editing Slugs
    $scope.editslug = false;
    $scope.editnewsslug = function(){
        $scope.editslug = true;
    };
    $scope.cancelnewsslug = function(title){
        $scope.editslug = false;
        $scope.onnewstitle(title);
    };
    $scope.setslug = function(){
        $scope.editslug = false;
    };
    $scope.clearslug = function(title){
        $scope.editslug = false;
        $scope.onnewstitle(title);
    };
    $scope.onslugs = function(text1){
        $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
        validateSlugs();
    };

    $scope.SEO = function(title) {
        $scope.news.slugs = Factory.SEO(title);
    };
    // End Editing Slugs

    var orinews = angular.copy($scope.news);

    var validateSlugs = function(){
        // I will change this to validate if duplicate slugs
                $http({
                    url: Config.ApiURL + "/newsslugs/validate/"+ $scope.news.slugs + "/" + $stateParams.centerid,
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).success(function(data) {
                    if(data>=1){
                        $scope.invalidtitle = true;
                        $scope.formpage.$invalid = true;
                    }else{
                        $scope.invalidtitle = false;
                    }
                });
    };
    $scope.onnewstitle = function convertToSlug(Text)
    {

            if(Text === null)
            {

            }
            else
            {
                var text1 = Text.replace(/[^\w ]+/g,'');
                $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
                validateSlugs();
            }
    };

    $scope.onnewsdate = function convertToSlug(date)
    {
        console.log(date);
        $scope.news.datevalid = date;
    };

    $scope.cutlink = function convertToSlug(Text)
    {
        var texttocut = Config.amazonlink + '/uploads/newsimage/';
        $scope.news.banner = Text.substring(texttocut.length);
    };

    Createnews.loadcategory(function(data){
         $scope.category = data;
         $scope.news.category = data[0];

    });

    Createnews.loadcenter(function(data){
         $scope.newslocation = data;

    });

    console.log($stateParams.centerid);

    $scope.saveNews = function(news) {
      if(news.hasOwnProperty("body")) {
        $scope.isSaving = true;

            news['newslocation'] = $stateParams.centerid;
            news['centers'] = [{centerid:$stateParams.centerid}];

        Createnews.newcenternews(news, function(data){

            if(data.hasOwnProperty("suc")) {

              Factory.toaster("success","Success",data.suc);

              $scope.isSaving = false;
              $scope.amazonpath ="";
              $scope.news = angular.copy({date:Factory.datenow(), status:1}, $scope.news);

              Createnews.loadcategory(function(data){
               $scope.category = data;
               $scope.news.category = data[0];
              });

            } else if(data.hasOwnProperty("err")) {
              Factory.toaster("error","Error",data.err);
            }
        });
      } else {
          Factory.toaster("warning","Warning","Body Content is required!");
      }
            // $scope.alerts = [];
            // $scope.closeAlert = function (index) {
            //     $scope.alerts.splice(index, 1);
            // };
            //
            // console.log(news.datevalid);
            // if(news.body == undefined || news.body ==  ""){
            //     $scope.bodyvalidate = true;
            //     anchorSmoothScroll.scrollTo('bodyvalidate');
            //
            //     $scope.alerts.push({type: 'danger', msg: 'Body Contents is required!'});
            // }
            // else if(news.date == undefined ||news.date == ""){
            //     $scope.datevalidate = true;
            //     anchorSmoothScroll.scrollTo('datevalidate');
            //     $scope.alerts.push({type: 'danger', msg: 'Date is required!'});
            // }
            // else{
            //
            //     var centers = [$stateParams.centerid];
            //     news['newslocation'] = $stateParams.centerid;
            //     news['centers'] = centers;
            //     $scope.alerts = [];
            //
            //     $scope.closeAlert = function (index) {
            //         $scope.alerts.splice(index, 1);
            //     };
            //
            //     $scope.isSaving = true;
            //
            //     $http({
            //         url: Config.ApiURL + "/newscenter/create",
            //         method: "POST",
            //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            //         data: $.param(news)
            //     }).success(function (data, status, headers, config) {
            //         $scope.isSaving = false;
            //         $scope.alerts.push({type: 'success', msg: 'News successfully saved!'});
            //         $scope.news = angular.copy(orinews);
            //         $scope.formpage.$setPristine();
            //         anchorSmoothScroll.scrollTo('top');
            //         $scope.datevalidate = false;
            //         $scope.bodyvalidate = false;
            //     }).error(function (data, status, headers, config) {
            //         scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
            //         anchorSmoothScroll.scrollTo('top');
            //     });
            //
            //     Createnews.loadcategory(function(data){
            //      $scope.category = data;
            //      $scope.news.category = data[0];
            //
            //     });
            // }
    };

    $scope.previewNews = function(news)
    {
        // console.log(news.author)
        // // $state.go('/bnb-buzz/preview', {news: news });
        // $http({
        //     method: "post",
        //     url: Config.BaseURL + "/bnb-buzz/preview",
        //     data: {
        //         title: news.title,
        //         author: news.author
        //     },
        //     headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        // }).success(function(){
            $localStorage.newprev = news;
            console.log($localStorage.newprev);
        // });
        // $window.location.href = '/bnb-buzz/preview/&'+$.param(news);
        $window.open('/centernews/newspreview', 'bar');
        // $window.location.href = '/bnb-buzz/preview/&'+$.param(news);
        // console.log(news);
    };

    // loadimages();

    $scope.$watch('files', function () {
        $scope.upload($scope.files);
    });

    $scope.alertss = [];

        $scope.closeAlerts = function (index) {
            $scope.alertss.splice(index, 1);
        };

    $scope.upload = function (files)
    {
        var filename
        var filecount = 0;
        if (files && files.length)
        {
            $scope.imageloader=true;
            $scope.imagecontent=false;

            for (var i = 0; i < files.length; i++)
            {
                var file = files[i];

                    if (file.size >= 2000000)
                    {
                        $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
                        filecount = filecount + 1;

                        if(filecount == files.length)
                            {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            }
                    }
                    else
                    {
                        var promises;
                             // console.log(file.name);
                            promises = Upload.upload({

                                url: Config.amazonlink, //S3 upload url including bucket name
                                method: 'POST',
                                transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                                },
                                fields : {
                                  key: 'uploads/newsimage/' + file.name, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                },
                                file: file
                            });
                                promises.then(function(data){

                                    filecount = filecount + 1;
                                    filename = data.config.file.name;

                                    // console.log(filename);
                                    var fileout = {
                                        'imgfilename' : filename
                                    };
                                    $http({
                                        url: Config.ApiURL + "/news/saveimage",
                                        method: "POST",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        data: $.param(fileout)
                                    }).success(function (data, status, headers, config) {
                                        loadimages();
                                        if(filecount == files.length)
                                        {
                                            $scope.imageloader=false;
                                            $scope.imagecontent=true;
                                        }

                                    }).error(function (data, status, headers, config) {
                                            $scope.imageloader=false;
                                            $scope.imagecontent=true;
                                    });

                                });
                    }
            }
        }


    };

        // UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////
    $scope.showimageList = function(size,path){
        var amazon = $scope.amazon;
        $scope.type = path;
        var modalInstance = $modal.open({
            templateUrl: 'newsimagelist.html',
            controller: imagelistCTRL,
            size: size,
            resolve: {
                path: function() {
                    return amazon;
                }
            }

        });
    };

    $scope.stat="1";
    var pathimage = "";

    var pathimages = function(){
      if($scope.type == 'banner'){
          $scope.amazonpath=pathimage;
      } else {
          $scope.amazonpath2=pathimage;
      }
    };

    var imagelistCTRL = function($modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path) {
        $scope.amazonpath= path;
        $scope.imageloader=false;
        $scope.imagecontent=true;
        $scope.noimage = false;
        $scope.imggallery = false;

        var loadimages = function() {
            $http({
                url: Config.ApiURL + "/news/listimages",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                if(data.error == "NOIMAGE" ){
                    $scope.imggallery=false;
                    $scope.noimage = true;
                }else{
                    $scope.noimage = false;
                    $scope.imggallery=true;
                    $scope.imagelist = data;
                }
            }).error(function(data) {
            });
        };
        loadimages();

        $scope.path=function(path){
           var texttocut = Config.amazonlink + '/uploads/newsimage/';
            var newpath = path.substring(texttocut.length);
            pathimage = newpath;
            pathimages();
            $modalInstance.dismiss('cancel');
        };



        $scope.upload = function(files) {
            $scope.upload(files);
        };

        $scope.delete = function(id){
            var modalInstance = $modal.open({
                templateUrl: 'delete.html',
                controller: deleteCTRL,
                resolve: {
                    imgid: function() {
                        return id;
                    }
                }
            });
        };

        var deleteCTRL = function($scope, $modalInstance, imgid) {

            $scope.alerts = [];

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.message="Are you sure do you want to delete this Photo?";
            $scope.ok = function() {
                $http({
                    url: Config.ApiURL+"/news/deletenewsimg/"+ imgid,
                    method: "get",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).success(function(data, status, headers, config) {
                    $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
                    loadimages();
                    $modalInstance.close();
                }).error(function(data, status, headers, config) {
                    loadimages();
                    $modalInstance.close();
                    $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
                });
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        };

        $scope.closeAlert_image = function (index) {
            $scope.imagealert.splice(index, 1);
        };

        $scope.upload = function (files)
        {
            $scope.imagealert = [];
            var filename;
            var filecount = 0;
            if (files && files.length)
            {

                $scope.imageloader=true;
                $scope.imagecontent=false;

                for (var i = 0; i < files.length; i++)
                {
                    var file = files[i];

                    if (file.size >= 6000000) //6MB
                    {
                        $scope.imagealert.push({type: 'danger', msg: 'File ' + file.name + ' is too big. (Maximum of 6MB)'});
                        $scope.imageloader=false;
                        $scope.imagecontent=true;
                    }
                    else
                    {
                        var promises;

                        var fileExtension = '.' + file.name.split('.').pop();

                  // rename the file with a sufficiently random value and add the file extension back
                  var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + i + fileExtension;

                  promises = Upload.upload({

                               url:Config.amazonlink, //S3 upload url including bucket name
                               method: 'POST',
                               transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                  key: 'uploads/newsimage/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                              },
                              file: file
                          })
promises.then(function(data){

    filecount = filecount + 1;

    filename = data.config.file.name;
    var fileout = {
        'imgfilename' : renamedFile
    };
    $http({
        url: Config.ApiURL + "/news/saveimage",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(fileout)
    }).success(function (data, status, headers, config) {
        loadimages();
        if(filecount == files.length)
        {
            $scope.imageloader=false;
            $scope.imagecontent=true;
        }

    }).error(function (data, status, headers, config) {
        $scope.imageloader=false;
        $scope.imagecontent=true;
    });

});
}



}
}
};
$scope.cancel = function() {
    $modalInstance.dismiss('cancel');

};
};

    $scope.imagegallery = function(size,path){
        var amazon = Config.amazonlink;
        var modalInstance = $modal.open({
            templateUrl: 'newsimagelist2.html',
            controller: imagegalleryCTRL,
            size: size,
            resolve: {
                path: function() {
                    return amazon;
                }
            }
        });
    };

    var imagegalleryCTRL = function($modalInstance, $scope, path) {
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.amazonpath= path;
        $scope.imageloader=false;
        $scope.imagecontent=true;
        $scope.noimage = false;
        $scope.imggallery = false;

        var loadimages = function() {
            $http({
                url: Config.ApiURL + "/news/listimages",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                if(data.error == "NOIMAGE" ){
                    $scope.imggallery=false;
                    $scope.noimage = true;
                } else {
                    $scope.noimage = false;
                    $scope.imggallery=true;
                    $scope.imagelist = data;
                }
            }).error(function(data) {
            });
        };
        loadimages();

        $scope.getimageurl = function(filename) {

            var modalInstance = $modal.open({
                templateUrl: 'imagesrc.html',
                controller: imagesrcCTRL,
                resolve: {
                    imagesource: function() {
                        return path + "/uploads/newsimage/" + filename;
                    }
                }
            });
        };

        var imagesrcCTRL = function($scope, imagesource, $modalInstance) {
            $scope.imgsrc = imagesource;
            $scope.ok = function() {
                $modalInstance.dismiss('cancel');
            };
        };

        $scope.upload = function (files)
        {
            $scope.imagealert = [];
            var filename;
            var filecount = 0;
            if (files && files.length)
            {

                $scope.imageloader=true;
                $scope.imagecontent=false;

                for (var i = 0; i < files.length; i++)
                {
                    var file = files[i];

                    if (file.size >= 6000000) //6MB
                    {
                        $scope.imagealert.push({type: 'danger', msg: 'File ' + file.name + ' is too big. (Maximum of 6MB)'});
                        $scope.imageloader=false;
                        $scope.imagecontent=true;
                    }
                    else
                    {
                        var promises;

                        var fileExtension = '.' + file.name.split('.').pop();

                  // rename the file with a sufficiently random value and add the file extension back
                  var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + i + fileExtension;

                  promises = Upload.upload({

                               url:Config.amazonlink, //S3 upload url including bucket name
                               method: 'POST',
                               transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                  key: 'uploads/newsimage/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                              },
                              file: file
                          });

                          promises.then(function(data){

                            filecount = filecount + 1;

                            filename = data.config.file.name;
                            var fileout = {
                                'imgfilename' : renamedFile
                            };
                            $http({
                                url: Config.ApiURL + "/news/saveimage",
                                method: "POST",
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                data: $.param(fileout)
                            }).success(function (data, status, headers, config) {
                                loadimages();
                                if(filecount == files.length)
                                {
                                    $scope.imageloader=false;
                                    $scope.imagecontent=true;
                                }

                            }).error(function (data, status, headers, config) {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            });

                        });
                        }



                        }
                        }
                        };

        //DELETE IMAGE INSIDE IMAGE GALLERY (MODAL)
        $scope.delete = function(id){
            var modalInstance = $modal.open({
                templateUrl: 'delete.html',
                controller: deleteCTRL,
                resolve: {
                    imgid: function() {
                        return id;
                    }
                }
            });
        };

        var deleteCTRL = function($scope, $modalInstance, imgid) {
            $scope.alerts = [];
            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.message="Are you sure do you want to delete this Photo?";
            $scope.ok = function() {
                $http({
                    url: Config.ApiURL+"/news/deletenewsimg/"+ imgid,
                    method: "get",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).success(function(data, status, headers, config) {
                    console.log(data);
                    $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
                    loadimages();
                    $modalInstance.close();
                }).error(function(data, status, headers, config) {
                    loadimages();
                    $modalInstance.close();
                    $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
                });
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        };
    };

    //DATE PICKER
    $scope.today = function () {
        $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;

    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.deletenewsimg = function (id)
    {
        // var fileout = {
        //     'imgfilename' : dataimg
        // };

        $http({
            url: Config.ApiURL + "/news/deletenewsimg/"+id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            // data: $.param(fileout)
        }).success(function (data, status, headers, config) {
            loadimages();
        }).error(function (data, status, headers, config) {
            loadimages();
        });

    };
});
