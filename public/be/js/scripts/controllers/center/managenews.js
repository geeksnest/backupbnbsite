'use strict';

/* Controllers */

app.controller('managenewsCtrl', function ($scope, $state, $q,$stateParams, Config, managenewsFactory, $http, $modal){

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

	var num = 10;
    var off = 1;
    var keyword = null;

    $scope.search = function (keyword) {
        managenewsFactory.newslist(num,off, keyword,$stateParams.centerid, function(data){
			$scope.data = data;
	        $scope.maxSize = 5;
	        $scope.bigTotalItems = data.total_items;
	        $scope.bigCurrentPage = data.index;

		})

    }
    
    $scope.numpages = function (off, keyword) {
       	managenewsFactory.newslist(num,off, keyword,$stateParams.centerid, function(data){
			$scope.data = data;
	        $scope.maxSize = 5;
	        $scope.bigTotalItems = data.total_items;
	        $scope.bigCurrentPage = data.index;

		})
    }

    $scope.setPage = function (pageNo) {
	    managenewsFactory.newslist(num,pageNo, keyword,$stateParams.centerid, function(data){
			$scope.data = data;
	        $scope.maxSize = 5;
	        $scope.bigTotalItems = data.total_items;
	        $scope.bigCurrentPage = data.index;

		})
    };
	
    var loadnews = function(){
        managenewsFactory.newslist(num,off, keyword,$stateParams.centerid, function(data){
            $scope.data = data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }

    loadnews();
	



	$scope.setstatus = function (status,newsid,keyword,newslocation,newsslugs) {
        
            if(status == 1)
            {
                $http({
                        url: Config.ApiURL + "/news/updatenewscenterstatus/0/" + newsid + '/' + keyword,
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {
                     
                        
                        $scope.currentstatusshow = newsslugs;
                        var i = 2;
                        setInterval(function(){
                            i--;

                            if(i == 0)
                            {
                                 managenewsFactory.newslist(num,off, keyword,$stateParams.centerid, function(data){
									// console.log(data);
									$scope.data = data;
							        $scope.maxSize = 5;
							        $scope.bigTotalItems = data.total_items;
							        $scope.bigCurrentPage = data.index;

								})
                            }


                        },1000)
                        
                    }).error(function (data, status, headers, config) {
                         	managenewsFactory.newslist(num,off, keyword,$stateParams.centerid, function(data){
									$scope.data = data;
							        $scope.maxSize = 5;
							        $scope.bigTotalItems = data.total_items;
							        $scope.bigCurrentPage = data.index;

							})
                        
                    });
            }
            else
            {
                 $http({
                        url: Config.ApiURL + "/news/updatenewscenterstatus/1/" + newsid + '/' + keyword,
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {
                     

                        $scope.currentstatusshow = newsslugs;
                        var i = 2;
                        setInterval(function(){
                            i--;
                            if(i == 0)
                            {
                                managenewsFactory.newslist(num,off, keyword,$stateParams.centerid, function(data){
									$scope.data = data;
							        $scope.maxSize = 5;
							        $scope.bigTotalItems = data.total_items;
							        $scope.bigCurrentPage = data.index;

								})
                                $scope.currentstatusshow = 0;
                            }


                        },1000)
                    }).error(function (data, status, headers, config) {
                       managenewsFactory.newslist(num,off, keyword,$stateParams.centerid, function(data){
									$scope.data = data;
							        $scope.maxSize = 5;
							        $scope.bigTotalItems = data.total_items;
							        $scope.bigCurrentPage = data.index;

						})
                    });
            }
        
    }

    var successloadalert = function(){
            $scope.alerts[0] = { type: 'success', msg: 'News successfully Deleted!' };
            
    }

    var errorloadalert = function(){
            $scope.alerts[0] = { type: 'danger', msg: 'Something went wrong News not Deleted!' };
    }


    $scope.deletenewscenter = function(newsid) {
        var modalInstance = $modal.open({
            templateUrl: 'newsCenterDelete.html',
            controller: newsCenterDeleteCTRL,
            resolve: {
                newsid: function() {
                    return newsid
                }
            }
        });
    }
       
    var newsCenterDeleteCTRL = function($scope, $modalInstance, newsid) {
        $scope.ok = function() {
            $http({
                url: Config.ApiURL + "/news/newscenterdelete/" + newsid,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {
                
                loadnews();
                $modalInstance.close();
                successloadalert();

            }).error(function(data, status, headers, config) {
                loadnews();
                $modalInstance.close();
                errorloadalert();
            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };


        };



        var newscenterEditCTRL = function($scope, $modalInstance, newsid, $state) {
            $scope.newsid = newsid;
            $scope.ok = function(newsid) {
                $scope.newsid = newsid;
                $state.go('centerview.editnews', {newsid: newsid });
                    $modalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }

        $scope.editnewscenter = function(newsid) {
            $scope.newsid
            var modalInstance = $modal.open({
                templateUrl: 'centernewsEdit.html',
                controller: newscenterEditCTRL,
                resolve: {
                    newsid: function() {
                        return newsid
                    }
                }
            });
        }

})