'use strict';

/* Controllers */

app.controller('allbeneplacecancelledCtrl', function($scope, $state, Upload ,$q, $http, Config, $stateParams,allbeneplaceFactory, $modal){

     $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    var num = 10;
    var off = 1;
    var keyword = null;

    $scope.search = function (searchkeyword) {
        keyword = searchkeyword;
        allbeneplaceFactory.loadclasscancelledlist(num,off, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }

    $scope.numpages = function (off, keyword) {
        allbeneplaceFactory.loadclasscancelledlist(num,off, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })
    }

    $scope.setPage = function (pageNo) {
        off = pageNo;
        allbeneplaceFactory.loadclasscancelledlist(num,pageNo, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })
    };

    var loadbeneplace = function(){
        allbeneplaceFactory.loadclasscancelledlist(num,off, keyword, function(data){
            console.log(data);
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }

    loadbeneplace();

    $scope.viewsession = function(sessionid){
        var modalInstance = $modal.open({
            templateUrl: 'beneplaceView.html',
            controller: beneplaceViewCTRL,
            resolve: {
                sessionid: function() {
                    return sessionid
                }
            }
        });
    }

    var beneplaceViewCTRL = function($scope, $modalInstance, sessionid, $state) {
        allbeneplaceFactory.loadbenecancelledsession(sessionid, $stateParams.userid, function(data){
            $scope.session = data;
            console.log(data);
        })

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    $scope.referrallink = function(){
        var modalInstance = $modal.open({
            templateUrl: 'referralAdd.html',
            controller: referralAddCTRL
        });
    };

     var referralAddCTRL = function($scope, $modalInstance, $state) {
        allbeneplaceFactory.viewreferrallink(function(data){
           console.log(data);
            $scope.ref = data;
        })

        $scope.ok = function (ref) {
            console.log(ref);
            allbeneplaceFactory.savereferrallink(ref,function(data){
                console.log(data);

            })

            // $modalInstance.dismiss('cancel');
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

})
