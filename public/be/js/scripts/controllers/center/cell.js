'use strict';

/* Controllers */

app.controller('cellCtrl', function ($scope, $state, $q,$stateParams, Config, cellFactory){

	$scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

	var loadcontact = function(){
		
		cellFactory.loadcontact($stateParams.centerid, function(data){
			// console.log(data);
			if(data != ''){
				$scope.contact = data;
				$scope.hasvalue = true;
			}
			else{
				$scope.hasvalue = false;
			}
			
		});
	};

	loadcontact();

	$scope.saveContact = function(contact){
		contact['centerid'] = $stateParams.centerid;
		console.log(contact);
		cellFactory.savecontact(contact, function(data){
			
			$scope.alerts[0]= {type: data.type, msg: data.msg};
		});
	};

	$scope.testContact = function(contact){
		
		cellFactory.testcontact(contact, function(data){
			
			$scope.alerts[0]= {type: data.type, msg: data.msg};
		});
	};

})