'use strict';

/* Controllers */

app.controller('allintrosessionCtrl', function($scope, $state, Upload ,$q, $http, Config, $stateParams,allintrosessionFactory, $modal){

     $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    var num = 10;
    var off = 1;
    var keyword = null;

    $scope.search = function (searchkeyword) {
        keyword = searchkeyword;
        allintrosessionFactory.loadclasslist(num,off, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        });

    };

    $scope.numpages = function (off, keyword) {
        allintrosessionFactory.loadclasslist(num,off, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        });
    };

    $scope.setPage = function (pageNo) {
        off = pageNo;
        allintrosessionFactory.loadclasslist(num,pageNo, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        });
    };

    var loadsession = function(){
        allintrosessionFactory.loadclasslist(num,off, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        });

    };

    loadsession();

    $scope.viewsession = function(sessionid){
        var modalInstance = $modal.open({
            templateUrl: 'sessionView.html',
            controller: sessionViewCTRL,
            resolve: {
                sessionid: function() {
                    return sessionid;
                }
            }
        });
    };

    var sessionViewCTRL = function($scope, $modalInstance, sessionid, $state) {
        allintrosessionFactory.loadsession(sessionid,$stateParams.userid, function(data){
            $scope.session = data;
            loadsession();
        });

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

});
