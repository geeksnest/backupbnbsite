app.controller('Createcenter', function($scope, Config, CreateCenterFactory, $location, anchorSmoothScroll, $timeout, $modal, Factory){
  'use strict';

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.center = {
        status:1,
        centertype:1 ,
        slugs: []
    };

    // var oricenter = angular.copy($scope.center);

    $timeout(function() {
      $("tags-input").removeClass("ng-invalid");
    },1000);

    $scope.saveCenter = function(center) {
      $scope.isSaving = true;
      if(center.slugs.length === 0 ) {
          Factory.toaster("warning","Warning","Please check your Center SEO");
          $scope.isSaving = false;
      } else {
          CreateCenterFactory.savecenter(center,function(data){
              if(data.hasOwnProperty("scs")) {
                Factory.toaster("success","Success", data.scs);
                $scope.center = angular.copy({status:1,centertype:1,slugs:[]}, $scope.center);
              } else if (data.hasOwnProperty("err")) {
                Factory.toaster("error","Error", data.err + ". Please try to submit again.");
              }
              $scope.isSaving = false;
          });
      }

    };

    $scope.oncentertitle = function convertToSlug(Text) {
        if(Text === null) {
            $scope.center.slugs = '';
        } else {
            var text1 = Text.replace(/[^\w ]+/g,'');
            if($scope.center.slugs.length > 0){
              $scope.center.slugs[0] = { slug : angular.lowercase(text1.replace(/ +/g,'-')) };
            } else {
              $scope.center.slugs = [];
              $scope.center.slugs.push({ slug : angular.lowercase(text1.replace(/ +/g,'-')) });
            }
        }
    };

    CreateCenterFactory.loadregionmanager(function(data1){
         $scope.regionmanager = data1;
    });

    CreateCenterFactory.listregion(function (data){
        if(data.length > 0) {
            $scope.regions = data;
            $scope.center.centerregion = data[0].regionid;
            $scope.hideregion = false;
        } else {
          $scope.hideregion = true;
        }
    });

    CreateCenterFactory.loadcentermanager(function(data3){
         $scope.centermanager = data3;
    });


    CreateCenterFactory.loadstate(function(data){
         $scope.centerstate = data;
    });


    $scope.statechange = function(statecode) {
        CreateCenterFactory.loadcity(statecode,function(data){
            $scope.centercity = data;
            $scope.center.centercity =data[0].city;
            CreateCenterFactory.loadzip(data[0].city,function(data){
                $scope.getcenterzip = data;
                $scope.center.centerzip =data[0].zip;
            });
        });
    };

    $scope.citychange = function(cityname) {
      CreateCenterFactory.loadzip(cityname,function(data){
        CreateCenterFactory.loadzip(data[0].city,function(data){
          $scope.getcenterzip = data;
          $scope.center.centerzip =data[0].zip;
        });
      });
    };

    $scope.authorizehelp = function() {
      Factory.modaL("authorizeHelp.html","md",
        function($scope, $modalInstance) {
          $scope.cancel = function() {
              $modalInstance.dismiss('cancel');
          };
        }
      );
    };

    $scope.paypalhelp = function() {
      Factory.modaL("paypalHelp.html","md",
        function($scope, $modalInstance) {
          $scope.cancel = function() {
              $modalInstance.dismiss('cancel');
          };
        }
      );
    };

    $scope.percent = function(price){
        var getpercent = 0.25 * price;
        $scope.center.dis1monthprice = price - getpercent;
    };

    $scope.percent2 = function(price){
        var getpercent = 0.25 * price;
        $scope.center.dis3monthprice = price - getpercent;
    };

    //DATE PICKER
    $scope.today = function () {
    $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;

    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.onslug = function() {
      if($scope.center.slugs.length > 3) {
        $scope.center.slugs.splice(($scope.center.slugs.length-1), 1);
      }
    };

});
