'use strict';

/* Controllers */

app.controller('regionanddistrictCtrl', function($scope, $state ,$q, $http, Config, $interval, $modal,regionanddistrictFactory){

    var num = 10;
    var off = 1;
    var keyword = null;

    var loadregion = function(){
        regionanddistrictFactory.loadregion(num,off, keyword,function(data){
            $scope.regionlist = data.data;
            $scope.maxSize = 5;
            $scope.regionTotalItems = data.total_items;
            $scope.regionCurrentPage = data.index;
        });
    }
    loadregion();

    $scope.search = function (textsearch) {
        keyword = textsearch;
        regionanddistrictFactory.loadregion(num,off, keyword,function(data){
            $scope.regionlist = data.data;
            $scope.maxSize = 5;
            $scope.regionTotalItems = data.total_items;
            $scope.regionCurrentPage = data.index;
        });

    }

    $scope.setregionPage = function (pageNo) {
        regionanddistrictFactory.loadregion(num,pageNo, keyword,function(data){
            $scope.regionlist = data.data;

            $scope.maxSize = 5;
            $scope.regionTotalItems = data.total_items;
            $scope.regionCurrentPage = data.index;
        });
    };

    $scope.clearsearch = function(){
       
        $scope.regionsearchtext = '';
        num = 10;
        off = 1;
        keyword = null;
        loadregion();
    }


    $scope.addregion = function() {
        var modalInstance = $modal.open({
            templateUrl: 'regionAdd.html',
            controller: regionAddCTRL,
            resolve: {

            }
        });
    }

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    var loadaddalert = function(){
        $scope.alerts[0]= {type: 'success', msg: 'Region successfully Added!'};
    }

       
    var regionAddCTRL = function($scope, $modalInstance) {
        
        $scope.ok = function(region) {

            regionanddistrictFactory.saveregion(region, function(data){
                loadaddalert();
                loadregion();
                $modalInstance.dismiss('cancel');

            });

           
        }
    
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        }

    }

    var loaddeletealert = function(){
        $scope.alerts[0]= {type: 'success', msg: 'Region successfully Deleted!'};
    }


    var regionDeleteCtrl = function($scope, $modalInstance, regionid, $state) {

        $scope.ok = function() {
            var region = {'regionid':regionid}
            regionanddistrictFactory.deleteregion(region, function(data){
                loaddeletealert();
                loadregion();
                $modalInstance.dismiss('cancel');

            });
        }

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        }
    }

    $scope.regionDelete = function(regionid) {
        var modalInstance = $modal.open({
            templateUrl: 'regionDelete.html',
            controller: regionDeleteCtrl,
            resolve: {
                regionid: function() {
                    return regionid
                }
            }
        });
    }

    var loadupdatealert = function(){
        $scope.alerts[0]= {type: 'success', msg: 'Region successfully Updated!'};
    }

    $scope.updateregion = function(regionname,regionid) {

            var region = {
                'regionname' : regionname,
                'regionid': regionid
            }

            regionanddistrictFactory.updateregion(region, function(data){
                loadupdatealert();

            });
        

    }

    // end of region



    $scope.adddistrict = function() {
        var modalInstance = $modal.open({
            templateUrl: 'districtAdd.html',
            controller: districtAddCTRL,
            resolve: {

            }
        });
    }


    var loadadddistrictalert = function(){
        $scope.alerts[0]= {type: 'success', msg: 'District successfully Added!'};
    }

       
    var districtAddCTRL = function($scope, $modalInstance) {

        regionanddistrictFactory.loadallregion(function(data){
            $scope.regionlist = data;

        });
        
        $scope.ok = function(district) {

            regionanddistrictFactory.savedistrict(district, function(data){
                loadadddistrictalert();
                loaddistrict();
                $modalInstance.dismiss('cancel');

            });

           
        }
    
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        }

    }

    var dnum = 10;
    var doff = 1;
    var dkeyword = null;

    var loaddistrict = function(){
        regionanddistrictFactory.loaddistrict(dnum,doff, dkeyword,function(data){
            $scope.districtlist = data.data;
            $scope.maxSize = 5;
            $scope.districtTotalItems = data.total_items;
            $scope.districtCurrentPage = data.index;
        });
    }
    loaddistrict();

    $scope.search = function (textsearch) {
        dkeyword = textsearch;
        regionanddistrictFactory.loaddistrict(dnum,doff, dkeyword,function(data){
            $scope.districtlist = data.data;
            $scope.maxSize = 5;
            $scope.districtTotalItems = data.total_items;
            $scope.districtCurrentPage = data.index;
        });

    }

    $scope.setdistrictPage = function (pageNo) {
        regionanddistrictFactory.loaddistrict(dnum,pageNo,dkeyword,function(data){
            $scope.districtlist = data.data;

            $scope.maxSize = 5;
            $scope.districtTotalItems = data.total_items;
            $scope.districtCurrentPage = data.index;
        });
    };

    $scope.clearsearchdistrict = function(){
       
        $scope.regionsearchtext = '';
        dnum = 10;
        doff = 1;
        dkeyword = null;
        loaddistrict();
    }

    var loaddistrictdeletealert = function(){
        $scope.alerts[0]= {type: 'success', msg: 'District successfully Deleted!'};
    }


    var districtDeleteCtrl = function($scope, $modalInstance, districtid, $state) {

        $scope.ok = function() {
            var district = {'districtid':districtid}
            // console.log(district);
            regionanddistrictFactory.deletedistrict(district, function(data){
                // console.log(data);
                loaddistrictdeletealert();
                loaddistrict();
                $modalInstance.dismiss('cancel');

            });
        }

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        }
    }

    $scope.districtDelete = function(districtid) {
        var modalInstance = $modal.open({
            templateUrl: 'districtDelete.html',
            controller: districtDeleteCtrl,
            resolve: {
                districtid: function() {
                    return districtid
                }
            }
        });
    }


     $scope.districtEdit = function(districtid) {
        var modalInstance = $modal.open({
            templateUrl: 'districtEdit.html',
            controller: districtEditCTRL,
            resolve: {
                districtid: function() {
                    return districtid
                }
            }
        });
    }


    var loadadddistrictalert = function(){
        $scope.alerts[0]= {type: 'success', msg: 'District successfully Added!'};
    }

       
    var districtEditCTRL = function($scope, $modalInstance,districtid) {
        var district = {'districtid':districtid}
        // console.log(district);
        regionanddistrictFactory.loaddistrictbyid(district, function(data){
            $scope.district = data;
        });
        
        regionanddistrictFactory.loadallregion(function(data){
            $scope.regionlist = data;

        });
        
        $scope.ok = function(district) {

            regionanddistrictFactory.updateistrict(district, function(data){
                // console.log(data);
                loadadddistrictalert();
                loaddistrict();
                $modalInstance.dismiss('cancel');

            });

           
        }
    
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        }

    }
 
})