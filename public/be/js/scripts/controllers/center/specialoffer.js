'use strict';

/* Controllers */

app.controller('specialofferCtrl', function($scope, $filter, $state ,$q, $http, Config, $stateParams , $modal, Upload, specialofferFactory){
    $scope.offeralert = [];

    $scope.closeAlert = function (index, type) {
        if(type=='default') {
            $scope.offeralert.splice(index, 1);
        } else if (type=='image'){
            $scope.imagealert.splice(index, 1);
        }
    };

    var updateloadalert = function(){
        $scope.offeralert[0]= {type: 'success', msg: 'Offer Coupon successfully Updated!'};   
    }

    var offerAddCTRL = function($scope, $modalInstance, $state) {
        $scope.ok = function(offer,offerfile) {
            $scope.issaving = true;
            $scope.upload(offer,offerfile);
            // console.log(offer,offerfile);
        }
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        }


        $scope.upload = function (offer,files) 
        {
            $scope.imagealert = [];
            var filename
            var filecount = 0;
            if (files && files.length) 
            {
                $scope.imageloader=true;
                $scope.imagecontent=false;

                for (var i = 0; i < files.length; i++) 
                {
                    var file = files[i];

                        if (file.size >= 6000000) //6MB
                        {
                            $scope.imagealert[0]=({type: 'danger', msg: 'File ' + file.name + ' is too big. (Maximum of 6MB)'});
                            $scope.issaving = false;
                        }
                        else
                        {

                            var promises;
                                
                                promises = Upload.upload({
                                    
                                    url: Config.amazonlink, //S3 upload url including bucket name
                                    method: 'POST',
                                    transformRequest: function (data, headersGetter) {
                                    //Headers change here
                                    var headers = headersGetter();
                                    delete headers['Authorization'];
                                    return data;
                                    },
                                    fields : {
                                      key: 'uploads/offerimages/' + file.name, // the key to store the file on S3, could be file name or customized
                                      AWSAccessKeyId: Config.AWSAccessKeyId,
                                      acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                      policy: Config.policy, // base64-encoded json policy (see article below)
                                      signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                      "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                    },
                                    file: file
                                })
                                    promises.then(function(data){

                                        filecount = filecount + 1;
                                        filename = data.config.file.name;
                                        offer['image'] =  filename;
                                        offer['centerid'] = $stateParams.centerid;
                                        specialofferFactory.saveOffer(offer,function(data){
                                            successloadalert();
                                            loadoffer();
                                            $modalInstance.dismiss('cancel');
                                        })
                                    });
                        }           
                }
            }
          
        } //end of upload

        //custom DATESAVER [..]
        var monthNow = moment().format('MM');
        var dayNow = moment().format('DD');
        var yearNow = new Date().getFullYear();

        $scope.yearNow = yearNow;
        $scope.monthNow = monthNow;
        $scope.dayNow = dayNow;

         $scope.datemonth = [
            {name: "January", value: "01"}, {name: "Feb", value: "02"},
            {name: "Mar", value: "03"}, {name: "Apr", value: "04"},
            {name: "May", value: "05"}, {name: "Jun", value: "06"},
            {name: "Jul", value: "07"}, {name: "Aug", value: "08"},
            {name: "Sep", value: "09"}, {name: "Oct", value: "10"},
            {name: "Nov", value: "11"}, {name: "Dec", value: "12"}
        ];

        $scope.dateday = ['01','02','03','04','05','06','07','08','09','10',
            '11','12','13','14','15','16','17','18','19','20',
            '21','22','23','24','25','26','27','28','29','30', '31'];

        var getDays = function(m, y) {
            if(m=='01' || m=='03' || m=='05' || m=='07' || m=='08' || m=='10' || m=='12') {
                $scope.dayOfMonth = 31;
            } else if (m=='02') {
                if(y % 4 === 0) { $scope.dayOfMonth = 29; }
                else { $scope.dayOfMonth = 28; }
            } else {
                $scope.dayOfMonth = 30;
            }
        }
        getDays(monthNow, yearNow);

        $scope.selectedMonth = function(month, year) {
            getDays(month, year);
        }

        var startingYear = yearNow - 15; // datenow minus 15 (you can change this)
        var yearOptions = []; //array for year options to be displayed
        for(var x=0; x<=30; x++) {
            var year = startingYear + x;
            yearOptions.push(year); 
        }
        $scope.dateyear = yearOptions;

        $scope.selectedYear = function(month, year) {
            getDays(month, year);
        }

    }

    var successloadalert = function(){
            $scope.offeralert[0]= {type: 'success', msg: 'Offer Coupon successfully Added!'};   
    }

    var errorloadalert = function(){
            $scope.offeralert[0]= {type: 'danger', msg: 'Something went wrong Offer Coupon not Added!'};
    }

    $scope.addoffer = function() {
        var modalInstance = $modal.open({
            templateUrl: 'offerAdd.html',
            controller: offerAddCTRL
        });
    }

    //UPDATE OFFER/COUPON [MODAL]
    var offerEditCTRL = function($scope, $modalInstance, offerid) {
       specialofferFactory.offerinfo(offerid, function (data){
            $scope.upoffer = data.offerprop;
            $scope.coupon = data.offerprop;
            $scope.coupon.expiyear = parseInt(moment(data.offerprop.expiration).format('YYYY'));
            $scope.coupon.expimonth = moment(data.offerprop.expiration).format('MM');
            $scope.coupon.expiday = moment(data.offerprop.expiration).format('DD');
       })

       //custom DATESAVER [..]
        var monthNow = moment().format('MM');
        var dayNow = moment().format('DD');
        var yearNow = new Date().getFullYear();

        $scope.yearNow = yearNow;
        $scope.monthNow = monthNow;
        $scope.dayNow = dayNow;

         $scope.datemonth = [
            {name: "Jan", value: "01"}, {name: "Feb", value: "02"},
            {name: "Mar", value: "03"}, {name: "Apr", value: "04"},
            {name: "May", value: "05"}, {name: "Jun", value: "06"},
            {name: "Jul", value: "07"}, {name: "Aug", value: "08"},
            {name: "Sep", value: "09"}, {name: "Oct", value: "10"},
            {name: "Nov", value: "11"}, {name: "Dec", value: "12"}
        ];

        $scope.dateday = ['01','02','03','04','05','06','07','08','09','10',
            '11','12','13','14','15','16','17','18','19','20',
            '21','22','23','24','25','26','27','28','29','30', '31'];

        var getDays = function(m, y) {
            if(m=='01' || m=='03' || m=='05' || m=='07' || m=='08' || m=='10' || m=='12') {
                $scope.dayOfMonth = 31;
            } else if (m=='02') {
                if(y % 4 === 0) { $scope.dayOfMonth = 29; }
                else { $scope.dayOfMonth = 28; }
            } else {
                $scope.dayOfMonth = 30;
            }
        }
        getDays(monthNow, yearNow);

        $scope.selectedMonth = function(month, year) {
            getDays(month, year);
        }

        var startingYear = yearNow - 15; // datenow minus 15 (you can change this)
        var yearOptions = []; //array for year options to be displayed
        for(var x=0; x<=30; x++) {
            var year = startingYear + x;
            yearOptions.push(year); 
        }
        $scope.dateyear = yearOptions;

        $scope.selectedYear = function(month, year) {
            getDays(month, year);
        }

        $scope.update = function (coupon, image) {
            $scope.issaving = true;
            if(image == undefined) {
                specialofferFactory.updateOffer(coupon,function(data){
                    updateloadalert();
                    loadoffer();
                    $modalInstance.dismiss('cancel');
                })
            } else {
                $scope.imagechange(coupon, image);
            }
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        }

        $scope.imagechange = function(coupon, files) {
            $scope.imagealert = [];
            var filename
            var filecount = 0;
            if (files && files.length) 
            {

                $scope.imageloader=true;
                $scope.imagecontent=false;

                for (var i = 0; i < files.length; i++) 
                {
                    var file = files[i];

                        if (file.size >= 6000000) //6MB
                        {
                            $scope.imagealert[0]=({type: 'danger', msg: 'File ' + file.name + ' is too big. (Maximum of 6MB)'});
                            $scope.issaving = false;
                        }
                        else
                        {
                            var promises;
                                
                                promises = Upload.upload({
                                    
                                    url: Config.amazonlink, //S3 upload url including bucket name
                                    method: 'POST',
                                    transformRequest: function (data, headersGetter) {
                                    //Headers change here
                                    var headers = headersGetter();
                                    delete headers['Authorization'];
                                    return data;
                                    },
                                    fields : {
                                      key: 'uploads/offerimages/' + file.name, // the key to store the file on S3, could be file name or customized
                                      AWSAccessKeyId: Config.AWSAccessKeyId,
                                      acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                      policy: Config.policy, // base64-encoded json policy (see article below)
                                      signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                      "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                    },
                                    file: file
                                })
                                    promises.then(function(data){

                                        filecount = filecount + 1;
                                        filename = data.config.file.name;
                                        coupon['image'] =  filename;
                                        specialofferFactory.updateOfferWithUpload(coupon,function(data){
                                            updateloadalert();
                                            loadoffer();
                                            $modalInstance.dismiss('cancel');
                                        })
                                    });
                        }           
                }
            }
        } //end of imagechange
    }

    $scope.editoffer = function(offerid) {
        var modalInstance = $modal.open({
            templateUrl: 'offerEdit.html',
            controller: offerEditCTRL,
            resolve: {
                offerid: function() {
                    return offerid
                }
            }
        });
    }

    specialofferFactory.loadOffer($stateParams.centerid,function(data){
        $scope.offerdata = data;
    })

    
    var loadoffer = function(){

        specialofferFactory.loadOffer($stateParams.centerid,function(data){
            $scope.offerdata = data;
        })
    }

    $scope.deleteoffer = function(offerid) {
        var modalInstance = $modal.open({
            templateUrl: 'offerDelete.html',
            controller: offerDeleteCTRL,
            resolve: {
                offerid: function() {
                    return offerid
                }
            }
        });
    }

    
    var successdeletealert = function(){

            $scope.offeralert[0]= {type: 'success', msg: 'Offer successfully Deleted!'};
            
    }

    var errordeletealert = function(){

            $scope.offeralert[0]= {type: 'danger', msg: 'Something went wrong Offer not Deleted!'};
    }


       
    var offerDeleteCTRL = function($scope, $modalInstance, offerid) {
        $scope.ok = function() {

            specialofferFactory.deleteOffer(offerid,function(data){
                loadoffer();
                successdeletealert();
                $modalInstance.dismiss('cancel');
            })

        }

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        }


    }


    $scope.setstatus = function (status,offerid) {
            // console.log(status);
            if(status == 1)
            {
                 $http({
                    url: Config.ApiURL + "/center/offer/updateofferstatus/1/" + offerid,
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function (data, status, headers, config) {

                    loadoffer();

                }).error(function (data, status, headers, config) {
                     loadoffer();

                });
            }
            else
            {
               $http({
                    url: Config.ApiURL + "/center/offer/updateofferstatus/0/" + offerid,
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function (data, status, headers, config) {

                    loadoffer();

                }).error(function (data, status, headers, config) {

                    loadoffer();
                });
            }
       
    }

})