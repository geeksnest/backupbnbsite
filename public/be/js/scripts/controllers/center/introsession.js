'use strict';

/* Controllers */

app.controller('introsessionCtrl', function($scope, $state, Upload ,$q, $http, Config, $stateParams,introsessionFactory, $modal){

     $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    var num = 10;
    var off = 1;
    var keyword = null;

    $scope.search = function (searchkeyword) {
        keyword = searchkeyword;
        introsessionFactory.loadintrolist(num,off, keyword,$stateParams.centerid, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }
    
    $scope.numpages = function (off, keyword) {
        introsessionFactory.loadintrolist(num,off, keyword,$stateParams.centerid, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })
    }

    $scope.setPage = function (pageNo) {
        
        introsessionFactory.loadintrolist(num,pageNo, keyword,$stateParams.centerid, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })
    };
    
    var loadnews = function(){
        introsessionFactory.loadintrolist(num,off, keyword,$stateParams.centerid, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }

    loadnews();

    $scope.viewsession = function(sessionid){
        var modalInstance = $modal.open({
            templateUrl: 'sessionView.html',
            controller: sessionViewCTRL,
            resolve: {
                sessionid: function() {
                    return sessionid
                }
            }
        });
    }

    var sessionViewCTRL = function($scope, $modalInstance, sessionid, $state) {
        introsessionFactory.loadsession(sessionid,$stateParams.userid, function(data){
            $scope.session = data;
            loadnews();
        })

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

})