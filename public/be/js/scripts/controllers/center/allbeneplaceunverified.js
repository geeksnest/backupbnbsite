'use strict';

/* Controllers */

app.controller('allbeneplaceunverifiedCtrl', function($scope, $state, Upload ,$q, $http, Config, $stateParams,allbeneplaceFactory, $modal){

     $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    var num = 10;
    var off = 1;
    var keyword = null;

    $scope.search = function (searchkeyword) {
        keyword = searchkeyword;
        allbeneplaceFactory.unverifiedclasslist(num,off, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }

    $scope.numpages = function (off, keyword) {
        allbeneplaceFactory.unverifiedclasslist(num,off, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })
    }

    $scope.setPage = function (pageNo) {
        off = pageNo;
        allbeneplaceFactory.unverifiedclasslist(num,pageNo, keyword, function(data){
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })
    };

    var loadbeneplace = function(){
        allbeneplaceFactory.unverifiedclasslist(num,off, keyword, function(data){
            console.log(data);
            $scope.sessiondata = data.data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;

        })

    }

    loadbeneplace();

    $scope.viewsession = function(sessionid){
        var modalInstance = $modal.open({
            templateUrl: 'beneplaceunverifiedView.html',
            controller: beneplaceViewCTRL,
            resolve: {
                sessionid: function() {
                    return sessionid
                }
            }
        });
    }

    var beneplaceViewCTRL = function($scope, $modalInstance, sessionid, $state) {
        allbeneplaceFactory.loadunverifiedsession(sessionid, $stateParams.userid, function(data){
          console.log(data);
            $scope.session = data;
            loadbeneplace();
        })

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.verify = function() {
            var modalInstance = $modal.open({
                templateUrl: 'sessionVerify.html',
                controller: benepsessionVerifyCtrl,
                resolve: {
                    sessionid: function() {
                        return sessionid
                    }
                }
            }).result.then(function(data){

            });
            $modalInstance.dismiss();
        }
    }

    var pushAlert = function(data) {
        $scope.alerts = [];
        if(data.hasOwnProperty('success')){
            $scope.alerts.push({ type: 'success', msg: data.success });
        }else {
            $scope.alerts.push({ type: 'danger', msg: 'An error occurred please try again later.'});
        }
    };

    var benepsessionVerifyCtrl = function($scope, $modalInstance, sessionid) {
        $scope.cancel = function() {
            $modalInstance.dismiss();
        }

        $scope.change = function(confirmation) {
            if(confirmation != undefined){
                $scope.error = false;
            }else {
                $scope.error = true;
            }
        }

        $scope.ok = function(confirmation) {
            if(confirmation != undefined){
                var session = {
                  'sessionid':sessionid,
                  'confirmation':confirmation
                }
                allbeneplaceFactory.verifysession(session, function(data) {
                    console.log(data);
                    if(data.hasOwnProperty('success')){

                      $modalInstance.dismiss();
                      pushAlert(data);
                      loadbeneplace();
                    }
                    else{
                      $scope.error = true;
                    }

                });
            }else {
                $scope.error = true;
            }
        }
    }

    $scope.referrallink = function(){
        var modalInstance = $modal.open({
            templateUrl: 'referralAdd.html',
            controller: referralAddCTRL
        });
    };

     var referralAddCTRL = function($scope, $modalInstance, $state) {
        allbeneplaceFactory.viewreferrallink(function(data){
           console.log(data);
            $scope.ref = data;
        })

        $scope.ok = function (ref) {
            console.log(ref);
            allbeneplaceFactory.savereferrallink(ref,function(data){
                console.log(data);

            })

            // $modalInstance.dismiss('cancel');
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

})
