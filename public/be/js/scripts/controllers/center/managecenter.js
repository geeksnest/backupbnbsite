app.controller('Managecenter', function($scope, $state ,$q, $http, Config, $log, ManageCenterFactory, $interval, $modal, $stateParams, Factory){
    'use strict';

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    var userid = $stateParams.userid;
    $scope.currentstatusshow = '';

    ManageCenterFactory.centerlist(num,off, keyword, userid, function(data){
        $scope.data = data;
        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });

    var dropdowncenter = function() {
        ManageCenterFactory.dropdowncenterlist(num,off, keyword, userid, function(data){
            $scope.centerdatalist = data;
        });
    };

    dropdowncenter();

    $scope.search = function (keyword) {
        var off = 0;
        ManageCenterFactory.centerlist('10','1', keyword, userid, function(data){
          $scope.data = data;
          $scope.maxSize = 5;
          $scope.bigTotalItems = data.total_items;
          $scope.bigCurrentPage = data.index;
        });
  };

    $scope.numpages = function (off, keyword) {
      ManageCenterFactory.centerlist('10', '1', keyword, userid, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
      });
  };

    $scope.setPage = function (pageNo) {
        ManageCenterFactory.centerlist(num,pageNo, keyword, userid, function(data){
            $scope.data = data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        });
    };

    $scope.setstatus = function (status,centerid,keyword,centerslugs) {

            if(status == 1)
            {
                $http({
                        url: Config.ApiURL + "/center/updatecenterstatus/0/" + centerid + '/' + keyword,
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {
                        $scope.currentstatusshow = centerslugs;
                        var i = 2;
                        setInterval(function(){
                            i--;
                            if(i == 0)
                            {
                                ManageCenterFactory.centerlist(num,$scope.bigCurrentPage, keyword, userid, function(data){
                                    $scope.data = data;

                                    $scope.maxSize = 5;
                                    $scope.bigTotalItems = data.total_items;
                                    $scope.bigCurrentPage = data.index;
                                });
                                 $scope.currentstatusshow = 0;
                            }
                        },1000);

                    }).error(function (data, status, headers, config) {
                        ManageCenterFactory.centerlist(num,$scope.bigCurrentPage, keyword, userid, function(data){
                            $scope.data = data;

                            $scope.maxSize = 5;
                            $scope.bigTotalItems = data.total_items;
                            $scope.bigCurrentPage = data.index;
                        });

                    });
            }
            else
            {
                 $http({
                        url: Config.ApiURL + "/center/updatecenterstatus/1/" + centerid + '/' + keyword,
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {


                        $scope.currentstatusshow = centerslugs;
                        var i = 2;
                        setInterval(function(){
                            i--;
                            if(i == 0) {
                                ManageCenterFactory.centerlist(num,$scope.bigCurrentPage, keyword, userid, function(data){
                                    $scope.data = data;

                                    $scope.maxSize = 5;
                                    $scope.bigTotalItems = data.total_items;
                                    $scope.bigCurrentPage = data.index;
                                });
                                $scope.currentstatusshow = 0;
                            }
                        },1000);
                    }).error(function (data, status, headers, config) {
                       ManageCenterFactory.centerlist(num,$scope.bigCurrentPage, keyword, userid, function(data){
                            $scope.data = data;

                            $scope.maxSize = 5;
                            $scope.bigTotalItems = data.total_items;
                            $scope.bigCurrentPage = data.index;
                        });
                    });
            }
    };

    $scope.deletecenter = function(centerid) {
        Factory.modal("centerDelete.html",centerDeleteCTRL,"sm",centerid);
    };
    var centerDeleteCTRL = function($scope, $modalInstance, data) {
        $scope.ok = function() {
          ManageCenterFactory.deletecenter(data, function(Facdata){
              if(Facdata.hasOwnProperty("scs")) {
                  loadpage();
                  $modalInstance.close();
                  Factory.toaster("success","Success",Facdata.scs);
              } else if (Facdata.hasOwnProperty("err")) {
                  Factory.toaster("error","Error",Facdata.err);
              }
          });
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    };

    var loadpage = function(){

        ManageCenterFactory.centerlist(num,$scope.bigCurrentPage, $scope.searchtext, userid, function(data){
            $scope.data = data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        });
    };

    var successloadalert = function(){
        $scope.alerts[0]= {type: 'success', msg: 'Center successfully Deleted!'};
    };

    var errorloadalert = function(){
        $scope.alerts[0]= {type: 'danger', msg: 'Something went wrong Center not Deleted!'};
    };

    $scope.editcenter = function(centerid) {
        // $scope.centerid
        var modalInstance = $modal.open({
            templateUrl: 'centerEdit.html',
            controller: centerEditCTRL,
            resolve: {
                centerid: function() {
                    return centerid;
                }
            }
        });
    };
    var centerEditCTRL = function($scope, $modalInstance, centerid, $state) {
        $scope.centerid = centerid;
        $scope.ok = function(centerid) {
            $scope.centerid = centerid;
            $state.go('editcenter', {centerid: centerid});
            $modalInstance.dismiss('cancel');
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    var centerManageCTRL = function($scope, $modalInstance, centerid, $state) {
        $scope.centerid = centerid;
        $scope.ok = function(centerid) {
            $scope.centerid = centerid;
            $state.go('centerview', {centerid: centerid,uifrom :'none'});
            $modalInstance.dismiss('cancel');
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.managecenter = function(centerid) {
        // $scope.centerid
        var modalInstance = $modal.open({
            templateUrl: 'centerManage.html',
            controller: centerManageCTRL,
            resolve: {
                centerid: function() {
                    return centerid;
                }
            }
        });
    };

});
