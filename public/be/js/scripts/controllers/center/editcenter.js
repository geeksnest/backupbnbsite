app.controller('Editcenter', function($scope, $state, Upload ,$q, $http, Config, EditCenterFactory, $location, anchorSmoothScroll, $timeout, $stateParams,$modal, Factory){
    'use strict';

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    // $scope.center = {
    //     status:1,
    //     centertype:1 ,
    //     centerslugs: []
    // };
    //
    // var oricenter = angular.copy($scope.center);

    $scope.saveCenter = function(center) {
      $scope.isSaving = true;
      if(center.centerslugs.length === 0 ) {
          Factory.toaster("warning","Warning","Please check your Center SEO");
          $scope.isSaving = false;
      } else {
        if (center.openingdate == undefined) { center.openingdate = new Date(center.openingdate); }
        else {  center.openingdate = new Date(center.openingdate); }

        EditCenterFactory.savecenter(center,function(data){
            if(data.hasOwnProperty("scs")) {
                Factory.toaster("success","Success", data.scs);
            } else if (data.hasOwnProperty("err")) {
                Factory.toaster("error","Error", data.err);
            }
            editcenter();
            $scope.isSaving = false;
        });
      }
    };

    $scope.oncentertitle = function (Text) {
        if(Text == null){
            $scope.center.centerslugs = '';
        }
        else {
            // console.log('wew');
            var text1 = Text.replace(/[^\w ]+/g,'');
            if($scope.center.centerslugs.length > 0){
              $scope.center.centerslugs[0] = { slug : angular.lowercase(text1.replace(/ +/g,'-')) };
            } else {
              $scope.center.centerslugs = [];
              $scope.center.centerslugs.push({ slug : angular.lowercase(text1.replace(/ +/g,'-')) });
            }
        }
    };

    var tempdata = [];
    var editcenter = function () {
        EditCenterFactory.loadcenter($stateParams.centerid,function(data){
            $scope.center = data.center;

            if($scope.center.openingdate == "0000-00-00") {
                $scope.center.openingdate = undefined;
            }

            $scope.center.centerslugs = [{ "slug" : data.center.centerslugs }];

            if(data.center.centerslugs2 != "" && data.center.centerslugs2 != null) {
              $scope.center.centerslugs.push({ "slug" : data.center.centerslugs2});
            }
            if(data.center.centerslugs3 != "" && data.center.centerslugs3 != null) {
              $scope.center.centerslugs.push({ "slug" : data.center.centerslugs3});
            }
            tempdata.push(data.center);

            EditCenterFactory.loadcity(data.center.centerstate,function(data){
                $scope.centercity = data;
            });

            EditCenterFactory.loadzip(data.center.centercity,function(data){
                $scope.getcenterzip = data;
            });

        });
    };
    editcenter();

    EditCenterFactory.loadcentermanager(function(data3){
         $scope.centermanager = data3;
         // $scope.center.centerstate = data[0];
    });


    EditCenterFactory.loadstate(function(data){
         $scope.centerstate = data;
         // $scope.center.centerstate = data[0];
    });

    EditCenterFactory.listregion(function(data){
        if(data.length > 0) {
            $scope.regions = data;
            $scope.hideregion = false;
        } else { $scope.hideregion = true; }
    });

    $scope.statechange = function(statecode) {
        EditCenterFactory.loadcity(statecode,function(data){
            $scope.centercity = data;
            $scope.center.centercity =data[0].city;

            EditCenterFactory.loadzip(data[0].city,function(data){
                $scope.getcenterzip = data;
                $scope.center.centerzip =data[0].zip;
            });
        });
    };

    $scope.citychange = function(cityname) {
             EditCenterFactory.loadzip(cityname,function(data){
                $scope.getcenterzip = data;
                $scope.center.centerzip =data[0].zip;
            });
    };

    $scope.percent = function(price){
        var getpercent = 0.25 * price;
        $scope.center.dis1monthprice = price - getpercent;
    };

    $scope.percent2 = function(price){
        var getpercent = 0.25 * price;
        $scope.center.dis3monthprice = price - getpercent;
    };

    $scope.authorizehelp = function() {
      Factory.modaL("authorizeHelp.html","md",
        function($scope, $modalInstance) {
          $scope.cancel = function() {
              $modalInstance.dismiss('cancel');
          };
        }
      );
    };

    $scope.paypalhelp = function() {
      Factory.modaL("paypalHelp.html","md",
        function($scope, $modalInstance) {
          $scope.cancel = function() {
              $modalInstance.dismiss('cancel');
          };
        }
      );
    };

    //DATE PICKER
    $scope.today = function () {
    $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;

    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.onslug = function() {
      if($scope.center.centerslugs.length > 3) {
        $scope.center.centerslugs.splice(($scope.center.centerslugs.length-1), 1);
      }
    };
});
