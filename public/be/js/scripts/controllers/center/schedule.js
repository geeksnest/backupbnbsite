'use strict';

/* Controllers */

app.controller('scheduleCtrl', function ($scope, $state, Config, $stateParams, scheduleFactory ,$modal,$q, store, Login, CenterCalendarFactory, $http, $timeout){

	var loadschedule = function(){
		scheduleFactory.loadschedule($stateParams.centerid, function(data){
			$scope.schedlelist = data;
		});
	};
	loadschedule();

	$scope.alerts = [];
	$scope.alerts2 = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

	var addalert = function(type,message){
		 $scope.alerts[0]= {type: type, msg: message};
	};


    $scope.editsession = function(sessionid) {
        var modalInstance = $modal.open({
            templateUrl: 'scheduleedit.html',
            controller: scheduleEditCTRL,
            resolve: {
                sessionid: function() {
                    return sessionid;
                }
            }
        });
    };

    var scheduleEditCTRL = function($scope, $modalInstance, sessionid, $state) {
    	$scope.hourlist = [
         {time:'00'},
         {time:'01'},
         {time:'02'},
         {time:'03'},
         {time:'04'},
         {time:'05'},
         {time:'06'},
         {time:'07'},
         {time:'08'},
         {time:'09'},
         {time:'10'},
         {time:'11'},
         {time:'12'}];

         $scope.minlist = [
         {time:'00'},
         {time:'05'},
         {time:'10'},
         {time:'15'},
         {time:'20'},
         {time:'25'},
         {time:'30'},
         {time:'35'},
         {time:'40'},
         {time:'45'},
         {time:'50'},
         {time:'55'}];

        // $scope.classlist = [
        //  {classlist:'Basic Yoga'},
        //  {classlist:'Tai Chi'},
        //  {classlist:'Meridian Stretching'},
        //  {classlist:'Yoga and Qigong'},
        //  {classlist:'Advanced Yoga'},
        //  {classlist:'Core Strengthening'},
        //  {classlist:'Energy Meditation'},
        //  {classlist:'Energy Movement'},
        //  {classlist:'Yoga for Kids'},
        //  {classlist:'Yoga for Young Adults'},
        //  {classlist:'Successful Aging'},
        //  {classlist:'Yoga en Español'},
        //  {classlist:'Open Class'},
        //  {classlist:'Special Class'}];

				scheduleFactory.getClass(function(data) {
					$scope.classlist = data;
				});

         $scope.daylist = [
         {day:'Monday'},
         {day:'Tuesday'},
         {day:'Wednesday'},
         {day:'Thursday'},
         {day:'Friday'},
         {day:'Saturday'},
         {day:'Sunday'} ];

         scheduleFactory.loadschedulebyid(sessionid, function(data){
            $scope.session = data;
            $scope.session.starthourtime = data.starttime.slice( 0, 2 );
            $scope.session.startmintime = data.starttime.slice( 3, 5 );
            $scope.session.starttimeformat = data.starttime.slice( 6, 8 );
            $scope.session.endhourtime = data.endtime.slice( 0, 2 );
            $scope.session.endmintime = data.endtime.slice( 3, 5 );
            $scope.session.endtimeformat = data.endtime.slice( 6, 8 );
         });


        $scope.ok = function(session) {
            scheduleFactory.updateschedule(session, function(data){
                addalert(data.type,data.msg);
                loadschedule();
            });

            $modalInstance.dismiss('cancel');
        };
        $scope.cancel = function () {

            $modalInstance.dismiss('cancel');
        };
    };






    $scope.addnewschedule = function() {
        var modalInstance = $modal.open({
            templateUrl: 'scheduleAdd.html',
            controller: scheduleAddCTRL
        });
    };

    var scheduleAddCTRL = function($scope, $modalInstance, $state, scheduleFactory) {
    	$scope.hourlist = [
         {time:'00'},
         {time:'01'},
         {time:'02'},
         {time:'03'},
         {time:'04'},
         {time:'05'},
         {time:'06'},
         {time:'07'},
         {time:'08'},
         {time:'09'},
         {time:'10'},
         {time:'11'},
         {time:'12'}];

         $scope.minlist = [
         {time:'00'},
         {time:'05'},
         {time:'10'},
         {time:'15'},
         {time:'20'},
         {time:'25'},
         {time:'30'},
         {time:'35'},
         {time:'40'},
         {time:'45'},
         {time:'50'},
         {time:'55'}];


        // $scope.classlist = [
        //  {classlist:'Basic Yoga'},
        //  {classlist:'Tai Chi'},
        //  {classlist:'Meridian Stretching'},
        //  {classlist:'Yoga and Qigong'},
        //  {classlist:'Advanced Yoga'},
        //  {classlist:'Core Strengthening'},
        //  {classlist:'Energy Meditation'},
        //  {classlist:'Energy Movement'},
        //  {classlist:'Yoga for Kids'},
        //  {classlist:'Yoga for Young Adults'},
        //  {classlist:'Successful Aging'},
        //  {classlist:'Yoga en Español'},
        //  {classlist:'Open Class'},
        //  {classlist:'Special Class'}]

				scheduleFactory.getClass(function(data) {
					$scope.classlist = data;
				});

         $scope.daylist = [
         {day:'Monday'},
         {day:'Tuesday'},
         {day:'Wednesday'},
         {day:'Thursday'},
         {day:'Friday'},
         {day:'Saturday'},
         {day:'Sunday'}];


        $scope.ok = function(session) {
        	session.centerid = $stateParams.centerid;
        	scheduleFactory.saveschedule(session, function(data){
        		addalert(data.type,data.msg);
        		loadschedule();
        	});
        	$modalInstance.dismiss('cancel');

        };
        $scope.cancel = function () {

            $modalInstance.dismiss('cancel');
        };
    };

    $scope.delete = function(sessionid) {
        var modalInstance = $modal.open({
            templateUrl: 'scheduleDelete.html',
            controller: scheduleDeleteCTRL,
            resolve: {
                sessionid: function() {
                    return sessionid;
                }
            }
        });
    };

    var scheduleDeleteCTRL = function($scope, $modalInstance, $state, sessionid) {

        $scope.ok = function() {
            scheduleFactory.deleteschedule(sessionid, function(data){
                addalert(data.type,data.msg);
                loadschedule();
            });
            $modalInstance.dismiss('cancel');

        };
        $scope.cancel = function () {

            $modalInstance.dismiss('cancel');
        };
    };


		$scope.calendar = {
      calendartitle: "",
      calendardate: "",
      timefrom: "",
      timeto: "",
      description: "",
      status: 1
    };

    $scope.timelist = [
         {time:'01:00'}, {time:'01:05'}, {time:'01:10'}, {time:'01:15'}, {time:'01:20'}, {time:'01:25'},
         {time:'01:30'}, {time:'01:35'}, {time:'01:40'}, {time:'01:45'}, {time:'01:50'}, {time:'01:55'},
         {time:'02:00'}, {time:'02:05'}, {time:'02:10'}, {time:'02:15'}, {time:'02:20'}, {time:'02:25'},
         {time:'02:30'}, {time:'02:35'}, {time:'02:40'}, {time:'02:45'}, {time:'02:50'}, {time:'02:55'},
         {time:'03:00'}, {time:'03:05'}, {time:'03:10'}, {time:'03:15'}, {time:'03:20'}, {time:'03:25'},
         {time:'03:30'}, {time:'03:35'}, {time:'03:40'}, {time:'03:45'}, {time:'03:50'}, {time:'03:55'},
         {time:'04:00'}, {time:'04:05'}, {time:'04:10'}, {time:'04:15'}, {time:'04:20'}, {time:'04:25'},
         {time:'04:30'}, {time:'04:35'}, {time:'04:40'}, {time:'04:45'}, {time:'04:50'}, {time:'04:55'},
         {time:'05:00'}, {time:'05:05'}, {time:'05:10'}, {time:'05:15'}, {time:'05:20'}, {time:'05:25'},
         {time:'05:30'}, {time:'05:35'}, {time:'05:40'}, {time:'05:45'}, {time:'05:50'}, {time:'05:55'},
         {time:'06:00'}, {time:'06:05'}, {time:'06:10'}, {time:'06:15'}, {time:'06:20'}, {time:'06:25'},
         {time:'06:30'}, {time:'06:35'}, {time:'06:40'}, {time:'06:45'}, {time:'06:50'}, {time:'06:55'},
         {time:'07:00'}, {time:'07:05'}, {time:'07:10'}, {time:'07:15'}, {time:'07:20'}, {time:'07:25'},
         {time:'07:30'}, {time:'07:35'}, {time:'07:40'}, {time:'07:45'}, {time:'07:50'}, {time:'07:55'},
         {time:'08:00'}, {time:'08:05'}, {time:'08:10'}, {time:'08:15'}, {time:'08:20'}, {time:'08:25'},
         {time:'08:30'}, {time:'08:35'}, {time:'08:40'}, {time:'08:45'}, {time:'08:50'}, {time:'08:55'},
         {time:'09:00'}, {time:'09:05'}, {time:'09:10'}, {time:'09:15'}, {time:'09:20'}, {time:'09:25'},
         {time:'09:30'}, {time:'09:35'}, {time:'09:40'}, {time:'09:45'}, {time:'09:50'}, {time:'09:55'},
         {time:'10:00'}, {time:'10:05'}, {time:'10:10'}, {time:'10:15'}, {time:'10:20'}, {time:'10:25'},
         {time:'10:30'}, {time:'10:35'}, {time:'10:40'}, {time:'10:45'}, {time:'10:50'}, {time:'10:55'},
         {time:'11:00'}, {time:'11:05'}, {time:'11:10'}, {time:'11:15'}, {time:'11:20'}, {time:'11:25'},
         {time:'11:30'}, {time:'11:35'}, {time:'11:40'}, {time:'11:45'}, {time:'11:50'}, {time:'11:55'},
         {time:'12:00'}, {time:'12:05'}, {time:'12:10'}, {time:'12:15'}, {time:'12:20'}, {time:'12:25'},
         {time:'12:30'}, {time:'12:35'}, {time:'12:40'}, {time:'12:45'}, {time:'12:50'}, {time:'12:55'}
      ];

    var oricalendar = angular.copy($scope.calendar);

    $scope.alerts = [];

    $scope.closeAlert2 = function (index) {
        $scope.alerts2.splice(index, 1);
    };

		$scope.showaddeventf = function() {
			$scope.showaddevent = true;
		};

		$scope.defualt = function() {
			$scope.showaddevent = false;
			$scope.showeditevent = false;
			$scope.calendar = {
	      calendartitle: "",
	      calendardate: "",
	      timefrom: "",
	      timeto: "",
	      description: "",
	      status: 1
	    };
		};

    $scope.saveEvent = function (calendar) {
        calendar.centerid = $stateParams.centerid;

        CenterCalendarFactory.saveEvent(calendar,function(data){

        $scope.alerts2[0]= {type: data.type, msg: data.msg};

        $scope.showaddevent = false;

        $scope.calendar = {};
        // $scope.formcalendar.$setPristine();

        loadlist();

        });

    };


    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    $scope.currentstatusshow = '';

    var loadlist = function (){
        CenterCalendarFactory.listEvent(num,off, keyword,$stateParams.centerid, function(data){
						$scope.showaddevent = false;
            $scope.data = data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        });
    };


    loadlist();

    $scope.search = function (keyword) {
				if(keyword === ''){
					keyword = undefined;
				}
        CenterCalendarFactory.listEvent('10','1', keyword,$stateParams.centerid, function(data){
        $scope.data = data;
        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
			});
    };

    $scope.numpages = function (off, keyword) {

        CenterCalendarFactory.listEvent('10', '1', keyword,$stateParams.centerid, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
			});
    };

    $scope.setPage = function (pageNo) {
        CenterCalendarFactory.listEvent(num,pageNo, keyword,$stateParams.centerid, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
			});
    };

    $scope.setstatus = function (status,activityid,keyword,centerslugs) {

            if(status == 1)
            {
                $http({
                        url: Config.ApiURL + "/center/updateeventstatus/0/" + activityid + '/' + keyword,
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {

                        $scope.currentstatusshow = centerslugs;
                        var i = 2;
                        setInterval(function(){
                            i--;

                            if(i === 0)
                            {
                                CenterCalendarFactory.listEvent(num,$scope.bigCurrentPage, keyword,$stateParams.centerid, function(data){
                                    $scope.data = data;

                                    $scope.maxSize = 5;
                                    $scope.bigTotalItems = data.total_items;
                                    $scope.bigCurrentPage = data.index;
                                });
                                 $scope.currentstatusshow = 0;
                            }
                        },1000);

                    }).error(function (data, status, headers, config) {
                        CenterCalendarFactory.listEvent(num,$scope.bigCurrentPage, keyword,$stateParams.centerid, function(data){
                            $scope.data = data;

                            $scope.maxSize = 5;
                            $scope.bigTotalItems = data.total_items;
                            $scope.bigCurrentPage = data.index;
                        });

                    });
            }
            else
            {
                 $http({
                        url: Config.ApiURL + "/center/updateeventstatus/1/" + activityid + '/' + keyword,
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {


                        $scope.currentstatusshow = centerslugs;
                        var i = 2;
                        setInterval(function(){
                            i--;
                            if(i === 0)
                            {
                                CenterCalendarFactory.listEvent(num,$scope.bigCurrentPage, keyword,$stateParams.centerid, function(data){
                                    $scope.data = data;

                                    $scope.maxSize = 5;
                                    $scope.bigTotalItems = data.total_items;
                                    $scope.bigCurrentPage = data.index;
                                });
                                $scope.currentstatusshow = 0;
                            }


                        },1000);
                    }).error(function (data, status, headers, config) {
                       CenterCalendarFactory.listEvent(num,$scope.bigCurrentPage, keyword,$stateParams.centerid, function(data){
                            $scope.data = data;

                            $scope.maxSize = 5;
                            $scope.bigTotalItems = data.total_items;
                            $scope.bigCurrentPage = data.index;
                        });
                    });
            }
    };


    $scope.editevent = function(activityid){
        $scope.showeditevent = true;

        CenterCalendarFactory.loadEvent(activityid,function(data){

            $scope.calendar = data;

        });
    };

    $scope.updateEvent = function(calendar){


        CenterCalendarFactory.updateEvent(calendar,function(data){
            $scope.showeditevent = false;
            $scope.alerts2[0]= {type: data.type, msg: data.msg};

            $scope.showaddevent = false;

            $scope.calendar = angular.copy(oricalendar);
            $scope.formcalendar.$setPristine();

            loadlist();
        });
    };


    $scope.deleteevent = function(activityid) {
        var modalInstance = $modal.open({
            templateUrl: 'eventDelete.html',
            controller: eventDeleteCTRL,
            resolve: {
                activityid: function() {
                    return activityid;
                }
            }
        });
    };

    var loadpage = function(){

        CenterCalendarFactory.listEvent(num,$scope.bigCurrentPage, $scope.searchtext,$stateParams.centerid, function(data){
            $scope.data = data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        });
    };


    var successloadalert = function(){
          $scope.alerts2[0]= {type: 'success', msg: 'Event successfully Deleted!'};
    };

    var errorloadalert = function(){
          $scope.alerts2[0]= {type: 'danger', msg: 'Something went wrong Event not Deleted!'};
    };



    var eventDeleteCTRL = function($scope, $modalInstance, activityid) {
        $scope.ok = function() {
            $http({
                url: Config.ApiURL + "/center/eventdelete/" + activityid,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {

                loadpage();
                $modalInstance.close();
                successloadalert();

            }).error(function(data, status, headers, config) {
                loadpage();
                $modalInstance.close();
                errorloadalert();
            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    };





    $scope.hstep = 1;
    $scope.mstep = 30;

    $scope.options = {
      hstep: [1, 2, 3],
      mstep: [1, 5, 10, 15, 25, 30]
    };

    $scope.ismeridian = true;


    $scope.changed = function () {
      //console.log('Time changed to: ' + $scope.mytime);
    };

    $scope.clear = function() {
      $scope.mytime = null;
    };

    //DATE PICKER
    $scope.today = function () {
        $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
        $scope.dt = null;

    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

		$scope.datePicker = {isOpen: false };
		$scope.openDatePicker = function($event) {
		  $event.preventDefault();
		  $event.stopPropagation();
		  $timeout( function(){
		     $scope.datePicker.isOpen = true;
		  }, 50);
		};

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
});
