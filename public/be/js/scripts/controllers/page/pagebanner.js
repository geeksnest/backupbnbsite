'use strict';

/* Controllers */

app.controller('PageBanner', function($scope, $stateParams ,$q, $http, Config, $log, Pagebanner, Factory, $interval, $modal){
  $scope.amazonlink = Config.amazonlink;
  $scope.alerts = [];
  function LOADBANNERS() {
    $scope.sequence = [];
    Pagebanner.loadimages($stateParams.slugs, function(data){
      $scope.banners = data.banners;
      $scope.pageinfo = data.page;
      $scope.page = data.page.pageslugs;

      angular.forEach(data.banners, function(value, key) {
        $scope.sequence.push((key + 1));
      });
    });
  }

  LOADBANNERS();

  $scope.imageUploaderWithUrl = function(page) {
    Factory.modal("imageuploaderwithurl",imageUploaderWithUrlCtrl,"lg",page);
  };
  var imageUploaderWithUrlCtrl = function($scope, $modalInstance, data) {
    $scope.imgalert = [];
    var page = data;
    $scope.closeImgAlert = function(index) {
        $scope.imgalert.splice(index, 1);
    };

    $scope.uploadImgWithUrl = function(images) {
      images.map(function(value, key) {
        $scope.imageloader = true;
        if (value.size >= 2000000){
            $scope.imgalert.push({type: 'danger', msg: 'File ' + value.name + ' is too big'});
        } else {
          Factory.upload("uploads/slidernbanner/",value, function(img){
            img.then(function(data) {
              var image = {page:page, image:data.config.file.name, url:data.config.file.url, sequence:key+1,  };
              console.log(image);
              Factory.saveimage(image, function(data) {
                if(data.error==true) {
                  Factory.toaster("error","Failed", image['image'] + ' is failed to upload');
                } else {
                  Factory.toaster("success","Success", image['image'] + ' has been uploaded.');
                }
                if(key+1 == images.length) {
                  $scope.imageloader = false;
                  $modalInstance.dismiss();
                  LOADBANNERS();
                }
              });
            });
          });
        }
      });
    };

    $scope.cancel = function() {
      $modalInstance.dismiss();
    };
  };

  $scope.removeImage = function(id) {
    Factory.modal('confirm',RemoveBannerCtrl,'sm',id);
  };

  var RemoveBannerCtrl = function($scope, $modalInstance, data) {
    $scope.confirm = {
      title: "Confirm",
      msg: "Do you want to continue?",
      ok: "Yes",
      cancel: "No",
      key: data
    };

    $scope.ok = function(key) {
      Factory.removeimage(key, function(data) {
        if(data.error == true) {
          Factory.toaster("error","Error", data.errorMsg);
        } else {
          Factory.toaster("success","Success", 'Image has been deleted');
          LOADBANNERS();
        }
      });
      $modalInstance.dismiss();
    };
    $scope.cancel = function() {
      $modalInstance.dismiss();
    };
  };

  $scope.editImage = function(id) {
    Factory.editimage(id, function(data) {
      Factory.modal('bannerurl',EditBannerCtrl,'md',data);
    });

    var EditBannerCtrl = function($scope, $modalInstance, data) {
      $scope.image = data;
      $scope.ok = function(image) {
        Factory.updateimage(image, function(data) {
          if(data.error == true) {
            Factory.toaster("error","Error", data.errorMsg);
          } else {
            Factory.toaster("success","Success", 'Image has been updated');
            LOADBANNERS();
          }
        });
        $modalInstance.dismiss();
      };
      $scope.cancel = function() {
        $modalInstance.dismiss();
      };
    };
  };

  $scope.hideShowImage = function(id) {
    Factory.togglestatusimage(id, function(data) {
      if(data.success == true) {
        Factory.toaster("success","Success", 'Image status has been toggled');
        LOADBANNERS();
      }
    });
  };

  $scope.changeorder = function(img) {
    Factory.updateimgorder(img, function(data) {
      if(data.hasOwnProperty('success')){
        Factory.toaster("success","Success", 'Image sequence has been updated');
        LOADBANNERS();
      } else {
        Factory.toaster("error","Error", 'An error occured please try again later.');
      }
    });
  };

});
