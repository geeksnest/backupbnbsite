'use strict';

/* Controllers */

app.controller('Managepage', function($scope, $state ,$q, $http, Config, $log, Managepages, $interval, $modal){
    
    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = null;
    $scope.currentstatusshow = '';


    Managepages.sample(num,off, keyword, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });



    $scope.search = function (keyword) {
        var off = 0;
        Managepages.sample('10','1', keyword, function(data){
        $scope.data = data;
        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });

    }
    
    $scope.numpages = function (off, keyword) {
        Managepages.sample('10', '1', keyword, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });
    }

    $scope.setPage = function (pageNo) {
        Managepages.sample(num,pageNo, keyword, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });
    };


    $scope.setstatus = function (status,pageid,keyword) {
        if(status == 1)
        {
            $http({
                    url: Config.ApiURL + "/pages/updatepagestatus/0/" + pageid + '/' + keyword,
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function (data, status, headers, config) {
                    // Managepages.sample(num,$scope.bigCurrentPage, keyword, function(data){
                    //     $scope.data = data;

                    //     $scope.maxSize = 5;
                    //     $scope.bigTotalItems = data.total_items;
                    //     $scope.bigCurrentPage = data.index;
                    // });

                    $scope.currentstatusshow = pageid;
                    var i = 2;
                    setInterval(function(){
                        i--;

                        if(i == 0)
                        {
                            Managepages.sample(num,$scope.bigCurrentPage, keyword, function(data){
                                $scope.data = data;

                                $scope.maxSize = 5;
                                $scope.bigTotalItems = data.total_items;
                                $scope.bigCurrentPage = data.index;
                            });

                            $scope.currentstatusshow = 0;
                        }


                    },1000)
                    
                }).error(function (data, status, headers, config) {
                    Managepages.sample(num,$scope.bigCurrentPage, keyword, function(data){
                        $scope.data = data;

                        $scope.maxSize = 5;
                        $scope.bigTotalItems = data.total_items;
                        $scope.bigCurrentPage = data.index;
                    });
                    
                });
        }
        else
        {
             $http({
                    url: Config.ApiURL + "/pages/updatepagestatus/1/" + pageid + '/' + keyword,
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function (data, status, headers, config) {
                    // Managepages.sample(num,$scope.bigCurrentPage, keyword, function(data){
                    //     $scope.data = data;

                    //     $scope.maxSize = 5;
                    //     $scope.bigTotalItems = data.total_items;
                    //     $scope.bigCurrentPage = data.index;
                    // });

                    $scope.currentstatusshow = pageid;
                    var i = 2;
                    setInterval(function(){
                        i--;
                        if(i == 0)
                        {
                            Managepages.sample(num,$scope.bigCurrentPage, keyword, function(data){
                                $scope.data = data;

                                $scope.maxSize = 5;
                                $scope.bigTotalItems = data.total_items;
                                $scope.bigCurrentPage = data.index;
                            });
                            $scope.currentstatusshow = 0;
                        }


                    },1000)
                }).error(function (data, status, headers, config) {
                   Managepages.sample(num,$scope.bigCurrentPage, keyword, function(data){
                        $scope.data = data;

                        $scope.maxSize = 5;
                        $scope.bigTotalItems = data.total_items;
                        $scope.bigCurrentPage = data.index;
                    });
                });
        }
    }

    

  
    $scope.deletepage = function(pageid) {

        var modalInstance = $modal.open({
            templateUrl: 'pageDelete.html',
            controller: pageDeleteCTRL,
            resolve: {
                pageid: function() {
                    return pageid
                }
            }
        });
    }

    var loadpage = function(){

        Managepages.sample(num,$scope.bigCurrentPage, $scope.searchtext, function(data){
                        $scope.data = data;
                        $scope.maxSize = 5;
                        $scope.bigTotalItems = data.total_items;
                        $scope.bigCurrentPage = data.index;
                });

    }


    var successloadalert = function(){
            $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'Page successfully Deleted!' });
            
    }

    var errorloadalert = function(){
            $scope.alerts.push({ type: 'danger', msg: 'Something went wrong Page not Deleted!' });
    }


       
    var pageDeleteCTRL = function($scope, $modalInstance, pageid) {
        $scope.ok = function() {
            $http({
                url: Config.ApiURL + "/page/pagedelete/" + pageid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {
                
                loadpage();
                $modalInstance.close();
                successloadalert();

            }).error(function(data, status, headers, config) {
                loadpage();
                $modalInstance.close();
                errorloadalert();
            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };


        };


    var pageEditCTRL = function($scope, $modalInstance, pageid, $state) {
        $scope.pageid = pageid;
        $scope.ok = function(pageid) {
            $scope.pageid = pageid;
            $state.go('editpage', {pageid: pageid });
                $modalInstance.dismiss('cancel');
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    $scope.editpage = function(pageid) {
        var modalInstance = $modal.open({
            templateUrl: 'pageEdit.html',
            controller: pageEditCTRL,
            resolve: {
                pageid: function() {
                    return pageid
                }
            }
        });
    }

    $scope.editpagebanner = function(slugs) {
        var modalInstance = $modal.open({
            templateUrl: 'pageBanner.html',
            controller: function($scope, $modalInstance, slugs) {
                $scope.ok = function() {
                    $state.go('pagebanner', { slugs: slugs });
                    $modalInstance.dismiss();
                }

                $scope.cancel = function() {
                    $modalInstance.dismiss();
                }
            },
            resolve: {
                slugs: function() {
                    return slugs
                }
            }
        });
    }
})