'use strict';

/* Controllers */

app.controller('Createpage', function($scope, $state, Upload ,$q, $http, Config, $modal){
    $scope.amazonlink = Config.amazonlink;
    $scope.imageloader=false;
    $scope.imagecontent=true;

    $scope.page = {
      title: ""
    };

    var oripage = angular.copy($scope.page);

    $scope.onpagetitle = function convertToSlug(Text)
    {
        if(Text != null)
        {
            var text1 = Text.replace(/[^\w ]+/g,'');
            $scope.page.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
        }
        
    }

    $scope.alerts = [];
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.savePage = function(page)
    {
        $scope.isSaving = true;

        $http({
            url: Config.ApiURL + "/pages/create",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(page)
        }).success(function (data, status, headers, config) {
            console.log(data);
            if(data.hasOwnProperty('success')){
                $scope.isSaving = false;
                $scope.alerts.push({type: 'success', msg: 'Page successfully saved!'});
                $scope.page = angular.copy(oripage);
                $scope.formpage.$setPristine();
            }
            else{
                $scope.isSaving = false;
                $scope.alerts.push({type: 'danger', msg: 'Page not save please try again later!'});
            }
            
        }).error(function (data, status, headers, config) {
            scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
        });
    }

    // var pathimage = "";
    // var pathimages = function(){
    //     $scope.imagefilename=pathimage;
    //     $scope.pageimagesbannerimg = true;
    //     $scope.page.banner = pathimage;
    // }

    // $scope.showimageList = function(size,path){
    //     var amazon = $scope.amazon;
    //     var modalInstance = $modal.open({
    //         templateUrl: 'pageimagelist.html',
    //         controller: imagelistCTRL,
    //         size: size,
    //         resolve: {
    //             path: function() {
    //                 return amazon
    //             }
    //         }

    //     });
    // }

    // var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
    //     $scope.amazonlink = Config.amazonlink;
    //     $scope.imageloader=false;
    //     $scope.imagecontent=true;
    //     var loadimages = function() {
    //         $http({
    //             url: Config.ApiURL + "/pages/listimages",
    //             method: "GET",
    //             headers: {
    //                 'Content-Type': 'application/x-www-form-urlencoded'
    //             }
    //         }).success(function(data) {
    //             $scope.imagelist = data;
    //         }).error(function(data) {
    //             $scope.status = status;
    //         });
    //     }

    //     loadimages();

    //     $scope.upload = function(files) {
    //         $scope.upload(files);  
    //     };

    //     $scope.alertss = [];

    //     $scope.closeAlerts = function (index) {
    //         $scope.alertss.splice(index, 1);
    //     };

    //     $scope.upload = function (files) 
    //     {

        
    //     var filename
    //     var filecount = 0;
    //     if (files && files.length) 
    //     {
    //         $scope.imageloader=true;
    //         $scope.imagecontent=false;

    //         for (var i = 0; i < files.length; i++) 
    //         {
    //             var file = files[i];

    //                 if (file.size >= 2000000)
    //                 {
    //                     $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
    //                     filecount = filecount + 1;
                        
    //                     if(filecount == files.length)
    //                         {
    //                             $scope.imageloader=false;
    //                             $scope.imagecontent=true;
    //                         }
                        
    
    //                 }
    //                 else

    //                 {
                        
                   

    //                     var promises;
                            
    //                         promises = Upload.upload({
                                
    //                             url:'https://bodynbrain.s3.amazonaws.com/', //S3 upload url including bucket name
    //                             method: 'POST',
    //                             transformRequest: function (data, headersGetter) {
    //                             //Headers change here
    //                             var headers = headersGetter();
    //                             delete headers['Authorization'];
    //                             return data;
    //                             },
    //                             fields : {
    //                               key: 'uploads/pageimage/' + file.name, // the key to store the file on S3, could be file name or customized
    //                               AWSAccessKeyId: Config.AWSAccessKeyId,
    //                               acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
    //                               policy: Config.policy, // base64-encoded json policy (see article below)
    //                               signature: Config.signature, // base64-encoded signature based on policy string (see article below)
    //                               "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
    //                             },
    //                             file: file
    //                         })
    //                             promises.then(function(data){

    //                                 filecount = filecount + 1;
    //                                 filename = data.config.file.name;
    //                                 var fileout = {
    //                                     'imgfilename' : filename
    //                                 };
    //                                 $http({
    //                                     url: Config.ApiURL + "/pages/saveimage",
    //                                     method: "POST",
    //                                     headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    //                                     data: $.param(fileout)
    //                                 }).success(function (data, status, headers, config) {
    //                                     loadimages();
    //                                     if(filecount == files.length)
    //                                     {
    //                                         $scope.imageloader=false;
    //                                         $scope.imagecontent=true;
    //                                     }
                                        
    //                                 }).error(function (data, status, headers, config) {
    //                                         $scope.imageloader=false;
    //                                         $scope.imagecontent=true;
    //                                 });
                                    
    //                             });


    //              }


                            
    //         }
    //     }
        
          
    // };

    //     $scope.putimagestobanner = function(filename){
    //        pathimage = filename;
    //        pathimages();
    //        $modalInstance.dismiss('cancel');
    //     }

    //     $scope.cancel = function() {
    //         $modalInstance.dismiss('cancel');
    //     };

    // };

    

})