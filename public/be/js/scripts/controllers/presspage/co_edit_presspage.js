'use strict';

/* Controllers */

app.controller('Editpresspage', function($scope, $state, Upload ,$modal,$q, $http, Config, Createnews, ManagenewsFactory, $stateParams, $window,$sce, $localStorage, imgUpload){

    $scope.pressban = {};
    $scope.pressthumb = {};

    //Editing Slugs
    $scope.editslug = false;
    $scope.editnewsslug = function(){
        $scope.editslug = true;
    }
    $scope.cancelnewsslug = function(title){
        $scope.editslug = false;
        $scope.onnewstitle(title)
    }
    $scope.setslug = function(){
        $scope.editslug = false;
    }
    $scope.clearslug = function(title){
        $scope.editslug = false;
        $scope.onnewstitle(title);
    }
    $scope.onslugs = function(text1){
        $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
        validateSlugs();
    }


    var returnYoutubeThumb = function(item){
        var x= '';
        var thumb = {};
        if(item){
            var newdata = item;
            var x;
            x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
            thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
            thumb['yid'] = x[1];
            return x[1];
        }else{
            return x;
        }
    }

    // End Editing Slugs
    var validateSlugs = function(){
        // I will change this to validate if duplicate slugs
        $http({
            url: Config.ApiURL + "/newsslugss/validate/"+ $scope.news.slugs,
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            if(data>=1){
                $scope.invalidtitle = true;
                $scope.formpage.$invalid = true;
            }else{
                $scope.invalidtitle = false;
            }
        })
    }

    Createnews.loadcenter(function(data){
     $scope.newslocation = data;
 });

    $scope.imageloader=false;
    $scope.imagecontent=true;

    $http({
        url: Config.ApiURL + "/presspage/newsedit/" + $stateParams.newsid ,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {

        Createnews.loadcategory(function(data1){
         $scope.category = data1;
         $scope.datatitle = $sce.trustAsHtml(data.title)
         $scope.news = data;
         if($scope.news.banner.substring(1,7) == 'iframe'){
            $scope.pressban.type = 'video';
            $scope.pressban.thumb = 'http://img.youtube.com/vi/' + returnYoutubeThumb($scope.news.banner) + '/hqdefault.jpg';
            $scope.amazonpath = { url : $scope.news.banner } 
         }else {
            $scope.pressban.type = 'image';
            $scope.amazonpath = $scope.news.banner; 
         }

         if($scope.news.thumbnail.substring(1,7) == 'iframe'){
            $scope.pressthumb.type = 'video';
            $scope.pressthumb.thumb = 'http://img.youtube.com/vi/' + returnYoutubeThumb($scope.news.thumbnail) + '/hqdefault.jpg';
            $scope.amazonpath2 = { url : $scope.news.thumbnail } 
         }else {
            $scope.pressthumb.type = 'image';
            $scope.amazonpath2 = $scope.news.thumbnail; 
         }

         $scope.defaulttitle = data.title;
         $scope.pathimagelogo = data.logo;
         for (var i = 0; i < data1.length; i++) 
         {
            if(data.category == data1[i].categoryid){
                $scope.news.category = data1[i];
            }

        }
    });
        
    }).error(function (data, status, headers, config) {
        $scope.status = status;
    });

    $scope.news = {title: "" };

    var orinews = angular.copy($scope.news);

    $scope.onnewstitle = function convertToSlug(Text)
    {

        if(Text=="" || Text==null){
            $scope.news.slugs = " ";
        }
        else
        {
            var text1 = Text.replace(/[^\w ]+/g,'');
            $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));

            if($scope.defaulttitle != Text){
                $http({
                    url: Config.ApiURL + "/newsslugs/validate/"+ $scope.news.slugs,
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).success(function(data) {
                    if(data>=1){
                        $scope.invalidtitle = true;
                        $scope.formpage.$invalid = true;
                    }else{
                        $scope.invalidtitle = false;
                    }
                })
            }else{
                $scope.invalidtitle= false;
            }
        }

    }


    $scope.cutlink = function convertToSlug(Text){
        var texttocut = Config.amazonlink + '/uploads/newsimage/';
        $scope.news.banner = Text.substring(texttocut.length); 
    }

    // UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////
    $scope.showimageList = function(size,path){
       var amazon = $scope.amazon;
        $scope.type = "banner";
       var modalInstance = $modal.open({
        templateUrl: 'newsimagelist.html',
        controller: imagelistCTRL,
        size: size,
        resolve: {
            path: function() {
                return amazon
            },
            apiUrl_imgload: function() {
                return "/press/listbanner"
            },
            apiUrl_upload: function() {
                return "/press/bannersave"
            },
            apiUrl_delete: function() {
                return "/press/deletebanner/"
            },
            apiUrl_video_save: function() {
                return "/press/savebanner"
            }
        }
    });
   }

   $scope.showimageListthumb = function(size,path){
       var amazon = $scope.amazon;
        $scope.type = "thumbnail";
       var modalInstance = $modal.open({
        templateUrl: 'newsimagelist.html',
        controller: imagelistCTRL,
        size: size,
        resolve: {
            path: function() {
                return amazon
            },
            apiUrl_imgload: function() {
                return "/press/listthumbnail"
            },
            apiUrl_upload: function() {
                return "/press/thumbnailsave"
            },
             apiUrl_delete: function() {
                return "/press/deletethumbnail/"
            },
            apiUrl_video_save: function() {
                return "/press/savethumbnailvid"
            }
        }
    });
   }

   $scope.stat="1";
   var pathimage = "";
   var pathimages = function(){
        if($scope.type == "banner"){
            if(pathimage.type == 'video'){
                $scope.amazonpath=pathimage;
            }else {
                $scope.amazonpath=pathimage.filename;
            }
            $scope.pressban = pathimage;
        } else {
            if(pathimage.type == 'video'){
                $scope.amazonpath2=pathimage;
            }else {
                $scope.amazonpath2=pathimage.filename;
            }
            $scope.pressthumb = pathimage;
        }
    }

var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path, apiUrl_imgload ,apiUrl_upload, apiUrl_delete, imgUpload, Createnews, apiUrl_video_save){
        $scope.showbtn = true;
        $scope.videoenabled = true;
        $scope.mediaGallery = 'all';
        $scope.amazonpath= path;
        var list = [];
        $scope.press = true;
        var _noimg_imggallery = function(_noimage, _imggallery){
            $scope.noimage    = _noimage;
            $scope.imggallery = _imggallery;
        }
        _noimg_imggallery(false, false);

        var _loader_content = function(_loader, _content){
            $scope.imageloader=_loader;
            $scope.imagecontent=_content;
        }
        _loader_content(false, true);

        var countgallery = function(data) {
            $scope.alllength = data.length;
            $scope.imagelength = 0;
            $scope.videoslength = 0;

            angular.forEach(data, function(value, key) {
                if(value['type'] == 'image'){
                    $scope.imagelength++;
                } else {
                    $scope.videoslength++;
                }
            });
        }

        $scope.click = function(show) {
            $scope.imagelist = [];
            $scope.mediaGallery = show;
            if(show == 'all'){
                $scope.imagelist = list;
            } else if(show == 'images'){
                angular.forEach(list, function(value, key) {
                    if(value['type'] == 'image'){
                        $scope.imagelist.push(value);
                    }   
                });
            } else if(show =='videos'){
                angular.forEach(list, function(value, key) {
                    if(value['type'] == 'video'){
                        value['thumb'] = 'http://img.youtube.com/vi/' + returnYoutubeThumb(value['url']) + '/hqdefault.jpg';
                        $scope.imagelist.push(value);
                    }   
                });
            }
        }

        var parseQueryString = function(queryString) {
            var params = {},
                queries, temp, i, l;

            // Split into key/value pairs
            queries = queryString.split("&");

            // Convert the array of strings into an object
            for (i = 0, l = queries.length; i < l; i++) {
                temp = queries[i].split('=');
                params[temp[0]] = temp[1];
            }

            return params;
        };

        $scope.savevid = function(video) {
            if (video !== undefined) {
                var queryString = video.substring(video.indexOf('?') + 1);
                var param = parseQueryString(queryString);
                if (param.hasOwnProperty('v')) {
                    var link = { 'vid' : '<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" style="width:500px; height:300px;" allowfullscreen></iframe>' }
                    Createnews.saveVid(apiUrl_video_save, link,function(data){
                        if(data.hasOwnProperty('success')){
                            loadimages('videos');
                            $scope.video='';
                            $scope.invalidvideo = false;
                        }
                    });
                } else {
                    $scope.invalidvideo = true;
                    $scope.video='';
                }
            } else {
                $scope.video = '';
            }
        };

        var filtervid = function() {
            angular.forEach($scope.imagelist, function(value, key) {
                $scope.imagelist[key].thumb = 'http://img.youtube.com/vi/' + returnYoutubeThumb(value['url']) + '/hqdefault.jpg';
            })
        }

        var loadimages = function(show) {
            Createnews.loadimgbanner(apiUrl_imgload, function(_rdata){
                if(_rdata.error == "NOIMAGE" ){
                    _noimg_imggallery(true, false);
                }else{
                   _noimg_imggallery(false, true);
                   list = _rdata;
                   $scope.click(show);
                   countgallery(_rdata);
                   filtervid();
               }
           })

        }
        loadimages($scope.mediaGallery);
        $scope.path=function(path){  
            pathimage=path;
           pathimages();
           $modalInstance.dismiss('cancel');
       }


       // $scope.upload = function(files) {$scope.upload(files)};


       $scope.upload = function (files){
         $scope.imagealert = []; 

         imgUpload.upload(files, Upload, "uploads/newsimage/", apiUrl_upload, 
            function(_alert){
                $scope.imagealert.push(_alert);
            }, 
            function(_loader_cont){
                _loader_content(_loader_cont.loader, _loader_cont.cont);
            }, 
            function(_rdata){
               loadimages($scope.mediaGallery);
                // $scope.videoenabled = true;
                // $scope.mediaGallery = 'all';
           }
        );

     };

     $scope.delete = function(id){
        var modalInstance = $modal.open({
            templateUrl: 'delete.html',
            controller: deleteCTRL,
            resolve: {
                imgid: function(){
                    return id
                },
                show: function() {
                    return $scope.mediaGallery
                }
            }
        });

    }

var deleteCTRL = function($scope, $modalInstance, imgid, show) {
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };
    $scope.message="Are you sure do you want to delete this Photo?";
    $scope.ok = function() {
        $http({
            url: Config.ApiURL+""+apiUrl_delete+""+ imgid,
            method: "get",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).success(function(data, status, headers, config) {
            console.log(data);
            $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
            loadimages(show);
            $modalInstance.close();
        }).error(function(data, status, headers, config) {
            loadimages();
            $modalInstance.close();
            $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
        });
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
}

$scope.closeAlert_image = function (index) {
    $scope.imagealert.splice(index, 1);
};
$scope.cancel = function() {$modalInstance.dismiss('cancel');};
}
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////



///LOGO START
$scope.ListLogo = function(size,path){
    var amazon = $scope.amazon;
    var modalInstance = $modal.open({
        templateUrl: 'newsimagelist.html',
        controller: imageLogoCTRL,
        size: size,
        resolve: {
           path: function() {
            return amazon
        },
        apiUrl_imgload: function() {
            return "/press/listlogo"
        },
        apiUrl_upload: function() {
            return "/press/logosave"
        },
        apiUrl_delete: function() {
            return "/press/deletelogo/"
        }
    }
});
}
$scope.stat="1";
var pathimage_logo = "";
var pathimagelogo = function(){
    $scope.pathimagelogo=pathimage_logo;
}
var imageLogoCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path, apiUrl_imgload ,apiUrl_upload, apiUrl_delete, imgUpload, Createnews ){
    $scope.amazonpath= path;
    var _noimg_imggallery = function(_noimage, _imggallery){
        $scope.noimage    = _noimage;
        $scope.imggallery = _imggallery;
    }
    _noimg_imggallery(false, false);
    var _loader_content = function(_loader, _content){
        $scope.imageloader=_loader;
        $scope.imagecontent=_content;
    }
    _loader_content(false, true);
    var loadimages = function() {
        Createnews.loadimgbanner(apiUrl_imgload, function(_rdata){
            if(_rdata.error == "NOIMAGE" ){
                _noimg_imggallery(true, false);
            }else{
             _noimg_imggallery(false, true);
             $scope.imagelist = _rdata;
         }
     })
    }
    loadimages();
    $scope.path=function(path){
     var texttocut = Config.amazonlink + '/uploads/newsimage/';
     var newpath = path.substring(texttocut.length); 
     pathimage_logo = newpath;
     pathimagelogo();
     $modalInstance.dismiss('cancel');
 }
 $scope.upload = function(files) {$scope.upload(files)};
 $scope.upload = function (files){
   $scope.imagealert = []; 
   imgUpload.upload(files, Upload, "uploads/newsimage/", apiUrl_upload, 
    function(_alert){
        $scope.imagealert.push(_alert);
    }, 
    function(_loader_cont){
        _loader_content(_loader_cont.loader, _loader_cont.cont);
    }, 
    function(_rdata){
     loadimages();
 });
};

$scope.delete = function(id){
    var modalInstance = $modal.open({
        templateUrl: 'delete.html',
        controller: deleteCTRL,
        resolve: {imgid: function(){return id}}
    });
}
var deleteCTRL = function($scope, $modalInstance, imgid) {
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };
    $scope.message="Are you sure do you want to delete this Photo?";
    $scope.ok = function() {
        $http({
            url: Config.ApiURL+""+apiUrl_delete+""+ imgid,
            method: "get",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).success(function(data, status, headers, config) {
            console.log(data);
            $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
            loadimages();
            $modalInstance.close();
        }).error(function(data, status, headers, config) {
            loadimages();
            $modalInstance.close();
            $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
        });
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
}

$scope.closeAlert_image = function (index) {
    $scope.imagealert.splice(index, 1);
};
$scope.cancel = function() {$modalInstance.dismiss('cancel');};
};
///LOGO end

$scope.saveNews = function(news){
    $scope.alerts = [];
    $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1);};
    $scope.isSaving = true;

    $http({
        url: Config.ApiURL + "/presspage/edit",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(news)
    }).success(function (data, status, headers, config) {
        console.log(data);
        $scope.isSaving = false;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({type: 'success', msg: 'Press successfully Updated!'});
        $scope.category = news.category;
        Createnews.loadcategory(function(data1){
            $scope.category = data1;

            for (var i = 0; i < data1.length; i++) 
            {
                if(news.category == data1[i].categoryid)
                {
                    $scope.news.category = data1[i];

                }

            }
        });
    }).error(function (data, status, headers, config) {
        $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
    });
}

$scope.imagegallery = function(size, path){
    var amazon = $scope.amazon;
    var modalInstance = $modal.open({
        templateUrl: 'newsimagelist2.html',
        controller: imagegalleryCTRL,
        size: size,
        resolve: {
            path: function() {
                return amazon
            }
        }
    });
}

var imagegalleryCTRL = function($modalInstance, $scope, path) {
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };

    $scope.amazonpath= path;
    $scope.imageloader=false;
    $scope.imagecontent=true;
    $scope.noimage = false;
    $scope.imggallery = false;

    var loadimages = function() {
        $http({
            url: Config.ApiURL + "/news/listimages",
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            if(data.error == "NOIMAGE" ){
                $scope.imggallery=false;
                $scope.noimage = true;
            } else {
                $scope.noimage = false;
                $scope.imggallery=true;
                $scope.imagelist = data;
            }
        }).error(function(data) {
        });
    }
    loadimages();

    $scope.getimageurl = function(filename) {

        var modalInstance = $modal.open({
            templateUrl: 'imagesrc.html',
            controller: imagesrcCTRL,
            resolve: {
                imagesource: function() {
                    return path + "/uploads/newsimage/" + filename;
                }
            }
        });
    }

    var imagesrcCTRL = function($scope, imagesource, $modalInstance) {
        $scope.imgsrc = imagesource;

        $scope.ok = function() {
            $modalInstance.dismiss('cancel');
        }

    }

    $scope.upload = function (files) 
    {
        $scope.imagealert = [];
        var filename;
        var filecount = 0;
        if (files && files.length) 
        {

            $scope.imageloader=true;
            $scope.imagecontent=false;

            for (var i = 0; i < files.length; i++) 
            {
                var file = files[i];

                    if (file.size >= 6000000) //6MB
                    {
                        $scope.imagealert.push({type: 'danger', msg: 'File ' + file.name + ' is too big. (Maximum of 6MB)'});
                        $scope.imageloader=false;
                        $scope.imagecontent=true;
                    }
                    else
                    {
                        var promises;

                        var fileExtension = '.' + file.name.split('.').pop();

                          // rename the file with a sufficiently random value and add the file extension back
                          var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + i + fileExtension;

                          promises = Upload.upload({

                                       url:Config.amazonlink, //S3 upload url including bucket name
                                       method: 'POST',
                                       transformRequest: function (data, headersGetter) {
                                        //Headers change here
                                        var headers = headersGetter();
                                        delete headers['Authorization'];
                                        return data;
                                    },
                                    fields : {
                                          key: 'uploads/newsimage/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                          AWSAccessKeyId: Config.AWSAccessKeyId,
                                          acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                          policy: Config.policy, // base64-encoded json policy (see article below)
                                          signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                          "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                      },
                                      file: file
                                  })

                        promises.then(function(data){

                            filecount = filecount + 1;

                            filename = data.config.file.name;
                            var fileout = {
                                'imgfilename' : renamedFile
                            };
                            $http({
                                url: Config.ApiURL + "/news/saveimage",
                                method: "POST",
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                data: $.param(fileout)
                            }).success(function (data, status, headers, config) {
                                if(filecount == files.length)
                                {
                                    $scope.imageloader=false;
                                    $scope.imagecontent=true;
                                    loadimages();
                                }

                            }).error(function (data, status, headers, config) {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            });

                        });
                    }
            }
        }
};

        //DELETE IMAGE INSIDE IMAGE GALLERY (MODAL)
        $scope.delete = function(id){
            var modalInstance = $modal.open({
                templateUrl: 'delete.html',
                controller: deleteCTRL,
                resolve: {
                    imgid: function() {
                        return id
                    }
                }

            });

        }

        var deleteCTRL = function($scope, $modalInstance, imgid) {

            $scope.alerts = [];

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.message="Are you sure do you want to delete this Photo?";
            $scope.ok = function() {
                $http({
                    url: Config.ApiURL+"/news/deletenewsimg/"+ imgid,
                    method: "get",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).success(function(data, status, headers, config) {
                    console.log(data);
                    $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
                    loadimages();
                    $modalInstance.close();
                }).error(function(data, status, headers, config) {
                    loadimages();
                    $modalInstance.close();
                    $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
                });
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        }
    }
    
    


    $scope.today = function () {
        $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
        $scope.dt = null;

    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.previewNews = function(news){

        $localStorage.newprev = news;
        console.log($localStorage.newprev);
        $window.open('/bnb-buzz/preview', 'bar');
    }


})