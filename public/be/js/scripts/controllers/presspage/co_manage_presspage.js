'use strict';

/* Controllers */

app.controller('ManagPresspageCtrl', function($scope, $state ,$q, $http, Config, $log, ManagenewsFactory, $interval, $modal){

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.data = {};
    var num = 9;
    var off = 1;
    var keyword = null;
    $scope.currentstatusshow = '';


    var loadlist  = function(){
        ManagenewsFactory.loadlist(num,off, keyword, function(data){
            $scope.data = data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        });
    }

    loadlist();

    $scope.search = function (keyword) {
        var off = 1;
        ManagenewsFactory.loadlist('9','1', keyword, function(data){
          $scope.data = data;
          $scope.maxSize = 5;
          $scope.bigTotalItems = data.total_items;
          $scope.bigCurrentPage = data.index;
        });

    }
    $scope.resetsearch  = function(){
         $scope.searchtext = undefined;
         loadlist();
     };
    $scope.numpages = function (off, keyword) {
        ManagenewsFactory.loadlist('9', off, $scope.searchtext, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });
    }

    $scope.setPage = function (pageNo) {
        ManagenewsFactory.loadlist(num,pageNo, $scope.searchtext, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });
    };

    $scope.setstatus = function (pressid) {
      ManagenewsFactory.pressstatus(pressid, function(data) {
          if(data.error != true) {
            ManagenewsFactory.loadlist(num,off, keyword, function(data){
              $scope.data = data;
              $scope.maxSize = 5;
              $scope.bigTotalItems = data.total_items;
              $scope.bigCurrentPage = data.index;
            });
          }
      });
    }

    $scope.deletenews = function(newsid) {
        var modalInstance = $modal.open({
            templateUrl: 'newsDelete.html',
            controller: newsDeleteCTRL,
            resolve: {
                newsid: function() {
                    return newsid
                }
            }
        });
    }

    var loadpage = function(){

        ManagenewsFactory.loadlist(num,$scope.bigCurrentPage, $scope.searchtext, function(data){
                        $scope.data = data;
                        $scope.maxSize = 5;
                        $scope.bigTotalItems = data.total_items;
                        $scope.bigCurrentPage = data.index;
                });

    }


    var successloadalert = function(){
       $scope.alerts.splice(0, 1);
       $scope.alerts.push({ type: 'success', msg: 'News successfully Deleted!' });
   }

    var errorloadalert = function(){
            $scope.alerts.push({ type: 'danger', msg: 'Something went wrong News not Deleted!' });
    }



    var newsDeleteCTRL = function($scope, $modalInstance, newsid) {
        $scope.msg = "Are you sure you want to delete this Press?";
        $scope.ok = function() {
            $http({
                url: Config.ApiURL + "/pressgage/delete/" + newsid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {
                console.log(data);
                loadpage();
                $modalInstance.close();
                successloadalert();

            }).error(function(data, status, headers, config) {
                loadpage();
                $modalInstance.close();
                errorloadalert();
            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };


        };


     $scope.deletenewscenter = function(newsid) {
        var modalInstance = $modal.open({
            templateUrl: 'newsCenterDelete.html',
            controller: newsCenterDeleteCTRL,
            resolve: {
                newsid: function() {
                    return newsid
                }
            }
        });
    }

    var newsCenterDeleteCTRL = function($scope, $modalInstance, newsid) {
        $scope.ok = function() {
            $http({
                url: Config.ApiURL + "/news/newscenterdelete/" + newsid,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {

                loadpage();
                $modalInstance.close();
                successloadalert();

            }).error(function(data, status, headers, config) {
                loadpage();
                $modalInstance.close();
                errorloadalert();
            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };


        };


    var newsEditCTRL = function($scope, $modalInstance, newsid, $state) {
        $scope.newsid = newsid;
        $scope.msg = "Are you sure you want to edit this Press?";
        $scope.ok = function(newsid) {
            $scope.newsid = newsid;
            $state.go('edit_presspage', {newsid: newsid });
                $modalInstance.dismiss('cancel');
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }

    $scope.editnews = function(newsid) {
        $scope.newsid
        var modalInstance = $modal.open({
            templateUrl: 'newsEdit.html',
            controller: newsEditCTRL,
            resolve: {
                newsid: function() {
                    return newsid
                }
            }
        });
    }




})
