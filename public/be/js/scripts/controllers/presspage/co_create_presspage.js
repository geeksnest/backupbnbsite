'use strict';

/* Controllers */

app.controller('PresspageCtrl', function($scope, $state, Upload ,$q, $http, Config,$modal, $window, Createnews, $localStorage, imgUpload, $timeout){
    $scope.imageloader=false;
    $scope.imagecontent=true;
    $scope.news = {};
    $scope.editslug = false;
    $scope.editnewsslug = function(){
        $scope.editslug = true;
    }
    $scope.cancelnewsslug = function(title){
        $scope.editslug = false;
        $scope.onnewstitle(title)
    }
    $scope.setslug = function(){
        $scope.editslug = false;
    }
    $scope.clearslug = function(title){
        $scope.editslug = false;
        $scope.onnewstitle(title);
    }
    $scope.onslugs = function(text1){
        $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
        validateSlugs();
    }
    // End Editing Slugs

    var orinews = angular.copy($scope.news);

    var validateSlugs = function(){
        $http({
            url: Config.ApiURL + "/newsslugs/validate/"+ $scope.news.slugs,
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            if(data>=1){
                $scope.invalidtitle = true;
                $scope.formpage.$invalid = true;
            }else{
                $scope.invalidtitle = false;
            }
        })
    }

    $scope.onnewstitle = function convertToSlug(Text){
        if(Text=="" || Text==null) {
            $scope.news.slugs = " ";
        } else {
            var text1 = Text.replace(/[^\w ]+/g,'');
            $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
            validateSlugs()

        }
    }

    $scope.cutlink = function convertToSlug(Text){
        var texttocut = Config.amazonlink + '/uploads/newsimage/';
        $scope.news.banner = Text.substring(texttocut.length); 
    }

    var loadcategory = function() {
        Createnews.loadcategory(function(data){ $scope.category = data; $scope.news.category = data[0];});
    }

    loadcategory();

    // Createnews.loadcenter(function(data){$scope.newslocation = data;});

    $scope.closeAlert = function (index) {$scope.alerts.splice(index, 1); };

    $scope.saveNews = function(news){
        $scope.isSaving = true;
        news['category'] = $scope.news.category.categoryid;
        $scope.alerts = [];

        $http({
            url: Config.ApiURL + "/presspage/create",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(news)
        }).success(function (data, status, headers, config) {
            if(!data.hasOwnProperty('error')){
                $scope.isSaving = false;
                $scope.alerts.push({type: 'success', msg: 'Press successfully saved!'});
                $scope.amazonpath ="";
                $scope.amazonpath2 ="";
                $scope.pathimagelogo = "";
                $scope.pressthumb = {};
                $scope.pressban = {};
                $scope.formpage.$setPristine();
                $scope.news = { status: 1, date: $scope.news.date }
                loadcategory();
            }
        }).error(function (data, status, headers, config) {
            $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
        });

    }

    $scope.previewNews = function(news){
        $localStorage.newprev = news;
        console.log($localStorage.newprev);

        $window.open('/bnb-buzz/preview', 'bar');
    }

    $scope.ListBanner = function(size,path){
        var amazon = $scope.amazon;
        $scope.type = "banner";
        var modalInstance = $modal.open({
            templateUrl: 'newsimagelist.html',
            controller: imagelistCTRL,
            size: size,
            resolve: {
                path: function() {
                    return amazon
                },
                apiUrl_imgload: function() {
                    return "/press/listbanner"
                },
                apiUrl_upload: function() {
                    return "/press/bannersave"
                },
                 apiUrl_delete: function() {
                    return "/press/deletebanner/"
                },
                apiUrl_video_save: function() {
                    return "/press/savebanner"
                }
            }
        });
    }

    $scope.stat="1";
    var pathimage = "";
    var pathimages = function(){
        if($scope.type == "banner"){
            if(pathimage.type == 'video'){
                $scope.amazonpath=pathimage;
            }else {
                $scope.amazonpath=pathimage.filename;
            }
            $scope.pressban = pathimage;
        } else {
            if(pathimage.type == 'video'){
                $scope.amazonpath2=pathimage;
            }else {
                $scope.amazonpath2=pathimage.filename;
            }
            $scope.pressthumb = pathimage;
        }
    }

    $scope.ListThumbnail = function(size,path){
        var amazon = $scope.amazon;
        $scope.type = "thumbnail";
        var modalInstance = $modal.open({
            templateUrl: 'newsimagelist.html',
            controller: imagelistCTRL,
            size: size,
            resolve: {
                path: function() {
                    return amazon
                },
                apiUrl_imgload: function() {
                    return "/press/listthumbnail"
                },
                apiUrl_upload: function() {
                    return "/press/thumbnailsave"
                },
                 apiUrl_delete: function() {
                    return "/press/deletethumbnail/"
                },
                apiUrl_video_save: function() {
                    return "/press/savethumbnailvid"
                }
            }
        });
    }


    var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path, apiUrl_imgload ,apiUrl_upload, apiUrl_delete, imgUpload, Createnews, apiUrl_video_save ){
        $scope.showbtn = true;
        $scope.videoenabled = true;
        $scope.mediaGallery = 'all';
        $scope.amazonpath= path;
        var list = [];
        $scope.press = true;
        var _noimg_imggallery = function(_noimage, _imggallery){
            $scope.noimage    = _noimage;
            $scope.imggallery = _imggallery;
        }
        _noimg_imggallery(false, false);

        var _loader_content = function(_loader, _content){
            $scope.imageloader=_loader;
            $scope.imagecontent=_content;
        }
        _loader_content(false, true);

        var countgallery = function(data) {
            if(data != null){
                $scope.alllength = 0;
                $scope.imagelength = 0;
                $scope.videoslength = 0;

                angular.forEach(data, function(value, key) {
                    if(value['type'] == 'image'){
                        $scope.imagelength++;
                    } else {
                        $scope.videoslength++;
                    }
                    $scope.alllength++;
                });
            }
        }

        $scope.click = function(show) {
            $scope.imagelist = [];
            $scope.mediaGallery = show;
            if(show == 'all'){
                $scope.imagelist = list;
            } else if(show == 'images'){
                angular.forEach(list, function(value, key) {
                    if(value['type'] == 'image'){
                        $scope.imagelist.push(value);
                    }   
                });
            } else if(show =='videos'){
                angular.forEach(list, function(value, key) {
                    if(value['type'] == 'video'){
                        value['thumb'] = 'http://img.youtube.com/vi/' + returnYoutubeThumb(value['url']) + '/hqdefault.jpg';
                        $scope.imagelist.push(value);
                    }   
                });
            }
        }

        var parseQueryString = function(queryString) {
            var params = {},
                queries, temp, i, l;

            // Split into key/value pairs
            queries = queryString.split("&");

            // Convert the array of strings into an object
            for (i = 0, l = queries.length; i < l; i++) {
                temp = queries[i].split('=');
                params[temp[0]] = temp[1];
            }

            return params;
        };

        $scope.savevid = function(video) {
            if (video !== undefined) {
                var queryString = video.substring(video.indexOf('?') + 1);
                var param = parseQueryString(queryString);
                if (param.hasOwnProperty('v')) {
                    var link = { 'vid' : '<iframe src="https://www.youtube.com/embed/' + param.v + '" frameborder="0" style="width:500px; height:300px;" allowfullscreen></iframe>' }
                    Createnews.saveVid(apiUrl_video_save, link,function(data){
                        if(data.hasOwnProperty('success')){
                            loadimages('videos');
                            $scope.video='';
                            $scope.invalidvideo = false;
                        }
                    });
                } else {
                    $scope.invalidvideo = true;
                    $scope.video='';
                }
            } else {
                $scope.video = '';
            }
        };

        var returnYoutubeThumb = function(item){
            var x= '';
            var thumb = {};
            if(item){
                var newdata = item;
                var x;
                x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
                thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
                thumb['yid'] = x[1];
                return x[1];
            }else{
                return x;
            }
        }

        var filtervid = function() {
            angular.forEach($scope.imagelist, function(value, key) {
                $scope.imagelist[key].thumb = 'http://img.youtube.com/vi/' + returnYoutubeThumb(value['url']) + '/hqdefault.jpg';
            })
        }

        var loadimages = function(show) {
            Createnews.loadimgbanner(apiUrl_imgload, function(_rdata){
               _noimg_imggallery(false, true);
               list = _rdata;
               $scope.click(show);
               countgallery(_rdata);
               filtervid();
           })

        }
        loadimages($scope.mediaGallery);
        $scope.path=function(path){  
            pathimage=path;
           pathimages();
           $modalInstance.dismiss('cancel');
       }


       // $scope.upload = function(files) {$scope.upload(files)};


       $scope.upload = function (files){
         $scope.imagealert = []; 

         imgUpload.upload(files, Upload, "uploads/newsimage/", apiUrl_upload, 
            function(_alert){
                $scope.imagealert.push(_alert);
            }, 
            function(_loader_cont){
                _loader_content(_loader_cont.loader, _loader_cont.cont);
            }, 
            function(_rdata){
               loadimages($scope.mediaGallery);
        });

     };

     $scope.delete = function(id){
        var modalInstance = $modal.open({
            templateUrl: 'delete.html',
            controller: deleteCTRL,
            resolve: {
                imgid: function(){
                    return id
                },
                show: function() {
                    return $scope.mediaGallery
                }
            }
        });

    }

    var deleteCTRL = function($scope, $modalInstance, imgid, show) {
        $scope.alerts = [];
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
        $scope.message="Are you sure do you want to delete this Photo?";
        $scope.ok = function() {
            $http({
                url: Config.ApiURL+""+apiUrl_delete+""+ imgid,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {
                console.log(data);
                $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
                loadimages(show);
                $modalInstance.close();
            }).error(function(data, status, headers, config) {
                loadimages();
                $modalInstance.close();
                $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
            });
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }

    $scope.closeAlert_image = function (index) {
        $scope.imagealert.splice(index, 1);
    };
    $scope.cancel = function() {$modalInstance.dismiss('cancel');};
};

    ///LOGO START
    $scope.ListLogo = function(size,path){
        var amazon = $scope.amazon;
        var modalInstance = $modal.open({
            templateUrl: 'newsimagelist.html',
            controller: imageLogoCTRL,
            size: size,
            resolve: {
                path: function() {
                    return amazon
                },
                apiUrl_imgload: function() {
                    return "/press/listlogo"
                },
                apiUrl_upload: function() {
                    return "/press/logosave"
                },
                 apiUrl_delete: function() {
                    return "/press/deletelogo/"
                }
            }
        });
    }
     $scope.stat="1";
    var pathimage_logo = "";
    var pathimagelogo = function(){
        $scope.pathimagelogo=pathimage_logo;
    }
    var imageLogoCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path, apiUrl_imgload ,apiUrl_upload, apiUrl_delete, imgUpload, Createnews ){
        $scope.amazonpath= path;
        var _noimg_imggallery = function(_noimage, _imggallery){
            $scope.noimage    = _noimage;
            $scope.imggallery = _imggallery;
        }
        _noimg_imggallery(false, false);

        var _loader_content = function(_loader, _content){
            $scope.imageloader=_loader;
            $scope.imagecontent=_content;
        }
        _loader_content(false, true);

        var loadimages = function() {
            Createnews.loadimgbanner(apiUrl_imgload, function(_rdata){
                if(_rdata.error == "NOIMAGE" ){
                    _noimg_imggallery(true, false);
                }else{
                   _noimg_imggallery(false, true);
                   $scope.imagelist = _rdata;
               }
           })

        }
        loadimages();
        $scope.path=function(path){
           var texttocut = Config.amazonlink + '/uploads/newsimage/';
           var newpath = path.substring(texttocut.length); 
           pathimage_logo = newpath;
           pathimagelogo();
           $modalInstance.dismiss('cancel');
       }


       $scope.upload = function(files) {$scope.upload(files)};

       $scope.upload = function (files){
         $scope.imagealert = []; 
         imgUpload.upload(files, Upload, "uploads/newsimage/", apiUrl_upload, 
            function(_alert){
                $scope.imagealert.push(_alert);
            }, 
            function(_loader_cont){
                _loader_content(_loader_cont.loader, _loader_cont.cont);
            }, 
            function(_rdata){
               loadimages();
           });

     };

     $scope.delete = function(id){
        var modalInstance = $modal.open({
            templateUrl: 'delete.html',
            controller: deleteCTRL,
            resolve: {imgid: function(){return id}}
        });

    }

    var deleteCTRL = function($scope, $modalInstance, imgid) {
        $scope.alerts = [];
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
        $scope.message="Are you sure do you want to delete this Photo?";
        $scope.ok = function() {
            $http({
                url: Config.ApiURL+""+apiUrl_delete+""+ imgid,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {
                console.log(data);
                $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
                loadimages();
                $modalInstance.close();
            }).error(function(data, status, headers, config) {
                loadimages();
                $modalInstance.close();
                $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
            });
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }

    $scope.closeAlert_image = function (index) {
        $scope.imagealert.splice(index, 1);
    };
    $scope.cancel = function() {$modalInstance.dismiss('cancel');};
};

  ///LOGO end












$scope.imagegallery = function(size, path){
    var amazon = $scope.amazon;
    var modalInstance = $modal.open({
        templateUrl: 'newsimagelist2.html',
        controller: imagegalleryCTRL,
        size: size,
        resolve: {path: function() {return amazon}}
    });
}

var imagegalleryCTRL = function($modalInstance, $scope, path) {

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };

    $scope.amazonpath= path;
    $scope.imageloader=false;
    $scope.imagecontent=true;
    $scope.noimage = false;
    $scope.imggallery = false;

    var loadimages = function() {
        $http({
            url: Config.ApiURL + "/news/listimages",
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            if(data.error == "NOIMAGE" ){
                $scope.imggallery=false;
                $scope.noimage = true;
            } else {
                $scope.noimage = false;
                $scope.imggallery=true;
                $scope.imagelist = data;
            }
        }).error(function(data) {
        });
    }
    loadimages();

    $scope.getimageurl = function(filename) {

        var modalInstance = $modal.open({
            templateUrl: 'imagesrc.html',
            controller: imagesrcCTRL,
            resolve: {
                imagesource: function() {
                    return path + "/uploads/newsimage/" + filename;
                }
            }
        });
    }

    var imagesrcCTRL = function($scope, imagesource, $modalInstance) {
        $scope.imgsrc = imagesource;

        $scope.ok = function() {
            $modalInstance.dismiss('cancel');
        }

    }

    $scope.upload = function (files) {  
       $scope.imagealert = []; 
       imgUpload.upload(files, Upload, "uploads/newsimage/", "/news/saveimage", 
        function(_alert){
            $scope.imagealert.push(_alert);
        }, 
        function(_loader_cont){
            _loader_content(_loader_cont.loader, _loader_cont.cont);
        }, 
        function(_rdata){
           loadimages();
       });





   };


   $scope.delete = function(id){
    var modalInstance = $modal.open({
        templateUrl: 'delete.html',
        controller: deleteCTRL,
        resolve: {
            imgid: function() {
                return id
            }
        }

    });

}

var deleteCTRL = function($scope, $modalInstance, imgid) {

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.message="Are you sure do you want to delete this Photo?";
    $scope.ok = function() {
        $http({
            url: Config.ApiURL+"/news/deletenewsimg/"+ imgid,
            method: "get",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).success(function(data, status, headers, config) {
            console.log(data);
            $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
            loadimages();
            $modalInstance.close();
        }).error(function(data, status, headers, config) {
            loadimages();
            $modalInstance.close();
            $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
        });
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
}
}


    //DATE PICKER
    $scope.today = function () {$scope.dt = new Date();};
    $scope.today(); $scope.clear = function () {$scope.dt = null;};
    $scope.toggleMin = function () {$scope.minDate = $scope.minDate ? null : new Date();};
    $scope.toggleMin();
    $scope.open = function ($event) {$event.preventDefault();$event.stopPropagation();$scope.opened = true;};
    $scope.dateOptions = {formatYear: 'yy',startingDay: 1,class: 'datepicker'};
    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];


})