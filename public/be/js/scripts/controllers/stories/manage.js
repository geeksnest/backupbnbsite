'use strict';

app.controller('managestoriesCtrl', function ($scope, $http, $modal, Config, ManageStoryFactory){

	var viewStoryCTRL = function($scope, $modalInstance, storyid, $state) {

		$scope.storyid = storyid;
		$scope.ok = function(storyid) {
			$scope.storyid = storyid;
			$state.go('viewstory', {storyid: storyid });
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	};

	$scope.viewStory = function(storyid) {

        var modalInstance = $modal.open({
            templateUrl: 'viewStory.html',
            controller: viewStoryCTRL,
            resolve: {
                storyid: function() {
                    return storyid;
                }
            }
        });
    };

    $scope.deleteStory = function(storyid) {
        var modalInstance = $modal.open({
            templateUrl: 'deleteStory.html',
            controller: deleteStoryCTRL,
            resolve: {
                storyid: function() {
                    return storyid;
                }
            }
        });
    };

    var deleteStoryCTRL = function($scope, $modalInstance, storyid, $state) {

        $scope.storyid = storyid;
        $scope.ok = function(storyid) {
            ManageStoryFactory.deleteStory(storyid, function (data){
                paginate(off, keyword);
                $modalInstance.dismiss('cancel');
            });
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

	//START of PAGINATION
	$scope.data = {};
  var num     = 10;
  var off     = 1;
  var keyword = null;

	var paginate = function(off, keyword) {
        $http({
            url: Config.ApiURL + "/stories/managestories/" + num + '/' + off + '/' + keyword,
            method: "GET",
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        }).success(function(data, status, headers, config) {
            $scope.stories     = data;
            $scope.maxSize     = 5;
            $scope.TotalItems  = data.total_items;
            $scope.CurrentPage = data.index;

        }).error(function(data, status, headers, config) {
            $scope.status = status;
        });
    };

    paginate(off, keyword);

    $scope.search = function (searchkeyword) {
        // var off = 1;
				off     = 1;
        keyword = searchkeyword;
				paginate(off, searchkeyword);
    };

    $scope.clearsearch = function() {
    	// var off = 1;
			off               = 1;
			keyword           = null;
			$scope.searchtext = '';
    	paginate(off,null);
    };

    $scope.numpages = function (off, keyword) {
        paginate(off, keyword);
    };

    $scope.setPage = function (pageNo) {
			off = pageNo;
      paginate(pageNo, keyword);
    };
    //END of PAGINAION

    // var dropdowncenter = function(){
    //     ManageStoryFactory.dropdowncenterlist(num,off, keyword, userid, function(data) {
    //         $scope.centerdatalist = data;
    //     });
    // };
    // dropdowncenter();

		//this is substitute to the code that is commented just above, if nothing's wrong, feel free to remove the old code. TY
		//null
		ManageStoryFactory.dropdowncenterlist(num,off, keyword, userid, function(data) {
				$scope.centerdatalist = data;
		});

});
