'use strict';

app.controller('viewstoryCtrl', function ($scope, $http, $modal, $stateParams, Config, ViewStoryFactory, ManageStoryFactory, Factory, Upload) {
	var storydetails = function() {
		ViewStoryFactory.getInformation($stateParams.storyid, function(data){
			$scope.story   = data.story;
			$scope.centers = data.centers;
			if(data.story.optimized == null || data.story.optimized == 0) {
				$scope.optimized = false;
			} else { $scope.optimized = true; }
		});
	};
	storydetails();

	$scope.save = function(story, file) {
		if(file === undefined) {
				ViewStoryFactory.saveReviewedStory(story, function (data) {

					$scope.viewstoryform.$setPristine(true);
					storydetails();

					var element = {type:data.type , title: "Success Story: "+data.story.subject, msg: data.msg};
					Factory.Modal("tpl_okay", "sm", element, TPLOKAYCTRL);

				});
		} else { //profile picture is changed
				$scope.upload(story,file);
		}
	};

	$scope.processing = false;
	$scope.upload = function (story,files) {
		var filename;
		$scope.processing = true;
	  var filecount = 0;
	  if (files && files.length) {
	  	$scope.imageloader=true;
      $scope.imagecontent=false;
	    for(var i = 0; i < files.length; i++) {

	      var file = files[i];
	      if (file.size >= 2000000) {
	        // notification here that image size is too big!
	        filecount = filecount + 1;
	        if(filecount == files.length) {
	        	$scope.imageloader=false;
	        	$scope.imagecontent=true;
	        }
	      } else {
	        var promises;
	        promises = Upload.upload({
	          url: Config.amazonlink, //S3 upload url including bucket name
	          method: 'POST',
	          transformRequest: function (data, headersGetter) {
	            var headers = headersGetter();
	            delete headers['Authorization'];
	            return data;
	          },
	          fields : {
	            key: 'uploads/testimonialimages/' + file.name, // the key to store the file on S3, could be file name or customized
	            AWSAccessKeyId: Config.AWSAccessKeyId,
	            acl: 'private', // sets the access to the uploaded file in the bucket: private or public
	            policy: Config.policy, // base64-encoded json policy (see article below)
	            signature: Config.signature, // base64-encoded signature based on policy string (see article below)
	            "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
	          },
	          file: file
	        }); //added semi-colon

	        promises.then(function(data){

	          filecount = filecount + 1;
	          filename = data.config.file.name;

	          story['photo'] = filename;

	          ViewStoryFactory.saveReviewedStory(story, function (data){
	          	$scope.viewstoryform.$setPristine(true);
	          	storydetails();
	          	$scope.processing = false;
	          	var modalInstance = $modal.open({
	          		templateUrl: 'tpl_okay',
	          		controller: TPLOKAYCTRL,
	          		resolve: {
	          			element: function() {
	          				return {type:data.type , title: "Success Story: "+data.story.subject, msg: data.msg};
	          			}
	          		}
	          	});
						});
	        });
	      }
	    }
	  }
	};

	var TPLOKAYCTRL = function($scope, $modalInstance, data) {
		$scope.tpl_type = data.type;
		$scope.tpl_title = data.title;
		$scope.tpl_msg = data.msg;
		$scope.tpl_true = 'Okay';
		$scope.ok = function() {
			$modalInstance.dismiss('cancel');
		};
	};

	var deleteStoryCTRL = function($scope, $modalInstance, storyid, $state) {
	        $scope.storyid = storyid;
	        $scope.ok = function(storyid) {
	            ManageStoryFactory.deleteStory(storyid, function (data) {
	            $modalInstance.dismiss('cancel');
	            $state.go('managestories');
						});
	        };
	        $scope.cancel = function () {
	        $modalInstance.dismiss('cancel');
	    };
	};

	$scope.delete = function(storyid) {
		var modalInstance = $modal.open({
            templateUrl: 'deleteStory.html',
            controller: deleteStoryCTRL,
            resolve: {
                storyid: function() {
                    return storyid;
                }
            }
        });
	};

		$scope.alerts = [];

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.addmetatag = function(metatag, storyid) {
    	// var text1 = metatag.replace(/[^\w ]+/g,''); //(OLD)
    	var text1 = metatag.replace(/[^a-zA-Z0-9\s ,'&-]+/g,'');

    	// metatag = angular.lowercase(text1.replace(/ +/g,'-')); // (OLD)
      metatag = angular.lowercase(text1.replace(/ \s +/g,','));

    	var metaprop = { 'metatag': metatag, 'storyid': storyid };
    	ViewStoryFactory.addmetatag(metaprop, function(data) {
    		metatags();
    		$scope.metatag = '';
    		$scope.metaform.$setPristine(true);
    	});
    };

    var metatags = function () {
    	ViewStoryFactory.metatags($stateParams.storyid, function(data) {

			if(data.length <= 2 || data == null || data == 'null' || data == undefined) {
				$scope.empty_tag = false;
			} else {
				var tags = data;
    		 	tags = tags.slice(1, -1); //remove the double qoute
				tags = tags.replace(/^,/, ''); //remove the first comma
				tags = tags.split(',');
				$scope.empty_tag = true;
				$scope.metatags = tags;
			}
    	});
    };
    metatags();

    $scope.removemetatag = function(metatag, metatags) {
    	var indexof = metatags.indexOf(metatag);
    	if(indexof !== -1) {
    		metatags.splice(indexof, 1);
    	}
    	ViewStoryFactory.updatemetatags($stateParams.storyid, metatags, function(data){
    		console.log($scope.optimized = data.optimized);
    		// storydetails(); //details will be back to original if there are changes
    	});
    };

		var workshoptitles = function() {
			ViewStoryFactory.workshoptitles($stateParams.storyid, function(data){
				if(data.related == null || data.related == '' || data.related.length == 0) {
					$scope.empty = true;
				} else {
					data.related = data.related.replace(/^,/, '');
					data.related = data.related.split(',');
					for(var x=0; x<data.related.length; x++) {
						var indexof = data.titles.indexOf(data.related[x]);
						if(indexof !== -1) {
							data.titles.splice(indexof, 1);
						}
					}
					$scope.empty   = false;
					$scope.related = data.related;
				}
				$scope.workshoptitles = data.titles;
			});
		};

    workshoptitles();

    $scope.addrelated = function (title) {

    	ViewStoryFactory.addrelated($stateParams.storyid, title, function(data){
    		workshoptitles();
    	});
    };

		$scope.removerelated = function (title, related) {
			var indexof = related.indexOf(title);
			if(indexof !== -1) {
				related.splice(indexof, 1);
			}
			ViewStoryFactory.removerelated($stateParams.storyid, related, function(data){
				workshoptitles();
			});
		};

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.open1 = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = true;

    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };
});
