'use strict';

/* Controllers */

app.controller('ManageClassCtrl', function($scope, $modal, store, Login, ClassFactory){

  var num = 10;
  var off = 1;
  var keyword = null;

  $scope.alerts = [];

  $scope.closeAlert = function() {
    $scope.alerts = [];
  };

  var loadlist = function(num, off, keyword) {
      ClassFactory.get(num, off, keyword, function(data) {
        $scope.classes = data.data;
        $scope.maxSize = 10;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
      });
  };

  loadlist(num, off, keyword);

  var alertmsg = function(data) {
    $scope.closeAlert();
    if(data.hasOwnProperty('success')) {
      $scope.alerts.push({ type: 'success', 'msg': data.success });
    } else {
      $scope.alerts.push({ type: 'danger', 'msg': data.err });
    }
  };

  $scope.addclass = function() {
    var modalInstance = $modal.open({
        templateUrl: 'Classtpl.html',
        controller: function($scope, $modalInstance) {
          $scope.header = "Add Class";
          $scope.cancel = function() {
            $modalInstance.dismiss();
          };

          $scope.validateClass = function(id, classs) {
            ClassFactory.validateClass(id, classs, function(data) {
              $scope.exist = data.exist;
            });
          };

          $scope.ok = function(classs) {
            ClassFactory.add(classs, function(data) {
              if(data.hasOwnProperty('success')){
                loadlist(num, off, keyword);
              }
              $modalInstance.dismiss();
              alertmsg(data);
            });
          };
        }
    });
  };

  $scope.editClass = function(classdata) {
    var modalInstance = $modal.open({
      templateUrl: 'Classtpl.html',
      controller: function($scope, $modalInstance, classes) {
        $scope.header = "Edit Class";
        $scope.cl = classes;
        $scope.cancel = function() {
          $modalInstance.dismiss();
        };

        $scope.validateClass = function(id, classs) {
          ClassFactory.validateClass(id, classs, function(data) {
            $scope.exist = data.exist;
          });
        };

        $scope.ok = function(classs) {
          ClassFactory.editclass(classs, function(data) {
            if(data.hasOwnProperty('success')){
              loadlist(num, off, keyword);
            }
            $modalInstance.dismiss();
            alertmsg(data);
          });
        };
      },
      resolve: {
        classes: function() {
          return classdata;
        }
      }
    });
  };

  $scope.deleteClass = function(id) {
    var modalInstance = $modal.open({
      templateUrl: 'deleteclass.html',
      controller: function($scope, $modalInstance, id) {
        $scope.cancel = function() {
          $modalInstance.dismiss();
        };

        $scope.ok = function() {
          ClassFactory.deleteclass(id, function(data) {
            if(data.hasOwnProperty('success')){
              loadlist(num, off, keyword);
            }
            $modalInstance.dismiss();
            alertmsg(data);
          });
        };
      },
      resolve: {
        id: function() {
          return id;
        }
      }
    });
  };

  $scope.setPage = function(off) {
    loadlist(num, off, keyword);
  };

  $scope.search = function(keyword) {
    loadlist(10, 1, keyword);
    $scope.searchtext = undefined;
  };

  $scope.resetsearch = function() {
    loadlist(num, off, keyword);
  };
});
