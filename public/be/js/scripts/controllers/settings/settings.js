'use strict';

/* Controllers ni : Ryan jeric Sabado.*/

app.controller('Settings', function($scope, $state, Upload ,$q, $http, Config, $stateParams ,$modal,Settings){

  $scope.alerts = [];
      $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
  };

  $scope.data = {};
    var maintenance = function(data, status, headers, config) {
        $http({
            url: Config.ApiURL+"/settings/managesettings",
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.id = data.id;
            $scope.value1 = data.maintenance.value1;
            $scope.value2 = data.maintenance.value2;
            $scope.value3 = data.maintenance.value3;
            $scope.amazonpath = data.maintenance.logo;
            $scope.social = data.socialmedia;
            $scope.script=data.script;
        }).error(function (data, status, headers, config) {

        });
    };
    maintenance();


    $scope.savelogo = function(logo){
      $http({
        url: Config.ApiURL + "/settings/savedefaultlogo",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(logo)
      }).success(function (data, status, headers, config) {
       $scope.alerts.push({ type: 'success', msg: 'Logo successfully Change!' });
      }).error(function (data, status, headers, config) {

      });
    }

    $scope.savesettings = function (on){
        $http({
            url: Config.ApiURL+ "/settings/maintenanceon",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(on)
        }).success(function (data, status, headers, config) {
            window.location.reload();
        }).error(function (data, status, headers, config) {

        });
    };

    $scope.savesettingsoff = function (){
        $http({
            url: Config.ApiURL + "/settings/maintenanceoff",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}

        }).success(function (data, status, headers, config) {
            window.location.reload();
        }).error(function (data, status, headers, config) {

        });
    };

    //Google Analytics
    $scope.save= function(info){
            $http({
                url: Config.ApiURL + "/settings/googleanalytics",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(info)
            }).success(function (data, status, headers, config) {
                $scope.alerts.push({type: 'success', msg: 'Google Analytics Save!'});
            }).error(function (data, status, headers, config) {
                $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
            });
    }

    $scope.showimageList = function(size,path){
    var amazon = $scope.amazon;
    var modalInstance = $modal.open({
      templateUrl: 'imagelist.html',
      controller: imagelistCTRL,
      size: size,
      resolve: {
        path: function() {
          return amazon
        }
      }

    });
  }

  var pathimage = "";

  var pathimages = function(){
    $scope.amazonpath=pathimage;
  }

  var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
    $scope.amazonpath= path;
    $scope.imageloader=false;
    $scope.imagecontent=true;
    $scope.noimage = false;
      $scope.imggallery = false;

    var loadimages = function() {
      Settings.loadimage(function(data){
        if(data.error == "NOIMAGE" ){
                $scope.imggallery=false;
                $scope.noimage = true;
              }else{
                $scope.noimage = false;
                $scope.imggallery=true;
                $scope.imagelist = data;
              }
      });
    }
    loadimages();

    $scope.path=function(path){
      var texttocut = Config.amazonlink + '/uploads/banner/';
      var newpath = path.substring(texttocut.length);
      pathimage = newpath;
      pathimages();
      $modalInstance.dismiss('cancel');
    }

    $scope.upload = function(files) {
      $scope.upload(files);
    };

    $scope.delete = function(id){
          var modalInstance = $modal.open({
            templateUrl: 'delete.html',
            controller: deleteCTRL,
            resolve: {
              imgid: function() {
                return id
              }
            }

          });

       }

 var deleteCTRL = function($scope, $modalInstance, imgid) {

  $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

  $scope.message="Are you sure do you want to delete this Photo?";
  $scope.ok = function() {
   $http({
   url: Config.ApiURL+"/settings/delete/"+ imgid,
    method: "get",
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  }).success(function(data, status, headers, config) {
    $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
    loadimages();
    $modalInstance.close();
  }).error(function(data, status, headers, config) {
    loadimages();
    $modalInstance.close();
    $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
  });
};

$scope.cancel = function() {
  $modalInstance.dismiss('cancel');
};
}


    $scope.upload = function (files)
    {
      var filename
      var filecount = 0;
      if (files && files.length)
      {
        $scope.imageloader=true;
        $scope.imagecontent=false;

        for (var i = 0; i < files.length; i++)
        {
          var file = files[i];

          if (file.size >= 2000000)
          {
            $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
            filecount = filecount + 1;

            if(filecount == files.length)
            {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            }
          }
          else
          {
            var promises;
            var fileExtension = '.' + file.name.split('.').pop();

                      // rename the file with a sufficiently random value and add the file extension back
                    var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;


            promises = Upload.upload({

              url:Config.amazonlink,
              method: 'POST',
              transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                              key: 'uploads/banner/' + renamedFile,
                              AWSAccessKeyId: Config.AWSAccessKeyId,
                              acl: 'private',
                              policy: Config.policy,
                              signature: Config.signature,
                              "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
                            },
                            file: file
                        })
              promises.then(function(data){

                filecount = filecount + 1;
                filename = data.config.file.name;
                var fileout = {
                  'imgfilename' : renamedFile
                };

                ////UPLOAD FACTORY
                Settings.saveimage(fileout, function(data){
                  if(data[0].success){
                    loadimages();
                    if(filecount == files.length)
                    {
                      $scope.imageloader=false;
                      $scope.imagecontent=true;
                    }
                  }else{
                    $scope.imageloader=false;
                    $scope.imagecontent=true;
                  }
                });

            });
          }

        }
      }
    };



    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');

    };
  };
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////

  $scope.savesocial = function(links) {
    $scope.alerts = [];
    Settings.update(links, function(data) {
      if(data.hasOwnProperty('success')){
        $scope.alerts.push({type: 'success', msg: data.success});
      } else {
        $scope.alerts.push({type: 'danger', msg: data.err});
      }
    });
  };


})
