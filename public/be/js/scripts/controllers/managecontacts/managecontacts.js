'use strict';

app.controller('Managecontacts', function($scope,$http, $modal, $state, $stateParams, Config ,Managecontacts,$rootScope){
	console.log("JS LOADED")

	 $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = undefined;
    var stat = undefined;
  
    var loadlist  = function(){
       Managecontacts.loadlist(num,off, $scope.searchtext, $scope.searchstat, function(data){
        $scope.data = data;
        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
        $scope.countnew = data.countnew;
       });
    }
    loadlist();
    //RESET
    $scope.resetsearch  = function(){
       $scope.searchtext = undefined;
       $scope.searchstat= undefined;
       loadlist();
    };

    $scope.search = function (keyword,stat) {
        var off = 1;
        Managecontacts.loadlist('10','1', keyword,stat, function(data){
        $scope.data = data;
        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });

    }
    
    $scope.numpages = function (off, keyword,stat) {
        Managecontacts.loadlist('10',off, $scope.searchtext, $scope.searchstat, function(data){
        $scope.data = data;
        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });
    }
    $scope.setPage = function (pageNo) {
        off = pageNo;
        Managecontacts.loadlist(num,pageNo, $scope.searchtext, $scope.searchstat, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });
    };
    //END OF LIST


      $scope.deletemodal = function(id) {

        var modalInstance = $modal.open({
            templateUrl: 'contactDelete.html',
            controller: contactDeleteCTRL,
            resolve: {
                id: function() {
                    return id
                }
            }
        });
    }
    var successloadalert = function(){
        $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'Message successfully Deleted!' });
            
    }

    var errorloadalert = function(){
            $scope.alerts.push({ type: 'danger', msg: 'Something went wrong message not Deleted!' });
    }
    var messsagesent = function(){
        $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'MESSAGE SENT!' });
    }

    var contactDeleteCTRL = function($scope, $modalInstance, id) {
        $scope.ok = function() {
            $http({
                url: Config.ApiURL+"/managecontacts/messagedelete/"+ id,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {
                console.log(data);
                loadlist();
                $modalInstance.close();
                successloadalert();
            }).error(function(data, status, headers, config) {
                loadlist();
                $modalInstance.close();
                errorloadalert();
            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
        };



     ///MODAL REVIEW REQUEST

        $scope.reviewmodal = function(id) {

            $scope.process = false;
            var modalInstance = $modal.open({
                templateUrl: 'contactReview.html',
                controller: reviewcontactCTRL,
                resolve: {
                    id: function () {
                        return id;
                    }
                }
            });
        }

        /////////////////////////////////////////////////////////////////////////////
        var reviewcontactCTRL = function($scope, $modalInstance, id, $sce) {
           $scope.process = false;
           $http({
            url: Config.ApiURL+"/managecontacts/listreplies/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
           $scope.datalist = data;
           loadlist();
       })

        $scope.process = false;
        $http({
            url: Config.ApiURL+"/message/view/" + id,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.rid = data.id;
            $scope.reply.rid = data.id;
            $scope.cname = data.name;
            $scope.email = data.email;
            $scope.reply.email = data.email;
            $scope.content = data.content;
            $scope.reply.content = data.content;
            $scope.datesubmitted = data.datesubmitted;
            $scope.status = data.status;
            $scope.state = data.state;
            $scope.center = data.city;
        })

        $scope.reply = function(id) {

        };

        $scope.clear = function() {
            $scope.reply.messages = [];
            $scope.formsubmit.$setPristine(true);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.send = function (reply){
            $http({
                url: Config.ApiURL + "/message/reply",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(reply)
            }).success(function (data, status, headers, config) {
                console.log('success');
                messsagesent();
                $modalInstance.dismiss('cancel');
                loadlist();
            }).error(function (data, status, headers, config) {

            });


        };

    };





        ////END PROPOSAL CONTROLLER


});