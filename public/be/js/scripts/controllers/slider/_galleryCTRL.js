'use strict';

app.controller('GalleryCTRL', function($scope, $state ,$q, $http, Config, $modal,$sce, Egallery, Upload, Factory){

    $scope.amazonpath= Config.amazonlink;
    $scope.imageloader=false;
    $scope.imagecontent=true;
    $scope.noimage = false;
    $scope.imggallery = false;
    $scope.alerts = [];
    $scope.range = [];

    var loadimage = function(){
        Egallery.loadimage(function(_rdata){
            $scope.imggallery  = (_rdata.error == "NOIMAGE") ? false : true;
            $scope.noimage     = (_rdata.error == "NOIMAGE") ? true : false;
            $scope.imagelist   = _rdata;
            $scope.homelength = [];
            $scope.shoplength = [];
            var x = 0;
            var y = 0;
            angular.forEach(_rdata, function(value, key) {
                if(value['page'] == 'home'){
                    x++;
                    $scope.homelength.push(x);
                } else {
                    y++;
                    $scope.shoplength.push(y);
                }
            });
        });
    };

    loadimage();

    $scope.delete = function(id){
        var modalInstance = $modal.open({
            templateUrl: 'delete.html',controller: deleteCTRL,
            resolve: {imgid: function() {return id}}
        });
    };

    var deleteCTRL = function($scope, $modalInstance, imgid) {
        $scope.alerts = [];
        $scope.message="Are you sure do you want to delete this Photo?";

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.ok = function() {
            Egallery.deletebanner(imgid, function(_rdata){
                loadimage();
                $modalInstance.close();
            });
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    };


    $scope.upload = function(_type){
        var modalInstance = $modal.open({
            templateUrl: 'egallery.html',
            size:'lg',
            controller: egalleryCTRL,
            resolve: {
                page: function() {
                    return _type;
                },
            }
        });
    };

    var egalleryCTRL = function($modalInstance,$scope,page, $state, Upload ,$q, $http, Config, $stateParams) {
        $scope._pagelocation = page;
        $scope.amazonPath = Config.amazonlink;
        $scope._filename = "";
        $scope.alerts = [];

        $scope.path=function(path){
            pathimage1=path.trim();
        };

        var contloader = function(_loader, _content){
            $scope.imageloader = _loader;
            $scope.imagecontent = _content;
        };

        contloader(false, true);

        $scope.upload = function(files) {
            $scope.upload(files);
        };

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.upload = function (file) {
            var filename;
            var filecount = 0;

            if (file && file.length) {
                if (file.size >= 7000000){
                    $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big. (7mb max)'});
                    filecount = filecount + 1;
                } else {
                    $scope.file = file[0];
                    $scope.closeAlert(0);
                }
            }
        };

        $scope.cancel = function() {
            $modalInstance.dismiss();
        };

        $scope.submit = function(url){
            var param = {
                url : url,
                page : page,
                image : $scope.file.name
            };

            var promises;
            $scope.imageloader = true;
            promises = Upload.upload({

                url:'https://bodynbrain.s3.amazonaws.com/', //S3 upload url including bucket name
                method: 'POST',
                transformRequest: function (data, headersGetter) {
                //Headers change here
                var headers = headersGetter();
                delete headers['Authorization'];
                return data;
                },
                fields : {
                  key: 'uploads/slider/' + page + "/" + $scope.file.name, // the key to store the file on S3, could be file name or customized
                  AWSAccessKeyId: Config.AWSAccessKeyId,
                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                  policy: Config.policy, // base64-encoded json policy (see article below)
                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                  "Content-Type": $scope.file.type != '' ? $scope.file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                },
                file: $scope.file
            });

            promises.then(function(data){
                Egallery.saveImage(param, function(data){
                    if(data.hasOwnProperty('success')){
                        loadimage();
                        $scope.imageloader = false;
                        $modalInstance.dismiss();
                    } else {
                        $scope.alertss.push({
                            type : 'danger',
                            msg: 'An error occured please try again later.'
                        });
                    }
                });
            });
        };
    };

    var msgAlert = function(data) {
        if(data.hasOwnProperty('success')){
            $scope.alerts.push({ type: 'success', msg : data.success});
        }else {
            $scope.alerts.push({ type: 'danger', msg : data.error});
        }
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.delete = function(id) {
        var modalInstance = $modal.open({
            templateUrl: 'delete.html',
            controller: function($scope, $modalInstance) {
                $scope.message = "Are you sure you want to delete this message?";
                $scope.cancel = function() {
                    $modalInstance.dismiss();
                };
                $scope.ok = function() {
                    Egallery.deleteslider({ id : id }, function(data) {
                        msgAlert(data);
                        loadimage();
                        $modalInstance.dismiss();
                    });
                };
            },
            resolve : {
                id : function() {
                    return id;
                }
            }
        });
    };

    // $scope.changeorder = function(img) {
    //     Egallery.updateimgorder(img, function(data) {
    //         if(data.hasOwnProperty('success')){
    //             loadimage();
    //         } else {
    //             $scope.alerts.push({ type: 'danger', msg: 'An error occured please try again later.'});
    //         }
    //     });
    // };

    var successAlert = function() {
        $scope.alerts = [];
        $scope.alerts.push({ type : 'success', msg : 'Image url link has been updated successfully.'});
    };

    $scope.edit = function(img) {
        var modalInstance = $modal.open({
            templateUrl: 'editslider.html',
            controller: function($scope, $modalInstance) {
                $scope.img = img;

                $scope.cancel = function() {
                    $modalInstance.dismiss();
                };

                $scope.ok = function() {
                    Egallery.updateUrl($scope.img, function(data) {
                        if(data.hasOwnProperty('success')){
                            loadimage();
                            successAlert();
                            $modalInstance.dismiss();
                        }
                    });
                };
            },
            resolve: {
                img : function() {
                    return img;
                }
            }
        });
    };




  // <<< multiple imageUploaderWithUrl module -nu77
  $scope.amazonlink = Config.amazonlink;
  var tab = 'home';
  function LOADBANNERS(page) {
    $scope.sequence = [];
    Factory.loadimages(page, function(data){
      $scope.banners = data;
      angular.forEach(data, function(value, key) {
        $scope.sequence.push((key + 1));
      });
    });
  }
  LOADBANNERS(tab);

  $scope.changetab = function(tab) {
    LOADBANNERS(tab);
  };

  $scope.imageUploaderWithUrl = function(page) {
    Factory.modal("imageuploaderwithurl",imageUploaderWithUrlCtrl,"lg",page);
  };
  var imageUploaderWithUrlCtrl = function($scope, $modalInstance, data) {
    var page = data;

    $scope.imgalert = [];
    $scope.closeImgAlert = function(index) {
        $scope.imgalert.splice(index, 1);
    };

    $scope.uploadImgWithUrl = function(images) {
      images.map(function(value, key) {
        $scope.imageloader = true;
        if (value.size >= 7000000){
            $scope.imgalert.push({type: 'danger', msg: 'File ' + value.name + ' is too big. (7mb max)'});
        } else {
          Factory.upload("uploads/slidernbanner/",value, function(img){
            img.then(function(data) {
              var image = {page:page, image:data.config.file.name, url:data.config.file.url };
              Factory.saveimage(image, function(data) {
                if(data.error==true) {
                  Factory.toaster("error","Failed", image['image'] + ' is failed to upload');
                } else {
                  Factory.toaster("success","Success", image['image'] + ' has been uploaded.');
                }
                if(key+1 == images.length) {
                  $scope.imageloader = false;
                  $modalInstance.dismiss();
                  LOADBANNERS(page);
                }
              });
            });
          });
        }
      });
    };

    $scope.cancel = function() {
      $modalInstance.dismiss();
    };
  };

  $scope.removeImage = function(id) {
    Factory.modal('confirm',RemoveBannerCtrl,'sm',id);
  };

  var RemoveBannerCtrl = function($scope, $modalInstance, data) {
    $scope.confirm = {
      title: "Confirm",
      msg: "Do you want to continue?",
      ok: "Yes",
      cancel: "No",
      key: data
    };

    $scope.ok = function(key) {
      Factory.removeimage(key, function(data) {
        if(data.error == true) {
          Factory.toaster("error","Error", data.errorMsg);
        } else {
          Factory.toaster("success","Success", 'Image has been deleted');
          LOADBANNERS(tab);
        }
      });
      $modalInstance.dismiss();
    };
    $scope.cancel = function() {
      $modalInstance.dismiss();
    };
  };

  $scope.editImage = function(id) {
    Factory.editimage(id, function(data) {
      Factory.modal('bannerurl',EditBannerCtrl,'md',data);
    });

    var EditBannerCtrl = function($scope, $modalInstance, data) {
      $scope.image = data;
      $scope.ok = function(image) {
        Factory.updateimage(image, function(data) {
          if(data.error == true) {
            Factory.toaster("error","Error", data.errorMsg);
          } else {
            Factory.toaster("success","Success", 'Image has been updated');
            LOADBANNERS(tab);
          }
        });
        $modalInstance.dismiss();
      };
      $scope.cancel = function() {
        $modalInstance.dismiss();
      };
    };
  };

  $scope.hideShowImage = function(id) {
    Factory.togglestatusimage(id, function(data) {
      if(data.success == true) {
        Factory.toaster("success","Success", 'Image status has been toggled');
        LOADBANNERS(tab);
      }
    });
  };

  $scope.changeorder = function(img) {
    Factory.updateimgorder(img, function(data) {
      if(data.hasOwnProperty('success')){
        Factory.toaster("success","Success", 'Image sequence has been updated');
        LOADBANNERS(img.page);
      } else {
        Factory.toaster("error","Error", 'An error occured please try again later.');
      }
    });
  };
  // multiple imageUploaderWithUrl module ENDS -nu77 >>>

  // <<< single imageuploader module -nu77
  function outerOneClassBanner() {
      Factory.loadSinglebanner('oneclassouter', function(data){
        $scope.outeroneclassbanner = data;
      });
  }
  outerOneClassBanner();

  function innerOneClassBanner() {
      Factory.loadSinglebanner('oneclassinner', function(data){
        $scope.inneroneclassbanner = data;
      });
  }
  innerOneClassBanner();

  function outerGroupClassBanner() {
      Factory.loadSinglebanner('groupclassouter', function(data){
        $scope.outergroupclassbanner = data;
      });
  }
  outerGroupClassBanner();

  function innerGroupClassBanner() {
      Factory.loadSinglebanner('groupclassinner', function(data){
        $scope.innergroupclassbanner = data;
      });
  }
  innerGroupClassBanner();


  $scope.newSingleBanner = function(image, page, imageloader) {

    image.map(function(value,key){
      if (value.size >= 7000000) {
        Factory.toaster("warning","Failed", value.name + ' is too large. Please try an image with less than 7mb in size.');
      } else {
        Factory.upload("uploads/slidernbanner/",value, function(img){

          switch (imageloader) {
                  case 'outerOneClassImageLoader':
                      $scope.outerOneClassImageLoader = true;
                      break;
                  case 'innerOneClassImageLoader':
                      $scope.innerOneClassImageLoader = true;
                      break;
                  case 'outerGroupClassImageLoader':
                      $scope.outerGroupClassImageLoader = true;
                      break;
                  case 'innerGroupClassImageLoader':
                      $scope.innerGroupClassImageLoader = true;
                      break;
                  default:
          }

          img.then(function(data) {
            var image = {page:page, image:data.config.file.name, url:data.config.file.url  };
            Factory.saveimage(image, function(data) {
              if(data.error==true) {
                Factory.toaster("error","Failed", image['image'] + ' is failed to upload');
              } else {
                Factory.toaster("success","Success", image['image'] + ' has been uploaded.');
              }

                switch (imageloader) {
                        case 'outerOneClassImageLoader':
                            $scope.outerOneClassImageLoader = false;
                            outerOneClassBanner();
                            break;
                        case 'innerOneClassImageLoader':
                            $scope.innerOneClassImageLoader = false;
                            innerOneClassBanner();
                            break;
                        case 'outerGroupClassImageLoader':
                            $scope.outerGroupClassImageLoader = false;
                            outerGroupClassBanner();
                            break;
                        case 'innerGroupClassImageLoader':
                            $scope.innerGroupClassImageLoader = false;
                            innerGroupClassBanner();
                            break;
                        default:
                }

            });
          });
        });
      }
    });
  };
  // single imageuploader module -nu77 >>>
});
