app.controller('ManageOrder', function($scope, $state, Config, $modal, $anchorScroll, Order, $timeout){
    var filter = null;
    var num = 10;
    var off = 1;
    $scope.keyword = null
    $scope.maxSize = 10;

    var loadlist = function(num, off, keyword, filter) {
        Order.loadlist(num, off, keyword, filter, function(data) {
            $scope.orders = data.orders;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
            $scope.today = data.today;
            $scope.processing = data.processing;
            $scope.pending = data.pending;
            $scope.completed = data.completed
        });
    }

    loadlist(num, off, $scope.keyword, filter);

    $scope.notif = function(filter) {
        loadlist(num, off, null, filter);
        $scope.keyword = filter;
    }

    $scope.setPage = function(off) {
        loadlist(num, off, $scope.keyword, filter);
    }

    $scope.search = function(keyword) {
        $scope.keyword = keyword;
        $scope.searchtext = undefined;
        loadlist(num, off, keyword, filter);
    }

    $scope.clear = function() {
        $scope.keyword = null;
        loadlist(num, off, $scope.keyword, filter);
    }

    $scope.view = function(id) {
        $state.go('vieworder', {id: id });
    }

    $scope.print = function() {
        Order.getorderreport(function(data) {
            $scope.orders2 = data;
            $timeout(function() {
                var pdf = new jsPDF('p', 'pt', 'letter');
                source = $('#print')[0];
                specialElementHandlers = {
                  '#bypassme': function(element, renderer){
                    return true
                  }
                }
                 
                margins = {
                    top: 50,
                    left: 60,
                    right: 0,
                    width: 545
                  };

                pdf.fromHTML(
                    source // HTML string or DOM elem ref.
                    , margins.left // x coord
                    , margins.top // y coord
                    , {
                      'width': margins.width // max width of content on PDF
                      , 'elementHandlers': specialElementHandlers
                    },
                    function (dispose) {
                    
                      // dispose: object with X, Y of the last line add to the PDF
                      //          this allow the insertion of new lines after html
                        pdf.save('BodynBrain.pdf');
                    }
                )
            }, 500);
        });
    }
})
