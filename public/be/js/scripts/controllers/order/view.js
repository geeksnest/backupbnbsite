app.controller('ViewOrder', function($scope, $state, Config, $modal, $anchorScroll, Order, $stateParams, $timeout){
    $scope.subtotal = 0;
    $scope.comments = [];
    $scope.error = {};

    var getorder = function(id) {
        Order.getorder(id, function(data) {
            $scope.order = data.order;
            $scope.products = data.products;
            $scope.acctinfo = data.acctinfo;
            $scope.shippinginfo = data.shippinginfo;
            $scope.itemcount = data.products.length;
            $scope.totalamount = data.order.amount;
            $scope.comments = data.comments;

            angular.forEach($scope.products, function(value, key){
                var discount_from = new Date(value['discount_from']).getTime();
                var discount_to = new Date(value['discount_to']).getTime();
                var now = new Date().getTime();
                if(discount_from <= now && now >= discount_to){
                    $scope.products[key].price2 =  value['price'] - ((value['discount'] / 100) * value['price']);
                    $scope.products[key].total = $scope.products[key].price2 * value['orderquantity'];
                }else {
                    $scope.products[key].price2 = value['price'];
                    $scope.products[key].total = value['price'] * value['orderquantity'];
                }
                $scope.subtotal = $scope.subtotal + $scope.products[key].total;
            });
        });
    }

    getorder($stateParams.id);

    $scope.submitcomment = function(order){
        if(order.comment!=undefined){
            $scope.error.comment = false;
            Order.comment(order, function(data){
                $scope.comments = data;
                $scope.order.comment = undefined;
            });
        }else {
            $scope.error.comment = true;
        }
        console.log(order.comment);
        console.log($scope.error.comment);
    }

    $scope.change = function(comment) {
        if(comment!=undefined){
            $scope.error.comment = false;
        }else {
            $scope.error.comment = true;
        }
    }

    $scope.submit = function(status) {
        Order.updateStatus({ status : status, id : $scope.order.id }, function(data) {
            if(data.hasOwnProperty('success')){
                $scope.order.status = status;
            }
        }); 
    }

    $scope.print = function() {
        $timeout(function() {
            var pdf = new jsPDF('p', 'pt', 'letter');
            source = $('#print')[0];
            specialElementHandlers = {
              '#bypassme': function(element, renderer){
                return true
              }
            }
             
            margins = {
                top: 50,
                left: 60,
                width: 545
              };

            pdf.fromHTML(
                source // HTML string or DOM elem ref.
                , margins.left // x coord
                , margins.top // y coord
                , {
                  'width': margins.width // max width of content on PDF
                  , 'elementHandlers': specialElementHandlers
                },
                function (dispose) {
                
                  // dispose: object with X, Y of the last line add to the PDF
                  //          this allow the insertion of new lines after html
                    pdf.save('BodynBrain.pdf');
                }
            )
        }, 500);
    }

})
