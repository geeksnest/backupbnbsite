app.controller('AddOrder', function($scope, $state, Config, $modal, $anchorScroll, Order, $timeout, toaster){
    $scope.products = [];
    $scope.order = [];
    $scope.billingaddr = {};
    $scope.shippingaddr = {};
    $scope.disabled = false;
    $scope.userccreg = {};
    $scope.userecheck = {};
    var order = { paymenttype: 'cash'}
    $scope.required = false;
    $scope.alerts = [];
    $scope.months =  Order.month();
    $scope.days =  Order.day();
    $scope.years =  Order.year();
    $scope.payment = false;
    $scope.centerid = undefined;
    $scope.shippingfee = 2;
    $scope.total = 0;
    $scope.tax = 0;
    var taxpercentage = 0.05;
    $scope.error = {};
    $scope.selected = undefined;
    $scope.steps = {}

    var getproducts = function() {
        Order.getproducts(function(data) {
            $scope.products = data;
        });
    }
    // $timeout(function() {
    // console.log($scope.centerid);
    // }, 500);

    getproducts();

    $scope.closeAlert = function() {
        $scope.alerts = [];
    }

    $scope.addproduct = function(product){
        if(product != undefined){
            $scope.error.product = false;
        	angular.forEach($scope.products, function(value, key){
        		if(product == value['name']){
        			value['orderquantity'] = 1;
        			$scope.order.push(value);
                    $scope.onqty(value['productid']);
        			var temp = $scope.products;
        			temp.splice(key, 1);
        			$scope.products = [];
        			$timeout(function() {
        				$scope.products = temp;
                        $scope.selected = undefined;
        			});
        		}
        	});
        } else {
            $scope.error.product = true;
        }
    }

    $scope.remove = function(id){
		var temp = $scope.products;
    	angular.forEach($scope.order, function(value, key) {
    		if(id == value['productid']){
    			$scope.products = [];
    			$timeout(function() {
    				temp.push(value);
    				$scope.products = temp;
    			});
    			$scope.order.splice(key, 1);
    		}
    	});
    }

    $scope.onqty = function(id) {
    	angular.forEach($scope.order, function(value, key){
    		if(value['productid'] == id){
    			var discount_from = new Date(value['discount_from']).getTime();
    			var discount_to = new Date(value['discount_to']).getTime();
    			var now = new Date(new Date().toJSON().slice(0,10)).getTime();

    			if(discount_from <= now && now <= discount_to){
    				var total =  value['price'] - ((value['discount'] / 100) * value['price']);
    				$scope.order[key].total = total * value['orderquantity'];
    			}else {
    				$scope.order[key].total = value['price'] * value['orderquantity'];
    			}
    		}
    	})
    }

    // account information
    $scope.onemail = function(email) {
    	Order.getaccountinfo(email, function(data) {
    		if(data.hasOwnProperty('memberid')){
                var temp = $scope.billingaddr.email;
	    		$scope.billingaddr = data;
                $scope.billingaddr.email = temp;
	    		$scope.disabled = true;
                var state = data.state;
                $timeout(function() {
                    $scope.billingaddr.state = state;
                    console.log(state);
                }, 500);
	    	}
    	});
    }

    $scope.no = function() {
    	$scope.disabled = false;
    	$scope.billingaddr = { email : $scope.billingaddr.email }
    }

    $scope.yes = function() {
    	$scope.disabled = false;
    }

    $scope.click = function() {
    	$scope.steps.step3 = true; 
    }

    // shipping address
    $scope.usebillingaddr = function(use) {
    	var state = $scope.billingaddr.state;
    	if(use == true){
    		$scope.shippingaddr = $scope.billingaddr;
    		$timeout(function() {
    			$scope.shippingaddr.state = state;
    			$scope.billingaddr.state = state;
    			console.log(state);
	        }, 500);
    	} else {
    		$scope.shippingaddr = {};
    	}
    }

    //payment method
    $scope.paymentt = function(paymenttype){
        order.paymenttype = paymenttype;
    }

    $scope.$watch(function() {
        return $scope.steps.step5;
    }, function() {
        $scope.totalamount = 0;
        if($scope.steps.step5){
            angular.forEach($scope.order, function(value, key) {
                $scope.totalamount = $scope.totalamount + value['total'];
            });
            $scope.tax = $scope.totalamount * taxpercentage;
            if($scope.shippingaddr.firstname != undefined){
                $scope.gtotal = $scope.totalamount + $scope.shippingfee + $scope.tax;
            }else {
                $scope.gtotal = $scope.totalamount + $scope.tax;
            }
        }
    })

    //submit
    $scope.submit = function() {
        order = {
            paymenttype : order.paymenttype,
            billinginfo: $scope.billingaddr,
            order: $scope.order,
            shippinginfo: $scope.shippingaddr,
            payment : order.paymenttype =='check' ? $scope.userecheck : $scope.userccreg,
            totalamount: $scope.totalamount,
            tax : $scope.tax,
            shippingfee : $scope.shippingfee
        }
        if($scope.centerid==''){
            $scope.centerid = null;
        }
        
        $scope.alerts = [];
        Order.addoffline(order, $scope.centerid, function(data) {
            if(data.hasOwnProperty('success')){
                $scope.products = [];
                $scope.order = [];
                $timeout(function() {
                    getproducts();
                });
                $scope.billingaddr = {};
                $scope.shippingaddr = {};
                $scope.disabled = false;
                $scope.userccreg = {};
                $scope.userecheck = {};
                $scope.use = false;
                $scope.error = {};
                $scope.steps={percent:20, step1:true, step2:false, step3:false, step4: false, step5:false}; 
                $scope.view=false;
                order = { paymenttype: 'cash'}
                $scope.required = false;
                $scope.payment = false;
                $scope.alerts.push({ type: 'success', msg: data.success });
            }else {
                $scope.alerts.push({ type: 'danger', msg: 'An error occurred please try again later.'});
            }
        });
    }

    $scope.change = function(data, type){
        switch(type){
            case 'ccn' :
                if(data != undefined){
                    $scope.error.ccn = false;
                } else {
                    $scope.error.ccn = true;
                }
                break;
            case 'cvvn' :
                if(data != undefined){
                    $scope.error.cvvn = false;
                } else {
                    $scope.error.cvvn = true;
                }
                break;
            case 'expiremonth' :
                if(data != undefined){
                    $scope.error.expiremonth = false;
                } else {
                    $scope.error.expiremonth = true;
                }
                break;
            case 'expireyear' :
                if(data != undefined){
                    $scope.error.expireyear = false;
                } else {
                    $scope.error.expireyear = true;
                }
                break;
            case 'accountname' :
                if(data != undefined){
                    $scope.error.accountname = false;
                } else {
                    $scope.error.accountname = true;
                }
                break;
            case 'bankname' :
                if(data != undefined){
                    $scope.error.bankname = false;
                } else {
                    $scope.error.bankname = true;
                }
                break;
            case 'bankrouting' :
                if(data != undefined){
                    $scope.error.bankrouting = false;
                } else {
                    $scope.error.bankrouting = true;
                }
                break;
            case 'bankaccountnumber' :
                if(data != undefined){
                    $scope.error.bankaccountnumber = false;
                } else {
                    $scope.error.bankaccountnumber = true;
                }
                break;
            case 'firstname' : 
                if(data != undefined){
                    $scope.error.firstname = false;
                }else {
                    $scope.error.firstname = true;
                }
                break;
            case 'lastname' : 
                if(data != undefined){
                    $scope.error.lastname = false;
                }else {
                    $scope.error.lastname = true;
                }
                break;
            case 'address' : 
                if(data != undefined){
                    $scope.error.address = false;
                }else {
                    $scope.error.address = true;
                }
                break;
            case 'city' : 
                if(data != undefined){
                    $scope.error.city = false;
                }else {
                    $scope.error.city = true;
                }
                break;
            case 'country' : 
                if(data != undefined){
                    $scope.error.country = false;
                }else {
                    $scope.error.country = true;
                }
                break;
            case 'state' : 
                if(data != undefined){
                    $scope.error.state = false;
                }else {
                    $scope.error.state = true;
                }
                break;
        }
    }

    $scope.preview = function(paymenttype) {
        if(paymenttype == 'cash'){
            $scope.steps.step5 = true; 
            $scope.view = true;
        } else if(paymenttype == 'card') {
            $scope.change($scope.userccreg.ccn, 'ccn');
            $scope.change($scope.userccreg.cvvn, 'cvvn');
            $scope.change($scope.userccreg.expiremonth, 'expiremonth');
            $scope.change($scope.userccreg.expireyear, 'expireyear');
            if($scope.error.ccn == false &&
                $scope.error.cvvn == false &&
                $scope.error.expiremonth == false &&
                $scope.error.expireyear == false){
                $scope.steps.step5 = true; 
                $scope.view = true;
            }
        } else if(paymenttype == 'check') {
            $scope.change($scope.userecheck.accountname, 'accountname');
            $scope.change($scope.userecheck.bankname, 'bankname');
            $scope.change($scope.userecheck.bankrouting, 'bankrouting');
            $scope.change($scope.userecheck.bankaccountnumber, 'bankaccountnumber');
            if($scope.error.accountname == false &&
                $scope.error.bankname == false &&
                $scope.error.bankrouting == false &&
                $scope.error.bankaccountnumber == false){
                $scope.steps.step5 = true; 
                $scope.view = true;
            }
        }
    }
})
