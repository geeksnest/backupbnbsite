'use strict';

/* Controllers */

app.controller('AddUserCtrl', function($scope, $http, $modal, Usersfactory, Config, Upload, Factory) {
  $scope.sampledata = 'rainier';
  var userid = "this-is-dummy-but-useful";
  var username_exist;
  var useremail_exist;

	angular.element( '#formsubmit_hid' ).hide();

	$scope.user = {
      username: "",
      email: "",
      password: "",
      conpass: "",
      userrole: "Administrator",
      fname: "",
      lname: "",
      bday: "",
      gender: "Male",
      status: 0,
      file: ""
    };
  var oriUser = angular.copy($scope.user);

	$scope.chkusername =function(username) {
    Usersfactory.validateusername(username, function(data){
        $scope.usrname= data.exists;
        username_exist = data.exists;
    });
	};

	//VALIDATE EMAIL
	$scope.chkemail =function(useremail){
    Usersfactory.validateuseremail(useremail, function(data){
        $scope.usremail= data.exists;
        useremail_exist = data.exists;
    });
	};

	//VALIDATE PASSWORD IF MATCH
	$scope.confirmpass =function(password, confirmpass){
		if(password!=confirmpass){
			$scope.confpass = true;
		} else {
			$scope.confpass = false;
		}
	};

  $scope.closeAlert = function(index, type) {
      if(type=='default') {
          $scope.alerts.splice(index, 1);
      } else if (type=='image') {
          $scope.imagealert.splice(index, 1);
      }
  };
	//SAVE DATA
	$scope.submitData = function(user, file){
		$scope.alerts = [];

    if(username_exist == true) {
        Factory.toaster("warning","Username Exist!","Please change your username into something unique.");
    }
    else if (useremail_exist == true) {
        Factory.toaster("warning","Email is already used!","Please change your email.");
    }
    else if (user.password != user.conpass) {
        Factory.toaster("warning","Password Mismatch!","Your password didn't match. Kindly check it.");
    }
    else if (file === undefined || file === null || file === '') {
        Usersfactory.register(user, function(data) {

            Factory.modaL("success.html","sm",
              function ($scope, $modalInstance) {
                  $scope.message = "New User has been successfully created";
                  $scope.cancel = function () {
                      $modalInstance.dismiss('cancel');
                  };
              }
            );

            $scope.user = angular.copy(oriUser);
            $scope.createUserForm.$setPristine(true);
            $scope.isSaving = false;

            REGDISCEN();
            $scope.file = undefined;
            angular.element('#blah').attr('src','/img/default_profile_pic.jpg');
            $scope.processing = false;
        });
    }
    else {
      //PROFILE PIC
      $scope.upload(user,file);
    }

	};

	$scope.upload = function (user,files) {

        $scope.processing = true;
        $scope.imagealert = [];
        // var filename;
        // var filecount = 0;

        files.map(function(file){
            if(file.size >= 2000000) {
                $scope.imagealert.push({type: 'danger', msg: 'Image File: ' + file.name + ' is too big. (Maximum of 2MB)'});
                $scope.processing = false;
            }
            else {
              Factory.upload("uploads/userimages/",file, function(photo){
                photo.then(function(data) {

			              user['profile_pic_name'] = data.config.file.name;

                    Usersfactory.register(user, function(data) {

                        Factory.modaL("success.html","sm",
                          function ($scope, $modalInstance) {
                              $scope.message = "New User has been successfully created";
                              $scope.cancel = function () {
                                  $modalInstance.dismiss('cancel');
                              };
                          }
                        );

                        $scope.user = angular.copy(oriUser);
                        $scope.createUserForm.$setPristine(true);
                        $scope.file = undefined;
                        angular.element('#blah').attr('src','/img/default_profile_pic.jpg');
                        REGDISCEN();
                        $scope.isSaving = false;
                        $scope.processing = false;
                    });
                });
              });
            }
        });
    };

    var REGDISCEN = function() {
        Usersfactory.availableREGDISCENlist(userid, function(data) {
        	if(data.regions != '' ) {
        		$scope.availableregion = data.regions;
        		$scope.hideregion = false;
        	} else {
        		$scope.hideregion = true;
        	}

        	if(data.districts != '' ) {
        		$scope.availabledistrict = data.districts;
        		$scope.hidedistrict = false;
        	} else {
        		$scope.hidedistrict = true;
        	}

            if(data.centers != '' ) {
                $scope.availablecenter = data.centers;
                $scope.hidecenter = false;
            } else {
                $scope.hidecenter = true;
            }
        });
    }
    REGDISCEN();

	//DATE PICKER
	$scope.today = function () {
    $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;

    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

})
