'use strict';

/* Controllers */

app.controller('changepasswordCtrl', function ($scope, $state, $q,$stateParams, Config, Usersfactory){
	$scope.alerts = [];

	$scope.closeAlert = function (index) {
		$scope.alerts.splice(index, 1);
	};

	$scope.checkpassword = function(pass,repass){
		if(pass != repass){
			$scope.IsMatch = true;
			$scope.errormsg = 'Password not match!';
			$scope.formpassword.$invalid = true;
		}
		else{
			$scope.IsMatch = false;
		}
	}

	$scope.send = function(cpassword){
		cpassword['userid'] = $stateParams.userid;
		Usersfactory.changepassword(cpassword, function(data){
			$scope.alerts[0]= {type: data.type, msg: data.msg};
			if(data.type == 'success'){
				$scope.cpassword = {

				}
				$scope.formpassword.$setPristine();
			}
		});
	}
})