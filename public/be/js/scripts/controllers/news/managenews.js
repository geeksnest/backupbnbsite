'use strict';

/* Controllers */

app.controller('Managenews', function($scope, $state ,$q, $http, Config, $log, ManagenewsFactory, $interval, $modal, $stateParams){
    
    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.data = {};
    var num = 10;
    var off = $stateParams.page;
    var keyword = null;
    $scope.currentstatusshow = '';


    var loadlist  = function(){
        ManagenewsFactory.sample(num,off, keyword, function(data){
            $scope.data = data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        });
    }

    loadlist();

    $scope.search = function (keyword) {
        var off = 1;
        ManagenewsFactory.sample('10','1', keyword, function(data){
        $scope.data = data;
        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });

    }
    $scope.resetsearch  = function(){
         $scope.searchtext = undefined;
         loadlist();
     };
    $scope.numpages = function (off, keyword) {
        ManagenewsFactory.sample('10', off, $scope.searchtext, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });
    }

    $scope.setPage = function (pageNo) {
        ManagenewsFactory.sample(num,pageNo, $scope.searchtext, function(data){
        $scope.data = data;

        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
    });
    };



    $scope.setstatus = function (status,newsid,keyword,newslocation,newsslugs) {
        if(newslocation == 'mainsite')
        {


            if(status == 1)
            {
                $http({
                        url: Config.ApiURL + "/news/updatenewsstatus/0/" + newsid + '/' + keyword,
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {
                        $scope.currentstatusshow = newsslugs;
                        var i = 2;
                        setInterval(function(){
                            i--;

                            if(i == 0)
                            {
                                loadlist();
                                $scope.currentstatusshow = 0;
                            }
                        },1000)
                        
                    }).error(function (data, status, headers, config) {
                        loadlist();
                    });
            }
            else
            {
                 $http({
                        url: Config.ApiURL + "/news/updatenewsstatus/1/" + newsid + '/' + keyword,
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {
                        $scope.currentstatusshow = newsslugs;
                        var i = 2;
                        setInterval(function(){
                            i--;
                            if(i == 0)
                            {
                                loadlist();
                                $scope.currentstatusshow = 0;
                            }


                        },1000)
                    }).error(function (data, status, headers, config) {
                      loadlist();
                    });
            }
        }
        else
        {
            if(status == 1)
            {
                $http({
                        url: Config.ApiURL + "/news/updatenewscenterstatus/0/" + newsid + '/' + keyword,
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {
                        $scope.currentstatusshow = newsslugs;
                        var i = 2;
                        setInterval(function(){
                            i--;

                            if(i == 0)
                            {
                                 loadlist();
                                 $scope.currentstatusshow = 0;
                            }


                        },1000)
                        
                    }).error(function (data, status, headers, config) {
                        loadlist();   
                    });
            }
            else
            {
                 $http({
                        url: Config.ApiURL + "/news/updatenewscenterstatus/1/" + newsid + '/' + keyword,
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function (data, status, headers, config) {
                        $scope.currentstatusshow = newsslugs;
                        var i = 2;
                        setInterval(function(){
                            i--;
                            if(i == 0)
                            {
                               loadlist();
                               $scope.currentstatusshow = 0;
                            }


                        },1000)
                    }).error(function (data, status, headers, config) {
                       loadlist();
                    });
            }
        }
    }

    

  
    $scope.deletenews = function(newsid) {
        var modalInstance = $modal.open({
            templateUrl: 'newsDelete.html',
            controller: newsDeleteCTRL,
            resolve: {
                newsid: function() {
                    return newsid
                }
            }
        });
    }

    var loadpage = function(){

        ManagenewsFactory.sample(num,$scope.bigCurrentPage, $scope.searchtext, function(data){
                        $scope.data = data;
                        $scope.maxSize = 5;
                        $scope.bigTotalItems = data.total_items;
                        $scope.bigCurrentPage = data.index;
                });

    }


    var successloadalert = function(){
       $scope.alerts.splice(0, 1);
       $scope.alerts.push({ type: 'success', msg: 'News successfully Deleted!' });     
   }

    var errorloadalert = function(){
            $scope.alerts.push({ type: 'danger', msg: 'Something went wrong News not Deleted!' });
    }


       
    var newsDeleteCTRL = function($scope, $modalInstance, newsid) {
        $scope.ok = function() {
            $http({
                url: Config.ApiURL + "/news/newsdelete/" + newsid,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {
                
                loadpage();
                $modalInstance.close();
                successloadalert();

            }).error(function(data, status, headers, config) {
                loadpage();
                $modalInstance.close();
                errorloadalert();
            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };


        };


     $scope.deletenewscenter = function(newsid) {
        var modalInstance = $modal.open({
            templateUrl: 'newsCenterDelete.html',
            controller: newsCenterDeleteCTRL,
            resolve: {
                newsid: function() {
                    return newsid
                }
            }
        });
    }
       
    var newsCenterDeleteCTRL = function($scope, $modalInstance, newsid) {
        $scope.ok = function() {
            $http({
                url: Config.ApiURL + "/news/newscenterdelete/" + newsid,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).success(function(data, status, headers, config) {
                
                loadpage();
                $modalInstance.close();
                successloadalert();

            }).error(function(data, status, headers, config) {
                loadpage();
                $modalInstance.close();
                errorloadalert();
            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };


        };


    var newsEditCTRL = function($scope, $modalInstance, newsid, $state, page) {
        $scope.newsid = newsid;
        $scope.ok = function(newsid) {
            $scope.newsid = newsid;
            $state.go('editnews', {newsid: newsid, page:page });
                $modalInstance.dismiss('cancel');
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }

    $scope.editnews = function(newsid) {
        $scope.newsid
        var modalInstance = $modal.open({
            templateUrl: 'newsEdit.html',
            controller: newsEditCTRL,
            resolve: {
                newsid: function() {
                    return newsid
                },
                page: function() {
                    return $scope.bigCurrentPage
                }
            }
        });
    }

    var newscenterEditCTRL = function($scope, $modalInstance, newsid, $state) {
        $scope.newsid = newsid;
        $scope.ok = function(newsid) {
            $scope.newsid = newsid;
            $state.go('editnewscenter', {newsid: newsid });
                $modalInstance.dismiss('cancel');
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }

    $scope.editnewscenter = function(newsid) {
        $scope.newsid
        var modalInstance = $modal.open({
            templateUrl: 'newscenterEdit.html',
            controller: newscenterEditCTRL,
            resolve: {
                newsid: function() {
                    return newsid
                }
            }
        });
    }

})