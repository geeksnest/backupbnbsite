'use strict';

/* Controllers */

app.controller('Createcategory', function($scope, $state ,$q, $http, Config, $stateParams, $modal){

    $scope.alerts = [];

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.editcategoryshow = false;


    $scope.data = {};
    var num = 10;
    var off = 1;
    var keyword = undefined;
    
    var paginate = function(off, keyword) {
        $http({
            url: Config.ApiURL + "/news/managecategory/" + num + '/' + off + '/' + keyword,
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).success(function(data, status, headers, config) {
            $scope.data = data;
            $scope.maxSize = 5;
            $scope.bigTotalItems = data.total_items;
            $scope.bigCurrentPage = data.index;
        }).error(function(data, status, headers, config) {
            $scope.status = status;
        });
    }

    paginate(off, keyword);

    $scope.resetsearch = function(){
        keyword = undefined;
        $scope.searchtext = undefined;
        paginate(off, keyword);
    }

    $scope.search = function (searchkeyword) {
        var off = 0;
        paginate(off, searchkeyword);
        keyword = searchkeyword;

    }
    
    $scope.numpages = function (off, keyword) {
        paginate(off, $scope.searchtext);
    }

    $scope.setPage = function (pageNo) {
        paginate(pageNo, $scope.searchtext);
        off = pageNo;
    };

    $scope.addcategory = function() {
        var modalInstance = $modal.open({
            templateUrl: 'categoryAdd.html',
            controller: addcategoryCTRL,
            resolve: {

            }
        });
    }

    var loadaddalert = function(){

        $scope.alerts[0]= {type: 'success', msg: 'Category successfully Added!'};
    }

       
    var addcategoryCTRL = function($scope, $modalInstance) {

        var categoryslugs = '';

        $scope.onnewscat = function convertToSlug(Text)
        {

                if(Text == null)
                {
               
                }
                else
                {
                    var text1 = Text.replace(/[^\w ]+/g,'');
                    categoryslugs = angular.lowercase(text1.replace(/ +/g,'-'));


                    $http({
                        url: Config.ApiURL + "/validate/categ/"+Text,
                        method: "GET",
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'}
                        }).success(function (data, status, headers, config) {

                         if(data==1){
                            $scope.notification = true;

                        }else{
                            $scope.notification = false;
                        }
                         });

                }
         
        }
        
        $scope.ok = function(category) {

            category['slugs'] = categoryslugs;

            $http({
                url: Config.ApiURL + "/news/savecategory",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(category)
            }).success(function(data, status, headers, config) {
                paginate(off, keyword);
                $modalInstance.close();
                $scope.success = true;
                loadaddalert();
            }).error(function(data, status, headers, config) {
                $scope.status = status;
            });
        }
    
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        }

    }

    var loadalert = function(){
        $scope.alerts[0]= {type: 'success', msg: 'Category successfully Deleted!'};
    }


    var deletecategoryInstanceCTRL = function($scope, $modalInstance, id, $state) {

        $scope.ok = function() {
            var news = {
                'news': id
            };
            $http({
                url: Config.ApiURL + "/news/categorydelete/" + id,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(news)
            }).success(function(data, status, headers, config) {
                paginate(off, $scope.searchtext);
                $modalInstance.close();
                loadalert();
            }).error(function(data, status, headers, config) {
                $scope.status = status;
            });
        }

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        }
    }

    $scope.categoryDelete = function(id) {
        var modalInstance = $modal.open({
            templateUrl: 'categoryDelete.html',
            controller: deletecategoryInstanceCTRL,
            resolve: {
                id: function() {
                    return id
                }
            }
        });
    }



   $scope.alerts = [];

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };



    $scope.updatecategory = function(data,memid) {

            var categoryslugs = '';

            var text1 = data.replace(/[^\w ]+/g,'');
            categoryslugs = angular.lowercase(text1.replace(/ +/g,'-'));

            var fileout = {
                'data' : data,
                'memid': memid,
                'slugs': categoryslugs
            }
            $http({
            url: Config.ApiURL + "/news/updatecategorynames",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(fileout)
            }).success(function(data, status, headers, config) {
                $scope.alerts.splice(0, 1);
                $scope.alerts.push({ type: 'success', msg: 'Category successfully updated!' });
                paginate(off, keyword);

            }).error(function(data, status, headers, config) {


            });

    }

})