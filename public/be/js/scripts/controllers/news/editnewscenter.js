app.controller('Editnewscenter', function($scope, $state, $modal,Upload ,$q, $http, Config, Createnews, ManagenewsFactory, $stateParams, $window){
  'use strict';

    //Editing Slugs
    $scope.editslug = false;
    $scope.editnewsslug = function(){
        $scope.editslug = true;
    }
    $scope.cancelnewsslug = function(title){
        $scope.editslug = false;
        $scope.onnewstitle(title)
    }
    $scope.setslug = function(){
        $scope.editslug = false;
    }
    $scope.clearslug = function(title){
        $scope.editslug = false;
        $scope.onnewstitle(title);
    }
    $scope.onslugs = function(text1){
        $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
    }
    $scope.onslugs = function(text1){
        $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));
        validateSlugs();
    }
    // End Editing Slugs
    var validateSlugs = function(){
        // I will change this to validate if duplicate slugs
                $http({
                    url: Config.ApiURL + "/newsslugs/validate/"+ $scope.news.slugs,
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).success(function(data) {
                    if(data>=1){
                        $scope.invalidtitle = true;
                        $scope.formpage.$invalid = true;
                    }else{
                        $scope.invalidtitle = false;
                    }
                })
    }

    Createnews.loadcenter(function(data){
         $scope.newslocation = data;

    });

    $scope.imageloader=false;
    $scope.imagecontent=true;

    $http({
        url: Config.ApiURL + "/news/newscenteredit/" + $stateParams.newsid ,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {

        Createnews.loadcategory(function(data1){
         $scope.category = data1;
         $scope.news = data;
         $scope.defaulttitle = data.title;
         $scope.amazonpath = data.banner;
        });

    }).error(function (data, status, headers, config) {
        $scope.status = status;
    });

    $scope.news = {
      title: ""
    };

    var orinews = angular.copy($scope.news);

    $scope.onnewstitle = function convertToSlug(Text)
    {

            if(Text=="" || Text==null){
                $scope.news.slugs = " ";
            }
            else
            {
                var text1 = Text.replace(/[^\w ]+/g,'');
                $scope.news.slugs = angular.lowercase(text1.replace(/ +/g,'-'));


                if($scope.defaulttitle != Text){
                    $http({
                        url: Config.ApiURL + "/news/validate/"+ Text,
                        method: "GET",
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    }).success(function(data) {
                        console.log(data)
                        if(data>=1){
                            $scope.invalidtitle = true;
                            $scope.formpage.$invalid = true;
                        }else{
                            $scope.invalidtitle = false;
                        }
                    })
                }else{
                    $scope.invalidtitle= false;
                }

            }

    }


    $scope.cutlink = function convertToSlug(Text)
    {
        var texttocut = Config.amazonlink + '/uploads/newsimage/';
        $scope.news.banner = Text.substring(texttocut.length);
    }

    $scope.saveNews = function(news)
    {
        if (news.date == undefined)
        {
            news.date = new Date(news.date2);
        }
        else
        {
            news.date = new Date(news.date);
        }

        if(news.newslocation == 'Main Site')
        {
            news['category'] = news.category.categoryid;
            // if(related===undefined) {
            //     news['relatedworkshop'] = '';
            // } else {
            //     news['relatedworkshop'] = related.toString();
            // }

            $scope.alerts = [];

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.isSaving = true;

            $http({
                url: Config.ApiURL + "/news/edit",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(news)
            }).success(function (data, status, headers, config) {
                $scope.isSaving = false;
                $scope.alerts.push({type: 'success', msg: 'News successfully Updated!'});
                $scope.category = news.category;
                Createnews.loadcategory(function(data1){
                 $scope.category = data1;

                 for (var i = 0; i < data1.length; i++)
                    {
                        if(news.category == data1[i].categoryid)
                        {
                            $scope.news.category = data1[i];
                        }
                    }
                });
            }).error(function (data, status, headers, config) {
                scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
            });
        }
        else
        {
            // if(related===undefined) {
            //     news['relatedworkshop'] = '';
            // } else {
            //     news['relatedworkshop'] = related.toString();
            // }
            $scope.alerts = [];
            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.isSaving = true;

            $http({
                url: Config.ApiURL + "/newscenter/edit",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(news)
            }).success(function (data, status, headers, config) {
                $scope.isSaving = false;
                $scope.alerts.push({type: 'success', msg: 'News successfully Updated!'});
            }).error(function (data, status, headers, config) {
                $scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
            });
        }


    }

     // UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////
    $scope.showimageList = function(size,path){
        var amazon = $scope.amazon;
        var modalInstance = $modal.open({
            templateUrl: 'newsimagelist.html',
            controller: imagelistCTRL,
            size: size,
            resolve: {
                path: function() {
                    return amazon
                }
            }

        });
    }
    $scope.stat="1";
    var pathimage = "";

    var pathimages = function(){
        $scope.amazonpath=pathimage;
    }

    var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
        console.log(path);
        $scope.amazonpath= path;
        $scope.imageloader=false;
        $scope.imagecontent=true;
        $scope.noimage = false;
        $scope.imggallery = false;

        var loadimages = function() {
            $http({
                url: Config.ApiURL + "/news/listimages",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                if(data.error == "NOIMAGE" ){
                    $scope.imggallery=false;
                    $scope.noimage = true;
                }else{
                    $scope.noimage = false;
                    $scope.imggallery=true;
                    $scope.imagelist = data;
                }
            }).error(function(data) {
            });
        }
        loadimages();

        $scope.path=function(path){
         var texttocut = Config.amazonlink + '/uploads/newsimage/';
         var newpath = path.substring(texttocut.length);
         pathimage = newpath;
         pathimages();
         $modalInstance.dismiss('cancel');
        }



        $scope.upload = function(files) {
            $scope.upload(files);
        };

        $scope.delete = function(id){
            var modalInstance = $modal.open({
                templateUrl: 'delete.html',
                controller: deleteCTRL,
                resolve: {
                    imgid: function() {
                        return id
                    }
                }

            });

        }

        var deleteCTRL = function($scope, $modalInstance, imgid) {

            $scope.alerts = [];

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.message="Are you sure do you want to delete this Photo?";
            $scope.ok = function() {
                $http({
                    url: Config.ApiURL+"/news/deletenewsimg/"+ imgid,
                    method: "get",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).success(function(data, status, headers, config) {
                    console.log(data);
                    $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
                    loadimages();
                    $modalInstance.close();
                }).error(function(data, status, headers, config) {
                    loadimages();
                    $modalInstance.close();
                    $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
                });
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        }

        $scope.closeAlert_image = function (index) {
            $scope.imagealert.splice(index, 1);
        };


        $scope.upload = function (files)
        {
            $scope.imagealert = [];
            var filename;
            var filecount = 0;
            if (files && files.length)
            {
                $scope.imageloader=true;
                $scope.imagecontent=false;

                for (var i = 0; i < files.length; i++)
                {
                    var file = files[i];

                    if (file.size >= 2000000) //2MB
                    {
                        $scope.imagealert.push({type: 'danger', msg: 'File ' + file.name + ' is too big. (Maximum of 2MB)'});

                        $scope.imageloader=false;
                        $scope.imagecontent=true;
                    }
                    else
                    {
                        var promises;

                        var fileExtension = '.' + file.name.split('.').pop();

                  // rename the file with a sufficiently random value and add the file extension back
                  var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + i + fileExtension;

                  promises = Upload.upload({

                               url:Config.amazonlink, //S3 upload url including bucket name
                               method: 'POST',
                               transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                  key: 'uploads/newsimage/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                              },
                              file: file
                          })
promises.then(function(data){

    filecount = filecount + 1;

    filename = data.config.file.name;
    var fileout = {
        'imgfilename' : renamedFile
    };
    $http({
        url: Config.ApiURL + "/news/saveimage",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: $.param(fileout)
    }).success(function (data, status, headers, config) {
        loadimages();
        if(filecount == files.length)
        {
            $scope.imageloader=false;
            $scope.imagecontent=true;
        }

    }).error(function (data, status, headers, config) {
        $scope.imageloader=false;
        $scope.imagecontent=true;
    });

});
}



}
}
};
$scope.cancel = function() {
    $modalInstance.dismiss('cancel');

};
};
// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////


    $scope.today = function () {
        $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;

    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.deletenewsimg = function (dataimg)
    {
        var fileout = {
            'imgfilename' : dataimg
        };

        $http({
            url: Config.ApiURL + "/news/deletenewsimg",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(fileout)
        }).success(function (data, status, headers, config) {
            loadimages();
        }).error(function (data, status, headers, config) {
            loadimages();
        });

    }


    $scope.imagegallery = function(size, path){
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        var amazon = $scope.amazon;
        var modalInstance = $modal.open({
            templateUrl: 'newsimagelist2.html',
            controller: imagegalleryCTRL,
            size: size,
            resolve: {
                path: function() {
                    return amazon
                }
            }
        });
    }

    var imagegalleryCTRL = function($modalInstance, $scope, path) {
        $scope.amazonpath= path;
        $scope.imageloader=false;
        $scope.imagecontent=true;
        $scope.noimage = false;
        $scope.imggallery = false;

        var loadimages = function() {
            $http({
                url: Config.ApiURL + "/news/listimages",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                if(data.error == "NOIMAGE" ){
                    $scope.imggallery=false;
                    $scope.noimage = true;
                } else {
                    $scope.noimage = false;
                    $scope.imggallery=true;
                    $scope.imagelist = data;
                }
            }).error(function(data) {
            });
        }
        loadimages();

        $scope.getimageurl = function(filename) {

            var modalInstance = $modal.open({
                templateUrl: 'imagesrc.html',
                controller: imagesrcCTRL,
                resolve: {
                    imagesource: function() {
                        return path + "/uploads/newsimage/" + filename;
                    }
                }
            });
        }

        var imagesrcCTRL = function($scope, imagesource, $modalInstance) {
            $scope.imgsrc = imagesource;

            $scope.ok = function() {
                $modalInstance.dismiss('cancel');
            }

        }

        $scope.upload = function (files)
        {
            $scope.imagealert = [];
            var filename;
            var filecount = 0;
            if (files && files.length)
            {

                $scope.imageloader=true;
                $scope.imagecontent=false;

                for (var i = 0; i < files.length; i++)
                {
                    var file = files[i];

                    if (file.size >= 2000000) //2MB
                    {
                        $scope.imagealert.push({type: 'danger', msg: 'File ' + file.name + ' is too big. (Maximum of 2MB)'});
                        $scope.imageloader=false;
                        $scope.imagecontent=true;
                    }
                    else
                    {
                        var promises;

                        var fileExtension = '.' + file.name.split('.').pop();

                  // rename the file with a sufficiently random value and add the file extension back
                  var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + i + fileExtension;

                  promises = Upload.upload({

                               url:Config.amazonlink, //S3 upload url including bucket name
                               method: 'POST',
                               transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                            },
                            fields : {
                                  key: 'uploads/newsimage/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                  AWSAccessKeyId: Config.AWSAccessKeyId,
                                  acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                                  policy: Config.policy, // base64-encoded json policy (see article below)
                                  signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                  "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                              },
                              file: file
                          })
                        promises.then(function(data){

                            filecount = filecount + 1;

                            filename = data.config.file.name;
                            var fileout = {
                                'imgfilename' : renamedFile
                            };
                            $http({
                                url: Config.ApiURL + "/news/saveimage",
                                method: "POST",
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                data: $.param(fileout)
                            }).success(function (data, status, headers, config) {
                                loadimages();
                                if(filecount == files.length)
                                {
                                    $scope.imageloader=false;
                                    $scope.imagecontent=true;
                                }

                            }).error(function (data, status, headers, config) {
                                $scope.imageloader=false;
                                $scope.imagecontent=true;
                            });

                        });
                        }



                        }
                        }
                        };

        //DELETE IMAGE INSIDE IMAGE GALLERY (MODAL)
        $scope.delete = function(id){
            var modalInstance = $modal.open({
                templateUrl: 'delete.html',
                controller: deleteCTRL,
                resolve: {
                    imgid: function() {
                        return id
                    }
                }

            });

        }

        var deleteCTRL = function($scope, $modalInstance, imgid) {

            $scope.alerts = [];

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.message="Are you sure do you want to delete this Photo?";
            $scope.ok = function() {
                $http({
                    url: Config.ApiURL+"/news/deletenewsimg/"+ imgid,
                    method: "get",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).success(function(data, status, headers, config) {
                    console.log(data);
                    $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
                    loadimages();
                    $modalInstance.close();
                }).error(function(data, status, headers, config) {
                    loadimages();
                    $modalInstance.close();
                    $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
                });
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        }
    }

   // WORKSHOP RELATED
   // var workshoprelated = function() {

   //      ManagenewsFactory.centernewsworkshoprelated($stateParams.newsid, function(data){
   //          // data.related = data.related.replace(/^,/, ''); //remove the first comma
   //          // console.log(data.related.length);
   //          if(data.related == null || data.related == '' || data.related.length == 0) {
   //              $scope.empty = true;
   //          } else {
   //              data.related = data.related.replace(/^,/, '');
   //              data.related = data.related.split(',');
   //              for(var x=0; x<data.related.length; x++) {
   //                  var indexof = data.titles.indexOf(data.related[x]);
   //                  if(indexof !== -1) {
   //                      data.titles.splice(indexof, 1);
   //                  }
   //              }
   //              $scope.empty = false;
   //              $scope.related = data.related;
   //          }
   //          console.log($scope.empty)
   //          $scope.workshoptitles = data.titles;
   //      });
   //  }

   //  workshoprelated();

   //  $scope.addrelated = function (title) {
   //      ManagenewsFactory.centernewsaddrelated($stateParams.newsid, title, function(data){
   //          workshoprelated();
   //      })
   //  }

   //  $scope.removerelated = function (title, related) {
   //      var indexof = related.indexOf(title);
   //      if(indexof !== -1) {
   //          related.splice(indexof, 1);
   //      }
   //      ManagenewsFactory.centernewsremoverelated($stateParams.newsid, related, function(data){
   //          workshoprelated();
   //      });
   //  }

   $scope.previewNews = function(news)
    {
        // console.log(news.author)
        // // $state.go('/bnb-buzz/preview', {news: news });
        // $http({
        //     method: "post",
        //     url: Config.BaseURL + "/bnb-buzz/preview",
        //     data: {
        //         title: news.title,
        //         author: news.author
        //     },
        //     headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        // }).success(function(){

        // });
        // $window.location.href = '/bnb-buzz/preview/&'+$.param(news);
        $window.open('/bnb-buzz/preview?'+$.param(news), 'bar');
        // $window.location.href = '/bnb-buzz/preview/&'+$.param(news);
        // console.log(news);
    }


})
