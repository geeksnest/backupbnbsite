'use strict';

/* Controllers */

app.controller('manageauditlogcsvCtrl', function ($scope, $state, $q,$stateParams, Config,manageauditlogFactory, $modal){
	var num = 10;
    var off = 1;
    var keyword = null;

	var loadlogs = function(){
		manageauditlogFactory.auditlogcsvlist(num,off, keyword, function(data){
            console.log(data);
			$scope.loglist = data.loglist;
			$scope.maxSize = 5;
	        $scope.bigTotalItems = data.total_items;
	        $scope.bigCurrentPage = data.index;
		});
	};

	loadlogs();

	 $scope.search = function (searchkeyword) {
        var off = 0;
        keyword = searchkeyword;
        loadlogs();

    }
    
    $scope.numpages = function (off, keyword) {
        loadlogs();
    }

    $scope.setPage = function (pageNo) {
    	off = pageNo;
        loadlogs();
    };

    $scope.resetsearch = function(){
    	off = 1;
    	keyword = null;
    	loadlogs();
    };

    $scope.deletefile = function(fileid) {
        var modalInstance = $modal.open({
            templateUrl: 'auditfileDelete.html',
            controller: fileDeleteCTRL,
            resolve: {
                fileid: function() {
                    return fileid
                }
            }
        });
    }

    var fileDeleteCTRL = function($scope, $modalInstance, fileid) {
        $scope.ok = function() {
            manageauditlogFactory.deletecsvfile(fileid, function(data){
                actionalert(data.status, data.msg);
                $modalInstance.dismiss('cancel');
                loadlogs();
            });
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    var actionalert = function(status,msg){
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: status, msg: msg });     
    }

	


})