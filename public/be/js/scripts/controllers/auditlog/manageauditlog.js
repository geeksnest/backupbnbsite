'use strict';

/* Controllers */

app.controller('manageauditlogCtrl', function ($scope, $state, $q,$stateParams, Config,manageauditlogFactory){
	var num = 10;
    var off = 1;
    var keyword = null;

	var loadlogs = function(){
		manageauditlogFactory.auditloglist(num,off, keyword, function(data){
			$scope.loglist = data.loglist;
			$scope.maxSize = 5;
	        $scope.bigTotalItems = data.total_items;
	        $scope.bigCurrentPage = data.index;
		});
	};

	loadlogs();

	 $scope.search = function (searchkeyword) {
        var off = 0;
        keyword = searchkeyword;
        loadlogs();

    }
    
    $scope.numpages = function (off, keyword) {
        loadlogs();
    }

    $scope.setPage = function (pageNo) {
    	off = pageNo;
        loadlogs();
    };

    $scope.resetsearch = function(){
    	off = 1;
    	keyword = null;
    	loadlogs();
    };

	


})