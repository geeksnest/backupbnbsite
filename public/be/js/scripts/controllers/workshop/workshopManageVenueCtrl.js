app.controller("workshopManageVenueCtrl", function($scope, Config, ManageWorkshopFactory, toaster, Factory, $modal){

  var num     = 10;
  var off     = 1;
  var keyword = null;

  var paginate = function(off, keyword) {
    ManageWorkshopFactory.listofvenues(num, off, keyword, function(data){
      $scope.venues      = data;
      $scope.maxSize     = 5;
      $scope.TotalItems  = data.total_items;
      $scope.CurrentPage = data.index;
    });
  };
  paginate(off, keyword);

  $scope.search = function (searchkeyword) {
    // var off = 1;
    off     = 1;
    keyword = searchkeyword;
    paginate(off, searchkeyword);
  };

  $scope.clearsearch = function() {
    // var off = 1;
    off     = 1;
    keyword = null;
    angular.element('.searchtext').val("");
    paginate(off,null);
  };

  $scope.setPage = function (pageNo) {
    paginate(pageNo, keyword);
    off = pageNo;
  };

  $scope.editworkshopvenue = function(workshopvenueid) {
    var modalInstance = $modal.open({
      templateUrl: 'workshopvenueedit.html',
      controller: workshopvenueEditCTRL,
      resolve: {
        workshopvenueid: function() {
          return workshopvenueid;
        }
      }
    });
  };
  var workshopvenueEditCTRL = function($scope, $modalInstance, workshopvenueid, $state) {
    $scope.ok = function() {
      $state.go('workshopvenueeditpage', {workshopvenueid: workshopvenueid });
      $modalInstance.dismiss('cancel');
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };

  $scope.delete = function(workshopvenueid) {
    var modalInstance = $modal.open({
      templateUrl: 'workshopvenuedelete.html',
      controller: workshopvenueDeleteCTRL,
      resolve: {
        workshopvenueid: function() {
          return workshopvenueid;
        }
      }
    });
  };
  var workshopvenueDeleteCTRL = function($scope, $modalInstance, workshopvenueid) {
    $scope.ok = function() {
      ManageWorkshopFactory.deleteworkshopvenue(workshopvenueid, function(data){
        $modalInstance.dismiss('cancel');
        paginate(off, keyword);
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  };
});
