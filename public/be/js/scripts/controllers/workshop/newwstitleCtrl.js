app.controller("newwstitleCtrl", function($scope, ManageWorkshopFactory, Factory){ "use strict";
  var Brochure = null;

  $scope.SEO = function(title) {
    $scope.wsttl.titleslugs = Factory.SEO(title);
  };

  $scope.onChangeBrochure = function(brochure) {
    $scope.wsttl.brochure = Brochure;
    if(Brochure === null) {
        if(brochure.length < 1) {
          $scope.pdfName = "";
        } else {

          $scope.pdfName = brochure[0].name;
          $scope.hasBrochure = Brochure = brochure;

        }
    } else {
        $scope.hasBrochure = Brochure;
        if(brochure.length < 1) {
          $scope.pdfName = Brochure[0].name;
        } else {
          $scope.pdfName = Brochure[0].name;
          Brochure = brochure;
        }
    }
  };

  $scope.clearBrochure = function() {
    $scope.pdfName = "";
    $scope.hasBrochure = Brochure = null;
  };

  $scope.newWstitle = function(wsttl) {
    $scope.wsttl.brochure = Brochure;
    if(wsttl.hasOwnProperty("body")) {
        if(wsttl.body === undefined || wsttl.body === "") {
            Factory.toaster("warning","Warning","Body is required!");
        } else {

          $scope.isBusy = true; //disable the form

          if(Brochure !== null) {
              Factory.upload('uploads/workshopbrochure/',Brochure[0], function(data) {
                data.then(function(data) {
                  wsttl['brochure'] = data.config.file.name;
                  Savenewsstitle(wsttl); //function
                });
              });
          } else {
              Savenewsstitle(wsttl); //function
          }

        }
    } else {
      Factory.toaster("warning","Warning","Body is required!");
    }
  };

  function Savenewsstitle(value) {
    ManageWorkshopFactory.savenewwstitle(value, function(data){
        if(data.hasOwnProperty("suc")) {
          Factory.toaster("success","Success",data.suc);
          $scope.wsttl = angular.copy(undefined, $scope.wsttl);
          $scope.pdfName = "";
        } else {
          Factory.toaster("error","Error",data.err);
        }
        $scope.isBusy = false;
    });
  }


});
