app.controller("workshopCreateVenueCtrl", function($scope, Config, ManageWorkshopFactory, Factory){

  ManageWorkshopFactory.loadstate(function(data){
       $scope.centerstate = data;
  });
  $scope.statechange = function(statecode)
  {
      ManageWorkshopFactory.loadcity(statecode,function(data){
          $scope.centercity = data;
          $scope.venue.city =data[0].city;
          ManageWorkshopFactory.loadzip(data[0].city,function(data){
              $scope.getcenterzip = data;
              $scope.venue.zipcode =data[0].zip;
          });
      });
  };

  $scope.citychange = function(cityname)
  {
      ManageWorkshopFactory.loadzip(cityname,function(data){
       ManageWorkshopFactory.loadzip(data[0].city,function(data){
          $scope.getcenterzip = data;
          $scope.venue.zipcode =data[0].zip;
      });
      });
  };

  $scope.createvenue = function(venue) {
    $scope.isBusy = true;
    ManageWorkshopFactory.createvenue(venue, function(data) {
			if(data.error == true) {
        Factory.modal('error',createvenueCtrl,'sm',data.errorMsg);
      } else {
        $scope.venue = angular.copy(undefined, $scope.venue);
        Factory.modal('success',createvenueCtrl,'sm','New Venue successfully created');
      }
      $scope.isBusy = false;
		});
  };

  var createvenueCtrl = function($modalInstance, $scope, data) {
    $scope.msg = data;
    $scope.close = function() {
      $modalInstance.dismiss();
    };
  };

});
