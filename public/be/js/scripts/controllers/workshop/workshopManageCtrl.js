app.controller("workshopManageCtrl", function($scope, Config, ManageWorkshopFactory, Factory, $modal){

  var num = 10;
	var off = 1;
	var keyword = null;

  var workshoplist = function(off, keyword) {
		ManageWorkshopFactory.workshoplist(num, off, keyword, function(data) {
			$scope.workshops = data;
			$scope.maxSize_workshop = 5;
			$scope.TotalItems_workshop = data.total_items;
			$scope.CurrentPage_workshop = data.index;
		});
	};
	workshoplist(off, keyword);

	$scope.setPage_workshop = function (pageNo) {
		workshoplist(pageNo, keyword);
		off = pageNo;
	};

	$scope.search_workshop = function (searchkeyword) {
		var off = 1;
		workshoplist(off, searchkeyword);
		keyword = searchkeyword;
	};

  $scope.clearsearch_workshop = function() {
    var off = 1;
    workshoplist(off, null);
    angular.element('.searchtextworkshop').val("");
    keyword = null;
  };

  $scope.details = function(workshop) {
    Factory.modal('workshopdetails',detailsCtrl,'md',workshop);
  };
  var detailsCtrl = function($modalInstance, $scope, data) {
    $scope.workshop = data;
    console.log(data);
    $scope.close = function() {
      $modalInstance.dismiss();
    };
  };


  $scope.editworkshop = function(workshopid) {
    Factory.modal('workshopedit',workshopEditCTRL,'sm',workshopid);
	};
	var workshopEditCTRL = function($modalInstance, $scope, $state, data) {
		$scope.ok = function() {
			$state.go('workshopeditpage', {workshopid: data });
			$modalInstance.dismiss();
		};
		$scope.cancel = function() {
			$modalInstance.dismiss();
		};
	};

  $scope.removeworkshop = function(workshopid) {
    Factory.modal('workshopremove',workshopRemoveCTRL,'sm',workshopid);
	};
	var workshopRemoveCTRL = function($modalInstance, $scope, data) {
		$scope.ok = function() {
			ManageWorkshopFactory.removeworkshop(data, function(data) {
        if(data.error == true) {
          Factory.modal('error',errorCtrl,'sm',data.errorMsg);
        } else {
          workshoplist(off, keyword);
        }
				$modalInstance.dismiss();
			});
		};
		$scope.cancel = function() {
			$modalInstance.dismiss();
		};
	};

  var errorCtrl = function($modalInstance, $scope, data) {
    $scope.msg = data;
    $scope.close = function() {
      $modalInstance.dismiss();
    };
  };

});
