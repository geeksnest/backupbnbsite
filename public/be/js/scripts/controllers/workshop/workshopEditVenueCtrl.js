app.controller("workshopEditVenueCtrl", function($scope, Config, ManageWorkshopFactory, $stateParams, Factory) {
  ManageWorkshopFactory.loadstate(function(data){
       $scope.centerstate = data;
  });

  ManageWorkshopFactory.editworkshopvenue($stateParams.workshopvenueid, function(data) {
    $scope.venue = data;
    ManageWorkshopFactory.loadcity(data.SPR,function(data){
      $scope.centercity = data;
    });
    ManageWorkshopFactory.loadzip(data.city,function(data){
      $scope.getcenterzip = data;
    });
  });

  $scope.statechange = function(statecode) {
      ManageWorkshopFactory.loadcity(statecode,function(data){
          $scope.centercity = data;
          $scope.venue.city =data[0].city;
          ManageWorkshopFactory.loadzip(data[0].city,function(data){
              $scope.getcenterzip = data;
              $scope.venue.zipcode =data[0].zip;
          });
      });
  };

  $scope.citychange = function(cityname) {
      ManageWorkshopFactory.loadzip(cityname,function(data){
       ManageWorkshopFactory.loadzip(data[0].city,function(data){
          $scope.getcenterzip = data;
          $scope.venue.zipcode =data[0].zip;
        });
      });
  };

  $scope.updatevenue = function(venue) {
    ManageWorkshopFactory.updatevenue(venue, $stateParams.workshopvenueid, function(data) {
      if(data.error == true) {
        Factory.modal('error',updatevenueCtrl,'sm',data.errorMsg);
      } else {
        Factory.modal('success',updatevenueCtrl,'sm','Workshop Venue has been successfully updated');
      }
    });
  };

  var updatevenueCtrl = function($modalInstance, $scope, data) {
    $scope.msg = data;
    $scope.close = function() {
      $modalInstance.dismiss();
    };
  };

});
