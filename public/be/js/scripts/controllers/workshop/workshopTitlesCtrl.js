app.controller("workshopTitlesCtrl", function($scope, Config, $modal, Upload, ManageWorkshopFactory, toaster, Factory){
  var num     = 10;
  var off     = 1;
  var keyword = null;

      //{MODAL**
      var msgmodal = function(msg){
        var message ="neil";
        var modalInstance = $modal.open({
          templateUrl: 'success.html',
          controller: msgCTRL,
          resolve: {
            message: function () {
              return msg;
            }
          }
        });
        var msgCTRL = function ($scope, $modalInstance, $state, message) {
          $scope.message = message;
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        };
      };
      //**MODAL}//

  	var titlelist = function(off, keyword) {
  		ManageWorkshopFactory.titlelist(num, off, keyword, function(data) {
  			$scope.titlelist    = data;
  			$scope.maxSize1     = 5;
  			$scope.TotalItems1  = data.total_items;
  			$scope.CurrentPage1 = data.index;
  		});
  	};
  	titlelist(off, keyword);

  	// setpage for workshop titles
  	$scope.setPage_title = function (pageNo) {
      off = pageNo;
  		titlelist(pageNo, keyword);
  	};

  	$scope.search_title = function (searchkeyword) {
  		// var off = 1;
      off     = 1;
      keyword = searchkeyword;
  		titlelist(off, searchkeyword);
  	};
    $scope.clearsearch_title = function() {
      // var off = 1;
      off     = 1;
      keyword = null;
      angular.element('.searchtexttitle').val("");
      titlelist(off, null);
    };

    $scope.openCreateTitle = function() {
      Factory.modal('workshop_title',workshop_title_createCTRL,'md',"");
    };

    var workshop_title_createCTRL = function ($scope, $modalInstance) {
      $scope.popupTitle  = "Create Workshop Title";
      $scope.popupSubmit = "Create";

      $scope.onChangeBrochure = function(brochure) {
        if(brochure.length < 1) { $scope.pdfName = ""; }
        else { $scope.pdfName = brochure[0].name; }
      };

      $scope.save = function(workshoptitle) {
        ManageWorkshopFactory.checktitlename(workshoptitle.title, function(data){
          function createNow() {
            ManageWorkshopFactory.createtitle(workshoptitle, function(data) {
              if(data.error === true) {
                Factory.toaster('error','Error!',data.errorMsg);
              } else {
                Factory.toaster('success','Success!','New Workshop Title successfully created.');
                titlelist(off, keyword);
                $modalInstance.dismiss();
              }
            });
          }

          if(data.exists === true) {
            Factory.toaster('error','Error!','The workshop title already exist. Please try a new one.');
          } else {
            if(workshoptitle.hasOwnProperty('brochure') === true) {
              if(workshoptitle.brochure !== null) {
                Factory.upload('uploads/workshopbrochure/',workshoptitle.brochure[0], function(data) {
                  data.then(function(data) {
                    workshoptitle['brochure'] = data.config.file.name;
                    createNow(); //function
                  });
                });
              } else {
                createNow(); //function
              }
            } else {
              createNow(); //function
            }
          }
        }); //end of checktitlename factory
      }; //end of save function

      $scope.cancel = function() {
        $modalInstance.dismiss();
      };
    }; //end of workshop_title_createCTRL var function

  	// remove title
  	$scope.removetitle = function(workshoptitleid) {
      Factory.modal('workshoptitleremove.html',workshoptitleremoveCTRL,'sm',workshoptitleid);
  	};
  	var workshoptitleremoveCTRL = function($scope, $modalInstance, data) {
  		$scope.ok = function() {
  			ManageWorkshopFactory.removetitle(data, function(data){
  				$modalInstance.dismiss();
  				titlelist(off, keyword);
  			});
  		};
  		$scope.cancel = function() {
  			$modalInstance.dismiss();
  		};
  	};
  	// ;remove title

  	// edit title
    $scope.openUpdateTitle = function(workshopTitle) {
      Factory.modal('workshop_title',workshop_title_updateCTRL,'md',workshopTitle);
    };
    var workshop_title_updateCTRL = function($scope, $modalInstance, data) {
      var wew              = data.brochure;
      $scope.workshopTitle = data;
      $scope.pdfName       = data.brochure;
      $scope.popupTitle    = "Update Workshop Title";
      $scope.popupSubmit   = "Update";

      $scope.onChangeBrochure = function(brochure) {
        if(brochure.length < 1) { $scope.pdfName = "" ; }
        else { $scope.pdfName = brochure[0].name; }
      };

      $scope.save = function(workshopTitle) {
        var brochure = "";
        workshopTitle['workshoptitleid'] = data.workshoptitleid;
        if(Array.isArray(workshopTitle.brochure) === true) {
            Factory.upload('uploads/workshopbrochure/',workshopTitle.brochure[0], function(data) {
              data.then(function(data) {
                workshopTitle['brochure'] = data.config.file.name;
                updateNow(); //function
              });
            });
        } else {
            updateNow(); //function
        }

        function updateNow() {
          ManageWorkshopFactory.updatetitle(workshopTitle, function(data){
            if(data.error === true) {
              Factory.toaster("error","Error!",data.errorMsg);
            } else {
              Factory.toaster("success","Success!","Workshop Title successfully updated.");
              titlelist(off, keyword);
              $modalInstance.dismiss();
            }
          });
        }
      };
      
      $scope.cancel = function() {
        titlelist(off, keyword);
        $modalInstance.dismiss();
      };
    }; //end of var function workshop_title_updateCTRL
});
