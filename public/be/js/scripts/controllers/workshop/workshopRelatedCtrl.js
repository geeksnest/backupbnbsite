app.controller("workshopRelatedCtrl", function($scope, Config, ManageWorkshopFactory, Factory){
  ManageWorkshopFactory.related(function (data) {
    $scope.titlesforrelated     = data.titles;
    $scope.relatedstory         = data.relatedstories;
    $scope.relatedmainarticle   = data.relatedmainarticles;
    $scope.relatedcenterarticle = data.relatedcenterarticles;
  });
});
