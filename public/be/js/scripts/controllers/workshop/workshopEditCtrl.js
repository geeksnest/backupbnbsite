app.controller("workshopEditCtrl", function($scope, Config, ManageWorkshopFactory, Factory,  $stateParams){
  ManageWorkshopFactory.workshoptitles(function(data) { $scope.workshoptitles = data; });
  ManageWorkshopFactory.workshoplists(function(data) { $scope.venues = data.venues; });
  ManageWorkshopFactory.loadcenter(function(data){ $scope.centers = data; });

  var loadworkshopinfo = function() {
    ManageWorkshopFactory.editworkshop($stateParams.workshopid, function(data){
      console.log(data);
      $scope.workshop = data.workshopprop;
      $scope.workshop.associatedcenter = data.workshopprop.associatedcenter;
      if(data.workshopprop.sale == null || data.workshopprop.sale == 0 ) {
        $scope.discount = false;
      } else {
        $scope.discount = true;
      }
    });
  };
  loadworkshopinfo();

  $scope.setdiscount = function() {
    $scope.discount = true;
  };
  $scope.nodiscount = function () {
    $scope.discount = false;
    $scope.workshop.sale = null;
  };

  $scope.updateworkshop = function(upworkshop) {
    ManageWorkshopFactory.updateworkshop(upworkshop, $stateParams.workshopid, function(data) {
      if(data.error == true) {
        Factory.modal('error',updateWorkshopCtrl,'sm',data.errorMsg);
      } else {
        Factory.modal('success',updateWorkshopCtrl,'sm',"Workshop has been successfully updated");
        loadworkshopinfo();
      }
    });
  };
  var updateWorkshopCtrl = function($modalInstance, $scope, data) {
    $scope.msg = data;
    $scope.close = function() {
      $modalInstance.dismiss();
    };
  };

  //DATE PICKER
  $scope.today = function () {
    $scope.dt = new Date();
  };

  $scope.today();
  $scope.clear = function () {
    $scope.dt = null;

  };

  $scope.toggleMin = function () {
    $scope.minDate = $scope.minDate ? null : new Date();
  };

  $scope.toggleMin();

  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };

  $scope.open1 = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened1 = true;
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1,
    class: 'datepicker'
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];

  $scope.hours = [
    {time:'01'},
    {time:'02'},
    {time:'03'},
    {time:'04'},
    {time:'05'},
    {time:'06'},
    {time:'07'},
    {time:'08'},
    {time:'09'},
    {time:'10'},
    {time:'11'},
    {time:'12'}
  ];
  $scope.minutes = [
    {time:'00'},
    {time:'05'},
    {time:'10'},
    {time:'15'},
    {time:'20'},
    {time:'25'},
    {time:'30'},
    {time:'35'},
    {time:'40'},
    {time:'45'},
    {time:'50'},
    {time:'55'}
  ];

  $scope.puttoend = function(datadate) {
    $scope.workshop.dateend = moment(datadate).format("YYYY-MM-DD");
  };
});
