'use strict';

app.controller('workshopCtrl', function($scope,$http, $modal, $state, $stateParams, Config, ManageWorkshopFactory){



	if ($stateParams.tab == '3') {
		$scope.workshoplist = true;
	} else if ($stateParams.tab == '4') {
		$scope.workshopregistrants = true;
	}

	$scope.cancel = false;
	$scope.validvenue = false;

	$scope.venue = {
		venuename: "",
	};
    
	var oriVenue = angular.copy($scope.venue);
	//{MODAL**
		var msgCTRL = function ($scope, $modalInstance, $state, message) {
			$scope.message = message;
			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
		}
		var msgmodal = function(msg){
			var message ="neil";
			var modalInstance = $modal.open({
				templateUrl: 'success.html',
				controller: msgCTRL,
				resolve: {
					message: function () {
						return msg;
					}
				}
			});
		}
    //**MODAL}//

	$scope.checkvenuename = function(venuename) {
		// var venuename = $scope.venue.venuename;
		ManageWorkshopFactory.checkvenuename(venuename, function(data){
				$scope.validvenue = data.exists;
		});
	};

	// FACTORIZED PAGINATION
	var num = 10;
	var off = 1;
	var keyword = null;

	var paginate = function(off, keyword) {
		ManageWorkshopFactory.listofvenues(num, off, keyword, function(data){
			$scope.venues = data;
			$scope.maxSize = 5;
			$scope.TotalItems = data.total_items;
			$scope.CurrentPage = data.index;
		});
	}

	paginate(off, keyword);

	$scope.search = function (searchkeyword) {
		var off = 1;
		paginate(off, searchkeyword);
		keyword = searchkeyword;
	}

	$scope.clearsearch = function() {
		var off = 1;
		paginate(off,null);
		angular.element('.searchtext').val("");
		keyword = null;
	}

	$scope.clearsearch_title = function() {
		var off = 1;
		titlelist(off, null);
		angular.element('.searchtexttitle').val("");
		keyword = null;
	}

	$scope.clearsearch_workshop = function() {
		var off = 1;
		workshoplist(off, null);
		angular.element('.searchtextworkshop').val("");
		keyword = null;
	}

	$scope.numpages = function (off, keyword) {
		paginate(off, keyword);
	}

	$scope.setPage = function (pageNo) {
		paginate(pageNo, keyword);
		off = pageNo;
	};

 // ;FACTORIZED PAGINATION

 	$scope.createvenue = function(venue) {
 		$scope.cancel = true;
		ManageWorkshopFactory.createvenue(venue, function(data) {
			angular.element('.venue').val('');
			$scope.venue = angular.copy(oriVenue);
			paginate(off, keyword);
			msgmodal("New Workshop Venue has been successfully created");
			$state.go('manageworkshop', {tab: 1 });
		});
	}

	// DELETE
	$scope.delete = function(workshopvenueid) {
		var modalInstance = $modal.open({
			templateUrl: 'workshopvenuedelete.html',
			controller: workshopvenueDeleteCTRL,
			resolve: {
				workshopvenueid: function() {
					return workshopvenueid
				}
			}
		});
	}
	var workshopvenueDeleteCTRL = function($scope, $modalInstance, workshopvenueid) {
		$scope.ok = function() {
			ManageWorkshopFactory.deleteworkshopvenue(workshopvenueid, function(data){
				$modalInstance.dismiss('cancel');
				paginate(off, keyword);
			});
		};
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};
	};
	// ;DELETE

	$scope.editworkshopvenue = function(workshopvenueid) {
		var modalInstance = $modal.open({
			templateUrl: 'workshopvenueedit.html',
			controller: workshopvenueEditCTRL,
			resolve: {
				workshopvenueid: function() {
					return workshopvenueid
				}
			}
		});
	}
	var workshopvenueEditCTRL = function($scope, $modalInstance, workshopvenueid, $state) {
		$scope.ok = function() {
			$state.go('workshopvenueeditpage', {workshopvenueid: workshopvenueid });
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};
	};

	// ================ EDIT VENUE PAGE ==================

	// $scope.wew = $stateParams.workshopvenueid;

	ManageWorkshopFactory.editworkshopvenue($stateParams.workshopvenueid, function(data) {
		$scope.upvenue = data;
		ManageWorkshopFactory.loadcity(data.SPR,function(data){
			$scope.centercity = data;
		});

		ManageWorkshopFactory.loadzip(data.city,function(data){

			$scope.getcenterzip = data;
		});
	});

	$scope.checkvenuenameWException = function(venuename) {
		// var venuename = $scope.venue.venuename;
		ManageWorkshopFactory.checkvenuenameWException(venuename, $stateParams.workshopvenueid, function(data){
			$scope.validvenue = data.exists;
		});
	};

	$scope.updatevenue = function(venue) {
		ManageWorkshopFactory.updatevenue(venue, $stateParams.workshopvenueid, function(data) {
			msgmodal("Workshop Venue has been successfully updated");
		});
	}

// ================== WORKSHOP TAB ==================

	$scope.workshop = { metatitle: '' }
	var oriWorkshop = angular.copy($scope.workshop);

	ManageWorkshopFactory.workshoplists(function(data) {
		$scope.selectlist = data;
	});

	$scope.createworkshop = function(workshop) {
		console.log(workshop);
		ManageWorkshopFactory.createworkshop(workshop, function(data) {
			$scope.workshop = angular.copy(oriWorkshop);
			msgmodal("New Workshop has been successfully created");
			$state.go('createworkshop', {tab: 3 });
		});
	}

	var loadcenter = function(){
		ManageWorkshopFactory.loadcenter(function(data){
			$scope.newslocation = data;
		});
	}
	loadcenter();

	var workshoplist = function(off, keyword) {
		ManageWorkshopFactory.workshoplist(num, off, keyword, function(data) {
			$scope.workshops = data;
			$scope.maxSize_workshop = 5;
			$scope.TotalItems_workshop = data.total_items;
			$scope.CurrentPage_workshop = data.index;
		});
	}
	workshoplist(off, keyword);

	$scope.setPage_workshop = function (pageNo) {
		workshoplist(pageNo, keyword);
		off = pageNo;
	};

	$scope.search_workshop = function (searchkeyword) {
		var off = 1;
		workshoplist(off, searchkeyword);
		keyword = searchkeyword;
	}

	// REMOVE WORKSHOP LIST
	$scope.removeworkshop = function(workshopid) {
		var modalInstance = $modal.open({
			templateUrl: 'workshopremove.html',
			controller: workshopRemoveCTRL,
			resolve: {
				workshopid: function() {
					return workshopid
				}
			}
		});
	}
	var workshopRemoveCTRL = function($scope, $modalInstance, workshopid) {
		$scope.ok = function() {
			ManageWorkshopFactory.removeworkshop(workshopid, function(data){
				$modalInstance.dismiss('cancel');
				workshoplist(off, keyword);
			});
		};
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};
	};
	// ;REMOVE WORKSHOP LIST

	$scope.editworkshop = function(workshopid) {
		var modalInstance = $modal.open({
			templateUrl: 'workshopedit.html',
			controller: workshopEditCTRL,
			resolve: {
				workshopid: function() {
					return workshopid
				}
			}
		});
	}
	var workshopEditCTRL = function($scope, $modalInstance, workshopid, $state) {
		$scope.ok = function() {
			$state.go('workshopeditpage', {workshopid: workshopid });
			$modalInstance.dismiss('cancel');
		};
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		};
	};

	$scope.workshop = {};

	// EDIT WORKSHOP
	var loadworkshopinfo = function() {
		ManageWorkshopFactory.editworkshop($stateParams.workshopid, function(data){
			$scope.upworkshop = data.workshopprop;
			$scope.workshop.associatedcenter = data.workshopprop.associatedcenter
			if(data.workshopprop.sale == null || data.workshopprop.sale == 0 ) {
				$scope.discount = false;
			} else {
				$scope.discount = true;
			}
		});
	}
	loadworkshopinfo();

	$scope.setdiscount = function() {
		$scope.discount = true;
	}
	$scope.nodiscount = function () {
		$scope.discount = false;
		$scope.upworkshop.sale = null;
	}

	$scope.updateworkshop = function(upworkshop) {
		ManageWorkshopFactory.updateworkshop(upworkshop, $stateParams.workshopid, function(data) {
			msgmodal("Workshop has been successfully updated");
			loadworkshopinfo();
		});
	}
	// ;EDIT WORKSHOP

	//  WORKSHOPRELATED ===HEAD===
	ManageWorkshopFactory.related(function (data) {
		$scope.titlesforrelated = data.titles;
		$scope.relatedstory = data.relatedstories;
		$scope.relatedmainarticle = data.relatedmainarticles;
		$scope.relatedcenterarticle = data.relatedcenterarticles;
	})

//////////////////////////////////////////////

    //DATE PICKER
	$scope.today = function () {
    $scope.dt = new Date();
    };

    $scope.today();
    $scope.clear = function () {
    $scope.dt = null;

    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.open1 = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = true;

    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        class: 'datepicker'
    };

    // $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

	$scope.hours = [
         {time:'01'},
         {time:'02'},
         {time:'03'},
         {time:'04'},
         {time:'05'},
         {time:'06'},
         {time:'07'},
         {time:'08'},
         {time:'09'},
         {time:'10'},
         {time:'11'},
         {time:'12'}
    ];
    $scope.minutes = [
    	 {time:'00'},
         {time:'05'},
         {time:'10'},
         {time:'15'},
         {time:'20'},
         {time:'25'},
         {time:'30'},
         {time:'35'},
         {time:'40'},
         {time:'45'},
         {time:'50'},
         {time:'55'}
    ]

    $scope.puttoend = function(datadate){
    	$scope.workshop.dateend = moment(datadate).format("YYYY-MM-DD");
    }

    ManageWorkshopFactory.loadstate(function(data){
         $scope.centerstate = data;
    });

    $scope.statechange = function(statecode)
    {
        ManageWorkshopFactory.loadcity(statecode,function(data){
            $scope.centercity = data;
            $scope.venue.city =data[0].city;
            ManageWorkshopFactory.loadzip(data[0].city,function(data){
                $scope.getcenterzip = data;
                $scope.venue.zipcode =data[0].zip;
            });
        });
    }

     $scope.citychange = function(cityname)
    {
        ManageWorkshopFactory.loadzip(cityname,function(data){
         ManageWorkshopFactory.loadzip(data[0].city,function(data){
            $scope.getcenterzip = data;
            $scope.venue.zipcode =data[0].zip;
        });
        });
    }

});
