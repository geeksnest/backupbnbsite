app.controller("workshopRegistrantsCtrl", function($scope, Config, ManageWorkshopFactory, toaster, Factory){
  var num    = 10;
  var page   = 1;
  var filter = '';

  ManageWorkshopFactory.workshoptitles(function(data) { $scope.workshoptitles = data; });
  ManageWorkshopFactory.centersnvenues(function(data) { $scope.centersnvenues = data; });

  function registrantsList(page,filter) {
    ManageWorkshopFactory.registrantslist(num,page,filter, function(data) {
      data.registrants.map(function(reg) {
        reg.dateregistered = new Date(reg.date_created).getTime();
      });
      $scope.registrants = data.registrants;
      $scope.TotalItems  = data.TotalItems;
      $scope.CurrentPage = data.CurrentPage;
    });
  }
  registrantsList(page,filter); //function

  $scope.filterRegistrants= function (filter_) {
    page   = 1;
    filter = filter_;
  	registrantsList(page, filter);
  };

  $scope.setPage = function(page_) {
    page = page_;
  	registrantsList(page, filter);
  };

  $scope.moreDetails = function(registrant) {
    Factory.modal('registrant_details', moreDetailsCtrl, 'md', registrant);
  };

  var moreDetailsCtrl = function($modalInstance, $scope, data) {
    $scope.ws = data;
    $scope.close = function() {
      $modalInstance.dismiss();
    };
  };
});
