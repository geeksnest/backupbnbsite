app.factory("CenterincomeFactory", function ($http, Config) {
	return {
		oneonone: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/oneonone",
				method: "GET"
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		filteroneonone: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filteroneonone",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		groupsession: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/groupsession",
				method: "GET"
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		filtergroupsession: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filtergroupsession",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		chartperday: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/chartperday",
				method: "GET"
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		chartpermonth: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/chartpermonth",
				method: "GET"
			}).success(function (data){
					callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		filterchartperday: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filterchartperday",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		filterchartpermonth: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filterchartpermonth",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		oneononefranchise: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/oneononefranchise",
				method: "GET"
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		filteroneononefranchise: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filteroneononefranchise",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		groupfranchise: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/groupfranchise",
				method: "GET"
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		filtergroupfranchise: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filtergroupfranchise",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		allfranchise: function(callback) {
			$http({
				url: Config.ApiURL + "/centerincome/allfranchise",
				method: "GET",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" }
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		filterallfranchise: function(filter, callback) {
			$http({
				url: Config.ApiURL + "/centerincome/filterallfranchise",
				method: "POST",
				headers: { "Content-Type" : "application/x-www-form-urlencoded" },
				data: $.param(filter)
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		}
	};
});
