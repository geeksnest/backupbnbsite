app.factory('Egallery', function($http, $q, Config){
	return {
		data: {},
		getpathfromlist:'',
		loadimage: function(callback){
			$http({
				url: Config.ApiURL+"/gallery/list",
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		saveImage: function(fileout, callback){
			$http({
				url: Config.ApiURL+"/gallery/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data ,fileout );
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		deleteslider: function(id, callback) {
			$http({
				url: Config.ApiURL+"/gallery/delete",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(id)
			}).success(function (data) {
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		updateimgorder: function(data, callback) {
			$http({
				url: Config.ApiURL+"/gallery/updateorder",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data) {
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		updateUrl: function(data, callback) {
			$http({
				url: Config.ApiURL+"/gallery/updateUrl",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		}
	};
});
