app.factory('Createnews', function($http, $q, Config){
  	return {

  		data: {},
    	loadcategory: function(callback){
    	 	$http({
    	 		url: Config.ApiURL + "/news/listcategory",
    	 		method: "GET",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    	 	}).success(function (data) {
    	 		data = data;
    	 		callback(data);
    	 	}).error(function () {
            callback({err:"Can't communicate to API properly"});
        });
    	 },
         loadcenter: function(callback){
            $http({
                url: Config.ApiURL + "/news/listcenter",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         workshoptitles: function(callback) {
            $http({
                url: Config.ApiURL + "/createnews/workshoptitles",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         loadimgbanner: function(_apiUrl, callback) {
           $http({
                url: Config.ApiURL +""+ _apiUrl,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         saveVid : function(_apiUrl, data, callback) {
            $http({
                url: Config.ApiURL + _apiUrl,
                method: "POST",
                headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data){
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         }
    };
});
