app.factory('ManagenewsFactory', function($http, $q, Config){
  return {
    data: {},
    loadlist: function(num, off, keyword , callback){
      $http({
        url: Config.ApiURL +"/presspage/managenews/" + num + '/' + off + '/' + keyword,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        callback(data);
        pagetotalitem = data.total_items;
        currentPage = data.index;
      }).error(function () {
          callback({err:"Can't communicate to API properly"});
      });
    },
    workshoptitles: function(newsid, callback) {
      $http({
        url: Config.ApiURL + "/mainnews/workshoptitles/"+newsid,
        method: "GET"
      }).success(function (data){
        callback(data);
      }).error(function () {
          callback({err:"Can't communicate to API properly"});
      });
    },
    addrelated: function(newsid, title, callback) {
      var related = { 'newsid':newsid, 'title':title };
      $http({
        url: Config.ApiURL + "/mainnews/addrelated",
        method: "POST",
        headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
        data: $.param(related)
      }).success(function (data){
        callback(data);
      }).error(function () {
          callback({err:"Can't communicate to API properly"});
      });
    },
    removerelated: function(newsid, related, callback) {
      var Related = { 'newsid':newsid, 'related': related.toString() };
      $http({
        url: Config.ApiURL + "/mainnews/removerelated",
        method: "POST",
        headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
        data: $.param(Related)
      }).success(function (data){
        callback(data);
      }).error(function () {
          callback({err:"Can't communicate to API properly"});
      });
    },
    centernewsworkshoprelated: function(newsid, callback) {
      $http({
        url: Config.ApiURL + "/centernews/workshoprelated/"+newsid,
        method: "GET"
      }).success(function (data){
        callback(data);
      }).error(function () {
          callback({err:"Can't communicate to API properly"});
      });
    },
    centernewsaddrelated: function(newsid, title, callback) {
      var related = { 'newsid':newsid, 'title':title };
      $http({
        url: Config.ApiURL + "/centernews/addrelated",
        method: "POST",
        headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
        data: $.param(related)
      }).success(function (data){
        callback(data);
      }).error(function () {
          callback({err:"Can't communicate to API properly"});
      });
    },
    centernewsremoverelated: function(newsid, related, callback) {
      var Related = { 'newsid':newsid, 'related': related.toString() };
      $http({
        url: Config.ApiURL + "/centernews/removerelated",
        method: "POST",
        headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
        data: $.param(Related)
      }).success(function (data){
        callback(data);
      }).error(function () {
          callback({err:"Can't communicate to API properly"});
      });
    },
    pressstatus: function(pressid, callback) {
      $http({
        url: Config.ApiURL + "/presspage/changestatus",
        method: "POST",
        headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
        data: $.param({pressid:pressid})
      }).success(function (data){
        callback(data);
      }).error(function () {
          callback({err:"Can't communicate to API properly"});
      });
    }
  };
});
