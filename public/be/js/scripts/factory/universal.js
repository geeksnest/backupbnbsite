app.factory("Factory", function($http, Config, $modal, Upload, toaster) {
  return {
    modal: function(tplUrl, ctrl, size, data) {
      var modalInstance = $modal.open({
          templateUrl: tplUrl,
          controller: ctrl,
          size: size,
          resolve: {
              data: function() {
                  return data;
              }
          }
      });
      return modalInstance;
    },
    Modal: function(tplUrl, size, data, ctrl) {
      var modalInstance = $modal.open({
          templateUrl: tplUrl,
          controller: ctrl,
          size: size,
          resolve: {
              data: function() {
                  return data;
              }
          }
      });
      return modalInstance;
    },
    modaL: function(tplUrl, size, ctrl) {
      var modalInstance = $modal.open({
          templateUrl: tplUrl,
          controller: ctrl,
          size: size
      });
      return modalInstance;
    },
    upload: function(path, file, callback) {
      var promise = Upload.upload({
          url: Config.amazonlink, //S3 upload url including bucket name
          method: 'POST',
          transformRequest: function (data, headersGetter) {
                              //Headers change here
                              var headers = headersGetter();
                              delete headers['Authorization'];
                              return data;
                            },
          fields : {
            key: path + file.name, // the key to store the file on S3, could be file name or customized
            AWSAccessKeyId: Config.AWSAccessKeyId,
            acl: 'private', // sets the access to the uploaded file in the bucket: private or public
            policy: Config.policy, // base64-encoded json policy (see article below)
            signature: Config.signature, // base64-encoded signature based on policy string (see article below)
            "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
          },
          file: file
      });
      callback(promise);
    },
    toaster: function(type, title, body) {
      return toaster.pop({
        type: type,
        title: title,
        body: body,
        showCloseButton: true
      });
    },
    randomalphanum: function() {
        var s = "";
        while(s.length<36&&36>0) {
            var r = Math.random();
            s+= (r<0.1?Math.floor(r*100):String.fromCharCode(Math.floor(r*26) + (r>0.5?97:65)));
        }
        return s;
    },
    // THIS IS FOR BANNER MODULE
    saveimage: function(image, callback) {
      $http({
        url: Config.ApiURL+"/save/image",
        method: "POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        data: $.param(image)
      }).success(function(data) {
        callback(data);
      }).error(function() {
        callback({err:"Can't communicate to API properly"});
      });
    },
    loadimages: function(page, callback) {
      $http({
        url: Config.ApiURL+"/loadimages/"+page,
        method: "GET",
      headers: {'Content-Type':'application/x-www-form-urlencoded'}
      }).success(function(data) {
        callback(data);
      });
    },
    removeimage: function(id, callback) {
      $http({
        url: Config.ApiURL+"/removeimage/"+id,
        method: "GET",
      headers: {'Content-Type':'application/x-www-form-urlencoded'}
      }).success(function(data) {
        callback(data);
      }).error(function() {
        callback({err:"Can't communicate to API properly"});
      });
    },
    editimage: function(id, callback) {
      $http({
        url: Config.ApiURL+"/editimage/"+id,
        method: "GET",
      headers: {'Content-Type':'application/x-www-form-urlencoded'}
      }).success(function(data) {
        callback(data);
      }).error(function() {
        callback({err:"Can't communicate to API properly"});
      });
    },
    updateimage: function(banner, callback) {
      $http({
        url: Config.ApiURL+"/update/image",
        method: "POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        data: $.param(banner)
      }).success(function(data) {
        callback(data);
      }).error(function() {
        callback({err:"Can't communicate to API properly"});
      });
    },
    togglestatusimage: function(id, callback) {
      $http({
        url: Config.ApiURL+"/togglestatus/image/"+id,
        method: "GET",
        headers: {'Content-Type':'application/x-www-form-urlencoded'}
      }).success(function(data) {
        callback(data);
      }).error(function() {
        callback({err:"Can't communicate to API properly"});
      });
    },
    loadSinglebanner: function(page, callback) {
      $http({
        url: Config.ApiURL+"/loadsinglebanner/image/"+page,
        method: "GET",
        headers: {'Content-Type':'application/x-www-form-urlencoded'}
      }).success(function(data) {
        callback(data);
      }).error(function() {
        callback({err:"Can't communicate to API properly"});
      });
    },
    updateimgorder: function(data, callback) {
      $http({
        url: Config.ApiURL+"/slider/updateorder",
        method: "POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        data: $.param(data)
      }).success(function (data) {
        callback(data);
      }).error(function() {
        callback({err:"Can't communicate to API properly"});
      });
    },
    // THIS IS THE END FOR BANNER MODULE
    SEO: function(title) {
      if(title === undefined ) {
        return "";
      } else {
        title = title.replace(/[^\w -]+/g,'');
        return angular.lowercase(title.replace(/ +/g,'-'));
      }
    },
    datenow: function() {
      var d = new Date();

      var curr_date = d.getDate();
        if(curr_date < 10) { curr_date = "0" + curr_date; }

      var curr_month = d.getMonth() + 1;
        if(curr_month < 10) { curr_month = "0" + curr_month; }

      var curr_year = d.getFullYear();

      return curr_year + "-" + curr_month + "-" + curr_date;
    }
  };
});
