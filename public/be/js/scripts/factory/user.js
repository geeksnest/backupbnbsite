app.factory('Usersfactory', function($http, $q, Config){
 return {

    usernameExists: function(data, callback) {
       $http({
        url: "http://bnbapi/validate/username/"+data,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data) {
            callback(data);
        }).error(function () {
  					callback({err:"Can't communicate to API properly"});
  			});
    },
    emailExists: function(data, callback) {
        $http.post({
            url:'http://pi.api/user/emailexist/' + data.email,
            method: 'POST',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data) {
            callback(data);
        }).error(function () {
  					callback({err:"Can't communicate to API properly"});
  			});
        return;
    },
    register:function(user, callback){
        $http({
            url: Config.ApiURL + '/user/register',
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(user)
        }).success(function (data) {
            callback(data);
        }).error(function () {
            callback({'error':true});
        });
        return;
    },
    availableREGDISCENlist:function(userid, callback){
        $http({
            url: Config.ApiURL + '/user/available/regdiscen/' + userid,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data) {
            callback(data);
        }).error(function (data, status, headers, config) {
            callback({'error':true});
        });
        return;
    },
    login: function(log, callback){
        $http({
            url: 'http://pi.api/user/login',
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(log)
        }).success(function (data) {
            callback(data);
        }).error(function (data, status, headers, config) {
            callback({'error':true});
        });
        return;
    },
    changepassword: function(cpassword, callback){
        $http({
            url: Config.ApiURL + '/user/changepassword',
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(cpassword)
        }).success(function (data) {
            callback(data);
        }).error(function (data, status, headers, config) {
            callback({'error':true});
        });
    },
    validateusername: function(username, callback) {
      $http({
        url:Config.ApiURL + "/validate/username/"+username,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data) {
           callback(data);
      }).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    },
    validateuseremail: function(useremail, callback) {
      $http({
  			url: Config.ApiURL + "/validate/email/"+useremail,
  			method: "GET",
  			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  		}).success(function (data) {
  			   callback(data);
  		}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
    }
  };
});
