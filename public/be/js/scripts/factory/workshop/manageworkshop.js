app.factory('ManageWorkshopFactory', function($http, Config){
	return {
		createvenue: function(venue,callback) {
			$http({
				url: Config.ApiURL + "/be/workshop/createvenue",
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param(venue)
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		listofvenues: function(num, off, keyword, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/listofvenues/"+num+"/"+off+"/"+keyword,
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		// //dati to, pwd na siguro idelete soon (12-28-15)
		// checkvenuename: function(venuename, callback) {
		// 	$http({
		// 		url: Config.ApiURL + "/BE/workshop/checkvenuename/"+venuename,
		// 		method: "GET",
		// 		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		// 	}).success(function (data, status, headers, config) {
		// 		data = data;
		// 		callback(data);
		// 	}).error(function (data, status, headers, config) {
		// 		data = data;
		// 		callback(data);
		// 	});
		// },
		deleteworkshopvenue: function(workshopvenueid, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/deleteworkshopvenue/"+workshopvenueid,
				method: "GET",
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		editworkshopvenue: function(workshopvenueid, callback) {
			$http({
				url: Config.ApiURL + "/be/workshop/editworkshopvenue/"+workshopvenueid,
				method: "GET",
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		// //dati to, pwd na siguro idelete soon (12-29-15)
		// checkvenuenameWException: function(venuename, workshopvenueid, callback) {
		// 	$http({
		// 		url: Config.ApiURL + "/BE/workshop/checkvenuenamewexception/"+venuename+"/"+workshopvenueid,
		// 		method: "GET",
		// 		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		// 	}).success(function (data, status, headers, config) {
		// 		data = data;
		// 		callback(data);
		// 	}).error(function (data, status, headers, config) {
		// 		data = data;
		// 		callback(data);
		// 	});
		// },
		updatevenue: function(venue, workshopvenueid, callback) {
			$http({
				url: Config.ApiURL + "/be/workshop/updatevenue",
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param({venue:venue,workshopvenueid:workshopvenueid})
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		createtitle: function(workshoptitle, callback) {
			var titleslugs = workshoptitle['title'].replace(/[^\w ]+/g,'');
            titleslugs = angular.lowercase(titleslugs.replace(/ +/g,'-'));
			workshoptitle['titleslugs'] = titleslugs

			$http({
				url: Config.ApiURL + "/be/workshop/createtitle",
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param(workshoptitle)
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		titlelist: function(num, off, keyword, callback)	{
			$http({
				url: Config.ApiURL + "/be/workshop/listoftitles/"+num+"/"+off+"/"+keyword,
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		removetitle: function(workshoptitleid, callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/removetitle/"+workshoptitleid,
				method: "GET",
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		checktitlename: function(titlename, callback) {

			var titleslugs = titlename.replace(/[^\w ]+/g,'');
            titleslugs = angular.lowercase(titleslugs.replace(/ +/g,'-'));
            // var thisPost = { 'title': titlename, 'titleslugs': titleslugs };

			$http({
				url: Config.ApiURL + "/BE/workshop/checktitlename/"+titleslugs,
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		workshoplists: function(callback) {
			$http({
				url: Config.ApiURL + "/BE/workshop/lists",
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		workshoptitles: function(callback) {
			$http({
				url: Config.ApiURL + "/workshop/titles",
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data) {
					callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		centersnvenues: function(callback) {
			$http({
				url: Config.ApiURL + "/workshop/centersnvenues",
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data) {
					callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		createworkshop: function(workshop, callback) {
			$http({
				url: Config.ApiURL + "/be/workshop/createworkshop",
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param(workshop)
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		workshoplist: function(num, off, keyword, callback)	{
			$http({
				url: Config.ApiURL + "/BE/workshop/listofworkshop/"+num+"/"+off+"/"+keyword,
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		removeworkshop: function(workshopid, callback) {
			$http({
				url: Config.ApiURL + "/be/workshop/removeworkshop/"+workshopid,
				method: "GET",
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		editworkshop: function(workshopid, callback) {
			$http({
				url: Config.ApiURL + "/be/workshop/edit/"+workshopid,
				method: "GET",
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		updateworkshop: function(upworkshop, workshopid, callback) {
			$http({
				url: Config.ApiURL + "/be/workshop/update",
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param({workshop:upworkshop,workshopid:workshopid})
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		updatetitle: function(workshopTitle, callback) {
			var titleslugs = workshopTitle['title'].replace(/[^\w ]+/g,'');
            titleslugs = angular.lowercase(titleslugs.replace(/ +/g,'-'));

			workshopTitle['titleslugs'] = titleslugs

			$http({
				url: Config.ApiURL + "/be/workshop/updatetitle",
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param(workshopTitle)
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		registrantslist: function(num, page, filter, callback)	{
			$http({
				url: Config.ApiURL + "/workshop/registrantslist",
				method: "POST",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: $.param({filter:filter,num:num,page:page})
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		removeregistrant: function(registrantid, callback)	{
			$http({
				url: Config.ApiURL + "/workshop/removeregistrant/"+registrantid,
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		related: function(callback) {
			$http({
				url: Config.ApiURL + "/workshop/related",
				method: "GET",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
			}).success(function (data) {
				data = data;
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		loadcenter: function(callback){
            $http({
                url: Config.ApiURL + "/news/listcenter",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
         },
         loadstate: function(callback){
            $http({
                url: Config.ApiURL + "/center/statelist",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
         },
         loadcity: function(statecode,callback){
            $http({
                url: Config.ApiURL + "/center/citylist/" + statecode,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
         },
         loadzip: function(cityname,callback){
            $http({
                url: Config.ApiURL + "/center/ziplist/" + cityname,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
         },
				 savenewwstitle: function(wstitle, callback) {
						 $http({
								 url: Config.ApiURL + "/workshop/new/title",
								 method: "POST",
								 headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								 data: $.param(wstitle)
						 }).success(function (data) {
								 callback(data);
						 }).error(function () {
								 callback({err:"Can't communicate to API properly"});
						 });
				 }
	};
});
