app.factory('manageauditlogFactory', function($http, $q, Config){
    return {
        auditloglist: function(num, off, keyword, callback){
            $http({
                url: Config.ApiURL +"/be/auditlogs/auditloglist/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function() {
                callback({err:"Can't communicate to API properly"});
            });
        },
        auditlogcsvlist: function(num, off, keyword, callback){
            $http({
                url: Config.ApiURL +"/be/auditlogs/auditloglistcsv/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
         deletecsvfile: function(fileid, callback){
            $http({
                url: Config.ApiURL +"/be/auditlogs/deletecsvfile/" + fileid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        }
    };
});
