app.factory('ManageStoryFactory', function($http, $q, Config){
	return {
		storyPerCenter: function(centerid, num, off, keyword, callback) {
			$http({
            url: Config.ApiURL + "/stories/managestoriespercenter/" + centerid + '/' + num + '/' + off + '/' + keyword,
            method: "GET",
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        	}).success(function(data) {
	            callback(data);
       		 }).error(function(data, status, headers, config) {
            	callback(status);
        	});
		},
		deleteStory: function(storyid, callback) {
			$http({
				url: Config.ApiURL + "/story/delete/" + storyid,
				method: "GET",
				headers: {'Content-Type' : 'application-x-www-form-urlencode'}
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		dropdowncenterlist: function(num, off, keyword ,userid, callback){
	    	 	$http({
	    	 		url: Config.ApiURL +"/center/managecenterdropdown/" + num + '/' + off + '/' + keyword + '/' + userid,
	    	 		method: "GET",
	    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	    	 	}).success(function (data, status, headers, config) {
	    	 		callback(data);
	    	 		pagetotalitem = data.total_items;
	    	 		currentPage = data.index;
	    	 	}).error(function () {
							callback({err:"Can't communicate to API properly"});
					});
    	 }
	};
});
