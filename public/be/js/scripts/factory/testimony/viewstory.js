app.factory('ViewStoryFactory', function($http, $q, Config){
	return {
		getInformation: function(storyid, callback) {
			$http({
				url: Config.ApiURL + "/story/information/" + storyid,
				method: "GET",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		saveReviewedStory: function(story, callback) {
			$http({
				url: Config.ApiURL + "/story/savereviewedstory",
				method: "POST",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
				data: $.param(story)
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		addmetatag: function(metaprop, callback) {
			$http({
				url: Config.ApiURL + "/story/addmetatags",
				method: "POST",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
				data: $.param(metaprop)
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		metatags: function(storyid, callback) {
			$http({
				url: Config.ApiURL + "/story/metatags/"+storyid,
				method: "GET",
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		updatemetatags: function(storyid, metatags, callback) {
			var metaprop = { 'storyid':storyid, 'metatags': metatags.toString() };
			$http({
				url: Config.ApiURL + "/story/updatemetatags",
				method: "POST",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
				data: $.param(metaprop)
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		workshoptitles: function(storyid, callback) {
			$http({
				url: Config.ApiURL + "/story/workshoptitles/"+storyid,
				method: "GET"
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		addrelated: function(storyid, title, callback) {
			var related = { 'storyid':storyid, 'title':title };
			$http({
				url: Config.ApiURL + "/story/addrelated",
				method: "POST",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
				data: $.param(related)
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		removerelated: function(storyid, related, callback) {
			var Related = { 'storyid':storyid, 'related': related.toString() };
			$http({
				url: Config.ApiURL + "/story/removerelated",
				method: "POST",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
				data: $.param(Related)
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
	};
});
