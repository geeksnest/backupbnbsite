app.factory('MycenterFactory', function($http, Config){
  	return {
      mycenter: function(ctr_id, ctr_mgr_id, callback) {
      	 	$http({
      	 		url: Config.ApiURL +"/centermanager/mycenter",
      	 		method: "POST",
      	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param({ctrid:ctr_id, ctrmgrid:ctr_mgr_id})
      	 	}).success(function (data) {
      	 		  callback(data);
      	 	}).error(function () {
      	 		  callback({err:"Can't communicate to API properly"});
      	 	});
    	 },
       savecenter: function(center,callback){
          $http({
              url: Config.ApiURL + "/center/saveeditedcenter",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(center)
          }).success(function (data) {
              callback(data);
          }).error(function () {
              callback({err:"Can't communicate to API properly"});
          });
       },
       loadcity: function(statecode,callback){
          $http({
              url: Config.ApiURL + "/center/citylist/" + statecode,
              method: "GET",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function (data) {
              callback(data);
          }).error(function () {
              callback({err:"Can't communicate to API properly"});
          });
       },
       loadzip: function(cityname,callback){
          $http({
              url: Config.ApiURL + "/center/ziplist/" + cityname,
              method: "GET",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function (data) {
              callback(data);
          }).error(function () {
              callback({err:"Can't communicate to API properly"});
          });
       }
    };
});
