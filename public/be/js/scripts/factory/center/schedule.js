app.factory('scheduleFactory', function($http, $q, Config){
    return {

         loadschedule: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/schedule/loadschedule/"+ centerid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         saveschedule:function(session,callback){
            $http({
                url: Config.ApiURL + "/center/schedule/saveschedule",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(session)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         loadschedulebyid:function(sessionid,callback){
            $http({
                url: Config.ApiURL + "/center/schedule/loadschedulebyid/"+sessionid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         updateschedule:function(session,callback){
            $http({
                url: Config.ApiURL + "/center/schedule/updateschedule",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(session)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         deleteschedule:function(sessionid,callback){
            $http({
                url: Config.ApiURL + "/center/schedule/deleteschedule/"+sessionid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         getClass: function(callback) {
           $http({
             url: Config.ApiURL + "/center/getclasses",
             method: "GET",
             headers: {
               'Content-Type': 'application/x-www-form-urlencoded'
             }
           }).success(function(data, status, headers, config) {
             callback(data);
           });
         }
    };
});
