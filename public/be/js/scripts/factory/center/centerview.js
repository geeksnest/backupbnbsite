app.factory('centerViewFactory', function($http, $q, Config){
    return {
         loadcenter: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/centerview/loadcenter/"+ centerid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },

        saveimagesforslider: function(fileout, callback) {
            $http({
                url: Config.ApiURL + "/center/saveimagesforslider",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(fileout)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },

        loadCenterImages: function(centerid, callback) {
            $http({
                url: Config.ApiURL + "/be/center/loadcenterimages/" + centerid,
                method: "GET",
                headers: {'Content-Type' : 'application/x-www-form-urlencoded' }
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },

        saveordering: function(thispost, callback) {
            $http({
                url: Config.ApiURL + "/center/saveordering",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(thispost)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
    };
});
