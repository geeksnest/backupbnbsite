app.factory('CenterCalendarFactory', function($http, $q, Config){
    return {

         saveEvent: function(calendar,callback){

            $http({
                url: Config.ApiURL + "/center/calendar/createevent",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(calendar)
            }).success(function (data, status, headers, config) {
                callback({msg: data.msg, type: data.type});
            }).error(function (data, status, headers, config) {
                callback({msg: data.msg, type: data.type});
            });
         },
         listEvent: function(num, off, keyword,centerid , callback){
                $http({
                    url: Config.ApiURL +"/center/calendar/listevent/" + num + '/' + off + '/' + keyword + '/' + centerid,
                    method: "GET",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function (data, status, headers, config) {
                    callback(data);
                    pagetotalitem = data.total_items;
                    currentPage = data.index;
                }).error(function () {
                    callback({err:"Can't communicate to API properly"});
                });
         },
         loadEvent: function(activityid,callback){
            $http({
                url: Config.ApiURL + "/center/calendar/loadevent/"+ activityid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         updateEvent: function(calendar,callback){
            $http({
                url: Config.ApiURL + "/center/calendar/updateevent",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(calendar)
            }).success(function (data, status, headers, config) {
                callback({msg: data.msg, type: data.type});
            }).error(function (data, status, headers, config) {
                callback({msg: data.msg, type: data.type});
            });
         },
    };
});
