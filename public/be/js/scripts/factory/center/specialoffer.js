app.factory('specialofferFactory', function($http, $q, Config){
    return {
         saveOffer: function(offer,callback){
            $http({
                url: Config.ApiURL + "/center/offer/saveoffer",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(offer)
            }).success(function (data) {
               callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         loadOffer: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/offer/loadoffer/"+ centerid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         deleteOffer: function(offerid,callback){
            $http({
                url: Config.ApiURL + "/center/offer/deleteoffer/"+ offerid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         offerinfo: function(offerid,callback) {
            $http({
                url: Config.ApiURL + "/center/offer/editoffer/"+ offerid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         updateOffer: function(offer, callback) {
            $http({
                url: Config.ApiURL + "/center/offer/updateoffer",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(offer)
            }).success(function (data) {
               callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         updateOfferWithUpload: function(offer, callback){
            $http({
                url: Config.ApiURL + "/center/offer/updateofferwithupload",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(offer)
            }).success(function (data) {
               callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         }
    };
});
