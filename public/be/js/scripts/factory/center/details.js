app.factory('detailsFactory', function($http, $q, Config){
    return {
         saveLocation: function(location,callback){
            $http({
                url: Config.ApiURL + "/center/location/savelocation",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(location)
            }).success(function (data, status, headers, config) {
                callback({msg: data.msg, type: data.type});
            }).error(function (data, status, headers, config) {
                callback({msg: data.msg, type: data.type});
            });
         },
         loadlocation: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/location/loadlocation/"+ centerid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         saveOpenhours: function(openhours,callback){
            $http({
                url: Config.ApiURL + "/center/openhours/saveopenhour",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(openhours)
            }).success(function (data) {
                callback({msg: data.msg, type: data.type});
            }).error(function (data) {
                callback({msg: data.msg, type: data.type});
            });
         },
         loadhours: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/openhours/loadhour/"+ centerid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         loadsociallinks: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/social/loadsocial/"+ centerid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:true,errMsg:"Cant connect to API"});
            });
         },
         loadlinksbyid: function(linkid,callback){
            $http({
                url: Config.ApiURL + "/center/social/loadlinksbyid/"+ linkid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         // updateSociallinks: function(linkdata,callback){

         //    $http({
         //        url: Config.ApiURL + "/center/social/updatelinksbyid",
         //        method: "POST",
         //        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
         //        data: $.param(linkdata)
         //    }).success(function (data, status, headers, config) {
         //        callback({msg: data.msg, type: data.type});
         //    }).error(function (data, status, headers, config) {
         //        callback({msg: data.msg, type: data.type});
         //    });

         // },
         loadphonenumber: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/phone/getcenterphonenumber/"+ centerid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         savephonenumber: function(phone,callback){
            $http({
                url: Config.ApiURL + "/center/phone/savecenterphonenumber",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(phone)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         getemail: function(centerid,callback){
            $http({
                url: Config.ApiURL + "/center/email/getcenteremail/"+ centerid,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         saveemail: function(email,callback) {
            $http({
                url: Config.ApiURL + "/center/email/saveemail",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(email)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         savedescription: function(details, callback) {
            $http({
                url: Config.ApiURL + "/centerdetails/savedescription",
                method: "POST",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(details)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
        centerdetails: function(centerid, callback) {
            $http({
                url: Config.ApiURL + "/center/details/"+centerid,
                method: "GET"
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        sociallinks: function(centerid, callback) {
            $http({
                url: Config.ApiURL + "/center/sociallinks/"+centerid,
                method: "GET"
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        updatesociallink: function(asset, callback) {
            $http({
                url: Config.ApiURL + "/centerdetails/updatesociallink",
                method: "POST",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(asset)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         updatesocialurlstatus: function(asset, callback) {
            $http({
                url: Config.ApiURL + "/centerdetails/updatesocialurlstatus",
                method: "POST",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(asset)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         getspnldata: function(centerid, callback) {
            $http({
                url: Config.ApiURL + "/center/spnl/"+centerid,
                method: "GET"
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
         setspnldata: function(centerid, spnlstatus, callback) {
            $http({
                url: Config.ApiURL + "/center/setspnl/"+centerid+"/"+spnlstatus,
                method: "GET"
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
    };
});
