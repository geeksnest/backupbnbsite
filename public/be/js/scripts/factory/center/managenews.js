app.factory('managenewsFactory', function($http, $q, Config){
  	return {
    		newslist: function(num, off, keyword, centerid , callback){
	    	 	$http({
	    	 		url: Config.ApiURL +"/center/news/newslist/" + num + '/' + off + '/' + keyword + '/' + centerid,
	    	 		method: "GET",
	    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	    	 	}).success(function (data) {
	    	 		callback(data);
	    	 	}).error(function () {
              callback({err:"Can't communicate to API properly"});
          });
    	 	}
    };
});
