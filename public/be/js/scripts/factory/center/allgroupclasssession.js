app.factory('allgroupclasssessionFactory', function($http, $q, Config){
    return {
        loadclasslist: function(num, off, keyword , callback){
            $http({
                url: Config.ApiURL +"/be/allgroupclasssession/allgroupclasssession/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        loadgroupsession: function(sessionid, userid, callback){
            $http({
                url: Config.ApiURL +"/be/getstarted/loadgroupsession/"+ sessionid +"/"+userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        loadclassunverifiedlist: function(num, off, keyword, callback) {
            $http({
                url: Config.ApiURL +"/be/allgroupclasssession/allgroupclasssessionunverfied/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        loadunverifiedgroupsession: function(sessionid, callback){
            $http({
                url: Config.ApiURL +"/be/getstarted/loadunverifiedgroupsession/"+ sessionid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        verifysession: function(session, callback) {
            $http({
                url: Config.ApiURL +"/be/getstarted/verifygroupsession",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(session)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        loadclasscanceledlist: function(num, off, keyword, callback) {
            $http({
                url: Config.ApiURL +"/be/allgroupclasssession/allgroupclasssessioncanceledlist/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        loadcanceledgroupsession: function(sessionid, callback){
            $http({
                url: Config.ApiURL +"/be/getstarted/loadcanceledgroupsession/"+ sessionid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
    };
});
