app.factory('cellFactory', function($http, $q, Config){
    return {
        savecontact: function(contact , callback){
            $http({
                url: Config.ApiURL +"/be/managercontract/savecontact",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(contact)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        loadcontact: function(centerid , callback){
            $http({
                url: Config.ApiURL +"/be/managercontract/loadcontact/"+ centerid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        testcontact: function(contact , callback){
            $http({
                url: Config.ApiURL +"/be/managercontract/testsmsmessage",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(contact)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
    };
});
