app.factory('beneplacepricingFactory', function($http, $q, Config){
  	return {
    	loadprice: function(callback){
    	 	$http({
    	 		url: Config.ApiURL + "/price/loadbeneplaceprice",
    	 		method: "GET",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    	 	}).success(function (data) {
    	 		callback(data);
    	 	}).error(function () {
            callback({err:"Can't communicate to API properly"});
        });
    	 },
         saveintro: function(price,callback){
            $http({
                url: Config.ApiURL + "/price/beneplacesaveintro",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(price)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
    };
});
