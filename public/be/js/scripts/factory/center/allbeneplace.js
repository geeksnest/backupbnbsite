app.factory('allbeneplaceFactory', function($http, $q, Config){
    return {
        loadclasslist: function(num, off, keyword , callback){
            $http({
                url: Config.ApiURL +"/be/allbeneplace/allbeneplacelist/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        loadgroupsession: function(sessionid , userid, callback){
            $http({
                url: Config.ApiURL +"/be/beneplace/loadbeneplacesession/"+ sessionid+"/"+userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        viewreferrallink: function(callback){
            $http({
                url: Config.ApiURL +"/be/beneplace/viewreferrallink",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        savereferrallink: function(ref,callback){
            $http({
                url: Config.ApiURL +"/be/beneplace/savereferral",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(ref)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        unverifiedclasslist: function(num, off, keyword , callback){
            $http({
                url: Config.ApiURL +"/be/allbeneplace/allunverifiedclasslist/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        loadunverifiedsession: function(sessionid , userid, callback){
            $http({
                url: Config.ApiURL +"/be/beneplace/loadunverifiedsession/"+ sessionid+"/"+userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        verifysession: function(session, callback) {
            $http({
                url: Config.ApiURL +"/be/getstarted/verifybeneplacesession",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(session)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        loadclasscancelledlist: function(num, off, keyword , callback){
            $http({
                url: Config.ApiURL +"/be/allbeneplace/loadclasscancelledlist/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadbenecancelledsession: function(sessionid , userid, callback){
            $http({
                url: Config.ApiURL +"/be/beneplace/loadbenecancelledsession/"+ sessionid+"/"+userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        }
    };
});
