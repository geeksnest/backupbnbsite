app.factory('ManageCenterFactory', function($http, $q, Config){
  	return {
    	 data: {},
    	 centerlist: function(num, off, keyword ,userid, callback){
	    	 	$http({
	    	 		url: Config.ApiURL +"/center/managecenter/" + num + '/' + off + '/' + keyword + '/' + userid,
	    	 		method: "GET",
	    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	    	 	}).success(function (data) {
	    	 		callback(data);
	    	 		pagetotalitem = data.total_items;
	    	 		currentPage = data.index;
	    	 	}).error(function () {
	    	 		callback({err:"Can't communicate to API properly"});
	    	 	});
    	 },
    	 dropdowncenterlist: function(num, off, keyword ,userid, callback){
	    	 	$http({
	    	 		url: Config.ApiURL +"/center/managecenterdropdown/" + num + '/' + off + '/' + keyword + '/' + userid,
	    	 		method: "GET",
	    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	    	 	}).success(function (data, status, headers, config) {
	    	 		callback(data);
	    	 		pagetotalitem = data.total_items;
	    	 		currentPage = data.index;
	    	 	}).error(function () {
	    	 		callback({err:"Can't communicate to API properly"});
	    	 	});
    	 },
       deletecenter: function(centerid, callback) {
         $http({
             url: Config.ApiURL + "/center/centerdelete/" + centerid,
             method: "POST",
             headers: {'Content-Type': 'application/x-www-form-urlencoded'}
         }).success(function(data) {
            callback(data);
         }).error(function() {
            callback({err:"Can't communicate to API properly"});
         });
       }

    };

});
