app.factory('CreateCenterFactory', function($http, $q, Config){
    return {
        data: {},
        loadstate: function(callback){
            $http({
                url: Config.ApiURL + "/center/statelist",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         loadcity: function(statecode,callback){
            $http({
                url: Config.ApiURL + "/center/citylist/" + statecode,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         loadzip: function(cityname,callback){
            $http({
                url: Config.ApiURL + "/center/ziplist/" + cityname,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         loadregionmanager: function(callback){
            $http({
                url: Config.ApiURL + "/center/regionmanagerlist",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         listregion: function(callback){
            $http({
                url: Config.ApiURL + "/center/listregion",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         loadcentermanager: function(callback){
            $http({
                url: Config.ApiURL + "/center/centermanagerlist",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         savecenter: function(center, callback){
            $http({
                url: Config.ApiURL + "/center/createcenter",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(center)
            }).success(function (data) {
                callback(data);
            }).error(function (data) {
                callback({err:"Can't communicate to API properly"});
            });
         }
    };
});
