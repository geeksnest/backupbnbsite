app.factory('regionanddistrictFactory', function($http, $q, Config){
  	return {
    	saveregion: function(region,callback){
    	 	$http({
    	 		url: Config.ApiURL + "/center/saveregion",
    	 		method: "POST",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(region)
    	 	}).success(function (data) {
    	 		callback(data);
    	 	}).error(function () {
            callback({err:"Can't communicate to API properly"});
        });
    	 },
         loadregion: function(num,off, keyword,callback){
            $http({
                url: Config.ApiURL + "/center/loadregion/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         deleteregion: function(region,callback){
            $http({
                url: Config.ApiURL + "/center/deleteregion",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(region)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         updateregion: function(region,callback){
            $http({
                url: Config.ApiURL + "/center/updateregion",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(region)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         savedistrict: function(district,callback){
            $http({
                url: Config.ApiURL + "/center/savedistrict",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(district)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         loadallregion: function(callback){
            $http({
                url: Config.ApiURL + "/center/loadallregion",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         loaddistrict: function(num,off, keyword,callback){
            $http({
                url: Config.ApiURL + "/center/loaddistrict/" + num + '/' + off + '/' + keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         deletedistrict: function(district,callback){
            $http({
                url: Config.ApiURL + "/center/deletedistrict",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(district)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         loaddistrictbyid: function(district,callback){
            $http({
                url: Config.ApiURL + "/center/loaddistrictbyid",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(district)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
          updateistrict: function(district,callback){
            $http({
                url: Config.ApiURL + "/center/updateistrict",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(district)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         }
    };
});
