app.factory('Managecontacts', function($http, $q, Config){
  	return {
        data: {},
    	loadlist:  function(num, off, keyword,stat, callback){
    	 	$http({
    	 		url: Config.ApiURL + "/managecontacts/list/"+ num + '/' + off + '/' + keyword + '/' + stat,
    	 		method: "GET",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    	 	}).success(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
                pagetotalitem = data.total_items;
                currentPage = data.index;
    	 	}).error(function () {
            callback({err:"Can't communicate to API properly"});
        });
    	 }
    };
});
