app.factory('Settings', function($http, $q, Config){
	return {
		saveimage: function(fileout, callback){
			$http({
				url: Config.ApiURL + "/settings/uploadlogo",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data) {
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		loadimage: function(callback){
			$http({
				url: Config.ApiURL + "/settings/logolist",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			}).error(function () {
				callback({err:"Can't communicate to API properly"});
			});
		},
		update: function(data, callback){
			$http({
				url: Config.ApiURL + "/settings/updatesociallink",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function () {
				callback({err:"Can't communicate to API properly"});
			});
		}
	};
});
