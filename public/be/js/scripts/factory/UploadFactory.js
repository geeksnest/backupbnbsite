app.factory("UploadFactory", function($http, Config, Upload) {
  return {
      upload: function(path, file, callback) {
        var promise = Upload.upload({
            url: Config.amazonlink, //S3 upload url including bucket name
            method: 'POST',
            transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                              },
            fields : {
              key: path + file.name, // the key to store the file on S3, could be file name or customized
              AWSAccessKeyId: Config.AWSAccessKeyId,
              acl: 'private', // sets the access to the uploaded file in the bucket: private or public
              policy: Config.policy, // base64-encoded json policy (see article below)
              signature: Config.signature, // base64-encoded signature based on policy string (see article below)
              "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
            },
            file: file
        });
        callback(promise);
      }
  };
});
