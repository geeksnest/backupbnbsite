app.factory('Managenewsletter', function($http, $q, Config){
  	return {
        data: {},
    	loadlist:  function(num, off, keyword, callback){
    	 	$http({
    	 		url: Config.ApiURL + "/managenewsletter/list/"+ num + '/' + off + '/' + keyword,
    	 		method: "GET",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    	 	}).success(function (data, status, headers, config) {
    	 		data = data;
    	 		callback(data);
                pagetotalitem = data.total_items;
                currentPage = data.index;
    	 	}).error(function () {
            callback({err:"Can't communicate to API properly"});
        });
    	 },
         create: function(newsletter,callback) {
            $http({
                url: Config.ApiURL + "/newsletter/add",
                method: "POST",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(newsletter)
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        checktitle: function(title, callback) {
            $http({
                url: Config.ApiURL + "/newsletter/checktitle/"+title,
                method: "GET",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        deletenews: function(id,callback){
         $http({
                url: Config.ApiURL + "/newsletter/deletenewsletter/"+id,
                method: "GET",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        deletenewspdf: function(id,callback){
         $http({
                url: Config.ApiURL + "/newsletter/deletenewspdf/"+id,
                method: "GET",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        editnewsletter: function(id, callback) {
            $http({
                url: Config.ApiURL + "/newsletter/edit/"+id,
                method: "GET",
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
        editnewsletterpdf: function(id, callback) {
            $http({
                url: Config.ApiURL + "/newsletter/editpdf/"+id,
                method: "GET",
            }).success(function (data) {
                data = data;
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
        },
         updatenewsletter: function(newsletter,callback) {
            $http({
            url: Config.ApiURL+"/newsletter/updatenewsletter",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data:$.param(newsletter)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         updatenewsletterpdf: function(newsletter,callback) {
            $http({
            url: Config.ApiURL+"/newsletter/updatenewsletterpdf",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data:$.param(newsletter)
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         }
    };
});
