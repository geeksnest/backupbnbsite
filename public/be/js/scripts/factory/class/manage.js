app.factory('ClassFactory', function($http, $q, Config){
	return {
		// loadimage: function(callback){
		// 	$http({
		// 		url: Config.ApiURL+"/gallery/list",
		// 		method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
		// 	}).success(function(data) {
		// 		callback(data);
		// 	})
		// },
		// saveImage: function(fileout, callback){
			// $http({
			// 	url: Config.ApiURL+"/gallery/save",
			// 	method: "POST",
			// 	headers: {'Content-Type':'application/x-www-form-urlencoded'},
			// 	data: $.param(fileout)
			// }).success(function (data, status, headers, config) {
			// 	callback(data ,fileout );
			// }).error(function (data, status, headers, config) {
			// 	callback(data ,fileout);
			// });
		// }
    get: function(num, off, keyword, callback) {
			$http({
				url: Config.ApiURL+"/class/get/" + num + "/" + off + "/" + keyword,
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			});
    },
    validateClass: function(id, classes, callback){
			$http({
				url: Config.ApiURL+"/class/validate/" + id + "/" + classes,
				method: "GET",headers: {'Content-Type':'application/x-www-form-urlencoded'}
			}).success(function(data) {
				callback(data);
			});
		},
    add: function(classes, callback) {
      $http({
				url: Config.ApiURL+"/class/save",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(classes)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
    },
    editclass: function(classes, callback) {
      $http({
				url: Config.ApiURL+"/class/update",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(classes)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
    },
    deleteclass: function(id, callback) {
      $http({
				url: Config.ApiURL+"/class/delete",
				method: "POST",
				headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param({ id: id })
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
    }
	};
});
