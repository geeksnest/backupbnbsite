app.factory('imgUpload', function($http, $q, Config){
   return {
     upload: function(files, Upload, _imgpath, _apilink, _alert, _loader_cont, callback){
        var filename;
        var filecount = 0;
        if (files && files.length){
             _loader_cont({loader:true, cont:false});
            for (var i = 0; i < files.length; i++)
            {
                var file = files[i];

                if (file.size >= 2000000){
                    _alert({type: 'danger', msg: 'File ' + file.name + ' is too big. (Maximum of 2MB)'});
                    _loader_cont({loader:false, cont:true});
                }
                else
                {
                    var promises;
                    var fileExtension = '.' + file.name.split('.').pop();
                    var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + i + fileExtension;
                    promises = Upload.upload({

                     url:Config.amazonlink,
                     method: 'POST',
                     transformRequest: function (data, headersGetter) {
                        var headers = headersGetter();
                        delete headers['Authorization'];
                        return data;
                    },
                    fields : {
                      key: _imgpath+''+renamedFile,
                      AWSAccessKeyId: Config.AWSAccessKeyId,
                      acl: 'private',
                      policy: Config.policy,
                      signature: Config.signature,
                      "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
                  },
                  file: file
                });

                    promises.then(function(data){
                        filecount = filecount + 1;
                        filename = data.config.file.name;
                        var fileout = {'imgfilename' : renamedFile};
                        $http({
                            url: Config.ApiURL+""+_apilink,
                            method: "POST",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            data: $.param(fileout)
                        }).success(function (data, status, headers, config) {

                            callback(data);
                            if(filecount == files.length) {
                               _loader_cont({loader:false, cont:true});

                           }

                       }).error(function (data, status, headers, config) {
                        _loader_cont({loader:false, cont:true});

                    });

                   });
                }



            }
        }
    },

};

});
