app.factory('Product', function($http, $q, Config){
	return {
		images : [],
		add: function(data, callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/add/product",
    	 		method: "post",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(data)
    	 	}).success(function (data) {
    	 		callback(data);
    	 	}).error(function () {
						callback({err:"Can't communicate to API properly"});
				});
		},
		loadimages: function(id, callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/product/loadimages/" + id,
    	 		method: "get",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    	 	}).success(function (data) {
    	 		callback(data);
    	 	}).error(function () {
						callback({err:"Can't communicate to API properly"});
				});
		},
		saveImage: function(id, data, callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/product/saveimage/" + id,
    	 		method: "post",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(data)
    	 	}).success(function (data) {
    	 		callback(data);
    	 	}).error(function () {
						callback({err:"Can't communicate to API properly"});
				});
		},
		deleteImage: function(id, data, callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/product/deleteimage/" + id,
    	 		method: "post",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param(data)
    	 	}).success(function (data) {
    	 		callback(data);
    	 	}).error(function () {
						callback({err:"Can't communicate to API properly"});
				});
		},
		load: function(callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/product/loadaddproduct",
    	 		method: "get",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    	 	}).success(function (data) {
    	 		callback(data);
    	 	}).error(function () {
						callback({err:"Can't communicate to API properly"});
				});
		},
		complete: function(data, callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/product/load/" + data ,
    	 		method: "get",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    	 	}).success(function (data) {
    	 			callback(data);
    	 	}).error(function () {
						callback({err:"Can't communicate to API properly"});
				});
		},
		getlist: function(num, off, keyword, find, callback) {
    	 	$http({
    	 		url: Config.ApiURL + "/product/loadlist/" + num + "/" + off + "/" + keyword + "/" + find,
    	 		method: "get",
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    	 	}).success(function (data) {
    	 		callback(data);
    	 	}).error(function () {
						callback({err:"Can't communicate to API properly"});
				});
		},
        validatecode: function(id, name, callback) {
            $http({
                url: Config.ApiURL + "/product/validatecode/" + id + "/" + name,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        delete: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/delete/" + id,
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        loadedit: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/loadedit/" + id,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        edit: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/edit",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        validatename: function(id, name, callback) {
            $http({
                url: Config.ApiURL + "/product/validatename/" + id + "/" + name,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        updatestatus: function(status, id, callback) {
            $http({
                url: Config.ApiURL + "/product/updatestatus/" + id + "/" + status,
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        validateSlugs: function(id, slugs, callback) {
            $http({
                url: Config.ApiURL + "/product/validateslug/" + id + "/" + slugs,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        addQuantity: function(id, data, callback) {
            $http({
                url: Config.ApiURL + "/product/addquantity/" + id,
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        addDiscount: function(id, data, callback){
            $http({
                url: Config.ApiURL + "/product/adddiscount/" + id,
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        viewproduct: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/viewproduct/" + id,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        viewproductdetails: function(id, page, type, callback) {
            $http({
                url: Config.ApiURL + "/product/viewdetails/" + id + "/" + type + "/" + page,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        selectImage: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/selectimage",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        loadcategory: function(num, off, keyword, callback) {
            $http({
                url: Config.ApiURL + "/product/loadcategorylist/" + num + "/" + off + "/" + keyword,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        validateCategory: function(id, category, callback) {
            $http({
                url: Config.ApiURL + "/product/validatecategory/" + id + "/" + category,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        loadsubcateogry: function(callback) {
            $http({
                url: Config.ApiURL + "/product/loadsubcateogry",
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        saveCategory: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/saveCategory",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        deleteCategory: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/deleteCategory/" + id,
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        loadeditcategory: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/loadeditcategory/" + id,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        editCategory: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/editcategory",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        loadtags: function(num, off, keyword, callback) {
            $http({
                url: Config.ApiURL + "/product/loadtags/" + num + "/" + off + "/" + keyword,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        deleteTags: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/deletetags/" + id,
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        validateTags: function(id, tags, callback) {
            $http({
                url: Config.ApiURL + "/product/validatetags/" + id + "/" + tags,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        addTags: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/saveTag",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        updatetags: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/updatetags",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        loadsubcategory: function(num, off, keyword, callback) {
            $http({
                url: Config.ApiURL + "/product/loadsubcategory/" + num + "/" + off + "/" + keyword,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        validateSubCategory: function(id, subcategory, callback) {
            $http({
                url: Config.ApiURL + "/product/validatesubcategory/" + id + "/" + subcategory,
                method: "get",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        loadtype: function(callback) {
            $http({
                url: Config.ApiURL + "/product/loadtypes",
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        addSubCategory: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/addsubcategory",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        deleteSubcategory: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/deletesubcategory/" + id,
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        loadsubcatedit: function(id, callback){
            $http({
                url: Config.ApiURL + "/product/loadeditsubcategory/" + id,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        updateSubcategory: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/updatesubcategory",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        loadtypes: function(num, off, keyword, callback) {
            $http({
                url: Config.ApiURL + "/product/loadtypes/" + num + "/" + off + "/" + keyword,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        deleteType: function(id, callback){
            $http({
                url: Config.ApiURL + "/product/deletetype/" + id,
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        updatetype: function(data, callback){
            $http({
                url: Config.ApiURL + "/product/updatetype",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        validateType: function(type, callback){
            $http({
                url: Config.ApiURL + "/product/validatetype/" + type,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        addType: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/savetype",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        getsubcat: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/getsubcat",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        gettypes: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/gettypes",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        getReport: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/getReport/" + id,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        loadtagssize: function(callback) {
            $http({
                url: Config.ApiURL + "/product/loadtagssize",
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        loadsize: function(num, off, keyword, callback){
            $http({
                url: Config.ApiURL + "/product/loadsize/" + num + "/" + off + "/" + keyword,
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        deleteSize: function(id, callback) {
            $http({
                url: Config.ApiURL + "/product/deletesize/" + id,
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        updatesize: function(data, callback) {
            $http({
                url: Config.ApiURL + "/product/updatesize",
                method: "post",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        },
        getproductreport: function(callback) {
            $http({
                url: Config.ApiURL + "/product/getproductreport",
                method: "get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
								callback({err:"Can't communicate to API properly"});
						});
        }
	};
});
