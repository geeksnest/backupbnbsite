app.factory('notificationFactory', function($http, $q, Config){
    return {
         listnotification: function(userid, offset, filter,callback){
            $http({
                url: Config.ApiURL + "/be/notification/listnotification/" + userid + "/" + offset + "/" + filter,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         changestatus: function(notificationid, userid, callback){
            $http({
                url: Config.ApiURL + "/be/notification/changestatus/" + notificationid + "/" + userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         notificationcount: function(userid, callback){
            $http({
                url: Config.ApiURL + "/be/notification/notificationcount/" + userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         },
         notificationcountzero: function(userid, callback){
            $http({
                url: Config.ApiURL + "/be/notification/notificationcountzero/" + userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                callback(data);
            }).error(function () {
                callback({err:"Can't communicate to API properly"});
            });
         }
    };
});
