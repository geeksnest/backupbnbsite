app.factory('Pagebanner', function($http, $q, Config){
  	return {
    	data: {},
	    loadimages: function(page, callback) {
	      $http({
	        url: Config.ApiURL+"/page/loadimages/"+page,
	        method: "GET",
	      	headers: {'Content-Type':'application/x-www-form-urlencoded'}
	      }).success(function(data) {
	        callback(data);
	      }).error(function () {
            callback({err:"Can't communicate to API properly"});
        });
	    }
    };
});
