app.factory('Createnews', function($http, $q, Config){
  	return {
  		data: {},
    	loadcategory: function(callback){
      	 	$http({
      	 		url: Config.ApiURL + "/news/listcategory",
      	 		method: "GET",
      	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      	 	}).success(function (data) {
      	 		data = data;
      	 		callback(data);
      	 	}).error(function () {
              callback({err:"Can't communicate to API properly"});
          });
    	 },
       loadcenter: function(callback){
          $http({
              url: Config.ApiURL + "/news/listcenter",
              method: "GET",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function (data) {
              data = data;
              callback(data);
          }).error(function () {
              callback({err:"Can't communicate to API properly"});
          });
       },
       workshoptitles: function(callback) {
          $http({
              url: Config.ApiURL + "/createnews/workshoptitles",
              method: "GET",
          }).success(function (data) {
              data = data;
              callback(data);
          }).error(function () {
              callback({err:"Can't communicate to API properly"});
          });
       },
       newmainnews: function(news, callback) {
         $http({
             url: Config.ApiURL + "/news/create",
             method: "POST",
             headers: {'Content-Type': 'application/x-www-form-urlencoded'},
             data: $.param(news)
         }).success(function (data, status, headers, config) {
             callback(data);
         }).error(function () {
             callback({err:"Can't communicate to API properly"});
         });
       },
       newcenternews: function(centernews, callback) {
         $http({
             url: Config.ApiURL + "/newscenter/create",
             method: "POST",
             headers: {'Content-Type': 'application/x-www-form-urlencoded'},
             data: $.param(centernews)
         }).success(function (data, status, headers, config) {
             callback(data);
         }).error(function () {
             callback({err:"Can't communicate to API properly"});
         });
       }
    };
});
