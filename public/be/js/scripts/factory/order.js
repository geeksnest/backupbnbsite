app.factory('Order', function($http, $q, Config){
  	return {
    	getproducts: function(callback) {
			$http({
				url: Config.ApiURL + "/order/getproducts",
				method: "get",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
			}).success(function (data){
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		getaccountinfo: function(email, callback) {
			$http({
				url: Config.ApiURL + "/order/getaccountinfo/" + email,
				method: "get",
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
			}).success(function (data) {
				callback(data);
			}).error(function () {
					callback({err:"Can't communicate to API properly"});
			});
		},
		month: function () {
            return [
                {name: 'January', val: 1},
                {name: 'February', val: 2},
                {name: 'March', val: 3},
                {name: 'April', val: 4},
                {name: 'May', val: 5},
                {name: 'June', val: 6},
                {name: 'July', val: 7},
                {name: 'August', val: 8},
                {name: 'September', val: 9},
                {name: 'October', val: 10},
                {name: 'November', val: 11},
                {name: 'December', val: 12}
            ];
        },
        day: function () {
            var day = [];
            for (var x = 1; x <= 31; x++) {
                day.push({'val': x});
            }
            return day;
        },
        year: function () {
            var year = [];
            for (var y = 2015; y <= 2050; y++) {
                year.push({'val': y});
            }
            return year;
        },
        addoffline: function(data, centerid, callback) {
        	$http({
        		url: Config.ApiURL + "/order/offline/add/" + centerid,
        		method: "POST",
        		headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
        		data: $.param(data)
        	}).success(function(data) {
        		callback(data);
        	}).error(function () {
    					callback({err:"Can't communicate to API properly"});
    			});
        },
        loadlist: function(num, off, keyword, filter, callback) {
        	$http({
        		url: Config.ApiURL + "/order/list/" + num + "/" + off + "/" + keyword + "/" + filter,
        		method: "GET",
        		headers: {'Content-type' : 'application/x-www-form-urlencoded'}
        	}).success(function(data) {
        		callback(data);
        	}).error(function () {
    					callback({err:"Can't communicate to API properly"});
    			});
        },
        getorder: function(id, callback){
        	$http({
        		url: Config.ApiURL + "/order/get/" + id,
        		method: "GET",
        		headers: {'Content-type' : 'application/x-www-form-urlencoded'}
        	}).success(function(data) {
        		callback(data);
        	}).error(function () {
    					callback({err:"Can't communicate to API properly"});
    			});
        },
        comment: function(data, callback){
        	$http({
        		url: Config.ApiURL + "/order/comment/add",
        		method: "POST",
        		headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
        		data: $.param(data)
        	}).success(function(data) {
        		callback(data);
        	}).error(function () {
    					callback({err:"Can't communicate to API properly"});
    			});
        },
        updateStatus: function(data, callback) {
        	$http({
        		url: Config.ApiURL + "/order/updatestatus",
        		method: "POST",
        		headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
        		data: $.param(data)
        	}).success(function(data) {
        		callback(data);
        	}).error(function () {
    					callback({err:"Can't communicate to API properly"});
    			});
        },
        getorderreport: function(callback) {
            $http({
                url: Config.ApiURL + "/order/getreport",
                method: "GET",
                headers: {'Content-type' : 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, Config) {
                callback(data);
            });
        }
    };
});

// getproducts: function(callback) {
// 	var related = { 'newsid':newsid, 'title':title };
// 	$http({
// 		url: Config.ApiURL + "/mainnews/addrelated",
// 		method: "POST",
// 		headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
// 		data: $.param(related)
// 	}).success(function (data, status, headers, config){
// 		callback(data);
// 	})
// }
