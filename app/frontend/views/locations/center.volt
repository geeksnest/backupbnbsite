<div class="container-fluid" ng-controller="centerCtrl">
  <div class="row"> <!-- center title and address row -->
    <div class="col-md-8 col-sm-8 centertitlepadding">
      <div class="centerbreadcrumbs">Location > {{center.centerstate}} </div>
      <div class="centertitle">{{center.centertitle}}</div>

      {% if sociallinks|length > 0 %}
      <div class="ctr-scl-icon">
        {% for lnk in sociallinks %}

          {% if lnk.title == "facebook" %}
            <a href="{{lnk.linkurl}}" target="_blank" class="pad-sm-r"><img src="/img/fb.png" class="social-btn-o si-fb"/></a>
          {% elseif lnk.title == "google" %}
            <a href="{{lnk.linkurl}}" target="_blank" class="pad-sm-r"><img src="/img/g+.png" class="social-btn-o si-go"/></a>
          {% elseif lnk.title == "twitter" %}
            <a href="{{lnk.linkurl}}" target="_blank" class="pad-sm-r"><img src="/img/twitter.png" class="story-social si-tw"/></a>
          {% elseif lnk.title == "yelp" %}
            <a href="{{lnk.linkurl}}" target="_blank" class="pad-sm-r"><img src="/img/frontend/sns_yepl.gif" class="story-social si-ye"/></i></a>
          {% endif %}

        {% endfor %}
      </div>
      {% endif %}

    </div>
    <div class="col-md-4 col-sm-4 centeraddresspadding centeraddressnormal">
        <div class="centeraddresscontent">
          <div class="centeraddresstext">
            <a href="https://www.google.com.ph/maps/dir//{[{centerlat}]},{[{centerlon}]}" target="_blank"><i class="fa fa-level-up"></i> {{center.centeraddress}}</a>
          </div>
          <div class="centeraddresstext">{{center.centercity}}, {{center.centerstate}} {{center.centerzip}} </div>
          <div class="centeraddresstext">
            <a href="tel:{{center.phonenumber}}"><i class="fa fa-phone"></i> {{center.phonenumber}}</a>
          </div>
          <div class="centeraddresstext">{{center.email}}</div>
        </div>
    </div>
  </div> <!-- end of center title and address row -->

  <!-- Swiper -->
  <a class="no-hover" href="/getstarted/starterspackage/{[{ slug }]}">
    <div class="row center-sec hidden">
        Click here to try a class!
    </div>
  </a>

  <div class="row centermainslider center-swiper">
    <div class="swiper-center">
      <div class="swiper-wrapper">
       {% for img in centerimages %}
        <div class="swiper-slide"><img src="{{centerimg}}/{{img.imagename}}" style="width: auto; height: 434px; top: 0px;left: -93.5652px;" /></div>
       {% endfor %}
      </div>
      <!-- Add Pagination -->
      <div class="swiper-pagination" style="position:relative; bottom:30px;"></div>
    </div>
  </div>

  <div class="row centeraddressmobilecontent" style="display:none;">
    <div class="col-md-6 col-sm-6 centeraddressmobile">
        <div class="centeraddresscontent">
          <div class="centeraddresstext">
            <a href="https://www.google.com.ph/maps/dir//{[{centerlat}]},{[{centerlon}]}" target="_blank"><i class="fa fa-level-up"></i> {{center.centeraddress}}</a>
          </div>
          <div class="centeraddresstext">{{center.centercity}}, {{center.centerstate}} {{center.centerzip}} </div>
          <div class="centeraddresstext">
            <a href="tel:{{center.phonenumber}}"><i class="fa fa-phone"></i> {{center.phonenumber}}</a>
          </div>
          <div class="centeraddresstext">{{center.email}}</div>
        </div>
    </div>
  </div>

  <div class="row schedulenormal" vertilize-container ng-cloak>
    {#<div class="col-sm-6 col-md-3 item schedulelist" vertilize>
      <center><p>Select a date</p></center>
        <datepicker id="centercalendar"
            ng-model="dt"
            min-date="minDate"
            show-weeks="false"
            class="datepicker"
            ng-click="classselecteddate(dt)">
        </datepicker>

        <a href="#" ng-click="viewmonthsched()" class="b-link" style="width:100%; display:block; text-align:center;">View full months Schedule ></a>

      <h1>SCHEDULE</h1>
    </div>#}

    <div class="col-sm-6 col-md-3 item schedulelist" ng-repeat="list in schedulelist">
      <div ng-if="$index == 0" vertilize>
        <center><p>Select a date</p></center>
          <datepicker id="centercalendar"
              ng-model="dt"
              min-date="minDate"
              show-weeks="false"
              class="datepicker"
              ng-click="classselecteddate(dt)">
          </datepicker>

          <a href="#" ng-click="viewmonthsched()" class="b-link" style="width:100%; display:block; text-align:center;">View full months Schedule ></a>

        <h1>SCHEDULE</h1>
      </div>

      <div ng-if="$index != 0" vertilize>
        <div class="daydate">{[{days[$index-1] | date:'EEE MM/dd'}]}</div>
        <div class="joindirection">Select a class below to join</div>
        <div ng-repeat="slist in list" style="overflow:hidden;">
          <table style="width:100%;">
            <tr>
              <td> <div class="classtitle">{[{slist.etitle}]}</div> </td>
              <td rowspan="2" ng-if="slist.type=='class'"> <a href="/getstarted/starterspackage/{[{ slist.ecenterslugs }]}/{[{slist.edate}]}/{[{slist.estarthour + ' ' + slist.estarttimeformat }]}/{[{$index}]}/{[{slist.etitle}]}" class="btn btn-sm btn-orange pull-right">join</a> </td>
            </tr>
            <tr>
              <td> <div class="classtime">{[{slist.estarthour + ' ' + slist.estarttimeformat}]} - {[{slist.eendhour + ' ' + slist.eendtimeformat}]}</div> </td>
            </tr>
            <tr>
              <td style="padding-bottom: 10px;"> <a href="#" class="b-link" ng-click="viewmoresched(slist.eid, slist.edate, slist.estarthour + ' ' + slist.estarttimeformat, $index)"> <i class="fa fa-chevron-right"></i> view more</a> </td>
            </tr>
          </table>
          {#<div class="classtitle">{[{slist.classandday}]}</div>
          <div class="classtime">{[{slist.starttime}]} - {[{slist.endtime}]}</div>#}
          <!-- <div class="classseparator"></div> -->
        </div>
      </div>
    </div>
  </div>

  <div class="row schedulemobile ng-cloak" style="display:none;">
    <div class="schedulemobilecontainer">
      <div class="SCHEDULE" style="text-align:left!important">
        {#<section>SCHEDULE</section>#}
        SCHEDULE
      </div>
      <div class="col-sm-12 calendarinlinecenter">
         <center><datepicker ng-init="classselecteddate(dt)" ng-model="dt" min-date="minDate" show-weeks="false" class="datepicker" ng-click="classselecteddate(dt)"></datepicker></center>
      </div>
      <div class="col-sm-12 schedulelist-mv" ng-repeat="list in schedulelist | limitTo:2">
        <br>
        <div ng-if="$index > 0">
          <div class="daydate ng-cloak">{[{days[$index-1] | date:'EEE MM/dd'}]}</div>
          <div class="joindirection ng-cloak"><label class="btn btn-orange">Select a class below to join</label></div>

          <div ng-repeat="slist in list">
            <a  ng-if="slist.type=='class'" href="/getstarted/starterspackage/{[{ slist.ecenterslugs }]}/{[{slist.edate}]}/{[{slist.estarthour + ' ' + slist.estarttimeformat }]}/{[{$index}]}/{[{slist.etitle}]}" class="b-link ng-cloak">
            <div class="classtitle ng-cloak">{[{slist.etitle}]}</div>
            <div class="classtime ng-cloak">{[{slist.estarthour + ' ' + slist.estarttimeformat}]} - {[{slist.eendhour + ' ' + slist.eendtimeformat}]}</div>
            </a>
            <div  ng-if="slist.type!='class'">
              <div class="classtitle ng-cloak">{[{slist.etitle}]}</div>
              <div class="classtime ng-cloak">{[{slist.estarthour + ' ' + slist.estarttimeformat}]} - {[{slist.eendhour + ' ' + slist.eendtimeformat}]}</div>
            </div>
            <a href="#" class="b-link" ng-click="viewmoresched(slist.eid, slist.edate, slist.estarthour + ' ' + slist.estarttimeformat, $index)"> <i class="fa fa-chevron-right"></i> view more</a>
            <div class="classseparator"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- comment -->
  {#<div class="row centersectionheader">
    <div class="col-lg-12">
      Get Started and Join Today<span class="viewallevents">|&nbsp; &nbsp;10% off all memberships </span>
    </div>
  </div>
  <div class="row centermembership" vertilize-container ng-cloak>
    <div class="col-sm-6 col-md-3 item membershiplist text-left" vertilize>
      <div class="classtitle">Try it First!</div>
      <div class="classdesc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</div>
      <button class="btn btn-orange">intro session</button>
      <h1>MEMBERSHIP</h1>
    </div>

    <?php foreach($centermembership as $key=>$value) {
        if($key <= 4) {
      ?>
      <div class="col-sm-6 col-md-2 item membershiplist" vertilize>
        <div class="classtitle"><?php
          if($value->period == 1){
            echo "1 Month";
          } else if($value->period == 3) {
            echo "3 Months";
          } else if($value->period == 6) {
            echo "6 Months";
          } else if($value->period == 12) {
            echo "1 Year";
          } else if($value->period == 24) {
            echo "2 Years";
          } else if($value->period == 36) {
            echo "3 Year";
          } else if($value->period == 110) {
            echo "10 Classes";
          } else if($value->period == 120) {
            echo "20 Classes";
          }
        ?></div>
        <div class="classdesc"><?php echo "$".$value->price . "<br>" .$value->description; ?></div>
        <button class="btn btn-orange">join</button>
      </div>
    <?php }
    } ?>
  </div>#}
  <!-- comment -->

  {#<div class="row centersectionheader">
    <div class="col-lg-12">
      Events Calendar <span class="viewallevents" ng-click="viewfullevents()"> view full events &#10095; </span>
    </div>
  </div>
  <div class="row centersectioncontainer">
    <?php foreach($centerevents as $events) {
        $date = date_create($events->edate); ?>
      <div class="col-sm-6 centerevents" style="padding: 30px 17px;">
        <div class="row">
          <div class="col-sm-6 col-md-3">
            <h4><b><?php echo date_format($date, "l"); ?></b></h4>
            <span class="day"><?php echo date_format($date, "m/d"); ?></span>
            <span class="mv">|</span>
            <span class="time">
              <?php echo $events->estarthour . " " . $events->estarttimeformat . " - " . $events->eendhour . " " . $events->eendtimeformat;  ?>
            </span>
          </div>
          <div class="col-sm-6 col-md-9">
            <h4><b><?php echo $events->etitle;  ?></b></h4>
            <?php echo $events->edesc;  ?>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>#}

  <div class="row centersectionheader">
    <div class="col-lg-12">
      Location News <span class="viewallnews" ng-click="viewfullnews()"> view full news &#10095; </span>
      {% if centernews|length > 2 %}
        <span class="swiperbtn">
          <button class="prev-btn-news"><i class="fa fa-caret-left"></i></button>
          <button class="next-btn-news"><i class="fa fa-caret-right"></i></button>
        </span>
      {% endif %}
    </div>
  </div>

  <div class="row centersectioncontainer">
    {% if centernews|length > 2 %}
      <div class="swiper-centernews hiddenoverflow">
        <div class="swiper-wrapper">
          {% for news in centernews %}
          <div class="swiper-slide text-left item">
            <a class="b-link" href="/{{center.centerslugs}}/newspost/{{ news.newsslugs }}">
              <div class="row centernews">
                <?php
                  if($news->banner) {
                    $photo = $amazonlink . "/uploads/newsimage/" . $news->banner;
                  } else {
                    $photo = "/img/no-banner.png";
                  }
                ?>
                <div class="item img bg-image pull-left" style="background-image: url('<?php echo $photo; ?>'); width: 225px; height: 136px;">
                </div>
                <div class="newstitle">{{news.title}}</div>
                <div class="newscontent">
                  {{news.description}}
                </div>
              </div>
            </a>
          </div>
          {% endfor %}
        </div>
      </div>
    {% else %}
        {% for news in centernews %}
        <div class="col-md-6 col-sm-6">
          <a class="b-link" href="/{{center.centerslugs}}/newspost/{{ news.newsslugs }}">
            <div class="row centernews">
              <div class="item img">
                <img src="{{centernewsimg}}/{{news.banner}}" width="100%" err-src="/img/no-banner.png" />
              </div>
              <div class="newstitle">{{news.title}}</div>
              <div class="newscontent">
                {{news.description}}
              </div>
            </div>
          </a>
        </div>
        {% endfor %}
    {% endif %}
  </div>

  <div class="row centersectionheader">
    <div class="col-lg-12">
      Testimonials <span class="viewall" ng-click="viewfulltestimonials()"> view all center testimonials &#10095; </span>
      <span class="swiperbtn">
        <button class="prev-btn"><i class="fa fa-caret-left"></i></button>
        <button class="next-btn"><i class="fa fa-caret-right"></i></button>
      </span>
    </div>
  </div>
  <div class="row centertesti">
    <div class="swiper-centertestimonial">
      <div class="swiper-wrapper">
        {% for story in centertestimonials %}
          <?php
            if($story->photo) {
              $photo = $amazonlink . "/uploads/testimonialimages/" . $story->photo;
            } else {
              $photo = "/img/unknownProfile.png";
            }
          ?>
          <div class="swiper-slide text-left item"  style="width: 437px; overflow: hidden; position: relative; top: 0px; margin-right: 30px;">
            <a class="story-list-a" href="/{{stories}}/details/{{story.ssid}}/{{story.subject}}">
              <div class="img-ttml bg-image" style="background-image: url('<?php echo $photo; ?>')">
              </div>
              <div class="col-ttml-content">
                <div class="story-list-subject">{{story.subject}}</div>
                <div class="story-list-author">{{story.author}}</div>
                <div class="p-ttml">"{{story.metadesc}}"</div>
              </div>
            </a>
          </div>
        {% endfor %}
      </div>
    </div>
  </div>

  {#<div class="row centersectionheader">
    <div class="col-lg-12">
      Location News <span class="viewallnews" ng-click="viewfullnews()"> view full news &#10095; </span>
    </div>
  </div>

  <div class="row centersectioncontainer">
    {% for news in centernews %}
    <div class="col-md-6 col-sm-6">
      <a class="b-link" href="/{{center.centerslugs}}/newspost/{{ news.newsslugs }}">
        <div class="row centernews">
          <div class="item img">
            <img src="{{centernewsimg}}/{{news.banner}}" width="100%" err-src="/img/no-banner.png" />
          </div>
          <div class="newstitle">{{news.title}}</div>
          <div class="newscontent">
            {{news.description}}
          </div>
        </div>
      </a>
    </div>
    {% endfor %}
  </div>#}

  <div class="row centermap">
    <div id="map" style="position:relative;">
      <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options" bounds="map.bounds">
        <ui-gmap-markers models="randomMarkers" coords="'self'"  icon="'icon'">
          <ui-gmap-windows show="show">
            <div ng-non-bindable ng-cloak>{[{title}]}</div>
          </ui-gmap-windows>
        </ui-gmap-markers>
      </ui-gmap-google-map>
    </div>
  </div>
</div> {#controller#}

<script type="text/ng-template" id="viewfullevents">
  <div ng-include="'/fe/tpl/viewfullevents.html'"></div>
</script>
<script type="text/ng-template" id="viewfullnews">
  <div ng-include="'/fe/tpl/viewfullnews.html'"></div>
</script>
<script type="text/ng-template" id="viewfulltestimonials">
  <div ng-include="'/fe/tpl/viewfulltestimonials.html'"></div>
</script>
<script type="text/ng-template" id="viewsched">
  <div ng-include="'/fe/tpl/viewsched.html'"></div>
</script>
