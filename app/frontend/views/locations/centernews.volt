<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<div class="pagebanner">
  <img src="{{amazonlink}}/uploads/newsimage/{{centernews.banner}}" width="100%" err-src="/img/no-banner.png" />
</div>

<div class="container-fluid wrapper-lg-vw">
  <div class="col-sm-12">
    <div class="orange font-sm"><b>Location News | {{centernews.date}}</b></div>
    <div class="pg-mini-title">{{centernews.title}}</div>
    <div class="pg-p-details body-content">
      {{centernews.body}}
    </div>

    <div>
      <a href="/{{centerslugs}}" class="back-press"> &lt; Back to {{centertitle}}</a>
    </div>

    <br><br><br>

    <div class="sc-btn">
      <a href="http://www.facebook.com/sharer/sharer.php?u={{BaseURL}}/{{centerslugs}}/newspost/{{centernewsslugs}}&title={{centernews.title}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/fb.png" class="story-social si-fb"/></a>
      <a href="https://plus.google.com/share?url={{BaseURL}}/{{centerslugs}}/newspost/{{centernewsslugs}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/g+.png" class="story-social si-go"/></a>
      <a href="http://twitter.com/intent/tweet?status={{centernews.title}}+{{BaseURL}}/{{centerslugs}}/newspost/{{centernewsslugs}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/twitter.png" class="story-social si-tw"/></a>
      <a href="http://pinterest.com/pin/create/bookmarklet/?media=[MEDIA]&url={{BaseURL}}/{{centerslugs}}/newspost/{{centernewsslugs}}&is_video=false&description={{centernews.title}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/pint.png" class="story-social si-pi"/></a>
    </div>

    <br><br>

    <div class="fb-comments" data-href="http://bodynbrain.com/{{centerslugs}}/newspost/{{centernewsslugs}}" data-numposts="10" width="100%"></div>
  </div>
</div>
