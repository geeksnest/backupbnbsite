<div class="container-fluid" ng-controller="centerCtrl">
<div class="row"> <!-- center title and address row -->
    <div class="col-md-6 col-sm-6 centertitlepadding">
      <p class="centerbreadcrumbs">Location > {{center.centerstate}} </p>
      <p class="centertitle">{{center.centertitle}}</p>
    </div>
    <div class="col-md-6 col-sm-6 centeraddresspadding centeraddressnormal">
        <div class="centeraddresscontent">
          <div class="centeraddresstext">{{center.centeraddress}}</div>
          <div class="centeraddresstext">{{center.centertitle}}, {{center.centerstate}} {{center.centerzip}} </div>
          <div class="centeraddresstext">{{center.phonenumber}}</div>
          <div class="centeraddresstext">{{center.email}}</div>
        </div>
    </div>
  </div> <!-- end of center title and address row -->

  <!-- slider row -->
  <div class="row centermainslider">

    <section id="slider_wrapper">
      <div id="slider" class="divas-slider">
        <ul class="divas-slide-container custom-ul">
        {% for img in centerimages %}
          <li class="divas-slide"><img src="/img/placeholder.gif" alt="" data-src="{{centerimg}}/{{img.imagename}}" data-title="" /></li>
        {% endfor %}
        </ul>
      <div class="divas-navigation">
        <span class="divas-prev">&nbsp;</span>
        <span class="divas-next">&nbsp;</span>
      </div>
      <div class="divas-controls">
        <span class="divas-start"><i class="fa fa-play"></i></span>
        <span class="divas-stop"><i class="fa fa-pause"></i></span>
      </div>
      </div>
    </section>

  </div>
  <!-- end of slider row -->

  <!-- Swiper -->
  <div class="row centermainslider">
    <div class="swiper-container">
      <div class="swiper-wrapper">
       {% for img in centerimages %}
       <div class="swiper-slide"><img src="{{centerimg}}/{{img.imagename}}" width="100%" /></div>
       {% endfor %}
      </div>
      <!-- Add Pagination -->
      <div class="swiper-pagination"></div>
    </div>
  </div>

  <div class="row centermobileslider" style="display:none">
    <div class="swiper-containermobile">
      <div class="swiper-wrapper">
       {% for img in centerimages %}
       <div class="swiper-slide"><img class="my-image" src="{{centerimg}}/{{img.imagename}}" width="100%" /></div>
       {% endfor %}
      </div>
    </div>
  </div>

  <!-- slider row -->
<!--   <div class="row centermobileslider" style="display:none">
    <section id="slider_wrapper">
      <div id="slider1" class="divas-slider">
        <ul class="divas-slide-container custom-ul">
          {% for img in centerimages %}
            <li class="divas-slide">
              <img src="/img/placeholder.gif" alt=""
                    data-src="{{centerimg}}/{{img.imagename}}"
                    data-title="" />
            </li>
          {% endfor %}
        </ul>
      <div class="divas-navigation">
        <span class="divas-prev">&nbsp;</span>
        <span class="divas-next">&nbsp;</span>
      </div>
      <div class="divas-controls">
        <span class="divas-start"><i class="fa fa-play"></i></span>
        <span class="divas-stop"><i class="fa fa-pause"></i></span>
      </div>
      </div>
    </section>

  </div>  -->
  <!-- end of slider row -->

  <div class="row centeraddressmobilecontent" style="display:none;">
    <div class="col-md-6 col-sm-6 centeraddressmobile">
        <div class="centeraddresscontent">
          <div class="centeraddresstext">{{center.centeraddress}}</div>
          <div class="centeraddresstext">{{center.centertitle}}, {{center.centerstate}} {{center.centerzip}} </div>
          <div class="centeraddresstext">{{center.phonenumber}}</div>
          <div class="centeraddresstext">{{center.email}}</div>
        </div>
    </div>
  </div>

  <!-- schedule row -->
  <!-- <div class="row schedulenormal">
    <div class="col-sm-6 col-md-3 item">
      <center><datepicker ng-model="dt" min-date="minDate" show-weeks="false" class="datepicker" ng-click="classselecteddate(dt)" ng-change="classdetailsselecteddatechange(dt)"></datepicker></center>
    </div>
    {% for schedule in centerschedule %}
    <div class="col-sm-6 col-md-3 item">
      <div class="daydate">{{schedule.date}}</div>

      {% for agenda in schedule.agenda %}
      <div class="classtitle">{{agenda.class}}</div>
      <div class="classtime">{{agenda.starttime}} - {{agenda.endtime}}</div>
      <div class="classseparator"></div>
      {% endfor %}

    </div>
    {% endfor %}

  </div> -->
  <!-- end of schedule row -->

  <div class="row schedulenormal" vertilize-container>

    <div class="col-sm-6 col-md-3 item" vertilize>
      <center>
        <p>Select a date</p>
        <datepicker ng-init="classselecteddate(dt)" ng-model="dt" min-date="minDate" show-weeks="false" class="datepicker" ng-click="classselecteddate(dt)"></datepicker>
        <a href="#" ng-click="viewmonthsched()"><p style="margin-top:10px; margin-left: 20px; text-align:left;">View full months Schedule ></p></a>
      </center>
      <h1>SCHEDULE</h1>
    </div>

    <div class="col-sm-6 col-md-3 item" ng-repeat="list in schedulelist" style="overflow:hidden;min-height:331.85px;" vertilize>
      <div class="daydate">{[{days[$index] | date:'EEE MM/dd'}]}</div>
      <div ng-repeat="slist in list" style="overflow:hidden;">
        <div class="classtitle">{[{slist.classandday}]}</div>
        <div class="classtime">{[{slist.starttime}]} - {[{slist.endtime}]}</div>
        <!-- <div class="classseparator"></div> -->
      </div>
    </div>
  </div>

  <div class="row schedulemobile" style="display:none;">
    <div class="schedulemobilecontainer">
      <div class="col-sm-12">
        <section class="schedule">SCHEDULE</section>
      </div>
      <div class="col-sm-12 calendarinlinecenter">
         <center><datepicker ng-init="classselecteddate(dt)" ng-model="dt" min-date="minDate" show-weeks="false" class="datepicker" ng-click="classselecteddate(dt)"></datepicker></center>
      </div>
      <div class="col-sm-12" ng-repeat="list in schedulelist | limitTo:1">
        <div class="daydate">{[{days[$index] | date:'EEE MM/dd'}]}</div>

        <div ng-repeat="slist in list">
          <div class="classtitle">{[{slist.classandday}]}</div>
          <div class="classtime">{[{slist.starttime}]} - {[{slist.endtime}]}</div>
          <div class="classseparator"></div>
        </div>

      </div>
    </div>
  </div>

  <div class="row centersectionheader">
    <div class="col-lg-12">
      Events Calendar <span class="viewallevents" ng-click="viewfullevents()"> &#10095; view full events</span>
    </div>
  </div>
  <div class="row centersectioncontainer">
    <?php foreach($centerevents as $events) {
        $date = date_create($events->edate); ?>
      <div class="col-sm-6 centerevents" style="padding: 30px 17px;">
        <div class="row">
          <div class="col-sm-6 col-md-3">
            <h4><b><?php echo date_format($date, "l"); ?></b></h4>
            <span class="day"><?php echo date_format($date, "m/d"); ?></span>
            <span class="mv">|</span>
            <span class="time">
              <?php echo $events->estarthour . " " . $events->estarttimeformat . " - " . $events->eendhour . " " . $events->eendtimeformat;  ?>
            </span>
          </div>
          <div class="col-sm-6 col-md-9">
            <h4><b><?php echo $events->etitle;  ?></b></h4>
            <?php echo $events->edesc;  ?>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>

  <div class="row centersectionheader">
    <div class="col-lg-12">
      Location News
    </div>
  </div>

  <div class="row centersectioncontainer">
    {% for news in centernews %}
    <div class="col-md-6 col-sm-6">
      <a href="/{{ news.newsslugs }}">
        <div class="row centernews">
          <div class="item img">
            <img src="{{centernewsimg}}/{{news.banner}}" width="100%">
          </div>
          <div class="newstitle item">{{news.title}}</div>
          <div class="col-xs-12 newscontent">
            {{news.description}}
          </div>
        </div>
      </a>
    </div>
    {% endfor %}
  </div>

  <div class="row centermap" style="height:500px;">
    <div id="map" style="position:relative;">
      <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options" bounds="map.bounds">
        <ui-gmap-markers models="randomMarkers" coords="'self'"  icon="'icon'">
          <ui-gmap-windows show="show">
            <div ng-non-bindable>{[{title}]}</div>
          </ui-gmap-windows>
        </ui-gmap-markers>
      </ui-gmap-google-map>
    </div>
  </div>
</div> {#controller#}

<script type="text/ng-template" id="viewfullevents">
  <div ng-include="'/fe/tpl/viewfullevents.html'"></div>
</script>
