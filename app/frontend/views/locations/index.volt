<style>
html, body {
  padding:0; margin:0;
}
.container-fluid {
  padding:0;
}
#mainfooter {
  position:fixed;
  bottom:-1px;
  width:100%;
}
#locationbanner {
  position:fixed;
  left:0;
  /*top: 132px;*/
  width:100%!important;
  height:100%;
  z-index:0;
  margin-bottom:-132px!important;
  box-sizing: border-box;
}
.locationfooter-res {
  position:relative!important;
}
@media screen and (max-width:850px) {
  #locationbanner {
    position:relative!important;
    top:0!important;
    height:50vw!important;
  }
  #mainfooter {
    position:relative;
  }
}
@media screen and (max-width:767px) {
  #locationbanner {
    width:100%;
    height:400px!important;
  }
}
</style>
<div ng-controller="locindexCtrl">

<div class="row locationsecondarynav">
  <a href="#" ng-click="select()">
    <div class="loc-select" ng-cloak>
      {[{ location.state }]}
    </div>
  </a>
</div>

<div class="selectoptions" ng-show="isOpen">
  <div class="row">
    <div class="col" ng-repeat="state in states">
      <div class="selectrow" ng-repeat="s in state">
        <a href="#" ng-click="selectstate(s)" ng-cloak>
          {[{ s.state }]}
        </a>
      </div>
    </div>
  </div>
</div>
<div class="selectoptions mobile" ng-show="isOpen">
  <div class="row">
    <div class="col-xs-12">
      <a href="#" ng-click="selectstate(s)" ng-repeat="s in statesmobile  | slice:offset:limit" ng-cloak>
        <div class="states">
            {[{ s.state }]}
        </div>
      </a>
      <div class="scroller">
        <button class="down bg-image" ng-click="scroll('down')" ng-if="statesmobile.length >= limit"></button>
        <button class="up bg-image" ng-click="scroll('up')" ng-if="offset > 0"></button>
      </div>
    </div>
    <!-- <div class="col-xs-2">
      <button class="down bg-image" ng-click="scroll('down')" ng-if="regionsmobile.length >= (limit - 8)"></button>
      <button class="up bg-image" ng-click="scroll('up')" ng-if="offset > 0"></button>
    </div> -->
  </div>
</div>

<div id="locationbanner" ng-hide="centertoggle">

  {% if nobanner == false %}
    {% if slider == true %}

      <div class="swiper-banner">
        <div class="swiper-wrapper">
          {% for banner in pagebanners %}
            <div class="swiper-slide">
              {% if banner.url == null OR banner.url == "" %}
                <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" />
              {% else %}
                <a href="{{banner.url}}" target="_blank">
                  <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" />
                </a>
              {% endif %}
            </div>
          {% endfor %}
        </div>
        <div class="swiper-pagination"></div>
      </div>

    {% else %}

        {% if pagebanners[0].url == null OR pagebanners[0].url == "" %}
          <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
        {% else %}
          <a href="{{pagebanners[0].url}}" target="_blank">
            <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
          </a>
        {% endif %}

    {% endif %}
  {% else %}
    <img src="/img/banners/locations.jpg" style="margin:0; padding:0;">
  {% endif %}

</div>

<div ng-show="centertoggle">
  <div class="row centermap" style="margin:0;">
    <div id="map" style="position:relative; margin:0;">
      <!-- <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options" bounds="map.bounds"> -->
        <!-- <ui-gmap-markers models="randomMarkers" coords="'self'"  icon="'icon'" fit="'true'">
          <ui-gmap-windows show="show">
            <div ng-cloak>{[{ self }]}</div>
          </ui-gmap-windows>
        </ui-gmap-markers> -->
       <!--  <ui-gmap-marker ng-repeat="m in randomMarkers" coords="{ latitude : m.latitude, longitude : m.longitude }" options="{ icon: 'img/pin.png' }" idkey="m.id" events="m.events">
          <ui-gmap-window show="m.show">
            <div ng-cloak>{[{ m.id }]}</div>
          </ui-gmap-window>
        </ui-gmap-marker>
      </ui-gmap-google-map> -->
      <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options" bounds="map.bounds">
          <ui-gmap-markers models="randomMarkers" coords="'self'"  icon="'icon'">
            <ui-gmap-windows show="show">
              <div ng-non-bindable>{[{title}]}</div>
            </ui-gmap-windows>
          </ui-gmap-markers>
        </ui-gmap-google-map>
    </div>
  </div>

  <div class="row locationresultcontainer" vertilize-container style="margin:0;">
    <div class="col-sm-4 locationcontainer" vertilize ng-repeat="center in centers">
      <div class="wrapper-md">
        <div class="locationcontainerlocationtype" ng-show="center.centertype == 1"><img src="/img/smalllogo.png"></div>
        <a class="b-link" ng-href="/{[{center.centerslugs}]}"><div class="locationcontainerlocationtitle" ng-bind="center.centertitle"></div></a>
        <div class="locationcontainerlocationaddress">
          <span ng-bind="center.centeraddress"></span> <br>
          <span ng-bind="center.centercity"></span>, <span ng-bind="center.centerstate"></span> <span ng-bind="center.centerzip"></span>
        </div>
        <div class="locationcontainerlocationphone" ng-bind="center.phonenumber"></div>
        <div class="locationcontainergetdirections">
          <a class="b-link" ng-href="https://www.google.com/maps/dir//{[{center.lat}]},{[{center.lon}]}" target="_blank">get directions</a>
        </div>
      </div>
    </div>
  </div>

</div>
</div> {#controller#}
