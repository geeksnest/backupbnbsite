<style>
.vidcontainer {
  height:inherit;
}
.vidcontainer > iframe {
  position:relative;
  height:inherit!important;
  width:100%!important;
}

div.press-sc-btn a { padding-right:30px; }
@media screen and (max-width:350px) {
  div.press-sc-btn a { padding-right:10px; }
}
</style>
<script type="text/javascript">
  var slug = "<?=$slug ?>";
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div ng-controller="ReadCtrl" ng-cloak>
  <div class="pg-banner wrapper-lg" style="background:url('{[{amazonlink}]}/{[{_ddata.banner}]}');" ng-if="_ddata.bannertype=='image' && show == false">
    <div class="col-pad-y" style="position:absolute; bottom:0;">
    </div>
  </div>
  <div class="pg-banner wrapper-lg" style="background: #000; padding: 0px;" ng-if="_ddata.bannertype=='video' && show == false">
    <div ng-bind-html="_ddata.banner" class="vidcontainer"></div>
  </div>
  <div class="pg-banner wrapper-lg" style="background:url('/img/no-banner.png');" ng-if="show">
    <div class="col-pad-y" style="position:absolute; bottom:0;">
    </div>
  </div>
  <div class="wrapper-lg-vw">
    <div style="padding-top: 10px;">

      {# This is the old code -nu77 #}
      {#<div class="presslogo-container" style="border:1px solid red;">
      <img class="press-read-img" src="{[{amazonlink}]}/{[{_ddata.logo}]}">
      </div>
      <div class="press-read-title" style="border:1px solid red;">
      <div class="tryaclasstitle">{[{_ddata.title}]} | {[{_ddata.date}]}</div>
      #}

      {#renewed by nu77#}
      <table style="width:100%;">
        <tr>
          <td class="presslogo-container">
            <img class="press-read-img" src="{[{amazonlink}]}/{[{_ddata.logo}]}" ng-if="_ddata.logo">
            <img class="press-read-img hidden" src="/img/no-banner.png" ng-if="!_ddata.logo">
          </td>
          <td class="press-read-title" style="float:left; padding:25px 0px;">
            <div class="tryaclasstitle">
              <div class="inbl wrbr" ng-bind="_ddata.title"></div>
              <div class="inbl fv">|</div>
              <div class="press-date" ng-bind="_ddata.date"></div>
            </div>
          </td>
        </tr>
      </table>
    </div>
    <div class="press-read-cont">
      <div ng-bind-html="htmlbody" class="wrbr"></div>
      <a href="/press/1" class="back-press"> &lt; Back to Press</a>
      <br>
      <br>
      <br>
      <div class="press-sc-btn">
        <a ng-href="http://www.facebook.com/sharer/sharer.php?u={{BaseURL}}/read-press/{[{_ddata.slugs}]}&title={[{_ddata.title}]}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/fb.png" class="social-btn-o si-fb"/></a>
        <a ng-href="https://plus.google.com/share?url={{BaseURL}}/read-press/{[{_ddata.slugs}]}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/g+.png" class="social-btn-o si-go"/></a>
        <a ng-href="http://twitter.com/intent/tweet?status={[{_ddata.title}]}+{{BaseURL}}/read-press/{[{_ddata.slugs}]}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/twitter.png" class="social-btn-o si-tw"/></a>
        <a ng-href="http://pinterest.com/pin/create/bookmarklet/?media=[MEDIA]&url={{BaseURL}}/read-press/{[{_ddata.slugs}]}&is_video=false&description={[{_ddata.title}]}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/pint.png" class="social-btn-o si-pi"/></a>
      </div>
      <br>

      <div class="fb-comments" data-href="http://bodynbrain.com/read-press/{[{_ddata.slugs}]}" data-numposts="10" width="100%">
      </div>
    </div>
  </div>
</div>
