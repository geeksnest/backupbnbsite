<div ng-controller="PressCtrl">
 <div class="presstitle">
  Body & Brain has been mentioned in the following media outlets.
</div>

<!-- <div class="row press" isotope-container>
  <div class="col-sm-4 item nopadding" ng-repeat="data in data" isotope-item="isotope-item">
      <a href="/read-press/{[{ data.newsslugs }]}">
        <div class="pressbanner zoom" ng-if="data.type == 'image'">
          <div class="bg-div full-width" style="background-image: url('{[{amazonlink}]}/{[{data.banner}]}'); height: 100%!important;"></div>
        </div>
        <div class="pressbanner" ng-if="data.type != 'image'">
          <div ng-bind-html="data.banner" ng-if="data.type == 'video'"></div>
        </div>
        <div class="pressinfo" style="margin:0px 2.84%;">
          <div class="pressinoftitle">
          <div>
            <img src="{[{amazonlink}]}/{[{data.imglogo}]}">
          </div>
          <div style="float:left;width:70%;">
            <h2>{[{data.title}]}</h2>
            <h3>{[{data.date.toString()}]}</h3>
          </div>
        </div>
      </a>
    </div>
</div> -->

<div class="press" isotope-container>
  {% for data in datanewslist %}
    <div class="col-sm-4 item nopadding" isotope-item="isotope-item">
      <a class="a-blue" href="/read-press/{{ data.newsslugs }}">
        <?php if($data->bannertype == 'image') { ?>
        <div class="pressbanner zoom">
          <div class="bg-div full-width" style="background-image: url('{[{amazonlink}]}/{{data.banner}}'); height: 100%!important;"></div>
        </div>
        <?php } else { ?>
        <div class="pressbanner">
          <!-- <div ng-bind-html="data.banner" ng-if="data.type == 'video'"></div> -->
          {{ data.banner }}
        </div>
        <?php } ?>

        <div class="pressinfo" style="margin:0px 2.84%;">
          <div class="pressinoftitle">
            <div>
              <img src="{[{amazonlink}]}/{{data.imglogo}}">
            </div>
            <div style="float:left;width:70%;">
              <h2>{{data.title}}</h2>
              <h3>{{ date('M j, Y', strtotime(data.date)) }}</h3>
            </div>
          </div>
        </div>
      </a>
    </div>
  {% endfor %}
</div>

<!-- <div class="col-sm-4" ng-repeat="data in data">
    <a href="../read-press/{[{data.newsslugs}]}">
      <div class="row pressbanner">
        <img src="{[{amazonlink}]}/{[{data.banner}]}">
      </div>
      <div class="pressinfo">
        <div class="pressinoftitle">

         <img src="{[{amazonlink}]}/{[{data.imglogo}]}" width="150px" height="120px">
         <div style="margin-left:180px">
          <h2>{[{data.title}]}</h2>
          <h3>{[{data.date.toString()}]}</h3>
        </div>
      </div>
    </div>
  </a>
</div>
 -->
</div>


<!-- <div class="row">
  <div class="panel-body">
    <footer class="panel-footer text-center bg-light lter">
      <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
    </footer>
  </div>
</div> -->

<div class="container-fluid">
  <div class="col-sm-12 newspagination">
    <div class="paginationcontainer">
      <?php
      echo '<div class="myPagination">';
      echo '<span first_page_line></span><a href="/press/1" class="first_page"></a>&nbsp';
      $pageLastNumberKey = $totalpage - 4;
      if($page >= $pageLastNumberKey) {
        if($page > 1)
          echo '<a href="/press/'.($page - 1).'" class="back_pagination"></a>&nbsp';
        for($i=max(1, $pageLastNumberKey); $i<=max(1, min($totalpage,$page+4)); $i++)
        {
         if($i == $page)
          echo '<span class="active_page pagination_pages">'.$i.'</span>';
        else
          echo '<a href="/press/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
      }
    }
    else {
      if($page > 1)
        echo '<a href="/press/'.($page - 1).'" class="back_pagination"></a>&nbsp';
      for($i=max(1, $page); $i<=max(1, min($totalpage,$page+4)); $i++)
      {
       if($i == $page)
        echo '<span class="active_page pagination_pages">'.$i.'</span>';
      else
        echo '<a href="/press/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
    }
  }
  if ($page < $totalpage)
    echo '<a href="/press/'.($page + 1).'" class="next_pagination"></a>&nbsp;';
  echo '<a href="/press/'.$totalpage.'" class="last_page"></a><span last_page_line></span>';
  echo"</div>";
         ?>
    </div>
  </div>
</div>


</div>
