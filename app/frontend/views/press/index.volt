<div class="container-fluid" ng-controller="PressCtrl" style="background-color: #FFFFFF;">
  <div class="presstitle" style="margin: 0px -15px;">
    Body & Brain has been mentioned in the following media outlets.
  </div>

  <div class="row press" isotope-container>
    {% for data in datanewslist %}
    <div class="col-sm-4 item nopadding" isotope-item="isotope-item">
      <a href="/read-press/{{ data.newsslugs }}">
        <?php if($data->thumnailtype == 'image') { ?>
          <div class="pressbanner zoom">
            <?php if($data->thumbnail) {?>
              <div class="bg-div full-width" style="background-image: url('{{amazonlink}}/uploads/newsimage/{{data.thumbnail}}'); height: 100%!important;"></div>
            <?php } else { ?>
              <div class="bg-div full-width" style="background-image: url('/img/no-banner.png'); height: 100%!important;"></div>
            <?php } ?>
          </div>
        <?php } else { ?>
          <div class="pressbanner">
            <!-- <div ng-bind-html="data.banner" ng-if="data.type == 'video'"></div> -->
            {{ data.thumbnail }}
          </div>
        <?php } ?>

          <div class="pressinfo" style="margin:0px 2.84%;">
            <div class="pressinoftitle">
              <div>
                <?php if($data->imglogo) {?>
                  <img src="{{amazonlink}}/uploads/newsimage/{{data.imglogo}}">
                <?php } else { ?>
                  <img src="/img/no-banner.png">
                <?php } ?>
              </div>
              <div style="float:left;width:70%; padding-left:15px;">
                <h2>{{data.title}}</h2>
                <h3>{{ date('M j, Y', strtotime(data.date)) }}</h3>
              </div>
            </div>
          </div>
        </a>
      </div>
    {% endfor %}
  </div>

  <div class="container-fluid">
    <div class="col-sm-12 newspagination">
      <div class="paginationcontainer">
        <?php
        echo '<div class="myPagination">';
          echo '<span first_page_line></span><a href="/press/1" class="first_page"></a>&nbsp';
          $pageLastNumberKey = $totalpage - 4;
          if($page >= $pageLastNumberKey) {
            if($page > 1)
            echo '<a href="/press/'.($page - 1).'" class="back_pagination"></a>&nbsp';
            for($i=max(1, $pageLastNumberKey); $i<=max(1, min($totalpage,$page+4)); $i++)
            {
              if($i == $page)
              echo '<span class="active_page pagination_pages">'.$i.'</span>';
              else
              echo '<a href="/press/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
            }
          }
          else {
            if($page > 1)
            echo '<a href="/press/'.($page - 1).'" class="back_pagination"></a>&nbsp';
            for($i=max(1, $page); $i<=max(1, min($totalpage,$page+4)); $i++)
            {
              if($i == $page)
              echo '<span class="active_page pagination_pages">'.$i.'</span>';
              else
              echo '<a href="/press/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
            }
          }
          if ($page < $totalpage)
          echo '<a href="/press/'.($page + 1).'" class="next_pagination"></a>&nbsp;';
          echo '<a href="/press/'.$totalpage.'" class="last_page"></a><span last_page_line></span>';
          echo"</div>";
          ?>
        </div>
      </div>
    </div>
</div>
