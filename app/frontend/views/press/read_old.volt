<script type="text/javascript">
  var slug = "<?=$slug ?>";
</script>
<div ng-controller="ReadCtrl">
  <div class="pg-banner wrapper-lg" style="background:url('{[{amazonlink}]}/{[{_ddata.banner}]}');" ng-if="_ddata.bannertype=='image'">
    <div class="col-pad-y" style="position:absolute; bottom:0;">
    </div>
  </div>
  <div class="pg-banner wrapper-lg" style="background: #000; padding: 0px;" ng-if="_ddata.bannertype=='video'">
    <div ng-bind-html="_ddata.banner"></div>
  </div>
  <div class="container-fluid wrapper-md">
    <div class="col-sm-12 pg-col" style="padding-top: 10px;">
      <div class="presslogo-container">
        <img class="press-read-img" src="{[{amazonlink}]}/{[{_ddata.logo}]}">
      </div>
      <div class="press-read-title ">
        <div class="tryaclasstitle">{[{_ddata.title}]} | {[{_ddata.date}]}</div>
      </div>
      <div style="clear:both"></div>
    </div>
    <div class="col-sm-12 pg-col press-read-cont ">
     <div ng-bind-html="htmlbody"></div>
    <a href="/press/1" class="back-press"> &lt; Back to Press</a>
    <!-- <div id="disqus_thread"></div>
    <script>
      (function() {
        var d = document, s = d.createElement('script');

        s.src = '//bodynbrain.disqus.com/embed.js';

        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
      })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript> -->

    <!-- <div id="disqus_thread"></div>
    <script type="text/javascript">
      var disqus_shortname = 'bodynbrain';
      (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
      })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript> -->

  </div>
</div>
</div>
