<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head>
  <title><?php echo $titletag; ?></title>

  <meta name="description" content="<?php echo $descriptiontag; ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" charset="utf-8">

  <!-- Facebook custom post preview -->
  <meta property="og:url"           content="<?php echo $facebookmetaurl; ?>" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="<?php echo $facebookmetatitle; ?>" />
  <meta property="og:description"   content="<?php echo $facebookmetadescription; ?>" />
  <meta property="og:image"         content="<?php echo $facebookmetaimage; ?>" />

  <!-- favicon -->
  <link rel="shortcut icon" href="/img/smalllogo.png">

  <!-- css -->
  <link type="text/css" rel="stylesheet" href="/vendors/bootstrap/dist/css/bootstrap.min.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="/vendors/bootstrap/dist/css/bootstrap-theme.min.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="/fe/css/style.css" />
  <link type="text/css" rel="stylesheet" href="/fe/css/printstyle.css"/>
  <link type="text/css" rel="stylesheet" href="/fe/css/shop.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="/fe/css/pages.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="/fe/css/loader.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="/fe/css/overlay.css"  media="screen,projection"/>
  <link id="skin" rel="stylesheet" type="text/css" media="screen" href="/fe/css/divas_free_skin.css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/4.10.1/bootstrap-social.min.css" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="/vendors/iCheck/skins/square/orange.css" media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="/fe/css/divas_free_skin.css" id="skin" media="screen"  />
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <link type="text/css" rel="stylesheet" href="/vendors/Swiper/dist/css/swiper.css" media="screen,projection"/>

  <!-- CDNs -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/0.4.16/toaster.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>

</head>
<body zoom-container>
<!-- <body ng-controller="MemberCtrl" zoom-container> -->

<!-- <div id="loginreg" ng-show="(signup==true || login==true)" class="animate-if hide" id="non-printable">
  <div class="row" id="signup" ng-if="signup">
    <div class="col-sm-2">
      <h3>Sign Up</h3>
    </div>
    <div class="col-sm-10">
      <a href="" class="btn btn-link pull-left left" ng-click="loginbtn()">login</a>
      <span class="pull-left">|</span>
      <a href="" class="btn btn-link pull-left right">forgot my password</a>
      <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {[{ perc }]}%;">
        </div>
      </div>
      <button type="button" class="close" data-dismiss="alert" ng-click="closebtn()" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>

    <div class="col-sm-12 animate-hide" ng-hide="wizard!=1">
      <form class="form-inline">
        <div class="form-group">
          <input type="email" class="form-control" id="exampleInputName2" placeholder="email address" ng-model="member.email">
          <p ng-if="error.email!=undefined">{[{ error.email }]}</p>
        </div>
        <div class="form-group">
          <input type="password" class="form-control" id="exampleInputEmail2" placeholder="password" ng-model="member.password">
          <p ng-if="error.password!=undefined">{[{ error.password }]}</p>
        </div>
        <div class="form-group">
          <input type="password" class="form-control" id="exampleInputEmail2" placeholder="confirm password" ng-model="member.confpass">
          <p ng-if="error.confpass!=undefined">{[{ error.confpass }]}</p>
        </div>
        <button type="button" class="btn btn-bnb" ng-click="next(member)">{[{ btn }]}</button>
      </form>
    </div>

    <div class="col-sm-12 animate-hide" ng-hide="wizard!=2">
      <form class="form-inline">
        <div class="form-group">
          <input type="text" class="form-control" id="exampleInputName2" placeholder="first name" ng-model="member.firstname">
          <p ng-if="error.firstname!=undefined">{[{ error.firstname }]}</p>
        </div>
        <div class="form-group">
          <input type="text" class="form-control" id="exampleInputEmail2" placeholder="last name" ng-model="member.lastname">
          <p ng-if="error.lastname!=undefined">{[{ error.lastname }]}</p>
        </div>
        <div class="form-group">
          <input type="text" class="form-control" id="exampleInputEmail2" placeholder="phone number" ng-model="member.phoneno" only-digits>
          <p ng-if="error.phoneno!=undefined">{[{ error.phoneno }]}</p>
        </div>
        <button type="button" class="btn btn-bnb" ng-click="next(member)">next</button>
      </form>
    </div>

    <div class="col-sm-12 animate-hide" ng-hide="wizard!=3">
      <form class="form-inline">
        <div class="form-group">
          <select class="form-control" ng-model="member.state" ng-options="state.state_code for state in states" style="width:230px;" ng-change="changestate(member.state)"></select>
        </div>
        <div class="form-group">
          <select class="form-control" ng-model="member.center" ng-options="center.centertitle for center in centers" style="width:230px;"></select>
        </div>
        <div class="form-group">
          <div class="gray-text text-center" style="width:211px;margin-top:10px;">(optional, members only)</div>
        </div>
        <button type="button" class="btn btn-bnb" ng-click="next(member)">next</button>
      </form>
    </div>

  </div>

  <div class="row" id="login" ng-if="login">
    <div class="col-sm-2">
      <h3>Log In</h3>
    </div>
    <div class="col-sm-10">
      <a href="" class="btn btn-link pull-left left" ng-click="signupbtn()">sign up</a>
      <span class="pull-left">|</span>
      <a href="" class="btn btn-link pull-left right">forgot my password</a>

      <button type="button" class="close close2" data-dismiss="alert" ng-click="closebtn()" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>

    <div class="col-sm-12">
      <form class="form-inline">
        <div class="form-group">
          <input type="email" class="form-control" id="exampleInputName2" placeholder="email address" ng-model="member.email">
        </div>
        <div class="form-group">
          <input type="password" class="form-control" id="exampleInputEmail2" placeholder="password" ng-model="member.password">
        </div>
        <button type="button" class="btn btn-bnb" ng-click="loginaction(member)" id="loginmember">log in</button>
      </form>
    </div>
    <div class="col-sm-8">
      <p ng-if="alert!=null">{[{ alert }]}</p>
    </div>
    <div class="col-sm-4 sc">
      <p class="pull-left">Log with: </p>
      <a href="" ng-click="logintwitter()"><img src="/img/socialmedia/twitter.png" alt="" class="pull-right"></a>
      <a href="" ng-click="logingplus()"><img src="/img/socialmedia/google.png" alt="" class="pull-right"></a>
      <a href="" ng-click="loginFb()"><img src="/img/socialmedia/fb.png" alt="" class="pull-right"></a>
    </div>
  </div>
</div> -->

<div class="container-fluid" class="containerminheight">

  <div class="row normalnavbar" id="non-printable"> <!-- navbar row -->
    <nav class="navbar navbar-default navheight">
      <div class="navbar-header">
        <a class="navbar-brand navlogo" href="/">
          <img alt="Brand" src="/img/logo.png">
        </a>
      </div>

      <div class="collapse navbar-collapse navheight" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navlistleftpadding">
          <!-- <li class="<?php echo $pageslugs == $about ? 'active':''; ?>"><a href="/{{about}}">about</a></li>
          <li class="<?php echo $pageslugs == $press ? 'active':''; ?>"><a href="/{{press}}/1">press</a></li>
          <li class="<?php echo $pageslugs == $mainnews ? 'active':''; ?>"><a href="/{{mainnews}}/1">b&b buzz</a></li>
          <li class="<?php echo $pageslugs == $location ? 'active':''; ?>"><a href="/{{location}}">location</a></li> -->

          <li><a href="/{{about}}">about</a></li>
          <li><a href="/{{press}}/1">press</a></li>
          <li><a href="/{{mainnews}}/1">b&b buzz</a></li>
          <li><a href="/{{locations}}">locations</a></li>
          <!-- <li><a href="/{{shop}}">shop</a><span class="cartlogo"><a href="/bag"><img src="/img/cartlogo.png"><span class="cartlogocount ng-cloak">{[{ bagcount }]}</span></span></a></li> -->
        </ul>
        <!-- <span class="shopitem" ng-if="new" ng-cloak>{[{ new }]} has been added</span> -->

        <ul class="nav navbar-nav navbar-right navlistrightpadding ng-cloak">
          <!-- <li>
            <a href="#" ng-click="loginbtn()" ng-if="!member.id" id="login">log in</a>
            <a href="#" ng-if="member.id">Hi, {[{ member.firstname }]}.</a>
          </li>
          <li><a class="loginseparator">|</a></li>
          <li>
            <a href="#" ng-click="signupbtn()" ng-if="!member.id">sign up</a>
            <a href="#" ng-click="logout()" ng-if="member.id" id="logout">logout</a>
          </li> -->
          <li class="navtryaclass"><a href="/getstarted/starterspackage" style="color:white!important;">try a class!</a></li>
        </ul>
      </div>
    </nav>
  </div> <!-- end of navbar row -->

  <div class="row mobilenavbar" style="display:none;" id="non-printable"> <!-- navbar row -->
      <nav class="navbar navbar-default" role="navigation">
         <div class="container-fluid" id="navfluid">
             <div class="navbar-header">
                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigationbar">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand navmobilelogo" href="/">
                      <img alt="Brand" src="/img/logo.png" width="100%">
                  </a>
                  <a href="/getstarted/starterspackage" class="btn-tryclass pull-right">try a class!</a>
             </div>
             <div class="collapse navbar-collapse" id="navigationbar">
                 <ul class="nav navbar-nav">
                    <li><a href="/{{about}}">about</a></li>
                    <li><a href="/{{press}}/1">press</a></li>
                    <li><a href="/{{mainnews}}/1">b&b buzz</a></li>
                    <li><a href="/{{locations}}">locations</a></li>
                    <!-- <li class="cartlogoli">
                      <a href="/{{shop}}">shop</a>
                      <span class="cartlogo"><a href="/bag"><img src="/img/cartlogo.png"><span class="cartlogocount ng-cloak">{[{ bagcount }]}</span></span></a></li>
                    <li><a href="#">login</a></li>
                    <li><a href="#">sign up</a></li> -->
                 </ul>
                <ul class="nav navbar-nav navbar-right navlistrightpadding ng-cloak fv">
                  <!-- <li>
                    <a href="#" ng-click="loginbtn()" ng-if="!member.id">login</a>
                    <a href="#" ng-if="member.id">Hi, {[{ member.firstname }]}.</a>
                  </li>
                  <li><a class="loginseparator">|</a></li>
                  <li>
                    <a href="#" ng-click="signupbtn()" ng-if="!member.id">sign up</a>
                    <a href="#" ng-click="logout()" ng-if="member.id">logout</i></a>
                  </li> -->
                  <li class="navtryaclass"><a href="/getstarted/starterspackage"><span>try a class!</span> <i class="fa fa-users"></i></a></li>
                </ul>

            </div><!-- /.navbar-collapse -->
         </div><!-- /.container-fluid -->
      </nav>
  </div> <!-- end of navbar row -->

  <?php echo $this->getContent(); ?>

  <div class="row footernormal" id="non-printable"> <!-- footer row -->
    <div class="mainfooter">
      <nav class="navbar navbar-default">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navlistleftpadding">
            <li style="<?php echo $pageslugs == $founder ? 'font-weight:bold;':''; ?>"><a href="/{{founder}}">founder</a></li>
            <li style="<?php echo $pageslugs == $affiliates  ? 'font-weight:bold;':''; ?>"><a href="/{{affiliates}}">affiliates</a></li>
            <li style="<?php echo $pageslugs == $faqs ? 'font-weight:bold;':''; ?>"><a href="/{{faqs}}">FAQs</a></li>
            <li style="<?php echo $pageslugs == $franchising ? 'font-weight:bold;':''; ?>"><a href="/{{franchising}}">franchising</a></li>
            <li><a class="a-contactus">contact us</a></li>
            <!-- <li><a href="/{{terms}}">terms & privacy</a></li> -->
          </ul>

          <ul class="nav navbar-nav navbar-right navlistrightpadding navheight">
            <li style="<?php echo $pageslugs == $terms || $pageslugs == $privacy  ? 'font-weight:bold; color:black!important;':''; ?>"><a href="/{{terms}}">terms and privacy</a></li>
            <li><a style="padding-left:0; padding-right:0;">|</a></li>
            <li><a href="/">&#169; 2015 BodynBrain</a></li>
            <li class="navsmalllogo">
              <a class="navbar-brand" href="/">
                <img alt="Brand" src="/img/smalllogo.png">
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </div> <!-- end of footer row -->

  <div class="footer-mv" id="non-printable">
    <div class="row footermobile" style="display:none;">
        <div class="col-sm-12 footermenu"><a href="/{{founder}}">founder</a></div>
        <div class="col-sm-12 footermenu"><a href="/{{affiliates}}">affiliates</a></div>
        <div class="col-sm-12 footermenu"><a href="/{{faqs}}">FAQs</a></div>
        <div class="col-sm-12 footermenu"><a href="/{{franchising}}">franchising</a></div>
        <div class="col-sm-12 footermenu a-contactus">contact us</div>
        <!-- <div class="col-sm-12 footermenu"><a href="/{{terms}}">terms & privacy</a></div> -->
    </div>

    <div class="row footermobile" style="display:none;"> <!-- footer row -->
        <div class="mainfooter">
            <nav class="navbar navbar-default mobilefooternavbar">
                <div id="footercontent-mv">
                  <a id="terms-mv" href="/{{terms}}">terms & privacy</a>
                  <a id="separator-mv">|</a>
                  <span id="copyright-mv">&#169; 2015 BodynBrain</span>
                </div>

                <div class="footersmalllogo">
                  <a class="navbar-brand" href="/">
                      <img id="bot-minilogo-mv" alt="Brand" src="/img/smalllogo.png">
                  </a>
                </div>
            </nav>
        </div>
    </div> <!-- end of footer row -->
  </div>

</div> <!-- end of container -->

<div class="dv-contactus" id="non-printable" ng-controller="contactUsCtrl">
	<img src="/img/pages/contactus/x.png" class="x-contactus"/>
	<div class="font-md">
		For questions or comments:
		<br>
		Send us an email or contact one of our centers.

		<div class="col-sm-6 mg-md-x">
			<div class="styled-select1">
				<select ng-model="contact.state" ng-change="changeContactState(contact.state)">
					<option value="" hidden selected>State</option>
					<option ng-repeat="state in contactUsStates" value="{[{state.state_code}]}" ng-bind="state.state"></option>
				</select>
			</div>
		</div>
		<div class="col-sm-6 mg-md-x">
			<div class="styled-select1">
				<select ng-model="contact.center" ng-change="changeContactCenter(contact.center)">
					<option value="" hidden selected>Center Name</option>
					<option ng-repeat="center in contactUsCenters" value="{[{center.centerslugs}]}" ng-bind="center.centertitle"></option>
				</select>
			</div>
		</div>
    <div class="col-sm-12">
      <span ng-if="contact.center != undefined && contact.center != '' " class="visitcenter" ng-click="visitCenter(contact.center)">visit this center</span>
    </div>

		<label class="font-xs">email</label>
		<div><a ng-href="mailto:{[{contact.email}]}" ng-bind="contact.email"></a></div>
		<hr class="dr1 mg-sm">
		<label class="font-xs">phone</label>
    <div ng-bind="contact.phone"></div>
    <br>
		<a href="/{{locations}}">view our centers</a>
	</div>
</div> <!-- end of dv-contactus -->

  <!-- Angularjs -->
  <script type="text/javascript" src="/vendors/jquery/dist/jquery.min.js"></script>
  <script type="text/javascript" src="/vendors/angular/angular.min.js"></script>
  <script type="text/javascript" src="/be/js/angular/ui-bootstrap-tpls.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-jwt/dist/angular-jwt.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-animate/angular-animate.min.js"></script>
  <script type="text/javascript" src="/vendors/a0-angular-storage/dist/angular-storage.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-facebook/lib/angular-facebook.js"></script>
  <script type="text/javascript" src="/vendors/angular-google-plus/dist/angular-google-plus.min.js"></script>
  <script type="text/javascript" src="/vendors/oauth-js/dist/oauth.min.js"></script>
  <script type="text/javascript" src="/fe/scripts/twitter.js"></script>
  <script type="text/javascript" src="/vendors/angular-google-maps/dist/angular-google-maps.min.js"></script>
  <script type="text/javascript" src="/vendors/lodash/lodash.min.js"></script>
  <script type="text/javascript" src="/vendors/angularUtils-pagination/dirPagination.js"></script>
  <script type="text/javascript" src="/vendors/angular-vertilize/angular-vertilize.min.js"></script>
  <script type="text/javascript" src="/vendors/ng-file-upload/ng-file-upload.min.js"></script>
  <script type="text/javascript" src="/vendors/angularjs-toaster/toaster.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-moment/angular-moment.min.js"></script>
  <script type="text/javascript" src="/vendors/moment/min/moment.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-country-state-select/dist/angular-country-state-select.js"></script>
  <script type="text/javascript" src="/vendors/angular-uuid-service/angular-uuid-service.min.js"></script>
  <script type="text/javascript" src="/vendors/re-tree/re-tree.min.js"></script>
  <script type="text/javascript" src="/vendors/ng-device-detector/ng-device-detector.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-sanitize/angular-sanitize.min.js"></script>
  <script type="text/javascript" src="/vendors/Swiper/dist/js/swiper.min.js"></script>
  <script type="text/javascript" src="/vendors/isotope/dist/isotope.pkgd.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-isotope/dist/angular-isotope.min.js"></script>


  <!-- Angularjs app controller services factory and directives -->
  <script type="text/javascript" src="/fe/scripts/app.js"></script>
  <script type="text/javascript" src="/fe/scripts/config.js"></script>
  <!--<script type="text/javascript" src="/fe/scripts/controllers/member.js"></script> -->
  <script type="text/javascript" src="/fe/scripts/controllers/contactUsCtrl.js"></script>
  <script type="text/javascript" src="/fe/scripts/factory/factory.js"></script>
  <!--<script type="text/javascript" src="/fe/scripts/factory/member.js"></script>
  <script type="text/javascript" src="/fe/scripts/factory/login.js"></script> -->
  <script type="text/javascript" src="/fe/scripts/directives/validations.js"></script>

  <!-- js -->
  <script type="text/javascript" src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script type="text/javascript" src="/fe/scripts/others/jquery.divas-1.1.min.js"></script>
  <script type="text/javascript" src="/fe/scripts/others/jquery.mask.js"></script>
  <script type="text/javascript" src="/fe/scripts/others/jquery.resizeimagetoparent.min.js"></script>
  <!-- // <script type="text/javascript" src="/vendors/ez-plus/src/jquery.ez-plus.js"></script> -->

  <!-- Dynamically loaded js -->
  <?php
    if(isset($otherjvascript)){
      echo $otherjvascript ;
    }
  ?>

  <script type="text/javascript">
   $("#phonenumber").mask("999-999-9999");
    $(function() {
      $( "#datepicker" ).datepicker();
      $( "#datepicker1" ).datepicker();
    });
    $(".a-contactus").click(function(){
      $(".dv-contactus").css("display","inline-block");
      $("html, body").animate({ scrollTop: 0 }, "slow");
    });
    $(".x-contactus").click(function(){
      $(".dv-contactus").css("display","none");
    });
  </script>

  <script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 0,
        loop: true,
        preloadImages: false,
        lazyLoading: true,
        autoplay: 5000,
        autoplayDisableOnInteraction: false
    });
    var swiper = new Swiper('.swiper-containermobile', {
        // pagination: '.swiper-pagination',
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 0,
        loop: true,
        preloadImages: false,
        lazyLoading: true,
        autoplay: 5000,
        autoplayDisableOnInteraction: false
    });
    </script>

</body>
</html>
