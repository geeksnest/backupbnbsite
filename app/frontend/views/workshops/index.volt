<div class="secondarynav">
  <ul>
    <li><a href="/{{about}}">Who Are We?</a></li>
    <li><a href="/{{classes}}">Classes</a></li>
    <li class="active"><a href="/{{workshops}}">Workshops</a></li>
    <li><a href="/{{wellness}}">Wellness at Work</a></li>
    <li><a href="/{{stories}}/1">Testimonials</a></li>
  </ul>
</div>

<div class="secondarynavmobile" style="display:none;">
  <div class="secondarynavdropdown">
    <ul class="nav nav-pills">
      <li role="presentation" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <label class="activetext">{{pagetitle}}</label> <i class="fa fa-chevron-down"></i>
        </a>
        <ul class="dropdown-menu">
          <li><a href="/{{about}}">Who Are We?</a></li>
          <li><a href="/{{classes}}">Classes</a></li>
          <li><a href="/{{workshops}}">Workshops</a></li>
          <li><a href="/{{wellness}}">Wellness at Work</a></li>
          <li><a href="/{{stories}}/1">Testimonials</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<div class="pagebanner" style="background:none;">
  <div class="pageheader"> WORKSHOPS </div>

  {% if nobanner == false %}
    {% if slider == true %}

      <div class="swiper-banner">
        <div class="swiper-wrapper">
          {% for banner in pagebanners %}
            <div class="swiper-slide">
              {% if banner.url == null OR banner.url == "" %}
                <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" width="100%" />
              {% else %}
                <a href="{{banner.url}}" target="_blank">
                  <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" width="100%" />
                </a>
              {% endif %}
            </div>
          {% endfor %}
        </div>
        <div class="swiper-pagination"></div>
      </div>

    {% else %}

        {% if pagebanners[0].url == null OR pagebanners[0].url == "" %}
          <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
        {% else %}
          <a href="{{pagebanners[0].url}}" target="_blank">
            <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
          </a>
        {% endif %}

    {% endif %}
  {% else %}
    <img src="/img/banners/about-workshops.jpg" width="100%" />
  {% endif %}

</div>

<div class="container-fluid wrapper-lg">
  {% if notitle == true %}
    <div class="col-sm-12">
      <span style="font-size:20px!important;">
        <b>NO WORKSHOP AVAILABLE</b>
      </span>
    </div>

    <div class="col-sm-12">
      <p class="pg-p font-xs i">
        Information about more advanced workshops are available at Body & Brain centers upon request and per practitioner’s training level. They include, but are not limited to: Brain Management Training, Korea / New Zealand Meditation Suhaeng, DahnMuDo, Sedona Health Coaching, Brain Education Leadership, Dahn Master Course and much more.
      <p>
    </div>
  {% else %}
    {% for workshoptitle in workshoptitles %}
      <div class="col-sm-4 item wrapper-bot-md">
        <div class="pg-mini-title text-up">{{workshoptitle.title}}</div>

        {% if workshoptitle.brochure != null OR workshoptitle.brochure != "" %}
        <div class="ws-brochure">
          <a href="{{amazonlink}}/uploads/workshopbrochure/{{workshoptitle.brochure}}" class="a-blue" download>
            <img src="/img/pages/workshop/brochure.png"/>
            Download Brochure
          </a>
        </div>
        {% endif %}
        <p class="pg-p">{{ workshoptitle.description }}</p>
        <!-- <a href="/{{workshops}}/{{workshoptitle.titleslugs}}" class="btn btn-orange">Sign Up</a> -->
      </div>
    {% endfor %}
    <div class="col-sm-4 item">
      <p class="pg-p font-xs i">
        Information about more advanced workshops are available at Body & Brain centers upon request and per practitioner’s training level. They include, but are not limited to: Brain Management Training, Korea / New Zealand Meditation Suhaeng, DahnMuDo, Sedona Health Coaching, Brain Education Leadership, Dahn Master Course and much more.
      </p>
    </div>
  {% endif %}

</div>
