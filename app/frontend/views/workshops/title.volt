<div id="workshops" ng-controller="wsTitleCtrl">
  <div class="secondarynav">
    <ul class="lipointer">
      <li class="phase1 active" ng-disabled="disPhase1" ng-click="disPhase1 || gotoPhase(1)">Find a center nearest you to schedule your workshop</li>
      <li class="phase2" ng-click="disPhase2 || gotoPhase(2)" ng-disabled="disPhase2" ng-init="disPhase2 = true">Enter Information</li>
      <li class="phase3">Confirm Your Appointment</li>
    </ul>
  </div>

  <div class="secondarynavmobile" style="display:none;">
    <div class="secondarynavdropdown">
      <ul class="nav nav-pills">
        <li role="presentation" class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">

            {# the pagetitle variable here is ANGULAR, unlike on the other pages its VOLT -nu77 #}
            <label class="activetext">{[{pagetitle}]}</label> <i class="fa fa-chevron-down"></i>

          </a>
          <ul class="dropdown-menu lipointer">
            <li class="phase1 active" ng-disabled="disPhase1" ng-click="gotoPhase(1)">Find a center</li>
            <li class="phase2" ng-click="disPhase2 || gotoPhase(2)" ng-disabled="disPhase2" ng-init="disPhase2 = true">Enter Information</li>
            <li class="phase3">Confirm Your Appointment</li>
          </ul>
        </li>
      </ul>
    </div>
  </div>

  <div ng-show="step1" ng-init="step1 = true">
    <div class="pagebanner pg-banner">
      <div class="pageheader"> {{workshoptitle}} </div>
      <img src="/img/banners/tryaclassbanner.png" width="100%" />
    </div>

    <div class="container-fluid wrapper-lg-vw">
      <div class="col-sm-6 centersearchform">
        <fieldset ng-disabled="isBusy">
          <form name="findWorkshopFORM" ng-submit="findWorkshopSubmit(findWorkshop)">
            <div class="searchformheader">
              Find a center nearest you to schedule your workshop.
            </div>

            <div class="form-group">
              <label class="col-sm-5">Search By Zip Code</label>
              <div class="col-sm-7"><input type="text" class="form-control" ng-model="findWorkshop.zip" placeholder="Ex. 82382"/></div>
            </div>

            <div class="form-group">
              <label class="col-sm-5">Search By City & State</label>
              <div class="col-sm-7"><input type="text" class="form-control" ng-model="findWorkshop.city"  placeholder="Ex. Glendale, AZ"/></div>
            </div>

            <div class="form-group">
              <label class="col-sm-5 ">Search By State</label>
              <div class="col-sm-7">
                <select class="form-control" id="locationstate" ng-model="findWorkshop.state">
                  <option value="">Select State</option>
                  <option ng-repeat="state in statelist" value="{[{state.state_code}]}">{[{state.state}]}</option>
                </select>
              </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-orange">search</button>
            </div>
          </form>
        </fieldset>
      </div>

      <div class="col-sm-6 centersearchresult">
        <div ng-show="hasResult">
          <div ng-bind="workshopState" class="workshopstate"></div>
          Choose a location
          <br><br>
          <div>
            <div class="gthanArrow centerlist" ng-repeat="ws in workshopResult" ng-click="gotostep2(ws)">
              <div ng-if="ws.venue == 'bnbcenter' ">
                <span class="first" ng-bind="ws.centertitle"></span>
              </div>
              <div ng-if="ws.venue == 'bnbvenue' ">
                <span class="first" ng-bind="ws.workshopcity"></span>
                (<span ng-bind="ws.centertitle"></span>)
              </div>
            </div>
          </div>
        </div>
        <div ng-show="noResult">
          <center>
          <img src="/img/frontend/nosearch.png" />
            <br><br>
            <h3>NO RESULT FOUND</h3>
         </center>
        </div>
      </div>
    </div>
  </div>

  <div class="centermap" ng-show="hasResult || step2" style="height:500px;">
    <div id="map" style="position:relative;">
      <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options">
        <ui-gmap-markers models="randomMarkers" coords="'self'" icon="'icon'" fit="true">
          <ui-gmap-windows show="show">
            <div ng-non-bindable>{[{title}]}</div>
          </ui-gmap-windows>
        </ui-gmap-markers>
      </ui-gmap-google-map>
    </div>
  </div>

  <div ng-show="step2" ng-init="step2 = false">
    <div ng-include="'/fe/tpl/workshops_step2.html'"></div>
  </div>

  <div ng-show="step3" ng-init="step3 = false">
    <div ng-include="'/fe/tpl/workshops_step3.html'"></div>
  </div>

  <div ng-show="step4" ng-init="step4 = false">
    <div ng-include="'/fe/tpl/workshops_step4.html'"></div>
  </div>

<toaster-container toaster-options="{'time-out': 7000, 'close-button':true, 'animation-class': 'toast-top-center'}"></toaster-container>
</div> {#controller#}
