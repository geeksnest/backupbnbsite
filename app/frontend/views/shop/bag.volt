<div ng-controller="BagCtrl" class="ng-cloak minheight">
	<toaster-container ></toaster-container>
	<div class="bag" ng-if="bag.length > 0">
		<table>
			<thead>
				<tr>
					<td style="width: 50%">Your Bag</td>
					<td style="width: 20%">Price</td>
					<td style="width: 20%">Quantity</td>
					<td style="width: 20%">Subtotal</td>
					<td>&nbsp;</td>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="product in bag">
					<td>
						<div class="img pull-left">
							<img src="{[{ amazonlink }]}/uploads/productimage/{[{ product.productid }]}/{[{ product.filename }]}" alt="">
						</div>
						<div class="name pull-left">
							{[{ product.name }]}
						</div>
					</td>
					<td>{[{ product.price | currency }]} <span ng-if="product.disc">{[{ "(-"+product.discount+"%)" }]}</span></td>
					<td>
						<select class="form-control" ng-model="product.bagquantity" style="width: 65px;" ng-change="onqty(product)">
              				<option ng-repeat="avquantity in product.avquantity">{[{ avquantity }]}</option>
						</select>
					</td>
					<td>{[{ product.total | currency }]}</td>
					<td>
						<button type="button" class="close" data-dismiss="alert" ng-click="remove(product.id)" aria-label="Close">
					    	<span aria-hidden="true">&times;</span>
					    </button>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="checkout hiddenoverflow">
			<div class="tax">
				<label class="pull-left">Tax</label>
				<div class="pull-left">{[{ tax | currency }]}</div>
			</div>
			<div class="shipping">
				<label class="pull-left">Shipping</label>
				<div class="pull-left">{[{ shippingfee | currency }]}</div>
			</div>
			<div class="total">
				<label class="pull-left">Estimated Total</label>
				<div class="pull-left text-bold">{[{ total | currency }]}</div>
			</div>
			<a href="/checkout" class="btn btn-block btn-lg btn-bnb">
				checkout
			</a>
			<a href="/shop" class="btn btn-link btn-block no-hover">continue shopping</a>
		</div>
	</div>

	<div class="bag empty" ng-if="bag.length == 0">
		<h1>Your bag is empty!</h1>
	</div>
</div>