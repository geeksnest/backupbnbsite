<script type="text/ng-template" id="billinginfo.html">
  <div ng-include="'/fe/tpl/billinginfo.html'"></div>
</script>

<div ng-controller="CheckoutCtrl" class="ng-cloak minheight">
	<toaster-container ></toaster-container>
	<div id="logreg" ng-if="member.id==undefined">
		<h3>Sign In</h3>
		<p>Already have an account? Continue your checkout by signing in.</p>
    	<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
		<form class="form-horizontal">
			<div class="form-group">
				<div class="col-sm-12">
                  	<span class="label label-danger mg-left" ng-if="error.email!=''">{[{ error.email }]}</span>
					<input type="email" class="form-control input-lg" ng-model="member.email" placeholder="email address" ng-change="onemail(member.email)">
				</div>
			</div>
		  	<div class="form-group">
		    	<div class="col-sm-12">
                  	<span class="label label-danger mg-left" ng-if="error.password!=''">{[{ error.password }]}</span>
		      		<input type="password" class="form-control input-lg" ng-model="member.password" placeholder="password" ng-change="onpass(member)">
		    	</div>
		  	</div>

		  	<div ng-if="register">
			  	<div class="form-group">
			    	<div class="col-sm-12">
                  		<span class="label label-danger mg-left" ng-if="error.confpass!=''">{[{ error.confpass }]}</span>
			      		<input type="password" class="form-control input-lg" ng-model="member.confpass" placeholder="confirm password" ng-change="onpass(member)">
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<div class="col-sm-12">
                  		<span class="label label-danger mg-left" ng-if="error.firstname!=''">{[{ error.firstname }]}</span>
			      		<input type="text" class="form-control input-lg" ng-model="member.firstname" placeholder="first name" ng-change="onname(member.firstname, 'fname')">
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<div class="col-sm-12">
                  		<span class="label label-danger mg-left" ng-if="error.lastname!=''">{[{ error.lastname }]}</span>
			      		<input type="text" class="form-control input-lg" ng-model="member.lastname" placeholder="last name" ng-change="onname(member.lastname, 'lname')">
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<div class="col-sm-12">
                  		<span class="label label-danger mg-left" ng-if="error.phoneno!=''">{[{ error.phoneno }]}</span>
			      		<input type="text" class="form-control input-lg" ng-model="member.phoneno" placeholder="phone number" ng-change="onname(member.phoneno, 'phoneno')" only-digits>
			    	</div>
			  	</div>
		  	</div>

		  	<div class="form-group">
		    	<div class="col-sm-12" ng-if="register==false">
		      		<button type="button" class="btn btn-info btn-block btn-lg" ng-click="signin(member)">Sign in</button>
		    	</div>
		    	<div class="col-sm-12">
		      		<button type="button" class="btn btn-bnb btn-block btn-lg" ng-click="reg(member)">Create an account</button>
		    	</div>
		    	<div class="col-sm-12" ng-if="register">
		      		<button type="button" class="btn btn-info btn-block btn-lg" ng-click="registerfalse()">Go back</button>
		    	</div>
		    	<div ng-if="!register">
		    		<div class="col-sm-12">
	                	<a class="btn btn-block btn-social btn-facebook btn-lg" ng-click="loginFb()">
	                		<span class="fa fa-facebook"></span> Sign in with Facebook
	                	</a>
	                </div>
		    		<div class="col-sm-12">
	                	<a class="btn btn-block btn-social btn-twitter btn-lg" ng-click="logintwitter()">
	                		<span class="fa fa-twitter"></span> Sign in with Twitter
	                	</a>
	                </div>
		    		<div class="col-sm-12">
	                	<a class="btn btn-block btn-social btn-google btn-lg" ng-click="logingplus()">
	                		<span class="fa fa-google-plus"></span> Sign in with Google
	                	</a>
	                </div>
	            </div>
		  	</div>
		</form>
	</div>

	<div id="checkout" ng-if="member.id!=undefined">
		<header>
			<div class="bag no-margin-top">
				<div class="row">
					<div class="col-sm-6">
						<h5 class="pull-right" ng-class="{ borderbottom : ccinfo }">Credit Card Information</h5>
					</div>
					<div class="col-sm-6">
						<h5 class="pull-left" ng-class="{ borderbottom : verification }">Verification</h5>
					</div> 
				</div>
			</div>
		</header>
		
		<div class="bag" ng-if="ccinfo">
			<div class="row">
				<div class="col-lg-6">
					<div class="row tryaclasspaymentmethodtype">
		            	<div class="col-sm-6 col-xs-6 tryaclasspaymentmethodchoices">
		              		<button type="submit" class="btn btn-lg btn-block" ng-click="onpaymenttype('paypal')" ng-class="{'orangebutton':paymenttype == 'paypal'}">
		                		<i class="fa fa-cc-paypal"></i> Paypal
		              		</button>
		            	</div>
		            	<div class="col-sm-6 col-xs-6 tryaclasspaymentmethodchoices">
		              		<button type="submit" class="btn btn-lg btn-block" ng-click="onpaymenttype('card')" ng-class="{'orangebutton':paymenttype == 'card'}">
		                		<i class="fa fa-credit-card"></i> Credit Card
		              		</button>
		            	</div>
		        	</div>
					
					<form class="form-horizontal customform" name="billingform">
			        	<div ng-if="paymenttype=='card'">
							<div class="form-group hiddenoverflow">
						    	<label for="inputPassword3" class="col-sm-12">Billing Info</label>
								<div class="col-sm-12">
          							<span class="label label-danger" ng-if="checkout.name">This field is required.</span>
						      		<input type="text" class="form-control input-lg" ng-model="creditcard.name" placeholder="Name" ng-change="change(creditcard.name, 'name')" required>
						    	</div>
						  	</div>
							<div class="form-group hiddenoverflow">
						    	<div class="col-sm-12">
          							<span class="label label-danger" ng-if="checkout.ccn">This field is required.</span>
						      		<input type="text" class="form-control input-lg" ng-model="creditcard.ccn" placeholder="Card Number" ng-change="change(creditcard.ccn, 'ccn')" required>
						    	</div>
							</div>
							<div class="form-group hiddenoverflow">
						    	<div class="col-sm-5">
          							<span class="label label-danger" ng-if="checkout.ccvn">This field is required.</span>
						      		<input type="text" class="form-control input-lg" ng-model="creditcard.ccvn" placeholder="CVV" ng-change="change(creditcard.ccvn, 'ccvn')" required>
						    	</div>
							</div>
						  	<div class="form-group hiddenoverflow">
								<div class="col-sm-5">
          							<span class="label label-danger" ng-if="checkout.expiremonth">This field is required.</span>
						      		<select ng-model="creditcard.expiremonth" class="form-control" ng-options="m.val as m.name for m in months" ng-change="change(creditcard.expiremonth, 'expiremonth')" required></select>
						    	</div>
						  	</div>
						  	<div class="form-group hiddenoverflow">
						    	<div class="col-sm-5">
          							<span class="label label-danger" ng-if="checkout.expireyear">This field is required.</span>
						      		<select ng-model="creditcard.expireyear" class="form-control" ng-options="y.val as y.val for y in years" ng-change="change(creditcard.expireyear, 'expireyear')" required></select>
						    	</div>
						  	</div>
						</div>

					 	<div class="form-group hiddenoverflow">
					    	<label for="inputPassword3" class="col-sm-12">Shipping Address</label>
							<div class="col-sm-12">
      							<span class="label label-danger" ng-if="checkout.firstname">This field is required.</span>
					      		<input type="text" class="form-control input-lg" ng-model="shippinginfo.firstname" placeholder="First Name" ng-change="change(shippinginfo.firstname, 'firstname')" required>
					    	</div>
					  	</div>
					  	<div class="form-group hiddenoverflow">
					    	<div class="col-sm-12">
      							<span class="label label-danger" ng-if="checkout.lastname">This field is required.</span>
					      		<input type="text" class="form-control input-lg" ng-model="shippinginfo.lastname" placeholder="Last Name" ng-change="change(shippinginfo.lastname, 'lastname')" required>
					    	</div>
					  	</div>
					  	<div class="form-group hiddenoverflow">
							<div class="col-sm-12">
          							<span class="label label-danger" ng-if="checkout.address">This field is required.</span>
					      		<input type="text" class="form-control input-lg" ng-model="shippinginfo.address" placeholder="Address" ng-change="change(shippinginfo.address, 'address')" required>
					    	</div>
					  	</div>
					  	<div class="form-group hiddenoverflow">
					    	<div class="col-sm-12">
					      		<input type="text" class="form-control input-lg" ng-model="shippinginfo.address2" placeholder="Address 2" required>
					    	</div>
					  	</div>
					  	<div class="form-group hiddenoverflow">
							<div class="col-sm-6">
      							<span class="label label-danger" ng-if="checkout.city">This field is required.</span>
					      		<input type="text" class="form-control input-lg" ng-model="shippinginfo.city" placeholder="City" ng-change="change(shippinginfo.city, 'city')" required>
					    	</div>
					    	<div class="col-sm-6">
      							<span class="label label-danger" ng-if="checkout.country">This field is required.</span>
              					<div ng-model="shippinginfo.country" country-select ng-change="change(shippinginfo.country, 'country')" required></div>
					    	</div>
					  	</div>
					  	<div class="form-group hiddenoverflow">
							<div class="col-sm-6">
      							<span class="label label-danger" ng-if="checkout.state">This field is required.</span>
              					<div country="{[{ shippinginfo.country }]}" ng-model="shippinginfo.state" ng-change="change(shippinginfo.state, 'state')" state-select required></div>
					    	</div>
					    	<div class="col-sm-6">
      							<span class="label label-danger" ng-if="checkout.zipcode">This field is required.</span>
					      		<input type="text" class="form-control input-lg" ng-model="shippinginfo.zipcode" placeholder="Zip code" ng-change="change(shippinginfo.zipcode, 'zipcode')" only-digits required>
					    	</div>
					  	</div>
					  	<div class="form-group hiddenoverflow" ng-if="paymenttype=='card'">
					    	<div class="col-sm-4">
					      		<button type="submit" class="btn btn-bnb btn-block btn-lg" ng-click="next()">Next</button>
					    	</div>
					  	</div>
					</form>

					<div ng-if="paymenttype=='paypal'">
		        		<form class="paypalform" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
		        			<input type="hidden" name="business" value="jeevon.ang-facilitator@geeksnest.com">
                            <input type="hidden" name="cmd" value="_cart">
                            <input type="hidden" name="upload" value="1">
                            <input type="hidden" name="lc" value="US">
                            <input type="hidden" name="custom" value="shop|{[{invoiceno}]}">
                            <input type="hidden" name="invoice" ng-model="invoiceno">
                            <input type="hidden" name="return" value="{[{BaseURL}]}/shop/payment/success">
				            <input type="hidden" name="cancel_return" value="{[{BaseURL}]}/shop/cancel">
                            <div ng-repeat="product in bag">
                                <input type="hidden" name="item_name_{[{ $index + 1 }]}" value="{[{ product.name }]}">
                                <input type="hidden" name="amount_{[{ $index + 1 }]}" value="{[{ product.price }]}">
                                <input type="hidden" name="quantity_{[{ $index + 1 }]}" value="{[{ product.bagquantity }]}">
                            </div>
                            <input type="hidden" name="currency_code" value="USD">
							
                        </form>

		        		<input type="image" name="submit" border="0" src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/buy-logo-large.png" alt="PayPal - The safer, easier way to pay online" ng-click="processpaypal()" style="padding: 0px;">
		        	</div>
				</div>
				<div class="col-lg-6">
					<div ng-repeat="product in bag">
						<img src="{[{ amazonlink }]}/uploads/productimage/{[{ product.productid }]}/{[{ product.filename }]}" style="width: 60%; margin:5px" alt="">
						<p class="text-bold">{[{ product.bagquantity + " x " + product.name }]}</p>
						<h3>{[{ product.total | currency }]}</h3>
						<hr>
					</div>

					<p class="text-bold">Tax : {[{ tax | currency }]}</p>
					<p class="text-bold">Shipping Fee : {[{ shippingfee | currency }]}</p>
					<p class="text-bold">Order Total : {[{ total | currency }]}</p>
				</div>
			</div>
		</div>

		<div class="bag verify" ng-if="verification">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="header">Please confirm your information.</h1>
					<h4 class="text-center"><a href="/shop">Return to shop page <</a></h4>
					<div class="row">
						<div class="col-lg-offset-3 col-lg-6">
							<div class="row">
								<div class="col-lg-6">
									<p>Order Summary</p>
									<div ng-repeat="product in bag">
										<div class="row">
											<div class="col-lg-6">
												<img src="{[{ amazonlink }]}/uploads/productimage/{[{ product.productid }]}/{[{ product.filename }]}" alt="">
											</div>
											<div class="col-lg-6">
												<p class="gray">{[{ product.bagquantity + " " + product.name }]}</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 memberinfo">
									<div ng-if="paymenttype=='creditcard'">
										<h4>Billing Info</h4>
										<p>{[{ creditcard.name }]}</p>
										<p>Visa - {[{ creditcard.ccn }]}</p>
										<p>CCV# - {[{ creditcard.ccvn }]}</p>
										<p>Expiration Date - {[{ creditcard.expiremonth +"/"+ creditcard.expireyear }]}</p>
									</div>
									<h4>Shipping Info</h4>
									<p>{[{ shippinginfo.firstname + " " + shippinginfo.lastname }]}</p>
									<p>{[{ shippinginfo.address }]}</p>
									<p>{[{ shippinginfo.address2 }]}</p>
									<p>{[{ shippinginfo.city + " " + shippinginfo.state + " " + shippinginfo.zipcode }]}</p>

									<button type="submit" class="btn btn-bnb btn-block btn-lg" ng-disabled="shippingform.$invalid" ng-click="placeorder()">Submit</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>