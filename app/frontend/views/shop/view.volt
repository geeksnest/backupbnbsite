<div ng-controller="addCartCtrl" class="ng-cloak"> 
  <script type="text/ng-template" id="imgzoom.html">
    <div ng-include="'/fe/tpl/imgzoom.html'"></div>
  </script>
  <div class="row">
    <!-- <div class="shop-menu-active">
      Featured Items
    </div> -->
    <div class="shop-menu">
      <ul>
        <li><a href="/shop">Featured Items</a></li>
        <li ng-repeat="featured in featured">
          {[{ featured.category }]} <span ng-if="featured.subcategories.length > 0">&#10095;</span>
          <div class="sub-layer1" ng-if="featured.subcategories.length > 0">
            <!-- <span class="xdtri">&xdtri;</span> -->
            <ul>
              <li ng-repeat="subcategory in featured.subcategories">
                {[{ subcategory.subcategory }]} <span ng-if="subcategory.types.length > 0">&#10095;</span>
                <div class="sub-layer2" ng-if="subcategory.types.length > 0">
                  <!-- <span class="xdtri">&xdtri;</span> -->
                  <ul>
                    <li ng-repeat="type in subcategory.types">{[{ type.type }]}</li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>

  <div class="row prod-view">
    <div class="col-sm-6 item wrapper-lg-vw img-lg">
      <a href="" ng-click="imgZoom(amazonlink + '/uploads/productimage/' + product.productid + '/' + featuredimg );">
        <img src="{[{ amazonlink }]}/uploads/productimage/{[{ product.productid }]}/{[{ featuredimg }]}" class="full-width"/>
      </a>
      <!-- <img ng-elevate-zoom
         ng-src="{[{ featuredimg }]}"
         zoom-image="{[{ featuredimg }]}" class="full-width" /> -->

      <h4 class="text-left">You might also be interested in ...</h4>
      <div class="row" style="margin-top: 30px; margin-bottom:50px">
        <div class="col-lg-4" ng-repeat="product in suggestions">
          <a href="/shop/{[{ product.slugs }]}">
            <div class="imgcont">
              <img src="{[{ amazonlink }]}/uploads/productimage/{[{ product.productid }]}/{[{ product.filename }]}" alt="" class="full-width">
            </div>
            <p class="gray-text text-left">{[{ product.name }]}</p>
            <p class="gray-text text-left">{[{ product.category }]}</p>
          </a>
        </div>  
      </div>
      
      <h4 class="text-left">Share your product review!</h4>
      <div id="disqus_thread"></div>
      <script>
          /**
           *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
           *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
           */
          /*
          var disqus_config = function () {
              this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
              this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
          };
          */
          (function() {  // DON'T EDIT BELOW THIS LINE
              var d = document, s = d.createElement('script');
              
              s.src = '//bnbtest.disqus.com/embed.js';
              
              s.setAttribute('data-timestamp', +new Date());
              (d.head || d.body).appendChild(s);
          })();
      </script>
      <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
    </div>
    <div class="col-sm-6 item wrapper-lg-vw">
      <div class="row">
        <div class="col-sm-12 name">{[{ product.name }]}</div>
        <div class="col-sm-12 price">{[{ product.price | currency }]}</div>
        <div class="col-sm-12 bl img-sm fv">
          <div class="shell shell-hover" ng-repeat="img in product.images" ng-click="setimg(img.filename)">
            <img src="{[{ amazonlink }]}/uploads/productimage/{[{ product.productid }]}/{[{ img.filename }]}" class="responsive-img full-width"/>
          </div>
        </div>

        <div class="col-sm-12">
          <br>
          <div class="in-bl valign-m" style="font-size:20px">Quantity</div>
          <div class="shop-select in-bl valign-m">
            <select class="form-control" ng-model="qty">
              <option ng-repeat="avquantity in product.avquantity">{[{ avquantity }]}</option>
            </select>
          </div>
        </div>

        <div class="col-sm-12 mv-c">
          <br>
          <a class="btn btn-orange" style="min-width:200px;" ng-click="addtocart(qty)">add to cart</a>
          <br><br>
          <div class="sc-btn fv">
            <a target="_blank" href="https://www.facebook.com/dialog/share?app_id=424095004448491&display=popup&href={[{BaseURL}]}/shop/{[{slugs}]}&redirect_uri={[{BaseURL}]}/shop/{[{slugs}]}">
              <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
              </span>
            </a>
            <a target="_blank" href="https://plus.google.com/share?url={[{BaseURL}]}/shop/{[{slugs}]}">
              <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
              </span>
            </a>
            <a target="_blank" href="https://twitter.com/intent/tweet?text=Body and Brain Shop %3A {[{product.name }]} %0A%0A {[{BaseURL}]}/shop/{[{slugs}]}">
              <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
              </span>
            </a>
            <a target="_blank" href="">
              <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-pinterest fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </div>
        </div>

        <div class="col-sm-12 description">
          <p>{[{ product.shortdesc }]}</p>
          <p ng-bind-html="product.description"></p>
        </div>
      </div>
    </div>

  </div>
</div>