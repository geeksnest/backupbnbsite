<div class="ng-cloak minheight">
	<div class="bag">
		<div class="bag">
			<div class="alert alert-success" role="alert">
				<h3>Thank you for your order!</h3>
				<p>When the order is being processed, you will receive an email with details and confirmation.</p>
			</div>
		</div>
	</div>
</div>