<div class="ng-cloak mbrship" ng-controller="shopMembershipCtrl">

  <div class="centermainslider shop-swiper">
    <div class="swiper-center">
      <div class="swiper-wrapper">
       {% for img in sliderimages %}
        <div class="swiper-slide"><img src="{{amazonlink}}/uploads/slidernbanner/{{img.image}}" style="width: auto; height: 434px; top: 0px;left: -64.778px;" /></div>
       {% endfor %}
      </div>
      <!-- Add Pagination -->
      <div class="swiper-pagination" style="position:relative; bottom:30px;"></div>
    </div>
  </div>

  <div class="centermobileslider" style="display:none"> <!-- slider row -->
    <div class="swiper-containermobile">
      <div class="swiper-wrapper">
       {% for img in sliderimages %}
       <div class="swiper-slide"><img class="my-image" src="{{amazonlink}}/uploads/slidernbanner/{{img.image}}" width="100%" /></div>
       {% endfor %}
      </div>
    </div>
  </div> <!-- end of slider row -->

  <div class="content">
    <div class="col-md-9">
      <div class="header text-center">Classes & Memberships</div>

      <div class="classes">
        <div class="col-md-6">
          <h1 class="text-right text-bold">STUDIO</h1>
          <h3 class="text-right">MEMBERSHIPS | CLASSES</h3>
          <h5 class="text-right">(Includes Online Membership & Classes)</h5>
          <input type="text" class="form-control" placeholder="Enter Zipcode">
          <button type="button" class="btn btn-orange">Find a Center Near You</button>
        </div>
        <div class="col-md-6">
          <h1 class="text-right text-bold">ONLINE</h1>
          <h3 class="text-right">MEMBERSHIPS | CLASSES</h3>
          <button type="button" class="btn btn-orange" style="width:147.844px!important; margin-top: 130px!important;">Start Class Now</button>
        </div>
      </div>

      <div class="header text-center">WORKSHOPS</div>

      {% for index, workshop in workshops %}
        <div class="col-md-12 wkshops">
          {% if index is even %}
            <div class="text-right">
              <h2>{{ workshop.title | upper }}</h2>
              <p>{{ workshop.description | upper }}</p>
              <button type="button" class="btn btn-orange">Tell Me More</button>
            </div>
          {% else %}
            <div class="text-left">
              <h2>{{ workshop.title | upper }}</h2>
              <p>{{ workshop.description | upper }}</p>
              <button type="button" class="btn btn-orange">Call To Action</button>
            </div>
          {% endif %}
        </div>
      {% endfor %}

    </div>
    <div class="col-md-3 eventsched">
      <div class="header text-center">Event Schedule</div>

      <tabset justified="true">
        <tab heading="Popular" active="tab.active">
          {% for centerevent in centereventspopular %}
            <div class="col-sm-12 hiddenoverflow">
              <div class="calendar text-center pull-left">
                <div class="month">{{ date('M', strtotime(centerevent.edate)) }}</div>
                <div class="date">{{ date('d', strtotime(centerevent.edate)) }}</div>
                <div class="day2">{{ date('D', strtotime(centerevent.edate)) }}</div>
              </div>
              <div class="content pull-left">
                <div class="info">
                  <p class="title">{{ centerevent.etitle }}</p>
                  <p class="center">{{ centerevent.ecentertitle ~ "," ~ centerevent.ecenterstate }}</p>
                </div>
                <div class="btn-cont">
                  <button class="btn btn-block btn-orange">Sign Up</button>
                  <p class="text-center">2 Remaining</p>
                </div>
              </div>
              <div class="bottom-txt"><a href="#">More Dates & Locations</a></div>
            </div>
          {% endfor %}
        </tab>
        <tab heading="By Date">
          {% for centerevent in centereventspopular %}
            <div class="col-sm-12 hiddenoverflow">
              <div class="calendar text-center pull-left">
                <div class="month">{{ date('M', strtotime(centerevent.edate)) }}</div>
                <div class="date">{{ date('d', strtotime(centerevent.edate)) }}</div>
                <div class="day2">{{ date('D', strtotime(centerevent.edate)) }}</div>
              </div>
              <div class="content pull-left">
                <div class="info">
                  <p class="title">{{ centerevent.etitle }}</p>
                  <p class="center">{{ centerevent.ecentertitle ~ "," ~ centerevent.ecenterstate }}</p>
                </div>
                <div class="btn-cont">
                  <button class="btn btn-block btn-orange">Sign Up</button>
                  <p class="text-center">2 Remaining</p>
                </div>
              </div>
              <div class="bottom-txt"><a href="#">More Dates & Locations</a></div>
            </div>
          {% endfor %}
        </tab>
        <tab heading="Search" ng-click="openSearch()">
          <div class="col-sm-12 hiddenoverflow">
            <div class="pull-right">
              <form class="form-inline" ng-submit="openSearch(keyword)">
                <div class="form-group">
                  <input type="text" class="form-control" id="exampleInputName2" placeholder="Search" ng-model="keyword">
                </div>
                <button type="submit" class="btn btn-orange">Search</button>
              </form>
            </div>
          </div>
          <div class="col-sm-12 hiddenoverflow" ng-repeat="event in events">
            <div class="calendar text-center pull-left">
              <div class="month">{[{ event.edate | date: 'MMM' }]}</div>
              <div class="date">{[{ event.edate | date: 'dd' }]}</div>
              <div class="day2">{[{ event.edate | date: 'EEE' }]}</div>
            </div>
            <div class="content pull-left">
              <div class="info">
                <p class="title">{[{ event.etitle }]}</p>
                <p class="center">{[{ event.ecentertitle + "," + event.ecenterstate }]}</p>
              </div>
              <div class="btn-cont">
                <button class="btn btn-block btn-orange">Sign Up</button>
                <p class="text-center">2 Remaining</p>
              </div>
            </div>
            <div class="bottom-txt"><a href="#">More Dates & Locations</a></div>
          </div>
        </tab>
      </tabset>

    </div>

  </div>
</div>
