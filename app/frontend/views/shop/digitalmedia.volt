<div ng-controller="ShopCtrl" class="ng-cloak">

  <div class="centermainslider shop-swiper">
    <div class="swiper-center">
      <div class="swiper-wrapper">
       {% for img in sliderimages %}
        <div class="swiper-slide"><img src="{{amazonlink}}/uploads/slidernbanner/{{img.image}}" style="width: auto; height: 434px; top: 0px;left: -93.5652px;" /></div>
       {% endfor %}
      </div>
      <!-- Add Pagination -->
      <div class="swiper-pagination" style="position:relative; bottom:30px;"></div>
    </div>
  </div>

  <div class="centermobileslider" style="display:none"> <!-- slider row -->
    <div class="swiper-containermobile">
      <div class="swiper-wrapper">
       {% for img in sliderimages %}
       <div class="swiper-slide"><img class="my-image" src="{{amazonlink}}/uploads/slidernbanner/{{img.image}}" width="100%" /></div>
       {% endfor %}
      </div>
    </div>
  </div> <!-- end of slider row -->

  <input type="text" class="shop-search-input" placeholder="Search" ng-show="searchinp">
  <div class="">

    <div class="shop-menu-active">
      Featured Items
    </div>

    <div class="shop-menu">
      <ul>
        <li ng-click="filter({filter: null})" class="featured">Featured Items</li>
        <li ng-repeat="featured in featured" ng-class="{ 'active focus' : (featured.category == active.category)}">
          <a href="" ng-click="filter({category : featured.category, filter : 'category'}, $event)">{[{ featured.category }]} <span ng-if="featured.subcategories.length > 0">&#10095;</span></a>
          <div class="sub-layer1" ng-if="featured.subcategories.length > 0">
            <!-- <span class="xdtri">&xdtri;</span> -->
            <ul>
              <li ng-repeat="subcategory in featured.subcategories" ng-class="{ 'active focus' : ((featured.category == active.category) && (subcategory.subcategory == active.subcategory))}">
                <a href="" ng-click="filter({category : featured.category, subcategory: subcategory.subcategory, filter : 'subcategory'})">{[{ subcategory.subcategory }]} <span ng-if="subcategory.types.length > 0">&#10095;</span></a>
                <div class="sub-layer2" ng-if="subcategory.types.length > 0">
                  <!-- <span class="xdtri">&xdtri;</span> -->
                  <ul>
                    <li ng-repeat="type in subcategory.types" ng-click="filter({category : featured.category, subcategory: subcategory.subcategory, type: type.type, filter : 'type'})" ng-class="{ 'active' : ((featured.category == active.category) && (subcategory.subcategory == active.subcategory) && (type.type == active.type))}">{[{ type.type }]}</li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </li>
      </ul>

      <a href="#" class="shop-search" ng-click="search()"><i class="fa fa-search fa-2x"></i></a>
    </div>

  </div>
  <!-- {[{ active.category }]}
  {[{ active.subcategory }]} -->
  <div class="prod-list hiddenoverflow" vertilize-container>
    <div class="col-sm-4 col-xs-6 item prod-ld" ng-repeat="product in products">
      <div vertilize>
        <a class="b-link" href="shop/products/{[{product.slugs}]}">
          <div class="dv-prod-img">
            <img src="{[{ amazonlink }]}/uploads/productimage/{[{ product.productid }]}/{[{ product.filename }]}" class="responsive-img rnd-border"/>
          </div>
          <div class="prod-name">{[{ product.name }]}</div>
          <div class="prod-type dm">
            <span ng-repeat="cat in product.category">{[{ $index == 0 ? cat.category : ", " + cat.category }]}</span>
            <!-- <span class="pull-right new">New!</span> -->
          </div>
          <div class="prod-price">{[{ product.price | currency }]}</div>
          <div class="prod-rating">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            131
          </div>

        </a>
        <div class="prod-btn">
          <button class="btn btn-orange btn-block">Purchase</button>
        </div>
      </div>
    </div>
  </div>
</div>
