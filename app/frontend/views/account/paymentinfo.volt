<div class="wrapper-lg-vw" ng-if="member.id">
  <div class="row">
    <div class="col-sm-12">
      <span class="sitemap">
        <a href="{{url_account}}" class="b-link">YOUR ACCOUNT</a> >
        YOUR PROFILE >
        PAYMENT INFO</span>
    </div>

    <div class="col-sm-12 mg-sm-v">
      <label class="font-bold font-md">Your Profile</label>
      <hr>
    </div>

    <div class="col-sm-3 col-md-2" id="profilenav">
      <span class="toggle mv">
        <button id="profilenav_switch" type="button"><i class="fa fa-caret-right text-danger font-md"></i></button>
      </span>
      <div class="heading mv">
        <label>Profile Navigation</label>
      </div>
      <div><a class="<?php echo $profileslugs == 'personalinfo' ? 'font-bold b-link':'b-link'; ?>" href="{{url_personalinfo}}">Personal Info</a></div>
      <div><a class="<?php echo $profileslugs == 'resetpassword' ? 'font-bold b-link':'b-link'; ?>" href="{{url_resetpassword}}">Reset Password</a></div>
      <div><a class="<?php echo $profileslugs == 'paymentinfo' ? 'font-bold b-link':'b-link'; ?>" href="{{url_paymentinfo}}">Payment Info</a></div>
      <div><a class="<?php echo $profileslugs == 'publicprofile' ? 'font-bold b-link':'b-link'; ?>" href="{{url_publicprofile}}">Public Profile</a></div>
    </div>

    <div id="profilebody" class="col-sm-9 col-md-10">
        <div class="row">
          <div class="col-sm-12">
              <span class="font-bold">Payment Info</span>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <div class="paymentinfo panel">
              <div class="left"><img src="/img/creditcards/visa.png"/></div>
              <div class="right">
                <div><b>Visa ****4321</b></div>
                <div><span class="text-xs">Expires: 08/2016</span></div>
                <br>
                <div><b>Neil Male</b></div>
                <div>123 GOOD STREET</div>
                <div>LOS ANGELEST, CA 91243</div>
                <div>Phone: 123-456-7890</div>
              </div>
            </div>
          </div>

          <div class="col-sm-12 col-md-6">
            <div class="paymentinfo panel">
              <div class="left"><img src="/img/creditcards/amex.png"/></div>
              <div class="right">
                <div><b>American Express ****4321</b></div>
                <div><span class="text-xs">Expires: 08/2016</span></div>
                <br>
                <div><b>Neil Male</b></div>
                <div>123 GOOD STREET</div>
                <div>LOS ANGELEST, CA 91243</div>
                <div>Phone: 123-456-7890</div>
              </div>
            </div>
          </div>

          <div class="col-sm-12 col-md-6">
            <div class="paymentinfo panel">
              <div class="left"><img src="/img/creditcards/mastercard.png"/></div>
              <div class="right">
                <div><b>Mastercard ****4321</b></div>
                <div><span class="text-xs">Expires: 08/2016</span></div>
                <br>
                <div><b>Neil Male</b></div>
                <div>123 GOOD STREET</div>
                <div>LOS ANGELEST, CA 91243</div>
                <div>Phone: 123-456-7890</div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-1">
              <button type="button" class="btn btn-md btn-info dker">Add New Credit Card</button>
          </div>
        </div>
    </div>
  </div>
</div>

{#login and signup portal if not logged in.#}
<div ng-include="'/fe/tpl/loginsignup.html'"></div>
