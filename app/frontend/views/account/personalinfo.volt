<div class="wrapper-lg-vw" ng-if="member.id" ng-controller="personalinfoCtrl">
  <div class="row">
    <div class="col-sm-12">
      <span class="sitemap">
        <a href="{{url_account}}" class="b-link">YOUR ACCOUNT</a> >
        YOUR PROFILE
      </span>
    </div>

    <div class="col-sm-12 mg-sm-v">
      <label class="font-bold font-md">Your Profile</label>
      <hr>
    </div>

    <div class="col-sm-3 col-md-2" id="profilenav">
      <span class="toggle mv">
        <button id="profilenav_switch" type="button"><i class="fa fa-caret-right text-danger font-md"></i></button>
      </span>
      <div class="heading mv">
        <label>Profile Navigation</label>
      </div>
      <div><a class="<?php echo $profileslugs == 'personalinfo' ? 'font-bold b-link':'b-link'; ?>" href="{{url_personalinfo}}">Personal Info</a></div>
      <div><a class="<?php echo $profileslugs == 'resetpassword' ? 'font-bold b-link':'b-link'; ?>" href="{{url_resetpassword}}">Reset Password</a></div>
      <div><a class="<?php echo $profileslugs == 'paymentinfo' ? 'font-bold b-link':'b-link'; ?>" href="{{url_paymentinfo}}">Payment Info</a></div>
      <div><a class="<?php echo $profileslugs == 'publicprofile' ? 'font-bold b-link':'b-link'; ?>" href="{{url_publicprofile}}">Public Profile</a></div>
    </div>

    <div id="profilebody" class="col-sm-9 col-md-10">
        <div class="row">
          <div class="col-sm-12">
              <span class="font-bold">Personal Info</span>
          </div>
        </div>
      <fieldset ng-disabled="isBusy">
        <form id="personalinfoForm" ng-submit="saveChanges(member)">
          <div class="row">
            <div class="col-sm-3">First Name:</div>
            <div class="col-sm-4"><input id="firstname" type="text" class="form-control" ng-model="member.firstname" required/></div>
          </div>
          <div class="row">
            <div class="col-sm-3">Last Name:</div>
            <div class="col-sm-4"><input id="lastname" type="text" class="form-control" ng-model="member.lastname" required/></div>
          </div>
          <div class="row">
            <div class="col-sm-3">Username:</div>
            <div class="col-sm-4"><input id="username" type="text" class="form-control" ng-model="member.username" required/></div>
          </div>
          <div class="row">
            <div class="col-sm-3">Email Address:</div>
            <div class="col-sm-4"><input id="email" type="email" class="form-control" ng-model="member.email" required/></div>
          </div>
          <div class="row">
            <div class="col-sm-3">Date of birth:</div>
            <div class="col-sm-4">2/8/1995</div>
          </div>
          <div class="row">
            <div class="col-sm-3">Gender:</div>
            <div class="col-sm-4">Male</div>
          </div>
          <div class="row">
            <div class="col-sm-3">Location:</div>
            <div class="col-sm-4">United States</div>
          </div>
          <div class="row">
            <div class="col-sm-3">Zip:</div>
            <div class="col-sm-4">12345</div>
          </div>
          <div class="row">
            <div class="col-sm-3">Profile Picture:</div>
            <div class="col-sm-4">

              <div style="width:150px">
                <img src="/img/a10.jpg" style="width:100%; height:170px;">
                <span style="display:block; font-size:11px; text-align:center; background:rgba(0,0,0,.7); height:30px; color:white">
                  Click to change <br> profile image
                </span>
              </div>

            </div>
          </div>
          <div class="row">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-md btn-info">Save changes</button>
            </div>
          </div>
        </form>
      </fieldset>
    </div>

  </div>
</div>

{#login and signup portal if not logged in.#}
<div ng-include="'/fe/tpl/loginsignup.html'"></div>
