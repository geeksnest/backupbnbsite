<style>
  #mainfooter {
    position:fixed;
    bottom:-1px;
    width:100%;
  }
</style>

<div class="wrapper-lg-vw" ng-if="member.id">
  <div class="row">
    <div class="col-sm-12">
      <span class="sitemap">
        <a href="{{url_account}}" class="b-link">YOUR ACCOUNT</a> >
        YOUR PROFILE >
        RESET PASSWORD
      </span>
    </div>

    <div class="col-sm-12 mg-sm-v">
      <label class="font-bold font-md">Your Profile</label>
      <hr>
    </div>

    <div class="col-sm-3 col-md-2" id="profilenav">
      <span class="toggle mv">
        <button id="profilenav_switch" type="button"><i class="fa fa-caret-right text-danger font-md"></i></button>
      </span>
      <div class="heading mv">
        <label>Profile Navigation</label>
      </div>
      <div><a class="<?php echo $profileslugs == 'personalinfo' ? 'font-bold b-link':'b-link'; ?>" href="{{url_personalinfo}}">Personal Info</a></div>
      <div><a class="<?php echo $profileslugs == 'resetpassword' ? 'font-bold b-link':'b-link'; ?>" href="{{url_resetpassword}}">Reset Password</a></div>
      <div><a class="<?php echo $profileslugs == 'paymentinfo' ? 'font-bold b-link':'b-link'; ?>" href="{{url_paymentinfo}}">Payment Info</a></div>
      <div><a class="<?php echo $profileslugs == 'publicprofile' ? 'font-bold b-link':'b-link'; ?>" href="{{url_publicprofile}}">Public Profile</a></div>
    </div>

    <div id="profilebody" class="col-sm-9 col-md-10">
        <div class="row">
          <div class="col-sm-12">
              <span class="font-bold">Reset Password</span>
          </div>
        </div>
      <fieldset ng-disabled="isBusy" ng-controller="resetpasswordCtrl">
        <form id="resetpassword" name="resetpasswordForm" ng-submit="saveChanges(password)">
          <div class="row">
            <div class="col-sm-3 col-md-2">Current Password:</div>
            <div class="col-sm-4 col-md-3">
              <input id="currentpassword" type="password" class="form-control" ng-model="password.current" required/>
              <center><u><a href="#" ng-click="forgotPassword(member)">Forgot your password?</a></u></center>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-3 col-md-2">New Password:</div>
            <div class="col-sm-4 col-md-3">
              <input id="newpassword" type="password" class="form-control" ng-model="password.new" pattern=".{8,50}" title="8-50 characters" required/>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-3 col-md-2">Confirm New Password:</div>
            <div class="col-sm-4 col-md-3">
              <input id="confirmnewpassword" name="confirmnewpassword" type="password" class="form-control" ng-model="password.confirm" 
                      match="password.new" match-ignore-empty="true" required/>
              <span ng-show="resetpasswordForm.confirmnewpassword.$error.match" class="danger">Please check and match your new password.</span>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-10">
                <button type="submit" class="btn btn-md btn-info">Save changes</button>
            </div>
          </div>
        </form>
      </fieldset>
    </div>

  </div>
</div>

<script type="text/ng-template" id="forgotpassword">
  <div ng-include="'/fe/tpl/forgotpassword.html'"></div>
</script>

{#login and signup portal if not logged in.#}
<div ng-include="'/fe/tpl/loginsignup.html'"></div>
