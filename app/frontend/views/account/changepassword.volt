<style>
  #mainfooter {
    position:fixed;
    bottom:-1px;
    width:100%;
  }
</style>

<div class="wrapper-lg-vw" ng-controller="changepasswordCtrl">
  <div class="row">
    <div class="col-sm-offset-4 col-sm-4">

        <form name="changepasswordForm" ng-submit="saveChanges(password)" ng-hide="isBusy || isSuccess">
          <div class="panel">
            <div class="panel-heading font-bold text-sm">Change Password Request</div>
            <div class="panel-body">
              <div class="pad-sm-v">
                New Password
                <input id="newpassword" type="password" class="form-control" ng-model="password.new" pattern=".{8,50}" title="8-50 characters" required/>
              </div>
              <div class="pad-sm-v">
                Confirm New Password
                <input id="confirmnewpassword" name="confirmnewpassword" type="password" class="form-control" ng-model="password.confirm" 
                match="password.new" match-ignore-empty="true" required/>
                <span ng-show="changepasswordForm.confirmnewpassword.$error.match" class="danger">Please check and match your new password.
                </span>
              </div>
              <div class="pad-sm-v">
                <button type="submit" class="btn btn-md btn-info btn-block">Save changes</button>
              </div>
            </div>
          </div>
        </form>

        <div class="text-lg align-c pad-md">
          <div ng-show="isBusy">
            <i class="fa fa-circle-o-notch fa-spin"></i> <br>
            Please wait
          </div>
          <div ng-show="isSuccess">
            Password successfully changed. You will redirect to homepage after 5 seconds.
          </div>
        </div>

    </div>
    
  </div>
</div>
