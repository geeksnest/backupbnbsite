<div class="wrapper-lg-vw" ng-if="member.id">
  <div class="row">
    <div class="col-sm-12">
      <span class="sitemap">
        <a href="{{url_account}}" class="b-link">YOUR ACCOUNT</a> >
        YOUR MEDIA >
        MUSIC</span>
    </div>

    <div class="col-sm-12 mg-sm-v">
      <label class="font-bold font-md">Your Media</label>
      <hr>
    </div>

    <div class="col-sm-3 col-md-2" id="medianav">
      <span class="toggle mv">
        <button id="medianav_switch" type="button"><i class="fa fa-caret-right text-danger font-md"></i></button>
      </span>
      <div class="heading mv">
        <label>Media Navigation</label>
      </div>
      <div><a class="<?php echo $mediaslugs == 'courses' ? 'font-bold b-link':'b-link'; ?>" href="{{url_courses}}">Courses</a></div>
      <div><a class="<?php echo $mediaslugs == 'videos' ? 'font-bold b-link':'b-link'; ?>" href="{{url_videos}}">Videos</a></div>
      <div><a class="<?php echo $mediaslugs == 'music' ? 'font-bold b-link':'b-link'; ?>" href="{{url_music}}">Music</a></div>
    </div>

    <div id="mediabody" class="col-sm-9 col-md-10">
        <div class="row">
          <div class="col-sm-12">
              <span class="font-bold">Music</span>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th colspan="2">Name</th>
                    <th>Type</th>
                    <th>Purchased</th>
                    <th>Expiration Date</th>
                    <th>View Details</th>
                  </tr>
                </thead>
                <tbody >
                  <tr>
                    <td style="width:150px;">
                      <img src="/img/c2.jpg" style="width:150px; height:85px;"/>
                    </td>
                    <td>
                      <div class="bold test">Dahn Mu Do Introduction</div>
                      <span class="text-xs">Change Your Energy</b>
                    </td>
                    <td>Video</td>
                    <td>8/10/15</td>
                    <td>8/10/16</td>
                    <td>View Receipt #1234567890</td>
                  </tr>
                  <tr>
                    <td style="width:150px;">
                      <img src="/img/c3.jpg" style="width:150px; height:85px;"/>
                    </td>
                    <td>
                      <div class="bold">Dahn Mu Do Introduction</div>
                      <span class="text-xs">Body & Brain</b>
                    </td>
                    <td>Video</td>
                    <td>8/10/15</td>
                    <td>8/10/16</td>
                    <td>View Receipt #1234567890</td>
                  </tr>
                  <tr>
                    <td style="width:150px;">
                      <img src="/img/c4.jpg" style="width:150px; height:85px;"/>
                    </td>
                    <td>
                      <div class="bold">Dahn Mu Do Introduction</div>
                      <span class="text-xs">Change Your Energy</b>
                    </td>
                    <td>Video</td>
                    <td>8/10/15</td>
                    <td>8/10/16</td>
                    <td>View Receipt #1234567890</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

{#login and signup portal if not logged in.#}
<div ng-include="'/fe/tpl/loginsignup.html'"></div>
