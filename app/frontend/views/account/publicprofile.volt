<div class="wrapper-lg-vw" ng-if="member.id">
  <div class="row">
    <div class="col-sm-12">
      <span class="sitemap">
        <a href="{{url_account}}" class="b-link">YOUR ACCOUNT</a> >
        YOUR PROFILE >
        PUBLIC PROFILE</span>
    </div>

    <div class="col-sm-12 mg-sm-v">
      <label class="font-bold font-md">Your Profile</label>
      <hr>
    </div>

    <div class="col-sm-3 col-md-2" id="profilenav">
      <span class="toggle mv">
        <button id="profilenav_switch" type="button"><i class="fa fa-caret-right text-danger font-md"></i></button>
      </span>
      <div class="heading mv">
        <label>Profile Navigation</label>
      </div>
      <div><a class="<?php echo $profileslugs == 'personalinfo' ? 'font-bold b-link':'b-link'; ?>" href="{{url_personalinfo}}">Personal Info</a></div>
      <div><a class="<?php echo $profileslugs == 'resetpassword' ? 'font-bold b-link':'b-link'; ?>" href="{{url_resetpassword}}">Reset Password</a></div>
      <div><a class="<?php echo $profileslugs == 'paymentinfo' ? 'font-bold b-link':'b-link'; ?>" href="{{url_paymentinfo}}">Payment Info</a></div>
      <div><a class="<?php echo $profileslugs == 'publicprofile' ? 'font-bold b-link':'b-link'; ?>" href="{{url_publicprofile}}">Public Profile</a></div>
    </div>

    <div id="profilebody" class="col-sm-9 col-md-10">
        <div class="row">
          <div class="col-sm-12">
              <span class="font-bold">Public Profile</span>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-3">Username</div>
          <div class="col-sm-9">Neildev</div>
        </div>
        <div class="row">
          <div class="col-sm-3">Profile Image:</div>
          <div class="col-sm-9">

            <div style="width:150px">
              <img src="/img/a10.jpg" style="width:100%; height:170px;">
              <span style="display:block; font-size:11px; text-align:center; background:rgba(0,0,0,.7); height:30px; color:white">
                Click to change <br> profile image
              </span>
            </div>

          </div>
        </div>
        <div class="row">
          <div class="col-sm-3">Badges Earned</div>
          <div class="col-sm-9">
            <button type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-fire"></i></button>
            <button type="button" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-leaf"></i></button>
            <button type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-star"></i></button>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-3">Signature</div>
          <div class="col-sm-9">
            <img src="/img/samplesign.png" style="border:2px solid gray"/>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-offset-3 col-sm-9">
              <button type="button" class="btn btn-md btn-info dker">Save Changes</button>
          </div>
        </div>
    </div>

  </div>
</div>

{#login and signup portal if not logged in.#}
<div ng-include="'/fe/tpl/loginsignup.html'"></div>
