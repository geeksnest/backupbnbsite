<style>
  #mainfooter {
    position:fixed;
    bottom  :-1px;
    width   :100%;
  }
</style>
<div class="wrapper-lg-vw" ng-if="member.id">
  <label class="font-bold font-md">Your Account</label>
  <hr>
  <div class="row mv-c">

    <div class="col-sm-6">

      <div class="row pd-sm-v">
        <div class="col-sm-3 icon-lg-wr">
          <img src="/img/profile.png" class="icon-lg" />
        </div>
        <div class="auto-flow">
          <label class="font-bold mgn-sm-mv">
            <a href="{{url_yourprofile}}" class="b-link">Your Profile</a>
          </label>
          <section>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris commodo lacus tincidunt leo finibus semper. Sed non sapien pretium, pulvinar magna eu, mattis est.</section>
        </div>
      </div>

      <div class="row pd-sm-v">
        <div class="col-sm-3 icon-lg-wr">
          <img src="/img/bag.png" class="icon-lg" />
        </div>
        <div class="auto-flow">
          <label class="font-bold mgn-sm-mv">Your Orders</label>
          <section>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris commodo lacus tincidunt leo finibus semper. Sed non sapien pretium, pulvinar magna eu, mattis est.</section>
        </div>
      </div>

    </div>

    <div class="col-sm-6">

      <div class="row pd-sm-v">
        <div class="col-sm-3 icon-lg-wr">
          <img src="/img/media.png " class="icon-lg" />
        </div>
        <div class="auto-flow">
          <label class="font-bold mgn-sm-mv">
            <a href="{{url_yourmedia}}" class="b-link">Your Media</a>
          </label>
          <section>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris commodo lacus tincidunt leo finibus semper. Sed non sapien pretium, pulvinar magna eu, mattis est.</section>
        </div>
      </div>

      <div class="row pd-sm-v">
        <div class="col-sm-3 icon-lg-wr">
          <img src="/img/membership.png" class="icon-lg" />
        </div>
        <div class="auto-flow">
          <label class="font-bold mgn-sm-mv">Your Membership & Workshops</label>
          <section>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris commodo lacus tincidunt leo finibus semper. Sed non sapien pretium, pulvinar magna eu, mattis est.</section>
        </div>
      </div>

    </div>

  </div>
</div>

{#login and signup portal if not logged in.#}
<div ng-include="'/fe/tpl/loginsignup.html'"></div>
