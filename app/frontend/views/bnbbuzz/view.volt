{% if newsdata %}

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<div class="bnbbuzzfullbanner">
  <?php if($newsdata->banner) {?>
    <img class="my-image lazy" data-original="{{amazonlink}}/uploads/newsimage/{{newsdata.banner}}" width="100%">
  <?php } else {?>
    <img data-original="/img/no-banner.png" width="100%" class="my-image lazy">
  <?php } ?>
</div>

<div class="bnbbuzzfullarticlecontainer">
<div class="col-sm-12 bnbbuzzfullarticledateandcategory">{{newsdata.categoryname}} | {{newsdata.date}}</div>
  <div class="col-sm-12 bnbbuzzfullarticletitle">{{newsdata.title}}</div>
  <div class="col-sm-12 bnbbuzzfullarticlecontent body-content">
    {{newsdata.body}}
  </div>

  <div class="col-sm-12"><a href="/bnb-buzz/1" class="back-press"> &lt; Back to B&B Buzz</a></div>

  <div class="col-sm-12 bnbbuzzfullarticlesocial">
    <a href="http://www.facebook.com/sharer/sharer.php?u={{BaseURL}}/bnb-buzz/view/{{newsdata.newsslugs}}&title={{newsdata.title}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/fb.png" class="story-social si-fb"/></a>
    <a href="https://plus.google.com/share?url={{BaseURL}}/bnb-buzz/view/{{newsdata.newsslugs}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/g+.png" class="story-social si-go"/></a>
    <a href="http://twitter.com/intent/tweet?status={{newsdata.title}}+{{BaseURL}}/bnb-buzz/view/{{newsdata.newsslugs}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/twitter.png" class="story-social si-tw"/></a>
    <a href="http://pinterest.com/pin/create/bookmarklet/?media=[MEDIA]&url={{BaseURL}}/bnb-buzz/view/{{newsdata.newsslugs}}&is_video=false&description={{newsdata.title}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/pint.png" class="story-social si-pi"/></a>
  </div>
  <div class="col-sm-12" ng-controller="bnbbuzzCtrl">

  <div class="fb-comments" data-href="http://bodynbrain.com/bnb-buzz/view/{{newsdata.newsslugs}}" data-numposts="10" width="100%"></div>

    {#<div class="view_discussion">
      <!-- <div id="disqus_thread"></div>
      <script type="text/javascript">
        var disqus_shortname = 'bodynbrain';
        (function() {
          var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
          dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
          (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
      </script>
      <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript> -->

      <div id="disqus_thread"></div>

      <script>
      /**
      * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
      * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
      */
      /*
      var disqus_config = function () {
      this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
      this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
      };
      */
      var disqus_shortname = 'bodynbrain';
      (function() { // DON'T EDIT BELOW THIS LINE
      var d = document, s = d.createElement('script');

      s.src = '//newbnb.disqus.com/embed.js';

      s.setAttribute('data-timestamp', +new Date());
      (d.head || d.body).appendChild(s);
      })();
      </script>
      <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
    </div>#}

  </div>
</div>

 {% endif %}
