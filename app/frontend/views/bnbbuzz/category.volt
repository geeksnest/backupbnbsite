<div class="secondarynav">
    <ul>
      <li><a href="/bnb-buzz/1">All</a></li>
      {% for categorylist in newscategory %}
        <li>
            {% if categorylist.categoryname == currentcategoryname %}
              <a href="/bnb-buzz/{{categorylist.categoryslugs}}/1" class="activetext">{{categorylist.categoryname}}</a>
            {% else %}
              <a href="/bnb-buzz/{{categorylist.categoryslugs}}/1">{{categorylist.categoryname}}</a>
            {% endif %}

        </li>
      {% endfor %}
    </ul>
  </div>

  <div class="secondarynavmobile" style="display:none;">
    <div class="secondarynavdropdown">
      <ul class="nav nav-pills">
        <li role="presentation" class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">

            <label class="activetext">{{currentcategoryname}}</label> <i class="fa fa-chevron-down"></i>

          </a>
          <ul class="dropdown-menu">
            <li><a href="/bnb-buzz/1">All</a></li>
            {% for categorylist in newscategory %}
            <li ><a href="/bnb-buzz/{{categorylist.categoryslugs}}/1">{{categorylist.categoryname}}</a></li>
            {% endfor %}
          </ul>
        </li>
      </ul>
    </div>
  </div>

  <div class="bnbbuzzindexcontainer" isotope-container>
    {% for newslist in datanewslist %}
      <div class="col-sm-4 item nopadding articlecontainer" isotope-item="isotope-item">
        <a class="a-blue" href="/bnb-buzz/view/{{newslist.newsslugs}}">
          <!-- <div class="articlebanner"><img class="my-image lazy" data-original="{{amazonlink}}/uploads/newsimage/{{newslist.banner}}" width="100%"></div> -->
          <div class="articlebanner zoom">
            <?php if($newslist->thumbnail) { ?>
              <div class="bg-div full-width" style="background-image: url('{{amazonlink}}/uploads/newsimage/{{newslist.thumbnail}}'); height: 100%!important;"></div>
            <?php } else {?>
              <div class="bg-div full-width" style="background-image: url('/img/no-banner.png'); height: 100%!important;"></div>
            <?php } ?>
            <!-- <img src="{{amazonlink}}/uploads/newsimage/{{newslist.banner}}" class="full-width" alt=""> -->
          </div>
          <div class="articledateandcategory">{{newslist.categoryname}} | {{newslist.date}}</div>
          <div class="articletitle">{{newslist.title}}</div>
        </a>
      </div>
    {% endfor %}
  </div>

  <div class="newspagination">
    <div class="paginationcontainer">
      <?php
      echo '<div class="myPagination">';
        echo '<span first_page_line></span><a href="/bnb-buzz/'.$currentcategory.'/1" class="first_page"></a>&nbsp';
        $pageLastNumberKey = $totalpage - 4;
        if($page >= $pageLastNumberKey) {
          if($page > 1)
          echo '<a href="/bnb-buzz/'.$currentcategory.'/'.($page - 1).'" class="back_pagination"></a>&nbsp';
          for($i=max(1, $pageLastNumberKey); $i<=max(1, min($totalpage,$page+4)); $i++)
          {
            if($i == $page)
            echo '<span class="active_page pagination_pages">'.$i.'</span>';
            else
            echo '<a href="/bnb-buzz/'.$currentcategory.'/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
          }
        }
        else {
          if($page > 1)
          echo '<a href="/bnb-buzz/'.$currentcategory.'/'.($page - 1).'" class="back_pagination"></a>&nbsp';
          for($i=max(1, $page); $i<=max(1, min($totalpage,$page+4)); $i++)
          {
            if($i == $page)
            echo '<span class="active_page pagination_pages">'.$i.'</span>';
            else
            echo '<a href="/bnb-buzz/'.$currentcategory.'/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
          }
        }
        if ($page < $totalpage)
        echo '<a href="/bnb-buzz/'.$currentcategory.'/'.($page + 1).'" class="next_pagination"></a>&nbsp;';
        echo '<a href="/bnb-buzz/'.$currentcategory.'/'.$totalpage.'" class="last_page"></a><span last_page_line></span>';
        echo"</div>";
        ?>
      </div>
    </div>
