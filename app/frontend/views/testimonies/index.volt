<div class="secondarynav">
  <ul>
    <li><a href="/{{about}}">Who Are We?</a></li>
    <li><a href="/{{classes}}">Classes</a></li>
    <li><a href="/{{workshops}}">Workshops</a></li>
    <li><a href="/{{wellness}}">Wellness at Work</a></li>
    <li class="active"><a href="/{{stories}}/1">Testimonials</a></li>
  </ul>
</div>

<div class="secondarynavmobile" style="display:none;">
  <div class="secondarynavdropdown">
    <ul class="nav nav-pills">
      <li role="presentation" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <label class="activetext">Testimonials</label> <i class="fa fa-chevron-down"></i>
        </a>
        <ul class="dropdown-menu">
          <li><a href="/{{about}}">Who Are We?</a></li>
          <li><a href="/{{classes}}">Classes</a></li>
          <li><a href="/{{workshops}}">Workshops</a></li>
          <li><a href="/{{wellness}}">Wellness at Work</a></li>
          <li><a href="/{{stories}}/1">Testimonials</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<div class="story-sss-wrapper">
  <div class="col-sm-3 container-img-ttml">&nbsp;</div>
  <div class="story-sss-content">
    <a class="btn btn-orange share-story" ng-controller="shareStoryCtrl" ng-click="shareStory()">share your story</a>
    <input ng-controller="searchStoryCtrl" type="text" class="form-control fl search search-ttml"
        ng-model="storyKeyword"
        ng-keyup="$event.keyCode == 13 && searchStory(storyKeyword)"
        placeholder="search" />
        <div class="sc-btn">
          <a href="http://www.facebook.com/sharer/sharer.php?u={{BaseURL}}/{{stories}}/{{page}}&title=BodyNBrain Success Stories" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/fb.png" class="story-social si-fb"/></a>
          <a href="https://plus.google.com/share?url={{BaseURL}}/{{stories}}/{{page}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/g+.png" class="story-social si-go"/></a>
          <a href="http://twitter.com/intent/tweet?status=BodyNBrain Success Stories+{{BaseURL}}/{{stories}}/{{page}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/twitter.png" class="story-social si-tw"/></a>
          <a href="http://pinterest.com/pin/create/bookmarklet/?media=[MEDIA]&url={{BaseURL}}/{{stories}}/{{page}}&is_video=false&description=BodyNBrain Success Stories" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/pint.png" class="story-social si-pi"/></i></a>
        </div>
  </div>
</div>

{% for story in storiesByTen %}
<div class="wrapper-lg-vw">
  <a class="story-list-a" href="/{{stories}}/details/{{story.ssid}}/{{story.subject}}">
    <div class="col-sm-3 container-img-ttml">
      <div class="img-ttml imageFill">
        <img src="{{storiesImg}}/{{story.photo}}" err-src="/img/unknownProfile.png"/>
      </div>
    </div>
    <div class="col-ttml-content">
      <div class="story-list-subject">{{story.subject}}</div>
      <div class="story-list-author">{{story.author}}</div>
      <div class="p-ttml">"{{story.metadesc}}"</div>
    </div>
  </a>
</div>
{% endfor %}

<div class="container-fluid wrapper-md-x">
  <div class="col-sm-12 newspagination">

    <div class="paginationcontainer story-pagination">
      <?php
      echo '<div class="myPagination">';
        $pageLastNumberKey = $totalpage - 4;

        if($page > 1) {
          echo '<a href="/'.$paginationUrl.'/1" class=""> << First </a>&nbsp';
          echo '<a href="/'.$paginationUrl.'/'.($page - 1).'" class=""> < </a>&nbsp';
        }

        if($page >= $pageLastNumberKey) {
          for($i=max(1, $pageLastNumberKey); $i<=max(1, min($totalpage,$page+4)); $i++) {
            if($i == $page)
              echo '<span class="active_page pagination_pages">'.$i.'</span>';
            else
              echo '<a href="/'.$paginationUrl.'/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
          }
        } else {
          for($i=max(1, $page); $i<=max(1, min($totalpage,$page+4)); $i++) {
            if($i == $page)
              echo '<span class="active_page pagination_pages">'.$i.'</span>';
            else
              echo '<a href="/'.$paginationUrl.'/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
          }
        }

        if($page + 4 < $totalpage) { echo '....&nbsp;' ; }

        if ($page < $totalpage) {
          echo '<a href="/'.$paginationUrl.'/'.($page + 1).'" class=""> > </a>&nbsp;';
          echo '<a href="/'.$paginationUrl.'/'.$totalpage.'" class=""> Last >> </a>';
        }

        echo " &nbsp; of " . $totalpage . " page(s)";
        ?>
      </div>
    </div>

    <div class="paginationcontainer story-pagination-mv">
      <?php
      echo '<div class="myPagination">';
        $pageLastNumberKey = $totalpage - 2;

        if($page > 1) {
          echo '<a href="/'.$paginationUrl.'/1" class=""> << First </a>&nbsp';
          echo '<a href="/'.$paginationUrl.'/'.($page - 1).'" class=""> < </a>&nbsp';
        }

        if($page >= $pageLastNumberKey) {
          for($i=max(1, $pageLastNumberKey); $i<=max(1, min($totalpage,$page+2)); $i++) {
            if($i == $page)
              echo '<span class="active_page pagination_pages">'.$i.'</span>';
            else
              echo '<a href="/'.$paginationUrl.'/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
          }
        } else {
          for($i=max(1, $page); $i<=max(1, min($totalpage,$page+2)); $i++) {
            if($i == $page)
              echo '<span class="active_page pagination_pages">'.$i.'</span>';
            else
              echo '<a href="/'.$paginationUrl.'/'.$i.'" class="other_pages pagination_pages">'.$i.'</a>&nbsp';
          }
        }

        if($page + 2 < $totalpage) { echo '....&nbsp;' ; }

        if ($page < $totalpage) {
          echo '<a href="/'.$paginationUrl.'/'.($page + 1).'" class=""> > </a>&nbsp;';
          echo '<a href="/'.$paginationUrl.'/'.$totalpage.'" class=""> Last >> </a>';
        }

        echo " &nbsp; of " . $totalpage . " page(s)";
        ?>
      </div>
    </div>

  </div>
</div>

<script type="text/ng-template" id="shareStoryTpl">
  <div ng-include="'/fe/tpl/shareStoryTpl.html'"></div>
</script>
