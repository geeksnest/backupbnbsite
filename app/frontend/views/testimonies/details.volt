<div class="secondarynav">
  <ul>
    <li><a href="/{{about}}">Who Are We?</a></li>
    <li><a href="/{{classes}}">Classes</a></li>
    <li><a href="/{{workshops}}">Workshops</a></li>
    <li><a href="/{{wellness}}">Wellness at Work</a></li>
    <li class="active"><a href="/{{stories}}/1">Testimonials</a></li>
  </ul>
</div>

<div class="secondarynavmobile" style="display:none;">
  <div class="secondarynavdropdown">
    <ul class="nav nav-pills">
      <li role="presentation" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <b>Testimonials</b> <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
          <li><a href="/{{about}}">Who Are We?</a></li>
          <li><a href="/{{classes}}">Classes</a></li>
          <li><a href="/{{workshops}}">Workshops</a></li>
          <li><a href="/{{wellness}}">Wellness at Work</a></li>
          <li class="active"><a href="/{{stories}}/1">Testimonials</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<div class="container-fluid wrapper-lg" style="border-top:1px solid #e7e7e7;">
  <div class="col-sm-12">
    <div class="img-ttml imageFill">
      <img src="{{storiesImg}}/{{story.photo}}" err-src="/img/unknownProfile.png" />
    </div>

    <div class="col-sm-12 nopad mg-md-x" ng-controller="searchStoryCtrl">
      <center>
        <a class="btn btn-orange share-story" ng-controller="shareStoryCtrl" ng-click="shareStory()">share your story</a>
        <input type="text"  ng-model="storyKeyword"
                            ng-controller="searchStoryCtrl"
                            ng-keyup="$event.keyCode == 13 && searchStory(storyKeyword)"
                            class="form-control fl search search-ttml"
                            style="display:inline-block;" />
      </center>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="pg-mini-title">{{story.author}}</div>
    <div class="pg-p-details body-content">
      {{story.details}}
    </div>
  </div>

  <div class="col-sm-12 txt-c">
    <br>
    <div class="sc-btn">
      <a href="http://www.facebook.com/sharer/sharer.php?u={{BaseURL}}/{{stories}}/details/{{story.ssid}}/{{story.subject}}&title={{story.subject}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/fb.png" class="story-social si-fb"/></a>
      <a href="https://plus.google.com/share?url={{BaseURL}}/{{stories}}/details/{{story.ssid}}/{{story.subject}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/g+.png" class="story-social si-go"/></a>
      <a href="http://twitter.com/intent/tweet?status={{story.subject}}+{{BaseURL}}/{{stories}}/details/{{story.ssid}}/{{story.subject}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/twitter.png" class="story-social si-tw"/></a>
      <a href="http://pinterest.com/pin/create/bookmarklet/?media=[MEDIA]&url={{BaseURL}}/{{stories}}/details/{{story.ssid}}/{{story.subject}}&is_video=false&description={{story.subject}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/img/pint.png" class="story-social si-pi"/></a>
    </div>
    <br><br>
    <a href="/{{stories}}/1"><strong>< Back to Testimonials</strong></a>
  </div>
</div>

<script type="text/ng-template" id="shareStoryTpl">
  <div ng-include="'/fe/tpl/shareStoryTpl.html'"></div>
</script>
