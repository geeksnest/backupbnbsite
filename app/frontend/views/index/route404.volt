<!DOCTYPE html>
<html lang="en" data-ng-app="app" >
<head>
  <title>Yoga Classes combining Tai Chi, Meditation | Body & Brain</title>
  <!-- <base href="/"> -->
  <meta name="description" content="Body & Brain offers dynamic classes in yoga, tai-chi, meditation, and energy healing for all types of ailments and stress. Beginners are welcome.">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" charset="utf-8">

  <!-- Facebook custom post preview -->
  <meta property="og:url"           content="http://www.your-domain.com/your-page.html" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Your Website Title" />
  <meta property="og:description"   content="Your description" />
  <meta property="og:image"         content="http://www.your-domain.com/path/image.jpg" />
  <!-- favicon -->
  <link rel="shortcut icon" href="/img/smalllogo.png">

  <!-- css -->
  <link type ="text/css" rel    ="stylesheet" href ="/vendors/bootstrap/dist/css/bootstrap.min.css" media       ="screen,projection"/>
  <link type ="text/css" rel    ="stylesheet" href ="/vendors/bootstrap/dist/css/bootstrap-theme.min.css" media ="screen,projection"/>
  <link type ="text/css" rel    ="stylesheet" href ="/fe/css/style.css" media                                   ="screen,projection"/>
  <link type ="text/css" rel    ="stylesheet" href ="/fe/css/pages.css" media                                   ="screen,projection"/>
  <link type ="text/css" rel    ="stylesheet" href ="/fe/css/home.css" media                                    ="screen,projection"/>
  <link type ="text/css" rel    ="stylesheet" href ="/vendors/Swiper/dist/css/swiper.min.css" media             ="screen,projection"/>
  <link rel  ="stylesheet" href ="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <style>
    html, body {
      height:100%;
      margin:0;
      padding:0;
    }
  </style>
</head>
<body ng-controller="MemberCtrl">
{#<body>#}

  <div id="loginreg" ng-show="signup==true || login==true" class="animate-if hide">
    <div class="row" id="signup" ng-if="signup">
      <div class="col-sm-2">
        <h3>Sign Up</h3>
      </div>
      <div class="col-sm-10" style="height: 35px;">
        <a href="" class="btn btn-link pull-left left" ng-click="loginbtn()">login</a>
        <span class="pull-left">|</span>
        <a href="" class="btn btn-link pull-left right">forgot my password</a>
        <div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {[{ perc }]}%;">
          </div>
        </div>
        <button type="button" class="close" data-dismiss="alert" ng-click="closebtn()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="col-sm-12 animate-hide" ng-hide="wizard!=1">
        <form class="form-inline">
          <div class="form-group">
            <input type="email" class="form-control" id="exampleInputName2" placeholder="email address" ng-model="member.email">
            <p ng-if="error.email!=undefined">{[{ error.email }]}</p>
          </div>
          <div class="form-group">
            <input type="password" class="form-control" id="exampleInputEmail2" placeholder="password" ng-model="member.password">
            <p ng-if="error.password!=undefined">{[{ error.password }]}</p>
          </div>
          <div class="form-group">
            <input type="password" class="form-control" id="exampleInputEmail2" placeholder="confirm password" ng-model="member.confpass">
            <p ng-if="error.confpass!=undefined">{[{ error.confpass }]}</p>
          </div>
          <button type="button" class="btn btn-bnb" ng-click="next(member)">{[{ btn }]}</button>
        </form>
      </div>

      <div class="col-sm-12 animate-hide" ng-hide="wizard!=2">
        <form class="form-inline">
          <div class="form-group">
            <input type="text" class="form-control" id="exampleInputName2" placeholder="first name" ng-model="member.firstname">
            <p ng-if="error.firstname!=undefined">{[{ error.firstname }]}</p>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="exampleInputEmail2" placeholder="last name" ng-model="member.lastname">
            <p ng-if="error.lastname!=undefined">{[{ error.lastname }]}</p>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="exampleInputEmail2" placeholder="phone number" ng-model="member.phoneno" only-digits>
            <p ng-if="error.phoneno!=undefined">{[{ error.phoneno }]}</p>
          </div>
          <button type="button" class="btn btn-bnb" ng-click="next(member)">next</button>
        </form>
      </div>

      <div class="col-sm-12 animate-hide" ng-hide="wizard!=3">
        <form class="form-inline">
          <div class="form-group">
            <select class="form-control" ng-model="member.state" ng-options="state.state_code for state in states" style="width:230px;" ng-change="changestate(member.state)"></select>
          </div>
          <div class="form-group">
            <select class="form-control" ng-model="member.center" ng-options="center.centertitle for center in centers" style="width:230px;"></select>
          </div>
          <div class="form-group">
            <div class="gray-text text-center" style="width:204px;margin-top:10px;">(optional, members only)</div>
          </div>
          <button type="button" class="btn btn-bnb" ng-click="next(member)">next</button>
        </form>
      </div>

    </div>

    <div class="row" id="login" ng-if="login">
      <div class="col-sm-2">
        <h3>Log In</h3>
      </div>
      <div class="col-sm-10">
        <a href="" class="btn btn-link pull-left left" ng-click="signupbtn()">sign up</a>
        <span class="pull-left">|</span>
        <a href="" class="btn btn-link pull-left right">forgot my password</a>

        <button type="button" class="close2 close" data-dismiss="alert" ng-click="closebtn()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="col-sm-12">
        <form class="form-inline">
          <div class="form-group">
            <input type="email" class="form-control" id="exampleInputName2" placeholder="email address" ng-model="member.email">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" id="exampleInputEmail2" placeholder="password" ng-model="member.password">
          </div>
          <button type="button" class="btn btn-bnb" ng-click="loginaction(member)" id="loginmember">login</button>
        </form>
      </div>
      <div class="col-sm-8">
        <p ng-if="alert!=null">{[{ alert }]}</p>
      </div>
      <div class="col-sm-4 sc">
        <p class="pull-left">sign in with: </p>
        <a href="" ng-click="logintwitter()"><img src="/img/socialmedia/twitter.png" alt="" class="pull-right"></a>
        <a href="" ng-click="logingplus()"><img src="/img/socialmedia/google.png" alt="" class="pull-right"></a>
        <a href="" ng-click="loginFb()"><img src="/img/socialmedia/fb.png" alt="" class="pull-right"></a>
      </div>
    </div>
  </div>

  <div class="wrapper">
    <nav id="homeNavbar" class="navbar navbar-default navbar-static-top">
        <div id="navHeader" class="navbar-header">
          <i id="navMenuSwitch" class="pull-left">
            <img src="/img/bnbmenu.png" />
          </i>
          <a class="navbar-brand" href="/">
            <img id="mainLogo" alt="Brand" src="/img/logo.png">
          </a>
        </div>
        <ul id="topMenu" class="nav navbar-nav">
          <li><a href="/{{about}}">about</a></li>
          <li><a href="/{{press}}/1">press</a></li>
          <li><a href="/{{mainnews}}/1">b&b buzz</a></li>
          <li><a href="/{{locations}}">locations</a></li>
          <li><a href="/{{shop}}">shop</a><span class="cartlogo"><a href="/bag"><img src="/img/cartlogo.png"><span class="cartlogocount ng-cloak">{[{ bagcount }]}</span></span></a></li>
        </ul>

        <a  id="tryaclass" href="/getstarted/starterspackage" type="button" class="btn btn-orange navbar-btn pull-right">try a class!</a>
        <div id="logsign" class="ng-cloak pull-right">
          <ul class="nav navbar-nav">
            <li ng-if="!member.id">
              <a href="#" ng-click="loginbtn()" id="login">log in</a>
              <span ng-if="!member.id">
                <a class="loginseparator">|</a>
              </span>
              <span ng-if="!member.id">
                <a href="#" ng-click="signupbtn()" ng-if="!member.id">sign up</a>
              </span>
            </li>
            {#<a href="#" ng-if="member.id">My account <span class="fa fa-caret-down"></span> </a>#}
            <li class="dropdown" ng-if="member.id">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My account <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Your Profile</a></li>
                <li><a href="#">Your Media</a></li>
                <li><a href="#">Your Orders</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Your Memberships</a></li>
                <li><a href="#">Your Workshops</a></li>
                <li role="separator" class="divider"></li>
                <li><span class="pull-left">Not {[{ member.firstname }]}?</span> <a href="#" ng-click="logout()">Sign Out</a></li>
              </ul>
            </li>
          </ul>
        </div>
    </nav>

    {#TOP NAVIGATION MOBILE VIEW#}
    <div id="topMenu-res">
      <i id="x">&times;</i>
      <div id="logsign-res" class="ng-cloak">
        <span>
          <a href="#" ng-click="loginbtn()" ng-if="!member.id" id="login">log in</a>
          <a href="#" ng-if="member.id">Hi, {[{ member.firstname }]}.</a>
        </span>
        <span>
          <a class="loginseparator">|</a>
        </span>
        <span>
          <a href="#" ng-click="signupbtn()" ng-if="!member.id">sign up</a>
          <a href="#" ng-click="logout()" ng-if="member.id" id="logout">logout</a>
        </span>
      </div>
      <div><a href="/{{about}}">about</a></div>
      <div><a href="/{{press}}/1">press</a></div>
      <div><a href="/{{mainnews}}/1">b&b buzz</a></div>
      <div><a href="/{{locations}}">locations</a></div>
    </div>
    {#END OF TOP NAVIGATION MOBILE VIEW#}

    <div class="body-container">
      <div class="content-box">
        <abbr>404</abbr>
        <div>
          The page you're looking <br>
          for can not be found. <br>
          <a class="btn btn-orange" href="/">home page</a>
        </div>
      </div>
    </div>

    <div id="footer">
      <div class="txt"><a class="" href="/{{founder}}">founder</a></div>
      <div class="txt"><a class="" href="/{{affiliates}}">affiliates</a></div>
      <div class="txt"><a class="" href="/{{faqs}}">FAQs</a></div>
      <div class="txt"><a class="" href="/{{franchising}}">franchising</a></div>
      <div class="txt a-contactus"  ng-controller="contactUsCtrl">
        <a class="" ng-click="openContactUs()">contact us</a>
      </div>

      <div id="rightcorner">
        <div class="txt"><a href="/{{terms}}">terms of use</a></div>
        <div class="txt divider"><a>|</a></div>
        <div class="txt"><a href="/{{privacy}}">privacy policy</a></div>
        <div class="logo">
          &copy; 2015 Body&Brain <a><img src="/img/smalllogo.png"></a>
        </div>
      </div>
    </div>
  </div> <!-- wrapper end -->

  <script type="text/ng-template" id="contactus">
    <div ng-include="'/fe/tpl/contactus.html'"></div>
  </script>

  <!-- Angularjs -->
  <script type="text/javascript" src="/vendors/jquery/dist/jquery.min.js"></script>
  <script type="text/javascript" src="/vendors/angular/angular.min.js"></script>
  <script type="text/javascript" src="/be/js/angular/ui-bootstrap-tpls.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-jwt/dist/angular-jwt.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-animate/angular-animate.min.js"></script>
  <script type="text/javascript" src="/vendors/a0-angular-storage/dist/angular-storage.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-facebook/lib/angular-facebook.js"></script>
  <script type="text/javascript" src="/vendors/angular-google-plus/dist/angular-google-plus.min.js"></script>
  <script type="text/javascript" src="/vendors/oauth-js/dist/oauth.min.js"></script>
  <script type="text/javascript" src="/fe/scripts/twitter.js"></script>
  <script type="text/javascript" src="/vendors/angular-google-maps/dist/angular-google-maps.min.js"></script>
  <script type="text/javascript" src="/vendors/lodash/dist/lodash.min.js"></script>
  <script type="text/javascript" src="/vendors/angularUtils-pagination/dirPagination.js"></script>
  <script type="text/javascript" src="/vendors/angular-vertilize/angular-vertilize.min.js"></script>
  <script type="text/javascript" src="/vendors/ng-file-upload/ng-file-upload.min.js"></script>
  <script type="text/javascript" src="/vendors/angularjs-toaster/toaster.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-moment/angular-moment.min.js"></script>
  <script type="text/javascript" src="/vendors/moment/min/moment.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-country-state-select/dist/angular-country-state-select.js"></script>
  <script type="text/javascript" src="/vendors/angular-uuid-service/angular-uuid-service.min.js"></script>
  <script type="text/javascript" src="/vendors/re-tree/re-tree.min.js"></script>
  <script type="text/javascript" src="/vendors/ng-device-detector/ng-device-detector.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-sanitize/angular-sanitize.min.js"></script>

  <!-- app, controllers, directives and factories -->
  <script type="text/javascript" src="/fe/scripts/app.js"></script>
  <script type="text/javascript" src="/fe/scripts/config.js"></script>
  <script type="text/javascript" src="/fe/scripts/controllers/member.js"></script>
  <script type="text/javascript" src="/fe/scripts/controllers/contactUsCtrl.js"></script>
  <script type="text/javascript" src="/fe/scripts/factory/factory.js"></script>
  <script type="text/javascript" src="/fe/scripts/factory/member.js"></script>
  <script type="text/javascript" src="/fe/scripts/factory/login.js"></script>
  <script type="text/javascript" src="/fe/scripts/directives/validations.js"></script>

  <!-- js -->
  <script type="text/javascript" src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js"></script>
  <script type="text/javascript" src="/fe/scripts/others/jquery-imagefill.js"></script>
  <script type="text/javascript" src="/fe/scripts/others/imagefill.js"></script>
  <script type="text/javascript" src="/fe/scripts/others/jquery.resizeimagetoparent.min.js"></script>
  <script type="text/javascript" src="/vendors/isotope/dist/isotope.pkgd.min.js"></script>
  <script type="text/javascript" src="/vendors/angular-isotope/dist/angular-isotope.min.js"></script>

  <script type="text/javascript">
    $("#topMenu-res").hide();

    $("#navMenuSwitch").click(function(){
      $("#topMenu-res").fadeToggle( "slow", "linear" );
      $("#topMenu-res").css("visibility","visible");
    })
    $('#x').click(function(){
      $("#topMenu-res").fadeOut( "slow", "linear" );
    })
  </script>

</body>
