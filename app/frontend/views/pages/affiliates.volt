<div class="pagebanner">
  {% if nobanner == false %}
    {% if slider == true %}

      <div class="swiper-banner">
        <div class="swiper-wrapper">
          {% for banner in pagebanners %}
            <div class="swiper-slide">
              {% if banner.url == null OR banner.url == "" %}
                <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" width="100%" />
              {% else %}
                <a href="{{banner.url}}" target="_blank">
                  <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" width="100%" />
                </a>
              {% endif %}
            </div>
          {% endfor %}
        </div>
        <div class="swiper-pagination"></div>
      </div>

    {% else %}

        {% if pagebanners[0].url == null OR pagebanners[0].url == "" %}
          <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
        {% else %}
          <a href="{{pagebanners[0].url}}" target="_blank">
            <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
          </a>
        {% endif %}

    {% endif %}
  {% else %}
    <img src="/img/banners/about-workshops.jpg" width="100%" />
  {% endif %}
  <div class="pageheader"> <?php echo $title; ?> </div>
</div>

<div class="wrapper-lg-vw" style="line-height: 32px;">
    <?php echo $body; ?>
</div>
