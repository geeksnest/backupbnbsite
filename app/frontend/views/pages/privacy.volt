<div class="secondarynav">
  <ul>
    <li><a href="/{{terms}}">Terms of Use</a></li>
    <li class="active"><a href="/{{privacy}}">Privacy Policy</a></li>
  </ul>
</div>

<div class="secondarynavmobile" style="display:none;">
  <div class="secondarynavdropdown">
    <ul class="nav nav-pills">
      <li role="presentation" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <label class="activetext">{{pagetitle}}</label> <i class="fa fa-chevron-down"></i>
        </a>
        <ul class="dropdown-menu">
          <li><a href="/{{terms}}">Terms of Use</a></li>
          <li><a href="/{{privacy}}">Privacy Policy</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<div class="container-fluid wrapper-lg-vw privacy" ng-controller="pgPrivacyCtrl">
  <div class="col-sm-12">
    <h4>Table of Contents</h4>
    <table>
      <tr ng-click="scrollTo('one')"><td>1.</td><td>Information Collected By Our Site</td></tr>
      <tr ng-click="scrollTo('two')"><td>2.</td><td>Personal Information</td></tr>
      <tr ng-click="scrollTo('three')"><td>3.</td><td>How We Use Your Personal Information</td></tr>
      <tr ng-click="scrollTo('four')"><td>4.</td><td>Discussions and Community Tools</td></tr>
      <tr ng-click="scrollTo('five')"><td>5.</td><td>Non-Personal Data</td></tr>
      <tr ng-click="scrollTo('six')"><td>6.</td><td>	Cookies</td></tr>
      <tr ng-click="scrollTo('seven')"><td>7.</td><td>Opt Out Procedures</td></tr>
      <tr ng-click="scrollTo('eight')"><td>8.</td><td>Reviewing or Changing Your Information</td></tr>
      <tr ng-click="scrollTo('nine')"><td>9.</td><td>Sharing of Your Information</td></tr>
      <tr ng-click="scrollTo('ten')"><td>10.</td><td>Testimonial Guidelines</td></tr>
      <tr ng-click="scrollTo('eleven')"><td>11.</td><td>Image and Photo Release Policy</td></tr>
      <tr ng-click="scrollTo('twelve')"><td>12.</td><td>How We Protect Your Personal Information</td></tr>
      <tr ng-click="scrollTo('thirteen')"><td>13.</td><td>Protection for Children</td></tr>
      <tr ng-click="scrollTo('fourteen')"><td>14.</td><td>Privacy Precaution Warning</td></tr>
      <tr ng-click="scrollTo('fifteen')"><td>15.</td><td>	Your Consent</td></tr>
      <tr ng-click="scrollTo('sixteen')"><td>16.</td><td>	Revisions to Our Privacy Policy</td></tr>
</table>
<br>
<h4>DAHNYOGA.COM PRIVACY POLICY</h4>
This DahnYoga.com Privacy Policy ("Privacy Policy") governs your use of the DahnYoga.com website the "site), which is owned by Body & Brain yoga & Health Centers, Inc. ("Body & Brain yoga"). We know how visitors like you value your privacy, and we have created this Privacy Policy to ensure that you understand our policies and procedures, what personal information you must provide if you wish to use certain portions of our Website and, ultimately, just how we use such personal information. This Privacy Policy is a part of and is incorporated into our DahnYoga.com Terms and Conditions of Use. By accessing or using our Website, you accept, without limitation or qualification, the terms of this Privacy Policy.
<p><h4 id="one">1.	Information Collected By Our Site</h4>
We collect two types of information from users of our Site: personal information described below; and non-personal information such as information about traffic patterns on our Site.
</p>
<p><h4 id="two">2.	Personal Information</h4>
When you interact with our Site, you may be asked to provide personally identifiable information, such as when you register and select a user name and password, when you make a purchase, or when you enter a contest or other promotion. We may ask you for certain personal information such as your name, address, e-mail address, or credit card number, in order to process your order, administer any contest, or send you promotional e-mails. Providing personal information in these instances is solely your choice; it is not a requirement that you provide such information, make purchases, or enter any contests to browse our Site
</p>
<p><h4 id="three">3.	How We Use Your Personal Information</h4>
We use the information collected on our Site for a variety of purposes, including, but not limited to, running the Site and contacting users. Our Site may also make available to other registered users information provided by you during registration, such as your screen name. We may use your information to communicate back to you, to update you on products, services and benefits, to personalize our Site for you, to contact you for market research or to provide you with marketing information, newsletters, or other information we think would be of particular interest. In addition. if you make a purchase on our Site, we may send you order and shipping confirmation ernails. We will always give you the opportunity to opt out of receiving such materials.
<br><br>
You can remove your e-mail address from our e-mail list at any time by following the procedures set forth in the "Opt Out Procedures" section below or by clicking on the "unsubscribe" link in every Body & Brain yoga e-mail. In addition, you can modify your information or change your preferences, as set forth in the "Reviewing or Changing Your Information" section below.
Information obtained through our Site may be intermingled with and used in conjunction with information obtained through sources other than our Site, including both offline and online sources.
</p>
<p><h4 id="four">4.	Discussions and Community Tools</h4>
Our Site may make product reviews, chat rooms, forums, bulletin boards, news groups and other community tools available to registered users and/or visitors. Please remember that any information that is disclosed in these areas becomes public information for other users to view and for Body & Brain yoga to use.
Please do not disclose any personally identifiable information in these publicly accessible areas of our Site. Please be considerate and respectful of others while using any chat rooms, forums or message boards to share your opinion.
</p>
<p><h4 id="five">5.	Non-Personal Data</h4>
In some cases, we may collect non-personal information. Examples of this type of information include the type of internet browser you are using, the type of computer operating system application software, and peripherals you are using and the domain name of the web site from which you linked to our Site. We use your information on an aggregated basis to do such things as operate and enhance our Site.
</p>
<p><h4 id="six">6.	Cookies</h4>
Certain features on our Site utilize cookie technology. A cookie is a small data file that certain web sites write to your hard drive when you visit them. A cookie file can contain various types of information, including a user ID that the site uses to track the pages you've visited. We may use cookies to enhance your experience on our Site, to determine user traffic patterns and for other purposes.
<br><br>
Most browsers are initially set up to accept cookies: however, you can reset your browser to refuse all cookies or indicate when a cookie is being sent or you can flush your browser of cookies from time to time. (Note: you may need to consult the help area of your browser application for instructions.) If you choose to disable your cookies setting or refuse to accept a cookie, however. you may not be able to access all areas of our Site.
</p>
<p><h4 id="seven">7.	Opt Out Procedures</h4>
You may opt out of receiving information from Body & Brain yoga and our Site. This opt out messaging will appear at the bottom of every email that is sent out If you no longer wish to take advantage of our Site or receive any form of direct contact from Body & Brain yoga or our Site, whether it is email, discounts, newsletters, or other promotional offers or materials, contact us.
</p>
<p><h4 id="eight">8.	Reviewing or Changing Your Information</h4>
In order to ensure that the information we maintain is accurate, Body & Brain yoga gives users the option to change or modify their information previously provided during registration. If you would like to change your information currently in our database please log in and click the "My Account" link on our Site.
</p>
<p><h4 id="nine">9.	Sharing of Your Information</h4>
Body & Brain yoga may share your personal information: (i) with third parties who are under obligations of confidentiality with Body & Brain yoga or with Body & Brain yoga's affiliates or subsidiaries, (ii) if Body & Brain yoga is required by law to do so, (hi) in the event of a transfer of ownership of Body & Brain yoga, merger or other similar transaction. or (iv) as otherwise set forth in this Privacy Policy. The following describes some of the ways that your personal information may be disclosed to third parties:
<br><br>
We may employ other third parties to perform services or functions on our behalf in order to improve our Site, merchandising, marketing and promotional efforts, communications or other services. Those third parties may include authorized contractors, consultants and other companies working with us (collectively, "agents"). They only have access to personal information needed to perform their functions, and they may not share any personal information with others or use it for any other purpose than providing or improving Body & Brain yoga's services and offerings.
<br><br>
We may share certain non-personal information with third parties for advertising, promotional and other purposes. For example, we may work with third party advertising companies, to serve and track our ads. These third parties may serve other cookies. Our advertising partners may use the non-personal information they collect from our Site, in the aggregate, to help us better market and serve our customers.
<br><br>
This Privacy Policy applies only to Body & Brain yoga's Site and does not address the practices of third parties who may collect your personal information. You may visit other web sites, through links on our Site, which may collect. use and share your personal information in accordance with their own privacy policies. The information practices of those linked websites are not covered by this Privacy Policy, and we encourage you to be very cautious before you disclose your personal information to others.
<br><br>
In order to provide you with the information, products or services which you have requested, we may share or transfer your personal information with our affiliates or subsidiaries, or third party agents acting on their behalf.
<br><br>
Body & Brain yoga may be obligated to cooperate with various law enforcement inquiries. Body & Brain yoga reserves the right to disclose or transfer personal information and non-personal information about you and your activities on our Site in order to comply with a legal requirement or request from law enforcement or other government officials, administrative agencies or third parties as we, in our sole discretion, determine necessary or appropriate for the administration of justice, or in connection with an investigation of fraud. intellectual property infringements or violations of any other law, rule or regulation, our DahnYoga.com Terms and Conditions of Use or other rules or policies of our Site, the rights of third parties, or an investigation of any other activity that may expose us or you to legal liability, or to investigate any suspected conduct which Body & Brain yoga in its sole discretion deems improper.
</p>
<p><h4 id="ten">10.	Testimonial Guidelines</h4>
Thank you for taking the time to share your experiences with Body & Brain yoga. Please let readers know how Body & Brain yoga practice has helped you. Be sure to share something about your background, including how long you have practiced and your condition prior to Body & Brain yoga. If possible, please provide a photo of yourself or allow us to use a photo of you that we may have on file. By clicking here you agree to our Photo Release Policy.
</p>
<p><h4 id="eleven">11.	Image and Photo Release Policy</h4>
You hereby grant permission to Body & Brain yoga to use your image on its Web site or in other official digital publications without further consideration, and you also grant Body & Brain yoga permission to use of a photograph or other image of you that they may otherwise have on file. You acknowledge Body & Brain yoga's right to crop or treat or discontinue use of such image at its discretion. You also understand that once your image is posted on Body & Brain yoga's Web site, the image can be downloaded. Therefore, you agree to indemnify and hold Body & Brain yoga and its affiliates or subsidiaries harmless from any and all related claims.
</p>
<p><h4 id="twelve">12.	How We Protect Your Personal Information</h4>
Providing a secure site is essential for your peace of mind and trust in Body & Brain yoga. We have installed encryption software conforming to the Secure Socket Layers (SSL) protocol to safeguard all of the information you send to us. All information is stored on our servers in a secure location. It is important for you to protect against unauthorized access to your password and to your computer. If your password is compromised, notify Customer Service at once.
</p>
<p><h4 id="thirteen">13.	Protection for Children</h4>
Our Site is not intended for users under the age of 18. Furthermore, we have no intention of collecting personally identifiable information from children (i.e., individuals under the age of 13). Where appropriate, we take reasonable measures to inform children not to submit such information to our Site or in response to advertisements.
</p>
<p><h4 id="fourteen">14.	Privacy Precaution Warning</h4>
Please note that no data transmission over the Internet is 100% secure. As a result, we cannot guarantee the security of the information that you transmit via our online services.
</p>
<p><h4 id="fifteen">15.	Your Consent</h4>
You may have certain rights under various state and federal statutes that may apply to the personal and non-personal information collected by Body & Brain yoga online. By accepting the terms of this Privacy Policy and using our Site, you are waiving all of such rights as to collection, use, disclosure and storage of your personally identifiable and non-personal information as described herein. You recognize that we are able to offer our Site and services to you at our rates based upon these terms, which are an integral part of our contract for the provision of services.
<br><br>
By using our Site and providing your personal information to us, you also authorize the export of your personal information to the USA, as well as its storage and use as specified herein. This Privacy Policy and our legal obligations are subject to the laws of Arizona and the USA, regardless of the location of any user.
</p>
<p><h4 id="sixteen">16.	Revisions to Our Privacy Policy</h4>
We may amend our Privacy Policy at any time, without notice to you, by posting such revised Privacy Policy on this page, so you are always aware of what information we collect, how we use it and under what circumstances we may disclose it. Any changes will only apply to information collected after the change is posted.
</p>
<p>
  <br>This Privacy Policy was last updated on November 21, 2011.
</p>
  </div>
</div>
