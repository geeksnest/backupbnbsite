<!-- <div class="pagebanner">
  <img src="/img/banners/franchise.jpg" width="100%" />
  <div class="pageheader"> FRANCHISING </div>
</div>

<div class="container-fluid wrapper-lg">
<div class="col-sm-12">
<div class="dl-brochure"><a class="a-blue" download="" href="https://bodynbrain.s3.amazonaws.com/uploads/otherbrochure/BNB-Franchise_Brochure.pdf"><img src="/img/pages/workshop/brochure.png" /> Download Brochure </a></div>

<p class="pg-p">Body &amp; Brain franchise system provides entrepreneurial opportunities for our instructors and members. Body &amp; Brain centers offer practical solutions to some of society&rsquo;s serious physical, emotional, and mental health problems by directly responding to the rising demand for alternative, comprehensive wellness. Operating a franchise allows owners the freedom to manage their own livelihood while developing themselves, helping others, and serving their communities with our founding spirit of widely benefiting all life on Earth.</p>

<p class="pg-p">Currently, there are more than 40 Body &amp; Brain franchise centers in the U.S. We also operate franchise centers around the world in South Korea, Japan, Germany, Russia, Kuwait, U.K., and Canada.</p>
</div>
</div> -->
<!-- end of row wrapper-lg -->

<div class="pagebanner">
  	{% if nobanner == false %}
      {% if slider == true %}

        <div class="swiper-banner">
          <div class="swiper-wrapper">
            {% for banner in pagebanners %}
              <div class="swiper-slide">
                {% if banner.url == null OR banner.url == "" %}
                  <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" width="100%" />
                {% else %}
                  <a href="{{banner.url}}" target="_blank">
                    <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" width="100%" />
                  </a>
                {% endif %}
              </div>
            {% endfor %}
          </div>
          <div class="swiper-pagination"></div>
        </div>

      {% else %}

          {% if pagebanners[0].url == null OR pagebanners[0].url == "" %}
            <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
          {% else %}
            <a href="{{pagebanners[0].url}}" target="_blank">
              <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
            </a>
          {% endif %}

      {% endif %}
    {% else %}
      <img src="/img/banners/about-workshops.jpg" width="100%" />
    {% endif %}
  <div class="pageheader"> <?php echo $title; ?> </div>
</div>

<div class="wrapper-lg-vw" style="line-height: 32px;">
    <?php echo $body; ?>
</div>
