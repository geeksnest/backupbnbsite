<!-- <div class="pagebanner">
  <img src="/img/banners/faq.jpg" width="100%" />
  <div class="pageheader"> FAQs </div>
</div>

<div class="container-fluid wrapper-lg">
  <div class="col-sm-12">
    <div class="pg-mini-title">Can I try a class without joining?</div>

    <p class="pg-p wrapper-md-b">Sure. You can make an appointment or visit a local center to try a class.</p>
  </div>

  <div class="col-sm-12">
    <div class="pg-mini-title">What do I wear for class?</div>

    <p class="pg-p wrapper-md-b">Body &amp; Brain classes promote relaxed movement and circulation of body heat. Therefore, light, loose clothing is encouraged. Most locations have uniforms available for classes. No yoga mats necessary. We have padded floors!</p>
  </div>

  <div class="col-sm-12">
    <div class="pg-mini-title">Will membership allow me to take class at more than one location?</div>

    <p class="pg-p wrapper-md-b">A basic membership will allow you to take classes at the center where you join. You may make arrangements to transfer your membership to another location. However, if you purchase a long term membership, you will be eligible to attend classes at any location.</p>
  </div>

  <div class="col-sm-12">
    <div class="pg-mini-title">What happens if I move to another city or state?</div>

    <p class="pg-p">If you move to another city or state, you can transfer your membership to a new location. If you move to a location where there is no Body &amp; Brain or affiliate then you may cancel your membership in accordance with the terms of your membership agreement.</p>
  </div>
</div> -->
<!-- end of row wrapper-lg -->

<div class="pagebanner">
  {% if nobanner == false %}
    {% if slider == true %}

      <div class="swiper-banner">
        <div class="swiper-wrapper">
          {% for banner in pagebanners %}
            <div class="swiper-slide">
              {% if banner.url == null OR banner.url == "" %}
                <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" width="100%" />
              {% else %}
                <a href="{{banner.url}}" target="_blank">
                  <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" width="100%" />
                </a>
              {% endif %}
            </div>
          {% endfor %}
        </div>
        <div class="swiper-pagination"></div>
      </div>

    {% else %}

        {% if pagebanners[0].url == null OR pagebanners[0].url == "" %}
          <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
        {% else %}
          <a href="{{pagebanners[0].url}}" target="_blank">
            <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
          </a>
        {% endif %}

    {% endif %}
  {% else %}
    <img src="/img/banners/about-workshops.jpg" width="100%" />
  {% endif %}

  <div class="pageheader"> <?php echo $title; ?> </div>
</div>

<div class="wrapper-lg-vw" style="line-height: 32px;">
    <?php echo $body; ?>
</div>
