<div class="secondarynav">
  <ul>
    <li class="active"><a href="/{{terms}}">Terms of Use</a></li>
    <li><a href="/{{privacy}}">Privacy Policy</a></li>
  </ul>
</div>

<div class="secondarynavmobile" style="display:none;">
  <div class="secondarynavdropdown">
    <ul class="nav nav-pills">
      <li role="presentation" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <label class="activetext">{{pagetitle}}</label> <i class="fa fa-chevron-down"></i>
        </a>
        <ul class="dropdown-menu">
          <li><a href="/{{terms}}">Terms of Use</a></li>
          <li><a href="/{{privacy}}">Privacy Policy</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>

<div class="container-fluid wrapper-lg-vw terms">
  <div class="col-sm-12">
    <h4>Terms of Use</h4>
    <p>Body & Brain yoga & Health Centers, Inc. ('Body & Brain yoga ') operates the web site www.bodynbrain.com ("Site") to provide online access to information about Body & Brain yoga and the products, services, and opportunities we provide (the
    "Service"). By accessing and using this Site, you agree to each of the terms and conditions set forth herein ('Terms of Use'). Additional terms and conditions applicable to specific areas of this Site or to particular content or transactions are also posted in particular areas of the Site and, together with these Terms of Use, govern your use of those areas content or transactions. These Terms of Use, together with applicable additional terms and conditions, are referred to as this 'Agreement".
    <br><br>
    Body & Brain yoga reserves the right to modify this Agreement at any time without giving you prior notice. Your use of the Site following any such modification constitutes your agreement to follow and be bound by the Agreement as modified. The last date these Terms of Use were revised is set forth below.
    </p>
    <p><h4>1. Products, Content and Specifications</h4>
    All features, content, specifications, products and prices of products and services described or depicted on this Site, are subject to change at any time without notice. Certain weights, measures and similar descriptions are approximate and are provided for convenience purposes only. Body & Brain yoga will make all reasonable efforts to accurately display the attributes of our products, including the applicable colors; however, the actual color you see will depend on your computer system, and we cannot guarantee that your computer will accurately display such colors. The inclusion of any products or services on this Site at a particular time does not imply or warrant that these products or services will be available at any lime. It is your responsibility to ascertain and obey all applicable local, state, federal and international laws (including minimum age
    requirements) in regard to the possession, use and sale of any item purchased from this Site. By placing an order, you represent that the products ordered will be used only in a lawful manner. All video cassettes, DVDs and similar products sold are for private, home use (where no admission fee is charged), non-public performance and may not be duplicated.
    </p>
    <p><h4>2. Shipping Limitations</h4>
    When an order is placed, it will be shipping to an address designated by the purchaser as long as that shipping address is compliant with the shipping restrictions contained on this Site. All purchases from this Site are made pursuant to a shipment contact. As a result, risk of loss and title for items purchased from this Site pass to you upon delivery of the items to the carrier. You are responsible for filing any claims with carriers for damaged and/or lost shipments. Click here to see Body & Brain yoga's Privacy Policy.
    </p>
    <p><h4>3. Accuracy of Information</h4>
    Body & Brain yoga attempts to ensure that information on this Site is complete, accurate and current. Despite our efforts, the information on this Site may occasionally be inaccurate, incomplete or out of date. We make no representation as to the completeness, accuracy or currentness of any information on this Site. For example, products included on this Site may be unavailable, may have different attributes than those listed, or may actually carry a different price than that stated on this Site. In addition, we may make changes in information about price and availability without notice. The price displayed on the website may differ from the price for the same item sold as merchandise at one of our locations. While it is our practice to confirm orders by email, the receipt of an email order confirmation does not constitute our acceptance of an order or our confirmation of an offer to sell a product or service. We reserve the right, without prior notice, to limit the order quantity on any product or service and/or to refuse service to any customer. We also may require verification of information prior to the acceptance and/or shipment of any order.
    </p>
    <p><h4>4. Use of Site</h4>
    You may use the Service, the Site, and the information, writings images and/or other works that you see, hear or otherwise experience on the Site (singly or collectively, the 'Content") solely for your non-commercial, personal purposes and/or to learn about Body & Brain yoga products and services. No right, title or interest in any Content is transferred to you, whether as a result of downloading such Content or otherwise. Body & Brain yoga reserves complete title and full intellectual property rights in all Content. Except as expressly authorized by this Agreement, you may not use, alter, copy, distribute, transmit, or derive another work from any Content obtained from the Site or the Service, except as expressly permitted by the Terms of Use.
    </p>
    <p><h4>5. Copyright</h4>
    The Site and the Content are protected by U.S. and/or foreign copyright laws, and belong to Body & Brain yoga or its partners, affiliates, contributors or third parties. The copyrights in the Content are owned by Body & Brain yoga or other copyright owners who have authorized their use on the Site. You may download and reprint Content for non-commercial, non-public, personal use only. (If you are browsing this Site as an employee or member of any business or organization, you may download and reprint Content only for educational or other non-commercial purposes within your business or organization, except as otherwise permitted by Body & Brain yoga, for example in certain password-restricted areas of the Site and in our Frequently Asked Questions). No manipulation or alteration, in any way of images or other Content on the Site is permitted.
    </p>
    <p><h4>6. Trademarks</h4>
    You are prohibited from using any of the marks or logos appearing throughout the Site without permission from the trademark owner, except as permitted by applicable law.
    <p>
    <p><h4>7. Links to Third-Party Web Sites</h4>
    Links on the Site to third party web sites or information are provided solely as a convenience to you. If you use these links, you will leave the Site. Such links do not constitute or imply an endorsement, sponsorship, or recommendation by Body & Brain yoga of the third party, the third-party web site, or the information contained therein. Body & Brain yoga is not responsible for the availability of any such web sites. Body & Brain yoga is not responsible or liable for any such web site or the content thereon. If you use the links to the web sites of Body & Brain yoga affiliates or service providers, you will leave the Site, and will be subject to the terms of use and privacy policy applicable to those web sites.
    </p>
    <p><h4>8. Linking to this Site</h4>
    lf you would like to link to the Site, you must 'follow Body & Brain yoga's link guidelines. Unless specifically authorized by Body & Brain yoga, you may not connect "deep links" to the Site i e create links to this site that bypass the home page or other parts of the Site. You may not mirror or frame the home page or any other pages of this Site on any other web site or web page.
    </p>
    <p><h4>9. Downloading Files</h4>
    Body & Brain yoga cannot and does not guarantee or warrant that files available for downloading through the Site will be free of infection by software viruses or other harmful computer code, files or programs.
    </p>
    <p><h4>10. Disclaimer of Warranties</h4>
    Body & Brain yoga MAKES NO EXPRESS OR IMPLIED WARRANTIES, REPRESENTATIONS OR ENDORSEMENTS WHATSOEVER WITH RESPECT TO THE SITE, THE SERVICE OR THE CONTENT. Body & Brain yoga EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT, WITH REGARD TO THE SITE, THE SERVICE, THE CONTENT, AND ANY PRODUCT OR SERVICE FURNISHED OR TO BE FURNISHED VIA THE SITE. Body & Brain yoga DOES NOT WARRANT THAT THE FUNCTIONS PERFORMED BY THE SITE OR THE SERVICE WILL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE, OR THAT DEFECTS IN THE SITE OR THE SERVICE WILL BE CORRECTED. Body & Brain yoga DOES NOT WARRANT THE ACCURACY OR COMPLETENESS OF THE CONTENT, OR THAT ANY ERRORS IN THE CONTENT WILL BE CORRECTED. THE SITE, THE
    SERVICE AND THE CONTENT ARE PROVIDED ON AN AS IS' AND 'AS AVAILABLE BASIS.
    <br><br>
    ALL PRODUCTS AND SERVICES PURCHASED ON OR THROUGH THIS SITE ARE SUBJECT ONLY TO ANY APPLICABLE WARRANTIES OF THEIR RESPECTIVE MANUFACTURES, DISTRIBUTORS AND SUPPLIERS, IF ANY. TO THE FULLEST EXTENT PERMISSIBLE BYAPPLICABLE LAW, WE HEREBY DISCLAIM ALL WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, ANY IMPLIED WARRANTIES WITH
    RESPECT TO THE PRODUCTS AND SERVICES LISTED OR PURCHASED ON OR THROUGH THIS SITE. WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, WE HEREBY EXPRESSLY DISCLAIM ALL LIABILITY FOR PRODUCT DEFECT OR FAILURE, CLAIMS THAT ARE DUE TO NORMAL WEAR, PRODUCT MISUSE, ABUSE, PRODUCT MODIFICATION, IMPROPER PRODUCT SELECTION, NON-COMPLIANCE WITH ANY CODES, OR MISAPPROPRIATION. WE MAKE NO WARRANTIES TO THOSE DEFINED AS "CONSUMERS' IN THE MAGNUSON-MOSS WARRANTY-FEDERAL TRADE COMMISSION IMPROVEMENTS ACT. THE FOREGOING EXCLUSIONS OF IMPLIED WARRANTIES DO NOT APPLY TO THE EXTENT PROHIBITED BY LAW. PLEASE REFER TO YOUR LOCAL LAWS FOR ANY SUCH PROHIBITIONS.
    </p>
    <p><h4>11. Limitation of Liability</h4>
    IN NO EVENT WILL Body & Brain yoga BE LIABLE FOR ANY
    DAMAGES WHATSOEVER, INCLUDING, BUT NOT LIMITED TO ANY DIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY OR OTHER INDIRECT DAMAGES ARISING OUT OF (I) THE USE OF OR INABILITY TO USE THE SITE, THE SERVICE, OR THE CONTENT, (II) ANY TRANSACTION CONDUCTED THROUGH OR FACILITATED BY THE SITE; (Ill) ANY CLAIM ATTRIBUTABLE TO ERRORS, OMISSIONS, OR OTHER INACCURACIES IN THE SITE, THE SERVICE AND/OR THE CONTENT, (IV) UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA, OR (V) ANY OTHER MATTER RELATING TO THE SITE, THE SERVICE, OR THE CONTENT, EVEN IF Body & Brain yoga HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IF YOU ARE DISSATISFIED WITH THE SITE, THE SERVICE, THE CONTENT, OR WITH THE TERMS OF USE, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE. BECAUSE SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU. IN SUCH STATES, Body & Brain yoga's LIABILITY IS LIMITED AND WARRANTIES ARE EXCLUDED TO THE GREATEST EXTENT PERMITTED BY LAW, BUT SHALL, IN NO EVENT, EXCEED $100.00.
    </p>
    <p><h4>12. Indemnificabon</h4>
    You understand and agree that you are personally responsible for your behavior on the Site. You agree to indemnify, defend and hold harmless Body & Brain yoga, its parent companies, subsidiaries, affiliated companies, joint venturers, business partners, licensors, employees, agents and any third-party information providers to the Service from and against all claims, losses, expenses, damages and costs (including, but not limited to, direct, incidental, consequential, exemplary and indirect damages), and reasonable attorneys' fees, resulting from or arising out of your use, misuse, or inability to use the Site, the Service, or the Content, or any violation by you of this Agreement.
    </p>
    <p><h4>13. Privacy</h4>
    Click <a href="/{{privacy}}">here</a> to see Body & Brain yoga's Privacy Policy.
    </p>
    <p><h4>14. User Conduct</h4>
    You agree to use the Site only for lawful purposes. You agree not to take any action that might compromise the security of the Site, render the Site inaccessible to others or otherwise cause damage to the Site or the Content. You agree not to add to, subtract tom, or otherwise modify the Content, or to attempt to access any Content that is not intended for you. You agree not to use the Site in any manner that might interfere with the rights of third parties.
    </p>
    <p><h4>15. Unsolicited Idea Submission Policy</h4>
    Body & Brain yoga or any of its employees do not accept or consider unsolicited ideas, including ideas for new advertising campaigns, marketing strategies, new or improved products, technologies, services, processes, materials, or new product names. We have bound this policy necessary in order to avoid misunderstandings should Body & Brain yoga's business activities bear coincidental similarities with one or more of the thousands of unsolicited ideas offered to us Please do not send your unsolicited ideas to Body & Brain yoga or anyone at Body & Brain yoga If, in spite of our request that you not send us your ideas, you still send them, then regardless of what your posting, email, letter, or other transmission may say, (1) your idea will automatically become the property of Body & Brain yoga, without any compensation to you; (2) Body & Brain yoga will have no obligation to return your idea to you or respond to you in any way; (3) Body & Brain yoga will have no obligation to keep your idea confidential; and (4) Body & Brain yoga may use your idea for any purpose whatsoever, including giving your idea to others. However, Body & Brain yoga does welcome feedback regarding many areas of Body & Brain yoga's existing businesses that will help satisfy customer's needs, and feedback can be provided through the many listed contact areas on the Site. Any feedback you provide shall be deemed a Submission under the terms in the User Supplied Information section above.
    </p>
    <p><h4>16. User Supplied Information</h4>
    Body & Brain yoga does not want to receive confidential or proprietary information from you via the Site. You agree that any material, information, or data you transmit to us or post to the Site (each a 'Submission' or collectively 'Submissions') will be considered non confidential and non proprietary For all Submissions, (1) you guarantee to us that you have the legal right to post the Submission and that it will not violate any law or the rights of any person or entity, and (2) you give Body & Brain yoga the royalty-free, irrevocable,
    perpetual, worldwide right to use, distribute, display and create derivative works from the
    Submission, in any and all media, in any manner, in whole or in part, without any restriction or responsibilities to you.
    </p>
    <p><h4>17. Password Security</h4>
    lf you register to become a DahnYoga.com member, you are responsible for maintaining the confidentiality of your member identification and password information, and for restricting access to your computer. You agree to accept responsibility for all activities that occur under your member identification and password.
    </p>
    <p><h4>18. General Provisions</h4>
    a.	Entire Agreement/No Waiver. These Terms of Use constitute the entire agreement of the parties with respect to the subject matter hereof. No waiver by Body & Brain yoga of any breach or default hereunder shall be deemed to be a waiver of any preceding or subsequent
    breach or default.
    <br><br>
    b.	Correction of Errors and Inaccuracies. The Content may contain typographical errors or other errors or inaccuracies and may not be complete or current. Body & Brain yoga therefore reserves the right to correct any errors,
    inaccuracies or omissions and to change or update the Content at any time without prior notice. Body & Brain yoga does not, however, guarantee that any errors,
    inaccuracies or omissions will be corrected.
    <br><br>
    c.	Enforcement/ Choice of Law/ Choice of Forum. lb any part of this Agreement is determined by a court of competent jurisdiction to be invalid or unenforceable, it will not impact any other provision of this Agreement, all of which will remain in full force and effect. Any and all disputes relating to this Agreement, Body & Brain yoga's Privacy Policy, your use of the Site, any other Body & Brain yoga web site, the Service, or the Content are governed by, and will be interpreted in accordance with, the laws of the State of Arizona, without regard to any conflicts of laws provisions.
    </p>
    <p><h4>19. Refund Policy</h4>
    At DahnYoga.com we value our relationship with you and hope to provide the highest quality services and information. If at any time you feel that our services do not meet your needs or you have changed your mind about reserving an appointment at one of our locations, you can request a refund within the applicable 30 day period.
    <br><br>
    a.	Refunds are only accepted for transactions completed via www.DahnYoga.com. <br>
    b.	Refunds may be requested up to 30 days from the date of issuance of a Transaction ID. <br>
    c.	Any deviation from the return policy outlined above could result in a delay of your refund. <br>
    d.	If you have any questions about your Refund Request or our Refund Policy, please contact customerservicee@dahnyoga.com
    </p>
  </div>
</div>
