<div ng-controller="pageCtrl">
  <div class="secondarynav">
    <ul>
      <li class="<?php echo $pageslugs == $about ? 'active':''; ?>"><a href="/{{about}}">Who Are We?</a></li>
      <li class="<?php echo $pageslugs == $classes ? 'active':''; ?>"><a href="/{{classes}}">Classes</a></li>
      <li class="<?php echo $pageslugs == $workshops ? 'active':''; ?>"><a href="/{{workshops}}">Workshops</a></li>
      <li class="<?php echo $pageslugs == $wellness ? 'active':''; ?>"><a href="/{{wellness}}">Wellness at Work</a></li>
      <li class="<?php echo $pageslugs == $stories ? 'active':''; ?>"><a href="/{{stories}}/1">Testimonials</a></li>
    </ul>
  </div>

  <div class="container-fluid secondarynavmobile" style="display:none;">
    <div class="secondarynavdropdown">
      <ul class="nav nav-pills">
        <li role="presentation" class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <label class="activetext">{{pagetitle}}</label> <i class="fa fa-chevron-down"></i>
          </a>
          <ul class="dropdown-menu">
            <li><a href="/{{about}}">Who Are We?</a></li>
            <li><a href="/{{classes}}">Classes</a></li>
            <li><a href="/{{workshops}}">Workshops</a></li>
            <li><a href="/{{wellness}}">Wellness at Work</a></li>
            <li><a href="/{{stories}}/1">Testimonials</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>

  <div class="pagebanner">
    
    <!-- <img src="{{amazonlink}}/uploads/pageimage/<?php echo $contentdata->pagebanner; ?>" width="100%"> -->
    {% if nobanner == false %}
      {% if slider == true %}

        <div class="swiper-banner">
          <div class="swiper-wrapper">
            {% for banner in pagebanners %}
              <div class="swiper-slide">
                {% if banner.url == null OR banner.url == "" %}
                  <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" width="100%" />
                {% else %}
                  <a href="{{banner.url}}" target="_blank">
                    <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" width="100%" />
                  </a>
                {% endif %}
              </div>
            {% endfor %}
          </div>
          <div class="swiper-pagination"></div>
        </div>

      {% else %}

          {% if pagebanners[0].url == null OR pagebanners[0].url == "" %}
            <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
          {% else %}
            <a href="{{pagebanners[0].url}}" target="_blank">
              <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
            </a>
          {% endif %}

      {% endif %}
    {% else %}
      <img src="/img/banners/about-workshops.jpg" width="100%" />
    {% endif %}

    <div class="pageheader"> {{contentdata.title}} </div>
  </div>

  {#<div style="box-sizing:border:box;">
      <?php echo $contentdata->body; ?>
  </div>#}
  <div class="wrapper-lg-vw" style="line-height: 32px;">
      <?php echo $contentdata->body; ?>
  </div>
</div>
