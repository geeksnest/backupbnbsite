<!-- <div class="pagebanner">
  <img src="/img/banners/founderbanner.png" width="100%" />
  <div class="pageheader"> FOUNDER </div>
</div>

<div class="container-fluid wrapper-lg">
<div class="col-sm-12">
<p class="pg-p">Body &amp; Brain Founder, Ilchi Lee, is a visionary, educator, and global humanitarian who has helped people around the world create and manifest a life of health, happiness, and peace through Brain Education.</p>
&nbsp;

<p class="pg-p">Lee is an avid believer in the power and responsibility of the individual to utilize their brain for the positive benefit of all life. For this, he created &quot;Brain Education,&quot; a new field of education which aims to help people utilize the full potential of their brain to find their true potential and purpose. He is currently working to pioneer Brain Education globally.</p>
&nbsp;

<p class="pg-p">Visit Ilchi Lee&rsquo;s website at ilchi.com</p>
</div>
</div> -->
<!-- end of row wrapper-lg -->

<div class="pagebanner">
  {% if nobanner == false %}
    {% if slider == true %}

      <div class="swiper-banner">
        <div class="swiper-wrapper">
          {% for banner in pagebanners %}
            <div class="swiper-slide">
              {% if banner.url == null OR banner.url == "" %}
                <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" width="100%" />
              {% else %}
                <a href="{{banner.url}}" target="_blank">
                  <img src="{{amazonlink}}/uploads/slidernbanner/{{banner.image}}" width="100%" />
                </a>
              {% endif %}
            </div>
          {% endfor %}
        </div>
        <div class="swiper-pagination"></div>
      </div>

    {% else %}

        {% if pagebanners[0].url == null OR pagebanners[0].url == "" %}
          <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
        {% else %}
          <a href="{{pagebanners[0].url}}" target="_blank">
            <img src="{{amazonlink}}/uploads/slidernbanner/{{pagebanners[0].image}}" width="100%" />
          </a>
        {% endif %}

    {% endif %}
  {% else %}
    <img src="/img/banners/about-workshops.jpg" width="100%" />
  {% endif %}
  <div class="pageheader"> <?php echo $title; ?> </div>
</div>

<div class="wrapper-lg-vw" style="line-height: 32px;">
    <?php echo $body; ?>
</div>
