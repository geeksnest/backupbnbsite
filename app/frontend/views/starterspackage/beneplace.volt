{# the below style is for responsive purpose on this page only. -null #}
<style>
.row, .col-sm-6 {
  margin:0;
  padding:0;
}
</style>
 <div ng-controller="beneplaceCtrl" class="starterspackage"> <!-- starterspackageCtrl -->
  <div ng-show="firststep" class="firststep"> <!-- firststep -->
    <div class="row secondarynav">
      <ul>
        <li class="active">Choose Program</li>
        <li>Find Location</li>
        <li>Schedule Session & Class</li>
        <li>Confirm Your Appointment</li>
      </ul>
    </div>

    <div class="row">
      <div class="col-sm-6">
        <div class="tryaclassbanner">
          <div class="tryaclassheader">BENEPLACE</div>
          {% if outeroneclassbanner == true %}
                <img src="{{amazonlink}}/uploads/slidernbanner/{{outeronebanner.image}}" class="pointer" width="100%" ng-click="pickclasstype('intro')">
          {% else %}
                <img src="/img/banners/tryaclass1.jpg" class="pointer" width="100%" ng-click="pickclasstype('intro')">
          {% endif %}
        </div>
        <div class="tryaclasscolcontainerone">
          <div class="tryaclasstitle" ng-cloak>1-on-1 Intro Session ${[{introonlinerate}]} (<span class="strikeout">${[{introofflinerate}]}</span>)</div>
          <div class="tryaclassdescription">This 45-minute private session checks your flexibility, balance, breathing, and stress levels. Get a customized practice plan that is tailored to your specific physical, mental, emotional, and/or spiritual needs.</div>
          <div class="tryaclassbtn hidden"><a class="btn orangebutton" ng-click="pickclasstype('intro')">get started</a></div>
        </div>
      </div>

      <div class="col-sm-6">
        <div class="tryaclassbanner">
          {% if outergroupclassbanner == true %}
                <img src="{{amazonlink}}/uploads/slidernbanner/{{outergroupbanner.image}}" class="pointer" width="100%" ng-click="pickclasstype('group')">
          {% else %}
                <img src="/img/banners/tryaclass2.jpg" class="pointer" width="100%" ng-click="pickclasstype('group')">
          {% endif %}
        </div>
        <div class="tryaclasscolcontainergroup">
          <div class="tryaclasstitle" ng-cloak>1 Group Class + 1-on-1 Intro Session ${[{grouponlinerate}]} (<span class="strikeout">${[{groupofflinerate}]}</span>)</div>
          <div class="tryaclassdescription">Get everything from a 1-on-1 intro session and also experience a regular Body&Brain class.</div>
          <div class="tryaclassbtn hidden"><a class="btn orangebutton" ng-click="pickclasstype('group')">get started</a></div>
        </div>
      </div>
    </div>

    <div class="tryaclassdisclaimer">
      <div>Disclaimer: The information provided during the Intro Session is not intended as professional medical advice and should not replace the advice of your healthcare practitioner.</div>
    </div>

  </div> <!-- end of firststep -->

  <div ng-show="secondstep" class="secondstep hidden"> <!-- secondstep -->

    <div class="row secondarynav">
      <ul>
        <li><a href="" ng-click="gotofirststep()">Choose Program</a></li>
        <li class="active">Find Location</li>
        <li>Schedule Session & Class</li>
        <li>Confirm Your Appointment</li>
      </ul>
    </div>

    <div class="row tryaclassbanner">
      <div class="tryaclassheader">TRY A CLASS</div>

      {% if inneroneclassbanner == true %}
            {#<img src="{{amazonlink}}/uploads/slidernbanner/{{inneronebanner.image}}" width="100%" ng-if="classtype =='one'">#}
            <div ng-if="classtype =='one'" style="width:100%; height:inherit; background:url({{amazonlink}}/uploads/slidernbanner/{{inneronebanner.image}}); background-size:cover; background-position:center center;">
            </div>
      {% else %}
            <div ng-if="classtype =='one'" style="width:100%; height:inherit; background:url('/img/banners/tryaclass1.2.jpg'); background-size:cover; background-position:center center;">
            </div>
            {#<img src="/img/banners/tryaclass1.2.jpg" width="100%" ng-if="classtype =='one'">#}
      {% endif %}

      {% if innergroupclassbanner == true %}
            {#<img src="{{amazonlink}}/uploads/slidernbanner/{{innergroupbanner.image}}" width="100%" ng-if="classtype =='group'">#}
            <div ng-if="classtype =='group'" style="width:100%; height:inherit; background:url({{amazonlink}}/uploads/slidernbanner/{{innergroupbanner.image}}); background-size:cover; background-position:center center;">
            </div>
      {% else %}
            <div ng-if="classtype =='group'" style="width:100%; height:inherit; background:url('/img/banners/tryaclass2.2.jpg'); background-size:cover; background-position:center center;">
            </div>
            {#<img src="/img/banners/tryaclass2.2.jpg" width="100%" ng-if="classtype =='group'">#}
      {% endif %}

    </div>

    <div class="row tryaclasssearchlocation">
      <div class="col-lg-6 searchcenterform">
        <div class="searchformheader">Find a center nearest you to schedule your first class.</div>
        <form class="form-horizontal">
          <div class="form-group">
            <label for="locationzip" class="col-sm-3 control-label customtextalignleft bababa">Search by Zip Code</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="locationzip" placeholder="Ex. 85282" ng-model="location.locationzip" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 0" ng-change="onzip(location.locationzip)">
            </div>
          </div>
          <div class="form-group">
            <label for="locationcity" class="col-sm-3 control-label customtextalignleft bababa">Search by City & State</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="locationcity" placeholder="Ex. Phoenix, AZ" ng-model="location.locationcity">
            </div>
          </div>
          <div class="form-group">
            <label for="locationstate" class="col-sm-3 control-label customtextalignleft bababa">Search by State</label>
            <div class="col-sm-9">
              <select class="form-control searchformdropdown" id="locationstate" ng-model="location.locationstate">
                <option value="">Select State</option>
                <option ng-repeat="datalist in statelist" value="{[{datalist.state_code}]}">{[{datalist.state}]}</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
              <button type="submit" class="btn orangebutton" ng-click="searchlocation(location)">search</button>
            </div>
          </div>
        </form>


        <div class="overlay" ng-show="searchlocationloader">
          <i class="fa fa-refresh fa-spin" style="font-size:50px;"></i>
        </div>
      </div>

      <div class="col-lg-6 tac-locresult" ng-show="locationnoresult">
        <center>
          <img src="/img/frontend/nosearch.png" />
          <br><br>
          <h3>NO RESULT FOUND</h3>
          <br>
          <br>
          <br>
        </center>
      </div>

      <div class="col-lg-6 tac-locresult" ng-show="locationresult">
        <div class="searchformheader">Choose a location</div>
        <!-- <div>Choose a location</div> -->
          <a dir-paginate="centerlist in centerlist | itemsPerPage: 5 | orderBy: 'fdistance' " href="" class="tryaclassscenterlink" ng-click="selectedcenter(centerlist.centerid, centerlist.centertitle, centerlist.centeraddress, centerlist.centerzip, centerlist.state, centerlist.centerstate, centerlist.phonenumber, centerlist.lat, centerlist.lon, centerlist.paypalid, centerlist.authorizeid, centerlist.authorizekey)">
            <div class="col-sm-12 tryaclasscenterdetails">
              <div class="tryaclasscentertitle"><div class="tryaclasscenterdetailsarrow"></div> {[{centerlist.centertitle}]}</div>
              <div class="tryaclasscenteraddress">{[{centerlist.centeraddress}]}, {[{centerlist.centerstate}]}, {[{centerlist.centerzip}]}</div>
              <div class="tryaclasscenterphone">Phone : {[{centerlist.phonenumber}]}</div>
            </div>
          </a>
          <dir-pagination-controls></dir-pagination-controls>
      </div>
    </div>

    <div class="row centermap" ng-show="locationresult">
      <div id="map_canvas">
        <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options" bounds="map.bounds">
          <ui-gmap-markers models="randomMarkers" coords="'self'"  icon="'icon'" fit="'true'">
            <ui-gmap-windows show="show">
              <div ng-non-bindable>{[{title}]}</div>
            </ui-gmap-windows>
          </ui-gmap-markers>
        </ui-gmap-google-map>
      </div>
    </div>

  </div> <!-- end of secondstep -->



  <div ng-if="thirdstep" class="thirdstep"> <!-- thirdstep -->

    <div class="row secondarynav">
      <ul>
        <li><a href="" ng-click="gotofirststep()">Choose Program</a></li>
        <li><a href="" ng-click="gotosecondstep()">Find Location</a></li>
        <li class="active">Schedule Session & Class</li>
        <li>Confirm Your Appointment</li>
      </ul>
    </div>

    <div class="row centermap">
      <div id="map_canvas">
        <ui-gmap-google-map center="singlecentermap.center" zoom="singlecentermap.zoom" draggable="true" options="options" bounds="singlecentermap.bounds">
          <ui-gmap-markers models="singlecenterrandomMarkers" coords="'self'"  icon="'icon'">
            <ui-gmap-windows show="show">
              <div ng-non-bindable>{[{title}]}</div>
            </ui-gmap-windows>
          </ui-gmap-markers>
        </ui-gmap-google-map>
      </div>
    </div>



    <div class="tryaclassschedule">
      <div class="tryaclassscheduleheader">Schedule Session & Class</div>
      <form class="form-horizontal">
        <div class="tryaclassscheduleform">
          <div class="col-sm-6">
            <div class="tryaclassschedulecentertitle">{[{scopecentertitle}]}, {[{scopecenterstatename}]}</div>
            <div class="tryaclassschedulecenteraddress">{[{scopecenteraddress}]}, {[{scopecenterzip}]}</div>
            <div class="tryaclassschedulecenterphone">Phone : {[{scopecenterphone}]}</div>
              <div class="form-group">
                <label for="fullname" class="col-sm-3 control-label customtextalignleft">Full Name</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="fullname" placeholder="Full Name" ng-init="classdetails.fullname = ''" ng-model="classdetails.fullname" ng-change="classdetailsfullnamechange(classdetails.fullname)">
                  <em class="emerrormessage" ng-bind-html="classdetailsfullnamevalidation"></em>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-sm-3 control-label customtextalignleft">Email</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="email" ng-init="classdetails.email = ''" placeholder="Email" ng-model="classdetails.email" ng-change="classdetailsemailchange(classdetails.email)">
                  <em class="emerrormessage" ng-bind-html="classdetailsemailvalidation"></em>
                </div>
              </div>
              <div class="form-group">
                <label for="phonenumber" class="col-sm-3 control-label customtextalignleft">Phone Number</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="phonenumber" placeholder="Phone Number" ng-init="classdetails.phonenumber = ''" ng-model="classdetails.phonenumber" ng-change="classdetailsphonenumberchange(classdetails.phonenumber)" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                  <em class="emerrormessage" ng-bind-html="classdetailsphonevalidation"></em>
                </div>
              </div>
            <div ng-if="classtype == 'intro'">
              <div class="row">
                <div class="tryaclassscheduleclasstitle">1-on-1 intro session</div>
                <div>Select a date and class time for a  1-on-1 intro session</div>
              </div>
              <div class="row tryaclassscheduleformdatetimecontainer">
                <div class="col-lg-6 tryaclassscheduleformdate">
                  <strong>Date</strong>
                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" is-open="cdate.customStartDate.open" ng-click = "cdate.customStartDate.open = true" max-date="maxDate" min-date="minDate" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" close-text="Close" readonly required ng-init="classdetails.classdate = ''" ng-model="classdetails.classdate" ng-change="classdetailsclassdatechange(classdetails.classdate)"/>
                    <em class="emerrormessage" ng-bind-html="classdetailsdatevalidation"></em>
                </div>
                <div class="col-lg-6 tryaclassscheduleformtime">
                  <strong>Time</strong>
                  <select class="form-control scheduleformdropdown" ng-init="classdetails.classtime = ''" ng-model="classdetails.classtime" ng-change="classdetailsclasstimechange(classdetails.classtime)">
                    <option value="" selected="">Time</option>
                    <option value="07:00 AM">07:00 AM</option>
                    <option value="07:30 AM">07:30 AM</option>
                    <option value="08:00 AM">08:00 AM</option>
                    <option value="08:30 AM">08:30 AM</option>
                    <option value="09:00 AM">09:00 AM</option>
                    <option value="09:30 AM">09:30 AM</option>
                    <option value="10:00 AM">10:00 AM</option>
                    <option value="10:30 AM">10:30 AM</option>
                    <option value="11:00 AM">11:00 AM</option>
                    <option value="11:30 AM">11:30 AM</option>
                    <option value="12:00 PM">12:00 PM</option>
                    <option value="12:30 PM">12:30 PM</option>
                    <option value="01:00 PM">01:00 PM</option>
                    <option value="01:30 PM">01:30 PM</option>
                    <option value="02:00 PM">02:00 PM</option>
                    <option value="02:30 PM">02:30 PM</option>
                    <option value="03:00 PM">03:00 PM</option>
                    <option value="03:30 PM">03:30 PM</option>
                    <option value="04:00 PM">04:00 PM</option>
                    <option value="04:30 PM">04:30 PM</option>
                    <option value="05:00 PM">05:00 PM</option>
                    <option value="05:30 PM">05:30 PM</option>
                    <option value="06:00 PM">06:00 PM</option>
                    <option value="06:30 PM">06:30 PM</option>
                    <option value="07:00 PM">07:00 PM</option>
                    <option value="06:30 PM">07:30 PM</option>
                    <option value="07:00 PM">08:00 PM</option>
                    <option value="06:30 PM">08:30 PM</option>
                    <option value="07:00 PM">09:00 PM</option>
                    <option value="06:30 PM">09:30 PM</option>
                    <option value="07:00 PM">10:00 PM</option>
                  </select>
                  <em class="emerrormessage" ng-bind-html="classdetailstimevalidation"></em>
                </div>
              </div>
              <div class="row col-lg-12">*An instructor will contact you to confirm your appointment or propose another time</div>
              <div class="row col-lg-12 tryaclassscheduleprivacy">*All of your personal information will be kept private: <a href="" class="privacylink">Privacy Policy</a></div>
            </div>
          </div>
        </div>

        <div class="row tryaclassscheduleform" ng-if="classtype == 'group'">
              <div class="col-lg-12 tryaclassscheduleclasstitle">Group class</div>
              <div class="col-lg-12 nopad">Select a date and class time for a group class {[{dsss}]}</div>
              <div class="col-sm-12 calendarcontainer">
                <div class="col-md-4 inlinedate nopad">
                  <em class="emerrormessage" ng-bind-html="classdetailsinlinecalendarvalidation"></em>
                  <datepicker ng-model="dt" min-date="minDate" show-weeks="false" class="datepicker" ng-click="classselecteddate(dt)"></datepicker>
                </div>
                <div class="col-md-4 schedulelistcontainer" ng-show="classdetailsschedulelist">
                <em class="emerrormessage" ng-bind-html="classdetailsschedulelistvalidation"></em>
                <h4>{[{returndate | date:'MMMM , dd yyyy'}]}</h4>
                <a ng-repeat="schedlist in schedulelistofcenter" href="" class="tryaclasssschedulelink">
                    <div class="col-sm-12 tryaclassscheduledetails" ng-class="{'tryaclassscheduledetailsactive':$index == selectedschedule}"  ng-click="selectschedule($index, schedlist.starttime, schedlist.classandday)">
                      <div class="tryaclassscheduletitle"><div  ng-class="{'tryaclassscheduledetailsarrowactive':$index == selectedschedule,'tryaclassscheduledetailsarrow':$index != selectedschedule}"></div>
                        <span ng-bind="schedlist.classandday"></span>
                      </div>
                      <div class="tryaclasscenteraddress">{[{schedlist.starttime}]} - {[{schedlist.endtime}]}</div>
                    </div>
                  </a>
                </div>
                <div class="col-lg-4 col-md-4 summaryselectedschedule" ng-show="summaryselectedschedule">
                <h3>You’ve selected: {[{returndayordinal}]}
                  at {[{scopeclasstime}]} for {[{scopeclassandday}]}</h3>
                </div>
              </div>
        </div>

        <div class="row">
          <div class="col-lg-12 nopad">
            <strong>Additional Comment</strong> <em>(optional)</em>
            <textarea class="form-control" rows="3" placeholder="Is there anything you'd like us to know?" ng-init="classdetails.classcomment = ''" ng-model="classdetails.classcomment"></textarea>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 tryaclassscheduleformprice">
            Item Price: ${[{itemprice}]} for {[{scopeclasstype}]}
          </div>
          <div class="col-lg-2 tryaclassscheduleformtime">
            <strong>Quantity</strong>
            <select class="form-control scheduleformdropdown" ng-init="classdetails.itemqty = 1" ng-model="classdetails.itemqty" ng-change="classdetailsitemqtychange(classdetails.itemqty)">
              <option selected="" value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
            </select>
          </div>
          <div class="col-lg-12 tryaclassscheduleformprice">
            Total Price: ${[{classtotalprice}]}
          </div>
        </div>
        <div class="row">
            <div class="col-lg-12 nopad">
              <button type="submit" class="btn orangebutton" ng-click="submitclassdetails(classdetails)">submit</button>
            </div>
        </div>
      </form>
    </div>

  </div> <!-- end of thirdstep -->



  <div ng-if="fourthstep" class="fourthstep"> <!-- fourthstep -->

    <div class="row secondarynav">
      <ul>
        <li><a href="" ng-click="gotofirststep()">Choose Program</a></li>
        <li><a href="" ng-click="gotosecondstep()">Find Location</a></li>
        <li><a href="" ng-click="gotothirdstep()">Schedule Session & Class</a></li>
        <li class="active">Confirm Your Appointment</li>
      </ul>
    </div>

    <div class="row centermap">
      <div id="map_canvas">
        <ui-gmap-google-map center="singlecentermap.center" zoom="singlecentermap.zoom" draggable="true" options="options" bounds="singlecentermap.bounds">
          <ui-gmap-markers models="singlecenterrandomMarkers" coords="'self'"  icon="'icon'">
            <ui-gmap-windows show="show">
              <div ng-non-bindable>{[{title}]}</div>
            </ui-gmap-windows>
          </ui-gmap-markers>
        </ui-gmap-google-map>
      </div>
    </div>



    <div class="tryaclasspaymentmethod">
      <div class="row">
        <div class="col-lg-6">
          <div class="tryaclasspaymentmethodheader">Payment Method</div>
          <div class="row tryaclasspaymentmethodtype">
            <div class="col-sm-6 col-xs-6 tryaclasspaymentmethodchoices">
              <button type="submit" class="btn btn-lg btn-block" ng-click="paymenttype = 'paypal'" ng-class="{'orangebutton':paymenttype == 'paypal'}">
                <i class="fa fa-cc-paypal"></i> Paypal
              </button>
            </div>
            <div class="col-sm-6 col-xs-6 tryaclasspaymentmethodchoices">
              <button type="submit" class="btn btn-lg btn-block" ng-click="paymenttype = 'creditcard'" ng-class="{'orangebutton':paymenttype == 'creditcard'}">
                <i class="fa fa-credit-card"></i> Credit Card
              </button>
            </div>
          </div>
        </div>
      </div>

      <div class="row" ng-if="paymenttype=='paypal'">
        <div class="col-lg-6 text-center ng-scope paypalbuttoncontainer">
          <h4 class="info-text"> <small>Just click the paypal button to proceed to paypal payment.</small></h4>
          <form class="paypalform ng-pristine ng-valid" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
            <input type="hidden" name="business" value="{[{scopepaypalid}]}">
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="item_name" value="{[{scopeclasstype}]}">
            <input type="hidden" name="item_number" value="1">
            <input type="hidden" name="custom" value="beneplace|{[{transactionid}]}">
            <input type="hidden" name="amount" value="{[{paypalpricerate}]}">
            <input type="hidden" name="quantity" value="{[{paypalitemquantity}]}">
            <input type="hidden" name="currency_code" value="USD">
            <input type="hidden" name="return" value="{[{BaseURL}]}/payment/transactions/success">
            <input type="hidden" name="cancel_return" value="{[{BaseURL}]}/payment/transactions/cancel">
            <input type="submit" name="btnPaypalSubmit"  id="btnPaypalSubmit" class="btn_submit_paypal" ng-show="validpaypal">
          </form>
          <!-- Display the payment button. -->
          <input type="image" name="submit" border="0" src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/buy-logo-large.png" alt="PayPal - The safer, easier way to pay online" ng-click="processpaypal()">
          <em class="emerrormessage" ng-bind-html="paypalprocesserror"></em>
          <div class="overlay"  ng-show="paypalprocess">
            <i class="fa fa-refresh fa-spin" style="font-size:50px;"></i>
          </div>
        </div>
      </div>
      <div ng-if="paymenttype=='creditcard'">
        <div class="row tryaclasspaymentform">
          <div class="col-lg-6">
            <div class="form-group">
              <img src="/img/creditcards/amex.png" width="49px" height="29px">
              <img src="/img/creditcards/discover.png" width="49px" height="29px">
              <img src="/img/creditcards/mastercard.png" width="49px" height="29px">
              <img src="/img/creditcards/visa.png" width="49px" height="29px">
            </div>
            <em class="emerrormessage" ng-bind-html="tryaclasspaymentformsummaryerror"></em>
            <br>
            <br>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="cardnumber" class="col-sm-3 control-label customtextalignleft bababa">Card Number</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="cardnumber" placeholder="Card Number" ng-init="billing.cardnumber = ''" ng-model="billing.cardnumber" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 0" ng-change="tryaclasspaymentformcardnumberchange(billing.cardnumber)">
                  <em class="emerrormessage" ng-bind-html="tryaclasspaymentformcardnumbervalidation"></em>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-sm-3 control-label customtextalignleft bababa">First Name</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="firstname" placeholder="First Name" ng-init="billing.firstname = ''" ng-model="billing.firstname" ng-change="tryaclasspaymentformfirstnamechange(billing.firstname)">
                  <em class="emerrormessage" ng-bind-html="tryaclasspaymentformfirstnamevalidation"></em>
                </div>
              </div>
              <div class="form-group">
                <label for="lastname" class="col-sm-3 control-label customtextalignleft bababa">Last Name</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="lastname" placeholder="Last Name" ng-init="billing.lastname = ''" ng-model="billing.lastname" ng-change="tryaclasspaymentformlastnamechange(billing.lastname)">
                  <em class="emerrormessage" ng-bind-html="tryaclasspaymentformlastnamevalidation"></em>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 nopad"><label for="expirationmonth" class="col-sm-3 nopad control-label customtextalignleft bababa">Expiration Date</label></div>
                <div class="col-sm-6 tryaclasspaymentformexpiration">
                  <select class="form-control searchformdropdown" id="expirationmonth" ng-init="billing.expirationmonth = ''" ng-model="billing.expirationmonth" ng-change="tryaclasspaymentformexpirationmonthchange(billing.expirationmonth)">
                    <option selected="selected" value="">Month</option>
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                    <option value="04">04</option>
                    <option value="05">05</option>
                    <option value="06">06</option>
                    <option value="07">07</option>
                    <option value="08">08</option>
                    <option value="09">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                  </select>
                  <em class="emerrormessage" ng-bind-html="tryaclasspaymentformexpirationmonthvalidation"></em>
                </div>
                <div class="col-sm-6">
                  <select class="form-control searchformdropdown" ng-init="billing.expirationyear = ''" ng-model="billing.expirationyear" ng-change="tryaclasspaymentformexpirationyearchange(billing.expirationyear)">
                    <option selected="selected" value="">Year</option>
                    <option value="15">2015</option>
                    <option value="16">2016</option>
                    <option value="17">2017</option>
                    <option value="18">2018</option>
                    <option value="19">2019</option>
                    <option value="20">2020</option>
                    <option value="21">2021</option>
                    <option value="22">2022</option>
                    <option value="23">2023</option>
                    <option value="24">2024</option>
                    <option value="25">2025</option>
                    <option value="26">2026</option>
                    <option value="27">2027</option>
                    <option value="28">2028</option>
                    <option value="29">2029</option>
                  </select>
                  <em class="emerrormessage" ng-bind-html="tryaclasspaymentformexpirationyearvalidation"></em>
                </div>
              </div>
            </form>
          </div>
        </div>


        <div class="tryaclassbillinginfo">
          <div class="col-lg-12">
            <div class="tryaclassbillinginfoheader">Billing Information</div>
          </div>
        </div>

        <div class="row tryaclasspaymentform">
          <div class="col-lg-6">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="fullname" class="col-sm-3 control-label customtextalignleft bababa">Full Name</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="fullname" placeholder="Full Name" ng-init="billing.fullname = ''" ng-model="billing.fullname" ng-change="tryaclasspaymentformfullnamechange(billing.fullname)">
                  <em class="emerrormessage" ng-bind-html="tryaclasspaymentformfullnamevalidation"></em>
                </div>
              </div>
              <div class="form-group">
                <label for="address1" class="col-sm-3 control-label customtextalignleft bababa">Address Line 1</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="address1" placeholder="Address Line 1" ng-init="billing.address1 = ''" ng-model="billing.address1" ng-change="tryaclasspaymentformaddress1change(billing.address1)">
                  <em class="emerrormessage" ng-bind-html="tryaclasspaymentformaddress1validation"></em>
                </div>
              </div>
              <div class="form-group">
                <label for="address2" class="col-sm-3 control-label customtextalignleft bababa" >Address Line 2</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="address2" placeholder="Address Line 2" ng-init="billing.address2 = ''" ng-model="billing.address2">
                  <em style="color:#A7A7A7;">(Address Line 2 is Optional)</em>
                </div>
              </div>
              <div class="form-group">
                <label for="city" class="col-sm-3 control-label customtextalignleft bababa">City</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="city" placeholder="City" ng-init="billing.city = ''" ng-model="billing.city" ng-change="tryaclasspaymentformcitychange(billing.city)">
                  <em class="emerrormessage" ng-bind-html="tryaclasspaymentformcityvalidation"></em>
                </div>
              </div>
              <div class="form-group">
                <label for="state" class="col-sm-3 control-label customtextalignleft bababa">Search by State</label>
                <div class="col-sm-9">
                  <select class="form-control searchformdropdown" id="state" ng-init="billing.state = ''" ng-model="billing.state" ng-change="tryaclasspaymentformstatechange(billing.state)">
                    <option value="">Select state</option>
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas</option>
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">District Of Columbia</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MD">Maryland</option>
                    <option value="MA">Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NM">New Mexico</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI">Wisconsin</option>
                    <option value="WY">Wyoming</option>
                  </select>
                  <em class="emerrormessage" ng-bind-html="tryaclasspaymentformstatevalidation"></em>
                </div>
              </div>
              <div class="form-group">
                <label for="phonenumber" class="col-sm-3 control-label customtextalignleft bababa">Phone Number</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="phonenumber"  placeholder="Phone Number" ng-init="billing.phonenumber = ''" ng-model="billing.phonenumber" ng-change="tryaclasspaymentformphonenumberchange(billing.phonenumber)" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                  <em class="emerrormessage" ng-bind-html="tryaclasspaymentformphonenumbervalidation"></em>
                </div>
              </div>
              <div class="form-group">
                <label for="zip" class="col-sm-3 control-label customtextalignleft bababa">Zip Code</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="zip" placeholder="Zip Code" ng-init="billing.zip = ''" ng-model="billing.zip" onkeypress="return event.charCode >= 48 && event.charCode <= 57" ng-change="tryaclasspaymentformzipchange(billing.zip)">
                  <em class="emerrormessage" ng-bind-html="tryaclasspaymentformzipvalidation"></em>
                </div>
              </div>
              <br>
              <div class="form-group">
                <div class="col-sm-12">
                  <input type="checkbox" id="flat-checkbox-1" i-check ng-init="billing.terms = false" ng-model="billing.terms" ng-change="tryaclasspaymentformtermschange(billing.terms)">
                  I understand and accept the terms and conditions.
                  <br>
                  <em class="emerrormessage" ng-bind-html="tryaclasspaymentformtermsvalidation"></em>
                </div>
              </div>
              <br>

              <div class="form-group">
                <div class="col-sm-12">
                  <button type="submit" class="btn orangebutton" ng-click="submitbillinginfo(billing)" ng-disabled="billbtndisabled">Submit <span ng-show="billbuttonload"> <i class="fa fa-refresh fa-spin" style="color:#FFF;"></i></span></button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- end of fourthstep -->

</div> <!-- end of starterspackageCtrl -->
