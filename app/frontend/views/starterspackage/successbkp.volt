<div class="row tryaclassbanner" id="non-printable">
	<div class="tryaclassheader">TRY A CLASS</div>
	<img src="/img/banners/tryaclassbanner.png" width="100%">
</div>

<div class="row secondarynav" id="non-printable">
	<ul>
		<li>Choose Program</li>
		<li>Find Location</li>
		<li>Schedule Session & Class</li>
		<li class="active">Confirm Your Appointment</li>
	</ul>
</div>

<div class="row successcontainer">
	<div class="graybordercontainer">
		<span class="sub_title" id="non-printable">
			<p class="deartxt">Dear <span class="nametxt">{{transactionresult.name}}</span>,</p>
		</span>
		<div class="program_wrap" id="non-printable">
			<div id="non-printable">
				<p class="graytxt minwidth">Thank you for your purchase with Body &amp; Brain Yoga! You made an appointment at the {{transactionresult.centertitle}} center on {{date("D, F d Y",  strtotime(transactionresult.day))}} at {{transactionresult.hour}}. One of our friendly center staff will contact you shortly to confirm your appointment. At the time of your session, we recommend loosely-fitted clothing and socks. No yoga mats are necessary.</p>
				<br>
				<p>If you have any questions, you can reach us at:<br>Phone: <span class="orangetext">{{transactionresult.phonenumber}}</span> or Email: <a href="mailto:{{transactionresult.email}}"><span class="orangetext">{{transactionresult.email}}</span></a><br>Confirmation Number is <span class="orangetext">{{transactionresult.confirmationnumber}}</span><br>Starter Package: <span class="orangetext">{{transactionresult.moduletype}}</span></p>
				<br>
				<br>
			</div>
		</div>
		<div class="successbnblogo"><img src="/img/logo.png" width="100%"></div>
		<p class="bigcyan">Your Appointment</p>

		<p class="graytxt">Starter Package: <span class="orangetext">{{transactionresult.moduletype}}</span></p>

		<p class="graytxt">Name: <span class="orangetext">{{transactionresult.name}}</span></p>

		<p class="graytxt">When: <span class="orangetext">{{ date("D, F d Y",  strtotime(transactionresult.day)) }}, {{transactionresult.hour}} </span><em>(Your appointment is subject to availability.)</em></p>

		<p class="graytxt">Where: <span class="orangetext">{{transactionresult.centertitle}}</span><br>
			<em class="orangetext">{{transactionresult.centeraddress}}, {{transactionresult.centerzip}}</em><br>
			<em class="orangetext">Phone: {{transactionresult.phonenumber}}</em>
		</p>

		<p class="graytxt">Confirmation Number: <span class="orangetext">{{transactionresult.confirmationnumber}}</span></p>

		<p class="graytxt">Payment: <span class="orangetext">Paid ${{transactionresult.paid}} {{transactionresult.payment}}</span></p>
		<div class="successcentermap">
			<div id="map"></div>
		</div>
		<br id="non-printable">
		<br id="non-printable">
		<button type="submit" onclick="myFunction()" class="btn orangebutton" id="non-printable">Print <span> <i class="fa fa-print" style="color:#FFF;"></i></span></button>
	</div>
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp"></script>
<script type="text/javascript">
	function myFunction() {
		window.print();
	}
	window.marker = null;

      function initialize() {
        var map;

        var nottingham = new google.maps.LatLng({{transactionresult.lat}}, {{transactionresult.lon}});

        var style = [
          { "featureType": "road",
             "elementType":
             "labels.icon",
             "stylers": [
              { "saturation": 1 },
              { "gamma": 1 },
              { "visibility": "on" },
              { "hue": "#e6ff00" }
             ]
          },
          { "elementType": "geometry", "stylers": [
            { "saturation": -100 }
            ]
          }
        ];

        var mapOptions = {
          // SET THE CENTER
          center: nottingham,

          // SET THE MAP STYLE & ZOOM LEVEL
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          zoom:16,

          // SET THE BACKGROUND COLOUR
          backgroundColor:"#eeeeee",

          // REMOVE ALL THE CONTROLS EXCEPT ZOOM
          panControl:true,
          scrollwheel: false,
          zoomControl:true,
          mapTypeControl:false,
          scaleControl:true,
          streetViewControl:true,
          overviewMapControl:true,
          zoomControlOptions: {
            style:google.maps.ZoomControlStyle.SMALL
          }

        }
        map = new google.maps.Map(document.getElementById('map'), mapOptions);

        google.maps.event.addDomListener(window, "resize", function() {
          var center = map.getCenter();
          google.maps.event.trigger(map, "resize");
          map.setCenter(center);
        });


        // SET THE MAP TYPE
        var mapType = new google.maps.StyledMapType(style, {name:"Grayscale"});
        map.mapTypes.set('grey', mapType);
        map.setMapTypeId('grey');

        //CREATE A CUSTOM PIN ICON
        var marker_image ='/img/pin.png';
        var pinIcon = new google.maps.MarkerImage(marker_image,null,null, null,new google.maps.Size(21, 34));
        map.setCenter(nottingham);
        marker = new google.maps.Marker({
          position: nottingham,
          map: map,
          icon: pinIcon,
          title: 'Absolute Nottingham'
        });
      }

      google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js [1]';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6027673984667',
{'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none"
src="https://www.facebook.com/tr?ev=6027673984667&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1
[2]" /></noscript>
