<div class="row tryaclassbanner" id="non-printable">
	<div class="tryaclassheader">TRY A CLASS</div>
	<img src="/img/banners/tryaclassbanner.png" width="100%">
</div>

<div class="row secondarynav" id="non-printable">
	<ul>
		<li>Choose Program</li>
		<li>Find Location</li>
		<li>Schedule Session & Class</li>
		<li class="active">Confirm Your Appointment</li>
	</ul>
</div>

<div class="row successcontainer">
	<div class="graybordercontainer">
    <p class="deartxt">Thank you! </p>
    <p class="graytxt">This purchase entitles <span class="orangetext">{{transactionresult.name}}</span> to a {{transactionresult.moduletype}} at the {{transactionresult.centertitle}} center. </p>
    <p class="graytxt">Confirmation Number: <span class="orangetext">{{transactionresult.confirmationnumber}}</span></p>
    <p class="deartxt">{{transactionresult.centertitle}}</p>
    <p class="deartxt">Phone: {{transactionresult.phonenumber}}</p>
    <p class="graytxt">Please contact your selected center to confirm your {{transactionresult.moduletype}}. This is your voucher. It does not expire and is redeemable at any time. YOU MUST PRINT THIS VOUCHER  and bring it with you. The centers require proof of purchase to redeem.</p>
		<button type="submit" onclick="myFunction()" class="btn orangebutton" id="non-printable">Print <span> <i class="fa fa-print" style="color:#FFF;"></i></span></button>
	</div>
</div>

<script type="text/javascript">
	function myFunction() {
		window.print();
	}
</script>

<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js [1]';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6027673984667',
{'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none"
src="https://www.facebook.com/tr?ev=6027673984667&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1
[2]" /></noscript>
