<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    protected function initialize(){
       $this->view->titletag = '';
       $this->view->descriptiontag = '';

       $this->view->facebookmetaurl = '';
       $this->view->facebookmetatitle = '';
       $this->view->facebookmetadescription = '';
       $this->view->facebookmetaimage = '';

       $this->view->about = "about";
       $this->view->press = "press";
       $this->view->locations = "locations";
       $this->view->founder = "ilchi-lee";
       $this->view->affiliates = "affiliates";
       $this->view->faqs = "faqs";
       $this->view->franchising = "franchising";
       $this->view->mainnews = "bnb-buzz";
       $this->view->stories = "success-stories";
       $this->view->shop = "shop";
       $this->view->classes = "classes";
       $this->view->wellness = "wellness-at-work";
       $this->view->workshops = "workshops";
       $this->view->terms = "terms-of-use";
       $this->view->privacy = "privacy-policies";

       $this->view->pageslugs = "";
       $this->view->activepage = "";

       //these are urls for account index pageslugs
       $this->view->url_account = "/account";
       $this->view->url_yourprofile = "/account/profile/personalinfo";
       $this->view->url_yourmedia   = "/account/media/courses";
       //end

       //these are urls for profile navigation
       $this->view->url_personalinfo   = "/account/profile/personalinfo";
       $this->view->url_resetpassword = "/account/profile/resetpassword";
       $this->view->url_paymentinfo    = "/account/profile/paymentinfo";
       $this->view->url_publicprofile  = "/account/profile/publicprofile";
       //end

       //these are urls for media navigation
       $this->view->url_courses   = "/account/media/courses";
       $this->view->url_videos = "/account/media/videos";
       $this->view->url_music    = "/account/media/music";
       //end

       $this->view->amazonlink = $this->config->application->amazonlink;
       $this->view->BaseURL = $this->config->application->BaseURL;

       $this->view->center = array();

       $this->view->script_google = $this->curl('/settings/script');
    }

    public function globalmetatags($titletag, $descriptiontag, $facebookmetaurl, $facebookmetatitle, $facebookmetadescription, $facebookmetaimage){
        $this->view->titletag = $titletag;
        $this->view->descriptiontag = $descriptiontag;

        $this->view->facebookmetaurl = $this->config->application->BaseURL.$facebookmetaurl;
        $this->view->facebookmetatitle = $facebookmetatitle;
        $this->view->facebookmetadescription = $facebookmetadescription;
        $this->view->facebookmetaimage = $this->config->application->amazonlink.$facebookmetaimage;
    }

    public function curl($url) {
        $service_url = $this->config->application->ApiURL.'/'.$url;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        return $decoded = json_decode($curl_response);
    }

    public function angularLoader($ang){
      $modules = array();
      $scripts = '';
      foreach($ang as $key => $val){
          $scripts .= $this->tag->javascriptInclude($val, false);
          $modules[] = $key;
      }
      $this->view->modules = (!empty($modules) ? $modules : array());
      $this->view->otherjvascript = $scripts;
    }

    private function createJsConfig(){
        $script="app.constant('Config', {
                // This is a generated Config
                // Any changes you make in this file will be replaced
                // Place you changes in app/config.php file \n";
        foreach( $this->config->application as $key=>$val){
            $script .= $key . ' : "' . $val .'",' . "\n";
        }
        $script .= "});";
        $fileName="../public/be/js/scripts/config.js";

        $exist =  file_get_contents($fileName);
        if($exist == $script){

        }else{
            file_put_contents($fileName, $script);
        }
    }

    public function route404() {
      $this->view->pick("index/route404");
      $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}
