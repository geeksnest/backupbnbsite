<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class AccountController extends ControllerBase
{
    public function indexAction()
    {

    }
    public function personalinfoAction()
    {
      $this->view->profileslugs = "personalinfo";
      $this->angularLoader(array(
          "/fe/scripts/profile.js", //this is the Profile Navigation animation
          "/fe/scripts/controllers/account/personalinfoCtrl.js",
          "/fe/scripts/factory/AccountFactory.js"
      ));
    }
    public function resetpasswordAction()
    {
      $this->view->profileslugs = "resetpassword";
      $this->angularLoader(array(
          "/fe/scripts/profile.js", //this is the Profile Navigation animation
          "/fe/scripts/controllers/account/resetpasswordCtrl.js",
          "/fe/scripts/factory/AccountFactory.js" 
      ));
    }
    //reset and change passwod are two different function. Reset = normal form; Change = forgot password;
    public function changepasswordAction($requestcode)
    {
      $decoded = $this->curl("/changepassword/validate/" . $requestcode);
      if($decoded->valid == true) {
        $this->view->profileslugs = "resetpassword";
        $this->angularLoader(array(
          "/fe/scripts/profile.js", //this is the Profile Navigation animation
          "/fe/scripts/controllers/account/changepasswordCtrl.js",
          "/fe/scripts/factory/AccountFactory.js" 
        ));
      } else {
        $this->route404();
      }
    }
    public function paymentinfoAction()
    {
      $this->view->profileslugs = "paymentinfo";
      $this->angularLoader(array(
          "/fe/scripts/profile.js" //this is the Profile Navigation animation
      ));
    }
    public function publicprofileAction()
    {
      $this->view->profileslugs = "publicprofile";
      $this->angularLoader(array(
          "/fe/scripts/profile.js" //this is the Profile Navigation animation
      ));
    }
    public function coursesAction()
    {
      $this->view->mediaslugs = "courses";
      $this->angularLoader(array(
          "/fe/scripts/media.js" //this is the Profile Navigation animation
      ));
    }
    public function videosAction()
    {
      $this->view->mediaslugs = "videos";
      $this->angularLoader(array(
          "/fe/scripts/media.js" //this is the Profile Navigation animation
      ));
    }
    public function musicAction()
    {
      $this->view->mediaslugs = "music";
      $this->angularLoader(array(
          "/fe/scripts/media.js" //this is the Profile Navigation animation
      ));
    }
}
