<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class BnbbuzzController extends ControllerBase
{
    public function indexAction($page)
    {
    	$this->angularLoader(array(
            'bnbbuzz' => '/fe/scripts/controllers/bnbbuzz/bnbbuzz.js',
            'bnbbuzzfactory' => '/fe/scripts/factory/bnbbuzz/bnbbuzz.js',
            'matchheight' => '/vendors/matchHeight/jquery.matchHeight-min.js',
            'matchheightcustom' => '/fe/scripts/others/matchheightcustom.js',
            'resizeimagetoparent' => '/fe/scripts/others/jquery.resizeimagetoparent.min.js',
            'imagesloaded' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
            'imagefill' => '/fe/scripts/others/jquery-imagefill.js',
            'imagefillcustom' => '/fe/scripts/others/imagefill.js',
            'lazyload' => '/vendors/jquery_lazyload/jquery.lazyload.js',
            'imglazyload' => '/fe/scripts/others/imglazyload.js',
            ));

        $decoded = $this->curl("/fe/bnbbuzz/index/all/". $page);

        if($decoded){
            // $this->view->pageslugs = "bnb-buzz";
            $this->view->activepage = 'mainnews';
            $this->view->datanewslist = $decoded->newslist;
            $this->view->newscategory = $decoded->newscategory;

            $itemperpage = 9;
            $this->view->page = $page;
            $this->view->totalpage = ceil($decoded->totalnews / $itemperpage);

            // head tags and facebook og tags
            $fortitletag = 'Yoga & Meditation Tips, Tai Chi, Qigong | Body & Brain yoga';
            $fordescriptiontag = 'Learn what it takes to have a healthy mind, body and spirit with Yoga Republic. Topics include yoga, meditation, tai chi, and qigong.';
            $forfacebookmetaurl = '';
            $forfacebookmetatitle = '';
            $forfacebookmetadescription = '';
            $forfacebookmetaimage = '';

            $this->globalmetatags($fortitletag, $fordescriptiontag, $forfacebookmetaurl, $forfacebookmetatitle, $forfacebookmetadescription,$forfacebookmetaimage);
        }
        else{
            $this->view->pick("index/route404");
            $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        }
    }


    public function categoryAction($category, $page)
    {
        $this->angularLoader(array(
            'bnbbuzz' => '/fe/scripts/controllers/bnbbuzz/bnbbuzz.js',
            'bnbbuzzfactory' => '/fe/scripts/factory/bnbbuzz/bnbbuzz.js',
            'matchheight' => '/vendors/matchHeight/jquery.matchHeight-min.js',
            'matchheightcustom' => '/fe/scripts/others/matchheightcustom.js',
            'resizeimagetoparent' => '/fe/scripts/others/jquery.resizeimagetoparent.min.js',
            'imagesloaded' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
            'imagefill' => '/fe/scripts/others/jquery-imagefill.js',
            'imagefillcustom' => '/fe/scripts/others/imagefill.js',
            'lazyload' => '/vendors/jquery_lazyload/jquery.lazyload.js',
            'imglazyload' => '/fe/scripts/others/imglazyload.js',
            ));

        $decoded = $this->curl("/fe/bnbbuzz/index/". $category ."/". $page);
        if($decoded){
            // $this->view->pageslugs = "bnb-buzz";
            $this->view->activepage = 'mainnews';
            $this->view->datanewslist = $decoded->newslist;
            $this->view->newscategory = $decoded->newscategory;

            $this->view->currentcategory = $category;
            $this->view->currentcategoryname = $decoded->currentcategoryname;
            $itemperpage = 10;
            $this->view->page = $page;
            $this->view->totalpage = ceil($decoded->totalnews / $itemperpage);

            // head tags and facebook og tags
            $fortitletag = 'Yoga & Meditation Tips, Tai Chi, Qigong | Body & Brain yoga';
            $fordescriptiontag = 'Learn what it takes to have a healthy mind, body and spirit with Yoga Republic. Topics include yoga, meditation, tai chi, and qigong.';
            $forfacebookmetaurl = '';
            $forfacebookmetatitle = '';
            $forfacebookmetadescription = '';
            $forfacebookmetaimage = '';

            $this->globalmetatags($fortitletag, $fordescriptiontag, $forfacebookmetaurl, $forfacebookmetatitle, $forfacebookmetadescription,$forfacebookmetaimage);
        }
        else{
            $this->route404();
        }

    }

    public function viewAction($pageslugs)
    {
        $this->angularLoader(array(
            'bnbbuzz' => '/fe/scripts/controllers/bnbbuzz/bnbbuzz.js',
            'bnbbuzzfactory' => '/fe/scripts/factory/bnbbuzz/bnbbuzz.js',
            'matchheight' => '/vendors/matchHeight/jquery.matchHeight-min.js',
            'matchheightcustom' => '/fe/scripts/others/matchheightcustom.js',
            'resizeimagetoparent' => '/fe/scripts/others/jquery.resizeimagetoparent.min.js',
            'imagesloaded' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
            'imagefill' => '/fe/scripts/others/jquery-imagefill.js',
            'imagefillcustom' => '/fe/scripts/others/imagefill.js',
            'lazyload' => '/vendors/jquery_lazyload/jquery.lazyload.js',
            'imglazyload' => '/fe/scripts/others/imglazyload.js',
            ));

        $decoded = $this->curl("/fe/bnbbuzz/newsview/". $pageslugs);
        if($decoded){
            $this->view->activepage = 'mainnews';
            $this->view->newsdata = $decoded->newsdata;
            // head tags and facebook og tags

            if($decoded->newsdata->metatitle == "") {
              $fortitletag = $decoded->newsdata->title;
            } else {
              $fortitletag = $decoded->newsdata->metatitle;
            }


            $fordescriptiontag = $decoded->newsdata->description;
            $forfacebookmetaurl = '/bnb-buzz/view/'.$decoded->newsdata->newsslugs;
            $forfacebookmetatitle = $decoded->newsdata->title;
            $forfacebookmetadescription = $decoded->newsdata->description;
            $forfacebookmetaimage = '/uploads/newsimage/'.$decoded->newsdata->banner;

            $this->globalmetatags($fortitletag, $fordescriptiontag, $forfacebookmetaurl, $forfacebookmetatitle, $forfacebookmetadescription,$forfacebookmetaimage);
        } else {
            $this->view->pick("index/route404");
            $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        }

    }
}
