<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class LocationsController extends ControllerBase
{
    public function indexAction()
    {
      $this->view->activepage = 'locations';
      $this->angularLoader(array(
          'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
          '/fe/scripts/others/jquery-imagefill.js',
          '/fe/scripts/others/imagefill.js',
          '/fe/scripts/controllers/location/loc.index.js',
          '/fe/scripts/factory/location/loc.index.js',
          '/fe/scripts/factory/map-fac.js',
          '/fe/scripts/others/sliderbanner.js'
        ));

      $decoded = $this->curl("/fe/locations/index");
      $this->view->nobanner = $decoded->nobanner;
      if($decoded->nobanner == false) {
          $this->view->slider = $decoded->slider;
          $this->view->pagebanners = $decoded->locationsbanners;
      }

      $this->view->titletag = "Body & Brain | Locations";
    }
    public function centerAction()
    {

    }

    public function centertestAction()
    {

    }

    public function centernewsAction($centerslugs, $centernewsslugs) {
      $decoded = $this->curl("/fe/".$centerslugs."/newspost/".$centernewsslugs);
      if($decoded->existingNews == true) {
        $this->view->centerslugs = $centerslugs;
        $this->view->centernewsslugs = $centernewsslugs;
        $this->view->centernews = $decoded->centernews;
        $this->view->centertitle = $decoded->centertitle;

        if($decoded->centernews->metatitle == "") {
          $fortitletag = $decoded->centernews->title;
        } else {
          $fortitletag = $decoded->centernews->metatitle;
        }

        $fordescriptiontag = $decoded->centernews->description;
        $forfacebookmetaurl = "/".$centerslugs."/newspost/".$centernewsslugs;
        $forfacebookmetatitle = $decoded->centernews->title;
        $forfacebookmetadescription = $decoded->centernews->description;
        $forfacebookmetaimage = "/uploads/newsimage/".$decoded->centernews->banner;
        $this->globalmetatags($fortitletag, $fordescriptiontag, $forfacebookmetaurl, $forfacebookmetatitle, $forfacebookmetadescription,$forfacebookmetaimage);
      } else {
        $this->route404();
      }
    }
}
