<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class WorkshopsController extends ControllerBase
{
    public function indexAction()
    {
      $this->view->activepage = 'about';
      $this->angularLoader(array(
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
        '/fe/scripts/others/jquery-imagefill.js',
        '/fe/scripts/others/imagefill.js',
        '/vendors/matchHeight/jquery.matchHeight-min.js',
        '/fe/scripts/others/matchheightcustom.js'
      ));
      $decoded = $this->curl("/_workshops/index");
      $this->view->nobanner = $decoded->nobanner;
      if($decoded->nobanner == false) {
          $this->view->slider = $decoded->slider;
          $this->view->pagebanners = $decoded->workshopbanners;
      }
      $this->view->workshoptitles = $decoded->workshoptitles;
      $this->view->notitle = $decoded->notitles;
      $this->view->pagetitle = "Workshops";
      $this->view->titletag = "Yoga Classes combining Tai Chi, Meditation | Body & Brain";
    }

    public function titleAction($title) {
      $check = $this->curl("_workshops/titlecheck/".$title);
      if($check->titleExist == true) {
          $this->view->activepage = 'about';
          $this->angularLoader(array(
            'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
            '/fe/scripts/others/jquery-imagefill.js',
            '/fe/scripts/others/imagefill.js',
            '/fe/scripts/controllers/workshops/wsTitleCtrl.js',
            '/fe/scripts/factory/workshops/WsTitleFactory.js',
            '/fe/scripts/factory/starterspackage/starterspackage.js',
            '/fe/scripts/factory/map-fac.js',
            '/fe/scripts/factory/DateFactory.js',
            '/vendors/iCheck/icheck.js',
            '/fe/scripts/directives/icheck.js',
            '/others/jspdf.js'
          ));
          $this->view->workshoptitle = strtoupper($check->title);
          $this->view->pagetitle = "Find a center";
      } else {
        $this->route404();
      }
    }
}
