<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class PagesController extends ControllerBase
{
    public function indexAction()
    {

    }

    public function getpagedetails($slugs) {
      $decoded = $this->curl("/center-page/view/validate/" . $slugs);

      $this->view->titletag = $decoded->content->metatitle;
      $this->view->pageslugs = $decoded->content->pageslugs;
      $this->view->title = $decoded->content->title;
      $this->view->body = $decoded->content->body;
      $this->view->nobanner = $decoded->nobanner;
      if($decoded->nobanner == false) {
        $this->view->slider = $decoded->slider;
        $this->view->pagebanners = $decoded->pagebanner;
      }
    }

    public function viewAction($dataslugs)
    {
    	$decoded = $this->curl("/center-page/view/validate/".$dataslugs);
        if($decoded->type == 'pages') {
            $this->view->activepage = 'about';
            $this->view->pageslugs = $dataslugs;
            $this->view->contentdata = $decoded->content;
            $this->view->pagetitle = ucwords(strtolower($decoded->content->title));
            // head tags and facebook og tags
            $fortitletag = $decoded->content->metatitle;
            $fordescriptiontag = $decoded->content->metadesc;
            $forfacebookmetaurl = '';
            $forfacebookmetatitle = $decoded->content->title;
            $forfacebookmetadescription = '';
            $forfacebookmetaimage = '/uploads/pageimage/'.$decoded->content->pagebanner;
            $this->view->nobanner = $decoded->nobanner;
            if($decoded->nobanner == false) {
              $this->view->slider = $decoded->slider;
              $this->view->pagebanners = $decoded->pagebanner;
            }

            $this->globalmetatags($fortitletag, $fordescriptiontag, $forfacebookmetaurl, $forfacebookmetatitle, $forfacebookmetadescription,$forfacebookmetaimage);

            $this->angularLoader(array(
                'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
                '/fe/scripts/others/jquery-imagefill.js',
                '/fe/scripts/others/imagefill.js',
                '/vendors/matchHeight/jquery.matchHeight-min.js',
                '/fe/scripts/others/matchheightcustom.js',
                '/fe/scripts/controllers/pages/page.js',
                '/fe/scripts/factory/pages/page.js',
                ));

        } elseif ($decoded->type == 'center') {
            $this->view->activepage = '';
            $this->view->slugs = $dataslugs;
            $this->angularLoader(array(
                'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
                '/fe/scripts/others/jquery.divas-1.1.js',
                '/fe/scripts/others/divasCustom.js',
                '/vendors/matchHeight/jquery.matchHeight-min.js',
                '/fe/scripts/others/matchheightcustom.js',
                '/fe/scripts/controllers/location/center.js',
                '/fe/scripts/factory/location/center.js',
                '/fe/scripts/factory/map-fac.js',
                '/fe/scripts/others/swiper-center.js',
                '/fe/scripts/others/jquery.resizeimagetoparent.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
                '/fe/scripts/others/jquery-imagefill.js',
                '/vendors/jquery_lazyload/jquery.lazyload.js',
                '/fe/scripts/others/imglazyload.js',
                '/fe/scripts/others/imagefill.js'
                ));
            $this->view->center             = $decoded->center;
            $this->view->centerimages       = $decoded->centerimages;
            $this->view->centernews         = $decoded->centernews;
            // $this->view->centerschedule     = $decoded->centerschedule;
            // $this->view->centerevents       = $decoded->centerevents;
            $this->view->centerimg          = $this->config->application->amazonlink ."/uploads/center/".$decoded->centerimages[0]->centerid;
            $this->view->centernewsimg      = $this->config->application->amazonlink ."/uploads/newsimage";
            $this->view->centermembership   = $decoded->centermembership;
            $this->view->centertestimonials = $decoded->centertesti;
            $this->view->sociallinks        = $decoded->scllnks;

            $this->view->pick("locations/center");

            if($decoded->center->metatitle == "") {
              $metatitle = "Body & Brain | " . $decoded->center->centertitle;
            } else {
              $metatitle = $decoded->center->metatitle;
            }

            $this->globalmetatags($metatitle,
              $decoded->center->metadesc,
              "/".$decoded->center->centerslugs,
              $metatitle,
              $decoded->center->metadesc,
              '');

        } else if($decoded->type == 'account') {
          $this->view->slugs = $dataslugs;
          $this->view->pick("account/index");
        } else {
          // $this->angularLoader(array(
          //     'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
          //     '/fe/scripts/others/jquery-imagefill.js',
          //     '/fe/scripts/others/imagefill.js',
          //     '/vendors/matchHeight/jquery.matchHeight-min.js',
          //     '/fe/scripts/others/matchheightcustom.js',
          //     '/fe/scripts/controllers/pages/page.js',
          //     '/fe/scripts/factory/pages/page.js',
          //     ));
            $this->route404();
        }
    }
    public function termsAction() {
      $this->view->pagetitle = "Terms of Use";
      $this->view->pageslugs = "terms-of-use";
    }
    public function privacyAction() {
      $this->angularLoader(array(
          '/fe/scripts/controllers/pages/pgPrivacyCtrl.js'
      ));
      $this->view->pagetitle = "Privacy Policy";
      $this->view->pageslugs = "privacy-policies";
    }
    public function affiliatesAction() {
      $this->angularLoader(array(
          'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
          '/fe/scripts/others/jquery-imagefill.js',
          '/fe/scripts/others/imagefill.js',
          '/vendors/matchHeight/jquery.matchHeight-min.js',
          '/fe/scripts/others/matchheightcustom.js',
          ));
      $this->getpagedetails('affiliates');
    }
    public function faqsAction() {
      $this->angularLoader(array(
          'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
          '/fe/scripts/others/jquery-imagefill.js',
          '/fe/scripts/others/imagefill.js',
          '/vendors/matchHeight/jquery.matchHeight-min.js',
          '/fe/scripts/others/matchheightcustom.js',
          ));

      $this->getpagedetails('faqs');
    }

    public function franchisingAction() {
      $this->angularLoader(array(
          'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
          '/fe/scripts/others/jquery-imagefill.js',
          '/fe/scripts/others/imagefill.js',
          ));

      $this->getpagedetails('franchising');
    }

    public function founderAction() {
      $this->angularLoader(array(
          'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
          '/fe/scripts/others/jquery-imagefill.js',
          '/fe/scripts/others/imagefill.js',
          ));
      $this->getpagedetails('ilchi-lee');
    }

}
