<?php

namespace Modules\Frontend\Controllers;

use Phalcon\Mvc\View;

class ShopController extends ControllerBase
{

  public function indexAction()
  {
  	$this->angularLoader(array(
      'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
      '/fe/scripts/others/jquery-imagefill.js',
      '/fe/scripts/others/imagefill.js',
      '/vendors/jquery_lazyload/jquery.lazyload.js',
      '/fe/scripts/others/imglazyload.js',
      '/fe/scripts/others/shopslider.js',
      '/fe/scripts/controllers/shop/shop.js',
      '/fe/scripts/factory/shop.js',
      '/vendors/Swiper/dist/js/swiper.min.js',
      '/fe/scripts/others/swiper-center.js'
    ));

    $this->view->sliderimages = $this->curl("/shop/slider");
  }

 	public function viewAction()
 	{
 		$this->angularLoader(array(
 			'/fe/scripts/controllers/shop/addcart.js',
 			'/fe/scripts/factory/shop.js',
      '/fe/scripts/factory/login.js'
	  ));
 	}

  public function bagAction()
  {
    $this->angularLoader(array(
      '/fe/scripts/controllers/shop/bag.js',
      '/fe/scripts/factory/shop.js',
      '/fe/scripts/factory/login.js'
    ));
  }

  public function checkoutAction()
  {
    $this->angularLoader(array(
      '/fe/scripts/controllers/shop/checkout.js',
      '/fe/scripts/factory/shop.js',
      '/fe/scripts/factory/login.js' ));

  }

  public function successAction()
  {

  }

  public function digitalmediaAction()
  {
    $this->angularLoader(array(
      'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
      '/fe/scripts/others/jquery-imagefill.js',
      '/fe/scripts/others/imagefill.js',
      '/vendors/jquery_lazyload/jquery.lazyload.js',
      '/fe/scripts/others/imglazyload.js',
      '/fe/scripts/others/shopslider.js',
      '/fe/scripts/controllers/shop/shop.js',
      '/fe/scripts/factory/shop.js',
      '/vendors/Swiper/dist/js/swiper.min.js',
      '/fe/scripts/others/swiper-center.js'
    ));

    $this->view->sliderimages = $this->curl("/shop/slider");
  }

  public function membershipAction()
  {
    $this->angularLoader(array(
      'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
      '/fe/scripts/others/jquery-imagefill.js',
      '/fe/scripts/others/imagefill.js',
      '/vendors/jquery_lazyload/jquery.lazyload.js',
      '/fe/scripts/others/imglazyload.js',
      '/fe/scripts/others/shopslider.js',
      '/vendors/Swiper/dist/js/swiper.min.js',
      '/fe/scripts/others/swiper-center.js',
      '/fe/scripts/controllers/shop/membership.js',
      '/fe/scripts/factory/shop.js'
    ));

    $decoded = $this->curl("/fe/membership");
    $this->view->nobanner = $decoded->nobanner;
    $this->view->workshops = $decoded->workshoptitles;
    $this->view->centereventspopular = $decoded->centereventspopular;
    $this->view->centerevents = $decoded->centerevents;
    if($decoded->nobanner == false) {
        $this->view->slider = $decoded->slider;
        $this->view->sliderimages = $decoded->locationsbanners;
    }
  }
}
