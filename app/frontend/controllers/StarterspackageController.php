<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class StarterspackageController extends ControllerBase
{
    public function indexAction()
    {
    	$this->angularLoader(array(
    		'starterspackage' => '/fe/scripts/controllers/starterspackage/starterspackage.js',
    		'starterspackagefactory' => '/fe/scripts/factory/starterspackage/starterspackage.js',
    		'mapFactory' =>'/fe/scripts/factory/map-fac.js',
            'icheckdirective' => '/fe/scripts/directives/icheck.js',
            'resizeimagetoparent' => '/fe/scripts/others/jquery.resizeimagetoparent.min.js',
            'imagesloaded' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
            'imagefill' => '/fe/scripts/others/jquery-imagefill.js',
            'matchheight' => '/vendors/matchHeight/jquery.matchHeight-min.js',
            '/fe/scripts/others/matchheightcustom.js',
            'imagefillcustom' => '/fe/scripts/others/imagefill.js',
            'matchheightcustom' => '/fe/scripts/others/matchheightcustom.js',
            'icheck' => '/vendors/iCheck/icheck.js'
        ));

        $decoded = $this->curl("/fe/tryaclass/index");

        $this->view->outeroneclassbanner = $decoded->outeroneclassbanner;
        if($decoded->outeroneclassbanner == true) {
           $this->view->outeronebanner = $decoded->outeronebanner;
        }

        $this->view->inneroneclassbanner = $decoded->inneroneclassbanner;
        if($decoded->inneroneclassbanner == true) {
           $this->view->inneronebanner = $decoded->inneronebanner;
        }

        $this->view->outergroupclassbanner = $decoded->outergroupclassbanner;
        if($decoded->outergroupclassbanner == true) {
           $this->view->outergroupbanner = $decoded->outergroupbanner;
        }

        $this->view->innergroupclassbanner = $decoded->innergroupclassbanner;
        if($decoded->innergroupclassbanner == true) {
           $this->view->innergroupbanner = $decoded->innergroupbanner;
        }
        $this->view->titletag = "Yoga Classes combining Tai Chi, Meditation | Body & Brain";
        // $this->view->oneclassbanner = $decoded->oneclassbanner;
        // if($decoded->oneclassbanner == true) {
        //     $this->view->oneclassslider = $decoded->oneclassslider;
        //     $this->view->oneclassbanners = $decoded->oneclassbanners;
        // }
        //
        // $this->view->groupclassbanner = $decoded->groupclassbanner;
        // if($decoded->groupclassbanner == true) {
        //     $this->view->groupclassslider = $decoded->groupclassslider;
        //     $this->view->groupclassbanners = $decoded->groupclassbanners;
        // }
    }

    public function successAction()
    {
        $this->angularLoader(array(
            'imagesloaded' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
            'imagefill' => '/fe/scripts/others/jquery-imagefill.js',
            'resizeimagetoparent' => '/fe/scripts/others/jquery.resizeimagetoparent.min.js',
            'matchheight' => '/vendors/matchHeight/jquery.matchHeight-min.js',
            'imagefillcustom' => '/fe/scripts/others/imagefill.js',
            'matchheightcustom' => '/fe/scripts/others/matchheightcustom.js',
            'icheck' => '/vendors/iCheck/icheck.js',
        ));

        $transactionid = $_GET['tx'];
        $data = explode('|', $_GET['cm']);
        $moduletype = $data[0];

        $decoded = $this->curl("/fe/starterspackage/getsuccesstransactionAction/". $transactionid."/".$moduletype);
        if($decoded != NULL){
          $this->view->transactionresult = $decoded;
        }
        else {
            // $this->route404();
            // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        }

    }

    public function beneplaceAction()
    {
    	$this->angularLoader(array(
    		'starterspackage' => '/fe/scripts/controllers/starterspackage/beneplace.js',
    		'starterspackagefactory' => '/fe/scripts/factory/starterspackage/beneplace.js',
    		'mapFactory' =>'/fe/scripts/factory/map-fac.js',
        'icheckdirective' => '/fe/scripts/directives/icheck.js',
        'resizeimagetoparent' => '/fe/scripts/others/jquery.resizeimagetoparent.min.js',
        'imagesloaded' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
        'imagefill' => '/fe/scripts/others/jquery-imagefill.js',
        'matchheight' => '/vendors/matchHeight/jquery.matchHeight-min.js',
        '/fe/scripts/others/matchheightcustom.js',
        'imagefillcustom' => '/fe/scripts/others/imagefill.js',
        'matchheightcustom' => '/fe/scripts/others/matchheightcustom.js',
        'icheck' => '/vendors/iCheck/icheck.js',
        ));

        $decoded = $this->curl("/fe/tryaclass/index");

        $this->view->outeroneclassbanner = $decoded->outeroneclassbanner;
        if($decoded->outeroneclassbanner == true) {
          $this->view->outeronebanner = $decoded->outeronebanner;
        }

        $this->view->inneroneclassbanner = $decoded->inneroneclassbanner;
        if($decoded->inneroneclassbanner == true) {
           $this->view->inneronebanner = $decoded->inneronebanner;
        }

        $this->view->outergroupclassbanner = $decoded->outergroupclassbanner;
        if($decoded->outergroupclassbanner == true) {
           $this->view->outergroupbanner = $decoded->outergroupbanner;
        }

        $this->view->innergroupclassbanner = $decoded->innergroupclassbanner;
        if($decoded->innergroupclassbanner == true) {
           $this->view->innergroupbanner = $decoded->innergroupbanner;
        }

        // $this->view->oneclassbanner = $decoded->oneclassbanner;
        // if($decoded->oneclassbanner == true) {
        //     $this->view->oneclassslider = $decoded->oneclassslider;
        //     $this->view->oneclassbanners = $decoded->oneclassbanners;
        // }
        //
        // $this->view->groupclassbanner = $decoded->groupclassbanner;
        // if($decoded->groupclassbanner == true) {
        //     $this->view->groupclassslider = $decoded->groupclassslider;
        //     $this->view->groupclassbanners = $decoded->groupclassbanners;
        // }
    }
}
