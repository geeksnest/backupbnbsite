<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class IndexController extends ControllerBase
{
    public function indexAction()
    {
      	$this->view->sliderimages = $this->curl("/home/slider");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function homeAction()
    {
        // $this->view->script_google = $this->curl('/settings/script'); nasa ControllerBase
        $decoded = $this->curl("/home/slider");
        $this->view->hasImage = $decoded->hasImage;
        $this->view->sociallinks = $decoded->sociallinks;
        if($decoded->hasImage == true) {
          $this->view->sliderimages = $decoded->images;
        }
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function route404Action()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}
