<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class PressController extends ControllerBase{
    public function indexAction($page) {
        $this->view->activepage = 'press';
        $decoded = $this->curl("/fe/loadpress/". $page);

        if($decoded){
            foreach ($decoded->presspage as $key => $value) {
                if(substr($value->thumbnail, 0, 7) == '<iframe'){
                    $decoded->presspage[$key]->thumnailtype = 'video';
                    $decoded->presspage[$key]->thumbnail = str_replace('style="width:500px; height:300px;"', 'style="width:100%; height:320px; margin: auto; display: block;"', $value->thumbnail);
                } else {
                    $decoded->presspage[$key]->thumnailtype = 'image';
                }
            }
            $this->view->datanewslist = $decoded->presspage;
            $itemperpage = 9;
            $this->view->page = $page;
            $this->view->totalpage = ceil($decoded->total / $itemperpage);
        }

        $this->angularLoader(array(
            '/fe/scripts/controllers/press/press.js',
            '/fe/scripts/factory/press/f_press.js'
        ));

        $this->view->titletag = "Yoga Classes combining Tai Chi, Meditation | Body & Brain";
    }
    public function readAction($slug){
        $this->view->activepage = 'press';
    	  $this->view->slug = $slug;

        $decoded = $this->curl("/read/presspage/".$slug);

        $fortitletag = $decoded->title;
        $fordescriptiontag = $decoded->newsdescription;
        $forfacebookmetaurl = "/read-press/".$slug;
        $forfacebookmetatitle = $decoded->title;
        $forfacebookmetadescription = $decoded->newsdescription;
        $forfacebookmetaimage = "/uploads/newsimage/".$decoded->logo;

        $this->globalmetatags($fortitletag, $fordescriptiontag, $forfacebookmetaurl, $forfacebookmetatitle, $forfacebookmetadescription,$forfacebookmetaimage);

        $this->angularLoader(array(
            '/fe/scripts/controllers/press/read.js',
            '/fe/scripts/factory/press/f_press.js'
        ));
    }

}
