<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class TestimoniesController extends ControllerBase
{
    public function indexAction($offset)
    {
      if( is_numeric($offset) ==true ) {
        $decoded = $this->curl("_testimonies/index/list/". $offset);
        if($decoded->error == false) {
          $this->angularLoader(array(
            'imagesLoaded' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
            'imagefillJquery' => '/fe/scripts/others/jquery-imagefill.js',
            'imagefillExt' => '/fe/scripts/others/imagefill.js',
            'searchStoryCtrl' => '/fe/scripts/controllers/testimonies/searchStoryCtrl.js',
            'shareStoryCtrl' => '/fe/scripts/controllers/testimonies/shareStoryCtrl.js',
            'StoryFactory' => '/fe/scripts/factory/testimonies/ShareStoryFactory.js',
            'LocIndexFactory' => '/fe/scripts/factory/location/loc.index.js',
            'UploadFactory' => '/fe/scripts/factory/UploadFactory.js'
          ));

          $fortitletag = "Body & Brain Success Stories: Stress, Pain, Flexibility, Headache";
          $fordescriptiontag = "The benefits of Body & Brain yoga are various. Many Body & Brain yoga practitioners are saying that they got lots of physical and mental benefits.";
          $forfacebookmetaurl = "/success-stories/".$offset;
          $forfacebookmetatitle = "Body & Brain Success Stories: Stress, Pain, Flexibility, Headache";
          $forfacebookmetadescription = "The benefits of Body & Brain yoga are various. Many Body & Brain yoga practitioners are saying that they got lots of physical and mental benefits.";
          $forfacebookmetaimage = "/uploads/testimonialimages/testimonialBanner.png";
          $this->globalmetatags($fortitletag, $fordescriptiontag, $forfacebookmetaurl, $forfacebookmetatitle, $forfacebookmetadescription,$forfacebookmetaimage);

          $this->view->storiesByTen = $decoded->storiesByTen;
          $this->view->storiesImg = $this->config->application->amazonlink . "/uploads/testimonialimages";

          $itemperpage = 10;
          $this->view->page = $offset;
          $this->view->totalpage = ceil($decoded->storiesCount / $itemperpage);
          $this->view->paginationUrl = "success-stories";
        } else {
          $this->route404();
        }
      } else {
        $this->route404();
      }
    }

    public function searchresultAction($keyword, $offset)
    {

      $this->view->keyword = $keyword;
      $this->view->page = $offset;
      if( is_numeric($keyword) == false && is_numeric($offset) == true ) {

        $keyword = str_replace(" ","%20",$keyword); //for keywords that has space for CURL coz curl not accepting whitespaces
        $decoded = $this->curl("_testimonies/searchresult/list/".$keyword."/".$offset);
        $this->view->nulldata = $decoded->nulL;

        $this->angularLoader(array(
          'imagesLoaded' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
          'imagefillJquery' => '/fe/scripts/others/jquery-imagefill.js',
          'imagefillExt' => '/fe/scripts/others/imagefill.js',
          'searchStoryCtrl' => '/fe/scripts/controllers/testimonies/searchStoryCtrl.js',
          'shareStoryCtrl' => '/fe/scripts/controllers/testimonies/shareStoryCtrl.js',
          'StoryFactory' => '/fe/scripts/factory/testimonies/ShareStoryFactory.js',
          'UploadFactory' => '/fe/scripts/factory/UploadFactory.js',
          'directives' => '/fe/scripts/directives/directives.js',
          'LocIndexFactory' => '/fe/scripts/factory/location/loc.index.js'
        ));

        if($decoded->nulL == false) {
          $fortitletag = "Body & Brain Success Stories: Stress, Pain, Flexibility, Headache | ".$keyword;
          $fordescriptiontag = "The benefits of Body & Brain yoga are various. Many Body & Brain yoga practitioners are saying that they got lots of physical and mental benefits.";
          $forfacebookmetaurl = "/success-stories/".$keyword."/".$offset;
          $forfacebookmetatitle = "Body & Brain Success Stories: Stress, Pain, Flexibility, Headache | ".$keyword;
          $forfacebookmetadescription = "The benefits of Body & Brain yoga are various. Many Body & Brain yoga practitioners are saying that they got lots of physical and mental benefits.";
          $forfacebookmetaimage = "/uploads/testimonialimages/testimonialBanner.png";
          $this->globalmetatags($fortitletag, $fordescriptiontag, $forfacebookmetaurl, $forfacebookmetatitle, $forfacebookmetadescription,$forfacebookmetaimage);

          $this->view->storiesByTen = $decoded->storiesByTen;
          $this->view->storiesImg = $this->config->application->amazonlink . "/uploads/testimonialimages";

          $itemperpage = 10;
          $this->view->page = $offset;
          $this->view->totalpage = ceil($decoded->storiesCount / $itemperpage);
          $this->view->paginationUrl = "success-stories/".$keyword;
        }
      } else {
        $this->route404();
      }
    }

    public function detailsAction($ssid, $subject) {
      if(is_numeric($ssid) == true) {
        $this->angularLoader(array(
          'imagesLoaded' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js',
          'imagefillJquery' => '/fe/scripts/others/jquery-imagefill.js',
          'imagefillExt' => '/fe/scripts/others/imagefill.js',
          'searchStoryCtrl' => '/fe/scripts/controllers/testimonies/searchStoryCtrl.js',
          'shareStoryCtrl' => '/fe/scripts/controllers/testimonies/shareStoryCtrl.js',
          'StoryFactory' => '/fe/scripts/factory/testimonies/ShareStoryFactory.js',
          'UploadFactory' => '/fe/scripts/factory/UploadFactory.js',
          'directives' => '/fe/scripts/directives/directives.js',
          'LocIndexFactory' => '/fe/scripts/factory/location/loc.index.js'
        ));
        $decoded = $this->curl("_testimonies/details/".$ssid);
        if($decoded->error == false) {
          $this->view->storiesImg = $this->config->application->amazonlink . "/uploads/testimonialimages";
          $this->view->story = $decoded->story;

          $fortitletag = $decoded->story->subject;
          $fordescriptiontag = "";
          $forfacebookmetaurl = "/success-stories/details/".$ssid."/".$subject;
          $forfacebookmetatitle = $decoded->story->subject .  "wew";
          $forfacebookmetadescription = $decoded->story->metadesc;
          $forfacebookmetaimage = "/uploads/testimonialimages/".$decoded->story->photo;

          $this->globalmetatags($fortitletag, $fordescriptiontag, $forfacebookmetaurl, $forfacebookmetatitle, $forfacebookmetadescription,$forfacebookmetaimage);
        } else {
          $this->route404();
        }
      } else {
        $this->route404();
      }
    }

}
