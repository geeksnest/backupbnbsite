<?php

namespace Modules\Frontend;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
class Module
{

	public function registerAutoloaders()
	{

		$loader = new \Phalcon\Loader();

		$loader->registerNamespaces(array(
			'Modules\Frontend\Controllers' => __DIR__ . '/controllers/',
			'Modules\Frontend\Models' => __DIR__ . '/models/',
		));

		$loader->register();
	}

	public function registerServices($di)
	{

		/**
		 * Read configuration
		 */
		$config = include __DIR__ . "/config/config.php";

		$di['dispatcher'] = function() {
			$dispatcher = new \Phalcon\Mvc\Dispatcher();
			$dispatcher->setDefaultNamespace("Modules\Frontend\Controllers");
			return $dispatcher;
		};

		/**
		 * Setting up the view component
		 */
		$di['view'] = function() {
			$view = new \Phalcon\Mvc\View();

			$view->setViewsDir(__DIR__ . '/views/');
			$view->setLayoutsDir('layouts/');
			$view->setTemplateAfter('main');

			    $view->registerEngines(array(
			        '.volt' => function ($view, $di){

			            $volt = new VoltEngine($view, $di);

			            $volt->setOptions(array(
			                'compiledPath' => __DIR__ . '/cache/',
			                'compiledSeparator' => '_'
			            ));

			            $compiler = $volt->getCompiler();
			            $compiler->addFunction('strtotime', 'strtotime');

			            return $volt;
			        },
			        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
			    ));		    
			return $view;
		};

		/**
		 * Database connection is created based in the parameters defined in the configuration file
		 */
		$di['db'] = function() use ($config) {
			return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
				"host" => $config->database->host,
				"username" => $config->database->username,
				"password" => $config->database->password,
				"dbname" => $config->database->name
			));
		};

	}

}