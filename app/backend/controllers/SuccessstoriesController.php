<?php

namespace  Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class SuccessstoriesController extends ControllerBase {
	public function managestoriesAction() {
		 $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}

	public function viewstoryAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
}

