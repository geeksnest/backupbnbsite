<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;


class TestController extends ControllerBase{
    public function esignatureAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}
