<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Modules\Backend\Forms\CreateuserForm;
use Modules\Backend\Models\Users as Users;

class CenterController extends ControllerBase
{
    public function indexAction()
    {

    }
    public function createcenterAction()
    {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function managecenterAction()
    {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function editcenterAction()
    {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function centerviewAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function sliderAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function calendarAction()
    {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function detailsAction()
    {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function specialofferAction()
    {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function pricingAction()
    {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function successAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function scheduleAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function managenewsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function addnewsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function editnewsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function regionanddistrictmanagementAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function introsessionAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function groupclasssessionAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function globalpricingAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function beneplaceAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function allbeneplaceAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function allbeneplaceunverifiedAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function allbeneplacecancelledAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function allintrosessionAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function allintrosession2Action()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function allintrosessioncanceledAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function allgroupclasssessionAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function allgroupclasssession2Action()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function allgroupclasssessioncanceledAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function cellAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function socialmediaAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function contactAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function spnlAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function hoursAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function descriptionAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function beneplacepricingAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function mycenterAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}
