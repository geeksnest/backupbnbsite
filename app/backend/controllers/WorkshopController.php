<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class WorkshopController extends ControllerBase {
	public function indexAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function createworkshopAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function editvenueAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function editworkshopAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function workshoptitlesAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function registrantsAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function createvenueAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function managevenueAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function manageworkshopsAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function workshoprelatedAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function workshopsettingsAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	public function new_wstitleAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
}
