<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Phalcon\Tag;
use Modules\Backend\Models\Users as Users;

class IndexController extends ControllerBase{
    public function initialize()
    {
        parent::initialize();
    }
    public function indexAction()
    {
        $auth = $this->session->get('auth');
        if ($auth){ $this->response->redirect('bnbadmin/admin'); }
        ////////////////////////////////////////////////////////////////////
        $this->view->error = null;
        if ($this->request->isPost()) {
            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
            $shapass='';
            //$hashpass = $this->security->hash($password);
            $service_url = $this->config->application->ApiURL .'/user/login/'.$username.'/'.$password;
            $curl = curl_init($service_url);
            curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $curl_response = curl_exec($curl);
            if ($curl_response === false) {
                $info = curl_getinfo($curl);
                curl_close($curl);
                die('error occured during curl exec. Additional info: ' . var_export($info));
            }
            curl_close($curl);
            $decoded = json_decode($curl_response);
            if(@$decoded->error){
                echo $decoded->error;
            }else{
                // $this->_registerSession($decoded->success);

                $this->_registerSession($decoded->success->id, $decoded->token);
                $this->response->redirect('bnbadmin/admin');
            }
        }

         $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

    }

    private function _registerSession($userid, $token)
    {
        $service_url = $this->config->application->ApiURL .'/loginuser/centermanager/'.$userid;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        if($decoded->user != false) {
            if($decoded->center){
              $centerid = $decoded->center->centerid;
            } else {
              $centerid = null;
            }
            
            $this->session->set('auth', array(
                'bnb_userid' => $decoded->user->id,
                'pi_fullname' => $decoded->user->first_name .' '. $decoded->user->last_name,
                'pi_username' => $decoded->user->username,
                'pi_userrole' => $decoded->user->task,
                'pi_profile_pic_name' => $decoded->user->profile_pic_name,
                'bnb_centerid' => $centerid,
                'token' => $token
            ));

            //Set SuperAdmin
            // if($user->userLevel){
                $this->session->set('SuperAgent', true );
            // }
        }
    }
}
