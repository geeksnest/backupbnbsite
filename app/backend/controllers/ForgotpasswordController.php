<?php


namespace Modules\Backend\Controllers;

use Phalcon\Http\Response;
use Phalcon\Mvc\View;

class ForgotpasswordController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function changepasswordAction($email,$token)
    {
        $this->view->email = $email;
        $this->view->token = $token;
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}

