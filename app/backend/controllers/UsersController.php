<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Modules\Backend\Forms\CreateuserForm;
use Modules\Backend\Models\Users as Users;

class UsersController extends ControllerBase
{
    public function indexAction()
    {

    }
    public function createAction()
    {
    	
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function createSaveAction(){
     
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
    }

    public function userlistAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
       
    }
     public function edituserAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
       
    }
    public function editprofileAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
       
    }
    public function changepasswordAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
       
    }

}

