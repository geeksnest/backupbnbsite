<?php

namespace Modules\Backend\Controllers;
use Phalcon\Mvc\View;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    protected function initialize()
    {
        $this->tag->setTitle("Body and Brain");
        $this->view->username = $this->session->get('auth');

        $this->createJsConfig();
        $this->view->amazonlink = $this->config->application->amazonlink;
    }
    private function createJsConfig(){
        $script="app.constant('Config', {
// This is a generated Config
// Any changes you make in this file will be replaced
// Place you changes in app/config.php file \n";
            foreach( $this->config->application as $key=>$val){
                $script .= $key . ' : "' . $val .'",' . "\n";
            }
        $script .= "});";
        $fileName="../public/be/js/scripts/config.js";
        $fileName1="../public/fe/scripts/config.js";

        $exist =  file_get_contents($fileName);
        if($exist == $script){

        }else{
            file_put_contents($fileName, $script);
            file_put_contents($fileName1, $script);
        }

    }
}
