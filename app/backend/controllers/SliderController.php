<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;


class SliderController extends ControllerBase{
    public function galleryAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}

