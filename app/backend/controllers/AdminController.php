<?php


namespace Modules\Backend\Controllers;

use Phalcon\Http\Response;
use Phalcon\Mvc\View;

class AdminController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->request->isPost()) {
            die('BOOM!');    
        }

    }
    public function dashboardAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function boomAction()
    {
        echo "boom";
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);   
    }
     public function logoutAction()
    {
        $this->session->destroy();
        $this->flash->warning('You have successfully logout.');
        $this->view->pick("index/index");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
      
    }

}

