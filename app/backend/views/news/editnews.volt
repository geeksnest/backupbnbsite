{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="newsimagelist.html">
  <div ng-include="'/be/tpl/newsimagelist.html'"></div>
</script>
<script type="text/ng-template" id="newsimagelist2.html">
  <div ng-include="'/be/tpl/newsimagelist2.html'"></div>
</script>
<script type="text/ng-template" id="imagesrc.html">
  <div ng-include="'/be/tpl/imagesrc.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>

<fieldset ng-disabled="isSaving">
<form  id="editMainnewsForm" name="editMainnewsForm" ng-submit="saveNews(news)" >

<div class="bg-light lter b-b wrapper-md">
  <span class="m-n font-thin h3">Edit News</span>
  <a id="top"></a>
  <span class="pull-right">
    <a ui-sref="managenews({ page : page })" class="btn btn-default" title="Back/Abort"> <i class="fa fa-mail-reply"></i> </a>
    <button type="submit" class="btn btn-success" scroll-to="Scrollup"><i class="fa fa-save"></i></button>
  </span>
</div>


  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" id="updatenoti" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-8">
          <div class="panel panel-default">

            <div class="panel-heading font-bold">
              News Information
            </div>

              <input type="hidden"  ng-model="news.newsid">
              <input type="hidden"  ng-model="news.featurednews">
              <input type="hidden"  ng-model="news.datecreated">

              <div class="panel-body">

                <div class="form-group col-md-12">
                  <label>Title</label>
                  <input type="text" id="title" name="title" class="form-control" ng-model="news.title" ng-change="SEO(news.title)" required/>
                </div>

                <div class="form-group col-md-12">
                  <label>SEO: </label>
                  <input type="text" id="newsslugs" class="form-control" ng-model="news.slugs" ng-change="SEO(news.slugs)" required />
                </div>

                {#<div class="col-md-12">
                  <b>News Slugs: </b>
                  <input type="text" ng-show="editslug" id="pageslugs" ng-model="news.slugs" ng-keypress="onslugs(news.slugs)" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern">
                  <span ng-bind="news.slugs"></span>
                  <div class="popOver" ng-show="invalidtitle">
                      Slugs is already taken.
                      <span class="pop-triangle"></span>
                  </div>
                  <div ng-show="editslug" class="ng-hide">
                    <a class="btn btn-danger btn-xs pull-right mg-left" ng-click="cancelnewsslug(news.title)">cancel</a>
                    <a class="btn btn-primary btn-xs pull-right mg-left" ng-click="setslug(news.slugs)">ok</a>
                  </div>
                  <a class="btn btn-danger btn-xs pull-right mg-left" ng-hide="editslug" ng-click="clearslug(news.title)">clear</a>
                  <a class="btn btn-primary btn-xs pull-right mg-left" ng-hide="editslug" ng-click="editnewsslug()">edit slug</a>
                  <br><br>
                </div>#}

<!--                 <div class="col-md-12">
                  <b>Meta Tags
                  <em class="text-muted pull-right">(Optional)</em>
                  </b>
                  <input type="text" name="metatags" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.metatags">
                  <span class="help-block m-b-none">Use comma (,) for multiple meta tags. E.g. (focus, strength, peace)</span>
                  <br>
                </div> -->

                <div class="form-group col-md-6">
                  <label>News Location</label>
                  <div ui-module="select2">
                    <select ui-select2 name="newslocation" ng-model="news.newslocation" class="form-control" required>
                      <option value="mainsite">Main Site</option>
                    </select>
                  </div>
                </div>

                <div class="form-group col-md-6">
                  <div ng-if='news.newslocation =="mainsite"'>
                  <label>Category</label>
                  <select name="category" class="form-control m-b" ng-model="news.category" ng-required='news.newslocation =="mainsite"' ng-options="cat.categoryname for cat in category track by cat.categoryid">
                  </select>
                  </div>
                </div>

                <div class="form-group col-md-12">
                  <b>Body Content
                  <em class="text-danger pull-right" ng-show="formpage.body.$error.required">(Required)</em>
                  </b>
                  <a class="btn btn-default btn-sm pull-right" ng-click="imagegallery('lg')">
                    <i class="fa fa-file-image-o text"></i>
                    Image Gallery
                  </a>
                  <br><br>
                  <textarea name="body" class="ck-editor" ng-model="news.body" required></textarea>
                </div>

                <div class="form-group col-md-12">
                  <label>Author</label>
                  <em class="text-muted pull-right">(Optional)</em>
                  <input type="text" class="form-control" ng-model="news.author" name="news.author" >
                </div>

              </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Date Published
              <em class="text-danger pull-right" ng-show="formpage.date.$error.required">(Required)</em>
              <em class="text-danger pull-right" ng-show="formpage.date.$invalid">(A valid date is required)</em>
            </div>
            <div class="panel-body">

              </b>
              <div class="input-group">
                <span class="input-group-btn">
                  <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy"
                    style="max-width:80%"
                    ng-model="news.date"
                    is-open="opened"
                    datepicker-options="dateOptions"
                    ng-required="true"
                    close-text="Close"
                    type="text"
                    disabled>
                  <button type="button" class="btn btn-default" ng-click="open($event)" style="width:20%"><i class="glyphicon glyphicon-calendar"></i></button>
                </span>
              </div>
            </div>
          </div>

          <div class="panel panel-default">

            <div class="panel-heading font-bold">News Banner</div>
            <div class="panel-body">
              <div class="form-group">
                <div class="input-group m-b">
                  <span class="input-group-btn">
                    <a class="btn btn-default"  ng-click="showimageList('lg', 'banner')">Select Image</a>
                  </span>
                  <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">
                  <input type="text" name="banner" class="form-control" ng-value="news.banner = amazonpath " ng-model="news.banner" placeholder="{[{amazonpath}]}" ng-required='news.newslocation == "mainsite"' readonly />
                </div>
                <div>
                  <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/newsimage/{[{news.banner}]}" width="100%" height="100%" alt="IMAGE PREVIEW" ng-show="news.banner != ''">
                </div>
              </div>
            </div>
            <div class="panel-footer">
              <span class="text-muted">A ratio of 3W x 8L is the recommended image size for better viewing in the frontend.</span>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">News Thumbnail</div>
            <div class="panel-body">
              <div class="form-group">
                <div class="input-group m-b">
                  <span class="input-group-btn">
                    <a class="btn btn-default"  ng-click="showimageList('lg', 'thumbnail')">Select Image</a>
                  </span>
                  <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">
                    <input type="text" name="thumbnail" class="form-control" ng-value="news.thumbnail = amazonpath2 " ng-model="news.thumbnail" placeholder="{[{amazonpath2}]}" ng-required='news.newslocation == "mainsite"' readonly />
                </div>
                <div>
                  <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/newsimage/{[{news.thumbnail}]}" width="100%" height="100%" alt="IMAGE PREVIEW" ng-show="news.thumbnail != ''">
                </div>
              </div>
            </div>
            <div class="panel-footer">
              <span class="text-muted">A ratio of 2W x 3L is the recommended image size for better viewing in the frontend.</span>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading"><b>Meta Properties</b> <em class="text-muted pull-right">(Optional)</em></div>
            <div class="panel-body">
              <div class="form-group">
                <label>Title</label>
                <input type="text" name="metatitle" class="form-control" ng-model="news.metatitle">
              </div>

              <div class="form-group">
                <label>Description</label>

                <textarea class="form-control" rows="4" ng-model="news.description" maxlength="350"></textarea>
              </div>

            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold" >
              News Status
            </div>
            <div class="panel-body">
              <div class="form-group" >
                  <label class="col-sm-3 control-label">
                    <span class="label bg-info" ng-show="news.status == 1">Online</span>
                    <span class="label bg-danger" ng-show="news.status == 0">404</span>
                  </label>

                  <label class="i-switch bg-info m-t-xs m-r" style="margin-top:1px">
                    <input type="checkbox" ng-true-value="'1'"  ng-false-value="'0'" ng-model="news.status">
                    <i></i>
                  </label>
              </div>
            </div>
          </div>
        </div>

        {#<div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              <em class="text-danger" ng-show="formpage.$invalid || formpage.$pending || invalidtitle==true">Please fill all required fields before submitting</em>
            </div>
            <div class="panel-body">
              <div class="form-group" >
                <a ui-sref="managenews({ page : page })" class="btn btn-default"> Close </a>
                <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid || formpage.$pending || invalidtitle==true" scroll-to="Scrollup">Submit</button>
              </div>
            </div>
          </div>
        </div>#}

<!--         <div class="col-sm-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <strong>Workshop Titles</strong>
            </div>
            <div class="panel-body">
              <form name="relatedform">
                <span ng-repeat="title in workshoptitles" ng-model="title" class="btn m-b-xs btn-sm btn-default btn-rounded btn-addon margin_right_5px" ng-click="addrelated(title)"><i class="fa fa-plus pull-right"></i>{[{title}]}</span>
              </form>
            </div>
          </div>
          <div class="panel panel-primary">
            <div class="panel-heading">
              <strong>Related Workshop</strong>
            </div>
            <div class="panel-body">
              <form name="relatedform">
                <span ng-repeat="title in related track by $index" ng-hide="empty" ng-model="titles" class="btn m-b-xs btn-sm btn-success btn-rounded btn-addon margin_right_5px" ng-click="removerelated(title, related)"><i class="fa fa-minus pull-right"></i>{[{title}]}</span>
                <span ng-show="empty">Empty</span>
              </form>
            </div>
          </div>
        </div> -->

      </div>

  </div>
</form>
</fieldset>
