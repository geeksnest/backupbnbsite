{{ content() }}

<script type="text/ng-template" id="pageimagelist.html">
  <div ng-include="'/be/tpl/pageimagelist.html'"></div>
</script>

<form id="formpage" ng-submit="savePage(page)" name="formpage">

<div class="bg-light lter b-b wrapper-md">
  <span class="m-n font-thin h3">Edit Page</span>
  <a id="top"></a>
  <span class="pull-right">
    <a ui-sref="managepage" class="btn btn-md btn-primary" title="Pages List"><i class="fa fa-list-alt"></i></a>
    <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid"><i class="fa fa-save"></i></button>
  </span>
</div>

<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page Information
            </div>
              <div class="panel-body">
              <input type="hidden" id="title" name="title" ng-model="page.pageid" >
                Title
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required" ng-keyup="onpagetitle(page.title)">
                <br>
                <b>Page Slugs: </b>

                <input type="text" class="form-control" ng-model="page.slugs" required>
                <!-- <span ng-bind="page.slugs"></span> -->

                {#<div class="line line-dashed b-b line-lg"></div>

                 Meta Title
                <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.metatitle" required="required">
                <br>
                <div class="line line-dashed b-b line-lg"></div>
                Meta Tags
                <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.metatags" required="required">
                <span class="help-block m-b-none">Use comma (,) for multiple meta tags. E.g. (initial awakening, sedona healing, dahn yoga)</span>
                <br>
                <div class="line line-dashed b-b line-lg"></div>

                Meta Description
                <!-- <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.metadesc"> -->
                <textarea class="form-control resize_vert" ng-model="page.metadesc" rows="3"></textarea>
                <br>#}

                <!-- <div class="line line-dashed b-b line-lg"></div>
                Page Banner
                <input type="text" ng-model="page.pagebanner" required="required" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" disabled="">
                <div class="pagebanner" style="background-image: url('{[{amazonlink}]}/uploads/pageimage/{[{imagefilename}]}')">
                  <div class="pagebannereditbutton" ng-click="showimageList('lg')">
                    <i class="fa fa-edit"></i> <small>Select Page Banner</small>
                  </div>
                  <div class="pagebannerheader" ng-bind="page.title"></div>
                </div> -->

                <div class="line line-dashed b-b line-lg"></div>
                Body Content
                <textarea class="ck-editor" ng-model="page.body" required="required"></textarea>
              </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Meta Details
            </div>
            <div class="panel-body">
              Meta Title
             <input type="text" class="form-control" ng-model="page.metatitle">
             <br>

             Meta Tags
             <input type="text" class="form-control" ng-model="page.metatags">
             <span class="help-block m-b-none">Use comma (,) for multiple meta tags. E.g. (initial awakening, sedona healing, dahn yoga)</span>
             <br>

             Meta Description
             <textarea class="form-control resize_vert" ng-model="page.metadesc" rows="3"></textarea>
            </div>
          </div>
        </div>


        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Facebook Custom Audience Pixel Code
            </div>
            <div class="panel-body">
              <textarea class="form-control" rows="4" placeholder="Paste your Code Here" ng-model="page.fbcapc"></textarea>
            </div>
          </div>
        </div>


      </div>


      {#<div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="managepage" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Submit</button>
            </footer>
        </div>
      </div>#}


      <div  class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Image Gallery
              </div>




                <div class="panel-body">
                  <alert ng-repeat="alert in alertss" type="{[{alert.type }]}" close="closeAlerts($index)">{[{ alert.msg }]}</alert>
                  <div class="loader" ng-show="imageloader">
                    <div class="loadercontainer">

                      <div class="spinner">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                        <div class="rect5"></div>
                      </div>
                      Uploading your images please wait...

                    </div>

                  </div>

                  <div ng-show="imagecontent">

                    <div class="col-sml-12">
                      <div class="dragdropcenter">
                        <div ngf-drop ngf-select ng-model="files" class="drop-box"
                        ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
                        accept="image/*,application/pdf">Drop images here or click to upload</div>
                      </div>
                    </div>

                    <div class="line line-dashed b-b line-lg"></div>
                    <div ui-jq="slimScroll" ui-options="{height:'500px', size:'8px'}">
                      <div class="col-sm-3" ng-repeat="data in imagelist">
                        <input type="text" id="title" name="title" class="form-control" value="<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{data.filename}]}" onClick="this.setSelectionRange(0, this.value.length)">
                        <div class="imagegallerystyle" style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/pageimage/{[{data.filename}]}');">
                        </div>
                      </div>
                    </div>

                  </div>


                </div>

          </div>
        </div>

      </div>

  </div>
</fieldset>
</form>
