<script type="text/ng-template" id="imageuploaderwithurl">
	<div ng-include="'/be/tpl/module/imageuploaderwithurl.html'"></div>
</script>
<script type="text/ng-template" id="confirm">
	<div ng-include="'/be/tpl/confirm.html'"></div>
</script>
<script type="text/ng-template" id="bannerurl">
	<div ng-include="'/be/tpl/bannerurl.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md ng-scope">
	<h1 class="m-n font-thin h3">Page Banner</h1>
	<a id="top"></a>
</div>

<div class="wrapper-md">
	<div class="panel panel-default">
		<div class="panel-heading font-bold">{[{ pageinfo.title + " images" }]}</div>
		<div class="panel-body">

		  	<alert ng-repeat="alert in imgalert" type="{[{alert.type }]}" close="closeImgAlert($index)">{[{ alert.msg }]}</alert>
		  	<div class="col-sm-12">
		  		<a ui-sref="managepage" class="btn m-b-xs btn-sm btn-default btn-addon pull-right">Cancel</a>
		  		<button class="btn m-b-xs btn-sm btn-primary btn-addon pull-right" ng-click="imageUploaderWithUrl(page)"><i class="fa fa-plus"></i>Upload New Images</button>
		  	</div>

		    <div class="col-sm-3" ng-repeat="banner in banners">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="imagelist" style="background:url('{[{amazonlink}]}/uploads/slidernbanner/{[{banner.image}]}') no-repeat center center; background-size:contain">
						</div>
					</div>
					<div class="panel-footer hiddenoverflow" style="text-align:right">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-sm btn-light" title="Click to toggle hide/show" ng-click="hideShowImage(banner.id)">
								<i class="fa fa-eye" ng-if="banner.status == 1"></i>
								<i class="fa fa-eye-slash" ng-if="banner.status != 1"></i>
							</button>
							<button type="button" class="btn btn-sm btn-info" title="Edit" ng-click="editImage(banner.id)" ><i class="icon icon-note"></i></button>
							<button type="button" class="btn btn-sm btn-danger" title="Remove" ng-click="removeImage(banner.id)" ><i class="icon icon-trash"></i></button>
							<select name="sequence" id="sequence" class="pull-right" ng-model="banner.sequence" ng-change="changeorder(banner)" style="height: 30px">
								<option ng-repeat="seq in sequence">{[{ seq }]}</option>
							</select>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
