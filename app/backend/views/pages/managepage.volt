{{ content() }}

<script type="text/ng-template" id="pageBanner.html">
  <div ng-include="'/be/tpl/pageBanner.html'"></div>
</script>

<script type="text/ng-template" id="pageDelete.html">
  <div ng-include="'/be/tpl/pageDelete.html'"></div>
</script>

<script type="text/ng-template" id="pageEdit.html">
  <div ng-include="'/be/tpl/pageEdit.html'"></div>
</script>


<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Page List</h1>
  <a id="top"></a>
</div>

<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Page List
            </div>


              <div class="panel-body">



                <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs">
                        <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>

                                <th style="width:25%">Title</th>
                                <th style="width:40%">Page Slugs</th>
                                <th style="width:20%">Page Status</th>
                                <th style="width:30%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="mem in data.data">

                                <td>{[{ mem.title }]}</td>
                                <td>{[{ mem.pageslugs }]}</td>
                                <td  ng-if="mem.status == 1">
                                <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Online</span></div>
                                <div class="checkstatuscontent">
                                  <label class="i-switch bg-info m-t-xs m-r">
                                    <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.pageid,searchtext)">
                                    <i></i>
                                  </label>

                                </div>
                                <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.pageid"><i class="fa fa-check"></i></spand></div>
                                </td>
                                <td  ng-if="mem.status == 0">
                                <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">404</span></div>
                                <div class="checkstatuscontent">
                                  <label class="i-switch bg-info m-t-xs m-r">
                                    <input type="checkbox" ng-click="setstatus(mem.status,mem.pageid,searchtext)">
                                    <i></i>
                                  </label>

                                </div>
                                 <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.pageid"><i class="fa fa-check"></i></spand></div>
                                </td>
                                </td>
                                <td>
                                    <a href="" ng-click="editpagebanner(mem.pageslugs)">
                                      <span class="btn btn-sm btn-default" title="Banner">
                                        <i class="glyphicon glyphicon-flag text-primary"></i>
                                      </span>
                                    </a>

                                    <a href="" ng-click="editpage(mem.pageid)">
                                      <span class="btn btn-sm btn-default" title="Edit">
                                        <i class="glyphicon glyphicon-edit text-info"></i>
                                      </span>
                                    </a>
                                    <a href="" ng-click="deletepage(mem.pageid)">
                                      <span class="btn btn-sm btn-default" title="Delete">
                                        <i class="glyphicon glyphicon-trash text-danger"></i>
                                      </span>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>



              </div>


          </div>
        </div>

      </div>

      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
</fieldset>
