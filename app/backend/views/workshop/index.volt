<script type="text/ng-template" id="success.html">
  <div ng-include="'/be/tpl/success.html'"></div>
</script>
<script type="text/ng-template" id="workshopvenueedit.html">
  <div ng-include="'/be/tpl/workshopvenueedit.html'"></div>
</script>
<script type="text/ng-template" id="workshopvenuedelete.html">
  <div ng-include="'/be/tpl/workshopvenuedelete.html'"></div>
</script>
<script type="text/ng-template" id="workshoptitleremove.html">
  <div ng-include="'/be/tpl/workshoptitleremove.html'"></div>
</script>
<script type="text/ng-template" id="workshopremove.html">
  <div ng-include="'/be/tpl/workshopremove.html'"></div>
</script>
<script type="text/ng-template" id="workshopedit.html">
  <div ng-include="'/be/tpl/workshopedit.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Welcome to Workshop</h1>
  <a id="top"></a>
</div>

<div class='contrainer'>
	<div class='row wrapper-md'>
		<div class="col-sm-12">
		<tabset class="tab-container">
          <tab>
          	<tab-heading class="font-bold">Workshop Venue</tab-heading>
          	<div class="panel b-a">
	          	<div class="panel-heading b-b b-light">
	          		<strong>Create Venue</strong>
	          		<button class="btn btn-primary btn-xs pull-right" ng-init="isCollapsed = true" ng-click="isCollapsed = !isCollapsed">Create Venue</button>
	          	</div>
	          	<div collapse="isCollapsed" class="panel-body">
	          		<form name="venueform" class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="createvenue(venue)">
	          			<div class="col-sm-6">
										<div class="form-group">
											<label class="col-md-3 control-label">Venue Name</label>
											<div class="col-md-9">
												<input type="text" name="venuename" class="form-control venue" placeholder="Venue Name" ng-model="venue.venuename" ng-change="checkvenuename(venue.venuename)" required>
												<div class="popOver" ng-show="validvenue" >
													Venue name is already taken.
													<span class="pop-triangle"></span>
												</div>
												<span class="help-block m-b-none">Example:Sedona Mago Retreat, Honor's Haven</span>

											</div>
										</div>

										<div class="line line-lg"></div>

										<div class="form-group">
											<label class="col-md-3 control-label">State</label>
											<div class="col-md-9">
												<div ui-module="select2">
													<select ui-select2 class="form-control"  ng-model="venue.SPR"  ng-change="statechange(venue.SPR)" required="required">
														<option ng-repeat="list in centerstate" ng-value="list.state_code">{[{list.state}]}</option>

													</select>
												</div>
											</div>
										</div>

										<div class="line line-lg"></div>

										<div class="form-group">
											<label class="col-md-3 control-label">City</label>
											<div class="col-md-9">
												<div ui-module="select2">
													<select ui-select2 class="form-control" ng-model="venue.city" ng-change="citychange(venue.city)" required="required">
														<option ng-repeat="list in centercity track by $index" ng-value="list.city">{[{list.city}]}</option>
													</select>
												</div>
											</div>
										</div>

										<div class="line line-lg"></div>

										<div class="form-group">
											<label class="col-md-3 control-label">Zip Code</label>
											<div class="col-md-9">
												<div ui-module="select2">
													<select ui-select2 class="form-control" ng-model="venue.zipcode"  required="required">
														<option ng-repeat="list in getcenterzip track by $index" ng-value="list.zip">{[{list.zip}]}</option>
													</select>
												</div>
											</div>
										</div>

										<div class="line line-lg"></div>

										<div class="form-group">
											<label class="col-md-3 control-label">Country</label>
											<div class="col-md-9">
												<input type="text" class="form-control venue" ng-model="venue.country" required>
											</div>
										</div>

										<div class="line line-lg"></div>

										<div class="form-group">
											<label class="col-md-3 control-label">Address 1</label>
											<div class="col-md-9">
												<input type="text" class="form-control venue" placeholder="Address 1" ng-model="venue.address1" required>
												<span class="help-block m-b-none">Street address, P.O. box, company name, c/o </span>
											</div>
										</div>

										<div class="line line-lg"></div>

										<div class="form-group">
											<label class="col-md-3 control-label">Address 2</label>
											<div class="col-md-9">
												<input type="text" class="form-control venue" placeholder="Apartment, suite, unit, building, floor, etc." ng-model="venue.address2">
											</div>
										</div>


									</div>

									<div class="col-sm-6">

										<div class="form-group">
											<label class="col-md-3 control-label">Website</label>
											<div class="col-md-9">
												<input type="url" class="form-control venue" ng-model="venue.website" placeholder="http://" required>
											</div>
										</div>

										<div class="line line-lg"></div>

										<div class="form-group">
											<label class="col-md-3 control-label">Latitude</label>
											<div class="col-md-9">
												<input type="text" class="form-control venue" ng-model="venue.latitude" required>
											</div>
										</div>

										<div class="line line-lg"></div>

										<div class="form-group">
											<label class="col-md-3 control-label">Longtitude</label>
											<div class="col-md-9">
												<input type="text" class="form-control venue" ng-model="venue.longtitude" required>
											</div>
										</div>

										<div class="line line-lg"></div>

										<div class="form-group">
											<label class="col-md-3 control-label">Phone Number</label>
											<div class="col-md-9">
												<input type="text" class="form-control phone_us venue" placeholder="e.g. 928-284-5046" ng-model="venue.contact" minlength=14 maxlength=14 required>
												<span class="help-block m-b-none"> (Admin should enter phone number including dashes, country code, area code, etc.)</span>
											</div>
										</div>

										<div class="line line-lg"></div>

										<div class="form-group">
											<label class="col-md-3 control-label">Authorizeid</label>
											<div class="col-md-9">
												<input type="text" class="form-control venue"  ng-model="venue.authorizeid" required>
											</div>
										</div>

										<div class="line line-lg"></div>

										<div class="form-group">
											<label class="col-md-3 control-label">Authorizekey</label>
											<div class="col-md-9">
												<input type="text" class="form-control venue" ng-model="venue.authorizekey" required>
											</div>
										</div>

										<div class="line line-lg"></div>

										<div class="form-group">
											<span class="pull-right">
												<a class="btn btn-md btn-danger" ng-click="isCollapsed = !isCollapsed" ng-disabled="cancel">Close</a>
												<button class="btn btn-md btn-success" ng-click="isCollapsed = isCollapsed" ng-disabled="venueform.$invalid || validvenue">Save</button>
											</span>
										</div>
									</div>
			          </form>
	          	</div> <!-- ;panelbody -->
	          </div>
    				<div class="panel b-a">
    					<div class="panel-heading panel-primary"> &nbsp;
    						<div class="col-sm-5">
    							<form>
    								<div class="input-group mr_tp-5px">
    									<span class="input-group-btn">
    										<button class="btn btn-sm btn-default" type="button" ng-click="clearsearch()">Clear</button>
    									</span>
    									<input class="input-sm form-control searchtext" placeholder="Search Venue Name" type="text" ng-model="searchtext">
    									<span class="input-group-btn">
    										<button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)"><i class="fa fa-search"></i></button>
    									</span>
    								</div>
    							</form>
    						</div>
    					</div>
    					<div class="panel-body">
								<div class="row">
									<div class="table-responsive">
										<table class="table table-striped b-t b-light">
											<thead class="bg-primary">
												<tr>
													<th>Venue Name</th>
													<th>Address</th>
													<th>City</th>
													<th>State</th>
													<th>Zip Code</th>
													<th>Country</th>
													<th>Contact</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-if="TotalItems == 0">
													<td colspan="8">No  Workshop Venue to show</td>
												</tr>
												<tr ng-repeat="venue in venues.data">
													<td>{[{venue.venuename}]}</td>
													<td>{[{venue.address1}]}</td>
													<td>{[{venue.city}]}</td>
													<td>{[{venue.SPR}]}</td>
													<td>{[{venue.zipcode}]}</td>
													<td>{[{venue.country}]}</td>
													<td>{[{venue.contact}]}</td>
													<td>
														<a class="btn btn-sm bg-warning" ng-click="editworkshopvenue(venue.workshopvenueid)">
															<i class="glyphicon glyphicon-fw glyphicon-edit"></i>
														</a>
														<a class="btn btn-sm bg-danger" ng-click="delete(venue.workshopvenueid)">
															<i class="glyphicon glyphicon-fw glyphicon-trash"></i>
														</a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="panel-footer">
								<center>
									<pagination total-items="TotalItems" ng-model="CurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(CurrentPage)" ng-if="TotalItems != 0"></pagination>
								</center>
							</div>
    				</div>
          </tab>

          <tab active="workshoplist">
          	<tab-heading class="font-bold">Workshop List</tab-heading>
<!--           	<form name="workshopform" class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="createworkshop(workshop)"> -->
          	<div class="panel b-a">
          		<div class="panel-heading b-b b-light">
	          		<strong>&nbsp;</strong>
	          		<a class="btn btn-primary btn-xs pull-right" ui-sref="create_workshop">Create Workshop</a>
	          		<!-- <a class="btn btn-primary btn-xs pull-right" ng-init="isCollapsed2 = true" ng-click="isCollapsed2 = !isCollapsed2">Create Workshop</a> -->
	          	</div>
          		<!-- <div class="panel-body" collapse="isCollapsed2">
          			wew
				</div> -->
			</div>
						<div class="panel panel-primary">
							<div class="panel-heading">Workshop List</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-sm-5">
										<div class="input-group">
											<span class="input-group-btn">
												<button class="btn btn-sm btn-default" type="button" ng-click="clearsearch_workshop()">Clear</button>
											</span>
											<input class="input-sm form-control searchtextworkshop" placeholder="Search" type="text" ng-model="searchtextworkshop">
											<span class="input-group-btn">
												<button class="btn btn-sm btn-default" type="button" ng-click="search_workshop(searchtextworkshop)">Go!</button>
											</span>
										</div>
									</div>
								</div>
								<div class="table-responsive">
										<table class="table table-striped b-t b-light">
											<thead>
												<tr>
													<th>Venue Name</th>
													<th>Title Name</th>
													<th>Tuition</th>
													<th>Starting Date</th>
													<th>Ending Date</th>
													<th>Accomodation</th>
													<th>Description</th>
													<th>Miscellaneous</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-if="TotalItems == 0">
													<td colspan="8">No  Workshop Venue to show</td>
												</tr>
												<tr ng-repeat="workshop in workshops.data">
													<td ng-if="workshop.venue != 'bnbcenter'">{[{workshop.venuename}]}</td>
													<td ng-if="workshop.venue == 'bnbcenter'">{[{workshop.centertitle}]}</td>
													<td>{[{workshop.titlename}]}</td>
													<td>{[{workshop.tuition}]}</td>
													<td>{[{workshop.datestart}]}</td>
													<td>{[{workshop.dateend}]}</td>
													<td>{[{workshop.accomodation}]}</td>
													<td>{[{workshop.about | limitTo:21 }]}..</td>
													<td>{[{workshop.miscellaneous}]}</td>
													<td>
														<a href="" ng-click="editworkshop(workshop.workshopid)">
															<span class="label bg-warning">Edit</span>
														</a>
														<a href="" ng-click="removeworkshop(workshop.workshopid)">
															<span class="label bg-danger">Remove</span>
														</a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
							</div>
							<div class="panel-footer">
								<pagination total-items="TotalItems_workshop" ng-model="CurrentPage_workshop" max-size="maxSize1" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage_workshop(CurrentPage_workshop)"></pagination>
							</div>
						</div>
          </tab>
          <tab active="">
          	<tab-heading class="font-bold">Workshop Related</tab-heading>
          	<table class="table table-striped b-t b-light">
          		<thead>
          			<tr>
          				<th>Title</th>
          				<th>Related Stories</th>
          				<th>Related Main Articles</th>
          				<th>Related Center Articles</th>
          			</tr>
          		</thead>
          		<tbody>
          			<tr ng-repeat="title in titlesforrelated">
          				<td>{[{title.title}]}</td>
          				<td>
          					<span ng-repeat="story in relatedstory[$index]" class="btn m-b-xs btn-sm btn-primary btn-rounded btn-addon margin_right_5px"> "{[{story.subject | limitTo:20}]}.." by: {[{story.author}]} </span>
          				</td>
          				<td>
          					<span ng-repeat="main in relatedmainarticle[$index]" class="btn m-b-xs btn-sm btn-primary btn-rounded btn-addon margin_right_5px"> "{[{main.title | limitTo:20}]}.." by: {[{main.author}]} </span>
          				</td>
          				<td>
          					<span ng-repeat="center in relatedcenterarticle[$index]" class="btn m-b-xs btn-sm btn-primary btn-rounded btn-addon margin_right_5px"> "{[{center.title | limitTo:20}]}.." by: {[{center.author}]} </span>
          				</td>
          			</tr>
          		</tbody>
          	</table>
          </tab>
       </tabset>
    </div>
	</div>
</div>
