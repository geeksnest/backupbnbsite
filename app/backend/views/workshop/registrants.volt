<script type="text/ng-template" id="registrantremove.html">
  <div ng-include="'/be/tpl/registrantremove.html'"></div>
</script>
<script type="text/ng-template" id="registrant_details">
  <div ng-include="'/be/tpl/workshops/registrant_details.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Workshop Registrants</h1>
  <a id="top"></a>
</div>

<div class="row wrapper-md">
  <div class="col-sm-12 form-group" style="padding-left:0;">
    <div class="col-md-3">
      <div ui-module="select2">
        <select ui-select2 class="form-control registrant" ng-model="registrant.titlename">
            <option value = ''>All Title</option>
            <option ng-repeat="title in workshoptitles" value="{[{title.workshoptitleid}]}">{[{title.title}]}</option>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div ui-module="select2">
        <select ui-select2 class="form-control registrant" ng-model="registrant.venue">
          <option value = ''>All Venue</option>
          <option ng-repeat="venue in centersnvenues" value="{[{venue.venueid}]}">{[{venue.venuename}]}, {[{venue.venuestate}]}</option>
        </select>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <!-- <a class="btn btn-default" ng-click="clearsearch_registrant()" ng-model="selected">Clear Filter</a> -->
        <a class="btn bg-orange" ng-click="filterRegistrants(registrant)">Filter</a>
      </div>
    </div>
  </div>

  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">Registrant List</div>
      <div class="panel-body">

        <div class="table-responsive">
          <table class="table table-striped b-t b-light">
            <thead>
              <tr>
                <th>Title</th>
                <th>Venue</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Registered Date</th>
                <th>Qty</th>
                <th>Payment</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr ng-if="TotalItems == 0">
                <td colspan="10">NO RESULT</td>
              </tr>
              <tr ng-repeat="registrant in registrants">
                <td ng-bind="registrant.workshoptitle"></td>
                <td ng-if="registrant.venue == 'bnbcenter'">{[{registrant.venuename}]} Center</td>
                <td ng-if="registrant.venue == 'bnbvenue'" ng-bind="registrant.venuename"></td>
                <td ng-bind="registrant.name"></td>
                <td ng-bind="registrant.contact"></td>
                <td ng-bind="registrant.dateregistered | date:'MMM d, y h:mm a'"></td>
                <td ng-bind="registrant.qty"></td>
                <td>$<span ng-bind="registrant.cash"></span>.00</td>
                <td>
                  <button type="button" class="btn btn-xs" ng-click="moreDetails(registrant)"><i class="icon icon-book-open"></i> more details</button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="panel-footer" align=center>
        <pagination total-items="TotalItems" ng-model="CurrentPage" max-size="5" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(CurrentPage)" ng-show="TotalItems != 0"></pagination>
        <span ng-show="TotalItems == 0">NO RESULT</span>
      </div>
    </div>
  </div>
</div>
