<script type="text/ng-template" id="error">
  <div ng-include="'/be/tpl/workshops/error.html'"></div>
</script>
<script type="text/ng-template" id="workshopdetails">
  <div ng-include="'/be/tpl/workshops/workshop_details.html'"></div>
</script>
<script type="text/ng-template" id="workshopremove">
  <div ng-include="'/be/tpl/workshops/workshopremove.html'"></div>
</script>
<script type="text/ng-template" id="workshopedit">
  <div ng-include="'/be/tpl/workshops/workshopedit.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Workshops</h1>
  <a id="top"></a>
</div>

<div class="row wrapper-md">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">Workshop List</div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-5">
            <div class="input-group">
              <span class="input-group-btn">
                <button class="btn btn-sm btn-default" type="button" ng-click="clearsearch_workshop()">Clear</button>
              </span>
              <input class="input-sm form-control searchtextworkshop" placeholder="Search" type="text" ng-model="searchtextworkshop">
              <span class="input-group-btn">
                <button class="btn btn-sm btn-default" type="button" ng-click="search_workshop(searchtextworkshop)">Go!</button>
              </span>
            </div>
          </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped b-t b-light">
              <thead>
                <tr>
                  <th>Venue</th>
                  <th>Workshop Title</th>
                  <th>Regular Tuition</th>
                  <th>Starting Date</th>
                  <th>Ending Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-if="TotalItems == 0">
                  <td colspan="8">No  Workshop Venue to show</td>
                </tr>
                <tr ng-repeat="workshop in workshops.data">
                  <td ng-if="workshop.venue != 'bnbcenter'">{[{workshop.venuename}]}</td>
                  <td ng-if="workshop.venue == 'bnbcenter'">{[{workshop.centertitle}]}</td>
                  <td>{[{workshop.titlename}]}</td>
                  <td>${[{workshop.tuition}]}</td>
                  <td>{[{workshop.datestart}]}</td>
                  <td>{[{workshop.dateend}]}</td>
                  <td>
                    <button type="button" class="btn btn-xs btn-default" ng-click="details(workshop)">More details</button>
                    <button type="button" class="btn btn-xs btn-info" ng-click="editworkshop(workshop.workshopid)">Edit</button>
                    <button type="button" class="btn btn-xs btn-danger" ng-click="removeworkshop(workshop.workshopid)">Remove</button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
      </div>
      <div class="panel-footer">
        <pagination total-items="TotalItems_workshop" ng-model="CurrentPage_workshop" max-size="maxSize1" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage_workshop(CurrentPage_workshop)"></pagination>
      </div>
    </div>
  </div>
</div>
