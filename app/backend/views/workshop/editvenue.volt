<script type="text/ng-template" id="error">
  <div ng-include="'/be/tpl/workshops/error.html'"></div>
</script>
<script type="text/ng-template" id="success">
  <div ng-include="'/be/tpl/workshops/success.html'"></div>
</script>

<fieldset ng-disabled="isBusy">
  <form name="venueform" ng-submit="updatevenue(venue)">

    <div class="bg-light lter b-b wrapper-md pageheader">
      <h1 class="m-n font-thin h3">Edit Venue</h1>
      <a id="top"></a>
      <button class="btn btn-md btn-success pull-right" title="Save"><i class="fa fa-floppy-o"></i></button>
    </div>

    <div class="row wrapper-md">
      <div class="col-sm-8">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">Venue Form</div>
          <div class="panel-body">
            <div class="col-sm-12 form-group">
              <label>Venue Name</label>
              <input type="text" name="venuename" class="form-control venue" placeholder="Venue Name" ng-model="venue.venuename" ng-change="checkvenuename(venue.venuename)" required>
              <div class="popOver" ng-show="validvenue">
                Venue name is already taken.
                <span class="pop-triangle"></span>
              </div>
              <span class="help-block m-b-none">Example:Sedona Mago Retreat, Honor's Haven</span>
            </div>

            <div class="col-sm-6 form-group">
              <label>State</label>
              <div ui-module="select2">
                <select ui-select2 class="form-control"  ng-model="venue.SPR"  ng-change="statechange(venue.SPR)" required>
                  <option ng-repeat="list in centerstate" ng-value="list.state_code">{[{list.state}]}</option>
                </select>
              </div>
            </div>

            <div class="col-sm-6 form-group">
              <label>City</label>
              <div ui-module="select2">
                <select ui-select2 class="form-control" ng-model="venue.city" ng-change="citychange(venue.city)" required>
                  <option ng-repeat="list in centercity" ng-value="list.city">{[{list.city}]}</option>
                </select>
              </div>
            </div>

            <div class="col-sm-6 form-group">
              <label>Country</label>
              <input type="text" class="form-control venue" ng-model="venue.country">
            </div>

            <div class="col-sm-6 form-group">
              <label>Zip Code</label>
              <div ui-module="select2">
                <select ui-select2 class="form-control" ng-model="venue.zipcode"  required>
                  <option ng-repeat="list in getcenterzip" ng-value="list.zip">{[{list.zip}]}</option>
                </select>
              </div>
            </div>

            <div class="col-sm-12 form-group">
              <label>Address 1</label>
              <input type="text" class="form-control venue" placeholder="Address 1" ng-model="venue.address1">
              <span class="help-block m-b-none">Street address, P.O. box, company name, c/o </span>
            </div>

            <div class="col-sm-12 form-group">
              <label>Address 2</label>
              <input type="text" class="form-control venue" placeholder="Apartment, suite, unit, building, floor, etc." ng-model="venue.address2">
            </div>

            <div class="col-sm-12 form-group">
              <label>Phone Number</label>
              <input type="text" class="form-control phone_us venue" placeholder="e.g. 928-284-5046" ng-model="venue.contact" minlength=14 maxlength=14 required>
              <span class="help-block m-b-none"> (Admin should enter phone number including dashes, country code, area code, etc.)</span>
            </div>

            <div class="col-sm-12 form-group">
              <label>Website</label>
              <input type="url" class="form-control venue" ng-model="venue.website" placeholder="http://" required>
            </div>

          </div> <!-- ;panelbody -->
        </div>
      </div>
      <div class="col-sm-4">
        <div class="panel panel-default font-bold">
          <div class="panel-heading">World Coordinate</div>
          <div class="panel-body">
            <div class="form-group">
              <label>Latitude</label>
              <input type="text" class="form-control venue" ng-model="venue.latitude" required>
            </div>
            <div class="form-group">
              <label>Longtitude</label>
              <input type="text" class="form-control venue" ng-model="venue.longtitude" required>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading font-bold">Authorized.Net Credentials</div>
          <div class="panel-body">
            <div class="form-group">
              <label>Authorized ID</label>
              <input type="text" class="form-control venue"  ng-model="venue.vauthorizeid" required>
            </div>
            <div class="form-group">
              <label>Authorized Key</label>
              <input type="text" class="form-control venue" ng-model="venue.vauthorizekey" required>
            </div>
          </div>
        </div>
      </div>
    </div>

  </form>
</fieldset>
