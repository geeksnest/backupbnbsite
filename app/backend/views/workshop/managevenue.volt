<script type="text/ng-template" id="workshopvenueedit.html">
  <div ng-include="'/be/tpl/workshopvenueedit.html'"></div>
</script>
<script type="text/ng-template" id="workshopvenuedelete.html">
  <div ng-include="'/be/tpl/workshopvenuedelete.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Venue</h1>
  <a id="top"></a>
</div>

<div class="row wrapper-md">
  <div class="col-sm-12 form-group">
    <div class="panel b-a">
      <div class="panel-heading panel-primary"> &nbsp;
        <div class="col-sm-5">
          <form>
            <div class="input-group mr_tp-5px">
              <span class="input-group-btn">
                <button class="btn btn-sm btn-default" type="button" ng-click="clearsearch()">Clear</button>
              </span>
              <input class="input-sm form-control searchtext" placeholder="Search Venue Name" type="text" ng-model="searchtext">
              <span class="input-group-btn">
                <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
        </div>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="table-responsive">
            <table class="table table-striped b-t b-light">
              <thead class="">
                <tr>
                  <th>Venue Name</th>
                  <th>Address</th>
                  <th>City</th>
                  <th>State</th>
                  <th>Zip Code</th>
                  <th>Country</th>
                  <th>Contact</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-if="TotalItems == 0">
                  <td colspan="8">No  Workshop Venue to show</td>
                </tr>
                <tr ng-repeat="venue in venues.data">
                  <td>{[{venue.venuename}]}</td>
                  <td>{[{venue.address1}]}</td>
                  <td>{[{venue.city}]}</td>
                  <td>{[{venue.SPR}]}</td>
                  <td>{[{venue.zipcode}]}</td>
                  <td>{[{venue.country}]}</td>
                  <td>{[{venue.contact}]}</td>
                  <td>
                    <a class="btn btn-sm bg-warning" ng-click="editworkshopvenue(venue.workshopvenueid)">
                      <i class="glyphicon glyphicon-fw glyphicon-edit"></i>
                    </a>
                    <a class="btn btn-sm bg-danger" ng-click="delete(venue.workshopvenueid)">
                      <i class="glyphicon glyphicon-fw glyphicon-trash"></i>
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="panel-footer">
        <center>
          <pagination total-items="TotalItems" ng-model="CurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(CurrentPage)" ng-if="TotalItems != 0"></pagination>
        </center>
      </div>
    </div>
  </div>
</div>
