<script type="text/ng-template" id="success.html">
  <div ng-include="'/be/tpl/success.html'"></div>
</script>
<script type="text/ng-template" id="workshoptitleremove.html">
  <div ng-include="'/be/tpl/workshoptitleremove.html'"></div>
</script>
<script type="text/ng-template" id="workshop_title">
  <div ng-include="'/be/tpl/workshop_title.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <span class="m-n font-thin h3">Workshop Title</span>
  {#<a ui-sref="new_wstitle"><button type="button" class="btn btn-default pull-right">Create Title</button></a>#}
  <button type="button" class="btn btn-default pull-right" ng-click="openCreateTitle()">Create Title</button>
</div>

<div class="contrainer">
  <div class="row wrapper-md">
    <div class="col-sm-12">

<div class="panel b-a panel-default">
  <div class="panel-heading"><b>Workshop Title List</b></div>
  <div class="panel-body">
    <div class="row>">
      <div class="col-sm-5" style="padding-left:0;">
        <div class="input-group">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="clearsearch_title()">Clear</button>
          </span>
          <input class="input-sm form-control searchtexttitle" placeholder="Search" type="text" ng-model="searchtexttitle">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="search_title(searchtexttitle)">Go!</button>
          </span>
        </div>
      </div>
    </div>
    <div class="line line-lg"></div>
    <div class="row">
      <div vertilize-container>
        <div class="col-sm-4" vertilize ng-repeat="title in titlelist.data">
          <div class="panel panel-default">
            <div class="panel-heading font-bold bg-orange" ng-bind="title.title | uppercase "></div>
            <div class="panel-body" ng-bind="title.description"></div>
            <div class="panel-footer" style="text-align:right">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-default" ng-click="openUpdateTitle(title)" title="Edit"><i class="icon icon-note"></i></button>
                  <button type="button" class="btn btn-sm btn-gray" ng-click="removetitle(title.workshoptitleid)" title="Delete"><i class="icon icon-trash"></i></button>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <pagination total-items="TotalItems1" ng-model="CurrentPage1" max-size="maxSize1" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage_title(CurrentPage1)"></pagination>
  </div>
</div>

    </div>
  </div>
</div>
