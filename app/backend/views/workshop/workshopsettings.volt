{# <<< REQUIRED BY THE MULTI IMAGEUPLOADER WITH URL -nu77 #}
<script type="text/ng-template" id="imageuploaderwithurl">
	<div ng-include="'/be/tpl/module/imageuploaderwithurl.html'"></div>
</script>
<script type="text/ng-template" id="confirm">
	<div ng-include="'/be/tpl/confirm.html'"></div>
</script>
<script type="text/ng-template" id="bannerurl">
	<div ng-include="'/be/tpl/bannerurl.html'"></div>
</script>
{# REQUIRED BY THE MULTI IMAGEUPLOADER WITH URL ENDS -nu77 >>>#}

<div class="bg-light lter b-b wrapper-md ng-scope">
	<h1 class="m-n font-thin h3">Workshop Settings</h1>
	<a id="top"></a>
</div>

<div class="row wrapper-md">

	{#<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading font-bold">Workshop Title</div>
			<div class="panel-body">
				<div class="input-group">
					<input type="text" class="form-control" />
					<span class="input-group-btn">
						<button class="btn btn-default" type="button"><i class="fa fa-edit"></i></button>
						<button class="btn btn-default" type="button"><i class="fa fa-save"></i></button>
						<button class="btn btn-default" type="button"><i class="fa fa-refresh"></i></button>
					</span>
				</div>
			</div>
		</div>
	</div>#}

	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading font-bold">Workshop Banner</div>
			<div class="panel-body">
				<button class="btn m-b-xs btn-sm btn-default btn-addon pull-right" ng-click="imageUploaderWithUrl('workshop')"><i class="fa fa-plus"></i>Upload New Banner</button>

				<div class="line line-lg line-dashed b-b"></div>

		    <div class="col-sm-3" ng-repeat="banner in banners">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="imagelist" style="background:url('{[{amazonlink}]}/uploads/slidernbanner/{[{banner.image}]}') no-repeat center center; background-size:contain">
							</div>
						</div>
						<div class="panel-footer" style="text-align:right">
								<div class="btn-group">
									<button type="button" class="btn btn-sm btn-light" title="Click to toggle hide/show" ng-click="hideShowImage(banner.id)">
										<i class="fa fa-eye" ng-if="banner.status == 1"></i>
										<i class="fa fa-eye-slash" ng-if="banner.status != 1"></i>
									</button>
									<button type="button" class="btn btn-sm btn-info" title="Edit" ng-click="editImage(banner.id)" ><i class="icon icon-note"></i></button>
									<button type="button" class="btn btn-sm btn-danger" title="Remove" ng-click="removeImage(banner.id)" ><i class="icon icon-trash"></i></button>
									<select name="sequence" id="sequence" class="pull-right" ng-model="banner.sequence" ng-change="changeorder(banner)" style="height: 30px">
										<option ng-repeat="seq in sequence">{[{ seq }]}</option>
									</select>
								</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
