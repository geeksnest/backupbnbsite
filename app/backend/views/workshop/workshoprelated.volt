<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Workshop Related</h1>
  <a id="top"></a>
</div>

<div class="row wrapper-md">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <table class="table table-striped b-t b-light">
          <thead>
            <tr>
              <th>Title</th>
              <th>Related Stories</th>
              <th>Related Main Articles</th>
              <th>Related Center Articles</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="title in titlesforrelated">
              <td>{[{title.title}]}</td>
              <td>
                <span ng-repeat="story in relatedstory[$index]" class="btn m-b-xs btn-sm btn-primary btn-rounded btn-addon margin_right_5px"> "{[{story.subject | limitTo:20}]}.." by: {[{story.author}]} </span>
              </td>
              <td>
                <span ng-repeat="main in relatedmainarticle[$index]" class="btn m-b-xs btn-sm btn-primary btn-rounded btn-addon margin_right_5px"> "{[{main.title | limitTo:20}]}.." by: {[{main.author}]} </span>
              </td>
              <td>
                <span ng-repeat="center in relatedcenterarticle[$index]" class="btn m-b-xs btn-sm btn-primary btn-rounded btn-addon margin_right_5px"> "{[{center.title | limitTo:20}]}.." by: {[{center.author}]} </span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
  </div>
</div>
