<fieldset ng-disabled="isBusy">
  <form name="venueform" ng-submit="newWstitle(wsttl)">

    <div class="bg-light lter b-b wrapper-md">
      <span class="m-n font-thin h3">Create Workshop Title</span>
      <span class="pull-right">
        <a ui-sref="workshoptitles" class="btn btn-md btn-default" title="Workshop Title List"><i class="fa fa-list-alt"></i></a>
        <button class="btn btn-md btn-default" title="Save"><i class="fa fa-floppy-o"></i></button>
      </span>
    </div>

    <div class="wrapper-md">
      <div class="row">
        <div class="col-sm-8">

          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group">
                <label>Title</label>
                <input id="wstitle" type="text" class="form-control" ng-model="wsttl.title" ng-change="SEO(wsttl.title)" required />
              </div>
              <div class="form-group">
                <label>Slugs</label>
                <input id="wstitleslugs" type="text" class="form-control" ng-model="wsttl.titleslugs" ng-change="SEO(wsttl.titleslugs)" required />
              </div>
              <div class="form-group">
                <label>Description</label>
                <textarea id="shortdesc" class="form-control rv" rows="3" ng-model="wsttl.description" required></textarea>
              </div>
              <div class="form-group">
                <label>Body</label>
                <textarea id="body" class="ck-editor form-control" ng-model="wsttl.body" required></textarea>
              </div>
            </div>
          </div>

        </div>

        <div class="col-sm-4">

          <div class="panel panel-default">
            <div class="panel-heading font-bold">Banner</div>
            <div class="panel-body">
              <div class="input-group">
                <span class="input-group-btn">
                  <label class="btn btn-default" for="imgbrochure">
                      <span class="fa fa-flag"></span>
                      &nbsp; Banner
                  </label>
                </span>
                <input id="imgbannertext" type="text" class="form-control" ng-value="Filename" placeholder="Browse for image banner"  disabled/>
                {#<span class="input-group-btn" ng-hide="hasBrochure == null" ng-click="clearBrochure()">
                  <label class="btn btn-default">
                      <span class="glyphicon glyphicon-remove"></span>
                  </label>
                </span>#}
              </div>
              <input type="file" id="imgbrochure" name="brochure"  class="form-control hidden" ngf-select ngf-allow-dir="true" accept="image/*" ng-model="wsttl.banner"/>
              
              <div class="col-sm-12 maxh-md" style="max-height:250px;">
                <img ngf-src="wsttl.banner[0]" class="img-responsive img-rps img-tmb" />
              </div>

            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading"><b>Workshop Brochure</b> <i class="pull-right">(optional)</i></div>
            <div class="panel-body">
              <div class="input-group">
                <span class="input-group-btn">
                  <label class="btn btn-default" for="brochure">
                      <span class="glyphicon glyphicon-folder-open"></span>
                      &nbsp; Brochure
                  </label>
                </span>
                <input id="brochuretext" type="text" class="form-control" ng-value="pdfName" placeholder="Browse for PDF files" title="{[{pdfName}]}" disabled/>
                <span class="input-group-btn" ng-hide="hasBrochure == null" ng-click="clearBrochure()">
                  <label class="btn btn-default">
                      <span class="glyphicon glyphicon-remove"></span>
                  </label>
                </span>
              </div>
              <input type="file" id="brochure" name="brochure"  class="form-control hidden" ng-model="wsttl.brochure" ngf-select="onChangeBrochure(wsttl.brochure)" ngf-allow-dir="true" accept="application/pdf" />
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading"><b>Meta Properties</b> <i class="pull-right">(optional)</i></div>
            <div class="panel-body">
              <div class="form-group">
                <label>Meta Title</label>
                <input id="metatitle" type="text" class="form-control" ng-model="wsttl.metatitle" />
              </div>
              <div class="form-group">
                <label>Meta Description</label>
                <textarea id="metadesc" class="form-control rv" rows="3" ng-model="wsttl.metadesc"></textarea>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

  </form>
</fieldset>
