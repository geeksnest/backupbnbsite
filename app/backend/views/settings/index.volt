{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="imagelist.html">
  <div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script type="text/javascript">
  function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>
<div class="bg-light lter b-b wrapper-md ng-scope">
  <h1 class="m-n font-thin h3">Settings</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md ng-scope">
  <div class="col-sm-12">
                    <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
  </div>
  <div class="col-sm-8">
  	<div class="panel panel-default">
  	         <div class="panel panel-default">
        <div class="panel-heading"><span class="h4">Maintenance</span></div>
        <div class="panel-body">
            <div ng-if="value1 == 0" >
              <form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formmaintenance1">
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <span class="help-block m-b-none">In order to to turn on maintenance mode you must fill all the fields.</span>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Message</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="on.maintenance_msg" required ng-model="on.maintenance_msg">
                </div><br/><br/>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Time</label>
                <div class="col-sm-4">
                  <div class="input-group bootstrap-touchspin">
                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                    <input ui-jq="TouchSpin" type="text" value="" class="form-control" data-min="0" data-max="100" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block;" required name="on.maintenance_time" ng-model="on.maintenance_time" onkeypress='return isNumberKey(event)'>
                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                  </div>
                </div><br/><br/>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <spam class="col-sm-1 control-label" style="font-size:20px;padding-top:5px;">Off</spam>
                <div class="m-b-sm col-sm-2">
                  <label class="i-switch i-switch-lg m-t-xs m-r">
                    <input type="checkbox" value="1" name="maintenance_on" ng-model="maintenance_on" required>
                    <i></i>
                  </label>
                </div>
                <spam class="col-sm-4 control-label" style="font-size:20px;padding-top:5px;margin-left:-30px;">On</spam>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="col-sm-12">
                <button type="submit" class="btn btn-sm  btn-success" ng-click="savesettings(on)" ng-disabled="formmaintenance1.$invalid" style="float:right;">Save change</button>
              </div>
              </form>
            </div>



          <div ng-if="value1 == 1">
            <form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formmaintenance">
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <span class="help-block m-b-none">Maintenance mode is turned on.</span>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <span class="help-block m-b-none">{[{ value2 }]}</span>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <span class="help-block m-b-none">{[{ value3 }]} hours maintenance</span>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">

                <spam class="col-sm-1 control-label" style="font-size:20px;padding-top:5px;">On</spam>
              <div class="m-b-sm col-sm-2">
                <label class="i-switch i-switch-lg m-t-xs m-r">
                  <input type="checkbox" value="0" name="maintenance_off" ng-model="maintenance_off" required="required">
                  <i></i>
                </label>
              </div>
              <spam class="col-sm-4 control-label" style="font-size:20px;padding-top:5px;margin-left:-30px;">Off</spam>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="col-sm-12">
              <button type="submit" class="btn btn-sm btn-primary" id="off"  ng-click="savesettingsoff()" ng-disabled="formmaintenance.$invalid" style="float:right;">Save change</button>
            </div>
          </form>
        </div>
      </div>
    </div>
   </div>

   <div class="panel panel-default">
     <div class="panel-heading"><span class="h4">Google Analytics</span></div>
     <div class="panel-body">
       <form class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="save(info)" name="form" >
        <div class="row">
         <div class="panel-body">
          <div class="form-group">
           <label class=" control-label">Script</label>
           <input type="text" id="title" name="title" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="info.data" required="required" placeholder="{[{script}]}" >
           <br>
         </div>
         <footer class="panel-footer text-right bg-light lter">
          <button disabled="disabled" type="submit" class="btn btn-success" ng-disabled="form.$invalid">Save</button>
        </footer>
         </div>
         </div>
       </form>
     </div>
   </div>
</div>
<!--   ///////////////////////////////////////////////////////      -->
  <div class="col-sm-4">
    <div class="panel panel-default">
      <div class="panel-heading font-bold">
                Logo
      </div>
      <div class="panel-body">
          <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savelogo(logo)"  name="formlogo">
          <div class="form-group">
                  <div class="input-group m-b">
                  <span class="input-group-btn">
                    <a class="btn btn-default" ng-click="showimageList('lg')">Select Logo</a>
                  </span>
                  <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">
                  <input type="text" class="form-control" ng-value="logo.logo = amazonpath" name="logo" ng-model="logo.logo" placeholder="{[{amazonpath}]}" readonly required>
                </div>
                  <div class="col-sm-12">
                  <br><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/banner/{[{amazonpath}]}" width="100%" height="100%" alt="IMAGE PREVIEW">
                  </div>
                  <div class="line line-dashed b-b line-lg pull-in"></div>
                  <button type="submit" class="btn btn-sm btn-primary" id="off" ng-disabled="formlogo.$invalid" style="float:right;" scroll-to="Scrollup">Save</button>
          </div>
          </form>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading font-bold">
        Social Media
      </div>
      <div class="panel-body">
        <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savesocial(social)"  name="formlogo">
          <div class="form-group hiddenoverflow">
            <label class="col-sm-4 control-label">Facebook</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" name="social.facebook" ng-model="social.facebook">
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group hiddenoverflow">
            <label class="col-sm-4 control-label">Google plus</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" name="social.gplus" ng-model="social.gplus">
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group hiddenoverflow">
            <label class="col-sm-4 control-label">Twitter</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" name="social.twitter" ng-model="social.twitter">
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group hiddenoverflow">
            <label class="col-sm-4 control-label">Yelp</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" name="social.yelp" ng-model="social.yelp">
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group hiddenoverflow">
            <label class="col-sm-4 control-label">Youtube</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" name="social.youtube" ng-model="social.youtube">
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <button type="submit" class="btn btn-sm btn-primary pull-right" id="off" scroll-to="Scrollup">Save</button>
        </form>
      </div>
    </div>

  </div>
 <!--   ///////////////////////////////////////////////////////      -->

</div>
