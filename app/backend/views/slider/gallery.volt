<script type="text/ng-template" id="editslider.html">
	<div ng-include="'/be/tpl/editslider.html'"></div>
</script>
<script type="text/ng-template" id="imagelist.html">
	<div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
	<div ng-include="'/be/tpl/delete.html'"></div>
</script>
<script type="text/ng-template" id="egallery.html">
	<div ng-include="'/be/tpl/egallery.html'"></div>
</script>

{# <<< REQUIRED BY THE MULTI IMAGEUPLOADER WITH URL -nu77 #}
<script type="text/ng-template" id="imageuploaderwithurl">
	<div ng-include="'/be/tpl/module/imageuploaderwithurl.html'"></div>
</script>
<script type="text/ng-template" id="confirm">
	<div ng-include="'/be/tpl/confirm.html'"></div>
</script>
<script type="text/ng-template" id="bannerurl">
	<div ng-include="'/be/tpl/bannerurl.html'"></div>
</script>
{# REQUIRED BY THE MULTI IMAGEUPLOADER WITH URL ENDS -nu77 >>>#}

<div class="bg-light lter b-b wrapper-md ng-scope">
	<h1 class="m-n font-thin h3">Slider</h1>
	<a id="top"></a>
</div>

<div class="wrapper-md">
	<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
	<tabset class="tab-container">
		<tab ng-click="changetab('home')">
			<tab-heading><i class="fa fa-home"></i> Home</tab-heading>
			<div class="panel panel-default">
				<div class="panel-body">
					<button class="btn m-b-xs btn-sm btn-default btn-addon pull-right" ng-click="imageUploaderWithUrl('home')"><i class="fa fa-plus"></i>Upload New Banner(s)</button>
					<div class="line line-lg line-dashed b-b"></div>
					<div class="col-sm-3" ng-repeat="banner in banners">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="imagelist" style="background:url('{[{amazonlink}]}/uploads/slidernbanner/{[{banner.image}]}') no-repeat center center; background-size:contain">
								</div>
							</div>
							<div class="panel-footer" style="text-align:right">
									<div class="btn-group">
										<button type="button" class="btn btn-sm btn-light" title="Click to toggle hide/show" ng-click="hideShowImage(banner.id)">
											<i class="fa fa-eye" ng-if="banner.status == 1"></i>
											<i class="fa fa-eye-slash" ng-if="banner.status != 1"></i>
										</button>
										<button type="button" class="btn btn-sm btn-info" title="Edit" ng-click="editImage(banner.id)" ><i class="icon icon-note"></i></button>
										<button type="button" class="btn btn-sm btn-danger" title="Remove" ng-click="removeImage(banner.id)" ><i class="icon icon-trash"></i></button>
										<select name="sequence" id="sequence" class="pull-right" ng-model="banner.sequence" ng-change="changeorder(banner)" style="height: 30px">
											<option ng-repeat="seq in sequence">{[{ seq }]}</option>
										</select>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</tab>
		<tab ng-click="changetab('shop')">
			<tab-heading><i class="fa fa-shopping-cart"></i> Shop</tab-heading>
			<div class="panel panel-default">
				<div class="panel-body">
					<button class="btn m-b-xs btn-sm btn-default btn-addon pull-right" ng-click="imageUploaderWithUrl('shop')"><i class="fa fa-plus"></i>Upload New Banner(s)</button>
					<div class="line line-lg line-dashed b-b"></div>
					<div class="col-sm-3" ng-repeat="banner in banners">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="imagelist" style="background:url('{[{amazonlink}]}/uploads/slidernbanner/{[{banner.image}]}') no-repeat center center; background-size:contain">
								</div>
							</div>
							<div class="panel-footer" style="text-align:right">
									<div class="btn-group">
										<button type="button" class="btn btn-sm btn-light" title="Click to toggle hide/show" ng-click="hideShowImage(banner.id)">
											<i class="fa fa-eye" ng-if="banner.status == 1"></i>
											<i class="fa fa-eye-slash" ng-if="banner.status != 1"></i>
										</button>
										<button type="button" class="btn btn-sm btn-info" title="Edit" ng-click="editImage(banner.id)" ><i class="icon icon-note"></i></button>
										<button type="button" class="btn btn-sm btn-danger" title="Remove" ng-click="removeImage(banner.id)" ><i class="icon icon-trash"></i></button>
										<select name="sequence" id="sequence" class="pull-right" ng-model="banner.sequence" ng-change="changeorder(banner)" style="height: 30px">
											<option ng-repeat="seq in sequence">{[{ seq }]}</option>
										</select>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</tab>

		<tab ng-click="changetab('locations')">
			<tab-heading><i class="fa  fa-map-marker"></i> Location Page</tab-heading>
			<div class="panel panel-default">
				<div class="panel-body">
					<button class="btn m-b-xs btn-sm btn-default btn-addon pull-right" ng-click="imageUploaderWithUrl('locations')"><i class="fa fa-plus"></i>Upload New Banner(s)</button>
					<div class="line line-lg line-dashed b-b"></div>
					<div class="col-sm-3" ng-repeat="banner in banners">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="imagelist" style="background:url('{[{amazonlink}]}/uploads/slidernbanner/{[{banner.image}]}') no-repeat center center; background-size:contain">
								</div>
							</div>
							<div class="panel-footer" style="text-align:right">
									<div class="btn-group">
										<button type="button" class="btn btn-sm btn-light" title="Click to toggle hide/show" ng-click="hideShowImage(banner.id)">
											<i class="fa fa-eye" ng-if="banner.status == 1"></i>
											<i class="fa fa-eye-slash" ng-if="banner.status != 1"></i>
										</button>
										<button type="button" class="btn btn-sm btn-info" title="Edit" ng-click="editImage(banner.id)" ><i class="icon icon-note"></i></button>
										<button type="button" class="btn btn-sm btn-danger" title="Remove" ng-click="removeImage(banner.id)" ><i class="icon icon-trash"></i></button>
										<select name="sequence" id="sequence" class="pull-right" ng-model="banner.sequence" ng-change="changeorder(banner)" style="height: 30px">
											<option ng-repeat="seq in sequence">{[{ seq }]}</option>
										</select>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</tab>

		<tab>
			<tab-heading><i class="fa  fa-group"></i> Try A Class Page</tab-heading>
			<div class="panel-panel-default">
				<div class="panel-body">

					<div class="col-sm-3">
						<div class="panel panel-default">
							<div class="panel-heading">Outer 1-on-1 Class Banner</div>
							<div class="panel-body">
								<div class="loader" ng-show="outerOneClassImageLoader">
									<div class="loadercontainer">
										<div class="spinner">
											<div class="rect1"></div>
											<div class="rect2"></div>
											<div class="rect3"></div>
											<div class="rect4"></div>
											<div class="rect5"></div>
										</div>
										Uploading New Banner. Please wait...
									</div>
								</div>
								<div ng-hide="outerOneClassImageLoader" class="imagelist" style="background:url('{[{amazonlink}]}/uploads/slidernbanner/{[{outeroneclassbanner.image}]}') no-repeat center center; background-size:contain">
								</div>
							</div>
							<div class="panel-footer">
								<button class="btn btn-sm btn-default" ng-hide="outerOneClassImageLoader"
									ngf-drop ngf-select class="drop-box" ngf-drag-over-class="dragover" ngf-multiple="false" ngf-allow-dir="true"
				          accept="image/*" ng-model="oneclass.outer" ngf-change="newSingleBanner(oneclass.outer, 'oneclassouter', 'outerOneClassImageLoader')">
									New
								</button>
							</div>
						</div>
					</div>

					<div class="col-sm-3">
						<div class="panel panel-default">
							<div class="panel-heading">Inner 1-on-1 Class Banner</div>
							<div class="panel-body">
								<div class="loader" ng-show="innerOneClassImageLoader">
									<div class="loadercontainer">
										<div class="spinner">
											<div class="rect1"></div>
											<div class="rect2"></div>
											<div class="rect3"></div>
											<div class="rect4"></div>
											<div class="rect5"></div>
										</div>
										Uploading New Banner. Please wait...
									</div>
								</div>
								<div ng-hide="innerOneClassImageLoader" class="imagelist" style="background:url('{[{amazonlink}]}/uploads/slidernbanner/{[{inneroneclassbanner.image}]}') no-repeat center center; background-size:contain">
								</div>
							</div>
							<div class="panel-footer">
								<button class="btn btn-sm btn-default" ng-hide="innerOneClassImageLoader"
									ngf-drop ngf-select class="drop-box" ngf-drag-over-class="dragover" ngf-multiple="false" ngf-allow-dir="true"
				          accept="image/*" ng-model="oneclass.inner" ngf-change="newSingleBanner(oneclass.inner, 'oneclassinner', 'innerOneClassImageLoader')">
									New
								</button>
							</div>
						</div>
					</div>

					<div class="col-sm-3">
						<div class="panel panel-default">
							<div class="panel-heading">Outer Group Class Banner</div>
							<div class="panel-body">
								<div class="loader" ng-show="outerGroupClassImageLoader">
									<div class="loadercontainer">
										<div class="spinner">
											<div class="rect1"></div>
											<div class="rect2"></div>
											<div class="rect3"></div>
											<div class="rect4"></div>
											<div class="rect5"></div>
										</div>
										Uploading New Banner. Please wait...
									</div>
								</div>
								<div ng-hide="outerGroupClassImageLoader" class="imagelist" style="background:url('{[{amazonlink}]}/uploads/slidernbanner/{[{outergroupclassbanner.image}]}') no-repeat center center; background-size:contain">
								</div>
							</div>
							<div class="panel-footer">
								<button class="btn btn-sm btn-default" ng-hide="outerGroupClassImageLoader"
									ngf-drop ngf-select class="drop-box" ngf-drag-over-class="dragover" ngf-multiple="false" ngf-allow-dir="true"
				          accept="image/*" ng-model="groupclass.outer" ngf-change="newSingleBanner(groupclass.outer, 'groupclassouter', 'outerGroupClassImageLoader')">
									New
								</button>
							</div>
						</div>
					</div>

					<div class="col-sm-3">
						<div class="panel panel-default">
							<div class="panel-heading">Inner Group Class Banner</div>
							<div class="panel-body">
								<div class="loader" ng-show="innerGroupClassImageLoader">
									<div class="loadercontainer">
										<div class="spinner">
											<div class="rect1"></div>
											<div class="rect2"></div>
											<div class="rect3"></div>
											<div class="rect4"></div>
											<div class="rect5"></div>
										</div>
										Uploading New Banner. Please wait...
									</div>
								</div>
								<div ng-hide="innerGroupClassImageLoader" class="imagelist" style="background:url('{[{amazonlink}]}/uploads/slidernbanner/{[{innergroupclassbanner.image}]}') no-repeat center center; background-size:contain">
								</div>
							</div>
							<div class="panel-footer">
								<button class="btn btn-sm btn-default" ng-hide="innerGroupClassImageLoader"
									ngf-drop ngf-select class="drop-box" ngf-drag-over-class="dragover" ngf-multiple="false" ngf-allow-dir="true"
				          accept="image/*" ng-model="groupclass.inner" ngf-change="newSingleBanner(groupclass.inner, 'groupclassinner', 'innerGroupClassImageLoader')">
									New
								</button>
							</div>
						</div>
					</div>

				</div>
			</div>
		</tab>

	</tabset>
</center>
