{{ content() }}
<!-- <div class="bg-light lter b-b wrapper-md">
  <button type="button" class="btn btn-default btn-addon pull-right m-t-n-xs" ui-toggle-class="show" target="#aside" ng-click="renderCalender(calendar1)">
  <i class="fa fa-bars"></i> Logs
  </button>
  <h1 class="m-n font-thin h3">Center List</h1>
  <a id="top"></a>
</div> -->
<script type="text/ng-template" id="contactDelete.html">
  <div ng-include="'/be/tpl/contactDelete.html'"></div>
</script>
<script type="text/ng-template" id="contactReview.html">
  <div ng-include="'/be/tpl/contactReview.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Contacts</h1>
  <a id="top"></a>
</div>
  <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Manage Contacts
            </div>
            <div class="panel-body">
                <div class="row wrapper">
                    {#<div class="col-sm-5 m-b-xs">#}

                      <form name="rEquired">
                        <div class="col-sm-3">
                          <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required>
                            <span class="input-group-btn">
                              <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext,searchstat)">Go!</button>
                            </span>
                          </div>
                        </div>

                        <div class="col-sm-3 col-sm-offset-6">
                          <div class="input-group">
                            <select ng-model="searchstat" class="form-control input-sm" ng-change="search(searchtext,searchstat)">
                              <option value="" style="display:none">Filter</option>
                              <option value="undefined">List All</option>
                              <option value="0">New</option>
                              <option value="1">Reviewed</option>
                              <option value="2">Replied</option>
                            </select>
                            <span class="input-group-btn">
                              <button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
                            </span>
                          </div>
                        </div>

                      </form>

                    {#</div>#}
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th style="width:15%">Subject</th>
                                <th style="width:15%">Name</th>
                                <th style="width:15%">Date Submitted</th>
                                <th style="width:15%">Status</th>
                                <th style="width:15%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="mem in data.data">
                               <td>{[{mem.subject}]}</td>
                               <td>{[{mem.name}]}</td>
                               <td>{[{mem.datesubmitted}]}</td>
                               <td ng-if="mem.status == 0"><span class=" text-success font-bold">New</span></td>
                               <td ng-if="mem.status == 1"><span class=" text-primary font-bold">Reviewed</span></td>
                               <td ng-if="mem.status == 2"><span class=" text-warning font-bold">Replied</span></td>
                               <td>
                                  <button type="button" class="btn btn-sm btn-default" ng-click="reviewmodal(mem.id)" title="Review">
                                    <i class="icon icon-magnifier text-info font-bold"></i>
                                  </button>

                                  <button type="button" class="btn btn-sm btn-default" ng-click="deletemodal(mem.id)" title="Delete/Remove">
                                    <i class="icon icon-trash text-danger font-bold"></i>
                                  </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
              </div>
              <div class="panel-footer">
                <center>
                  <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                  <br>
                  <span class="">New Contact/s : {[{ countnew }]}</span>
                </center>
              </div>
          </div>
        </div>
      </div>
  </div>
