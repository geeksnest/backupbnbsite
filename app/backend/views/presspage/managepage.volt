{{ content() }}

<script type="text/ng-template" id="newsDelete.html">
  <div ng-include="'/be/tpl/newsDelete.html'"></div>
</script>

<script type="text/ng-template" id="newsCenterDelete.html">
  <div ng-include="'/be/tpl/newsCenterDelete.html'"></div>
</script>

<script type="text/ng-template" id="newsEdit.html">
  <div ng-include="'/be/tpl/newsEdit.html'"></div>
</script>

<script type="text/ng-template" id="newscenterEdit.html">
  <div ng-include="'/be/tpl/newscenterEdit.html'"></div>
</script>


<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Press News </h1>
  <a id="top"></a>
</div>

<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              List
            </div>

              <div class="panel-body">

                <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs">
                        <form name="rEquired">
                        <div class="input-group">
                          <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required>
                          <span class="input-group-btn">
                          <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)" ng-disabled="rEquired.$invalid">Go!</button>
                            <button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
                          </span>
                        </div>
                      </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                              <th width="15%">News Id</th>
                              <th width="20%">Title</th>
                              <th>Author</th>
                              <th width="10%">Date Created</th>
                              <th width="10%">Date Publish</th>
                              <th>Press Status</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="mem in data.data">
                                <td>{[{ mem.newsid }]}</td>
                                <td class="wrbr">{[{mem.title}]}</td>
                                <td>{[{ mem.author }]} </td>
                                <td>{[{mem.datecreated}]}</td>
                                <td>{[{mem.date}]}</td>
                                <td  ng-if="mem.status == 1">
                                <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>
                                <div class="checkstatuscontent">
                                  <label class="i-switch bg-info m-t-xs m-r">
                                    <input type="checkbox" checked="" ng-click="setstatus(mem.newsid)">
                                    <i></i>
                                  </label>

                                </div>
                                <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.newsslugs"><i class="fa fa-check"></i></spand></div>
                                </td>
                                <td  ng-if="mem.status == 0">
                                <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Deactivated</span></div>
                                <div class="checkstatuscontent">
                                  <label class="i-switch bg-info m-t-xs m-r">
                                    <input type="checkbox" ng-click="setstatus(mem.newsid)">
                                    <i></i>
                                  </label>

                                </div>
                                 <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.newsslugs"><i class="fa fa-check"></i></spand></div>
                                </td>
                                </td>
                                <td >
                                    <a href="" ng-click="editnews(mem.newsid)"><span class="label bg-warning" >Edit</span></a>
                                    <a href="" ng-click="deletenews(mem.newsid)"> <span class="label bg-danger">Delete</span></a>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </div>



              </div>


          </div>
        </div>

      </div>

      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
</fieldset>
