{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="newsimagelist.html">
  <div ng-include="'/be/tpl/newsimagelist.html'"></div>
</script>
<script type="text/ng-template" id="newsimagelist2.html">
  <div ng-include="'/be/tpl/newsimagelist2.html'"></div>
</script>
<script type="text/ng-template" id="imagesrc.html">
  <div ng-include="'/be/tpl/imagesrc.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Pess Page</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formpage" ng-submit="saveNews(news)">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-8">
          <div class="panel panel-default">

            <div class="panel-heading font-bold">
              Press Page Information
            </div>

            <div class="panel-body">
              <div class="col-md-12">
                <b>Title
                  <em class="help text-danger pull-right" ng-show="formpage.title.$error.required">(Required)</em>
                </b>
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.title" required="required" ng-change="onnewstitle(news.title)">
                <br>
              </div>

              <div class="col-md-12">
                <b>Press Page Slugs: </b>
                <input type="text" ng-show="editslug" id="pageslugs" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.slugs" ng-keypress="onslugs(news.slugs)" >
                <span ng-bind="news.slugs"></span>
                <div class="popOver" ng-show="invalidtitle">
                  Slugs is already taken.
                  <span class="pop-triangle"></span>
                </div>
                <div ng-show="editslug" class="ng-hide">
                  <a class="btn btn-danger btn-xs pull-right mg-left" ng-click="cancelnewsslug(news.title)">cancel</a>
                  <a class="btn btn-primary btn-xs pull-right mg-left" ng-click="setslug(news.slugs)">ok</a>
                </div>
                <a class="btn btn-danger btn-xs pull-right mg-left" ng-hide="editslug" ng-click="clearslug(news.title)">clear</a>
                <a class="btn btn-primary btn-xs pull-right mg-left" ng-hide="editslug" ng-click="editnewsslug()">edit slug</a>
                <br><br>
              </div>

              <div class="line line-dashed b-b line-lg"></div>

              <div class="col-md-12">
                <b>Meta Tags
                  <em class="text-muted pull-right">(Optional)</em>
                </b>
                <input type="text" name="metatitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.metatitle">
                <br>
              </div>

              <div class="line line-dashed b-b line-lg"></div>

              <div class="col-md-12">
                <b>Author
                  <em class="text-muted pull-right">(Optional)</em>
                </b>
                <input type="text" class="form-control" ng-model="news.author" name="news.author">
                <br>
              </div>
              <div class="line line-dashed b-b line-lg"></div>

              <div class="col-md-6">
                <div ng-show='news.newslocation == "mainsite"'>
                  <b>Category</b>
                  <select class="form-control m-b" ng-model="news.category" ng-required='news.newslocation =="mainsite"' ng-options="cat.categoryname for cat in category track by cat.categoryid">
                  </select>
                </div>

                <div ng-show='news.newslocation == "center"'>
                  <b>Centers</b>
                  <select chosen multiple class=" chosen-choices form-control w-md" ng-model="news.centers" options="newslocation" ng-options="cat.centerid as cat.centertitle for cat in newslocation" ng-required="workshop.venue == 'bnbcenter'" ng-change="fucktion(workshop.associatedcenter)">
                  </select>
                </div>
              </div>

              <div class="line line-dashed b-b line-lg"></div>

              <div class="col-md-12">
               <b>Short Description</b>
               <em class="text-muted pull-right">(Optional: Maximum of 350 characters.)</em>
               <textarea class="form-control" rows="4" placeholder="Type your description" ng-model="news.newsdescription" ng-maxlength="350"></textarea>
               <br>
             </div>

             <div class="line line-dashed b-b line-lg"></div>

             <div class="col-md-12">
              <b>Body Content
                <em class="text-danger" ng-show="formpage.body.$error.required">(Required)</em>
              </b>
              <a class="btn btn-default btn-sm pull-right" ng-click="imagegallery('lg')">
                <i class="fa fa-file-image-o text"></i>
                Image Gallery
              </a>
              <br><br>
              <textarea name="body" class="ck-editor" ng-model="news.body" required="required"></textarea>
            </div>

            <div class="line line-dashed b-b line-lg"></div>
            
            <div class="col-sm-6">
              <div class="panel panel-default">
                <div class="panel-heading font-bold">
                  Press Status
                </div>
                <div class="panel-body" ng-init="news.status=1">
                  <div class="form-group" >
                    <label class="col-sm-3 control-label">
                      <span class="label bg-info" ng-show="news.status == 1">Activate</span>
                      <span class="label bg-danger" ng-show="news.status == 0">Deactivate</span>
                    </label>

                    <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                      <input type="checkbox" ng-true-value="1" ng-false-value="0" ng-model="news.status" >
                      <i></i>
                    </label>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="panel panel-default">
                <div class="panel-heading font-bold">
                  Press Logo
                  <em class="text-danger pull-right" ng-show="formpage.logo.$error.required">(Required)</em>
                </div>
                <div class="panel-body">
                  <div class="form-group">
                    <div class="input-group m-b pull-left" style="width: 1px;">
                      <span class="input-group-btn">
                        <a class="btn btn-default"  ng-click="ListLogo('lg')">Select Image</a>
                      </span>
                      <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">

                      <input type="text" name="logo" class="form-control" ng-value="news.logo = pathimagelogo " ng-required="true" ng-model="news.logo" placeholder="{[{pathimagelogo}]}" style="width: 170px;" readonly />

                    </div>
                    <div style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/newsimage/{[{news.logo}]}'); width: 65px !important; height: 35px; margin-left: 20px;" class="bg-image pull-left" alt="IMAGE PREVIEW" ng-show="news.logo != ''"></div>
                    <!-- <div class="col-sm-12">
                      <br>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>

          </div> <!-- END OF panel Body-->


        </div>
      </div>

          <div class="col-sm-4">
            <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Date Published
                <em class="text-danger pull-right" ng-show="formpage.date.$error.required">(Required)</em>
              </div>
              <div class="panel-body">
                <div class="input-group">
                  <span class="input-group-btn">
                    <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy"
                    style="max-width:80%"
                    ng-model="news.date"
                    type="text"
                    is-open="opened"
                    datepicker-options="dateOptions"
                    ng-required="true"
                    close-text="Close"
                    ng-init="news.date = '<?php echo date('Y-m-d'); ?>' "
                    disabled>
    <!--                   <span class="input-group-addon" ng-click="open($event)">
                        <i class="glyphicon glyphicon-calendar">
                        </span> -->
                    <button type="button" class="btn btn-default" ng-click="open($event)" style="width:20%"><i class="glyphicon glyphicon-calendar"></i></button>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Press Banner
                <em class="text-danger pull-right" ng-show="formpage.banner.$error.required">(Required)</em>
              </div>
              <div class="panel-body">
                <div class="form-group">
                  <div class="input-group m-b">
                    <span class="input-group-btn">
                      <a class="btn btn-default"  ng-click="ListBanner('lg')">Select Banner</a>
                    </span>
                    <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">
                    <input type="text" name="banner" class="form-control" ng-value="news.banner = pressban.type == 'video' ? amazonpath.url : amazonpath" ng-model="news.banner" placeholder="{[{ pressban.type == 'video' ? amazonpath.url : amazonpath }]}" ng-required='true' readonly />

                  </div>
                  <div class="col-sm-12">
                    <br>
                    <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/newsimage/{[{news.banner}]}" width="100%" height="100%" alt="IMAGE PREVIEW" ng-show="news.banner != ''" ng-if="pressban.type == 'image'">
                    <img src="{[{pressban.thumb}]}" width="100%" height="100%" alt="IMAGE PREVIEW" ng-show="news.banner != ''" ng-if="pressban.type == 'video'">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Press Thumbnail
                <em class="text-danger pull-right" ng-show="formpage.thumbnail.$error.required">(Required)</em>
              </div>
              <div class="panel-body">
                <div class="form-group">
                  <div class="input-group m-b">
                    <span class="input-group-btn">
                      <a class="btn btn-default"  ng-click="ListThumbnail('lg')">Select Banner</a>
                    </span>
                    <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">
                    <input type="text" name="thumbnail" class="form-control" ng-value="news.thumbnail = pressthumb.type == 'video' ? amazonpath2.url : amazonpath2" ng-model="news.thumbnail" placeholder="{[{ pressthumb.type == 'video' ? amazonpath2.url : amazonpath2 }]}" ng-required='true' readonly />

                  </div>
                  <div class="col-sm-12">
                    <br>
                    <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/newsimage/{[{news.thumbnail}]}" width="100%" height="100%" alt="IMAGE PREVIEW" ng-show="news.thumbnail != ''" ng-if="pressthumb.type == 'image'">
                    <img src="{[{pressthumb.thumb}]}" width="100%" height="100%" alt="IMAGE PREVIEW" ng-show="news.thumbnail != ''" ng-if="pressthumb.type == 'video'">
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-4" ng-if='news.newslocation == "mainsite"'>
            <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Set as featured
              </div>
              <div class="panel-body" ng-init="news.featurednews=0">
                <div class="form-group" >
                  <label class="col-sm-3 control-label">
                    <span class="label bg-success" ng-show="news.featurednews == 1">Yes</span>
                    <span class="label bg-dark" ng-show="news.featurednews == 0">No</span>
                  </label>

                  <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                    <input type="checkbox" ng-true-value="'1'" ng-model="news.featurednews" ng-false-value="'0'">
                    <i></i>
                  </label>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-4" ng-show='news.newslocation =="mainsite"'>
            <div class="panel panel-default">
              <div class="panel-heading font-bold">
                News Preview
              </div>
              <div class="panel-body" ng-init="news.status=1">
                <div class="form-group" >
                  <a class="btn btn-primary btn-md" ng-disabled="formpage.$invalid || formpage.$pending || invalidtitle==true" ng-click="previewNews(news)">Live Preview</a>
                </div>
                <span class="text-danger" ng-show="formpage.$invalid || formpage.$pending || invalidtitle==true">Please fill all required fields before Preview</span>
              </div>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="panel panel-default">
              <div class="panel-heading font-bold">
                <span class="text-danger" ng-show="formpage.$invalid || formpage.$pending || invalidtitle==true">Please fill all required fields before submitting</span>
              </div>
              <div class="panel-body">
                <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
                <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid || formpage.$pending || invalidtitle==true" scroll-to="Scrollup">Submit</button>
              </div>
            </div>
          </div>

<!--         <div class="col-sm-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <strong>Workshop Titles</strong>
            </div>
            <div class="panel-body">
              <form name="relatedform">
                <span ng-repeat="title in workshoptitles" ng-model="title" class="btn m-b-xs btn-sm btn-default btn-rounded btn-addon margin_right_5px" ng-click="addrelated(title)"><i class="fa fa-plus pull-right"></i>{[{title}]}</span>
              </form>
            </div>
          </div>
          <div class="panel panel-primary">
            <div class="panel-heading">
              <strong>Related Workshop</strong>
            </div>
            <div class="panel-body">
              <form name="relatedform">
                <span ng-repeat="title in related track by $index" ng-hide="empty" ng-model="titles" class="btn m-b-xs btn-sm btn-success btn-rounded btn-addon margin_right_5px" ng-click="removerelated(title, related)"><i class="fa fa-minus pull-right"></i>{[{title}]}</span>
                <span ng-show="empty">Empty</span>
              </form>
            </div>
          </div>
        </div> -->

      </div>


      <!-- <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid || formpage.$pending || invalidtitle==true" scroll-to="Scrollup">Submit</button>
            </footer>
        </div>
      </div> -->
    </div>
  </fieldset>
</form>
