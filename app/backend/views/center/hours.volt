<div class="wrapper bg-light lter b-b">
	<div class="btn-group m-r-sm">
		<h1 class="m-n font-thin h3">Center Hours</h1>
	</div>
</div>

<!-- <div class="col-md-12" ng-init="socialtablelist = true"> -->
<div class="col-md-12 wrapper-md" >
	<div class="panel panel-default">
		<div class="panel-heading font-bold">
			Open Hours
		</div>
		<div class="panel-body">
			<alert ng-repeat="alert in hoursalerts" type="{[{alert.type }]}" close="hourscloseAlert($index)">{[{ alert.msg }]}</alert>
			<form name="formopenhours" ng-submit="saveOpenhours(openhours)">
				<?php  
				$modelname = "openhours";
				$modelname_dum = "openhour";
				$days = array("mon","tue","wed","thu","fri");
				$models = array();
				foreach($days as $day) {
					$models[] = array(
						"day" => $day,
						"timestart" => $modelname.".".$day."timestart",
						"timestart2" => $modelname.".".$day."timestart",
						"timestart_hour" => $modelname_dum.".".$day."hourstart",
						"timestart_minute" => $modelname_dum.".".$day."minutesstart",
						"formatstart" => $modelname.".".$day."formatstart",
						"timeend" => $modelname.".".$day."timeend",
						"formatend" => $modelname.".".$day."formatend"
						);
				}
				foreach($models as $model) {
					?>
					<h4 class="m-t-lg">
						<?php echo ucwords($model['day']); ?>
					</h4>
					<div class="col-sm-12">

						<select class="inputBox" ng-model="<?php echo $model['timestart'];?>" 
							ng-options="mem.time as mem.time for mem in timelist">
						</select>
						<!--  -->
						<select class="inputBox" ng-model="<?php echo $model['formatstart']; ?>">
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
						~
						<select class="inputBox" ng-model="<?php echo $model['timeend']; ?>">
							<option ng-repeat="mem in timelist">{[{mem.time}]}</option>
						</select>

						<select class="inputBox" ng-model="<?php echo $model['formatend']; ?>">
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>


						<div class="line line-dashed b-b line-lg"></div>
					</div>

					<?php } ?>

					<h4 class="m-t-lg">
						Saturday
					</h4>
					<div class="col-sm-12" ng-init="openhours.satstatus=1">
						<div style="padding:10px 0;width:60px;float:left;">
							<label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
								<input type="checkbox" ng-true-value="'1'" ng-false-value="'0'" ng-model="openhours.satstatus" >
								<i></i>
							</label>
						</div>
						<div style="padding:10px;width:100px;float:left;">
							<span class="label bg-success" ng-show="openhours.satstatus == 1">Show</span>
							<span class="label bg-dark" ng-show="openhours.satstatus == 0">Hidden</span>
						</div>
					</div>
					<div class="col-sm-12">
						<select class="inputBox" ng-init="openhours.sattimestart ='09:00'" ng-model="openhours.sattimestart">
							<option ng-repeat="mem in timelist">{[{mem.time}]}</option>
						</select>

						<select class="inputBox" ng-init="openhours.satformatstart = 'AM'" ng-model="openhours.satformatstart">
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
						~
						<select class="inputBox" ng-init="openhours.sattimeend ='07:00'" ng-model="openhours.sattimeend">
							<option ng-repeat="mem in timelist">{[{mem.time}]}</option>
						</select>

						<select class="inputBox" ng-init="openhours.satformatend = 'PM'" ng-model="openhours.satformatend">
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>

						<div class="line line-dashed b-b line-lg"></div>
					</div>


					<h4 class="m-t-lg">
						Sunday 
					</h4>
					<div class="col-sm-12" ng-init="openhours.sunstatus=1">
						<div style="padding:10px 0;width:60px;float:left;" >
							<label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
								<input type="checkbox" ng-true-value="'1'" ng-false-value="'0'" ng-model="openhours.sunstatus" >
								<i></i>
							</label>
						</div>
						<div style="padding:10px;width:100px;float:left;">
							<span class="label bg-success" ng-show="openhours.sunstatus == 1">Show</span>
							<span class="label bg-dark" ng-show="openhours.sunstatus == 0">Hidden</span>
						</div>
					</div>
					<div class="col-sm-12">
						<select class="inputBox" ng-init="openhours.suntimestart ='09:00'" ng-model="openhours.suntimestart">
							<option ng-repeat="mem in timelist">{[{mem.time}]}</option>
						</select>

						<select class="inputBox" ng-init="openhours.sunformatstart = 'AM'" ng-model="openhours.sunformatstart">
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
						~
						<select class="inputBox" ng-init="openhours.suntimeend ='07:00'" ng-model="openhours.suntimeend">
							<option ng-repeat="mem in timelist">{[{mem.time}]}</option>
						</select>

						<select class="inputBox" ng-init="openhours.sunformatend = 'AM'" ng-model="openhours.sunformatend">
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
					</div>

					<footer class="panel-footer text-right bg-light lter">
						<button type="submit" id="submit" class="btn btn-success" ng-disabled="formNews.$invalid">Save</button>
					</footer>

				</form>
			</div>
		</div>
	</div>