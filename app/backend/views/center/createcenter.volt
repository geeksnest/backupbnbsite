{{ content() }}

<script type="text/ng-template" id="authorizeHelp.html">
  <div ng-include="'/be/tpl/authorizeHelp.html'"></div>
</script>

<script type="text/ng-template" id="paypalHelp.html">
  <div ng-include="'/be/tpl/paypalHelp.html'"></div>
</script>

<fieldset ng-disabled="isSaving">
<form id="createcenterForm" name="formcreatecenter" ng-submit="saveCenter(center)">

<div class="bg-light lter b-b wrapper-md" id="gototop">
  <span class="m-n font-thin h3">Create Center</span>
  <a id="top"></a>
  <span class="pull-right">
    {#<a ui-sref="managecenter({userid: '<?php echo $username["bnb_userid"];?>'})" class="btn btn-md btn-info" title="Center List"><i class="fa fa-list-alt"></i></a>#}
    <button type="submit" class="btn btn-md btn-success" title="Submit"><i class="fa fa-save"></i></button>
  </span>
</div>

  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)" >{[{ alert.msg }]}</alert>

    <div class="row">

      <div class="col-sm-8">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">Center Information</div>
          <div class="panel-body">
            <div class="col-sm-12 form-group">
              <label>Center Name</label>
              <input id="centername" type="text" name="centertitle" class="form-control" ng-model="center.centertitle" ng-change="oncentertitle(center.centertitle)" required>
            </div>

            <div class="col-sm-12 form-group">
              <label class="font-bold">Center SEO:</label>
              <small><i>note: you can input 3 slugs</i></small>
              <tags-input id="centerslugs" ng-model="center.slugs" display-property="slug" name="slugs" placeholder="Add a slug" min-tags="1" max-tags="3" on-tag-added="onslug(center.slugs)"></tags-input>
            </div>

            <div class="col-sm-12 form-group">
              <label>Short Description</label> <em>(optional)</em>
             <textarea id="shortdesc" class="form-control" rows="4" placeholder="Type your Description" ng-model="center.centerdesc" maxlength="400"></textarea>
            </div>

            <div class="col-sm-8 form-group">
              <label>Address</label>
              <input id="address" type="text" class="form-control" ng-model="center.centeraddress" required>
            </div>

            <div class="col-sm-4 form-group">
              <label>Region</label>
              <a class="btn btn-primary" disabled ng-show="hidedistrict">No Open Region</a>
              <div ui-module="select2" ng-hide="hideregion">
                <select id="region" ui-select2 ng-model="center.centerregion" class="form-control" required>
                  <option ng-repeat="list in regions" ng-value="list.regionid">{[{list.regionname}]}</option>
                </select>
              </div>
            </div>

            <div class="col-sm-5 form-group">
              <label>State</label>
              <div ui-module="select2">
                <select id="state" ui-select2 class="form-control"  ng-model="center.centerstate"  ng-change="statechange(center.centerstate)" required>
                  <option ng-repeat="list in centerstate" ng-value="list.state_code">{[{list.state}]}</option>
                </select>
              </div>
            </div>

            <div class="col-sm-5 form-group">
              <label>City</label>
              <div ui-module="select2">
                <select id="city" ui-select2 class="form-control" ng-model="center.centercity" ng-change="citychange(center.centercity)" required>
                  <option ng-repeat="list in centercity" ng-value="list.city">{[{list.city}]}</option>
                </select>
              </div>
            </div>

            <div class="col-sm-2 form-group">
              <label>Zip code</label>
              <div ui-module="select2">
                <select id="zipcode" ui-select2 class="form-control" ng-model="center.centerzip"  required="required">
                  <option ng-repeat="list in getcenterzip" ng-value="list.zip">{[{list.zip}]}</option>
                </select>
              </div>
            </div>

           {#<div class="col-sm-12">
              Meta Tags
              <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="center.metatitle" required="required">
               <span class="help-block m-b-none">Use comma (,) for multiple meta tags. E.g. (initial awakening, sedona healing, dahn yoga)</span>
              <div class="line line-dashed b-b line-lg"></div>
            </div>#}

            <div class="col-sm-12 form-group">
              <label>Meta Title</label>
              <input id="metatitle" type="text" class="form-control" ng-model="center.metatitle"/>
            </div>

            <div class="col-sm-12 form-group">
              <label>Meta Description</label>
              <textarea id="metadesc" class="form-control rv" rows="4" placeholder="Type your message" maxlength="150"
              ng-model="center.metadesc"></textarea>
            </div>

          </div> {# End OF panel Body #}
        </div>

        <div class="panel panel-default">
          <div class="panel-heading font-bold">Facebook Custom Audience Pixel Code</div>
          <div class="panel-body">
            <div class="col-sm-12">
              <textarea id="fbcapc" class="form-control rv" rows="4" placeholder="Paste your Code Here" ng-model="center.fbcapc"></textarea>
            </div>
          </div>
        </div>
      </div> {#end of col-sm-8#}

      <div class="col-sm-4">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">Center Opening Date</div>
             <div class="panel-body">
              <div class="col-sm-12">
                <div class="input-group w-md">
                  <span class="input-group-btn">
                    <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="center.openingdate" is-open="opened" datepicker-options="dateOptions" close-text="Close" type="text" disabled />
                    <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                  </span>
                </div>
              </div>
            </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading font-bold">Type of Center</div>
          <div class="panel-body" ng-init="center.centertype = 1">
            <div class="col-sm-12" >
              <div class="radio">
                <label class="i-checks">
                  <input id="typebnb" type="radio" ng-model="center.centertype" ng-value="1" >
                  <i></i>
                  Body and Brain
                </label>
              </div>
              <div class="radio">
                <label class="i-checks">
                  <input id="typefranc" type="radio" ng-model="center.centertype" ng-value="2" >
                  <i></i>
                  Franchise
                </label>
              </div>
              <div class="radio">
                <label class="i-checks">
                  <input id="typeaffil" type="radio" ng-model="center.centertype" ng-value="3" >
                  <i></i>
                  Operated by affiliated company
                </label>
              </div>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Authorize Account Details
            <span class="text-muted m-l-sm pull-right">
              <a href="" ng-click="authorizehelp()">
                <i class="fa  fa-minus-square-o"></i>Help
              </a>
            </span>
          </div>
          <div class="panel-body" ng-init="center.centertype = 1">
            <div class="col-sm-12" >
                <label>Login Id</label>
                <input id="loginid" type="text" name="centertitle" class="form-control" ng-model="center.authorizeid" pattern="[a-zA-Z0-9]+" title="Alphanumeric only" required>
                <br>
                <label>Transaction Key</label>
                <input id="transkey" type="text" name="centertitle" class="form-control" ng-model="center.authorizekey" pattern="[a-zA-Z0-9]+" title="Alphanumeric only" required>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Paypal Account Details
            <span class="text-muted m-l-sm pull-right">
              <a href="" ng-click="paypalhelp()">
                <i class="fa  fa-minus-square-o"></i>Help
              </a>
            </span>
          </div>
          <div class="panel-body" ng-init="center.centertype = 1">
            <div class="col-sm-12">
                Paypal Id
                <input id="paypalid" type="text" name="centertitle" class="form-control" ng-model="center.paypalid" required>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading font-bold">Center Contact</div>
          <div class="panel-body">
            <div class="form-group col-sm-12">
              <label>Phone</label>
              <input id="phonenumber" type="text" class="form-control phone_us" ng-model="center.phonenumber" ng-init="center.phonenumber=8774779642" minlength="14" title="Please enter a valid phone number or make it blank." required/>
            </div>
            <div class="form-group col-sm-12">
              <label>Email</label>
              <input id="email" type="email" class="form-control" ng-model="center.email" ng-init="center.email='phil@bodynbrain.com'" pattern="^[^\s@]+@[^\s@]+\.[^\s@]{2,}$" title="Please input a valid email address or make it blank." required/>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading font-bold">Center Status</div>
          <div class="panel-body" ng-init="center.status=1">
            <div class="form-group" >
              <div class="col-sm-2">
                <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                  <input id="centerstatus" type="checkbox" ng-true-value="1" ng-model="center.status" ng-false-value="0">
                  <i></i>
                </label>
              </div>
              <div class="col-sm-8">
                <label class="col-sm-3 control-label">
                  <span class="label bg-info" ng-show="center.status == 1">Online</span>
                  <span class="label bg-danger" ng-show="center.status == 0">Offline</span>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div> {# end of col-sm-4 #}


        {#<div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Starters Package Prices
              <span class="text-muted m-l-sm pull-right">
                <a href="" ng-click="paypalhelp()">
                  <i class="fa  fa-minus-square-o"></i>
                  Help
                </a>
              </span>
            </div>
            <div class="panel-body">
              <div class="col-sm-6">
                <div class="col-sm-12" >
                 1-on-1 Intro Session <em>(original price)</em>
                 <input type="text" name="centertitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="center.origsessionprice" required="required" onkeypress="return isNumberKey(event)" >
               </div>
               <div class="col-sm-12" >
                1-on-1 Intro Session <em>(discounted price)</em>
                <input type="text" name="centertitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="center.dissessionprice" required="required" onkeypress="return isNumberKey(event)">
              </div>
            </div>

            <div class="col-sm-6">
              <div class="col-sm-12" >
               1 Group Class + 1-on-1 Intro Session <em>(original price)</em>
               <input type="text" name="centertitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="center.origgroupprice" required="required" onkeypress="return isNumberKey(event)">
             </div>
             <div class="col-sm-12" >
              1 Group Class + 1-on-1 Intro Session <em>(discounted price)</em>
              <input type="text" name="centertitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="center.disgroupprice" required="required" onkeypress="return isNumberKey(event)">
            </div>
          </div>

        </div>
      </div>
    </div>#}

    {# <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Beneplace Package Prices

            </div>
            <div class="panel-body">
              <div class="col-sm-6">
                <div class="col-sm-12" >
                 1 Month <em>(original price)</em>
                 <input type="text" name="centertitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="center.orig1monthprice" required="required" onkeypress="return isNumberKey(event)" ng-change="percent(center.orig1monthprice)">
               </div>
               <div class="col-sm-12" >
                1 Month <em>(discounted price)</em>
                <input type="text" name="centertitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="center.dis1monthprice" required="required" onkeypress="return isNumberKey(event)" disabled="">
              </div>
            </div>

            <div class="col-sm-6">
              <div class="col-sm-12" >
               3 Months <em>(original price)</em>
               <input type="text" name="centertitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="center.orig3monthprice" required="required" onkeypress="return isNumberKey(event)" ng-change="percent2(center.orig3monthprice)">
             </div>
             <div class="col-sm-12" >
              3 Months <em>(discounted price)</em>
              <input type="text" name="centertitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="center.dis3monthprice" required="required" onkeypress="return isNumberKey(event)" disabled="">
            </div>
          </div>

        </div>
      </div>
    </div> #}

    </div> {# End of row #}

    {#<div class="row">
      <div class="panel-body">
          <footer class="panel-footer text-right bg-light lter">
            <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
            <button type="submit" class="btn btn-success" ng-disabled="formcreatecenter.$invalid">Submit</button>
          </footer>
      </div>
    </div>#}

  </div> <!-- End of wrapper-md-->
</form>
</fieldset>

<script language="javascript">
  function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
</script>
