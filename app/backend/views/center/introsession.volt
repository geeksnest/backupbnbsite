<script type="text/ng-template" id="sessionView.html">
  <div ng-include="'/be/tpl/sessionView.html'"></div>
</script>

  <!-- header -->
  <div class="wrapper bg-light lter b-b" style="height:61px;">
    <div class="btn-group m-r-sm">
      <h1 class="m-n font-thin h3">1-on-1 Intro Session</h1>
    </div>

  </div>


<fieldset ng-disabled="isSaving">


  <div class="wrapper-md">

    <div class="row wrapper">
      <div class="col-sm-5 m-b-xs">
        <div class="input-group">
          <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
          </span>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12"><alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert></div>

            
            <div class="panel-body">
              <div class="table-responsive">
                      <table class="table table-striped b-t b-light">
                          <thead>
                              <tr>
                                 
                                  <th style="width:5%">Status</th>
                                  <th style="width:12.2%">Confirmation</th>
                                  <th style="width:12.2%">Center</th>
                                  <th style="width:12.2%">Name</th>
                                  <th style="width:12.2%">Email</th>
                                  <th style="width:12.2%">Payment</th>
                                  <th style="width:12.2%">Device</th>
                                  <th style="width:5%">Q'ty</th>
                                  <th style="width:12.2%">Action</th>
                                  
                              </tr>
                          </thead>
                          <tbody>

                              <tr ng-repeat="list in sessiondata">
                                <td ng-if="list.adminsessionstatus == 0 && list.centersessionstatus == 0"><span class="label bg-warning">U</span></td>
                                <td ng-if="list.adminsessionstatus == 1 && list.centersessionstatus == 0"><span class="label bg-primary">A</span></td>
                                <td ng-if="list.adminsessionstatus == 0 && list.centersessionstatus == 1"><span class="label bg-info">C</span></td>
                                <td ng-if="list.adminsessionstatus == 1 && list.centersessionstatus == 1"><span class="label bg-success">V</span></td>
                                  <td>{[{list.confirmationnumber}]}</td>
                                  <td>{[{list.centertitle}]}</td>
                                  <td>{[{list.name}]}</td>
                                  <td>{[{list.email}]}</td>
                                  <td>{[{list.payment}]}</td>
                                  <td ng-if="list.device == 'android' || list.device == 'iphone' || list.device == 'blackberry' || list.device == 'windows-phone' || list.device == 'ipod' || list.device == 'ipad'">Mobile</td>
                                  <td ng-if="list.device == 'chrome-book'">Tablet</td>
                                  <td ng-if="list.device == 'desktop'">Desktop</td>
                                  <td ng-if="list.device == '' || list.device == null">none</td>
                                  <td>{[{list.quantity}]}</td>
                                  <td><a href="" ng-click="viewsession(list.sessionid)"><span class="label bg-info">view</span></a></td>
                              </tr>
                            
                          </tbody>
                      </table>
                  </div>
                </div>
    


  </div> <!-- end of row -->

  <div class="row">
    <div class="panel-body">
      <footer class="panel-footer text-center bg-light lter">
        <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
      </footer>
    </div>
  </div>


  
</div>
</fieldset>

<script language="javascript">
  function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
</script>