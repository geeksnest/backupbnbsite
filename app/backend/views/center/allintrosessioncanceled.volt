{{ content() }}

<script type="text/ng-template" id="sessionView2.html">
  <div ng-include="'/be/tpl/sessionView2.html'"></div>
</script>

<script type="text/ng-template" id="groupsessioncanceledView.html">
  <div ng-include="'/be/tpl/groupsessioncanceledView.html'"></div>
</script>

<script type="text/ng-template" id="groupsessioncanceledView.html">
  <div ng-include="'/be/tpl/groupsessioncanceledView.html'"></div>
</script>

<script type="text/ng-template" id="sessionVerify.html">
  <div ng-include="'/be/tpl/sessionVerify.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <a class="btn btn-default btn-addon pull-right m-t-n-xs" ui-sref="allintrosession({userid: '<?php echo $username["bnb_userid"];?>'})">
    <i class="glyphicon glyphicon-chevron-left"></i> Go back
  </a>
  <h1 class="m-n font-thin h3">1-on-1 Intro Session List (Cancelled)</h1>
  <a id="top"></a>
</div>


<fieldset ng-disabled="isSaving">
<div class="hbox hbox-auto-xs hbox-auto-sm">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              1-on-1 Intro Session List (Canceled)
            </div>

              <div class="panel-body">

                <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs">
                        <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                  <table class="table table-striped b-t b-light">
                    <thead>
                      <tr>
                        <th style="width:12.2%">Confirmation</th>
                        <th style="width:12.2%">Center</th>
                        <th style="width:12.2%">Name</th>
                        <th style="width:12.2%">Email</th>
                        <th style="width:12.2%">Schedule</th>
                        <th style="width:12.2%">Date Enrolled</th>
                        <th style="width:12.2%">Date Cancelled</th>
                        <th style="width:12.2%">Payment</th>
                        <th style="width:12.2%">Device</th>
                        <th style="width:5%">Q'ty</th>
                        <th style="width:12.2%">Action</th>

                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-if="sessiondata.length == 0"><td>No records found...</td></tr>
                      <tr ng-repeat="list in sessiondata">
                        <td>{[{list.confirmationnumber}]}</td>
                        <td>{[{list.centertitle}]}</td>
                        <td>{[{list.name}]}</td>
                        <td>{[{list.email}]}</td>
                        <td>{[{list.day}]} <br> {[{list.hour}]}</td>
                        <td>{[{list.classdatecreated}]}</td>
                        <td>{[{list.daterefunded}]}</td>
                        <td>{[{list.payment}]}</td>
                        <td ng-if="list.device == 'android' || list.device == 'iphone' || list.device == 'blackberry' || list.device == 'windows-phone' || list.device == 'ipod' || list.device == 'ipad'">Mobile</td>
                        <td ng-if="list.device == 'chrome-book'">Tablet</td>
                        <td ng-if="list.device == 'desktop'">Desktop</td>
                        <td ng-if="list.device == '' || list.device == null">none</td>
                        <td>{[{list.quantity}]}</td>
                        <td><a href="" ng-click="viewsession(list.sessionid)"><span class="label bg-info">view</span></a></td>
                      </tr>

                    </tbody>
                  </table>
                </div>



              </div>


          </div>
        </div>

      </div>

      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
</div>
</fieldset>
