<script type="text/ng-template" id="membershipAdd.html">
  <div ng-include="'/be/tpl/membershipAdd.html'"></div>
</script>
<script type="text/ng-template" id="membershipEdit.html">
  <div ng-include="'/be/tpl/membershipEdit.html'"></div>
</script>
<script type="text/ng-template" id="membershipDelete.html">
  <div ng-include="'/be/tpl/membershipDelete.html'"></div>
</script>

<div >
  <!-- header -->
  <div class="wrapper bg-light lter b-b" style="height:61px;">
    <div class="btn-group m-r-sm">
      <h1 class="m-n font-thin h3">Cell Phone setup</h1>
    </div>
  </div>


<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    

    <div class="row">
      <div class="col-sm-8">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      </div>
      <div class="col-sm-8">
        <div class="panel panel-primary">

          <div class="panel-heading font-bold">
            Center manager’s cell phone setup
          </div>

          <div class="panel-body">

            <form name="formcontact" ng-submit="saveContact(contact)">
              <div class="col-sm-3" style="padding: 5px 0;margin-bottom:5px;">
                Cell phone number:
              </div>
              <div class="col-sm-3" style="margin-bottom:5px;">
                <input type="text"  onkeypress="return isNumberKey(event)" class="form-control" ng-model="contact.phone1" maxlength="3" required>
              </div>
              <div class="col-sm-3" style="margin-bottom:5px;">
                <input type="text"  onkeypress="return isNumberKey(event)" class="form-control" ng-model="contact.phone2" maxlength="3" required>
              </div>
              <div class="col-sm-3" style="margin-bottom:5px;">
                <input type="text"  onkeypress="return isNumberKey(event)" class="form-control" ng-model="contact.phone3" maxlength="4" required>
              </div>

               <div class="line line-dashed b-b line-lg"></div>

              <div class="col-sm-3" style="padding: 5px 0;margin-bottom:5px;">
                Cell phone carrier:
              </div>
              <div class="col-sm-9" style="margin-bottom:5px;">
                <select name="account" class="form-control m-b" ng-model="contact.carrier" required>
                  <option value=""></option>
                  <option value="1">verizon</option>
                  <option value="2">AT&amp;T</option>
                  <option value="3">Sprint</option>
                  <option value="4">T-Mobile</option>
                  <option value="5">Virgin Mobile</option>
                  <option value="6">Metro PCS</option>
                </select>
              </div>

              

            <div class="line line-dashed b-b line-lg"></div>

            <div class="col-sm-12">
              <div style="float:left;width:20%;">
                <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                  <input type="checkbox" ng-true-value="'1'" ng-false-value="'0'" ng-model="contact.status1" >
                  <i></i>
                </label>
              </div>
              <div style="float:left;width:80%;">
                <span>
                  Do you want to receive a text message when people make an individual introductory session appointment?
                </span>
              </div>
            </div>

            <div class="line line-dashed b-b line-lg"></div>

            <div class="col-sm-12">
              <div style="float:left;width:20%;">
                <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                  <input type="checkbox" checked="" ng-model="contact.status2" ng-true-value="'1'" ng-false-value="'0'">
                  <i></i>
                </label>
              </div>
              <div style="float:left;width:80%;">
                <span>
                  Do you want to receive a text message when people sign up for online membership?
                </span>
              </div>
            </div>

            <div class="line line-dashed b-b line-lg"></div>

            <div class="col-sm-12">
              <div style="float:left;width:20%;" >
                <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                  <input type="checkbox" checked="" ng-model="contact.status3" ng-true-value="'1'" ng-false-value="'0'">
                  <i></i>
                </label>
              </div>
              <div style="float:left;width:80%;">
                <span>
                  Do you want to receive a text message when people ask a question about your center?
                </span>
              </div>
            </div>

            <footer class="panel-footer text-right bg-light lter">
              <a class="btn btn-info" ng-click="testContact(contact)">Test Sms</a>
              <button type="submit" id="submit" class="btn btn-success">Save</button>
            </footer>
            </form>




          </div> <!-- END OF panel Body-->


        </div>
      </div>




    </div> <!-- end of row -->
  </div>
</fieldset>

<script language="javascript">
  function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
</script>