<script type="text/ng-template" id="membershipAdd.html">
  <div ng-include="'/be/tpl/membershipAdd.html'"></div>
</script>
<script type="text/ng-template" id="membershipEdit.html">
  <div ng-include="'/be/tpl/membershipEdit.html'"></div>
</script>
<script type="text/ng-template" id="membershipDelete.html">
  <div ng-include="'/be/tpl/membershipDelete.html'"></div>
</script>

<div>
  <!-- header -->
  <div class="wrapper bg-light lter b-b" style="height:61px;">
    <div class="btn-group m-r-sm">
      <h1 class="m-n font-thin h3">Pricing</h1>
    </div>
  </div>

<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    

    <div class="row">

      <div class="col-sm-12">
        <div class="panel panel-primary">

          <div class="panel-heading font-bold">
            Pricing
          </div>

          <div class="panel-body">
            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
            Registration Fee
            <div class="col-sm-12">
              <div class="statusswitch">
                <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                  <input type="checkbox" ng-true-value="'1'" ng-model="regfeestatus" ng-false-value="'0'" class="ng-pristine ng-untouched ng-valid">
                  <!-- <input type="checkbox" ng-true-value="'1'" ng-model="regfeestatus" ng-false-value="'0'" class="ng-pristine ng-untouched ng-valid" ng-click="regfeeclick(regfeestatus)"> -->
                  <i></i>
                </label>
              </div>
              <div class="statusinfo">
                <span class="label bg-info" ng-show="regfeestatus == 1">Show</span>
                <span class="label bg-danger ng-hide" ng-show="regfeestatus == 0">Hidden</span>
              </div>
            </div>
            <div class="input-group m-b">
              <span class="input-group-addon">$</span>
              <input type="text"  onkeypress="return isNumberKey(event)" class="form-control" ng-model="regfee">
              <span class="input-group-addon">.00</span>
            </div>

            One Regular Class
            <div class="col-sm-12">
              <div class="statusswitch">
                <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                  <input type="checkbox" ng-true-value="'1'" ng-model="regclassstatus" ng-false-value="'0'" class="ng-pristine ng-untouched ng-valid">
                  <!-- <input type="checkbox" ng-true-value="'1'" ng-model="regclassstatus" ng-false-value="'0'" class="ng-pristine ng-untouched ng-valid" ng-click="regclassclick(regclassstatus)"> -->
                  <i></i>
                </label>
              </div>
              <div class="statusinfo">
                <span class="label bg-info" ng-show="regclassstatus == 1">Show</span>
                <span class="label bg-danger ng-hide" ng-show="regclassstatus == 0">Hidden</span>
              </div>
            </div>
            <div class="input-group m-b">
              <span class="input-group-addon">$</span>
              <input type="text"  onkeypress="return isNumberKey(event)" class="form-control" ng-model="regclass">
              <span class="input-group-addon">.00</span>
            </div>

            Individual Introductory Session Fee
            <div class="col-sm-12">
              <div class="statusswitch">
                <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                  <input type="checkbox" ng-true-value="'1'" ng-model="sessionfeestatus" ng-false-value="'0'" class="ng-pristine ng-untouched ng-valid">
                  <!-- <input type="checkbox" ng-true-value="'1'" ng-model="sessionfeestatus" ng-false-value="'0'" class="ng-pristine ng-untouched ng-valid" ng-click="sessionfeeclick(sessionfeestatus)"> -->
                  <i></i>
                </label>
              </div>
              <div class="statusinfo">
                <span class="label bg-info" ng-show="sessionfeestatus == 1">Show</span>
                <span class="label bg-danger ng-hide" ng-show="sessionfeestatus == 0">Hidden</span>
              </div>
            </div>
            <div class="input-group m-b">
              <span class="input-group-addon">$</span>
              <input type="text"  onkeypress="return isNumberKey(event)" class="form-control" ng-model="sessionfee">
              <span class="input-group-addon">.00</span>
            </div>

            <footer class="panel-footer text-right bg-light lter">
              <button type="submit" class="btn btn-success" ng-click="updatepricing(regfee, regfeestatus, regclass, regclassstatus, sessionfee, sessionfeestatus)">Save</button>
            </footer>


          </div> <!-- END OF panel Body-->


      </div>
    </div>

    <div class="col-sm-12">
      <alert ng-repeat="alert in membershipalerts" type="{[{alert.type }]}" close="closemembershipAlert($index)">{[{ alert.msg }]}</alert>
    </div>

    <div class="col-sm-12">
        <div class="panel panel-primary">

          <div class="panel-heading font-bold">
            Regular Membership
          </div>

          <div class="panel-body">

            <button class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addnewmembership()"><i class="fa fa-plus"></i>Add New Membership</button>



            <div class="row wrapper">
              <div class="col-sm-5 m-b-xs">
                <div class="input-group">
                  <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                  <span class="input-group-btn">
                    <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                  </span>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table table-striped b-t b-light">
                <thead>
                  <tr>

                    <th style="width:40%">Membership Period</th>
                    <th style="width:35%">Frequent</th>
                    <th style="width:15%">Price</th>
                    <th style="width:20%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="mem in membershipdata">

                    <td ng-if="mem.period == 1">1 Month</td>
                    <td ng-if="mem.period == 3">3 Month</td>
                    <td ng-if="mem.period == 6">6 Month</td>
                    <td ng-if="mem.period == 12">1 Year</td>
                    <td ng-if="mem.period == 24">2 Year</td>
                    <td ng-if="mem.period == 36">3 Year</td>
                    <td ng-if="mem.period == 110">10 Classes</td>
                    <td ng-if="mem.period == 120">20 Classes</td>

                    <td ng-if="mem.frequent == 7">Unlimited</td>
                    <td ng-if="mem.frequent == 2">2 times a week</td>
                    <td ng-if="mem.frequent == 3">3 times a week</td>
                    <td ng-if="mem.frequent == 0">No mention</td>
                   
                    <td>{[{ mem.price}]}</td>
                    <td>
                      <a href="" ng-click="editmembership(mem.membershipid)"><span class="label bg-warning" >Edit</span></a>
                      <a href="" ng-click="deletemembership(mem.membershipid)"> <span class="label bg-danger">Delete</span></a>
                    </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="4" align=center>
                    <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>

        </div> <!-- END OF panel Body-->

      </div>
    </div>

  </div> <!-- end of row -->

  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-info">
          <div class="panel-heading font-bold">
            Private Session
              <div class="pull-right">
                <div class="statusinfo">
                  <span class="label bg-{[{type}]}">{[{statusmsg}]}</span>
                </div>
                <div class="statusswitch" style="margin-top:-4px;">
                  <label class="i-switch bg-info m-t-xs m-r">
                    <input type="checkbox" ng-true-value="'1'" ng-false-value="'0'" ng-model="privatesession.status" ng-click="privatestatus(privatesession.status)" class="ng-valid ng-dirty ng-valid-parse ng-touched">
                    <i></i>
                  </label> 
                </div> 
              </div>
          </div>
          <div class="panel-body">
            <div class="col-sm-12">

              <alert ng-repeat="privatesessionAlert in privatesessionAlert" type="{[{privatesessionAlert.type}]}" close="closeprivatesessionAlert($index)">{[{ privatesessionAlert.msg }]}</alert>

              <textarea class="form-control resize_vert" ng-model="privatesession.details" rows="5"> 

              </textarea>
            </div>
            <div class="line line-lg"></div>
            <div class="col-sm-12">
              <button class="btn btn-md btn-success pull-right" ng-click="saveprivatesession(privatesession)">Save</button>
            </div> 
          </div>
      </div>
    </div>
  </div>
</div>
</fieldset>

<script language="javascript">
  function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
</script>