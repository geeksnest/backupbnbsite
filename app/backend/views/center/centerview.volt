<div class="bg-light lter b-b wrapper-md" id="gototop">
  <h1 class="m-n font-thin h3">{[{centerTitle}]}</h1>
  <a id="top"></a>
</div>
<div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="userid('<?php echo $username["bnb_userid"];?>')">
  <div class="col w-md bg-light dk b-r bg-auto bg-auto-left">
    <div class="wrapper b-b bg">
      <button class="btn btn-sm btn-default pull-right visible-sm visible-xs" ui-toggle-class="show" target="#email-menu"><i class="fa fa-bars"></i></button>
      <a ui-sref="dashboard" class="btn btn-sm btn-primary btn-block font-bold">Dashboard</a>
    </div>
    <div class="wrapper hidden-sm hidden-xs" id="email-menu">
      <ul class="nav nav-pills nav-stacked nav-sm">
        <li ui-sref-active="active">
          <a ui-sref="centerview.slider(slider)">
            Center Slider
          </a>
        </li>

        <li ui-sref-active="active">
          <a ui-sref="centerview.contact(contact)">
            Center Contact
          </a>
        </li>

        <li ui-sref-active="active">
          <a ui-sref="centerview.hours(hours)">
            Center Hours
          </a>
        </li>

        <li ui-sref-active="active">
          <a ui-sref="centerview.schedule(schedule)">
            Schedules & Events
          </a>
        </li>

        <li ui-sref-active="active">
          <a ui-sref="centerview.pricing(pricing)">
            Pricing
          </a>
        </li>

        <li ui-sref-active="active">
          <a ui-sref="centerview.details(details)">
            Map & Direction
          </a>
        </li>

        <li ui-sref-active="active">
          <a ui-sref="centerview.managenews(news)">
            Center News
          </a>
        </li>

        {#<li ui-sref-active="active">
          <a ui-sref="centerview.calendar(calendar)">
            Calendar
          </a>
        </li>#}

        <li ui-sref-active="active">
          <a ui-sref="centerview.socialmedia(socialmedia)">
            Social Media
          </a>
        </li>


        {#<li ui-sref-active="active">
          <a ui-sref="centerview.specialoffer(specialoffer)">
            Special Offers
          </a>
        </li>

        <li ui-sref-active="active">
          <a ui-sref="centerview.spnl(spnl)">
             ¡Se Habla Español!
          </a>
        </li>#}

        <div class="line line-dashed b-b line-lg" style="border-bottom: 1px solid #000;"></div>

        <li ui-sref-active="active">
          <a ui-sref="centerview.success(success)">
            Testimonials
          </a>
        </li>

        <li ui-sref-active="active">
          <a ui-sref="centerview.list({userid: '<?php echo $username["bnb_userid"];?>'})">
            1 on 1 Intro List
          </a>
        </li>

        <li ui-sref-active="active">
          <a ui-sref="centerview.priv({userid: '<?php echo $username["bnb_userid"];?>'})">
            1 Grp. + 1 Priv. List
          </a>
        </li>
         <li ui-sref-active="active">
          <a ui-sref="centerview.bene({userid: '<?php echo $username["bnb_userid"];?>'})">
            Beneplace List
          </a>
        </li>

        <div class="line line-dashed b-b line-lg" style="border-bottom: 1px solid #000;"></div>

        <li ui-sref-active="active">
          <a ui-sref="centerview.cell(cell)">
            Cell Phone setup
          </a>
        </li>

      </ul>
    </div>
  </div>
  <div class="col">
    <div class="fade-in-up" ui-view ></div>
  </div>
</div>
