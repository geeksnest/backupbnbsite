{{ content() }}

<script type="text/ng-template" id="beneplaceunverifiedView.html">
  <div ng-include="'/be/tpl/beneplaceunverifiedView.html'"></div>
</script>

<script type="text/ng-template" id="sessionVerify.html">
  <div ng-include="'/be/tpl/sessionVerify.html'"></div>
</script>

<script type="text/ng-template" id="referralAdd.html">
  <div ng-include="'/be/tpl/referralAdd.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <a class="btn btn-default btn-addon pull-right m-t-n-xs" ui-sref="allbeneplace({userid: '<?php echo $username["bnb_userid"];?>'})">
    <i class="glyphicon glyphicon-chevron-left"></i> Go back
  </a>
  <h1 class="m-n font-thin h3">Beneplace Unverified List</h1>
  <a id="top"></a>
</div>


<fieldset ng-disabled="isSaving">
<div class="hbox hbox-auto-xs hbox-auto-sm">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Beneplace List
              <a href ng-click="isall()"><span class="pull-right">Both viewed(v)<i class="fa fa-circle text-success"></i></span></a>
              <span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span>
              <a href ng-click="isall()"><span class="pull-right">Center viewed(C)<i class="fa fa-circle text-info"></i></span></a>
              <span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span>
              <a href ng-click="isread()"><span class="pull-right">Admin viewed(A)<i class="fa fa-circle text-primary"></i></span></a>
              <span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span>
              <a href ng-click="isunread()"><span class="pull-right">Unread(U)<i class="fa fa-circle text-warning"></i></span></a>
            </div>


              <div class="panel-body">



                <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs">
                        <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                  <table class="table table-striped b-t b-light">
                    <thead>
                      <tr>

                        <th style="width:5%">Status</th>
                        <th style="width:12.2%">Confirmation</th>
                        <th style="width:12.2%">Center</th>
                        <th style="width:12.2%">Program</th>
                        <th style="width:12.2%">Name</th>
                        <th style="width:12.2%">Email</th>
                        <th style="width:12.2%">Payment</th>
                        <th style="width:12.2%">Device</th>
                        <th style="width:5%">Q'ty</th>
                        <th style="width:12.2%">Action</th>

                      </tr>
                    </thead>
                    <tbody>

                      <tr ng-repeat="list in sessiondata">
                        <td ng-if="list.adminsessionstatus == 0 && list.centersessionstatus == 0"><span class="label bg-warning">U</span></td>
                        <td ng-if="list.adminsessionstatus == 1 && list.centersessionstatus == 0"><span class="label bg-primary">A</span></td>
                        <td ng-if="list.adminsessionstatus == 0 && list.centersessionstatus == 1"><span class="label bg-info">C</span></td>
                        <td ng-if="list.adminsessionstatus == 1 && list.centersessionstatus == 1"><span class="label bg-success">V</span></td>
                        <td>{[{list.confirmationnumber}]}</td>
                        <td>{[{list.centertitle}]}</td>
                        <td ng-if="list.program == 'intro'">1-on-1 Intro</td>
                        <td ng-if="list.program == 'group'">1 Group Class + 1-on-1 Intro</td>
                        <td>{[{list.name}]}</td>
                        <td>{[{list.email}]}</td>
                        <td>{[{list.payment}]}</td>
                        <td ng-if="list.device == 'android' || list.device == 'iphone' || list.device == 'blackberry' || list.device == 'windows-phone' || list.device == 'ipod' || list.device == 'ipad'">Mobile</td>
                        <td ng-if="list.device == 'desktop'">Desktop</td>
                        <td ng-if="list.device == '' || list.device == null">none</td>
                        <td>{[{list.quantity}]}</td>
                        <td><a href="" ng-click="viewsession(list.sessionid)"><span class="label bg-info">view</span></a></td>
                      </tr>

                    </tbody>
                  </table>
                </div>



              </div>


          </div>
        </div>

      </div>

      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
  <div class="col w-md w-auto-xs bg-light dk bg-auto b-l hide" id="aside">
  <div class="wrapper">

   <div class="padder-md">
      <!-- streamline -->
      <div class="m-b text-md">Recent Activity</div>
      <div class="streamline b-l m-b">
        <div class="sl-item">
          <div class="m-l">
            <div class="text-muted">2 minutes ago</div>
            <p><a href class="text-info">God</a> is now following you.</p>
          </div>
        </div>
        <div class="sl-item b-success b-l">
          <div class="m-l">
            <div class="text-muted">11:30</div>
            <p>Join comference</p>
          </div>
        </div>
        <div class="sl-item b-danger b-l">
          <div class="m-l">
            <div class="text-muted">10:30</div>
            <p>Call to customer <a href class="text-info">Jacob</a> and discuss the detail.</p>
          </div>
        </div>
        <div class="sl-item b-primary b-l">
          <div class="m-l">
            <div class="text-muted">Wed, 25 Mar</div>
            <p>Finished task <a href class="text-info">Testing</a>.</p>
          </div>
        </div>
        <div class="sl-item b-warning b-l">
          <div class="m-l">
            <div class="text-muted">Thu, 10 Mar</div>
            <p>Trip to the moon</p>
          </div>
        </div>
        <div class="sl-item b-info b-l">
          <div class="m-l">
            <div class="text-muted">Sat, 5 Mar</div>
            <p>Prepare for presentation</p>
          </div>
        </div>
        <div class="sl-item b-l">
          <div class="m-l">
            <div class="text-muted">Sun, 11 Feb</div>
            <p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
          </div>
        </div>
        <div class="sl-item b-l">
          <div class="m-l">
            <div class="text-muted">Thu, 17 Jan</div>
            <p>Follow up to close deal</p>
          </div>
        </div>
      </div>
      <!-- / streamline -->
    </div>

  </div>
</div>
</div>
</fieldset>
