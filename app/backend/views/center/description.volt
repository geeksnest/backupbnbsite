<div class="wrapper bg-light lter b-b">
	<div class="btn-group m-r-sm">
		<h1 class="m-n font-thin h3">Center Description</h1>
	</div>
</div>

<!-- <div class="col-md-12" ng-init="socialtablelist = true"> -->
<div class="col-md-12 wrapper-md" >
	<div class="panel panel-default">
		<form class="bs-example form-horizontal" name="formdesc" method="POST" ng-submit="savedescription(centerdesc.centerdesc)" >
			<div class="panel-heading font-bold">
			Center Description
			</div>
			<div class="panel-body">

				<alert ng-show="descalert" type="success" close="desccloseAlert($index)">Successfully Saved!</alert>

				<textarea class="form-control resize_vert" ng-model="centerdesc.centerdesc" row="6"></textarea>
				<br>
				<button type="submit" class="btn btn-success btn-md pull-right" title="Save">
					Save
				</button>
			</div> <!-- end of panel-body -->
		</form>
	</div>
</div>