<script type="text/ng-template" id="newsimagelist.html">
  <div ng-include="'/be/tpl/newsimagelist.html'"></div>
</script>
<script type="text/ng-template" id="newsimagelist2.html">
  <div ng-include="'/be/tpl/newsimagelist2.html'"></div>
</script>
<script type="text/ng-template" id="imagesrc.html">
  <div ng-include="'/be/tpl/imagesrc.html'"></div>
</script>
<script type="text/ng-template" id="newsDelete.html">
  <div ng-include="'/be/tpl/newsDelete.html'"></div>
</script>

<fieldset ng-disabled="isSaving">
<form id="centereditnewsForm" name="centereditnewsForm" ng-submit="saveNews(news)" >

  <div class="wrapper bg-light lter b-b" style="height:61px;" id="top">
      <span class="m-n font-thin h3">News List</span>
      <span class="pull-right">
        <a ui-sref="centerview.managenews(news)" class="btn btn-default" title="Back/Abort"><i class="fa fa-reply"></i></a>
        <button type="submit" class="btn btn-success" title="Save/Update"><i class="fa fa-save"></i></button>
      </span>
  </div>

  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              News Information
            </div>

              <input type="hidden"  ng-model="news.newsid">
              <input type="hidden"  ng-model="news.datecreated">
              <div class="panel-body">

                <div class="form-group">
                  <label>Title</label>
                  <input type="text" id="title" name="title" class="form-control" ng-model="news.title" ng-change="SEO(news.title)" required />
                </div>

                {#<div class="col-md-12">
                  <b>News Slugs: </b>
                  <input type="text" ng-show="editslug" id="pageslugs" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.slugs" ng-change="onslugs(news.slugs)" style="width: 200px">
                  <span ng-bind="news.slugs"></span>
                  <div class="popOver" ng-show="invalidtitle">
                      Slugs is already taken.
                      <span class="pop-triangle"></span>
                  </div>
                  <div ng-show="editslug" class="ng-hide">
                    <a class="btn btn-danger btn-xs pull-right mg-left" ng-click="cancelnewsslug(news.title)">cancel</a>
                    <a class="btn btn-primary btn-xs pull-right mg-left" ng-click="setslug(news.slugs)">ok</a>
                  </div>
                  <a class="btn btn-danger btn-xs pull-right mg-left" ng-hide="editslug" ng-click="clearslug(news.title)">clear</a>
                  <a class="btn btn-primary btn-xs pull-right mg-left" ng-hide="editslug" ng-click="editnewsslug()">edit slug</a>
                  <br><br>
                </div>#}

                <div class="form-group">
                  <label>SEO: </label>
                  <input type="text" id="newsslugs" class="form-control" ng-model="news.slugs" ng-change="SEO(news.slugs)" required />
                </div>

<!--                 <div class="col-md-12">
                  <b>Meta Tags
                  <em class="text-muted pull-right">(Optional)</em>
                  </b>
                  <input type="text" name="metatags" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.metatags">
                  <span class="help-block m-b-none">Use comma (,) for multiple meta tags. E.g. (focus, strength, peace)</span>
                  <br>
                </div> -->

                <div class="form-group" id="bodyvalidate">
                  <label>Body Content</label>
                  <a class="btn btn-default btn-sm pull-right" ng-click="imagegallery('lg')">
                    <i class="fa fa-file-image-o text"></i>
                    Image Gallery
                  </a>
                  <br><br>
                  <textarea class="ck-editor" ng-model="news.body" required></textarea>
                </div>

                <div class="form-group">
                  <label>Author</label>
                  <em class="text-muted pull-right">(Optional)</em>
                  <input type="text" class="form-control" ng-model="news.author" name="news.author">
                </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <b>Meta Properties</b>
              <em class="text-muted pull-right">(Optional)</em>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <label>Meta Title</label>
                <input type="text" name="metatitle" class="form-control" ng-model="news.metatitle">
              </div>

              <div class="form-group">
                 <label>Short Description</label>
                 <em class="text-muted pull-right">(Optional: Maximum of 350 characters.)</em>
                 <textarea class="form-control rv" rows="4" ng-model="news.description" ng-maxlength="350"></textarea>
                 <br>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Date Published
              <em class="text-danger pull-right" ng-show="formpage.date.$error.required">(Required)</em>
              <em class="text-danger pull-right" ng-show="formpage.date.$invalid">(A valid date is required)</em>
            </div>
            <div class="panel-body">
              <div class="input-group">
                <span class="input-group-btn" id="datevalidate">
                  <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy"
                    style="max-width:80%"
                    ng-model="news.date"
                    is-open="opened"
                    datepicker-options="dateOptions"
                    ng-required="true"
                    close-text="Close"
                    type="text"
                    disabled>
                  <button type="button" class="btn btn-default" ng-click="open($event)" style="width:20%"><i class="glyphicon glyphicon-calendar"></i></button>
                </span>
              </div>
              <input type="hidden" ng-model="news.datevalid">
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              News Banner
              <em class="text-muted pull-right">(Optional)</em>
            </div>
            <div class="panel-body">
              <div class="input-group m-b">
                <span class="input-group-btn">
                  <a class="btn btn-default"  ng-click="showimageList('lg', 'banner')">Select Image</a>
                </span>
                <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">
                <input type="text" name="banner" class="form-control" ng-value="news.banner = amazonpath " placeholder="{[{amazonpath}]}" ng-required='news.newslocation == "Main Site"' readonly />
              </div>
              <div class="col-sm-12">
                <br><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/newsimage/{[{news.banner}]}" width="100%" height="100%" alt="IMAGE PREVIEW" ng-show="news.banner != ''" />
              </div>
            </div>
            <div class="panel-footer">
              <span class="text-muted">A ratio of 3W x 8L is the recommended image size for better viewing in the frontend.</span>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <b>Thumbnail</b>
              <em class="text-muted pull-right">(Optional)</em>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <div class="input-group m-b">
                  <span class="input-group-btn">
                    <a class="btn btn-default"  ng-click="showimageList('lg', 'thumbnail')">Select Image</a>
                  </span>
                  <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">
                    <input type="text" name="thumbnail" class="form-control" ng-value="news.thumbnail = amazonpath2 " ng-model="news.thumbnail" placeholder="{[{amazonpath2}]}" readonly />
                </div>
                <div>
                  <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/newsimage/{[{news.thumbnail}]}" class="img-responsive" ng-show="news.thumbnail != ''">
                </div>
              </div>
            </div>
            <div class="panel-footer">
              <span class="text-muted">A ratio of 2W x 3L is the recommended image size for better viewing in the frontend.</span>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold" >
              News Status
            </div>
            <div class="panel-body">
              <div class="form-group" >
                  <label class="col-sm-3 control-label">
                    <span class="label bg-info" ng-show="news.status == 1">Online</span>
                    <span class="label bg-danger" ng-show="news.status == 0">404</span>
                  </label>

                  <label class="i-switch bg-info m-t-xs m-r" style="margin-top:1px">
                    <input type="checkbox" ng-true-value="'1'"  ng-false-value="'0'" ng-model="news.status">
                    <i></i>
                  </label>
                </div>
            </div>
          </div>

          {#<div class="panel panel-default">
            <div class="panel-heading font-bold">
              <em class="text-danger" ng-show="formpage.$invalid || formpage.$pending || invalidtitle==true">
              Please fill all required fields before submitting</em>
            </div>
            <div class="panel-body">
              <div class="form-group" >
                <a ui-sref="centerview.managenews" class="btn btn-default"> Close </a>
                <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid || formpage.$pending || invalidtitle==true" scroll-to="Scrollup">Submit</button>
              </div>
            </div>
          </div>#}
        </div>

<!--         <div class="col-sm-4">
          <div class="panel panel-primary">
              <div class="panel-heading">
                <strong>Workshop Titles</strong>
              </div>
              <div class="panel-body">
                <form name="relatedform">
                  <span ng-repeat="title in workshoptitles" ng-model="title" class="btn m-b-xs btn-sm btn-default btn-rounded btn-addon margin_right_5px" ng-click="addrelated(title)"><i class="fa fa-plus pull-right"></i>{[{title}]}</span>
                </form>
              </div>
            </div>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <strong>Related Workshop</strong>
              </div>
              <div class="panel-body">
                <form name="relatedform">
                  <span ng-repeat="title in related track by $index" ng-hide="empty" ng-model="titles" class="btn m-b-xs btn-sm btn-success btn-rounded btn-addon margin_right_5px" ng-click="removerelated(title, related)"><i class="fa fa-minus pull-right"></i>{[{title}]}</span>
                  <span ng-show="empty">Empty</span>
                </form>
              </div>
            </div>
          </div> -->

      </div>

      <!-- <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="centerview.managenews(news)" class="btn btn-default"> Close </a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid">Submit</button>
            </footer>
        </div>
      </div> -->


      <!-- <div  class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Image Gallery
              </div>

                <div class="panel-body">
                  <alert ng-repeat="alert in alertss" type="{[{alert.type }]}" close="closeAlerts($index)">{[{ alert.msg }]}</alert>
                  <div class="loader" ng-show="imageloader">
                    <div class="loadercontainer">

                      <div class="spinner">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                        <div class="rect5"></div>
                      </div>
                      Uploading your images please wait...

                    </div>

                  </div>

                  <div ng-show="imagecontent">

                    <div class="col-sml-12">
                      <div class="dragdropcenter">
                        <div ngf-drop ngf-select ng-model="files" class="drop-box"
                        ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
                        accept="image/*,application/pdf">Drop images here or click to upload</div>
                      </div>
                    </div>

                    <div class="line line-dashed b-b line-lg"></div>

                    <div class="col-sm-3" ng-repeat="data in imagelist">
                      <a href="" ng-click="deletenewsimg(data.id)" class="closebutton">&times;</a>
                      <input type="text" id="title" name="title" class="form-control" value="<?php echo $this->config->application->amazonlink; ?>/uploads/newsimage/{[{data.filename}]}" onClick="this.setSelectionRange(0, this.value.length)">
                      <div class="imagegallerystyle" style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/newsimage/{[{data.filename}]}');">
                      </div>
                    </div>

                  </div>


                </div>

          </div>
        </div>

      </div> -->

  </div>
</fieldset>
</form>
