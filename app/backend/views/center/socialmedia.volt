<div class="wrapper bg-light lter b-b">
	<div class="btn-group m-r-sm">
		<h1 class="m-n font-thin h3">Social Media</h1>
	</div>
</div>

<!-- <div class="col-md-12" ng-init="socialtablelist = true"> -->
<div class="col-md-12 wrapper-md" >
	<div class="panel panel-default">
		<div class="panel-heading font-bold">
			Social Media
		</div>

		<div class="panel-body">
			<!-- 4 fixed social links  -->
			<div class="col-sm-12">
				<alert ng-repeat="alert in socialAlert" type="{[{alert.type}]}" close="closesociallinkalert($index)">{[{alert.msg}]}</alert>
				<table class="table">
					<thead>
						<tr>
							<th style="width:10%">Status</th>
							<th style="width:5">Icon</th>
							<th style="width:65%">Link</th>
							<th style="width:20%">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="vertical-align:middle;" class="bg-light">
								<label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
									<input type="checkbox" ng-true-value="'1'" ng-false-value="'0'" ng-model="fblinkstatus" ng-click="updatesocialurlstatus(fblinkstatus, scl = 'facebook')" >
									<i></i>
								</label>
							</td>
							<td style="vertical-align:middle"><img src="/img/frontend/ico_facebook.gif"></td>
							<td style="vertical-align:middle">
								<input type="url" ng-model="facebooklink" class="form-control" ng-disabled="updateFbLink" placeholder="Please include https://" required>
							</td>
							<td style="vertical-align:middle">
								<a class="btn btn-md btn-primary" ng-hide="btnSaveFbLink" ng-click="btnEditLink(scl = 'facebook')">edit</a>
								<a class="btn btn-md btn-success" ng-show="btnSaveFbLink" ng-click="btnUpdateLink(facebooklink, scl = 'facebook')">Save</a>
								<a class="btn btn-md btn-danger" ng-show="btnSaveFbLink" ng-click="btnCancel(scl = 'facebook')">Cancel</a>
							</td>
						</tr>

						<tr>
							<td style="vertical-align:middle;" class="bg-light">
								<label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
									<input type="checkbox" ng-true-value="'1'" ng-false-value="'0'" ng-model="twlinkstatus" ng-click="updatesocialurlstatus(twlinkstatus, scl = 'twitter')" >
									<i></i>
								</label>
							</td>
							<td style="vertical-align:middle"><img src="/img/frontend/ico_twitter.gif"></td>
							<td style="vertical-align:middle">
								<input type="url" ng-model="twitterlink" class="form-control" ng-disabled="updateTwLink" placeholder="Please include https://" required>
							</td>
							<td style="vertical-align:middle">
								<a class="btn btn-md btn-primary" ng-hide="btnSaveTwLink" ng-click="btnEditLink(scl = 'twitter')">edit</a>
								<a class="btn btn-md btn-success" ng-show="btnSaveTwLink" ng-click="btnUpdateLink(twitterlink, scl = 'twitter')">Save</a>
								<a class="btn btn-md btn-danger" ng-show="btnSaveTwLink" ng-click="btnCancel(scl = 'twitter')">Cancel</a>
							</td>
						</tr>

						<tr>
							<td style="vertical-align:middle;" class="bg-light">
								<label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
									<input type="checkbox" ng-true-value="'1'" ng-false-value="'0'" ng-model="golinkstatus" ng-click="updatesocialurlstatus(golinkstatus, scl = 'google')" >
									<i></i>
								</label>
							</td>
							<td style="vertical-align:middle"><img src="/img/frontend/ico_google.gif"></td>
							<td style="vertical-align:middle">
								<input type="url" ng-model="googlelink" class="form-control" ng-disabled="updateGoLink" placeholder="Please include https://" required>
							</td>
							<td style="vertical-align:middle">
								<a class="btn btn-md btn-primary" ng-hide="btnSaveGoLink" ng-click="btnEditLink(scl = 'google')">edit</a>
								<a class="btn btn-md btn-success" ng-show="btnSaveGoLink" ng-click="btnUpdateLink(googlelink, scl = 'google')">Save</a>
								<a class="btn btn-md btn-danger" ng-show="btnSaveGoLink" ng-click="btnCancel(scl = 'google')">Cancel</a>
							</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;" class="bg-light">
								<label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
									<input type="checkbox" ng-true-value="'1'" ng-false-value="'0'" ng-model="yelinkstatus" ng-click="updatesocialurlstatus(yelinkstatus, scl = 'yelp')" >
									<i></i>
								</label>
							</td>
							<td style="vertical-align:middle"><img src="/img/frontend/sns_yepl.gif"></td>
							<td style="vertical-align:middle">
								<input type="url" ng-model="yelplink" class="form-control" ng-disabled="updateYeLink" placeholder="Please include https://" required>
							</td>
							<td style="vertical-align:middle">
								<a class="btn btn-md btn-primary" ng-hide="btnSaveYeLink" ng-click="btnEditLink(scl = 'yelp')">edit</a>
								<a class="btn btn-md btn-success" ng-show="btnSaveYeLink" ng-click="btnUpdateLink(yelplink, scl = 'yelp')">Save</a>
								<a class="btn btn-md btn-danger" ng-show="btnSaveYeLink" ng-click="btnCancel(scl = 'yelp')">Cancel</a>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="4"><label class="text-danger">** Hit the "Edit" button to update social links</label></td>
						</tr>
					</tfoot>
				</table>
			</div>

		</div> <!-- end of panel-body? -->
	</div>
</div> <!-- end of col sm 12 outer -->


                <!-- <alert ng-repeat="alert in socialalerts" type="{[{alert.type }]}" close="socialcloseAlertcloseAlert($index)">{[{ alert.msg }]}</alert>
            <button class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addsociallink()"><i class="fa fa-plus"></i>Add New Social Link</button>

            <form name="form" class="bs-example form-horizontal" ng-submit="savesociallink(link)"  ng-show="addsociallist">
            <div class="line line-dashed b-b line-lg"></div>
            <div class="col-sm-12">
              <h4>Add Social Link</h4>
              Title
              <input type="text" id="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="link.title" required="required">

              <div class="line line-dashed b-b line-lg"></div>

                Link Url
                <input type="url" id="linkurl" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="link.linkurl" placeholder="http://www." required="required">
                <span class="help-block m-b-none">(Please don't forget to include http://)</span>
              <div class="line line-dashed b-b line-lg"></div>

              Social Icon
              <div class="form-group">
                <div class="col-sm-9">
                  <input type="file" id="profile_pic" ngf-select ng-model="file" ngf-multiple="false" ngf-allow-dir="true" accept="image/*,application/pdf" onchange="readURL(this);" required>
                  <img width="100" height="100" id="blah" src="/img/default_profile_pic.jpg" alt="your image" />
                  <label for="profile_pic" class="label_profile_pic">Change Icon</label>
                </div>
              </div>

              <div class="col-sm-12">Status</div>
              <div class="line line-dashed b-b line-lg"></div>
                <div class="col-sm-12">

                  <div class="col-sm-3" ng-init="link.status = 1">
                    <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                      <input type="checkbox" ng-true-value="1" ng-model="link.status" ng-false-value="0">
                      <i></i>
                    </label>
                  </div>

                  <div class="col-sm-3">
                    <label class="col-sm-12 control-label">
                      <span class="label bg-info" ng-show="link.status == 1">Show</span>
                      <span class="label bg-danger" ng-show="link.status == 0">Hide</span>
                    </label>
                  </div>

                </div>

            </div>

            <footer class="panel-footer text-right bg-light lter">
              <button type="button" class="btn btn-default" ng-click="canceladdsociallist()">Cancel</button>
              <button type="submit" id="submit" class="btn btn-success" ng-disabled="form.$invalid">Save</button>
            </footer>
            </form> --> <!-- end of adding links -->

            <!-- <div class="table-responsive" ng-show="socialtablelist">
              <table class="table table-striped b-t b-light">
                <thead>
                  <tr>

                    <th style="width:20%">Title</th>
                    <th style="width:30%">Status</th>
                    <th style="width:30%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="mem in sociallinksdata">

                    <td>{[{ mem.title }]}</td>
                    <td  ng-if="mem.status == 1">
                      <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>
                      <div class="checkstatuscontent">
                        <label class="i-switch bg-info m-t-xs m-r">
                          <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.linkid,searchtext,mem.linkid)">
                          <i></i>
                        </label>

                      </div>
                      <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.linkid"><i class="fa fa-check"></i></spand></div>
                    </td>
                    <td  ng-if="mem.status == 0">
                      <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Hidden</span></div>
                      <div class="checkstatuscontent">
                        <label class="i-switch bg-info m-t-xs m-r">
                          <input type="checkbox" ng-click="setstatus(mem.status,mem.linkid,searchtext,mem.linkid)">
                          <i></i>
                        </label>

                      </div>
                      <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.linkid"><i class="fa fa-check"></i></spand></div>
                    </td>
                  </td>
                  <td>
                    <a href="" ng-click="editlink(mem.linkid)"><span class="label bg-warning" >Edit</span></a>
                    <a href="" ng-click="deletelink(mem.linkid)"> <span class="label bg-danger">Delete</span></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div> --> <!-- end of social list -->

          <!-- <form class="bs-example form-horizontal" name="formedit" ng-submit="updatesociallinks(linkdata)" ng-show="editsociallist">
            <div class="col-sm-12">
              <div class="line line-dashed b-b line-lg"></div>
              <h4>Edit Social Link</h4>
              Title
              <input type="hidden" ng-model="linkdata.linkid">
              <input type="text" id="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="linkdata.title" required="required">

              <div class="line line-dashed b-b line-lg"></div>

                Link Url
                <input type="url" id="linkurl"  class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="linkdata.linkurl" placeholder="http://www." required="required">
                <span class="help-block m-b-none">(Please don't forget to include http://)</span>

              <div class="line line-dashed b-b line-lg"></div>

              Social Icon
              <div class="form-group">
                <div class="col-sm-9">
                  <input type="file" ngf-select ng-model="files" ngf-multiple="false" ngf-allow-dir="true" accept="image/*,application/pdf" required>
                </div>
              </div>

              <div class="col-sm-12">Status</div>
              <div class="line line-dashed b-b line-lg"></div>
                <div class="col-sm-12">

                  <div class="col-sm-3">
                    <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                      <input type="checkbox" ng-true-value="'1'" ng-model="linkdata.status" ng-false-value="'0'">
                      <i></i>
                    </label>
                  </div>
                  <div class="col-sm-3">
                    <label class="col-sm-12 control-label">
                      <span class="label bg-info" ng-show="linkdata.status == 1">Show</span>
                      <span class="label bg-danger" ng-show="linkdata.status == 0">Hide</span>
                    </label>
                  </div>

                </div>

            </div>

            <footer class="panel-footer text-right bg-light lter">
              <button type="button" class="btn btn-default" ng-click="canceladdsociallist()">Cancel</button>
              <button type="submit" id="submit"  class="btn btn-success" ng-disabled="formedit.$invalid">Save</button>
            </footer>
            </form>  --><!-- end of editing links -->
