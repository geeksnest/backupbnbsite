<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>
<style>
.centerimages_thumbnails {
	background-size:cover; 
	height:150px; 
	background-position:center center; 
	background-repeat:no-repeat;
	border:2px solid #999;
	box-shadow:0 2px 5px #555;
}

.centerimage_delete {
	position:relative !important; 
	top:-10px !important; 
	left:-20px !important; 
	width:20px !important; 
	height:20px !important;
}

</style>
<div class="wrapper bg-light lter b-b" style="height:61px;">
	<div class="btn-group m-r-sm">
		<h1 class="m-n font-thin h3">Center Gallery</h1>
		<a id="top"></a>
	</div>
</div>

<div class="container-fluid">
	<div class="row wrapper-md">
		<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index, type='default')">{[{ alert.msg }]}</alert>
		<alert ng-repeat="alert in imagealerts" type="{[{alert.type }]}" close="closeAlert($index,type='image')">{[{ alert.msg }]}</alert>
		<div col="col-sm-12">
			<div class=" panel panel-success">
				<div class="panel-body">
					<div class="col-sm-12">
						<div class="loadercontainer" ng-show="imageloader">
							<div class="spinner">
								<div class="rect1"></div>
								<div class="rect2"></div>
								<div class="rect3"></div>
								<div class="rect4"></div>
								<div class="rect5"></div>
							</div>
							Uploading your images please wait...
						</div>

						<div class="dragdropcenter" ng-show="imagecontent">
							<div ngf-drop ngf-select ng-model="files" class="drop-box" 
							ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
							accept="image/*">Drop images here or click to upload </div>
						</div>
					</div>
					<div class="line line-dashed b-b line-lg"></div>
						<form name="orderingform" class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="saveordering(img, ord)">
							<div class="col-sm-4" ng-repeat="image in centerimages track by $index" style="margin:2% 0 0 0">
								<button class=" btn btn-rounded btn btn-icon btn-danger" ng-click="delete(image.id)" style="margin:10px 5px -50px; 5px"><i class="fa fa-trash-o"></i></button>
								<div style="background-image:url('<?php echo $this->config->application->amazonlink . '/uploads/center/'; ?>{[{image.centerid}]}/{[{image.imagename}]}');" class="centerimages_thumbnails">
									<label class="checkbox i-checks m-l-md m-b-md" class="centerimage_delete" style="	position:relative !important; 
									top:-10px !important; 
									left:-20px !important; 
									width:20px !important; 
									height:20px !important;">
									<!-- <input type="checkbox" ng-model="oneAtATime"><i></i> -->
									</label>
									<input type="hidden" ng-model="img[$index]" ng-init="img[$index] = image.id ">
									<input type="textbox" ng-model="ord[$index]" class="form-control ordering" title="{[{image.id}]}" pattern="\d{1,2}" maxlength="2" required>
								</div>
							</div>
							<div class="col-sm-12 wrapper-md">
								<button class="btn btn-info pull-right" ng-disabled="orderingform.$invalid">
									Save Ordering
								</button>
							</div>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
.ordering {max-width:40px; position:absolute; bottom:0; right:17px; text-align:center;}
</style>
