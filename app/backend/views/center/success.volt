<script type="text/ng-template" id="viewStory.html">
  <div ng-include="'/be/tpl/manageStory.html'"></div>
</script>
<script type="text/ng-template" id="deleteStory.html">
  <div ng-include="'/be/tpl/deleteStory.html'"></div>
</script>
<div class="wrapper bg-light lter b-b" style="height:61px;">
	<div class="btn-group m-r-sm">
		<h1 class="m-n font-thin h3">Success Stories</h1>
		<a id="top"></a>
	</div>
</div>

<div class="container-fluid">
	<div class="row wrapper-md">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading font-bold">
					Stories List
				</div>
				<div class="panel-body">
					<div class="row wrapper-md">
						<div class="col-sm-5">
							<div class="input-group">
								<span class="input-group-btn">
									<button class="btn btn-sm btn-default" type="button" ng-click="clearsearch()">Clear</button>
								</span>
								<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
								<span class="input-group-btn">
									<button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
								</span>
							</div>
						</div>
					</div>
					<div class="row wrapper-md">
						<div class="table-responsive">
							<table class="table table-striped b-t b-light">
								<thead>
									<tr>
										<th></th>
										<th>Status</th>
										<th>Author</th>
										<th>Subject</th>
										<th>State</th>
										<th>Date Submitted</th>
										<th>Date Published</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="story in stories.data">
										<td>{[{ CurrentPage * 10 - 10 + 1 + $index }]}</td>
										<td>
											<span class="label bg-success" ng-show="story.status == 0">New</span>
											<span class="label bg-primary" ng-show="story.status == 2">Reviewed</span>
											<span class="label bg-info" ng-show="story.status == 1 && story.spotlight == 0">Published</span>
											<span class="label bg-info" ng-show="story.status == 1 && story.spotlight == 1">Published <i class="icon-star"></i></span>
										</td>
										<td>{[{story.author}]}</td>
										<td>{[{story.subject}]}</td>
										<td>{[{story.centerstate}]}</td>
										<td>{[{story.date_submitted}]}</td>
										<td>{[{story.date_published}]}</td>
										<td>
											<a class="label bg-info" ng-click="viewStory(story.id)">Manage</a>
											<a class="label bg-danger" ng-click="deleteStory(story.id)">Delete</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<footer class="panel-footer text-center bg-light lter">
          <pagination total-items="TotalItems" ng-model="CurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(CurrentPage)"></pagination>
        </footer>
			</div>
		</div>
	</div>
</div>