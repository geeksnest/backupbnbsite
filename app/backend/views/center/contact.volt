<div class="wrapper bg-light lter b-b">
	<div class="btn-group m-r-sm">
		<h1 class="m-n font-thin h3">Contact/Email</h1>
	</div>
</div>

<!-- <div class="col-md-12" ng-init="socialtablelist = true"> -->
<div class="col-md-12 wrapper-md" >
	<div class="panel panel-default">
		<div class="panel-heading font-bold">
			Center Contact
		</div>

		<div class="panel-body">
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading font-bold">
						Center Tel Number
					</div>
					<div class="col-sm-12">
						<alert ng-repeat="alert in phonealerts" type="{[{alert.type }]}" close="phonecloseAlert($index)">{[{ alert.msg }]}</alert>
					</div>

					<div class="panel-body">
						<form class="bs-example form-horizontal" name="formtel" ng-submit="updatephone(phone)" >
							<input id="phone" placeholder="(555) 555-5555" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" required="required"  minlength="14"  ng-model="phone.phonenumber">

							<footer class="panel-footer text-right bg-light lter">
								<button type="submit" id="submit" class="btn btn-success" ng-disabled="formtel.$invalid">Save</button>
							</footer>
						</form>
					</div> <!-- end of panel-body -->
				</div>
			</div> <!-- end of col sm 6 -->

			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading font-bold">
						Center Email  <em>(example@bodynbrain.com)</em>
					</div>
					<div class="col-sm-12">
						<alert ng-repeat="alert in emailalerts" type="{[{alert.type }]}" close="emailcloseAlert($index)">{[{ alert.msg }]}</alert>
					</div>

					<div class="panel-body">
						<form class="bs-example form-horizontal" name="formemail" ng-submit="updateemail(email)" >
							<input placeholder="example@bodynbrain.com" type="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" required="required"  ng-model="email.email">

							<footer class="panel-footer text-right bg-light lter">
								<button type="submit" id="submit" class="btn btn-success" ng-disabled="formemail.$invalid">Save</button>
							</footer>
						</form>
					</div> <!-- end of panel-body -->
				</div>
			</div> <!-- end of col sm 6 -->
		</div>
	</div>
</div>

<script type="text/javascript">

  var options =  {onKeyPress: function(cep, e, field, options){

    $('#phone').mask('(000) 000-0000');
  }};
  $( document ).ready(function() {
    $('#phone').mask('(000) 000-0000', options);
  });


</script>