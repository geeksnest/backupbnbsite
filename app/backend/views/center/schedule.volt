<script type="text/ng-template" id="scheduleedit.html">
  <div ng-include="'/be/tpl/scheduleedit.html'"></div>
</script>

<script type="text/ng-template" id="scheduleAdd.html">
  <div ng-include="'/be/tpl/scheduleAdd.html'"></div>
</script>

<script type="text/ng-template" id="scheduleDelete.html">
  <div ng-include="'/be/tpl/scheduleDelete.html'"></div>
</script>

<script type="text/ng-template" id="eventDelete.html">
  <div ng-include="'/be/tpl/eventDelete.html'"></div>
</script>

  <!-- header -->
  <div class="wrapper bg-light lter b-b" style="height:61px;">
    <div class="btn-group m-r-sm">
      <h1 class="m-n font-thin h3">Schedules & Events</h1>
    </div>
  </div>


<fieldset ng-disabled="isSaving">


  <div class="wrapper-md">

    <tabset class="tab-container">
  		<tab ng-click="changetab('schedule')">
  			<tab-heading><i class="fa fa-calendar-check-o"></i> Schedule</tab-heading>
          <div class="row">
            <div class="col-sm-12"><alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert></div>
            <div class="col-sm-12" style="margin-bottom: 10px">
              <a ng-click="addnewschedule()" class="btn btn-sm btn-success w-xm font-bold">Add New Class Schedule</a>
            </div>
            <div class="col-sm-12">
              <div class="panel panel-primary">

                <div class="panel-heading font-bold">
                  Schedules
                </div>

                <div class="panel-body">
                  <div class="table-responsive">
                      <table class="table table-striped b-t b-light">
                          <thead class="tdborderbots">
                              <tr class="tdborderbot">

                                  <th style="width:12.2%" class="tdborderbots">Time</th>
                                  <th style="width:12.2%" class="tdborderbots">Monday</th>
                                  <th style="width:12.2%" class="tdborderbots">Tuesday</th>
                                  <th style="width:12.2%" class="tdborderbots">Wednesday</th>
                                  <th style="width:12.2%" class="tdborderbots">Thursday</th>
                                  <th style="width:12.2%" class="tdborderbots">Friday</th>
                                  <th style="width:12.2%" class="tdborderbots">Saturday</th>
                                  <th style="width:12.2%" class="tdborderbots">Sunday</th>
                                  <th style="width:12.2%" class="tdborderbots">Action</th>

                              </tr>
                          </thead>
                          <tbody>

                              <tr ng-repeat="list in schedlelist">
                                  <td class="schedtd tdborder">
                                     {[{list.starttime}]} {[{list.starttimeformat}]} ~ {[{list.endtime}]} {[{list.endtimeformat}]}
                                  </td>
                                  <td class="schedtd tdborder">
                                    <span>{[{list.mo}]}</span>
                                  </td>
                                  <td class="schedtd tdborder">
                                    <span>{[{list.tu}]}</span>
                                  </td>
                                  <td class="schedtd tdborder">
                                    <span>{[{list.we}]}</span>
                                  </td>
                                  <td class="schedtd tdborder">
                                    <span>{[{list.th}]}</span>
                                  </td>
                                  <td class="schedtd tdborder">
                                    <span>{[{list.fr}]}</span>
                                  </td>
                                  <td class="schedtd tdborder">
                                    <span>{[{list.sa}]}</span>
                                  </td>
                                  <td class="schedtd tdborder">
                                    <span>{[{list.su}]}</span>
                                  </td>
                                  <td class="schedtd tdborder">
                                    <a href="" ng-click="editsession(list.classid)"><span class="label bg-warning" >Edit</span></a>
                                    <a href="" ng-click="delete(list.classid)"> <span class="label bg-danger">Delete</span></a>
                                  </td>
                              </tr>

                          </tbody>
                      </table>
                   </div>
                </div> <!-- END OF panel Body-->
            </div>
          </div>
        </div> <!-- end of row -->

      </tab>

      <tab ng-click="changetab('event')">
  			<tab-heading><i class="fa fa-tasks"></i> Events</tab-heading>
        <div class="row">
          <fieldset ng-disabled="isSaving" ng-if="showaddevent != true && showeditevent != true">
            <div class="wrapper-md">
              <alert ng-repeat="alert in alerts2" type="{[{alert.type }]}" close="closeAlert2($index)">{[{ alert.msg }]}</alert>

              <div class="row">
                <div class="col-sm-12" style="margin-bottom:10px;">
                  <a ng-click="showaddeventf()" class="btn btn-sm btn-success w-xm font-bold">Add New Event</a>
                </div>
                <div class="col-sm-12">
                  <div class="panel panel-primary">

                    <div class="panel-heading font-bold">
                      Events List
                    </div>

                    <div class="panel-body">


                      <div class="row wrapper">
                        <div class="col-sm-5 m-b-xs">
                          <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                              <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="table-responsive">
                        <table class="table table-striped b-t b-light">
                          <thead>
                            <tr>

                              <th style="width:20%">Title</th>
                              <th style="width:15%">Date</th>
                              <th style="width:15%">From</th>
                              <th style="width:15%">To</th>
                              <th style="width:25%">Status</th>
                              <th style="width:30%">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="mem in data.data">

                              <td>{[{ mem.activitytitle }]}</td>
                              <td>{[{ mem.activitydate }]}</td>
                              <td>{[{ mem.activitytimefrom}]} {[{ mem.activitytimefromformat}]}</td>
                              <td>{[{ mem.activitytimeto}]} {[{ mem.activitytimetoformat}]}</td>
                              <td  ng-if="mem.status == 1">
                                <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Active</span></div>
                                <div class="checkstatuscontent">
                                  <label class="i-switch bg-info m-t-xs m-r">
                                    <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.activityid,searchtext,mem.activityid)">
                                    <i></i>
                                  </label>

                                </div>
                                <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.activityid"><i class="fa fa-check"></i></spand></div>
                              </td>
                              <td  ng-if="mem.status == 0">
                                <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Deactivated</span></div>
                                <div class="checkstatuscontent">
                                  <label class="i-switch bg-info m-t-xs m-r">
                                    <input type="checkbox" ng-click="setstatus(mem.status,mem.activityid,searchtext,mem.activityid)">
                                    <i></i>
                                  </label>

                                </div>
                                <div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.activityid"><i class="fa fa-check"></i></spand></div>
                              </td>
                            </td>
                            <td>
                              <a href="" ng-click="editevent(mem.activityid)"><span class="label bg-warning" >Edit</span></a>
                              <a href="" ng-click="deleteevent(mem.activityid)"> <span class="label bg-danger">Delete</span></a>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>


                  </div> <!-- END OF panel Body-->


                </div>
              </div>
            </div> <!-- end of row -->


            <div class="row">
              <div class="panel-body">
                <footer class="panel-footer text-center bg-light lter">
                  <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                </footer>
              </div>
            </div>


          </div>
          </fieldset>

          <form class="bs-example form-horizontal" name="formcalendar" ng-submit="saveEvent(calendar)">
           <fieldset ng-disabled="isSaving" class="fade-in-up" ng-if="showaddevent">
            <div class="wrapper-md">
              <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading font-bold">
                        Add new Event
                      </div>
                      <div class="panel-body">

                        Title
                        <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="calendar.calendartitle" required="required">

                        <div class="line line-dashed b-b line-lg"></div>

                        Date
                        <div class="input-group w-md">
                          <span class="input-group-btn">
                            <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="calendar.calendardate" is-open="datePicker.isOpen" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                            <button type="button" class="btn btn-default" ng-click="openDatePicker($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                          </span>
                        </div>

                        <div class="line line-dashed b-b line-lg"></div>

                        <div class="col-lg-3">
                        From <em class="text-muted">(format 7:00AM)</em>
                        <br>
                        <select class="inputBox" ng-model="calendar.timefrom"
                         ng-options="mem.time as mem.time for mem in timelist" required>
                        </select>

                        <select class="inputBox" ng-model="calendar.timefromformat" required>
                        <option value="AM">AM</option>
                        <option value="PM">PM</option>
                        </select>
                        </div>
                        <div class="col-lg-3">
                        To  <em class="text-muted">(format 12:30PM)</em>
                        <br>
                        <select class="inputBox" ng-model="calendar.timeto"
                         ng-options="mem.time as mem.time for mem in timelist" required>
                        </select>

                        <select class="inputBox" ng-model="calendar.timetoformat" required>
                        <option value="AM">AM</option>
                        <option value="PM">PM</option>
                        </select>
                        </div>
                        </div>

                        <div class="line line-dashed b-b line-lg"></div>


                        <div class="col-sm-12">
                        Description
                        <textarea  class="replyckEditor" rows="6" placeholder="Type your message" ng-model="calendar.description" required>

                        </textarea>

                        </div>

                        <div class="line line-dashed b-b line-lg"></div>

                        <div class="col-sm-12">Status</div>
                        <div class="line line-dashed b-b line-lg"></div>
                        <div class="col-sm-3">

                          <div class="col-sm-3">
                            <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                              <input type="checkbox" ng-true-value="1" ng-model="calendar.status" ng-false-value="0">
                              <i></i>
                            </label>
                          </div>

                          <div class="col-sm-3">
                            <label class="col-sm-3 control-label">
                              <span class="label bg-info" ng-show="calendar.status == 1">Show</span>
                              <span class="label bg-danger" ng-show="calendar.status == 0">Hide</span>
                            </label>
                          </div>

                        </div>

                        <footer class="panel-footer text-right bg-light lter">
                          <a class="btn btn-default" ng-click="defualt()"> Cancel </a>
                          <button type="submit" class="btn btn-success" ng-disabled="formcalendar.$invalid">Submit</button>
                        </footer>

                      </div> <!-- end of panel body -->
                  </div> <!-- end of panel panel-primary -->
                </div> <!-- end of col-sm-12 -->

              </div> <!-- end of row -->


          </fieldset>
          </form>


          <form class="bs-example form-horizontal" name="formupdatecalendar" ng-submit="updateEvent(calendar)">
           <fieldset ng-disabled="isSaving" class="fade-in-up" ng-if="showeditevent">
            <div class="wrapper-md">
              <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                      <div class="panel-heading font-bold">
                        Edit Event
                      </div>
                      <div class="panel-body">
                        <input type="hidden" ng-model="calendar.activityid">
                        Title
                        <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="calendar.activitytitle" required="required">

                        <div class="line line-dashed b-b line-lg"></div>

                        Date
                        <div class="input-group w-md">
                          <span class="input-group-btn">
                            <input id="date" name="date" class="form-control" datepicker-popup="yyyy-MM-dd" ng-model="calendar.activitydate" is-open="datePicker.isOpen" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                            <button type="button" class="btn btn-default" ng-click="openDatePicker($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                          </span>
                        </div>


                        <div class="line line-dashed b-b line-lg"></div>

                        <div class="col-lg-3">
                        From <em class="text-muted">(format 7:00AM)</em>
                        <br>
                        <select class="inputBox" ng-model="calendar.activitytimefrom"
                         ng-options="mem.time as mem.time for mem in timelist " required>
                        </select>

                        <select class="inputBox" ng-model="calendar.activitytimefromformat" required>
                        <option value="AM">AM</option>
                        <option value="PM">PM</option>
                        </select>
                        </div>
                        <div class="col-lg-3">
                        To  <em class="text-muted">(format 12:30PM)</em>
                        <br>
                        <select class="inputBox" ng-model="calendar.activitytimeto"
                         ng-options="mem.time as mem.time for mem in timelist" required>
                        </select>

                        <select class="inputBox" ng-model="calendar.activitytimetoformat" required>
                        <option value="AM">AM</option>
                        <option value="PM">PM</option>
                        </select>
                        </div>

                        <div class="line line-dashed b-b line-lg"></div>

                        <div class="col-sm-12">
                          Description
                          <textarea  class="form-control replyckEditor" rows="6" placeholder="Type your message" ng-model="calendar.description" required>

                          </textarea>

                        </div>

                        <div class="line line-dashed b-b line-lg"></div>

                        <div class="col-sm-12">Status</div>
                        <div class="line line-dashed b-b line-lg"></div>
                        <div class="col-sm-3">

                          <div class="col-sm-3">
                            <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                              <input type="checkbox" ng-true-value="1" ng-model="calendar.status" ng-false-value="0">
                              <i></i>
                            </label>
                          </div>

                          <div class="col-sm-3">
                            <label class="col-sm-3 control-label">
                              <span class="label bg-info" ng-show="calendar.status == 1">Show</span>
                              <span class="label bg-danger" ng-show="calendar.status == 0">Hide</span>
                            </label>
                          </div>

                        </div>

                        <footer class="panel-footer text-right bg-light lter">
                          <a class="btn btn-default" ng-click="defualt()"> Cancel </a>
                          <button type="submit" class="btn btn-success" ng-disabled="formupdatecalendar.$invalid">Submit</button>
                        </footer>

                      </div><!-- end of panel body -->
                    </div> <!-- end of panel primary -->
                  </div> <!-- end of col-sm-12 -->
                </div> <!-- end of row -->
              </div><!-- end of wrapper-md -->

          </fieldset>
          </form>
        </div>
  </tab>
</tabset>

</div>
</fieldset>



<script language="javascript">
  function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
</script>
