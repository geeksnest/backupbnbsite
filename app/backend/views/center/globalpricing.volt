{{ content() }}



<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Starters Package Pricing</h1>
    <a id="top"></a>
</div>


<fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

        <div class="row">

            <div class="col-sm-6">

                <div class="panel panel-default">
                    <div class="panel-heading font-bold">
                        1-on-1 Intro Session
                    </div>
                    <form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="fromintro" ng-submit="saveintro(price)">
                    <div class="panel-body">
                        

                        
                        Discounted Rate
                        <div class="input-group m-b">
                        <span class="input-group-addon">$</span>
                        <input type="text" id="introonlinerate" name="introonlinerate" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="price.introonlinerate" required="required" onkeypress="return isNumberKey(event)">
                        </div>

                        <div class="line line-dashed b-b line-lg"></div>

                        Original Rate
                        <div class="input-group m-b">
                        <span class="input-group-addon">$</span>
                        <input type="text" id="introofflinerate" name="introofflinerate" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="price.introofflinerate" required="required" onkeypress="return isNumberKey(event)">
                        </div>


                        <footer class="panel-footer text-right bg-light lter">
                          <alert ng-repeat="alert in introalerts" type="{[{alert.type }]}" close="closeintroAlert($index)">{[{ alert.msg }]}</alert>
                          <button type="submit" class="btn btn-success" ng-click="updatepricing(regfee,regclass,sessionfee)">Save</button>
                        </footer>

                    </div>
                    </form>

                </div>


            </div> <!-- end of 1 on 1 -->


            <div class="col-sm-6">

                <div class="panel panel-default">
                    <div class="panel-heading font-bold">
                        1 Group Class + 1-on-1 Intro Session
                    </div>
                    <form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="fromgroup" ng-submit="savegroup(price)">
                    <div class="panel-body">
                        

                        
                        Discounted Rate
                        <div class="input-group m-b">
                        <span class="input-group-addon">$</span>
                        <input type="text" id="grouponlinerate" name="grouponlinerate" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="price.grouponlinerate" required="required" onkeypress="return isNumberKey(event)">
                        </div>

                        <div class="line line-dashed b-b line-lg"></div>

                        Original Rate
                        <div class="input-group m-b">
                        <span class="input-group-addon">$</span>
                        <input type="text" id="groupofflinerate" name="groupofflinerate" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="price.groupofflinerate" required="required" onkeypress="return isNumberKey(event)">
                        </div>

                        <footer class="panel-footer text-right bg-light lter">
                          <alert ng-repeat="alert in groupalerts" type="{[{alert.type }]}" close="closegroupAlert($index)">{[{ alert.msg }]}</alert>
                          <button type="submit" class="btn btn-success" ng-click="updatepricing(regfee,regclass,sessionfee)">Save</button>
                        </footer>

                    </div>


                </div>


            </div> <!-- end of 1 group -->




      </div> <!-- end of row -->



  </div>
</fieldset>

<script language="javascript">
  function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
</script>