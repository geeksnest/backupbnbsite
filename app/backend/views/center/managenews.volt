<script type="text/ng-template" id="newsDelete.html">
  <div ng-include="'/be/tpl/newsDelete.html'"></div>
</script>

<script type="text/ng-template" id="newsCenterDelete.html">
  <div ng-include="'/be/tpl/newsCenterDelete.html'"></div>
</script>

<script type="text/ng-template" id="centernewsEdit.html">
  <div ng-include="'/be/tpl/centernewsEdit.html'"></div>
</script>

  <!-- header -->
  <div class="wrapper bg-light lter b-b" style="height:61px;">
    <div class="btn-group m-r-sm">
      <h1 class="m-n font-thin h3">News List</h1>
      <a id="top"></a>
    </div>
    <div class="btn-group m-r-sm">
      <a ui-sref="centerview.addnews(news)" class="btn btn-sm btn-success w-xm font-bold">Add New News</a>
    </div>
  </div>

  <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="formpage">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">News List</div>

              <div class="panel-body">

                <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs">
                        <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Title</th>
                                <th>Author</th>
                                <th>Date Created</th>
                                <th>Date Published</th>
                                <th>News Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="mem in data.data">
                                <td>{[{ bigCurrentPage * 10 - 10 + 1 + $index }]}</td>
                                <td title="{[{mem.title}]}">{[{ mem.title | limitTo: 20 }]}{[{mem.title.length > 20 ? '...' : ''}]}</td>
                                <td>{[{ mem.author }]}</td>
                                <td>{[{ mem.datecreated }]}</td>
                                <td>{[{ mem.date }]}</td>
                                <td  ng-if="mem.status == 1">
                                <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Online</span></div>
                                <div class="checkstatuscontent">
                                  <label class="i-switch bg-info m-t-xs m-r">
                                    <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.newsid,searchtext,mem.newslocation,mem.newsslugs)">
                                    <i></i>
                                  </label>
                                </div>
                                {#<div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.newsslugs"><i class="fa fa-check"></i></spand></div>#}
                                </td>
                                <td  ng-if="mem.status == 0">
                                <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">404</span></div>
                                <div class="checkstatuscontent">
                                  <label class="i-switch bg-info m-t-xs m-r">
                                    <input type="checkbox" ng-click="setstatus(mem.status,mem.newsid,searchtext,mem.newslocation,mem.newsslugs)">
                                    <i></i>
                                  </label>

                                </div>
                                 {#<div class="checkcontent"><spand class="fade-in-out" ng-show="currentstatusshow == mem.newsslugs"><i class="fa fa-check"></i></spand></div>#}
                                </td>
                                </td>
                                <td>
                                    <a href="" ng-click="editnewscenter(mem.newsid)">
                                      <span class="btn btn-sm btn-default" title="Edit">
                                        <i class="glyphicon glyphicon-edit text-info"></i>
                                      </span>
                                    </a>
                                    <a href="" ng-click="deletenewscenter(mem.newsid)">
                                      <span class="btn btn-sm btn-default" title="Delete">
                                        <i class="glyphicon glyphicon-trash text-danger"></i>
                                      </span>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>



              </div>


          </div>
        </div>

      </div>

      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
</fieldset>
</form>
