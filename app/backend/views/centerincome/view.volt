<div class="bg-info lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Center Income</h1>
  <a id="top"></a>
</div>

<div class=" wrapper-md">
	<tabset class="tab-container">
    <tab>
      <tab-heading class="font-bold">CHART</tab-heading>
        <div class="panel panel-info">
          <div class="panel-heading font-bold">Daily Chart</div>
          <div class="panel-body">
            <div class="col-sm-2">
              From
              <select class="form-control" ng-model="filterperday.fromyear" ng-init="filterperday.fromyear = year10daysbefore " ng-change="dualdatevalidationperday(filterperday.frommonth,filterperday.fromyear,filterperday.fromday, filterperday.tomonth,filterperday.toyear,filterperday.today)" ng-options="year for year in dateyear" required>
              </select>
            </div>
            <div class="col-sm-1">
              &nbsp;
              <select class="form-control" ng-model="filterperday.frommonth" ng-init="filterperday.frommonth = month10daysbefore " ng-change="dualdatevalidationperday(filterperday.frommonth,filterperday.fromyear,filterperday.fromday, filterperday.tomonth,filterperday.toyear,filterperday.today)" ng-options="month.value as month.name for month in datemonth" required>
              </select>
            </div>
            <div class="col-sm-1">
              &nbsp;
              <select class="form-control" ng-model="filterperday.fromday" ng-init="filterperday.fromday = day10daysbefore" ng-change="dualdatevalidationperday(filterperday.frommonth,filterperday.fromyear,filterperday.fromday, filterperday.tomonth,filterperday.toyear,filterperday.today)" ng-options="day for day in dateday | limitTo: dayOfMonth" required>
              </select>
            </div>
            <div class="col-sm-2">
              To
              <select class="form-control" ng-model="filterperday.toyear" ng-init="filterperday.toyear = yearNow" ng-change="dualdatevalidationperday(filterperday.frommonth,filterperday.fromyear,filterperday.fromday, filterperday.tomonth,filterperday.toyear,filterperday.today)" ng-options="year for year in dateyear" required>
              </select>
            </div>
            <div class="col-sm-1">
              &nbsp;
              <select class="form-control" ng-model="filterperday.tomonth" ng-init="filterperday.tomonth = monthNow " ng-change="dualdatevalidationperday(filterperday.frommonth,filterperday.fromyear,filterperday.fromday, filterperday.tomonth,filterperday.toyear,filterperday.today)" ng-options="month.value as month.name for month in datemonth.slice(sliceperday) " required>
              </select>
            </div>
            <div class="col-sm-1">
              &nbsp;
              <select class="form-control" ng-model="filterperday.today" ng-init="filterperday.today = dayNow " ng-change="dualdatevalidationperday(filterperday.frommonth,filterperday.fromyear,filterperday.fromday, filterperday.tomonth,filterperday.toyear,filterperday.today)" ng-options="day for day in dateday | limitTo: dayOfMonthTo" required>
              </select>
            </div>
            <div class="col-sm-1">
              &nbsp;
              <button class="btn btn-sm btn-info form-control" ng-click="filterchartperday(filterperday)">Filter</button>
            </div>
            <div class="col-sm-1">
              &nbsp;
              <button class="btn btn-sm btn-default form-control" ng-click="nofilterchartperday()">Latest</button>
            </div>

            <div class="col-sm-12">
            <highchart id="chart1" config="chartConfig_perday" style="width:95%;"></highchart>
            </div>

          </div>
        </div>
        <div class="panel panel-info">
          <div class="panel-heading font-bold">Monthly Chart</div>
          <div class="panel-body">
            <div class="col-sm-2">
              From
              <select class="form-control" ng-model="filterpermonth.fromyear" ng-change="dualdatevalidationpermonth(filterpermonth.frommonth, filterpermonth.fromyear, filterpermonth.tomonth, filterpermonth.toyear)" ng-options="year for year in dateyear" required>
              </select>
            </div>
            <div class="col-sm-1">
              &nbsp;
              <select class="form-control" ng-model="filterpermonth.frommonth" ng-change="dualdatevalidationpermonth(filterpermonth.frommonth, filterpermonth.fromyear, filterpermonth.tomonth, filterpermonth.toyear)" ng-options="month.value as month.name for month in datemonth" required>
              </select>
            </div>
            <div class="col-sm-2">
              To
              <select class="form-control" ng-model="filterpermonth.toyear" ng-init="filterpermonth.toyear = yearNow" ng-change="dualdatevalidationpermonth(filterpermonth.frommonth, filterpermonth.fromyear, filterpermonth.tomonth, filterpermonth.toyear)" ng-options="year for year in dateyear" required>
              </select>
            </div>
            <div class="col-sm-1">
              &nbsp;
              <select class="form-control" ng-model="filterpermonth.tomonth" ng-init="filterpermonth.tomonth = monthNow " ng-change="dualdatevalidationpermonth(filterpermonth.frommonth, filterpermonth.fromyear, filterpermonth.tomonth, filterpermonth.toyear)" ng-options="month.value as month.name for month in datemonth.slice(slicepermonth) " required>
              </select>
            </div>
            <div class="col-sm-1">
              &nbsp;
              <button class="btn btn-sm btn-info form-control" ng-click="filterchartpermonth(filterpermonth)">Filter</button>
            </div>

            <div class="col-sm-1">
              &nbsp;
              <button class="btn btn-sm btn-default form-control" ng-click="nofilterchartpermonth()">Latest</button>
            </div>

            <div class="col-sm-12">
            <highchart id="chart2" config="chartConfig_permonth" style="width:95%"></highchart>
            </div>
          </div>
        </div>
    </tab>
    <tab active="tab1" ng-click="tab1true()">
      <tab-heading class="font-bold">
        1 on 1 Intro Session
      </tab-heading>
      <form name="tab1form">
      <div class="panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">
          <div class="col-sm-3">
            <div class="panel panel-danger">
              <div class="panel-heading bg-info font-bold">Filter</div>
              <div class="panel-body">
                <div class="col-sm-6">
                  From
                  <select class="form-control" ng-model="filter.fromyear" ng-init="filter.fromyear = yearNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="year for year in dateyear" required>
                  </select>
                </div>
                <div class="col-sm-6">
                  &nbsp;
                  <select class="form-control" ng-model="filter.frommonth" ng-init="filter.frommonth = monthNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="month.value as month.name for month in datemonth" required>
                  </select>
                </div>

                <div class="col-sm-6">
                  To
                  <select class="form-control" ng-model="filter.toyear" ng-init="filter.toyear = yearNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="year for year in dateyear" required>
                  </select>
                </div>
                <div class="col-sm-6">
                  &nbsp;
                  <select class="form-control" ng-model="filter.tomonth" ng-init="filter.tomonth = monthNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="month.value as month.name for month in datemonth.slice(slice) " required>
                  </select>
                </div>

                <div class="col-sm-12">
                  Center
                  <select class="form-control" ng-model="filter.center" ng-change="centerchange(filter.center)" ng-init="filter.center = 12345 " ng-options="center.centerid as center.centertitle for center in centers" required>
                  </select>
                </div>

                <div class="col-sm-12">
                  State
                  <select class="form-control" ng-model="filter.state" ng-change="statechange(filter.state)" ng-init="filter.state = 'ALL' " ng-options="state.state_code as state.state for state in states" required>
                  </select>
                </div>
                <div class="col-sm-12 wrapper-md">
                  <button class="btn btn-sm btn-info pull-right" ng-click="filtertab1(filter)">Filter</button>
                  <button class="btn btn-sm btn-default" ng-click="nofiltertab1()">Clear</button>
                </div>
              </div>
            </div> <!-- end of panel-info -->
          </div>
          <div class="col-sm-9">
            <table class="table table-striped b-t b-light">
              <thead>
                <tr>
                  <th>Month Date</th>
                  <th>REGION</th>
                  <th>CENTER NAME</th>
                  <th>NUMBER OF REGISTRANT</th>
                  <th>QUANTITY</th>
                  <th>CENTER INCOME</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="session in sessions">
                  <td>{[{session.datecreated}]}</td>
                  <td>{[{session.centerstate}]}</td>
                  <td>{[{session.centertitle}]}</td>
                  <td>{[{session.noofregistrant}]}</td>
                  <td>{[{session.totalquantity}]}</td>
                  <td>${[{session.centerincome}]}</td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td class="font-bold" style="background:royalblue;color:white">{[{totalregistrant}]}</td>
                  <td class="font-bold" style="background:royalblue;color:white">{[{finalquantity}]}</td>
                  <td class="font-bold" style="background:royalblue;color:white">${[{totalincome}]}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      </form>
    </tab>
    <tab active="tab2" ng-click="tab2true()">
      <tab-heading class="font-bold">Group Class Intro Session</tab-heading>
      <form name="tab2form">
      <div class="panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">
          <div class="col-sm-3">
            <div class="panel panel-info">
              <div class="panel-heading bg-info font-bold">Filter</div>
              <div class="panel-body">
                <div class="col-sm-6">
                  From
                  <select class="form-control" ng-model="filter.fromyear" ng-init="filter.fromyear = yearNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="year for year in dateyear" required>
                  </select>
                </div>
                <div class="col-sm-6">
                  &nbsp;
                  <select class="form-control" ng-model="filter.frommonth" ng-init="filter.frommonth = monthNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="month.value as month.name for month in datemonth" required>
                  </select>
                </div>

                <div class="col-sm-6">
                  To
                  <select class="form-control" ng-model="filter.toyear" ng-init="filter.toyear = yearNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="year for year in dateyear" required>
                  </select>
                </div>
                <div class="col-sm-6">
                  &nbsp;
                  <select class="form-control" ng-model="filter.tomonth" ng-init="filter.tomonth = monthNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="month.value as month.name for month in datemonth.slice(slice) " required>
                  </select>
                </div>

                <div class="col-sm-12">
                  Center
                  <select class="form-control" ng-model="filter.center" ng-change="centerchange(filter.center)" ng-init="filter.center = 12345 " ng-options="center.centerid as center.centertitle for center in centers" required>
                  </select>
                </div>

                <div class="col-sm-12">
                  State
                  <select class="form-control" ng-model="filter.state" ng-change="statechange(filter.state)" ng-init="filter.state = 'ALL' " ng-options="state.state_code as state.state for state in states" required>
                  </select>
                </div>
                <div class="col-sm-12 wrapper-md">
                  <button class="btn btn-sm btn-info pull-right" ng-click="filtertab2(filter)">Filter</button>
                  <button class="btn btn-sm btn-default" ng-click="nofiltertab2()">Clear</button>
                </div>
              </div>
            </div> <!-- end of panel-info -->
          </div>
          <div class="col-sm-9">
            <table class="table table-striped b-t b-light">
              <thead>
                <tr>
                  <th>Month Date</th>
                  <th>REGION</th>
                  <th>CENTER NAME</th>
                  <th>NUMBER OF REGISTRANT</th>
                  <th>QUANTITY</th>
                  <th>CENTER INCOME</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="session in group_sessions">
                  <td>{[{session.datecreated}]}</td>
                  <td>{[{session.centerstate}]}</td>
                  <td>{[{session.centertitle}]}</td>
                  <td>{[{session.noofregistrant}]}</td>
                  <td>{[{session.totalquantity}]}</td>
                  <td>${[{session.centerincome}]}</td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td class="font-bold" style="background:royalblue;color:white">{[{group_totalregistrant}]}</td>
                  <td class="font-bold" style="background:royalblue;color:white">{[{group_finalquantity}]}</td>
                  <td class="font-bold" style="background:royalblue;color:white">${[{group_totalincome}]}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      </form>
    </tab>
    <tab active="tab3" ng-click="tab3true()">
       <tab-heading class="font-bold">1 on 1 Intro Session (Franchised Center)</tab-heading>
       <form name="tab3form">
      <div class="panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">
          <div class="col-sm-3">
            <div class="panel panel-danger">
              <div class="panel-heading bg-info font-bold">Filter</div>
              <div class="panel-body">
                <div class="col-sm-6">
                  From
                  <select class="form-control" ng-model="filter.fromyear" ng-init="filter.fromyear = yearNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="year for year in dateyear" required>
                  </select>
                </div>
                <div class="col-sm-6">
                  &nbsp;
                  <select class="form-control" ng-model="filter.frommonth" ng-init="filter.frommonth = monthNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="month.value as month.name for month in datemonth" required>
                  </select>
                </div>

                <div class="col-sm-6">
                  To
                  <select class="form-control" ng-model="filter.toyear" ng-init="filter.toyear = yearNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="year for year in dateyear" required>
                  </select>
                </div>
                <div class="col-sm-6">
                  &nbsp;
                  <select class="form-control" ng-model="filter.tomonth" ng-init="filter.tomonth = monthNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="month.value as month.name for month in datemonth.slice(slice) " required>
                  </select>
                </div>

                <div class="col-sm-12">
                  Center
                  <select class="form-control" ng-model="filter.center" ng-change="centerchange(filter.center)" ng-init="filter.center = 12345 " ng-options="center.centerid as center.centertitle for center in centers" required>
                  </select>
                </div>

                <div class="col-sm-12">
                  State
                  <select class="form-control" ng-model="filter.state" ng-change="statechange(filter.state)" ng-init="filter.state = 'ALL' " ng-options="state.state_code as state.state for state in states" required>
                  </select>
                </div>
                <div class="col-sm-12 wrapper-md">
                  <button class="btn btn-sm btn-info pull-right" ng-click="filtertab3(filter)">Filter</button>
                  <button class="btn btn-sm btn-default" ng-click="nofiltertab3()">Clear</button>
                </div>
              </div>
            </div> <!-- end of panel-info -->
          </div>
          <div class="col-sm-9">
            <table class="table table-striped b-t b-light">
              <thead>
                <tr>
                  <th>Month Date</th>
                  <th>REGION</th>
                  <th>CENTER NAME</th>
                  <th>NUMBER OF REGISTRANT</th>
                  <th>QUANTITY</th>
                  <th>CENTER INCOME</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="session in one_f_sessions">
                  <td>{[{session.datecreated}]}</td>
                  <td>{[{session.centerstate}]}</td>
                  <td>{[{session.centertitle}]}</td>
                  <td>{[{session.noofregistrant}]}</td>
                  <td>{[{session.totalquantity}]}</td>
                  <td>${[{session.centerincome}]}</td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td class="font-bold" style="background:royalblue;color:white">{[{one_f_totalregistrant}]}</td>
                  <td class="font-bold" style="background:royalblue;color:white">{[{one_f_finalquantity}]}</td>
                  <td class="font-bold" style="background:royalblue;color:white">${[{one_f_totalincome}]}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      </form>
    </tab>
    <tab active="tab4" ng-click="tab4true()">
       <tab-heading class="font-bold">Group + Intro Session (Franchised Center)</tab-heading>
       <form name="tab4form">
      <div class="panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">
          <div class="col-sm-3">
            <div class="panel panel-danger">
              <div class="panel-heading bg-info font-bold">Filter</div>
              <div class="panel-body">
                <div class="col-sm-6">
                  From
                  <select class="form-control" ng-model="filter.fromyear" ng-init="filter.fromyear = yearNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="year for year in dateyear" required>
                  </select>
                </div>
                <div class="col-sm-6">
                  &nbsp;
                  <select class="form-control" ng-model="filter.frommonth" ng-init="filter.frommonth = monthNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="month.value as month.name for month in datemonth" required>
                  </select>
                </div>

                <div class="col-sm-6">
                  To
                  <select class="form-control" ng-model="filter.toyear" ng-init="filter.toyear = yearNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="year for year in dateyear" required>
                  </select>
                </div>
                <div class="col-sm-6">
                  &nbsp;
                  <select class="form-control" ng-model="filter.tomonth" ng-init="filter.tomonth = monthNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="month.value as month.name for month in datemonth.slice(slice) " required>
                  </select>
                </div>

                <div class="col-sm-12">
                  Center
                  <select class="form-control" ng-model="filter.center" ng-change="centerchange(filter.center)" ng-init="filter.center = 12345 " ng-options="center.centerid as center.centertitle for center in centers" required>
                  </select>
                </div>

                <div class="col-sm-12">
                  State
                  <select class="form-control" ng-model="filter.state" ng-change="statechange(filter.state)" ng-init="filter.state = 'ALL' " ng-options="state.state_code as state.state for state in states" required>
                  </select>
                </div>
                <div class="col-sm-12 wrapper-md">
                  <button class="btn btn-sm btn-info pull-right" ng-click="filtertab4(filter)">Filter</button>
                  <button class="btn btn-sm btn-default" ng-click="nofiltertab3()">Clear</button>
                </div>
              </div>
            </div> <!-- end of panel-info -->
          </div>
          <div class="col-sm-9">
            <table class="table table-striped b-t b-light">
              <thead>
                <tr>
                  <th>Month Date</th>
                  <th>REGION</th>
                  <th>CENTER NAME</th>
                  <th>NUMBER OF REGISTRANT</th>
                  <th>QUANTITY</th>
                  <th>CENTER INCOME</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="session in group_f_sessions">
                  <td>{[{session.datecreated}]}</td>
                  <td>{[{session.centerstate}]}</td>
                  <td>{[{session.centertitle}]}</td>
                  <td>{[{session.noofregistrant}]}</td>
                  <td>{[{session.totalquantity}]}</td>
                  <td>${[{session.centerincome}]}</td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td class="font-bold" style="background:royalblue;color:white">{[{group_f_totalregistrant}]}</td>
                  <td class="font-bold" style="background:royalblue;color:white">{[{group_f_finalquantity}]}</td>
                  <td class="font-bold" style="background:royalblue;color:white">${[{group_f_totalincome}]}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      </form>
    </tab>
	</tabset>
</div>
