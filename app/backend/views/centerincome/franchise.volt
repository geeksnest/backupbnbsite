<div class="bg-info lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Franchise (Center Income)</h1>
  <a id="top"></a>
</div>

<form name="tab3form">
      <div class="panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">
          <div class="col-sm-3">
            <div class="panel panel-danger">
              <div class="panel-heading bg-info font-bold">Filter</div>
              <div class="panel-body">
                <div class="col-sm-6">
                  From
                  <select class="form-control" ng-model="filter.fromyear" ng-init="filter.fromyear = yearNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="year for year in dateyear" required>
                  </select>
                </div>
                <div class="col-sm-6">
                  &nbsp;
                  <select class="form-control" ng-model="filter.frommonth" ng-init="filter.frommonth = monthNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="month.value as month.name for month in datemonth" required>
                  </select>
                </div>

                <div class="col-sm-6">
                  To
                  <select class="form-control" ng-model="filter.toyear" ng-init="filter.toyear = yearNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="year for year in dateyear" required>
                  </select>
                </div>
                <div class="col-sm-6">
                  &nbsp;
                  <select class="form-control" ng-model="filter.tomonth" ng-init="filter.tomonth = monthNow " ng-change="selectedyearmonth(filter.frommonth, filter.fromyear, filter.tomonth, filter.toyear)" ng-options="month.value as month.name for month in datemonth.slice(slice) " required>
                  </select>
                </div>

                <div class="col-sm-12">
                  Center
                  <select class="form-control" ng-model="filter.center" ng-change="centerchange(filter.center)" ng-init="filter.center = 12345 " ng-options="center.centerid as center.centertitle for center in centers" required>
                  </select>
                </div>

                <div class="col-sm-12">
                  State
                  <select class="form-control" ng-model="filter.state" ng-change="statechange(filter.state)" ng-init="filter.state = 'ALL' " ng-options="state.state_code as state.state for state in states" required>
                  </select>
                </div>
                <div class="col-sm-12 wrapper-md">
                  <button class="btn btn-sm btn-info pull-right" ng-click="filtertab3(filter)">Filter</button>
                  <button class="btn btn-sm btn-default" ng-click="nofiltertab3()">Clear</button>
                </div>
              </div>
            </div> <!-- end of panel-info -->
          </div>
          <div class="col-sm-9">
            <table class="table table-striped b-t b-light">
              <thead>
                <tr>
                  <th></th>
                  <th>DATE</th>
                  <th>REGION</th>
                  <th>CENTER NAME</th>
                  <th>NAME</th>
                  <th>TYPE</th>
                  <th>PAYMENT TYPE</th>
                  <th>CENTER INCOME</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="session in one_f_sessions">
                  <td>{[{$index + 1}]}.)</td>
                  <td>{[{session.datecreated}]}</td>
                  <td>{[{session.centerstate}]}</td>
                  <td>{[{session.centertitle}]}</td>
                  <td>{[{session.name}]}</td>
                  <td>{[{session.status}]}</td>
                  <td>{[{session.payment}]}</td>
                  <td>${[{session.centerincome}]}</td>
                </tr>
                <tr>
                  <td colspan="7"></td>
                  <td class="font-bold" style="background:royalblue;color:white">${[{one_f_totalincome}]}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      </form>