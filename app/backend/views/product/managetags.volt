{{ content() }}

<script type="text/ng-template" id="deleteProducttags.html">
  <div ng-include="'/be/tpl/deleteProducttags.html'"></div>
</script>

<script type="text/ng-template" id="deleteProductsize.html">
  <div ng-include="'/be/tpl/deleteProductsize.html'"></div>
</script>

<script type="text/ng-template" id="addProducttags.html">
  <div ng-include="'/be/tpl/addProducttags.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">
    Manage Tags and Size
    <button type="button" class="pull-right btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addTags()"><i class="fa fa-plus"></i>Add Tags</button>
  </h1>
  <a id="top"></a>
</div>

<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
      <div class="row">
        <div class="col-sm-6">

        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Product Tags List
          </div>

          <div class="panel-body">
            <div class="row wrapper">
                <div class="col-sm-5 m-b-xs" ng-show="keyword">
                    <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
                </div>
                <div class="col-sm-5 m-b-xs pull-right">
                    <div class="input-group">
                        <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                        <span class="input-group-btn">
                        <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                        </span>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped b-t b-light">
                    <thead>
                        <tr>
                          <th style="width:80%;">Tags</th>
                          <th>Action</th>
                        </tr>
                    </thead>
                    <tbody ng-show="loading">
                        <tr colspan="4">
                            <td>Loading Products tags</td>
                        </tr>
                    </tbody>
                    <tbody ng-hide="loading">
                        <tr colspan="4" ng-show="bigTotalItems==0"> <td> No records found! </td></tr>
                        <tr ng-repeat="mem in tags">
                            <td>
                              <span editable-text="mem.tags" onbeforesave="updatetags($data, mem.id)" e-pattern="[a-zA-Z0-9'\s]+" e-oninvalid="setCustomValidity('Please enter Alphabets and Numbers only ')"  e-required e-form="textBtnForm">{[{ mem.tags }]}</span>
                            </td>
                            <td>
                                <a href="" ng-click="textBtnForm.$show()" ng-hide="textBtnForm.$visible"><span class="label bg-warning" >Edit</span></a>
                                <a href="" ng-click="delete(mem.id)" ng-hide="textBtnForm.$visible"> <span class="label bg-danger">Delete</span></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
          </div>

          </div>
          <div class="row" ng-hide="bigTotalItems==0 || loading">
            <div class="panel-body">
                <footer class="panel-footer text-center bg-light lter">
                    <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                    <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                </footer>
            </div>
          </div>
        </div>
        

        <div class="col-sm-6">

        <alert ng-repeat="alert in alerts2" type="{[{alert.type }]}" close="closeAlert2($index)">{[{ alert.msg }]}</alert>
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Product Size List
          </div>

          <div class="panel-body">
            <div class="row wrapper">
                <div class="col-sm-5 m-b-xs" ng-show="keyword2">
                    <strong>{[{ bigTotalItems2 }]}</strong> Results found for: <strong> "{[{ keyword2 }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear2()">Clear</button>
                </div>
                <div class="col-sm-5 m-b-xs pull-right">
                    <div class="input-group">
                        <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext2">
                        <span class="input-group-btn">
                        <button class="btn btn-sm btn-default" type="button" ng-click="search2(searchtext2)">Go!</button>
                        </span>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped b-t b-light">
                    <thead>
                        <tr>
                          <th style="width:80%;">Size</th>
                          <th>Action</th>
                        </tr>
                    </thead>
                    <tbody ng-show="loading">
                        <tr colspan="4">
                            <td>Loading Products Sizes</td>
                        </tr>
                    </tbody>
                    <tbody ng-hide="loading">
                        <tr colspan="4" ng-show="bigTotalItems==0"> <td> No records found! </td></tr>
                        <tr ng-repeat="mem in size">
                            <td>
                              <span editable-text="mem.size" onbeforesave="updatesize($data, mem.id)" e-pattern="[a-zA-Z0-9'\s]+"  e-required e-form="textBtnForm">{[{ mem.size }]}</span>
                            </td>
                            <td>
                                <a href="" ng-click="textBtnForm.$show()" ng-hide="textBtnForm.$visible"><span class="label bg-warning" >Edit</span></a>
                                <a href="" ng-click="delete2(mem.id)" ng-hide="textBtnForm.$visible"> <span class="label bg-danger">Delete</span></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
          </div>

          </div>
          <div class="row" ng-hide="bigTotalItems==0 || loading">
            <div class="panel-body">
                <footer class="panel-footer text-center bg-light lter">
                    <entries max="maxSize" offset="CurrentPage" total="TotalItems"></entries>
                    <pagination ng-hide="maxSize > TotalItems" total-items="TotalItems" ng-model="CurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage2(CurrentPage)"></pagination>
                </footer>
            </div>
          </div>
        </div>

      </div>
  </div>
</fieldset>