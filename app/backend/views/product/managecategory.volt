{{ content() }}

<script type="text/ng-template" id="addProductCategory.html">
  <div ng-include="'/be/tpl/addProductCategory.html'"></div>
</script>

<script type="text/ng-template" id="deleteProductCategory.html">
  <div ng-include="'/be/tpl/deleteProductCategory.html'"></div>
</script>

<script type="text/ng-template" id="editProductCategory2.html">
  <div ng-include="'/be/tpl/editProductCategory2.html'"></div>
</script>

<script type="text/ng-template" id="editProductCategory.html">
  <div ng-include="'/be/tpl/editProductCategory.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">
    Manage Category
    <button type="button" class="pull-right btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addCategory()"><i class="fa fa-plus"></i>Add Category</button>
  </h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formmanagenews">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
      <div class="row">
        <div class="col-sm-12">

          <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Product Category List
            </div>

              <div class="panel-body">
                <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs" ng-show="keyword">
                        <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
                    </div>
                    <div class="col-sm-5 m-b-xs pull-right">
                        <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                              <th>Category</th>
                              <th>Sub Category</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody ng-show="loading">
                            <tr colspan="4">
                                <td>Loading Products categories</td>
                            </tr>
                        </tbody>
                        <tbody ng-hide="loading">
                            <tr colspan="4" ng-show="bigTotalItems==0"> <td> No records found! </td></tr>
                            <tr ng-repeat="mem in category">
                                <td>{[{ mem.category }]}</td>
                                <td>{[{ mem.subcat }]}</td>
                                <td>
                                    <a href="" ng-click="edit(mem.id)"><span class="label bg-warning" >Edit</span></a>
                                    <a href="" ng-click="delete(mem.id)"> <span class="label bg-danger">Delete</span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

              </div>


          </div>
        </div>

      </div>

      <div class="row" ng-hide="bigTotalItems==0 || loading">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
                <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
</fieldset>
</form>