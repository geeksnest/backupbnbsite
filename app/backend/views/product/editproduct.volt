{{ content() }}

<script type="text/ng-template" id="addInvoice.html">
   <div ng-include="'/be/tpl/addInvoice.html'"></div>
</script>

<script type="text/ng-template" id="mediagallery.html">
   <div ng-include="'/be/tpl/mediagallery.html'"></div>
</script>

<script type="text/ng-template" id="removeProduct.html">
   <div ng-include="'/be/tpl/removeProduct.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md ng-scope">
  <h1 class="m-n font-thin h3">
    Edit Product 
  </h1>
  <a id="top"></a>
</div>

<div class="hbox hbox-auto-xs hbox-auto-sm">
    <div class="col w-sm bg-light dk b-r bg-auto bg-auto-left" ng-if="invoice.number">
        <!-- <div class="wrapper b-b bg">
            <h3 class="m-n font-thin">Add Products</h3>
        </div> -->
        <div class="wrapper hidden-sm hidden-xs" id="email-menu">
            <ul class="nav nav-pills nav-stacked nav-sm">
              <li ng-repeat="item in inventory">
                  <a href="" ng-click="setactive($index)">
                      {[{ item.name }]}
                  </a>
              </li>
            </ul>
        </div>
    </div>

    <div class="col">
      <form class="form-validation form-horizontal  ng-pristine ng-invalid ng-invalid-required" name="formproduct" id="formproductsa">
        <fieldset ng-disabled="isSaving">
          <div class="wrapper-md">
              <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

              <div class="row">

                <div class="col-sm-8">
                  <div class="panel panel-default">

                    <div class="panel-heading font-bold">
                      {[{ product.name }]}
                      <span ng-if="!product.name">Product</span>
                    </div>


                    <div class="panel-body">
                      <input type="hidden" ng-model="product.productid">
                      <!-- form -->
                      <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label class="col-sm-2 control-label">
                          <label for="title">Item Name</label>
                        </label>
                        <div class="col-sm-9">
                          <span class="label label-danger" ng-if="exist2">Item name already exist.</span>
                          <span class="label label-danger help-block mg-left" ng-if="formproduct.itemname.$error.required">Item name is required.</span>
                          <input type="text" name="itemname" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-sm" ng-model="product.name" ng-change="validateName(product.name, product.productid)" ui-autocomplete="myOption" required="required">
                        </div>
                      </div>
                      
                      <div class="line line-dashed b-b line-lg"></div>
                      <div class="form-group">
                        <label class="col-sm-offset-2 col-sm-2 control-label text-left" style="width:109px;">
                          <label for="title">Product Slugs</label>
                        </label>
                        <div class="col-sm-7" style="width:61%;">
                          <span class="label label-danger mg-left" ng-if="exist3">Product slugs already exist.</span>
                          <input type="text" ng-show="editslug" id="pageslugs" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern pull-left" ng-model="product.slugs" ng-change="onslugs(product.slugs)">
                          <span ng-bind="product.slugs" class="pull-left mg-left" style="margin-top:7px;"></span>
                          <div ng-show="editslug">
                            <a class="btn btn-danger btn-xs pull-right mg-left" ng-click="cancelpageslug(product.name)">cancel</a>
                            <a class="btn btn-primary btn-xs pull-right mg-left" ng-click="setslug(product.slugs)">ok</a>
                          </div>
                          <a class="btn btn-danger btn-xs pull-right mg-left" ng-hide="editslug" ng-disabled="disable" ng-click="clearslug(product.name)">clear</a>
                          <a class="btn btn-primary btn-xs pull-right mg-left" ng-hide="editslug" ng-disabled="disable" ng-click="editprodslug()">edit slug</a>
                        </div>
                      </div>

                      <!-- <div class="line line-dashed b-b line-lg"></div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">
                          <label for="title">Product Code</label>
                        </label>
                        <div class="col-sm-9">
                          <span class="label label-danger" ng-if="exist">Product code already exist.</span>
                          <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-sm" ng-model="product.productcode" ng-change="validatepcode(product.productcode, product.name)" ng-disabled="disable" required="required">
                        </div>
                      </div> -->
                      
                      <div class="line line-dashed b-b line-lg"></div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">
                          <label for="title">Category</label>
                        </label>
                        <div class="col-sm-9">
                          <span class="label label-danger" ng-if="required.category">Category is required.</span>
                          <ui-select multiple on-select="validation(product.category, 'category')" on-remove="validation(product.category, 'category')" ng-disabled="disable" ng-model="product.category" theme="bootstrap" ng-disabled="disabled" title="Choose a Category">
                            <ui-select-match placeholder="Select category...">{[{ $item }]} </ui-select-match>
                            <ui-select-choices repeat="cat in categories">
                              {[{ cat }]}
                            </ui-select-choices>
                          </ui-select>
                        </div>
                      </div>
                      
                      <div class="line line-dashed b-b line-lg"></div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">
                          <label for="title">Sub Category</label>
                        </label>
                        <div class="col-sm-9">
                          <span class="label label-danger" ng-if="required.subcategory">Sub category is required.</span>
                          <ui-select multiple on-select="validation(product.subcategory, 'subcategory')" on-remove="validation(product.subcategory, 'subcategory')" ng-disabled="disable" ng-model="product.subcategory" theme="bootstrap" ng-disabled="disabled" title="Choose a Sub Category">
                            <ui-select-match placeholder="Select sub category...">{[{ $item }]} </ui-select-match>
                            <ui-select-choices repeat="scat in subcategories">
                              {[{ scat }]}
                            </ui-select-choices>
                          </ui-select>
                        </div>
                      </div>

                      <div class="line line-dashed b-b line-lg"></div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">
                          <label for="title">Type</label>
                        </label>
                        <div class="col-sm-9">
                          <span class="label label-danger" ng-if="required.type">Item type is required.</span>
                          <ui-select on-select="validation(product.type, 'type')" on-remove="validation(product.type, 'type')" multiple name="type" ng-required="true" ng-disabled="disable" ng-model="product.type" theme="bootstrap" ng-disabled="disabled" title="Choose a type">
                            <ui-select-match placeholder="Select type...">{[{ $item }]} </ui-select-match>
                            <ui-select-choices repeat="type in types">
                              {[{ type }]}
                            </ui-select-choices>
                          </ui-select>
                        </div>
                      </div>
                      
                      <div class="line line-dashed b-b line-lg"></div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">
                          <label for="title">Tags</label>
                        </label>
                        <div class="col-sm-9">
                          <span class="label label-danger" ng-if="required.tags">Tag is required.</span>
                          <ui-select multiple on-select="validation(product.tags, 'tags')" on-remove="validation(product.tags, 'tags')" tagging ng-disabled="disable" tagging-label="false" ng-model="product.tags" theme="bootstrap" title="Choose a tag">
                            <ui-select-match placeholder="Select tags...">{[{ $item }]} </ui-select-match>
                            <ui-select-choices repeat="tag in tags">
                              {[{ tag }]}
                            </ui-select-choices>
                          </ui-select>
                        </div>
                      </div>
                      
                      <div class="line line-dashed b-b line-lg"></div>
                      <div class="form-group" show-errors='{ showSuccess: true }'>
                        <label class="col-sm-2 control-label">
                          <label for="title">Short Description</label>
                        </label>
                        <div class="col-sm-9">
                          <span class="label label-danger help-block mg-left" ng-if="formproduct.sdescription.$error.required">Short description is required.</span>
                          <textarea id="sdescription" name="sdescription" ng-disabled="disable" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-sm" ng-model="product.shortdesc" required="required"></textarea>
                        </div>
                      </div>

                      <div class="line line-dashed b-b line-lg"></div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">
                          <label for="title">Description</label>
                        </label>
                        <div class="col-sm-9">
                          <span class="label label-danger" ng-if="required.description">Description is required.</span>
                          <textarea id="description" name="description" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-sm ck-editor" ng-model="product.description" ng-change="validation(product.description, 'description')" required="required"></textarea>
                        </div>
                      </div>
                    </div>
                </div>
              </div>

              <div class="col-sm-4">
                <div class="panel panel-default">
                  <div class="panel-heading font-bold">
                    Product Image 
                    <span class="label label-danger" ng-if="required.images">Product image is required.</span>
                    <button type="button" class="btn btn-default btn-xs pull-right" ng-disabled="disable" ng-click="media('featured')"><i class="fa  fa-folder-open"></i> Media Library</button>
                  </div>
                  <div class="panel-body">                       
                    <div class="col-sm-4" style="padding: 3px;" ng-repeat="image in product.images" ng-if="image.status==1">
                      <div style="height: 99px; background-image: url('{[{ amazonlink }]}/uploads/productimage/{[{ image.productid }]}/{[{ image.filename }]}')" class="full-width bg-image featured">
                        <span class="fa fa-check"></span>
                      </div>
                    </div>
                    <div class="col-sm-4" style="padding: 3px;" ng-repeat="image in product.images" ng-if="image.status==0">
                      <a ng-click="setimage(image)">
                        <div style="height: 99px; background-image: url('{[{ amazonlink }]}/uploads/productimage/{[{ image.productid }]}/{[{ image.filename }]}')" class="full-width bg-image pimage"></div>
                      </a>
                    </div>
                    <div class="col-sm-12">
                      <p ng-if="product.images.length > 0">*Please click on the  desired image to be the main image on shop.</p> 
                    </div>  
                  </div>  
                </div>
                
                <div class="panel panel-default">
                  <div class="panel-heading font-bold">
                    Color and Size  <i class="font-italic">(Optional)</i>
                  </div>
                  <div class="panel-body">
                      <div class="form-group">
                        <label class="col-sm-3 control-label">
                          <label for="title">Size</label>
                        </label>
                        <div class="col-sm-8">
                          <ui-select multiple ng-disabled="disable" tagging tagging-label="false" ng-model="product.size" theme="bootstrap" title="Choose a size">
                            <ui-select-match placeholder="Select size...">{[{ $item }]} </ui-select-match>
                            <ui-select-choices repeat="size in sizes">
                              {[{ size }]}
                            </ui-select-choices>
                          </ui-select>
                        </div>
                      </div>

                      <div class="line line-dashed b-b line-lg"></div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Color</label>
                        <div class="col-sm-9 wrapper-sm">
                          <input colorpicker="" ng-disabled="disable" ng-model="product.color" class="form-control pull-left width90px pointer ng-pristine ng-valid ng-touched" title="Click me!" type="text">
                          <div class="colorpicker-colorbox form-control pull-left" style="width:50px; background: {[{product.color}]};"> </div>
                        </div>
                      </div>
                  </div>  
                </div>

                <div class="panel panel-default">
                  <div class="panel-heading font-bold">
                    Price and Quantity
                  </div>
                  <div class="panel-body">
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                      <label class="col-sm-3 control-label">
                        <label for="title">Quantity</label>
                      </label>
                      <div class="col-sm-3">
                        <span class="label label-danger help-block mg-left" ng-if="formproduct.quantity.$error.required">Required.</span>
                        <div>
                          <input ui-jq="TouchSpin" name="quantity" type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-sm" data-min='1' data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" ng-model="product.quantity" data-verticaldownclass="fa fa-caret-down" required="required" only-digits>
                        </div>
                      </div>
                    </div>

                    <div class="line line-dashed b-b line-lg"></div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                      <label class="col-sm-3 control-label">
                        <label for="title">Price</label>
                      </label>
                      <div class="col-sm-4">
                        <span class="label label-danger help-block mg-left" ng-if="formproduct.price.$error.required">Required.</span>
                        <div class="input-group m-b">
                          <span class="input-group-addon">$</span>
                          <input class="form-control input-sm" name="price" type="text" ng-disabled="disable" required="required" ng-model="product.price" only-digits>
                        </div>
                      </div>
                    </div>
                  </div>  
                </div>
                
                <accordion close-others="true">
                  <accordion-group 
                    is-open="stat" >
                    <accordion-heading class="font-bold">
                      <b>Discount</b> <i class="pull-right fa fa-angle-right" 
                      ng-class="{'fa-angle-down': stat, 'fa-angle-right': !stat}"></i>
                    </accordion-heading>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        <label for="title">Discount</label>
                      </label>
                      <div class="col-sm-4">
                        <div class="input-group m-b">
                          <span class="input-group-addon">$</span>
                          <input class="form-control input-sm" type="text" ng-model="product.discprice" ng-change="discount(product.discprice, 'price')" only-digits>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="input-group m-b">
                          <input class="form-control input-sm" type="text" ng-model="product.discount" ng-change="discount(product.discount, 'percentage')" only-digits>
                          <span class="input-group-addon">%</span>
                        </div>
                      </div>
                    </div>
                    
                    <div class="line line-dashed b-b line-lg"></div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        <label for="title">Discounted price</label>
                      </label>
                      <div class="col-sm-4">
                        <div class="input-group m-b">
                          <span class="input-group-addon">$</span>
                          <input class="form-control input-sm" ng-disabled="true" type="text" ng-model="product.discounted" ng-change="discount(product.discprice, 'price')" only-digits>
                        </div>
                      </div>
                    </div>
                    
                    <div class="line line-dashed b-b line-lg"></div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        <label for="title">From</label>
                      </label>
                      <div class="input-group col-sm-7">
                        <span class="input-group-btn">
                          <input id="datefrom" name="datefrom" class="form-control" datepicker-popup="yyyy-MM-dd" ng-model="product.discount_from" is-open="dataPickerStates.open1" datepicker-options="dateOptions" close-text="Close" type="text" disabled>
                          <button type="button" class="btn btn-default" ng-click="open($event, 'open1')"><i class="glyphicon glyphicon-calendar"></i></button>
                        </span>
                      </div>
                    </div>

                    <div class="line line-dashed b-b line-lg"></div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        <label for="title">To</label>
                      </label>
                      <div class="input-group col-sm-7">
                        <span class="input-group-btn">
                          <input id="dateto" name="dateto" class="form-control" datepicker-popup="yyyy-MM-dd" ng-model="product.discount_to" is-open="dataPickerStates.open2" datepicker-options="dateOptions" close-text="Close" type="text" disabled>
                          <button type="button" class="btn btn-default" ng-disabled="disable" ng-click="open($event, 'open2')"><i class="glyphicon glyphicon-calendar"></i></button>
                        </span>
                      </div>
                    </div>

                  </accordion-group>
                </accordion>

                <div class="panel panel-default">
                  <div class="panel-heading font-bold">
                    Settings
                  </div>
                  <div class="panel-body">
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                      <label class="col-sm-12 control-label text-left">
                        <label for="title">Notify for Quantity </label>
                      </label>
                      <label class="col-sm-3 control-label">
                        <label for="title">Below</label>
                      </label>
                      <div class="col-sm-3">
                        <span class="label label-danger help-block mg-left" ng-if="formproduct.below.$error.required">Required.</span>
                        <input ui-jq="TouchSpin" type="text" name="below" ng-disabled="disable" value="0" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-sm" data-min='1' data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" ng-model="product.belowquantity" data-verticaldownclass="fa fa-caret-down" only-digits required>
                      </div>
                    </div>

                    <div class="line line-dashed b-b line-lg"></div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        <label for="title">Status</label>
                      </label>
                      <div class="col-sm-8">
                        <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                          <input checked="" type="checkbox" ng-disabled="disable" ng-model="product.status">
                          <i></i>
                        </label>
                        <span class="label label-success stat" ng-if="product.status">Active</span>
                        <span class="label label-danger stat" ng-if="!product.status">Deactivated</span>
                      </div>
                    </div>
                      
                    <div class="line line-dashed b-b line-lg"></div>
                    <div class="form-group">
                      <label class="col-sm-12 control-label text-left">
                        <label for="title">Quanity Allowed in Shopping Cart</label>
                      </label>
                    </div>
                    <div class="form-group" show-errors='{ showSuccess: true }'>
                      <label class="col-sm-offset-1 col-sm-2 control-label text-left">
                        <label for="title">Maximum</label>
                      </label>
                      <div class="col-sm-3">
                        <span class="label label-danger help-block mg-left" ng-if="formproduct.maximum.$error.required">Required.</span>
                        <input ui-jq="TouchSpin" name="maximum" ng-disabled="disable" type="text" value="0" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-sm" data-min='1' data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" ng-model="product.maxquantity" data-verticaldownclass="fa fa-caret-down" only-digits required>
                      </div>
                    </div>

                    <div class="form-group" show-errors='{ showSuccess: true }'>
                      <label class="col-sm-offset-1 col-sm-2 control-label text-left">
                        <label for="title">Minimum</label>
                      </label>
                      <div class="col-sm-3">
                        <span class="label label-danger help-block mg-left" ng-if="formproduct.minimum.$error.required">Required.</span>
                        <input ui-jq="TouchSpin" name="minimum" ng-disabled="disable" type="text" value="0" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern input-sm" data-min='1' data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" ng-model="product.minquantity" data-verticaldownclass="fa fa-caret-down" only-digits required>
                      </div>
                    </div>

                  </div>
                </div>

                </div>
              </div>

              <div class="row">
                <div class="panel-body">
                    <footer class="panel-footer text-right bg-light lter">
                      <button type="button" class="btn btn-success" ng-click="editproduct(product)">Update</button>
                    </footer>
                </div>
              </div>
            </div>
            
          </div>
        </fieldset>
      </form>

    </div>
</div>