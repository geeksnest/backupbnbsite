{{ content() }}

<script type="text/ng-template" id="addInvoice.html">
   <div ng-include="'/be/tpl/addInvoice.html'"></div>
</script>

<script type="text/ng-template" id="mediagallery.html">
   <div ng-include="'/be/tpl/mediagallery.html'"></div>
</script>

<script type="text/ng-template" id="removeProduct.html">
   <div ng-include="'/be/tpl/removeProduct.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md ng-scope">
  <h1 class="m-n font-thin h3">
    View Product 
  </h1>
  <a id="top"></a>
</div>

<div class="hbox hbox-auto-xs hbox-auto-sm">
    <div class="col">
      <form class="form-validation form-horizontal  ng-pristine ng-invalid ng-invalid-required" name="formproduct" id="formproductsa" ng-submit="editproduct(product)">
        <fieldset ng-disabled="isSaving">
          <div class="wrapper-md">
              <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

              <div class="row">
                <div class="col-sm-12">
                  <div class="col-sm-6">
                    <h4>Product code: {[{ product.productcode }]}</h4>
                  </div>
                  <div class="col-sm-6">
                    <button ng-click="print()" class="btn btn-info viewpr-mg-bot pull-right pdf"> Print</button>
                    <a ui-sref="editproduct({ productid: product.productid })" class="btn btn-primary viewpr-mg-bot pull-right"> Edit</a>
                    <a ui-sref="manageproduct" class="btn btn-default viewpr-mg-bot pull-right"><i class="glyphicon glyphicon-chevron-left"></i> back</a>
                  </div>
                  <!-- product -->
                  <div class="col-sm-6">
                    <div class="panel panel-info">
                      <div class="panel-heading font-bold">
                        Product information
                      </div>
                      <div class="list-group bg-white">
                        <div class="list-group-item">
                          Product Name : {[{ product.name }]}
                        </div>
                        <div class="list-group-item">
                          Description : {[{ product.shortdesc }]}
                        </div>
                        <div class="list-group-item">
                          Category: <span ng-repeat="category in product.category">{[{ category }]} {[{ $last ? "" : ', '}]}</span>
                        </div>
                        <div class="list-group-item">
                          Sub category: <span ng-repeat="subcategory in product.subcategory">{[{ subcategory }]} {[{ $last ? "" : ', '}]}</span>
                        </div>
                        <div class="list-group-item">
                          Type: <span ng-repeat="type in product.type">{[{ type }]} {[{ $last ? "" : ', '}]}</span>
                        </div>
                        <div class="list-group-item">
                          Price: $ {[{ product.price }]}
                        </div>
                        <div class="list-group-item">
                          Quantity: {[{ product.quantity }]}
                        </div>
                        <div class="list-group-item">
                          Status: {[{ product.status == 1 ? "Active" : "Inactive" }]}
                        </div>
                        <div class="list-group-item">
                          Availability: {[{ product.quantity > 0 ? "In Stock" : "Out of Stock" }]}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="panel panel-info">
                      <div class="panel-heading font-bold">
                        Images
                      </div>
                      <div class="panel-body">                    
                        <div class="col-sm-4" ng-repeat="image in product.images" style="padding: 3px;">
                          <div style="height: 96.833px; background-image: url('{[{ amazonlink }]}/uploads/productimage/{[{ product.productid }]}/{[{ image.filename }]}')" class="full-width bg-image"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <!-- product distributors -->
                  <div class="col-sm-12">
                    <div class="panel panel-info">
                      <div class="panel-heading font-bold">
                        Product Distributors
                      </div>
                      <div class="panel-body">    
                        <div class="table-responsive">
                          <table class="table table-striped b-t b-light">
                              <thead>
                                  <tr>
                                    <th>Invoice #</th>
                                    <th>Distributor Name</th>
                                    <th>Date Delivered</th>
                                    <th>Quantity</th>
                                  </tr>
                              </thead>
                              <tbody ng-show="loading">
                                  <tr colspan="4">
                                      <td>Loading Products</td>
                                  </tr>
                              </tbody>
                              <tbody ng-hide="loading">
                                  <tr colspan="4" ng-show="bigTotalItems_invoice==0"> <td> No records found! </td></tr>
                                  <tr ng-repeat="mem in invoice">
                                      <td>{[{ mem.invoice_number }]}</td>
                                      <td>{[{ mem.distname }]}</td>
                                      <td>{[{ mem.date_added }]}</td>
                                      <td>{[{ mem.quantity }]}</td>
                                  </tr>
                              </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div class="row" ng-hide="bigTotalItems_invoice==0 || loading">
                      <div class="panel-body">
                        <footer class="panel-footer text-center bg-light lter">
                            <entries max="maxSize" offset="bigCurrentPage_invoice" total="bigTotalItems_invoice"></entries>
                            <pagination ng-hide="maxSize > bigTotalItems_invoice" total-items="bigTotalItems_invoice" ng-model="bigCurrentPage_invoice" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage_invoice, 'invoice')"></pagination>
                        </footer>
                      </div>
                    </div>
                  </div>

                  <!-- purchased list -->
                  <div class="col-sm-12">
                    <div class="panel panel-info">
                      <div class="panel-heading font-bold">
                        Purchased list
                      </div>
                      <div class="panel-body">         

                      </div>
                    </div>
                  </div>

                  <!-- purchased history -->
                  <div class="col-sm-12">
                    <div class="panel panel-info no-margin">
                      <div class="panel-heading font-bold">
                        Product History
                      </div>
                      <div class="panel-body">      
                        <div class="table-responsive">
                          <table class="table table-striped b-t b-light">
                              <thead>
                                  <tr>
                                    <th style="width:80%;">Action</th>
                                    <th style="width:20%;">Date Time</th>
                                  </tr>
                              </thead>
                              <tbody ng-show="loading">
                                  <tr colspan="4">
                                      <td>Loading Products</td>
                                  </tr>
                              </tbody>
                              <tbody ng-hide="loading">
                                  <tr colspan="4" ng-show="bigTotalItems_hist==0"> <td> No records found! </td></tr>
                                  <tr ng-repeat="history in history">
                                      <td ng-bind-html="history.action"></td>
                                      <td>{[{ history.datetime }]}</td>
                                  </tr>
                              </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div class="row" ng-hide="bigTotalItems_hist==0 || loading">
                      <div class="panel-body">
                        <footer class="panel-footer text-center bg-light lter">
                            <entries max="maxSize" offset="bigCurrentPage_hist" total="bigTotalItems_hist"></entries>
                            <pagination ng-hide="maxSize > bigTotalItems_hist" total-items="bigTotalItems_hist" ng-model="bigCurrentPage_hist" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage_hist, 'history')"></pagination>
                        </footer>
                      </div>
                    </div>
                  </div>
                
                </div>
              </div>
            </div>
            
          </div>
        </fieldset>
      </form>

    </div>
</div>

  
<div id="print">
  <img src="/img/bnblogo2.png" style="width:140px;
    visibility: collapse;
    position: absolute;
    top:0;
    left:0; 
    margin-bottom: 5px;
    margin-left: 240px;">
  <div class="hide">
    <div class="row">
      <div class="col-sm-12">
        <h4>Product code: {[{ product.productcode }]}</h4>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Item Name:</i> {[{ product.name }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Description:</i> {[{ product.shortdesc }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Category:</i> <span ng-repeat="type in product.type">{[{ type }]}{[{ $last ? "" : ', '}]}</span></p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Sub category:</i> <span ng-repeat="type in product.type">{[{ type }]}{[{ $last ? "" : ', '}]}</span></p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Type:</i> <span ng-repeat="type in product.type">{[{ type }]}{[{ $last ? "" : ', '}]}</span></p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Size:</i> {[{ product.size }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Color:</i> {[{ product.color }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Price:</i> $ {[{ product.price }]}</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Quantity:</i> {[{ product.quantity }]} qty.</p>
      </div>
      <div class="col-sm-12">
        <p><i class="text-bold">Status:</i> {[{ product.status == 1 ? "Active" : "Inactive" }]}</p>
      </div>
    </div>
  </div>
  
  <div class="table-responsive">
    <table class="table table-striped b-t b-light" style='position:absolute;bottom:200000px'>
      <h5 class="hide">Product Distributors</h5>
        <thead>
          <tr>
            <th>Invoice #</th>
            <th>Distributor Name</th>
            <th>Date Delivered</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody ng-hide="loading">
          <tr ng-repeat="mem in invoice2">
              <td>{[{ mem.invoice_number }]}</td>
              <td>{[{ mem.distname }]}</td>
              <td>{[{ mem.date_added }]}</td>
              <td>{[{ mem.quantity }]}</td>
          </tr>
        </tbody>
    </table>
  </div>
  
  <div class="table-responsive">
    <table class="table table-striped b-t b-light" style='position:absolute;bottom:200000px'>
      <h5 class="hide">Product History</h5>
        <thead>
            <tr>
              <th style="width:80%;">Action</th>
              <th style="width:20%;">Date Time</th>
            </tr>
        </thead>
        <tbody ng-hide="loading">
            <tr ng-repeat="history in history2">
                <td ng-bind-html="history.action"></td>
                <td>{[{ history.datetime }]}</td>
            </tr>
        </tbody>
    </table>
  </div>

</div>