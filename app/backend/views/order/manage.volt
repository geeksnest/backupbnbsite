{{ content() }}

<script type="text/ng-template" id="deleteProduct.html">
  <div ng-include="'/be/tpl/deleteProduct.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Order</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formmanagenews">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
      <div class="row">
        <div class="col-sm-4">
          <a ui-sref="addorder" class="btn btn-primary" style="margin-top:43px;">Add Order</a>
          <a ng-click="print()" class="btn btn-default" style="margin-top:43px;">Print</a>
        </div>
        <div class="col-sm-8">
          <div class="row row-sm text-center">
            <div class="col-xs-3 mouse-pointer">
              <a href="#" ng-click="notif('processing')" class="block panel padder-v bg-info item">
                <span class="text-white font-thin h1 block">{[{ processing }]}</span>
                <span class="text-muted text-xs">Processing</span>
              </a>
            </div>
            <div class="col-xs-3 mouse-pointer">
              <a href="#" ng-click="notif('completed')" class="block panel padder-v bg-warning item">
                <span class="text-white font-thin h1 block">{[{ completed }]}</span>
                <span class="text-muted text-xs">Completed</span>
              </a>
            </div>
            <div class="col-xs-3 mouse-pointer">
              <a href="#" href ng-click="notif('pending')" class="block panel padder-v bg-danger item">
                <span class="text-white font-thin h1 block">{[{ pending }]}</span>
                <span class="text-muted text-xs">Pending Orders</span>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="#" ng-click="notif('today')" class="block panel padder-v bg-primary item">
                <span class="text-white font-thin h1 block">{[{ today }]}</span>
                <span class="text-muted text-xs">Today's Orders</span>
              </a>
            </div>
          </div>
        </div>
        <div class="col-sm-12">

          <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Order List
            </div>

              <div class="panel-body">
                <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs" ng-show="keyword">
                        <strong>{[{ bigTotalItems }]}</strong> Results found for: <strong> "{[{ keyword }]}"</strong> <button class="btn btn-default btn-xs" ng-click="clear()">Clear</button>
                    </div>
                    <div class="col-sm-5 m-b-xs pull-right">
                        <div class="input-group">
                            <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                            <span class="input-group-btn">
                            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                              <th>Order no.</th>
                              <th>Purchased On</th>
                              <th>Bill to:</th>
                              <th>Ship to:</th>
                              <th>Payment Type</th>
                              <th>Grand Total(Purchased)</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody ng-show="loading">
                            <tr colspan="4">
                                <td>Loading Products</td>
                            </tr>
                        </tbody>
                        <tbody ng-hide="loading">
                            <tr colspan="4" ng-show="bigTotalItems==0"> <td> No records found! </td></tr>
                            <tr ng-repeat="mem in orders">
                                <td>{[{ mem.invoiceno }]}</td>
                                <td>{[{ mem.created_at }]}</td>
                                <td>{[{ mem.firstname!=null ? mem.firstname + " " + mem.lastname : '' }]}</td>
                                <td>{[{ mem.shipfirstname!=null ? mem.shipfirstname + " " + mem.shiplastname : '' }]}</td>
                                <td ng-if="mem.paymenttype=='cash'">Cash</td>
                                <td ng-if="mem.paymenttype=='card'">Credit Card</td>
                                <td ng-if="mem.paymenttype=='check'">E-check</td>
                                <td ng-if="mem.paymenttype=='paypal'">Paypal</td>
                                <td>{[{ mem.amount | currency }]}</td>
                                <td>{[{ mem.status }]}</td>
                                <td>
                                  <a href="" ng-click="view(mem.id)"><span class="label bg-info" >View</span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div class="row" ng-hide="bigTotalItems==0 || loading">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
                <entries max="maxSize" offset="bigCurrentPage" total="bigTotalItems"></entries>
                <pagination ng-hide="maxSize > bigTotalItems" total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
</fieldset>
</form>

<div id="print">  
  <img src="/img/bnblogo2.png" style="width:140px;
  visibility: collapse;
  position: absolute;
  top:0;
  margin-left: 240px;">
  <div class="table-responsive">
    <table class="table table-striped b-t b-light" style='position:absolute;bottom:200000px'>
      <h5 class="hide">Product list</h5>
        <thead>
          <tr>
            <th style="width: 20%">Order no.</th>
            <th style="width: 20%">Purchased On</th>
            <th style="width: 20%">Bill to:</th>
            <th style="width: 20%">Ship to:</th>
            <th style="width: 20%">Payment Type</th>
            <th style="width: 50%">Grand Total(Purchased)</th>
            <th style="width: 50%">Status</th>
          </tr>
        </thead>
        <tbody ng-hide="loading">
          <tr ng-repeat="mem in orders2">
              <td>{[{ mem.invoiceno }]}</td>
              <td>{[{ mem.created_at }]}</td>
              <td>{[{ mem.firstname + " " + mem.lastname }]}</td>
              <td>{[{ mem.shipfirstname!=null ? mem.shipfirstname + " " + mem.shiplastname : '' }]}</td>
              <td>{[{ mem.paymenttype=='cash' ? 'Cash' : mem.paymenttype=='card' ? 'Credit Card' : 'E-check' }]}</td>
              <td>{[{ mem.amount | currency }]}</td>
              <td>{[{ mem.status }]}</td>
          </tr>
        </tbody>
    </table>
  </div>
</div>