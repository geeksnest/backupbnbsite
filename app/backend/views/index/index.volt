<!DOCTYPE html>
<html lang="en">
<head>
  <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
  <META HTTP-EQUIV="EXPIRES" CONTENT="0">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  {{ get_title() }}
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="/img/smalllogo.png">
  <!-- Stylesheets -->
  {{ stylesheet_link('be/css/bootstrap.css') }}
  {{ stylesheet_link('be/css/animate.css') }}
  {{ stylesheet_link('be/css/font-awesome.min.css') }}
  {{ stylesheet_link('be/css/simple-line-icons.css') }}
  {{ stylesheet_link('be/css/font.css') }}
  {{ stylesheet_link('be/css/app.css') }}

  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon
  <link rel="shortcut icon" href="{{url.getBaseUri()}}public/img/favicon/favicon.png">-->
</head>

<body>
  <div class="container w-xxl w-auto-xs" ng-controller="SigninFormController">
  <a href class="navbar-brand block m-t">Body and Brain</a>
  <div class="m-b-lg">
    <div class="wrapper text-center">
      <img src='/img/bnblogo.gif' style="width:100%;height:auto;">
    </div>
    <form name="form" class="form-validation" method="post">
      <div class="text-danger wrapper text-center" ng-show="authError">
          {{ content() }}
      </div>
      <div class="list-group list-group-sm">
        <div class="list-group-item">
          {{ text_field('username', 'size': "30", 'class': "form-control no-border", 'id': "inputUsername", 'placeholder': 'Username', 'ng-model': 'user.password', 'required':'required') }}
        </div>
        <div class="list-group-item">
          {{ password_field('password', 'size': "30", 'class': "form-control no-border", 'id': "inputPassword", 'placeholder': 'Password', 'ng-model': 'user.password', 'required': 'required') }}
        </div>
      </div>
      <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="login()" ng-disabled='form.$invalid'>Log in</button>
      <div class="text-center m-t m-b"><a href="/bnbadmin/forgotpassword">Forgot password?</a></div>
      <div class="line line-dashed"></div>
      <p class="text-center"><small>Only the SuperAgent can access beyond this point.</small></p>
    </form>
  </div>
  <div class="text-center" ng-include="'tpl/blocks/page_footer.html'"></div>
</div>
<!-- JS -->
{{ javascript_include('be/js/jquery/jquery.min.js') }}
<!-- angular -->
{{ javascript_include('be/js/angular/angular.min.js') }}
{{ javascript_include('be/js/angular/angular-cookies.min.js') }}
{{ javascript_include('be/js/angular/angular-animate.min.js') }}
{{ javascript_include('be/js/angular/angular-ui-router.min.js') }}
{{ javascript_include('be/js/angular/angular-translate.js') }}
{{ javascript_include('be/js/angular/ngStorage.min.js') }}
{{ javascript_include('be/js/angular/ui-load.js') }}
{{ javascript_include('be/js/angular/ui-jq.js') }}
{{ javascript_include('be/js/angular/ui-validate.js') }}
{{ javascript_include('be/js/angular/ui-bootstrap-tpls.min.js') }}

<!-- APP -->

</body>
</html>
