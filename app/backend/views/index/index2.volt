<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="user-scalable = yes" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  {{ get_title() }}
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  {{ stylesheet_link('be/css/bootstrap.css') }}
  {{ stylesheet_link('be/css/animate.css') }}
  {{ stylesheet_link('be/css/font-awesome.min.css') }}
  {{ stylesheet_link('be/css/simple-line-icons.css') }}
  {{ stylesheet_link('be/css/font.css') }}
  {{ stylesheet_link('be/css/app.css') }}

  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]-->

  <link rel="shortcut icon" href="/img/smalllogo.png">

</head>

<body >
  <div class="container w-xxl w-auto-xs" >
    <a href class="navbar-brand block m-t">Body and Brain</a>
    <div class="m-b-lg" ng-app="myApp" ng-controller="myCtrl" >
      <div class="wrapper text-center">
        <img src='/img/bnblogo.gif' style="width:100%;height:auto;">
      </div>
      <span ng-show="showerror">Invalid Username/Password</span>
      <form name="form" class="form-validation" method="post" ng-submit="submitData(user)">

        <div class="list-group list-group-sm">
          <div class="list-group-item">
            <input type="text"  id="" name="" class="form-control no-border" ng-model="user.username" ng-change="chkusername(user.username)" required="required" >
          </div>
          <div class="list-group-item">
            <input type="password" id="password" name="" class="form-control no-border" ng-model="user.password" required="required" >
          </div>
        </div>
        <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="login()" ng-disabled='form.$invalid'>Log in</button>
        <div class="text-center m-t m-b"><a ui-sref="access.forgotpwd">Forgot password?</a></div>
        <div class="line line-dashed"></div>
        <p class="text-center"><small>Only the SuperAgent can access beyond this point.</small></p>
      </form>
    </div>
    <div class="text-center" ng-include="'tpl/blocks/page_footer.html'"></div>
  </div>
  <div >
  </div>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>



  <script>
    var app = angular.module("myApp", [])
    .config(function ($interpolateProvider){
     $interpolateProvider.startSymbol('{[{');
     $interpolateProvider.endSymbol('}]}');
    })


    .controller("myCtrl", ['$scope','$http',  function ($scope,$http) {
    // This directive definition throws unknown provider, because myCoolService
    // has been destroyed.
    }]);


  //////////////////////////LOGIN FACTORY
  app.factory('Login', function($http) {

  });
  //////USERFACTORY



  //   app.controller("myCtrl", function($scope,$http,User,Login) {
  //   //   $scope.submitData = function(user){
  //   //    $scope.alerts = [];
  //   //    $scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
  //   //    var alertme = function(){
  //   //      $scope.alerts.push({type: 'success', msg: 'Invalid Username/Password'});
  //   //    }
  //   //    $http({
  //   //     url: "http://bnbapi/user/login",
  //   //     method: "POST",
  //   //     headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  //   //     data: $.param(user)
  //   //   }).success(function (data, status, headers, config) {
  //   //    if(data.error){
  //   //     $scope.showerror = true;
  //   //   }else{
  //   //     $scope.showerror = false;
  //   //     console.log(data);
  //   //   }
  //   // })
  //   // }
  // });
</script>
</body>
</html>
