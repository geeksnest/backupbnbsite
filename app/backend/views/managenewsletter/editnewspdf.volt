{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="newsletterimagelist.html">
  <div ng-include="'/be/tpl/newsletterimagelist.html'"></div>
</script>
<script type="text/ng-template" id="newsletterpdflist.html">
  <div ng-include="'/be/tpl/newsletterpdflist.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'/be/tpl/delete.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit News Letter</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formpage" ng-submit="saveNews(news)">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-8">
          <div class="panel panel-default">

            <div class="panel-heading font-bold">
              News Letter Information <span class="label bg-danger">{[{ savecheck }]}</span>
            </div>

              <div class="panel-body">

                Title
                <input type="hidden" ng-model="news.id">
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.title" required="required" ng-change="onnewstitle(news.title)">
                <div class="popOver" ng-show="invalidtitle">
                    Title is already taken.
                    <span class="pop-triangle"></span>
                </div>
                <br>

                <div class="line line-dashed b-b line-lg"></div>

                <div class="panel-heading font-bold">
                  Date Published
                </div>
                <div class="col-sm-12"> 
                  <input type="hidden" required="required" ng-value="news.hiddendate = news.datepublished" ng-model="news.hiddendate" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" required="required">
                  <div class="input-group w-md">
                    <span class="input-group-btn">
                      <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="news.datepublished" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                      <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg"></div>

                <div class="form-group">
                Upload 
                <div class="input-group m-b">
                  <span class="input-group-btn">
                  <a class="btn btn-default"  ng-click="showpdfList('lg')">Select File</a>
                  </span>
                  <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">
                  <input type="text" class="form-control" ng-value="news.pdf = pdfamazonpath" ng-model="news.pdf" placeholder="{[{pdfamazonpath}]}" readonly required>
                </div>
                </div>
             
              </div> <!-- END OF panel Body-->


          </div>
        </div>


        <!--   ///////////////////////////////////////////////////////      -->
        <div class="col-sm-4">
          <div class="panel panel-default"> 
            <div class="panel-heading font-bold">
              Thumbnail
            </div>
            <div class="panel-body">
              <div class="form-group">
                <div class="input-group m-b">
                  <span class="input-group-btn">
                    <a class="btn btn-default"  ng-click="showimageList('lg')">Select Image</a>
                  </span>
                  <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon">
                  <input type="text" class="form-control" ng-value="news.image = amazonpath " ng-model="news.image" placeholder="{[{amazonpath}]}" readonly required>
                </div>
                <div class="col-sm-12">
                  <br><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/newsletter/{[{news.image}]}" width="100%" height="100%" alt="IMAGE PREVIEW">
                </div>
              </div>
            </div>
          </div>
        </div> 
        <!--   ///////////////////////////////////////////////////////      -->

<!--         <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Featured
            </div>
            <div class="panel-body" ng-init="news.featurednews=0">
              <div class="form-group" >
                  <label class="col-sm-3 control-label">
                    <span class="label bg-success" ng-show="news.featurednews == 1">Yes</span>
                    <span class="label bg-dark" ng-show="news.featurednews == 0">No</span>
                  </label>

                  <label class="i-switch bg-info l-t-xs l-r" style="margin-top:1px">
                    <input type="checkbox" ng-true-value="'1'" ng-model="news.featurednews" ng-false-value="'0'">
                    <i></i>
                  </label>
              </div>
            </div>
          </div>
        </div> -->

<!--         <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              News Preview
            </div>
            <div class="panel-body" ng-init="news.status=1">
              <div class="form-group" >
                  <button type="button" class="btn btn-primary btn-md" ng-disabled="formpage.$invalid || formpage.$pending || invalidtitle==true" ng-click="previewNews(news)">Live Preview</button>
              </div>
            </div>
          </div>
        </div> -->
        <div class="col-sm-4 pull-right">
          <div class="panel panel-default">
            <div class="form-group">
             <div class="panel-heading font-bold">
              Status
            </div>
            <div class="col-sm-12"> 
             <div class="radio">
              <label class="i-checks">
                <input type="radio" name="a" ng-model="news.status" required="required" value="1"><i></i>PUBLISHED
              </label>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <label class="i-checks">
              <input type="radio" name="a" ng-model="news.status" required="required" value="0"><i></i>UNPUBLISHED
            </label>
          </div>
        </div>
        <div class="line line-dashed b-b line-lg"></div>
        <div class="row pull-right">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
               <a ui-sref="managenewsletter" class="btn btn-default">CANCEL</a>
              <button type="submit" class="btn btn-success" ng-disabled="formpage.$invalid || formpage.$pending || invalidtitle==true" scroll-to="Scrollup">Submit</button>
            </footer>
        </div>
      </div>
      </div>
    </div>
  </div>
</form>