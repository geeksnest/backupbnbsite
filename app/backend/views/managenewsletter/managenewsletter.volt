{{ content() }}

<script type="text/ng-template" id="deletenewsletter.html">
  <div ng-include="'/be/tpl/deletenewsletter.html'"></div>
</script>

<script type="text/ng-template" id="newsletterEdit.html">
  <div ng-include="'/be/tpl/newsletterEdit.html'"></div>
</script>


<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">News Letter List</h1>
  <a id="top"></a>
</div>

<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">

        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Manage New Letter
            </div>


              <div class="panel-body">



                <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs">
                        <form name="rEquired">
                        <div class="input-group">
                          <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required>
                          <span class="input-group-btn">
                          <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)" ng-disabled="rEquired.$invalid">Go!</button>
                            <button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
                          </span>
                        </div>
                      </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                               
                                <th style="width:25%">Title</th>
                                <th style="width:25%">Date Published</th>
                                <th style="width:25%">Status</th>
                                <th style="width:25%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="mem in data.data">
                               
                                <td>{[{ mem.title }]}</td>
                                <td>{[{ mem.datepublished }]}</td>


                                <td  ng-if="mem.status == 1 && mem.type == 'pdf'">
                                  <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Published</span></div>
                                  <div class="checkstatuscontent">
                                    <label class="i-switch bg-info m-t-xs m-r">
                                      <input type="checkbox" checked="" ng-click="setstatus(mem.status,mem.id)">
                                      <i></i>
                                    </label>

                                  </div>
                                </td>
                                <td  ng-if="mem.status == 0 && mem.type == 'pdf'">
                                  <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Unpublished</span></div>
                                  <div class="checkstatuscontent">
                                    <label class="i-switch bg-primary m-t-xs m-r">
                                      <input type="checkbox" ng-click="setstatus(mem.status,mem.id)">
                                      <i></i>
                                    </label>
                                  </div>
                                </td>

                                <td  ng-if="mem.status == 1 && mem.type == 'html'">
                                  <div class="pagestatuscontent fade-in-out"><span class="label bg-success" >Published</span></div>
                                  <div class="checkstatuscontent">
                                    <label class="i-switch bg-info m-t-xs m-r">
                                    <input type="checkbox" checked="" ng-click="setstatus1(mem.status,mem.id)">
                                      <i></i>
                                    </label>

                                  </div>
                                </td>
                                <td  ng-if="mem.status == 0 && mem.type == 'html'">
                                  <div class="pagestatuscontent fade-in-out"><span class="label bg-danger">Unpublished</span></div>
                                  <div class="checkstatuscontent">
                                    <label class="i-switch bg-primary m-t-xs m-r">
                                      <input type="checkbox" ng-click="setstatus1(mem.status,mem.id)">
                                      <i></i>
                                    </label>
                                  </div>
                                </td>




                              <td ng-if="mem.type == 'pdf'">
                                <a href="" ng-click="editnewspdf(mem.id)"><span class="label bg-warning" >Edit</span></a>
                                <a href="" ng-click="deletepdf(mem.id)"> <span class="label bg-danger">Delete</span></a>
                              </td>
                              <td ng-if="mem.type == 'html'">
                                <a href="" ng-click="editnewsletter(mem.id)"><span class="label bg-warning" >Edit</span></a>
                                <a href="" ng-click="deletenews(mem.id)"> <span class="label bg-danger">Delete</span></a>
                              </td>
                            </tr>
                        </tbody>
                    </table>
                </div>



              </div>


          </div>
        </div>

      </div>

      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
</fieldset>