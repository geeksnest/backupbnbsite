{{ content() }}
<script type="text/ng-template" id="success.html">
  <div ng-include="'/be/tpl/success.html'"></div>
</script>
<script type="text/ng-template" id="deletesubscriber.html">
  <div ng-include="'/be/tpl/deletesubscriber.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Subscribers</h1>
  <a id="top"></a>
</div>

<div class='contrainer'>
	<div class='row wrapper-md'>
		<div class="col-sm-12">
          	 	<div class="panel b-a">
          	 		<div class="panel-heading b-b b-light">
          	 			<strong>Add Subscriber</strong>
          	 		</div>
          	 		<div class="panel-body">
          	 			<form name="Subscribersform" class="form-validation ng-pristine ng-invalid ng-invalid-required">
	          			<div class="col-sm-6">
										<div class="form-group">
											<label class="col-md-3 control-label">Subscriber Name</label>
											<div class="col-md-9">
												<input type="text" class="form-control subscribername" placeholder="Name" ng-model="subscriber.name" required>
											</div>
										</div>

										<div class="line line-lg"></div>

										<div class="form-group">
											<label class="col-md-3 control-label">Email</label>
											<div class="col-md-9">
												<input type="email" class="form-control subscriberemail" placeholder="Email" ng-model="subscriber.email" ng-change="checkemail(subscriber.email)" required>
												<div class="popOver" ng-show="invalidemail" >
													Email is already exist.
													<span class="pop-triangle"></span>
												</div>
											</div>
										</div>

										<div class="line line-lg"></div>
										<div class="form-group">
											<span class="pull-left col-md-9">
												<button class="btn btn-md btn-danger" ng-click="clearfields()" type="reset">Clear</button>
												<button type="submit" class="btn btn-md btn-success" ng-disabled="Subscribersform.$invalid || invalidemail==true || Subscribersform.$pending" 
												ng-click="addsubscriber(subscriber);
												">Save</button>
											</span>
										</div>
									</div>
			          </form>
          	 		</div>
          	 	</div>
          	 	<!-- END OF ADD PANEL -->


          	 	<div class="panel b-a">
    					<div class="panel-heading b-b b-light">
    						<strong>Subscribers List</strong>
    					</div>
    					<div class="col-sm-12">
          	 		<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
          	 	</div>
    				<div class="panel-body">
    						<div class="row">
    							<div class="col-sm-5">
    								<form name="rEquired">
    									<div class="input-group">
    										<span class="input-group-btn">
    											<button class="btn btn-sm btn-default" type="button" ng-click="clearsearch();searchtext='';CurrentPage =1">Clear</button>
    										</span>
    										<input class="input-sm form-control searchtext" placeholder="Search" type="text" ng-model="searchtext" required>
    										<span class="input-group-btn">
    											<button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext);CurrentPage =1" ng-disabled="rEquired.$invalid">Go!</button>
    										</span>
    									</div>
    								</form>
    							</div>
    						</div>

    						<div class="row">
									<div class="table-responsive">
										<table class="table table-striped b-t b-light">
											<thead>
												<tr>
													<th >Name</th>
													<th>Email</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-if="TotalItems == 0">
													<td colspan="8">No Subscribers to show</td>
												</tr>
												<tr ng-repeat="data in data.data">
													<td>{[{data.sname}]}</td>
													<td>{[{data.semail}]}</td>
													<td>
														<a href="" ng-click="delete(data.id)">
															<span class="label bg-danger">Remove</span>
														</a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
							</div>
							<div class="panel-footer">
								<pagination total-items="TotalItems" ng-model="CurrentPage" max-size="maxSize1" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(CurrentPage)"></pagination>
							</div>
    				</div>
    			</div>
    			
    </div>
	</div>
</div>