
<?php
$role = $username['pi_userrole'];
$userid = $username['bnb_userid'];
$fullname = $username['pi_fullname'];
if($role == 'Center Manager') { $centerid = $username['bnb_centerid']; }
?>
<!DOCTYPE html>
<html lang="en" data-ng-app="app" ng-cloak>
<head>
  <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
  <META HTTP-EQUIV="EXPIRES" CONTENT="0">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="user-scalable = yes" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  {{ get_title() }}
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  {#<link type="text/css" rel="stylesheet/less" href="/" />#}
  {#<link rel="stylesheet/less" type="text/css" href="/be/css/design.less">
  {{ tag_html("link", ["rel":"stylesheet/less", "type":"text/css", "href":"/be/css/design.less"]) }}#}

  {{ stylesheet_link('be/css/bootstrap.css') }}
  {{ stylesheet_link('be/css/animate.css') }}
  {{ stylesheet_link('vendors/font-awesome/css/font-awesome.min.css') }}
  {{ stylesheet_link('be/css/simple-line-icons.css') }}
  {{ stylesheet_link('be/css/font.css') }}
  {{ stylesheet_link('be/css/app.css') }}
  {{ stylesheet_link('be/css/custom.css') }}
  {{ stylesheet_link('be/js/angularjs-toaster/toaster.css') }}
  {{ stylesheet_link('be/css/wobblebar.css') }}

  {{ stylesheet_link('vendors/angular-xeditable/dist/css/xeditable.css') }}
  {{ stylesheet_link('vendors/ngImgCrop/compile/unminified/ng-img-crop.css') }}
  {{ stylesheet_link('vendors/angular-xeditable/dist/css/xeditable.css') }}
  {{ stylesheet_link('be/js/jquery/chosen/chosen.css') }}

  {{ stylesheet_link('vendors/ui-select/dist/select.css') }}

  {{ stylesheet_link('vendors/angular-bootstrap-colorpicker/css/colorpicker.min.css') }}
  {{ stylesheet_link('vendors/ng-tags-input/ng-tags-input.min.css') }}
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]-->

  <link rel="shortcut icon" href="/img/smalllogo.png">
  <script>
    var userid = "<?php echo $username['bnb_userid']; ?>";
  </script>
</head>
<body ng-controller="AppCtrl">

{{ content() }}
<toaster-container toaster-options="{'time-out': 6000, 'close-button':true, 'position-class': 'toast-bottom-left'}"></toaster-container>
<div class="app" id="app" ng-class="{'app-header-fixed':app.settings.headerFixed, 'app-aside-fixed':app.settings.asideFixed, 'app-aside-folded':app.settings.asideFolded}">
    <div class="app-header navbar">
      <!-- navbar header -->
      <div class="navbar-header bg-white">
        <button class="pull-right visible-xs dk" ui-toggle-class="show" data-target=".navbar-collapse">
          <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle-class="off-screen" data-target=".app-aside" ui-scroll="app">
          <i class="glyphicon glyphicon-align-justify"></i>
        </button>
        <!-- brand -->
        <a href="#/" class="navbar-brand text-lt">
          <img src="/img/bnblogomenu.gif" alt=".">
          <span class="hidden-folded m-l-xs" ng-bind="app.name"></span>
        </a>
        <!-- / brand -->
      </div>
      <!-- / navbar header -->

      <!-- navbar collapse -->
      <div class="collapse navbar-collapse box-shadow {[{app.settings.navbarCollapseColor}]}">
        <!-- buttons -->
        <div class="nav navbar-nav m-l-sm hidden-xs">
          <a href class="btn no-shadow navbar-btn" ng-click="app.settings.asideFolded = !app.settings.asideFolded">
            <i class="fa {[{app.settings.asideFolded ? 'fa-indent' : 'fa-dedent'}]} fa-fw"></i>
          </a>
          <a href class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
            <i class="icon-user fa-fw"></i>
          </a>
        </div>
        <!-- / buttons -->

        <!-- link and dropdown -->

    <?php if ($role == 'Administrator') { ?>
        <ul class="nav navbar-nav hidden-sm">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
              <span translate="header.navbar.new.NEW">New</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li>
                <a ui-sref="createcenter">
                  <span>Center</span>
                </a>
              </li>
              <li>
                <a ui-sref="createnews">
                  <span translate="header.navbar.new.NEWS">News</span>
                </a>
              </li>
              <li>
                <a ui-sref="createpage">
                  <span translate="header.navbar.new.PAGE">Page</span>
                </a>
              </li>
              <li>
                <a ui-sref="userscreate">
                  <span translate="header.navbar.new.USER">User</span>
                </a>
              </li>
            </ul>
          </li>
        </ul>
  <?php } ?>
        <!-- / link and dropdown -->

        <!-- search form -->
        <form class="navbar-form navbar-form-sm navbar-left shift" ui-shift="prependTo" target=".navbar-collapse" role="search" ng-controller="TypeaheadDemoCtrl">
          <div class="form-group">
            <div class="input-group">
              <input type="text" ng-model="selected" typeahead="state for state in states | filter:$viewValue | limitTo:8" class="form-control input-sm bg-light no-border rounded padder" placeholder="Search projects...">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-sm bg-light rounded"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </div>
        </form>
        <!-- / search form -->

        <!-- nabar right -->

        <ul class="nav navbar-nav navbar-right" ng-controller="notificationCtrl" ng-init="userid('<?php echo $username["bnb_userid"];?>', '<?php echo $username['token']; ?>')">

          <li class="hidden-xs">
            <a ui-fullscreen></a>
          </li>
          <li class="dropdown">
            <a href class="dropdown-toggle" ng-click="removeinfi()">
              <i class="icon-bell fa-fw"></i>
              <span class="visible-xs-inline">Notifications</span>
              <span class="badge badge-sm up bg-danger pull-right-xs nifiicon bounce animated1 infinite" ng-show="nofiticationlistcount != 0" ng-bind="nofiticationlistcount"></span>
            </a>
            <!-- dropdown -->
            <div class="dropdown-menu w-xl animated fadeInUp">
              <div class="panel bg-white">
                <div class="panel-heading b-light bg-light">
                  <!-- <strong ng-if="nofiticationlistcount != 0" >You have <span>{[{nofiticationlistcount}]}</span> unread notifications</strong> -->
                  <strong>Notifications</strong>
                </div>
                <div id="infinitescrolldiv" class="list-group" ui-jq="slimScroll" ui-options="{height:'410px', size:'8px'}">
                  <div ng-repeat="list in nofiticationlist">
                    <a ng-if="list.notificationstatus == 0" href class="media list-group-item bg-light" ng-click="viewnotification(list.notificationid,list.itemid,list.type,list.centerid,list[0].uigo)">
                      <!-- <i class="fa fa-circle text-warning pull-right text-xs m-t-sm"></i> -->
                      <i class="fa fa-circle pull-right text-warning"></i>
                      <span class="pull-left thumb-sm" ng-if="list.type == 'introsession'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-user-follow font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'introsession'">
                        {[{list.name}]}<br>
                        Enrolled to 1-on-1 Intro Session<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of introsession -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'groupsession'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-users font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'groupsession'">
                        {[{list.name}]}<br>
                        Enrolled to 1 Group Class + 1-on-1 Intro Session<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of groupsession -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'beneplace'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-paper-plane font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'beneplace'">
                        {[{list.name}]}<br>
                        Enrolled to Beneplace<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of beneplace -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'message'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-envelope font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'message'">
                        {[{list.name}]}<br>
                        Sent you a new message<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of message -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'workshop'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-users font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'workshop'">
                        {[{list.name}]}<br>
                        Enrolled to Workshop<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of workshop -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'story'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-bubbles font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'story'">
                        {[{list.name}]}<br>
                        Sent you Testimonies<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of story -->
                    </a>

                    <a ng-if="list.notificationstatus == 1" href class="media list-group-item" ng-click="viewnotification(list.notificationid,list.itemid,list.type,list.centerid,list[0].uigo)">

                      <span class="pull-left thumb-sm" ng-if="list.type == 'introsession'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-user-follow font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'introsession'">
                        {[{list.name}]}<br>
                        Enrolled to 1-on-1 Intro Session<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of introsession -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'groupsession'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-users font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'groupsession'">
                        {[{list.name}]}<br>
                        Enrolled to 1 Group Class + 1-on-1 Intro Session<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of groupsession -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'beneplace'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-paper-plane font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'beneplace'">
                        {[{list.name}]}<br>
                        Enrolled to Beneplace<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of beneplace -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'message'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-envelope font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'message'">
                        {[{list.name}]}<br>
                        Sent you a new message<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of message -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'workshop'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-users font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'workshop'">
                        {[{list.name}]}<br>
                        Enrolled to Workshop<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of workshop -->

                      <span class="pull-left thumb-sm" ng-if="list.type == 'story'">
                        <!-- <img src="/img/a0.jpg" alt="..." class="img-circle"> -->
                        <i class="icon-bubbles font30"></i>
                      </span>
                      <span class="media-body block m-b-none" ng-if="list.type == 'story'">
                        {[{list.name}]}<br>
                        Sent you Testimonies<br>
                        <i class="fa fa-clock-o"></i> <small class="text-muted">{[{list[0].timeago}]}</small>
                      </span>
                      <!-- end of story -->
                    </a>
                  </div>
                  <a ng-click="incrementLimit()" href class="media bg-light" style="height:20px;padding-top:1px;overflow:hidden;">
                  <center>
                    <span style="padding:5px;" ng-show="notiloadertext">loadmore...</span>
                    <span class="ouro ouro2" style="padding-top:1px;overflow:hidden;" ng-show="notiloader"> <!--  -->
                      <span class="left"><span class="anim"></span></span>
                      <span class="right"><span class="anim"></span></span>
                    </span>
                  </center>
                  </a>
                </div>
                <div class="panel-footer text-sm">
                  <a href  class="pull-right"><i class="fa fa-cog"></i></a>
                  <a href ui-sref="managenotification" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                </div>
              </div>
            </div>
            <!-- / dropdown -->
          </li>
          <li class="dropdown">
            <a href class="dropdown-toggle clear" data-toggle="dropdown">
              <span class="thumb-sm avatar m-t-n-sm m-b-n-sm m-l-sm">
                <img src="https://bodynbrain.s3.amazonaws.com/uploads/userimages/<?php echo $username['pi_profile_pic_name']; ?>" alt="...">
              </span>
              <span class="hidden-sm hidden-md"><?php echo $username['pi_username']; ?></span> <b class="caret"></b>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu animated fadeInRight w">
              <li class="wrapper b-b m-b-sm bg-light m-t-n-xs">
                <div>
                  <p>300mb of 500mb used</p>
                </div>
                <progressbar value="60" class="progress-xs m-b-none bg-white"></progressbar>
              </li>
              <li>
                <a href>
                  <span class="badge bg-danger pull-right">30%</span>
                  <span>Settings</span>
                </a>
              </li>
              <li>
                <a ui-sref="editprofile">Profile</a>
              </li>
              <li>
                <a ui-sref="changepassword({userid: '<?php echo $username["bnb_userid"];?>'})">
                  <span class="label bg-info pull-right"></span>
                  Change Password
                </a>
              </li>
              <li>
                <a ui-sref="app.docs">
                  <span class="label bg-info pull-right">new</span>
                  Help
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="/bnbadmin/admin/logout">Logout <?php echo $role; ?></a>
              </li>
            </ul>
            <!-- / dropdown -->
          </li>

        </ul>
        <!-- / navbar right -->



      </div>
      <!-- / navbar collapse -->
      </div>

  <!-- menu -->
  <div class="app-aside hidden-xs {[{app.settings.asideColor}]}">
<div class="aside-wrap">
  <div class="navi-wrap">
    <!-- user -->
    <div class="clearfix hidden-xs text-center hide" id="aside-user">
      <div class="dropdown wrapper">
        <a ui-sref="editprofile">
          <span class="thumb-lg w-auto-folded avatar m-t-sm">
            <img src="https://bodynbrain.s3.amazonaws.com/uploads/userimages/<?php echo $username['pi_profile_pic_name']; ?>" alt="..." class="img-full">
          </span>
        </a>
        <a href class="dropdown-toggle hidden-folded">
          <span class="clear">
            <span class="block m-t-sm">
              <strong class="font-bold text-lt"><?php echo $fullname; ?></strong>
              <b class="caret"></b>
            </span>
            <span class="text-muted text-xs block"><?php echo $role; ?></span>
          </span>
        </a>
        <!-- dropdown -->
        <ul class="dropdown-menu animated fadeInRight w hidden-folded">
          <li class="wrapper b-b m-b-sm bg-info m-t-n-xs">
            <span class="arrow top hidden-folded arrow-info"></span>
            <div>
              <p>300mb of 500mb used</p>
            </div>
            <progressbar value="60" type="white" class="progress-xs m-b-none dker"></progressbar>
          </li>
          <li>
            <a href>Settings</a>
          </li>
          <li>
            <a ui-sref="editprofile">Profile</a>
          </li>
          <li>
            <a href>
              <span class="badge bg-danger pull-right">3</span>
              Notifications
            </a>
          </li>
          <li class="divider"></li>
          <li>
            <a ui-sref="logout"> Logout</a>
          </li>
        </ul>
        <!-- / dropdown -->
      </div>
      <div class="line dk hidden-folded"></div>
    </div>
    <!-- / user -->

    <!-- nav -->
    <nav ui-nav class="navi">
<!-- first -->
<ul class="nav" id="sidenav">
  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span translate="aside.nav.HEADER">Navigation</span>
  </li>
  <li>
    <a ui-sref="dashboard">
      <i class="glyphicon glyphicon-stats icon text-primary-dker"></i>
      <span class="font-bold" translate="aside.nav.DASHBOARD">Dashboard</span>
    </a>
  </li>
  <?php if ($role == 'Administrator') { ?>
  <li ng-class="{active:$state.includes('app.users')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-user icon"></i>
      <span class="font-bold" translate="aside.nav.users.USERS">Users</span>
<?php } ?>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="userscreate">
          <span>Create User</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userlist">
          <span>List of Users</span>
        </a>
      </li>
    </ul>
  </li>
  <li ng-class="{active:$state.includes('app.center')}">
<?php if ($role == 'Administrator') { ?>
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="fa fa-building icon"></i>
      <span class='font-bold' translate='aside.nav.center.CENTER'>Center</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="createcenter">
          <span>Create Center</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="managecenter({userid: '<?php echo $username["bnb_userid"];?>'})">
          <span>List of Centers</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="regiondistrict">
          <span>Regions</span>
        </a>
      </li>
    <!--   <li ui-sref-active="active">
        <a ui-sref="globalpricing">
          <span translate="aside.nav.center.SPRICING">SPRICING</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="beneplacepricing">
          <span translate="aside.nav.center.BPRICING">BPRICING</span>
        </a>
      </li> -->
    </ul>
<?php } elseif ($role == 'Center Manager') { ?>
  <li>
      <a href class="auto" ui-sref="mycenter({centerid: '<?php echo $username["bnb_centerid"];?>'})">
        <span class="pull-right text-muted">
          <i class="fa fa-fw fa-angle-down text-active"></i>
        </span>
        <i class="glyphicon glyphicon-home icon"></i>
        <span class='font-bold'>My Center</span>
      </a>
  </li>
  <li>
      <a href class="auto" ui-sref="centerview({centerid: '<?php echo $username["bnb_centerid"];?>', uifrom: 'none'})">
        <span class="pull-right text-muted">
          <i class="fa fa-fw fa-angle-down text-active"></i>
        </span>
        <i class="fa fa-folder icon"></i>
        <span class='font-bold'>Manage My Center</span>
      </a>
  </li>
<?php } elseif ($role == 'Region Manager' || $role == 'District Manager') { ?>
  <li>
        <a href class="auto" ui-sref="managecenter({userid: '<?php echo $username["bnb_userid"];?>'})">
          <span class="pull-right text-muted">
            <i class="fa fa-fw fa-angle-down text-active"></i>
          </span>
          <i class="glyphicon glyphicon-inbox icon"></i>
          <span class='font-bold'>My Center</span>
        </a>
  </li>
<?php } ?>

  <?php if ($role == 'Administrator') { ?>

    <li ng-class="{active:$state.includes('app.pages')}">
      <a href class="auto">
        <span class="pull-right text-muted">
          <i class="fa fa-fw fa-angle-right text"></i>
          <i class="fa fa-fw fa-angle-down text-active"></i>
        </span>
        <i class="fa fa-file-text icon"></i>
        <span class="font-bold" translate="aside.nav.pages.PAGES">Pages</span>
      </a>
      <ul class="nav nav-sub dk">
        <li ui-sref-active="active">
          <a ui-sref="createpage">
            <span>Create Page</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="managepage">
            <span>List of Pages</span>
          </a>
        </li>
      </ul>
    </li>

    <li ui-sref-active="active">
      <a ui-sref="bnbslider">
       <i class="fa fa-image icon"></i>
        <span class="font-bold" translate="aside.nav.slider.BNBSLIDER">Banners & Sliders</span>
      </a>
    </li>

    <li ui-sref-active="active">
      <a ui-sref="manageclass">
         <i class="glyphicon glyphicon-calendar"></i>
        <span class="font-bold">Classes</span>
      </a>
    </li>
  <?php } ?>

  <?php if ($role == 'Administrator' || $role == 'Editor') { ?>
    <li class="line dk"></li>

    <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
      <span>Editor's Corner</span>
    </li>

    <li ng-class="{active:$state.includes('app.news')}">
      <a href class="auto">
        <span class="pull-right text-muted">
          <i class="fa fa-fw fa-angle-right text"></i>
          <i class="fa fa-fw fa-angle-down text-active"></i>
        </span>
        <i class="glyphicon glyphicon-edit icon"></i>
        <span class="font-bold" translate="aside.nav.news.NEWS">News</span>
      </a>
      <ul class="nav nav-sub dk">
        <li ui-sref-active="active">
          <a ui-sref="createnews">
            <span>Create News</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="managenews({ page : 1 })">
            <span>List of News</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="createcategory">
            <span>News Category</span>
          </a>
        </li>
      </ul>
    </li>

    <li ng-class="{active:$state.includes('app.news')}">
      <a href class="auto">
        <span class="pull-right text-muted">
          <i class="fa fa-fw fa-angle-right text"></i>
          <i class="fa fa-fw fa-angle-down text-active"></i>
        </span>
        <i class="fa fa-bullhorn icon"></i>
        <span class="font-bold" translate="aside.nav.press.PRESS">Press News</span>
      </a>
      <ul class="nav nav-sub dk">
        <li ui-sref-active="active">
          <a ui-sref="create_presspage">
            <span>Create Press News</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="manage_presspage">
            <span>List of Press News</span>
          </a>
        </li>

      </ul>
    </li>

      <li ui-sref-active="active">
        <a ui-sref="managestories">
         <i class="fa fa-wechat icon"></i>
          <span class="font-bold">Testimonials</span>
        </a>
      </li>

      <li ng-class="{active:$state.includes('app.pages')}">
        <a href class="auto">
          <span class="pull-right text-muted">
            <i class="fa fa-fw fa-angle-right text"></i>
            <i class="fa fa-fw fa-angle-down text-active"></i>
          </span>
          <i class="glyphicon glyphicon-envelope icon"></i>
           <span class="font-bold">Newsletter</span>
        </a>
        <ul class="nav nav-sub dk">
          <li ui-sref-active="active">
            <a ui-sref="createnewsletter">
              <span>Create Newsletter</span>
            </a>
          </li>
          <li ui-sref-active="active">
            <a ui-sref="managenewsletter">
              <span>List of Newsletter</span>
            </a>
          </li>
        </ul>
      </li>

      <li ui-sref-active="active">
        <a ui-sref="managecontacts">
           <i class="glyphicon  glyphicon-phone-alt icon"></i>
          <span class="font-bold" translate="aside.nav.contacts.MANAGE_CONTACTS">Manage Contacts</span>
        </a>
      </li>
  <?php } ?>

  <li class="line dk"></li>

  <?php if ($role == 'Administrator') { ?>
  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span>Transactions</span>
  </li>

  <li ui-sref-active="active">
    <a ui-sref="allintrosession({userid: '<?php echo $username["bnb_userid"];?>'})">
       <i class="icon-user-follow icon"></i>
      <span class="font-bold" translate="aside.1ON1">1ON1</span>
    </a>
  </li>
  <li ui-sref-active="active">
    <a ui-sref="allgroupclasssession({userid: '<?php echo $username["bnb_userid"];?>'})">
       <i class="icon-users icon"></i>
      <span class="font-bold" translate="aside.1GRP">1GRP</span>
    </a>
  </li>
  <li ui-sref-active="active">
    <a ui-sref="allbeneplace({userid: '<?php echo $username["bnb_userid"];?>'})">
       <i class="icon-paper-plane"></i>
      <span class="font-bold" translate="aside.BENEPLACE">BENEPLACE</span>
    </a>
  </li>

  <li>
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="fa fa-cubes"></i>
      <span class="font-bold" translate="aside.nav.workshop.WORKSHOP">Workshop</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="workshoptitles">
          <span>Workshop Titles</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="workshop_create_venue">
          <span>Create Venue</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="workshop_manage_venue">
          <span>List of Venues</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="create_workshop">
          <span>Create Workshop</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="manage_workshops">
          <span>List of Workshops</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="workshoprelated">
          <span translate="aside.nav.workshop.WORKSHOP_RELATED">Workshop Related</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="workshopregistrants">
          <span translate="aside.nav.workshop.REGISTRANTS">Registrants</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="workshopsettings">
          <span>Settings</span>
        </a>
      </li>
    </ul>
  </li>

  <li ng-class="{active:$state.includes('app.income')}">
    <a class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="fa fa-bar-chart-o"></i>
      <span class="font-bold">Center Income</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="centerincome">
          <span translate="aside.nav.centerincome.VIEW">Vision</span>
        </a>
      </li>
    </ul>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="centerincome_franchise">
          <span translate="aside.nav.centerincome.FRANCHISE">Franchised</span>
        </a>
      </li>
    </ul>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="centerincome_regions">
          <span>Regions</span>
        </a>
      </li>
    </ul>
  </li>
  <?php } ?>

  <li ng-class="{active:$state.includes('app.center')}">
<?php if ($role == 'Administrator') { ?>
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="fa fa-usd"></i>
      <span class='font-bold' translate='aside.nav.center.GPRICING'>Global Pricing</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="globalpricing">
          <span translate="aside.nav.center.SPRICING">SPRICING</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="beneplacepricing">
          <span translate="aside.nav.center.BPRICING">BPRICING</span>
        </a>
      </li>
    </ul>
  </li>
<?php } ?>

  <!-- <li class="line dk hidden-folded"></li>

  <li ng-class="{active:$state.includes('app.product')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="icon-diamond"></i>
      <span class="font-bold" translate="aside.nav.product.PRODUCT">Product</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="addproduct">
          <span translate="aside.nav.product.ADD">Add Product/Inventory</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="manageproduct">
          <span translate="aside.nav.product.MANAGE">Manage Product</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="manageproductcategory">
          <span translate="aside.nav.product.MANAGECATEGORY">Manage Category</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="manageproductsubcategory">
          <span translate="aside.nav.product.MANAGESUBCATEGORY">Manage Sub Category</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="manageproducttype">
          <span translate="aside.nav.product.MANAGETYPE">Manage Type</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="manageproducttags">
          <span translate="aside.nav.product.MANAGETAGS">Manage Tags</span>
        </a>
      </li>
    </ul>
  </li>

  <li ng-class="{active:$state.includes('app.order')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="icon-diamond"></i>
      <span class="font-bold" translate="aside.nav.order.ORDER">Order</span>
    </a>
    <ul class="nav nav-sub dk">
      <?php if($role == "Administrator" || $role == 'Center Manager'){ ?>
        <li ui-sref-active="active">
          <a ui-sref="addorder">
            <span translate="aside.nav.order.ADD">Offline Order</span>
          </a>
        </li>
      <?php } ?>

      <?php if($role == "Administrator"){ ?>
      <li ui-sref-active="active">
        <a ui-sref="manageorder">
          <span translate="aside.nav.order.MANAGE">Manage Order</span>
        </a>
      </li>
      <?php } ?>
    </ul>
  </li>
  <li class="line dk hidden-folded"></li> -->
  <li class="line dk"></li>

  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span>Settings</span>
  </li>
  <li>
    <a ui-sref="editprofile">
      <i class="icon-user icon text-success-lter"></i>
      <span translate="aside.nav.your_stuff.PROFILE">Profile</span>
    </a>
  </li>
<?php if ($role == 'Administrator') { ?>
  <li>
    <a ui-sref="manageauditlog"> <!-- app.docs-->
      <i class="glyphicon glyphicon-tasks icon"></i>
      <span translate="aside.nav.your_stuff.LOGS">System Logs</span>
    </a>
  </li>
  <li>
    <a ui-sref="settings"> <!-- app.docs-->
      <i class="glyphicon glyphicon-cog icon"></i>
      <span translate="aside.nav.your_stuff.SETTINGS">Settings</span>
    </a>
  </li>
  {#<li>
    <a ui-sref="esignature"> <!-- app.docs-->
      <i class="glyphicon glyphicon-cog icon"></i>
      <span>Test</span>
    </a>
  </li>#}
<?php } ?>
</ul>
<!-- / third -->

    </nav>
    <!-- nav -->

  </div>
</div>
  </div>
  <!-- / menu -->
  <!-- content -->
  <div class="app-content">
    <div ui-butterbar></div>
    <a href class="off-screen-toggle hide" ui-toggle-class="off-screen" data-target=".app-aside" ></a>
    <div class="app-content-body fade-in-up" ui-view></div>

  </div>
  <!-- /content -->
  <!-- aside right -->
  <div class="app-aside-right pos-fix no-padder w-md w-auto-xs bg-white b-l animated fadeInRight hide">
    <div class="vbox">
      <div class="wrapper b-b b-light m-b">
        <a href class="pull-right text-muted text-md" ui-toggle-class="show" target=".app-aside-right"><i class="icon-close"></i></a>
        Chat
      </div>
      <div class="row-row">
        <div class="cell">
          <div class="cell-inner padder">
            <!-- chat list -->
            <div class="m-b">
              <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                  <span class="arrow left pull-up"></span>
                  <p class="m-b-none">Hi John, What's up...</p>
                </div>
                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i> 2 minutes ago</small>
              </div>
            </div>
            <div class="m-b">
              <a href class="pull-right thumb-xs avatar"><img src="/img/a3.jpg" class="img-circle" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm bg-light r m-r-sm">
                  <span class="arrow right pull-up arrow-light"></span>
                  <p class="m-b-none">Lorem ipsum dolor :)</p>
                </div>
                <small class="text-muted">1 minutes ago</small>
              </div>
            </div>
            <div class="m-b">
              <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                  <span class="arrow left pull-up"></span>
                  <p class="m-b-none">Great!</p>
                </div>
                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i>Just Now</small>
              </div>
            </div>
            <!-- / chat list -->
          </div>
        </div>
      </div>
      <div class="wrapper m-t b-t b-light">
        <form class="m-b-none">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Say something">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">SEND</button>
            </span>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- / aside right -->

  <!-- footer -->
  <div class="app-footer wrapper b-t bg-light">
    <span class="pull-right"><img src="/img/minigeeksnestlogo.png" style="width:20px; height:20px;"/> <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
    &copy; 2015 Copyright.
  </div>
  <!-- / footer -->

</div>
<!-- JS -->

{{ javascript_include('vendors/jquery/dist/jquery.min.js') }}

{#<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>#}
{{ javascript_include('cdn/js/cdn.jquery-ui.min.js') }}

<!-- angular -->
{{ javascript_include('vendors/angular/angular.min.js') }}

<script type="text/javascript" src="/vendors/less/dist/less.min.js"></script>
{{ javascript_include('vendors/angular-jwt/dist/angular-jwt.min.js') }}
{{ javascript_include('vendors/angular-cookies/angular-cookies.min.js') }}
{{ javascript_include('vendors/angular-animate/angular-animate.min.js') }}
{{ javascript_include('be/js/angular/angular-ui-router.min.js') }}
{{ javascript_include('be/js/angular/angular-translate.js') }}
{{ javascript_include('be/js/angular/ngStorage.min.js') }}
{{ javascript_include('be/js/angular/ui-load.js') }}
{{ javascript_include('be/js/angular/ui-jq.js') }}
{{ javascript_include('be/js/angular/ui-validate.js') }}
{{ javascript_include('be/js/angular/ui-bootstrap-tpls.min.js') }}
{{ javascript_include('be/js/jquery/inputmask.js') }}
{{ javascript_include('vendors/moment/moment.js') }}
{{ javascript_include('vendors/angular-moment/angular-moment.min.js') }}
{{ javascript_include('vendors/angular-audio/app/angular.audio.js') }}
{{ javascript_include('be/js/angularjs-toaster/toaster.js') }}
{{ javascript_include('vendors/ng-tags-input/ng-tags-input.min.js') }}
{{ javascript_include('vendors/angular-vertilize/angular-vertilize.min.js') }}
{{ javascript_include('vendors/angular-xeditable/dist/js/xeditable.min.js') }}
{{ javascript_include('vendors/ng-file-upload/ng-file-upload.min.js') }}
{{ javascript_include('vendors/ng-file-upload/ng-file-upload-shim.js') }}
{{ javascript_include('vendors/angular-google-maps/dist/angular-google-maps.min.js') }}
{{ javascript_include('vendors/ngImgCrop/compile/unminified/ng-img-crop.js') }}
{{ javascript_include('vendors/angular-uuid-service/angular-uuid-service.min.js') }}
{{ javascript_include('vendors/ui-autocomplete/autocomplete.js') }}
{{ javascript_include('vendors/ui-select/dist/select.js') }}
{{ javascript_include('vendors/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.min.js') }}
{{ javascript_include('/vendors/angular-latest/src/ngSanitize/sanitize.js') }}
{{ javascript_include('vendors/a0-angular-storage/dist/angular-storage.min.js') }}
{{ javascript_include('vendors/angular-country-state-select/dist/angular-country-state-select.js') }}
{{ javascript_include('vendors/angular-chosen/angular-chosen.js') }}
{{ javascript_include('vendors/highcharts-ng/dist/highcharts-ng.min.js') }}
{{ javascript_include('be/js/jquery/highcharts.src.js') }}

<!-- APP -->
{{ javascript_include('be/js/scripts/app.js') }}
{{ javascript_include('be/js/scripts/factory/login.js') }}
{{ javascript_include('be/js/scripts/factory/factory.js') }}
{{ javascript_include('be/js/scripts/controllers/controllers.js') }}
{{ javascript_include('be/js/scripts/directives/directives.js') }}
{{ javascript_include('be/js/scripts/directives/showErrors.js') }}
{{ javascript_include('be/js/scripts/config.js') }}
{{ javascript_include('be/js/scripts/factory/universal.js') }}
{{ javascript_include('be/js/scripts/controllers/notification/notification.js') }}
{{ javascript_include('be/js/scripts/factory/notification/notification.js') }}
{{ javascript_include('be/js/scripts/factory/center/allintrosession.js') }}
{{ javascript_include('be/js/scripts/factory/center/allgroupclasssession.js') }}
{{ javascript_include('be/js/scripts/factory/center/allbeneplace.js') }}
{{ javascript_include('be/js/scripts/filter/htmlfilter.js') }}

<!-- CKeditor -->
{{ javascript_include('be/js/ckeditor/ckeditor.js') }}
{{ javascript_include('be/js/ckeditor/styles.js') }}

<!-- jsPDF -->
{{ javascript_include('be/js/jsPDF-master/jspdf.js') }}
{{ javascript_include('be/js/jquery/lodash/lodash.min.js') }}

{#<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js"></script>#}
{{ javascript_include('cdn/js/cdn.chosen.jquery.min.js') }}

{#<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script>#}
{{ javascript_include('cdn/js/cdn.imagesloaded.pkgd.min.js') }}

<script>
 function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>


<!-- neil scripts -->
<script>
$('.phone_us').mask('(000) 000-0000');
</script>

<script type="text/ng-template" id="myTemplateWithData.html">
<p class="toast-title">{[{toaster.data}]}</p>
<p>{[{toaster.data2}]}</p>
</script>

<script type="text/ng-template" id="beneplaceView.html">
  <div ng-include="'/be/tpl/beneplaceView.html'"></div>
</script>

<script type="text/ng-template" id="groupsessionView.html">
  <div ng-include="'/be/tpl/groupsessionView.html'"></div>
</script>

<script type="text/ng-template" id="sessionView.html">
  <div ng-include="'/be/tpl/sessionView.html'"></div>
</script>

<script type="text/ng-template" id="contactReview.html">
  <div ng-include="'/be/tpl/contactReview.html'"></div>
</script>
{#<script type="text/javascript" src="https://www.google.com/jsapi"></script>#}

<script>
(function(w,d,s,g,js,fjs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
  js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
}(window,document,'script'));
</script>

<script>
google.load("visualization", "1", {packages:["corechart"]});
gapi.analytics.ready(function() {

  // Step 3: Authorize the user.
  // Step 4: Create the view selector.

  var viewSelector = new gapi.analytics.ViewSelector({
    container: 'view-selector'
  });

  // Step 5: Create the timeline chart.

  var timeline = new gapi.analytics.googleCharts.DataChart({
    reportType: 'ga',
    query: {
      'dimensions': 'ga:date',
      'metrics': 'ga:sessions',
      'start-date': '30daysAgo',
      'end-date': 'yesterday',
    },
    chart: {
      type: 'LINE',
      container: 'timeline',
      options: {
        backgroundColor: 'transparent',
        width: '100%',
      }
    }
  });

  // Step 6: Hook up the components to work together.

  gapi.analytics.auth.on('success', function(response) {
    viewSelector.set().execute();
    function getData(response) {
      var jsonData = $.ajax({
        url: 'https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A113820133&start-date=3750daysAgo&end-date=yesterday&metrics=ga%3Ausers%2Cga%3Apageviews&access_token='+response.access_token,
        dataType: 'json',
        async: false
      }).responseText;
      var obj = $.parseJSON(jsonData);
      console.log(obj.totalsForAllResults['ga:pageviews']);
      // document.getElementById('pageviewsCount').innerHTML = obj.totalsForAllResults['ga:pageviews'];
    }
    google.setOnLoadCallback(drawChart(response.access_token));
    function drawChart(token) {
      var jsonData = function(){
        return $.ajax({
          url: 'https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A113820133&start-date=30daysAgo&end-date=today&metrics=ga%3Ausers&dimensions=ga%3AuserType&access_token='+token,
          dataType: 'json',
          async: false
        }).responseText;
      }
      var jsonData2 = function(){
        return $.ajax({
          url: 'https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A113820133&start-date=9daysAgo&end-date=today&metrics=ga%3Ausers%2Cga%3ApercentNewSessions%2Cga%3Asessions%2Cga%3AbounceRate%2Cga%3AavgSessionDuration%2Cga%3Apageviews%2Cga%3ApageviewsPerSession&dimensions=ga%3Adate&access_token='+token,
          dataType: 'json',
          async: false
        }).responseText;
      }
      var jsonData3 = function(){
        return $.ajax({
          url: 'https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A113820133&start-date=30daysAgo&end-date=today&metrics=ga%3Ausers%2Cga%3ApercentNewSessions%2Cga%3Asessions%2Cga%3AbounceRate%2Cga%3AavgSessionDuration%2Cga%3Apageviews%2Cga%3ApageviewsPerSession&dimensions=ga%3Adate&access_token='+token,
          dataType: 'json',
          async: false
        }).responseText;
      }
      var obj = $.parseJSON(jsonData());
      var obj2 = $.parseJSON(jsonData2());
      var obj3 = $.parseJSON(jsonData3());
      var nv = parseInt(obj.rows[0][1]);
      var rv = parseInt(obj.rows[1][1]);
      var data = google.visualization.arrayToDataTable([
        ['Stats', 'Counts'],
        ['New Visitor', nv],
        ['Returning Visitor', rv]
      ]);

      var usersC = [];
      var newSessionsC = [];
      var sessionsC = [];
      var bounceRateC = [];
      var AvgSessionDurationC = [];
      var pageviewsC = [];
      var pagesPerSectionC = [];
      document.getElementById('usersCount').innerHTML = obj3.totalsForAllResults['ga:users'];
      document.getElementById('newSessionsCount').innerHTML = Number(obj3.totalsForAllResults['ga:percentNewSessions']).toFixed(2);
      document.getElementById('sessionsCount').innerHTML = obj3.totalsForAllResults['ga:sessions'];
      document.getElementById('bounceRateCount').innerHTML = Number(obj3.totalsForAllResults['ga:bounceRate']).toFixed(2);
      document.getElementById('AvgSessionDurationCount').innerHTML = Number(obj3.totalsForAllResults['ga:avgSessionDuration']).toFixed(2);
      document.getElementById('pageviewsCount').innerHTML = obj3.totalsForAllResults['ga:pageviews'];
      document.getElementById('pagesPerSectionCount').innerHTML = Number(obj3.totalsForAllResults['ga:pageviewsPerSession']).toFixed(2);

      for(var i = 0; i < 10; i++) {
        usersC.push(parseInt(obj2.rows[i][1]));
        newSessionsC.push(Number(obj2.rows[i][2]).toFixed(2));
        sessionsC.push(parseInt(obj2.rows[i][3]));
        bounceRateC.push(Number(obj2.rows[i][4]).toFixed(2));
        AvgSessionDurationC.push(Number(obj2.rows[i][5]).toFixed(2));
        pageviewsC.push(parseInt(obj2.rows[i][6]));
        pagesPerSectionC.push(Number(obj2.rows[i][7]).toFixed(2));
      }

      $("#status1").sparkline(usersC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status2").sparkline(newSessionsC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status3").sparkline(sessionsC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status4").sparkline(bounceRateC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status5").sparkline(AvgSessionDurationC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status6").sparkline(pageviewsC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status7").sparkline(pagesPerSectionC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});

      var options = {
        chartArea : { height: '100%' },
        backgroundColor: 'transparent',
        legend : { 'position': 'right', 'alignment' : 'center' }
      };

      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

      $(window).resize(function(){
        chart.draw(data, options);
      });

      chart.draw(data, options);
    }
  });

  viewSelector.on('change', function(ids) {
    var newIds = {
      query: {
        ids: 'ga:113820133', // View IDs eintragen
        metrics: 'ga:sessions',
        dimensions: 'ga:date',
        'start-date': '30daysAgo',
        'end-date': 'yesterday'
      }
    }
    timeline.set(newIds).execute();

    $(window).resize(function(){
        timeline.set(newIds).execute();
      });
  });
});


</script>

</body>
</html>
