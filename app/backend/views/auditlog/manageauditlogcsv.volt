{{ content() }}
<script type="text/ng-template" id="auditfileDelete.html">
  <div ng-include="'/be/tpl/auditfileDelete.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md" ng-init="userid('<?php echo $username["bnb_userid"];?>')">
  <a class="btn btn-default btn-addon pull-right m-t-n-xs"  ui-sref="manageauditlog()"><i class="fa fa-bars"></i>Back</a>
  <h1 class="m-n font-thin h3">Audit Logs Csv Files</h1>
  <a id="top"></a>
</div>
<div class="hbox hbox-auto-xs hbox-auto-sm">
  <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Manage Csv Files
            </div>
            <div class="panel-body">

                 <div class="row wrapper">
                  <div class="col-sm-5 m-b-xs">
                    <form name="rEquired">
                      <div class="input-group">
                        <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required>
                        <span class="input-group-btn">
                          <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)" ng-disabled="rEquired.$invalid">Go!</button>
                          <button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
                        </span>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table table-striped b-t b-light">
                    <thead>
                      <tr>

                        <th style="width:60%">Filename</th>
                        <th style="width:25%">Date Created</th>
                        <th style="width:15%">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="list in loglist">

                        <td>{[{ list.filename }]}</td>
                        <td>{[{ list.datecreated }]}</td>
                        <td>
                          <a href="<?php echo $this->config->application->ApiURL; ?>/csvfiles/{[{ list.filename }]}" download> <span class="label bg-info">Download</span></a>
                          <a href="" ng-click="deletefile(list.fileid)"> <span class="label bg-danger">Delete</span></a>
                        </td>

                      </tr>
                    </tbody>
                  </table>
                </div>



            </div> <!-- end of panel body -->


          </div>
        </div>

      </div>

      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
</div>

