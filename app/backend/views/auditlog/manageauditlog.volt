{{ content() }}
<div class="bg-light lter b-b wrapper-md" ng-init="userid('<?php echo $username["bnb_userid"];?>')">
  <a class="btn btn-default btn-addon pull-right m-t-n-xs"  ui-sref="manageauditlogcsv()"><i class="fa fa-bars"></i>Csv Files</a>
  <h1 class="m-n font-thin h3">Audit Logs</h1>
  <a id="top"></a>
</div>
<div class="hbox hbox-auto-xs hbox-auto-sm">
  <div class="wrapper-md">
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Manage Logs 
              <a href ng-click="isread()"><span class="pull-right">Read <i class="fa fa-circle text-success"></i></span></a>
              <span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span>
              <a href ng-click="isunread()"><span class="pull-right">Unread <i class="fa fa-circle text-warning"></i></span></a>
              <span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span><span class="pull-right">&nbsp;</span>
              <a href ng-click="isall()"><span class="pull-right">All <i class="fa fa-circle text-primary"></i></span></a>
            </div>
            <div class="panel-body">

                 <div class="row wrapper">
                  <div class="col-sm-5 m-b-xs">
                    <form name="rEquired">
                      <div class="input-group">
                        <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required>
                        <span class="input-group-btn">
                          <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)" ng-disabled="rEquired.$invalid">Go!</button>
                          <button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
                        </span>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table table-striped b-t b-light">
                    <thead>
                      <tr>

                        <th style="width:25%">Username</th>
                        <th style="width:10%">Module</th>
                        <th style="width:10%">Event</th>
                        <th style="width:35%">Actions</th>
                        <th style="width:20%">Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="list in loglist">

                        <td>{[{ list.username }]}</td>
                        <td>{[{ list.module }]}</td>
                        <td>{[{ list.event }]}</td>
                        <td>{[{ list.title }]}</td>
                        <td>{[{ list.datetime }]}</td>

                      </tr>
                    </tbody>
                  </table>
                </div>



            </div> <!-- end of panel body -->


          </div>
        </div>

      </div>

      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-center bg-light lter">
              <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
            </footer>
        </div>
      </div>

  </div>
</div>

