{{ content() }}
<script type="text/ng-template" id="success.html">
  <div ng-include="'/be/tpl/success.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Update User</h1>
  <a id="top"></a>
</div>

<div class="line"></div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="updateData(user,file)" name="form">
<div class="container-fluid" >
  <div class="row">
    <div class="col-sm-6">
      <div class="panel panel-default">
          <div class="panel-heading font-bold"><h3>Account Information</h3></div>
          <div class="panel-body">
            <input type="hidden" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.id">

            <div class="form-group">
              <label class="col-sm-2 control-label">Username</label>
              <div class="col-sm-10">
                <input type="text" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.username" ng-change="chkusername(user.username)" ng-minlength="6" ng-pattern="/^[a-zA-Z0-9-@._]*$/" required="required" >
                <div class="popOver" ng-show="usrname">
                    Username is already taken.
                    <span class="pop-triangle"></span>
                </div>
                <em class="text-muted">(allow 'a-zA-Z0-9', 6-50 length)</em>
              </div>
            </div>

            <div class="line line-lg"></div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Email Address</label>
              <div class="col-sm-10">
                <input type="email" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.email" ng-change="chkemail(user.email)" required="required">
                <div class="popOver" ng-show="usremail">
                  Email is already taken.
                  <span class="pop-triangle"></span>
                </div>
              </div>
            </div>

            <div class="line line-lg"></div>
            <div class="form-group">
              <label class="col-lg-2 control-label">User Role</label>
              <div class="col-sm-10">
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="userrole" value="Administrator" ng-model="user.task" ng-checked="true" required="required">
                    <i></i>[ Administrator ]
                  </label>
                </div>
                 <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="userrole" value="Customer Service" ng-model="user.task" required="required">
                    <i></i>[ Customer Service ]
                  </label>
                </div>
                 <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="userrole" value="Editor" ng-model="user.task" required="required">
                    <i></i>[ Editor ]
                  </label>
                </div>

                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="userrole" value="Region Manager" ng-model="user.task" required="required">
                    <i></i>[ Region Manager ]
                  </label>
                </div>
                <div class="padding_left_27px form-group" ng-if="user.task == 'Region Manager' " >
                  <select ui-jq="chosen" class="w-md" ng-model="user.region" ng-options="regions.regionid as regions.regionname for regions in availableregion" required>
                    <optgroup label="Choose your Region">
                    </optgroup>
                  </select>
                </div>

                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="userrole" value="Center Manager" ng-model="user.task" required="required">
                    <i></i>[ Center Manager ]
                  </label>
                </div>
                <div class="padding_left_27px form-group" ng-if="user.task == 'Center Manager' " >
                  <select ui-jq="chosen" class="w-md" ng-model="user.center" ng-options="centers.centerid as centers.centertitle for centers in availablecenter" required>
                    <optgroup label="Choose your Center">
                    </optgroup>
                  </select>
                </div>
              </div>
            </div>

            <div class="line line-lg"></div>

            <div class="form-group">
              <label class="col-lg-2 control-label">Status</label>
              <div class="col-sm-10">
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="status" value="1" ng-model="user.status" required="required">
                    <i></i>Active User
                  </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="status" value="0" ng-model="user.status" required="required">
                    <i></i>Deactivate User
                  </label>
                </div>
              </div>
            </div>
          </div>
                  <div class="panel b-a">
          <div class="panel-heading b-b b-light">CHANGE PASSWORD?
            <label class="i-switch i-switch-md bg-info m-t-xs m-r">
                    <input type="checkbox"
                    ng-init="isCollapsed = false"
                    ng-click="disableditu = !disableditu;
                              user.newpassword = undefined;
                              user.password_c = undefined;
                              user.oldpassword = '';
                              isCollapsed = !isCollapsed;">
                    <i></i>
                  </label>
          </div>
        <div collapse="!isCollapsed" class="panel-body collapse out" style="height: auto;">
           <div class="form-group">
                <label class="col-sm-2 control-label">Old Password</label>
                <div class="col-sm-10">
                  <input type="password" name="oldpassword" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.oldpassword" ng-disabled="!disableditu" >
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">New Password</label>
                <div class="col-sm-10">
                  <input type="password" name="newpassword"
                    ng-model="user.newpassword"
                    ng-disabled="!disableditu"
                    class="form-control ng-invalid ng-invalid-required ng-valid-pattern"
                    ng-pattern="/^(?=.*^[a-zA-Z])(?=.*[a-z])(?=.*[A-Z])(?=.{8,})(?=.*\d)(?=.*(_|[^\w])).+$/" />
                  <em class="text-muted">(New Password must start with an alphabeth. Combination of at least 1 small letter, 1 capital letter, 1 number and 1 special character . Minimum length (8)</em>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Repeat Password</label>
                <div class="col-sm-10">
                  <input type="password" name="password_c"
                    ng-model="user.password_c"
                    class="form-control ng-invalid ng-invalid-required ng-valid-pattern"
                    ng-disabled="!disableditu"
                    ng-pattern="/^(?=.*^[a-zA-Z])(?=.*[a-z])(?=.*[A-Z])(?=.{8,})(?=.*\d)(?=.*(_|[^\w])).+$/" />
                </div>
              </div>
        </div>
      </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="panel panel-default">
          <div class="panel-heading font-bold"><h3>User Profile</h3></div>
          <div class="panel-body">

            <div class="form-group">
              <label class="col-sm-3 control-label">Profile Picture</label>
              <div class="col-sm-9">
                <input id="profile_pic" ng-model="file" type="file" data-icon="false" class="form-control" accept="image/*" ngf-select ngf-allow-dir="true" onchange="readURL(this);">
                <img id="blah" src="<?php echo $this->config->application->amazonlink . '/uploads/userimages/';?>{[{user.profile_pic_name}]}" alt="your image" />
                <label for="profile_pic" class="label_profile_pic">Change Picture</label>
              </div>
            </div>

            <div class="line line-lg"></div>

            <alert ng-repeat="alert in imagealert" type="{[{alert.type }]}" close="closeAlert($index, type='image')">{[{ alert.msg }]}</alert>

            <div class="form-group">
              <label class="col-sm-3 control-label">First Name</label>
              <div class="col-sm-9">
                <input type="text" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.fname" required="required" >
              </div>
            </div>

            <div class="line line-lg"></div>

            <div class="form-group">
              <label class="col-sm-3 control-label">Last Name</label>
              <div class="col-sm-9">
                <input type="text" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.lname" required="required" >
              </div>
            </div>

            <div class="line line-lg"></div>

            <div class="form-group">
              <label class="col-sm-3 control-label">Birthdate</label>
              <div class="col-sm-9">
                <div class="input-group w-md">
                  <span class="input-group-btn">
                  <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="user.bday" is-open="opened" datepicker-options="dateOptions" close-text="Close" type="text" disabled>
                  <button type="button" class="btn btn-default" ng-click="open($event)">
                    <i class="glyphicon glyphicon-calendar"></i>
                  </button>
                  </span>
                </div>
              </div>
            </div>

            <div class="line line-lg"></div>

            <div class="form-group">
              <label class="col-sm-3 control-label">Contact</label>
              <div class="col-sm-9">
                <input type="text" class="form-control phone_us" ng-model="user.contact" ng-required="true" minlength=14>
              </div>
            </div>

            <div class="line line-lg"></div>

            <div class="form-group">
              <label class="col-sm-3 control-label">Gender</label>
              <div class="col-sm-9">
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="gender" value="Male" ng-model="user.gender" required="required">
                    <i></i>Male
                  </label>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="gender" value="Female" ng-model="user.gender" required="required">
                    <i></i>Female
                  </label>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div> <!-- end of col-s-6 -->
  </div> <!-- row -->
  <div class="row">
    <div class="col-sm-12" style="text-align:right">

      <a ui-sref="userlist" class="btn btn-default"> Cancel </a>
      <button type="submit" class="btn btn-success" ng-disabled="form.$invalid" >Submit</button>

    </div>
  </div>
</div> <!-- container -->
</form>

<div class="back_chrono" ng-show="processing">
  <center>
  <div class="wobblebar-loader">
    Loading…
  </div>
  </center>
</div>
