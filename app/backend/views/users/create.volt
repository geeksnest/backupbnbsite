{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="success.html">
  <div ng-include="'/be/tpl/success.html'"></div>
</script>

<fieldset ng-disabled="isSaving">
  <form id="createUserForm" name="createUserForm" ng-submit="submitData(user,file)">

  <div class="bg-light lter b-b wrapper-md">
    <span class="m-n font-thin h3">Create User</span>
    <a id="top"></a>
    <span class="pull-right">
	  	<button class="btn btn-md btn-success" title="Save" ng-click="wew(user)"><i class="fa fa-floppy-o"></i></button>
	  </span>
  </div>


    <div class="row wrapper-md">
      <div class="col-sm-12">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index, type='default')">{[{ alert.msg }]}</alert>
        <alert ng-repeat="alert in imagealert" type="{[{alert.type }]}" close="closeAlert($index, type='image')">{[{ alert.msg }]}</alert>
      </div>

      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">Account Information</div>
          <div class="panel-body">

            <div class="form-group">
              <label>Username</label>
              <input id="username" type="text" class="form-control" ng-model="user.username" ng-change="chkusername(user.username)" minlength="6" maxlength="50" pattern="^[a-zA-Z0-9-@._]*$" required>
              <div class="popOver" ng-show="usrname">
                Username is already taken.
                <span class="pop-triangle"></span>
              </div>
              <em class="text-muted">(Must 6-50 characters.Only allow a-z 0-9 @ . _ )</em>
            </div>

            <div class="form-group">
              <label>Email Address</label>
              <input id="useremail" type="email" class="form-control" ng-model="user.email" ng-change="chkemail(user.email)" pattern="^[^\s@]+@[^\s@]+\.[^\s@]{2,}$" title="Please enter a valid email address" required>
              <div class="popOver" ng-show="usremail">
                  Email is already taken.
                  <span class="pop-triangle"></span>
              </div>
            </div>

            <div class="form-group">
              <label>Password</label>
              <input id="password" type="password" name="password" class="form-control" ng-model="user.password"
                        ng-change="confirmpass(user.password, user.conpass)"
                        minlength="8"
                        pattern="^(?=.*^[a-zA-Z])(?=.*[a-z])(?=.*[A-Z])(?=.{8,})(?=.*\d)(?=.*(_|[^\w])).+$"
                        required>
              <em class="text-muted">(New Password must start with an alphabeth. Combination of at least 1 small letter, 1 capital letter, 1 number and 1 special character . Minimum length (8)</em>
            </div>

            <div class="form-group">
              <label>Confirm Password</label>
              <input id="confirmpassword" type="password"  name="confirmpassword" class="form-control"
                ng-model="user.conpass"
                ng-change="confirmpass(user.password, user.conpass)"
                pattern="^(?=.*^[a-zA-Z])(?=.*[a-z])(?=.*[A-Z])(?=.{8,})(?=.*\d)(?=.*(_|[^\w])).+$"
                required>
              <span ng-show="confpass">Passwords dont</span>
               <div class="popOver" ng-show="confpass">
                  Please match your passwords
                  <span class="pop-triangle"></span>
              </div>
            </div>

            <div class="form-group" ng-init="user.userrole = 'Administrator' ">
              <label>User Role</label>
              <div class="col-sm-11 col-sm-offset-1">
                <div class="radio">
                  <label class="i-checks">
                    <input id="administrator" type="radio" name="userrole" value="Administrator" ng-model="user.userrole" required="required">
                    <i></i>
                    [ Administrator ]
                  </label>
                </div>

                <div class="line line-lg"></div>

                <div class="radio">
                  <label class="i-checks">
                    <input id="customerservice" type="radio" name="userrole" value="Customer Service" ng-model="user.userrole" required="required">
                    <i></i>
                    [ Customer Service ]
                  </label>
                </div>

                <div class="line line-lg"></div>

                <div class="radio">
                  <label class="i-checks">
                    <input id="editor" type="radio" name="userrole" value="Editor" ng-model="user.userrole" required="required">
                    <i></i>
                    [ Editor ]
                  </label>
                </div>

                <div class="line line-lg"></div>

                <div class="radio" ng-hide="hideregion">
                  <label class="i-checks">
                    <input id="regionamanager" type="radio" name="userrole" value="Region Manager" ng-model="user.userrole" required="required">
                    <i></i>
                    [ Region manager ]
                  </label>
                </div>

                <div class="padding_left_27px form-group" ng-if="user.userrole == 'Region Manager' " >
                  <select id="dropdownregion" ui-jq="chosen" class="w-md" ng-model="user.region" required>
                    <optgroup label="Choose your Region">
                      <option ng-repeat="region in availableregion" value="{[{region.regionid}]}">{[{region.regionname}]}</option>
                    </optgroup>
                  </select>
                </div>

                <div class="line line-lg"></div>

                <div class="radio" ng-hide="hidecenter">
                  <label class="i-checks">
                    <input id="centermanager" type="radio" name="userrole" value="Center Manager" ng-model="user.userrole" required="required">
                    <i></i>
                    [ Center Manager ]
                  </label>
                </div>
                <div class="padding_left_27px form-group" ng-if="user.userrole == 'Center Manager' ">
                  <select id="dropdowncenter" ui-jq="chosen" class="w-md" ng-model="user.center" required>
                    <optgroup label="Choose your Center">
                      <option ng-repeat="center in availablecenter" value="{[{center.centerid}]}">{[{center.centertitle}]}</option>
                    </optgroup>
                  </select>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label>Status</label>
              <label class="i-switch i-switch-md bg-info m-t-xs m-r" style="position: relative; top:8px; left:22px;">
                <input type="checkbox" checked="" ng-true-value="'1'" ng-false-value="'0'" ng-model="user.status" >
                <i></i>
              </label>
            </div>

          </div>
        </div> {# panel #}
      </div>  {# col-sm-6 #}

      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">User Profile</div>
          <div class="panel-body">

            <div class="form-group">
              <label>Profile Picture</label>
              <div>
                <input id="profile_pic" ng-model="file" type="file" data-icon="false" class="form-control" accept="image/*" ngf-select ngf-allow-dir="true" onchange="readURL(this);">
                <img id="blah" src="/img/default_profile_pic.jpg" alt="your image" />
                <label for="profile_pic" class="label_profile_pic">Change Picture</label>
              </div>
            </div>

            <div class="form-group">
              <label>First Name</label>
              <input type="text" id="firstname" name="firstname" class="form-control" ng-model="user.fname" required >
            </div>

            <div class="form-group">
              <label>Last Name</label>
              <input type="text" id="lastname" name="lastname" class="form-control" ng-model="user.lname" required>
            </div>

            <div class="form-group">
              <label>Birthday</label>
              <div class="input-group">
                <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="user.bday" is-open="opened" datepicker-options="dateOptions" close-text="Close" type="text" disabled>
                <span class="input-group-btn">
                  <button id="btndate" type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                </span>
              </div>
            </div>

            <div class="form-group">
              <label>Contact Number</label>
              <input type="text" id="contact" name="contact" class="form-control phone_us" ng-model="user.contact" placeholder="(123) 123-1234" minlength=14 required>
            </div>

            <div class="form-group" ng-init="user.gender='Male'">
              <label>Gender</label>
              <div class="col-sm-11 col-sm-offset-1">
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="gender" value="Male" ng-model="user.gender" required="required">
                    <i></i>
                    Male
                  </label>
                </div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="gender" value="Female" ng-model="user.gender" required="required">
                    <i></i>
                    Female
                  </label>
                </div>
              </div>
            </div>

          </div>
        </div> {# panel #}
      </div> {# col-sm-6 #}
    </div>

  </form>
</fieldset>



<div class="back_chrono" ng-show="processing">
  <center>
  <div class="wobblebar-loader">
    Loading…
  </div>
  </center>
</div>

<script>
 function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
