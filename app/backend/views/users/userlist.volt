{{ content() }}
<script type="text/ng-template" id="userDelete.html">
  <div ng-include="'/be/tpl/userDelete.html'"></div>
</script>
<script type="text/ng-template" id="userUpdate.html">
  <div ng-include="'/be/tpl/userUpdate.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">User List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md" ng-controller="UserListCtrl">
<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
  <div class="panel panel-default">
    <div class="row wrapper">
      <div class="col-sm-6">
        <div class="input-group">
          <span class="input-group-addon">
            Search
          </span>
          <input class="form-control" placeholder="Search Username/First Name/Last Name" type="text" name="searchtext" ng-model="searchtext" ng-change="search(searchtext)">
          <!-- <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" >Go!</button>
          </span> -->
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
<!--             <th style="width:20px;">
              <label class="i-checks m-b-none">
                <input type="checkbox"><i></i>
              </label>
            </th> -->
            {#<th>Name</th>#}
            <th>Username</th>
            <th>Email</th>
            <th>Status</th>
            <th>User Role</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="user in data.data" ng-if="user.username != 'superagent'">
            <!-- <td><label class="i-checks m-b-none"><input name="post[]" type="checkbox"><i></i></label></td> -->
            {#<td>{[{ user.first_name }]} {[{ user.last_name }]}</td>#}
            <td ng-bind="user.username"></td>
            <td ng-bind="user.email"></td>
            <td>
              <div class="pagestatuscontent fade-in-out">
                {#<i ng-if="user.status == 1" class="fa fa-lightbulb-o text-lg text-warning"></i>#}
                {#<i ng-if="user.status == 0" class="fa fa-lightbulb-o text-lg"></i>#}
                <span ng-if="user.status == 1" class="label bg-success">Online</span>
                <span ng-if="user.status == 0" class="label bg-danger">Offline</span>

              </div>
              <div class="checkstatuscontent">
                <label class="i-switch bg-info m-t-xs m-r">
                  <input type="checkbox" ng-true-value="'1'" ng-false-value="'0'" ng-model="user.status" ng-click="setstatus(user.id,user.status)">
                  <i></i>
                </label>
              </div>
              <!-- <div id="{[{user.id}]}" class="wew"><spand class="fade-in-out"><i class="fa fa-check"></i></spand></div> -->
            </td>
            <td>
              <span ng-bind="user.task"></span></td>
            <td>
              <button ng-click="update(user.id)" class="btn btn-sm btn-default" title="Edit">
                <i class="glyphicon glyphicon-edit text-info"></i>
              </button>
              <button ng-click="delete(user.id)" class="btn btn-sm btn-default">
                <i class="glyphicon glyphicon-trash text-danger"></i>
              </button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
        <center>
          <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
        </center>
      </div>
    </footer>
  </div>
</div>
